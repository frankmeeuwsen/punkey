<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>punkey.com&nbsp;-&nbsp;Een nieuwe pivothosting weblog</title>
	<link rel="alternate" type="application/rss+xml" title="RSS" href="/rss.xml"/>
	<link rel="alternate" type="application/atom+xml" title="Atom" href="/atom.xml"/>
	<meta http-equiv="Content-Type" content="text/html; charset='iso-8859-1'" />
	<link rel="stylesheet" type="text/css" href="/pivot/templates/pivot_template.css" />
	<script type="text/javascript">
		var dir = "/pivot/templates/";
	</script>
	<script src="/pivot/templates/sleight.js" type="text/javascript"></script>
	<style type="text/css">

	/* Some default styles for the calendar.. */
	.defcalendar { font-size: 12px;  }
	.defcalendar td { padding: 1px 4px; }
	.defcalendar td:hover { background-color: #FF6; }
	.defcalendarlink { font-size: 10px; }
	.defcalendartoday { background-color: #FF9; border: 1px solid #999; font-weight: bold; }

	</style>
	<?php $default_calender_style = TRUE; ?>

</head>

<body>
 <div id="header">
  <img src="/pivot/templates/grad_1.png" width="760" height="90" align="left" alt="" /> 
  <h1 class="top"><a href="/index.php">punkey.com</a></h1>
  <h1 class="shadow">punkey.com</h1>
</div>
<div id="subheader">
  <img src="/pivot/templates/grad_2.png" width="760" height="40" align="right" alt="" /> 
  <h2>Een nieuwe pivothosting weblog</h2>
</div>
 

<div class="column-wrap">
  <div class="column-1">
	<div class="column-inner">
	  <span id='e4'></span><span class="date"><a href="/pivot/entry.php?id=4" title="Permanent link to entry 'Welkom..'">&para;</a> <a href="/archives/archive_2004-m04.php#e4" title="Permanent link to 'Welkom..' in the archives">&sect;</a></span><h2>Welkom..</h2>

Als je dit kunt lezen, dan is de site operationeel. Welkom!<br />
<br />
(artikelen die in categorie 'default' worden gepost komen in deze kolom terecht)  

<div class="entryfooter"> 04 Apr '04 -  11:44 | <script type="text/javascript">
<!--
	var first = 'ma';
	var second = 'il';
	var third = 'to:';
	var address = '&#97;&#100;&#109;&#105;&#110;';
	var domain = '&#101;&#120;&#97;&#109;&#112;&#108;&#101;&#46;&#111;&#114;&#103;';
	document.write('<a href="');
	document.write(first+second+third);
	document.write(address);
	document.write('&#64;');
	document.write(domain);
	document.write('" title="email Admin">');
	document.write('Admin<\/a>');
// -->
</script> | default | <a href="/pivot/entry.php?id=4#comm" title="">Nog geen reacties</a> </div>
<span id='e1'></span><span class="date"><a href="/pivot/entry.php?id=1" title="Permanent link to entry 'Links..'">&para;</a> <a href="/archives/archive_2004-m04.php#e1" title="Permanent link to 'Links..' in the archives">&sect;</a></span><h2>Links..</h2>

Snelkoppelingen naar sommige van de functionaliteiten:<ul><li><a href="/pivot/"  target='_blank'>Inloggen Pivot;</a></li><li><a href="/photostack/"  target='_blank'>Fotoalbums bekijken;</a></li><li><a href="/poll/admin/"  target='_blank'>Inloggen Poll beheer;</a></li><li><a href="/bbclone/"  target='_blank'>BBclone statistieken bekijken;</a></li></ul>  

<div class="entryfooter">   11:43 | <script type="text/javascript">
<!--
	var first = 'ma';
	var second = 'il';
	var third = 'to:';
	var address = '&#97;&#100;&#109;&#105;&#110;';
	var domain = '&#101;&#120;&#97;&#109;&#112;&#108;&#101;&#46;&#111;&#114;&#103;';
	document.write('<a href="');
	document.write(first+second+third);
	document.write(address);
	document.write('&#64;');
	document.write(domain);
	document.write('" title="email Admin">');
	document.write('Admin<\/a>');
// -->
</script> | default | <a href="/pivot/entry.php?id=1#comm" title="">Nog geen reacties</a> </div>

	</div>
  </div>
  <div class="column-2">
	<div class="column-inner">
	  <h2>Linkdump</h2>
	  <span id='e2'></span><span class="date_linkdump"><a href="/pivot/entry.php?id=2" title="Permanent link to entry 'Rups..'">&para;</a> <a href="/archives/archive_2004-m04.php#e2" title="Permanent link to 'Rups..' in the archives">&sect;</a></span><b>Rups..</b><br />

<p>Een popup-plaatje, ter illustratie:<br />
<p style="text-align:center;"><a href='http://punkey.logt.cc/images/caterpillar.jpg'  onclick="window.open('http://punkey.logt.cc/pivot/includes/photo.php?img=http://punkey.logt.cc/images/caterpillar.jpg&amp;w=710&amp;h=550&amp;t=Rups!','imagewindow','width=710,height=550,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"><img src="http://punkey.logt.cc/images/caterpillar.thumb.jpg" border="1" alt="Rups!" title="Rups!" /></a></p>
<div class="entryfooter_linkdump">04-04 | <a href="/pivot/entry.php?id=2#comm" title="">Nog geen reacties</a></div>


<span id='e3'></span><span class="date_linkdump"><a href="/pivot/entry.php?id=3" title="Permanent link to entry 'Linkdump..'">&para;</a> <a href="/archives/archive_2004-m04.php#e3" title="Permanent link to 'Linkdump..' in the archives">&sect;</a></span><b>Linkdump..</b><br />

<p>Artikelen die in de categorie linkdump worden gepost komen hier terecht. </p>
<div class="entryfooter_linkdump">04-04 | <a href="/pivot/entry.php?id=3#comm" title="">Nog geen reacties</a></div>



	</div>
  </div>
  <div class="column-3">
	<div class="column-inner">
	  <h3>Search</h3>
	  <form method='post' action='/pivot/search.php'>
<input type='text' name='search' class='searchbox' value='' />
<input type='submit' class='searchbutton' value='go!' />
</form>
 

	  <h3> Archieven</h3>
	  <p><a href="/archives/archive_2004-m04.php">01 Apr - 31 Apr 2004 </a><br /></p>
		 
	  <h3>Links</h3>
	  <p>

<a href="http://www.google.com/"  target='_blank'>Google</a><br />
<a href="http://www.pivotlog.net/"  target='_blank'>Pivot</a><br /><br />

To change this list, edit the file '_aux_link_list.html' in your pivot's templates folder.

</p>
		 
	  <h3>Stuff</h3>
	  <p>
		<a href="http://www.pivotlog.net/?ver=Pivot+-+1.12%3A+%27Soundwave%27"  title="Powered by Pivot - 1.12: 'Soundwave'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered by Pivot" style="border:0;" /></a> &nbsp;<br />
		<a href="/rss.xml"  title='XML: RSS feed' target='_blank'><img src='/pivot/pics/rssbutton.png' width='94' height='15' alt='XML: RSS feed' style='border:0px;' /></a>&nbsp;<br />
		<a href="/atom.xml"  title='XML: Atom feed' target='_blank'><img src='/pivot/pics/atombutton.png' width='94' height='15' alt='XML: Atom feed' style='border:0px;' /></a>&nbsp;<br />
	  </p>
		 
	</div>
  </div>
</div>
<!-- close column-wrap -->
<div class="footer">
 <!-- this is where the footer text would go.. -->
</div>
</body>
<?php
	define("_BBC_PAGE_NAME", "Archiefpagina");
	define("_BBCLONE_DIR", "bbclone/");
	define("COUNTER", _BBCLONE_DIR."mark_page.php");
	if (is_readable(COUNTER)) include_once(COUNTER);
?>
</html>
