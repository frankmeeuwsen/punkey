<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under 
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------


// ------------------------------------
// The Pivot moblog script, version 1.4
// ------------------------------------

// defaults for the entry

// our advice: do not use a default for the password. If people guess 
// the location of your moblog script, this will allow to post to your pivot.

$entry['user']="moblog";
// $entry['pass']="moblog%1";

$entry['category']="default";
$entry['title']="Moblog post";
$entry['status']="publish";
$entry['allow_comments']=1;

// some prefs and defaults:
$maxwidth = 400;
$maxheight = 200;


// for now, use a file as input. (this will obviously be replaced by getting 
// the stdin when this script is called via a .procmailwhatsits
$input = stripslashes($HTTP_POST_VARS['input']);

// uncomment this if you want to log the incoming files..
// $fp = fopen("../mail_".date('mdHis').".txt", "wb");
// fwrite ($fp, $input);
// fclose($fp);


// --------

// The code to split the mail and write the images..

// mimeDecode.php is a mangled version of the PEAR class
include_once("includes/mimeDecode.php");
include_once("pv_core.php");

$params['include_bodies'] = TRUE;
$params['decode_bodies']  = TRUE;
$params['decode_headers'] = FALSE;


$decode = new Mail_mimeDecode($input, "\r\n");
$structure = $decode->decode($params);

// get the replyaddress.. 
// get the replyaddress.. 
if (isset($structure->headers['x-loop'])) {
	$replyaddress = "";
} else {
	$replyto1 = $structure->headers['from'];
	$replyto2 = $structure->headers['return-path'];
	$replyaddress = ($replyto2 != "") ? $replyto2 : $replyto1;
}


// for 'plain text' messages, parse the body.
parse_body($structure->body);

// for mime mail, parse each part
// for mime mail, parse each part
foreach ($structure->parts as $part) {

	echo "<br />part: ".$part->ctype_primary ."<br />";

	if (strtolower($part->ctype_primary) == "text") {

		parse_body($part->body);
				
	} else if ( (strtolower($part->ctype_primary) == "image") || (strtolower($part->ctype_primary) == "application") ) {
		// It's an image. We'll add all the images as an array to the entry..
		// get the original filename from the email..

		$filename = isset($part->ctype_parameters['name']) ? $part->ctype_parameters['name'] : $part->d_parameters['filename'];
		$filename= safe_string(strtolower($filename), FALSE);

		$ext= getextension($filename);

		if ( ($filename !="") && ( ($ext=="jpg") || ($ext=="jpeg") ||  ($ext=="gif") ||  ($ext=="png") )) {
			$fp = fopen("../".$Cfg['upload_path'].$filename, "wb");
			fwrite($fp, $part->body);
			fclose($fp);

			list ($mywidth, $myheight) = getimagesize("../".$Cfg['upload_path'].$filename);

			if ( ($mywidth=="") && ($mywidth=="") ) {
				// Some mailers like pine, need content to get base64_decode'd
				$fp = fopen("../".$Cfg['upload_path'].$filename, "wb");
				fwrite($fp, base64_decode($part->body));
				fclose($fp);
				list ($mywidth, $myheight) = getimagesize("../".$Cfg['upload_path'].$filename);
			}

			if ( ($mywidth > $maxwidth) || ($myheight > $maxheight)) {

				include_once "modules/module_imagefunctions.php";

				$thumbfile = resize_image($imagesdir.$filename, $maxwidth, $maxheight);
				
				if (strlen($entry['introduction'])>2) {
					$entry['introduction'] .="\n";
				}

				if (strlen($thumbfile)>2) {
					$entry['introduction'] .="[[popup:$filename:(thumbnail)::center:1]]";
				} else {
					$entry['introduction'] .="[[popup:$filename:$filename::center:1]]";
				}

			} else {

				$entry['introduction'] .= "\n[[image:$filename]]";

			}
		} // end if ($filename ... )
	}
}


// check if the user and pass are valid
if ($Users[$entry['user']]['pass'] == md5($entry['pass']))  {

	if (strlen($entry['introduction'])>8) {


		// if so, save the new entry and generate files (if necesary)
		$db = new db(); 
		$entry['code']=">";
		$entry['date']= date('Y-m-d-H-i');

		//fix the category
		$entry['category'] = array ($entry['category']);

		$entry = $db->set_entry($entry);
		$db->save_entry();

		// remove it from cache, to make sure the latest one is used.
		$db->unread_entry($entry['code']);

		make_filename($Pivot_Vars['piv_code'], $Pivot_Vars['piv_weblog'], 'message', $message);

		// regenerate entry, frontpage and archive..
		generate_pages($Pivot_Vars['piv_code'], TRUE, TRUE, TRUE);

		$msg = "Your entry has been posted! \n\n";
		$msg .= "title: ".$entry['title'];
		$msg .= "\nuser: ".$entry['user'];
		$msg .= "\ncat: ". implode("", $entry['category']);
		$msg .= "\nintroduction: ".$entry['introduction'];

		$msg_title = "[moblog] Succes!";

	} else {

		$msg = "Not posted: Could not parse your entry\n\n";
		$msg .= "please report this to bob@mijnkopthee.nl , and refer to message #".date('mdHis');
		$msg_title = "[moblog] Not succesful.";

	}

} else {

	$msg = "Not posted: Wrong User and or Password";
	$msg_title = "[moblog] Not succesful.";

}

// to wrap it up, send a confirmation by mail..
$msg.= "\n\nprocessed: ". date("dS of F Y H:i:s")."\n";
$add_header=sprintf("From: %s", $replyaddress."\n");
$add_header=sprintf("x-loop: pivot-moblog\n");

if ( $replyaddress != "") {
	mail( $replyaddress, $msg_title, $msg, $add_header);
}




// ---- functions -----

function parse_body($body) {
	global $entry;

	$body = strip_tags($body, "<a><b><i><u><s>");

		//We try to find out where the line containing the title is at...
	if (preg_match("/^title:(.*)/mi", $body, $title)) {
		//And put the title var in the proper place
		$entry['title'] = trim($title[1]);
	} 

	//We repeat the same trick as above... for all vars wanted
	if (preg_match("/^user:(.*)/mi", $body, $user)) {
		$entry['user'] = trim($user[1]); 
	}

	// in case ppl are lazy and use pass instead of password
	if (preg_match("/^pass:(.*)/mi", $body, $pass)) {
		$entry['pass'] = trim($pass[1]);
	} else if (preg_match("/^password:(.*)/mi", $body, $password)) {
		$entry['pass'] = trim($password[1]);
	}

	if (preg_match("/^publish:(.*)/mi", $body, $publish)) {
		if (trim($publish[1]) == "1") {
			$entry['status'] = 'publish';
		} else {
			$entry['status'] = 'hold';
		}
	}

	//remove the pivot: line..
	if (preg_match("/pivot:(.*)/mi", $body, $publish)) {
		//nothing..
	}
	
	// in case ppl are lazy and use cat instead of category
	if (preg_match("/^cat:(.*)/mi", $body, $cat)) {
		$entry['category'] = trim($cat[1]);
	} else if (preg_match("/^category:(.*)/mi", $body, $category)) {	
		$entry['category'] = trim($category[1]);
	}
	
	//We strip out all the lines we already used, and use what's left as the body
	$body = str_replace ($title[0], "", $body);		
	$body = str_replace ($user[0], "", $body);
	$body = str_replace ($pass[0], "", $body);
	$body = str_replace ($password[0], "", $body);
	$body = str_replace ($cat[0], "", $body);
	$body = str_replace ($publish[0], "", $body);
	$body = str_replace ($category[0], "", $body);
	$body = str_replace ($pivot[0], "", $body);


	// strip off a standard .sig. Properly formed .sigs start with '-- ' on a new line.
	list($body, $sig) = explode("\n-- ", $body);

	$body = tidy(nl2br(trim(tidy($body))));

	if (strlen($body)>strlen($entry['introduction'])) {
		$entry['introduction'] = $body;
	}	
}


function tidy($text) {

	$text = str_replace("&nbsp;<br />", "", $text);
	$text = preg_replace("/([\n\r\t])+/is", "\n", $text);

	return ($text);
}

?>