<?php
//Russian

//the above line is needed so that pivot knows how to display it in the user info.
//it also needs to be on the 2rd line.

// Russian translation of Pivot lang file
// Created by Mikhail M. Pigulsky
// http://mikhail.pp.ru

// allow for different encoding for non-western languages
$encoding="koi8-r";
$langname="ru";


//      General      \\
$lang['general'] = array (
	'yes' => '��',   //affirmative
	'no' => '���',      //negative
	'go' => '�����!',   //proceed

	'minlevel' => '� ��� ��� ���� ��� ����� � ��� ����� Pivot\'�',
	'email' => 'Email',         
	'url' => 'URL',
	'further_options' => "�������������� �����",
	'basic_view' => "������� ���",
	'basic_view_desc' => "�������� ������ ����� ������ �����",
	'extended_view' => "����������� ���",
	'extended_view_desc' => "�������� ���� �����",
	'select' => "�������",
	'cancel' => "��������",
	'delete' => '�������',
	'welcome' => "����� ���������� � %build%.",
	'write' => "��������",
	'done' => "���������!",
	'shortcuts' => "������",		 
	'cantdelete' => "You are not allowed to delete entry %title%!",
	'cantdothat' => "You are not allowed to do that with entry %title%!",
);


$lang['userlevels'] = array (
		 '���������', '�������������', '�����������', '�������', '���������'
		 //  this one might be a bit hard to translate, but basically it's an order of
		 //  power or trust.  Superadmin would be the person in charge - no one can do
		 //  anything about his decisions. Admin is only regulated by the Superadmin, 
		 //  Advanced by the Admin and Superadmin, etc..
		 //  Just get the idea of it.			 
);


$lang['numbers'] = array (
	'����', '����', '���', '���', '������', '����', '�����', '����' //, 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen'
);


$lang['months'] = array (
	'������', '�������', '����', '������', '���', '����', '����', '������', '��������', '�������', '������', '�������'
);   

		
		 
$lang['months_abbr'] = array (
	'���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���'	
);


$lang['days'] = array (
	'�����������', '�����������', '�������', '�����', '�������', '�������', '�������'
);


$lang['days_abbr'] = array (
	'���', '���', '���', '���', '���', '���', '���'
);


$lang['days_calendar'] = array (
	'S', 'M', 'T', 'W', 'T', 'F', 'S'
); 

	
$lang['datetime_words'] = array (
	'���', '�����', '������', '����', '���', '������', '�������'   //the actual words for them.
);


//      Login Page      \\
$lang['login'] = array (
	'title' => '����',
	'name' => '������������',
	'pass' => '������',
	'remember' => '���������',
	'rchoice' => array (
		'0' => '�� ����',
		'1' => '��� ������������ � ������',
		'2' => '��� � ���� ��� ����� ���� � �������',
	),
	'delete_cookies_desc' => 'If you are certain you\'re using the correct username and password, but you are <br />having problems logging in, you might try deleting the cookies for this domain:',
	'delete_cookies' => 'Delete cookies',
	'retry' => '������������ ��� ������������/������',
	'banned' => '�� �� ������ ����� � ������� �� ������ �������. ��� IP ��� ������ � ��� ���� �� 12 �����.',
);


//      Main Bar      \\
	$lang['userbar'] = array (
	'main' => '������ �����',
	'entries' => '������',
	'submit' => '����� ������',
	'comments' => '�����������',
	'modify' => '�������� ������',
	'userinfo' => '��� ����',
	'u_settings' => '��� ���������',
	'u_marklet' => 'Bookmarklets',
	'files' => '���������� �������',
	'upload' => '���������',
	'stats' => '����������',
	'admin' => '�����',

	'main_title' => '���������� ������ Pivot\'�',
	'entries_title' => '������ �������',
	'submit_title' => '����� ������',
	'comments_title' => '������ � �������� ������������',
	'modify_title' => '��������� ������',
	'userinfo_title' => '�������� ���������� � ����',
	'u_settings_title' => '������ ������ ��������',
	'u_marklet_title' => '������� ��������',
	'files_title' => '���������� � ������� ������',
	'upload_title' => '��������� �����',
	'uploaded_success' => '���� ��� ��������',
	'stats_title' => '�������� ����� � ����������.',
	'updatetitles_title' => '�������� ����� � ����������.',
	'admin_title' => '������ �����������������',
	'recent_entries' => 'Recent Entries',
	'recent_comments' => 'Recent Comments',
);


$lang['adminbar'] = array (
	//      Admin Bar      \\
	//'trebuild' => 'Rebuild all Files', rolled into maintenance
	'seeusers' => '������������',
	'seeconfig' => '���������',
	'templates' => '�������',
	'maintenance' => '��������',
	'regen' => '���������� ��� �����',
	'blogs' => '�������',
	'categories' => '���������',
	'verifydb' => '��������� ���� ������',
	'buildindex' => '���������� ������',
	'buildsearchindex' => 'Rebuild the Search Index',
	'buildfrontpage' => '���������� ���������(��) ��������(�)',
	'sendping' => '��������� �����',
	'backup' => '��������� �����',
	'description' => '��������',
	'conversion' => '��������������',
	'seeusers_title' => '��������, ������ � �������� �������������.',
	'userfields' => 'User Information Fields',
	'userfields_title' => '��������, ������ � �������� ���������������� �����',
	'seeconfig_title' => '�������� ���������������� ����',
	'templates_title' => '��������, ������ � �������� ��������',
	'maintenance_title' => '���������� ������� �������� � ������� Pivot\'�',
	'regen_title' => '���������� ����� � ������ ������� ������� Pivot',
	'blogs_title' => '��������, ������ � �������� ��������',
	'blogs_edit_title' => '�������� ��������� ������� ��� ',
	'categories_title' => '��������, ������ � �������� ���������',
	'verifydb_title' => '��������� ������������ ���� ������',
	'buildindex_title' => '���������� ������ ���� ������',
	'buildsearchindex_title' => 'Rebuild the Searchindex, to allow searching in entries',
	'buildfrontpage_title' => '���������� ��������� �����, ��������� ������ � RSS ����� ��� ������� �������.',
	'backup_title' => '�������� ��������� ����� ������� ������ �������',
	'ipblocks' => 'IP �����',
	'ipblocks_title' => '�������� � ������� ������������� IP.',
	'ipblocks_stored' => 'The IP-addresses have been stored.',
	'ipblocks_store' => 'Store these IP-addresses',
	'fileexplore' => '�������� ���������',
	'fileexplore_title' => '�������� ����� (��������� � ���� ������)',
	'sendping_title' => '�������� ����� � ������-��������.',
	'buildindex_finished' => 'Generating index took %num% seconds',
	'buildsearchindex_start' => 'Now building Search Index. This may take a short while, so please do not interrupt.',
	'buildindex_finished' => 'Opnieuw samenstellen van de index duurde %num% seconden',
);


$lang['templates'] = array (
	'rollback' => '�����',
	'create_template' => '������� ������',
	'create_template_info' => '������� ������ Pivot\'� � ����',
	'no_comment' => '��� ������������',
	'comment' => '�����������*',
	'comment_note' => '(*������: ����������� ����� ���� ��������� <b>������ ��� ������ ������ ���������</b> ��� ��������!)',
	'create' => '������� ������',
	'editing' => '������',
	'filename' => '��� �����',
	'save_changes' => '��������� ���������!',
	'save_template' => '��������� ������!',
);


//      Admin         \\
// bob notes: Mark made these, i think they should be replaced by the 'adminbar_xxx_title'] ones
$lang['admin'] = array (
	'seeusers' => '��������, ������ � �������� �������������',
	'seeconfig' => '������ ����� ��������',
	'templates' => '��������, ������ � �������� ��������',
	'maintenance' => '���������� ������� �������� Pivot\'� ��� �������, �������� \'���������� �����\', \'������� ���� ������\', \'���������� ������\' � \'�������� ��������� �����\'.',
	'regen' => '���������� ��� �������� ������� ������� Pivot',
	'blogs' => '��������, ������ � �������� ��������� �������� � ������� ����� ������ Pivot',
);


//      Maintenace      \\   
$lang['maint'] = array (
	'title' => '��������',
	'gen_arc_title' => '������������� �����', /* bob notes: redundant, see 'regen' */
	'gen_arc_text' => '���������������� ��� ������', /* bob notes: redundant, see 'regen' */
	'xml_title' => '��������� XML �����', /* bob notes: replace with more general 'Verify DB' */
	'xml_text' => '��������� (� ��������, ���� ����) ������������ ���� XML ������', /* bob notes: replace with more general 'Verify DB' */
	'backup_title' => '��������� �����',
	'backup_text' => '������� ��������� ����� ���� ������������ ������ pivot\'�',
);


//      Stats and referers      \\
$lang['stats'] = array (
	'show_last' => "Show the last",
	'20ref' => "20 referrers",
	'50ref' => "50 referrers",
	'allref' => "all referrers",
	'updateref' => "Update the referer to title mappings",
	'hostaddress' => 'Host-address (ip-address)', 
	'which page' => 'welke pagina',

	'getting' => '�������� ����� ���������',
	'awhile' => '��� ����� ������ �������� �����, ���������� ���������',
	'firstpass' => '������ ������',
	'secondpass' => '������ ������',
	'nowuptodate' => '��� ���������-��������� ���������',
	'finished' => '���������',
);


//      User Info      \\
	$lang['userinfo'] = array (
	'editfields' => '������ �����',
	'desc_editfields' => '������ ����� ������� ������������ ����� ������������ ����� ������� ����',
	'username' => '������������',
	'pass1' => '������',
	'pass2' => '������ (��� ���)',
	'email' => 'Email',
	'userlevel' => '������� ������������',
	'userlevel_desc' => '������� ������������ ���������� ����� �������� �� ����� �����������',
	'language' => '����',
	'edituser' => '��������� ������������',  //the link to.. well, edit the user (also the title)
	'edituserinfo' => '������ ���������� � ������������',
	'newuser' => '������� ������ ������������',
	'desc_newuser' => '������� ����� ������� ��� Pivot\'�, ����������� ������� � ������.',
	'newuser_button' => '�������',
	'edituser_button' => '��������',
	'pass_too_short' => '������ ������ ���� ��� ������� �� 4 ������.',
	'pass_dont_match' => '������ �� ���������',
	'username_in_use' => '����� ������������ ��� ���������',
	'username_too_short' => '��� ������������ ������ �������� �� 3 �������� ��� �����',
	'username_not_valid' => '��� ������������ ������ ��������� ������ ��������� ����� � ����� (A-Z, a-z, 0-9) � ������ ������������� (_).',
	'not_good_email' => '��������� e-mail �� �������� ����������',
	'c_admin_title' => '����������� �������� ��������������',
	'c_admin_message' => ''.$lang['userlevels']['1'].' ����� ������ ������ � pivot\'�, ����� �������� ��� ��������� ��������, ��� ����������� � �������� ����� ���������. �� ������� ��� ������ ������� %s '.$lang['userlevels']['1'].'?',
);


//      Config Page      \\      
	$lang['config'] = array (
	'save' => '��������� ���������',

	'sitename' => '�������� �����',
	'defaultlanguage' => '���� ��-���������',
	'siteurl' => 'URL �����',
	'header_fileinfo' => '���� � �����',
	'localpath' => '��������� ����',
	'debug_options' => '��������� debug ������',
	'debug' => 'Debug �����',
	'debug_desc' => '���������� ��������� debug ���������� �� ���, �� ��...',
	'log' => '��������',
	'log_desc' => '������� �������� ��������� ��������',

	'header_uploads' => '�������� ������',
	'upload_path' => '���� ��� �������� ������',
	'upload_accept' => '����������� ���� ������',            
	'upload_extension' => '���������� ��-���������',
	'upload_save_mode' => '������������',
	'make_safe' => '������� ����� ������',
	'c_upload_save_mode' => '����������� ����� � �������� �����',
	'max_filesize' => '������ �����',
	'header_datetime' => '����/�����',
	'timeoffset_unit' => '������� ����������',
	'timeoffset' => '���-��',
	'header_extra' => '��������� ���������',
	'wysiwyg' => '������������ WYSIWYG ��-���������',
	'wysiwyg_desc' => '���������� ������� �� ��-��������� �������� \'��� ���� �� � ������\'. ��� ������������ ����� �������� ���� ����� � ����� ������ ����������',
	'def_text_processing' => 'Default Text Processing', 
	'text_processing' => 'Text Processing',
	'text_processing_desc' => 'Determines the default text processing, when a user is using the non-wysiwyg editor. \'Convert Linebreaks\' does nothing more than change linebreaks to a &lt;br&gt;-tag. <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a> is a powerful, yet easy to learn markup style.',
	'none' => 'None',
	'convert_br' => 'Convert Linebreaks to &lt;br /&gt;',
	'textile' => 'Textile',

	'setup_ping' => '��������� �����',
	'ping_use' => '��������� ����-�������',
	'ping_use_desc' => '���������� ������ �� ������� ���� weblogs.com ������������� ����������� Pivot\'�� ���� �� ������� ����� ������. ������� ���� blogrolling.com ������� �� ���� ������',
	'ping_urls' => '������ ��� ������',
	'ping_urls_desc' => '�� ������ ����������� ��������� ������ ��� ������. �� ���������� http://, ����� ��� ��� �� ����� ��������. ������ ���������� ������ ������ �� ����� ������ �� ���������� �� �������� <b>-</b> . ��������� ������� ��� ����������:<br /><b>rpc.weblogs.com/RPC2</b> (weblogs.com ������, ����� ����������������)<br /><b>pivotlog.net/pinger</b> (pivotlog ������, ���� ��� �� ��������)<br /><b>rcs.datashed.net/RPC2</b> (euro.weblogs.com ������)<br /><b>ping.blo.gs</b> (blo.gs ������)<br />',

	'new_window' => '��������� ������ � ����� ����',
	'emoticons' => '������������ ����������� ������',
	'javascript_email' => '���������� Email ������?',
	'new_window_desc' => '���������� ��������� ������ � ����� ���� ��� � ��� ��.',

	'mod_rewrite' => '������������ Filesmatch',
	'mod_rewrite_desc' => '���� �� ����������� Apache\'s Filesmatch, Pivot ����� ������ ������ ���� www.mysite.com/archive/2003/05/30/nice_weather, ������ www.mysite.com/pivot/entry.php?id=134. �� ��� ������� ������������ ������ ������, ��� ��� ����������� ��� ������ ����� ��������� �������.',
	'search_index' => 'Autoupdate Zoek Index',
	'search_index_desc' => 'Dit bepaalt of de zoekindex altijd wordt bijgewerkt als je een nieuw artikel plaatst, of een artikel wijzigt.',

	'default_introduction' => 'Default Introduction/Body',
	'default_introduction_desc' => 'This will determine the default values for Introduction and Body when an author writes a new entry. Normally this will be an empty paragraph, which makes the most sense semantically.',

	'upload_autothumb'	=> 'Automatic Thumbnails',
	'upload_thumb_width' => 'Thumbnail width',
	'upload_thumb_height' => 'Thumbnail height',
	'upload_thumb_remote' => 'Remote cropping script',
	'upload_thumb_remote_desc' => 'If your server does not have the necessary libraries installed to perform image cropping, you can use a remote cropping script.',


);


//      Weblog Config   \\
$lang['weblog_config'] = array (
	'edit_weblog' => '������ �������',
	'edit_blog' => '������ ������',
	'new_weblog' => '����� ������',
	'new_weblog_desc' => '�������� ����� ������',
	'del_weblog' => '������� ������',
	'del_this_weblog' => '������� ���� ������.',
	'create_new' => '������� ����� ������',
	'subw_heading' => '��� ������� �������� ���������� � �������� �� ������ ������� ����� ������ ��� ��� ������������, � ��� �� ��������� ������� ����� � ��� ������������',
	'create' => '���������',
	
	'create_1' => '�������� / ������ �������, ��� 1 �� 3',
	'create_2' => '�������� / ������ �������, ��� 2 �� 3',
	'create_3' => '�������� / ������ �������, ��� 3 �� 3',

	'name' => '�������� �������',
	'payoff' => '������',
	'payoff_desc' => '������ ����� ���� ����������� ��� ������������ ��� ��� ������� �������� ������ �������',
	'url' => 'URL � �������',
	'url_desc' => 'Pivot ��� ��������� ������ �� ������ ���� �� �������� ��� ���� ������. ���� �� ����������� ���� ������ ��� ����� ������, ��� ��������� �� ������� �������, �� ������ ������������ ��� ���� ��� ��������.',
	'index_name' => '������� �������� (������)',
	'index_name_desc' => '��� ������-�����. ������ ��� ���-�� ����� \'index.html\' ��� \'index.php\'.',

	'ssi_prefix' => '������� SSI',
	'ssi_prefix_desc' => '���� ��� ���� ���������� SSI (��� �� ������������� �� ������ ����������� ������ ���� ����� ��������� ������� ������ Pivot\'� ��� SSI. �������� \'index.shtml?p=\'. �������� ���� ������ ���� �� ������ ��� �������.',

	'front_path' => '���� � ��������� ��������',
	'front_path_desc' => '������������� ��� ���������� ���� � �������� � ������� Pivot ����� ��������� ������� �������� � �����.',
	'file_format' => '��� �����',
	'entry_heading' => '��������� �������',
	'entry_path' => '���� �������',
	'entry_path_desc' => '������������� ��� ���������� ���� � �������� � ������� Pivot ����� ��������� ��������� �������� ��� ������ ������ (���� �� �������� �� ������������ \'����� ������\')',
	'live_comments' => '����� ������',
	'live_comments_desc' => '���� �� ����������� \'����� ������\', Pivot �� ����� ��������� ��������� �������� ��� ������ ������. ��� ����� ��-���������.',
	'readmore' => '����� ��� \'����������� ������\' ������',
	'readmore_desc' => '���� ����� ���������� ��� ������ ������� ����� ��� �������� �� ������� �������� � ���������� ������ ��� ����������� ������. ���� �� �������� ���� ������, ����� ������������ ��������� �� ��������� �����.',
	
	'arc_heading' => '��������� �������',
	'arc_index' => '������� ����',
	'arc_path' => '���� � ������',
	'archive_amount' => '���������� �������',
	'archive_unit' => '��� ������',
	'archive_format' => '������ ������',
	'archive_none' => '�� ������ �������',
	'archive_weekly' => '������������ ������',
	'archive_monthly' => '�������� ������',

	'archive_link' => '�������� ������',
	'archive_linkfile' => '���������� �� �����',
	'archive_order' => 'Archive Order',
	'archive_ascending' => 'Ascending (oldest first)',
	'archive_descending' => 'Descending (newest first)',

	'templates_heading' => '�������',
	'frontpage_template' => '������ ������� ��������',
	'frontpage_template_desc' => '��� ��������� �������� ����� �����.',
	'archivepage_template' => '������ �������� �������',
	'archivepage_template_desc' => '��� ������� � ��������. ����� ���� ����� �� ��� � \'������ ������� ��������\'.',
	'entrypage_template' => '������ ������',
	'entrypage_template_desc' => '��� �������� ������ ��������� ������.',
	'extrapage_template' => 'Extra Template',
	'extrapage_template_desc' => 'The Template that defines what your archive and search.php will look like.',

	'shortentry_template' => '������ �������� ������',
	'shortentry_template_desc' => '��� ������� � �������� ���� � ����� � �������.',
	'num_entries' => '���������� �������',
	'num_entries_desc' => '������� ������� ����� �������� �� ������� ��������.',
	'offset' => 'Offset',
	'offset_desc' => 'If Offset is set to a number, that amount of entries will be skipped when generating the page. You can use this to make a \'Previous entries\' list, for example.',
	'comments' => '��������� �����������?',
	'comments_desc' => '���������� ������ �� ������������ ��������� ����������.',

	'setup_rss_head' => '��������� RSS',
	'rss_use' => '������������ RSS',
	'rss_use_desc' => '���������� ����� �� Pivot\'� ��������� RSS ����� ��� ������� �����.',
	'rss_filename' => '���� � RSS ������',
	'atom_filename' => 'Atom bestandsnaam',
	'rss_path' => '���� � RSS',
	'rss_path_desc' => '������������� ��� ���������� ���� � RSS �����.',
	'rss_size' => '����� ������ � RSS',
	'rss_size_desc' => '����� ������ (���-�� ��������) ������ ����� � RSS �����',

	'lastcomm_head' => 'Settings for Last Comments',
	'lastcomm_amount' => 'Show how many',
	'lastcomm_length' => 'Cut at length',
	'lastcomm_format' => 'Format',
	'lastcomm_format_desc' => 'These settings determine the appearance of the \'last comments\' on the weblog\'s frontpage.',

	'lastref_head' => 'Settings for Last Referers',
	'lastref_amount' => 'Show how many',
	'lastref_length' => 'Cut at length',
	'lastref_format' => 'Format',
	'lastref_format_desc' => 'These settings determine the appearance of the \'last referers\' on the weblog\'s frontpage.',

	'various_head' => '��������� ���������',
	'emoticons' => '������������ ����. ������',
	'emoticons_desc' => '���������� ������� �� ��������� ������ ��������� :-) ���������������� � �� ����������� ����������.',
	'encode_email_addresses' => '���������� Email ������',
	'encode_email_addresses_desc' => '���������� ����� �� email-����� ������������ ��� ������ javascript, ��� ������ �� ����-�����.',
	'target_blank' => '���� �����',
	'xhtml_workaround' => 'XHTML Workaround',
	'target_blank_desc' => '���� ����� � �������� \'��\', �� ��� ������ ����� ����������� � ����� ����.',

	'date_head' => '��������� ����������� ����',
	'full_date' => '������ ������ ����',
	'full_date_desc' => '���������� ������ ������ ���� � �������. � �������� ������������ � �������.',
	'entry_date' => '���� ������',
	'diff_date' => '��������� ����',
	'diff_date_desc' => '\'��������� ����\' ���� ����� ������������ � ������ �  \'����� ������\'. ���� ������ ������������ � ����� ����, � �� ����� ��� ��������� ���� ������������ ������ ���� ���� ������� �� ������� ������.',
	'language' => '����',

	'comment_head' => '��������� ������������',
	'comment_sendmail' => '�������� �� �����?',
	'comment_sendmail_desc' => '����� ���� ��� ��� �������� �����������, Pivot ����� ��������� ������ �������� �����.',
	'comment_emailto' => '����',
	'comment_emailto_desc' => '�����(�) �� ������� ����� ���������� ��������� �� ����������� �����������. ��������� ������� ������� ��������� �������.',
	'comment_texttolinks' => '����� � ������',
	'comment_texttolinks_desc' => '���������, ������� �� ���������� ������ ������� � e-mail\'�� ���������� � ������.',
	'comment_wrap' => '���������� ����������� �����',
	'comment_wrap_desc' => '����� ����� ������ ������ �� ������� ��� ������, ����� ����� ���� ��������� �� ����� ������ ����� ���������� ��������� ������.',
	'comments_text_0' => '������ ��� \'��� ������������\'',
	'comments_text_1' => '������ ��� \'���� �����������\'',
	'comments_text_2' => '������ ��� \'������������ - �\'',
	'comments_text_2_desc' => '��������� �� �����, ������� ����� ����������� ��� ����������� ���������� ����������� ������������. ���� �� �������� ��� ���� �������, Pivot ����� ������������ ������ �� ����� �����',

	'comment_pop' => '���������� � ����� ����?',
	'comment_pop_desc' => '���������, ����� �� ����������� (��� \'��������� ������\') ������������ � ����� ���� ��� � ������������ ���� ��������.',
	'comment_width' => '������ ����',
	'comment_height' => '������ ����',
	'comment_height_desc' => '��������� ������ � ������ (� ��������)���� ��� ������������.',
				
	'comment_format' => "Format of Comments",
	'comment_format_desc' => "This specifies the formatting of comments on the entry pages.",

	'comment_textile' => 'Allow Textile',
	'comment_textile_desc' => 'If this is set to \'Yes\', visitors are allowed to use <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a> in their comments.',

	'saved_create' => '����� ���� ��� ������� ������.',
	'saved_update' => '������ �������.',
	'deleted' => '������ ��� ������.',
	'confirm_delete' => '�� ����������� ������� ������ %1. �� �������?',	

	'blogroll_heading' => 'Blogroll settings',
	'blogroll_id' => 'Blogrolling ID #',
	'blogroll_id_desc' => 'You can optionally include a <a href="http://www.blogrolling.com" target="_blank">blogrolling.com</a> blogroll in your weblog. Blogrolling is an excellent service to maintain a list of links, which shows how recently they were updated. If you do not want this, just skip skip these input field. Otherwise: When you\'re logged in to blogrolling.com, go to \'get code\', there you will find links containing your blogroll\'s ID #. It should look somthing like this: 2ef8b42161020d87223d42ae18191f6d',
	'blogroll_fg' => 'Text Color',
	'blogroll_bg' => 'Background Color',
	'blogroll_line1' => 'Line Color 1',
	'blogroll_line2' => 'Line Color 2',
	'blogroll_c1' => 'Color 1',
	'blogroll_c2' => 'Color 2',
	'blogroll_c3' => 'Color 3',
	'blogroll_c4' => 'Color 4',
	'blogroll_c4_desc' => 'These colors determine what your blogroll will look like. Color 1 to color 4 give a visual indication as to how recently updated a link is.',
);


$lang['upload'] = array (
	//      File Upload      \\
	'preview' => '���� ������',
	'thumbs' => '������������ �������',
	'create_thumb' => '(������ �����)',
	'title' => '�����',
	'thisfile' => '��������� ����� ����:',
	'button' => '���������',
	'filename' => '��� �����',
	'thumbnail' => '�����',
	'date' => '����',
	'filesize' => '������',
	'dimensions' => '������ x ������',
	'delete_title' => '������� �����������',
	'areyousure' => '�� ������� ��� ������ ������� ���� %s?',
	'picheader' => '������� ������ �����������?',
	'create' => '�������',
	'edit' => '������',

	'insert_image' => '�������� �����������',
	'insert_image_desc' => '����� �������� �����������, �� ������ ��� ��������� ��� ������� ����� �����������.',
	'insert_image_popup' => '�������� ������������ �����������',
	'insert_image_popup_desc' => '���� �� ������ ������� ������������ ���� � ������������, ��� ���� ������� ����������� �� �����.',
	'choose_upload' => '���������',
	'choose_select' => '��� �������',
	'imagename' => '�������� �����������',
	'alt_text' => '�������������� �����',
	'align' => '���������',
	'border' => '�����',
	'pixels' => '��������',
	'uploaded_as' => '��� ���� ��� �������� ��� \'%s\'.',
	'not_uploaded' => '��� ���� �� ��� ��������. ��������� ��������� ������:',
	'center' => '����� (�� ���������)',
	'left' => '����',
	'right' => '�����',
	'inline' => '������',
	'notice_upload_first' => '��� ������ �� ������ ������� ��� ��������� �����������',
	'select_image' => '������� �����������',

	'for_popup' => '��� ������ ����',
	'use_thumbnail' => '������������ �����',
	'edit_thumbnail' => '������� �����',
	'use_text' => '������������ �����',
);


$lang['link'] = array (
	//      Link Insertion \\
	'insert_link' => '�������� ������',
	'insert_link_desc' => '�������� ������ ����� ����� � ���� �����. ���������� ������ �������� ��� ��������� ������� �� ������.',
	'url' => 'URL',
	'title' => '��������',
	'text' => '�����',
);


//      Categories      \\
$lang['category'] = array (
	'edit_who' => '��������� ��� ��� ����� ������ � ��������� \'%s\'',
	'name' => '���',
	'users' => '������������',
	'make_new' => '������� ����� ���������',
	'create' => '������� ���������',
	'canpost' => '�������� �������������, ������� �� ������ ��������� ������ � ��� ���������',
	'same_name' => '��������� � ����� ��������� ��� ����������',
	'need_name' => '��������� ������ ���� �������',
	
	'allowed' => '���������',
	'allow' => '���������',
	'denied' => '���������',
	'deny' => '���������',
	'edit' => '������� ���������',
	
	'delete' => '������� ���������',
	'delete_desc' => 'Select \'yes\', if you wish to delete this category',

	'delete_message' => '� ���� ����� Pivot\'� ��������� ����� ������ �������. � ��������� �� ������ �� ������ ��������, ��� ������� � �������� �� ������ ���������.',
);


$lang['entries'] = array (
	//      Entries         \\
	'post_entry' => "��������",
	'preview_entry' => "������������",

	'first' => '������',
	'last' => '���������',
	'next' => '���������',
	'previous' => '����������',
	'jumptopage' => 'jump to page (%num%)',
	'filteron' => 'filter on (%name%)',
	'filteroff' => 'filter off',
	'title' => '���������',
	'subtitle' => '������������',
	'introduction' => '����������',
	'body' => '���� ������',
	'publish_on' => '����� ��� ������',
	'status' => '������',
	'post_status' => '������ ��������',
	'category' => '���������',
	'select_multi_cats' => '(Ctrl-���� ����� �������� ��������� ���������)',
	'last_edited' => "��������� ��� ���������",
	'created_on' => "�������",
	'date' => '����',
	'author' => '�����',
	'code' => '���',
	'comm' => '# Comm',
	'allow_comments' => '��������� ��������������',
	'convert_lb' => '�������������� �������� �����',
	'always_off' => '(������ ��������� ����� � ������ wysiwyg)',
	'be_careful' => '(������ ��������� � ����!)',
	'edit_comments' => '������� �����������',
	'edit_comments_desc' => '������� ����������� ����������� ��� ������ ������',
	'edit_comment' => '������ �����������',
	'delete_comment' => '������� �����������',
	'block_single' => '������������� IP %s',
	'block_range' => '������������� �������� IP %s',
	'unblock_single' => '�������������� IP %s',
	'unblock_range' => '�������������� �������� IP %s',
	'trackback' => 'Trackback ping',
);


//      Form Fun      \\
$lang['forms'] = array (
	'c_all' => '�������� ���',
	'c_none' => '����� ������ �� ����',
	'choose' => '- �������� -',
	'publish' => '���������� ������ � \'������������\'',
	'hold' => '���������� ������ � \'����������\'',
	'delete' => '������� ��',
	'generate' => '������������ � �������������',

	'with_checked_entries' => "� ����������� �������� �������:",
	'with_checked_files' => "� ����������� ������� �������:",
	'with_checked_templates' => '� ����������� ��������� �������:',
);


//      Errors         \\
$lang['error'] = array (
	'path_open' => '�� ���� ������� ����������, ��������� �����.',
	'path_read' => '�� ���� ��������� ����������, ��������� �����.',
	'path_write' => '�� ���� �������� � ����������, ��������� �����.',

	'file_open' => '�� ���� ������� ����, ��������� �����.',
	'file_read' => '�� ���� ��������� ����, ��������� �����.',
	'file_write' => '�� ���� �������� ����, ��������� �����.',
);


//      Notices         \\
$lang['notice'] = array (      
	'comment_saved' => "����������� ��� ��������.",
	'comment_deleted' => "����������� ��� ������.",
	'comment_none' => "� ������ ������ ��� ������������.",
);


// Comments, Karma and voting \\
$lang['karma'] = array (
	'vote' => '������������ \'%val%\' �� ��� ������',
	'good' => '������',
	'bad' => '�����',
	'already' => '�� ��� ���������� �� ��� ������ ��� �����',
	'register' => '��� ����� \'%val%\' ��� �������',
);


$lang['comment'] = array (
	'register' => '��� ����������� ��������.',
	'preview' => '�� ������������������ ���� �����������. ������ ������� ��� ������� \'�������� �����������\' ����� �� ����������.',
	'duplicate' => '��� ����������� �� ��������, �.�. �� �������� ����� ��� �� ��� � ����������',
	'no_name' => '�� ������ �������� ���� ��� (��� ���) � ���� \'���\'. ������ ������� ��� ������� \'�������� �����������\' ����� �� ����������.',
	'no_comment' => '�� ������ ���-������ �������� � ���� \'�����������\'. ������ ������� ��� ������� \'�������� �����������\' ����� �� ����������.',
);


$lang['comments_text'] = array (
	'0' => "��� ������������",
	'1' => "%num% �����������",
	'2' => "������������: %num%",
);


$lang['weblog_text'] = array (
	// these are used in the weblogs, for the labels related to archives
	'archives' => "������",
	'next_archive' => "��������� �����",
	'previous_archive' => "���������� �����",
	'last_comments' => "��������� �����������",
	'last_referrers' => "��������� ���������",
	'calendar' => "���������",
	'links' => "�������",
	'xml_feed' => "RSS ����� (1.0)",
	'powered_by' => "Powered by",
	'name' => "���",
	'email' => "Email",
	'url' => "URL",
	'date' => "����",
	'comment' => "�����������",
	'ip' => "IP �����",
	'yes' => "��",
	'no' => "���",
	'emoticons' => "������",
	'emoticons_reference' => 'Open Emoticons Reference',
	'textile' => 'Textile',
	'textile_reference' => 'Open Textile Reference',
	'post_comment' => "�������� �����������",
	'preview_comment' => "������������",
	'remember_info' => "��������� ������������ ����������?",
	'disclaimer' => "<b>��������:</b> ��� ���� �������� &lt;b&gt; � &lt;i&gt; ����� ������� �� ������ �����������. ����� ������� ������ ������ �������� �����.",
	'search_title' => "Zoek resultaten",
	'search' => "Zoek!",
	'nomatches' => "Niets gevonden voor '%name%'. Probeer iets anders.",
	'matches' => "Resultaten voor '%name%':",
);


$lang['ufield_main'] = array (
	//      Userfields      \\
	'title' => '������ ���������������� �����',
	'edit' => '������',
	'create' => '�������',

	'dispname' => '������������ ���',
	'intname' => '��������� ���',
	'intname_desc' => '��������� ��� ��� ��� �������� ������� �� �������� ����� �� ���������. ��� ����� �������� � ��������.',
	'size' => '������',
	'rows' => '�����',
	'cols' => '�����',
	'maxlen' => '����. �����',
	'minlevel' => '���. ������� ������������',
	'filter' => '�����������',
	'filter_desc' => '�������� ������ ������� �� ������������� ��� �� ��� ����� ���� � ��� ��������',
	'no_filter' => '������',
	'del_title' => '����������� ��������',
	'del_desc' => '������ ������ ���� (<b>%s</b>) �� ����� ���������� ��� ������ ������� ������������ ����� �������.',
	
	'already' => '������ ��� ��� ������������',
	'int' => '��������� ��� ����� ���� ������� ���� ��������',
	'short_disp' => '������������ ��� ������ ���� ������� 3 ��������',
);


$lang['bookmarklets'] = array (
	'bookmarklets' => 'Bookmarklets',
	'bm_add' => 'Add Bookmarklet.',
	'bm_withlink' => 'Piv � New',
	'bm_withlink_desc' => 'This Bookmarklet opens a window with a New Entry, which contains a link to the page it was opened from.',

	'bm_nolink' => 'Piv � New',
	'bm_nolink_desc' => 'This Bookmarklet opens a window with a blank New Entry.',

	'bookmarklets_info' => 'You can use Bookmarklets to quickly write New Entries with Pivot. To add a Bookmarklet to your browser, use one of the following options: (exact text varies, depending on which browser you are using)',
	'bookmarklets_info_1' => 'Click and drag the bookmarklet to your \'Links\'-toolbar or your browsers \'Bookmarks\'-button.',
	'bookmarklets_info_2' => 'Right-click on the bookmarklet and select \'Add to Bookmarks\'.',
);


// A little tool to help you check if the file is correct..
if (count(get_included_files())<2) {

	$groups = count($lang);
	$total = 0;
	foreach ($lang as $langgroup) {
		$total += count($langgroup);
	}
	echo "<h2>Language file is correct!</h2>";
	echo "This file contains $groups groups and a total of $total labels.";

}

?>
