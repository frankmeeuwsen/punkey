<?php
//Latvie&scaron;u (Latvian) (UTF-8)

//Augšējo līniju vajag, lai php zinatu, ka parādīt lietotāja info,
//tai jābut uz otrās līnijas

// Latviešu tulkojums (for version 1.30 beta 3)
// Uldis Kalnins (uldis@velokurjers.lv)
// http://www.pivotlog.net, www.velokurjers.lv

// allow for different encoding for non-western languages
$encoding="UTF-8";
$langname="lv";


//		General		\\
$lang['general'] = array (
	'yes' => 'Jā',	//affirmative
	'no' => 'Nē',		//negative
	'go' => 'Aiziet!',	//proceed

	'minlevel' => 'Lai veiktu to, ko biji iedomājies, tev nav nepieciešamo privilēģiju',	
	'email' => 'Epasts',			
	'url' => 'Saite',
	'further_options' => "Papildus opcijas",
	'basic_view' => "Pamata skatījums",
	'basic_view_desc' => "Parādīt tikai biežāk izmantotos laukus",
	'extended_view' => "Paplašinātais skatījums",
	'extended_view_desc' => "Parādīt labojamos laukus",
	'toggle_view' => "Pārslēgties starp pamata un paplašināto skatījumu",
	'select' => "Izvēlēties",
	'cancel' => "Atcelt",
	'delete' => 'Izdzēst',
	'edit' => 'Labot',
	'welcome' => "Laipni lūdzam %build%.",
	'write' => "Rakstīt",
	'write_open_error' => "Kļūda rakstot. Nevaru atvērt failu rakstīšanai",
	'write_write_error' => "Kļūda rakstot. Nevaru atvērt failu rakstīšanai",
	'done' => "Paveikts!",
	'shortcuts' => "Norādes",
	'cantdelete' => "Tev nav atļauts izdzēst %title%!",
	'cantdothat' => "Tu TO ar %title% neizdarīsi!",
	'cantdeletelast' => "Tu nevari izdzēst visus savus rakstus. Jābūt vismaz 2 rakstiem, lai vienu no tiem varētu izdzēst. Lapa nevar palikt tukša.",
);


$lang['userlevels'] = array (
		'Superlietotājs', 'Administrators', 'Gudrs lietotājs', 'Lietotājs', 'Komentētājs'
		//  Var būt grūti pārtulkot, bet pamatā ir sakārtojums pēc privilēģijām (..)

);


$lang['numbers'] = array (
	'nulle', 'viens', 'divi', 'trīs', 'četri', 'pieci', 'seši', 'septiņi', 'astoņi', 'deviņi', 'desmit', 'vienpadsmit', 'divpadsmit', 'trīspadsmit', 'četrpadsmit', 'piecpadsmit', 'sešpadsmit'
);


$lang['months'] = array (
	'Janvāris', 'Februāris', 'Marts', 'Aprīlis', 'Maijs', 'Jūnijs', 'Jūlijs', 'Augusts', 'Septembris', 'Oktobris', 'Novembris', 'Decembris'
);


$lang['months_abbr'] = array (
	'Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jūn', 'Jūl', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'
);


$lang['days'] = array (
	'Svētdiena', 'Pirmdiena', 'Otrdiena', 'Trešdiena', 'Ceturtdiena', 'Piektdiena', 'Sestdiena'
);


$lang['days_abbr'] = array (
	'Svēt', 'Pirm', 'Otrd', 'Treš', 'Cet', 'Pie', 'Sest'	
);


$lang['days_calendar'] = array (
	'S', 'M', 'T', 'W', 'T', 'F', 'S'
);


$lang['datetime_words'] = array (
	'Gads', 'Mēnesis', 'Nedēļa', 'Diena', 'Stunda', 'Minūte', 'Sekunde'	
);


//		Login Lapa		\\
$lang['login'] = array (
	'title' => 'Ielogoties',
	'name' => 'Lietotājs',
	'pass' => 'Parole',
	'remember' => 'Atcerēties',
	'rchoice' => array (
		'0' => 'Neko',
		'1' => 'Manu lietotajvārdu un paroli',
		'2' => 'Vēlos vienmēr automātiski ielogoties'
	),
	'delete_cookies_desc' => 'Ja Tev ir pārliecība, ka izmanto pareizo lietotājvārdu / paroli, taču joprojām nevari ielogoties <br /> pamēģini nodzēst šī domēna cepumiņus ;) :',
	'delete_cookies' => 'Dzēst cepumiņus',
	'retry' => 'Nepareizs lietotājvārds/parole',
	'banned' => 'No desmit reizēm nevienu reizi tu pareizi paroli neievadīji! Līdz ar to Tava datora IP uz 12 stundam ir izbanota.',

);


//		Galvenais rāmis		\\
	$lang['userbar'] = array (
	'main' => 'Pārskats',
	'entries' => 'Raksti',
	'submit' => 'Pievienot jaunu rakstu',
	'comments' => 'Komentāri',
	'trackbacks' => 'Citsaites(Trackbacks)',
	'modify' => 'Labot rakstus',
	'userinfo' => 'Informācija par mani',
	'u_settings' => 'Mani uzstādījumi',
	'u_marklet' => 'Grāmatzīmes',
	'files' => 'Pārvaldīt failus',
	'upload' => 'Augšupielāde',
	'stats' => 'Statistika',
	'admin' => 'Administrācija',

	'main_title' => 'Pivot globālais apskats',
	'entries_title' => 'Rakstu pārskats',
	'submit_title' => 'Jauns Raksts',
	'comments_title' => 'Labot vai dzēst komentārus',
	'trackbacks_title' => 'Labot vai dzēst citsaites',
	'modify_title' => 'Labot rakstu',
	'userinfo_title' => 'Parādīt manu personisko informāciju',
	'u_settings_title' => 'Labot manu personisko informāciju',
	'u_marklet_title' => 'Pievienot grāmatzīmes',
	'files_title' => 'Pārvaldīt un augšupielādēt failus',
	'upload_title' => 'Augšupielādēt failus',
	'uploaded_success' => 'Fails novietots uz servera!',
	'stats_title' => 'Parādīt log failus un statistiku.',
	'updatetitles_title' => 'Parādīt log failus un statistiku.',
	'admin_title' => 'Administratīvais pārskats',
	'recent_entries' => 'Pēdējie raksti',
	'recent_comments' => 'Pēdējie komentāri',
);


	//		Administrativā sleja		\\
	$lang['adminbar'] = array (
//
	'seeusers' => 'Lietotāji',
	'seeconfig' => 'Konfigurācija',
	'filemappings' => 'Izmantotie faili',
	'templates' => 'Paraugi',
	'maintenance' => 'Apkope',
	'regen' => 'Pārģenerēt visus failus',
	'blogs' => 'Lapa',
	'categories' => 'Kategorijas',
	'verifydb' => 'Parbaudīt DB',
	'buildindex' => 'Pārģenerēt Indeksu',
	'buildsearchindex' => 'Pārģenerēt meklētāja indeksu',
	'buildfrontpage' => 'Pārģenerēt pirmo lapu',
	'sendping' => 'Nosūtīt Pingus',


	'backup' => 'Rezerves kopija',
	'description' => 'Apraksts',
	'conversion' => 'Konvertēt',
	'seeusers_title' => 'Rādīt, labot un dzēst lietotājus',
	'userfields' => 'Informācijas lauki par lietotāju',
	'userfields_title' => 'Rādīt, labot un dzēst lietotāja informācijas laukus',
	'seeconfig_title' => 'Labot konfigurācijas failu',
	'filemappings_title' => 'Parādīt pārskatu par Pivot izmantotajiem failiem',
	'templates_title' => 'Rādīt, labot un dzēst Paraugus',
	'maintenance_title' => 'Veikt Pivota failu pārvaldību',
	'regen_title' => 'Pārģenerēt Pivota radītos failus un arhīvus',
	'blogs_title' => 'Rādīt, dzēst, labot lapas',
	'blogs_edit_title' => 'Labot lapas iestādījumus: ',
	'categories_title' => 'Radīt, labot un dzēst sadaļas',	
	'verifydb_title' => 'Pārbaudīt DB stabilitāti',
	'buildindex_title' => 'Pārģenerēt DB indeksu',
	'buildsearchindex_title' => 'Pārģenerēt meklētāja indeksu',
	'buildfrontpage_title' => 'Pārģenerēt pirmo lapu, pēdējos arhīvus un RSS failus katrai lapai.',
	'backup_title' => 'Radīt rakstu rezerves kopiju',
	'backup_config' => 'Konfigurācijas failu rezerves kopija',
	'backup_config_desc' => 'Spied, lai ielādētu sazipotus konfigurācijas failus',
	'ipblocks' => 'IP aizliegumi',
	'ipblocks_title' => 'Radīt un labot bloķētās IP adreses.',
	'ipblocks_stored' => 'IP adreses saglabātas.',
	'ipblocks_store' => 'Saglabāt IP adreses.',
	'ignoreddomains' => 'Bloķētās frāzes',
	'ignoreddomains_title' => 'Rādīt un labot bloķētās frāzes - cīņai ar refereru spamu.',
	'ignoreddomains_stored' => 'Bloķetās frāzes tika saglabātas.',
	'ignoreddomains_store' => 'Saglabāt bloķetās frāzes',
	'ignoreddomains_asterisk' => 'Ar zvaigznīti ( * ) atzīmētās frāzes tiks izmantotas tikai refereru bloķēšanai. Pārējās nebūs iespējams pielietot ne refereros, ne komentāros, ne citsaitēs (trackbacks).',
	'fileexplore' => 'Failu pārlūks',
	'fileexplore_title' => 'Pārlūkot failus (gan DB, gan teksta)',
	'sendping_title' => 'Nosūtīt signālu jaunumu gaidītājprogrammām!',
	'buildindex_start' => 'Nu ģenerēju indeksu. Tas var aizņemt kādu brīdi, nepārtraukt!',
	'buildsearchindex_start' => 'Ģenerēju meklētāja indeksu. Tas var aizņemt kādu brīdi, nepārtraukt!',
	'buildindex_finished' => 'Indeksu ģenerēju %num% sekundēs',

	'filemappings_desc' => 'Zemāk ir redzams katras Pivota apakšlapas apraksts, arī Pivota radītie faili un paraugi, kurus tas izmanto, lai radītu šos failus. Šī iespēja var palīdzēt atklāt kādas problēmas iemeslu.',

	'debug' => 'Open Debug window',

	'latest_pivot_news' => "Jaunākās ziņas no Pivot izstrādātājiem",
	'remove_setup_header' => "Instalācijas skripts",
	'remove_setup' => "Pivota instalācijas skripts 'pivot-setup.php' joprojām atrodas uz servera. Lūdzu ņem vērā, ka ar šo rīku nelabvēļi var nodarīt skādi tavai lapai. Tamdēļ lūdzu pārsauc vai izdzēs to.",

);


$lang['templates'] = array (
	'rollback' => 'Atjaunot',
	'create_template' => 'Radīt Paraugu',
	'create_template_info' => 'Radīt Pivot paraugu no nulles',
	'no_comment' => 'Komentāru nav',
	'comment' => 'Komentārs*',
	'comment_note' => '(*Piezīme: Komentāri var tikt saglabāti tikai vienu reizi.',
	'create' => 'Radīt paraugu',
	'editing' => 'Labošana',
	'filename' => 'Faila nosaukums',
	'save_changes' => 'Saglabāt izmaiņas!',
	'save_template' => 'Saglabāt Paraugu!',
	'aux_template' => 'Palīgparaugs (auxiliary)',
	'sub_template' => 'Apakšparaugs (sub)',
	'standard_template' => 'Standarta paraugs',
	'feed_template' => 'Barotnes paraugs',
	'css_template' => 'CSS fails',
	'txt_template' => 'Teksta fails',
	'php_template' => 'PHP fails',
);


//		Adminam			\\
// bob notes: Mark made these, i think they should be replaced by the 'adminbar']['xxx_title'] ones
$lang['admin'] = array (
	'seeusers' => 'Radīt, labot un dzēst lietotājus',
	'seeconfig' => 'Labot konfigurācijas failus',
	'templates' => 'Radīt, mainīt un dzēst paraugus',
	'maintenance' => 'Veikt darbības ar  Pivota failiem, piemēram \'Pārģenerēt failus\', \'Testēt DB\', \'Pārģenerēt indeksu\' and \'Rezerves kopēšana\'.',
	'regen' => 'Pārģenerēt visas Pivota lapas',
	'blogs' => 'Radīt, labot un dzēst Pivota lapas',
);


//		Apkope		\\	
$lang['maint'] = array (
	'title' => 'Apkope',	
	'gen_arc_title' => 'Pārģenerēt', /*  */
	'gen_arc_text' => 'Reģenerēt visus Tavus arhīvus', /*  */
	'xml_title' => 'Pārbaudīt DB', /* */
	'xml_text' => 'Pārbaudīt DB', /*  */
	'backup_title' => 'Rezerves kopēšana',
	'backup_text' => 'Sagatavot Pivota svarīgāko failu rezerves kopijas',
);


//		Statistika un refereri		\\
$lang['stats'] = array (
	'show_last' => "Parādīt pēdējos",
	'20ref' => "20 refererus",
	'50ref' => "50 refererus",
	'allref' => "Visus refererus",
	'showunblocked' => "nebloķētos",
	'showall' => "bloķētos un nebloķētos",
	'updateref' => "Parādīt refererus pēc lapām, no kurām tie nāk",
	'hostaddress' => 'Hostu IP adrese', 
	'whichpage' => 'Apmeklēts no lapas:',

	'getting' => 'Saņemu jaunos nosaukumus',
	'awhile' => 'Process var ilgt kādu brīdi, tikai mieru!.',
	'firstpass' => 'Pirmais tests',
	'secondpass' => 'Otrais tests',
	'nowuptodate' => 'Nu tavu refereru norades ir atjaunotas.',
	'finished' => 'Pabeigts',
);


//		Lietotāja info		\\
	$lang['userinfo'] = array (
	'editfields' => 'Labot lietotāja laukus',
	'desc_editfields' => 'Labot lietotāju aprakstošos laukus',
	'username' => 'Lietotājvārds',
	'pass1' => 'Parole',
	'pass2' => 'Parole (vēlreiz)',
	'email' => 'Epasts',
	'nickname' => 'Iesauka',
	'userlevel' => 'Lietotāja līmenis',	
	'userlevel_desc' => 'Admins, Viesis, Zinošs lietotājs, utt.',
	'language' => 'Valoda',	
	'lastlogin' => 'Pēdējā pieslēgšanās',
	'edituser' => 'Labot lietotāju',  //the link to.. well, edit the user (also the title)
	'edituserinfo' => 'Labot lietotāja info',
	'selfreg' => 'Lietotāju pašreģistrācija',
	'newuser' => 'Radīt jaunu lietotāju',
	'desc_newuser' => 'Radīt jaunu lietotāja kontu, rakstīšanai veblogā.',
	'newuser_button' => 'Radīt',
	'edituser_button' => 'Mainīt',
	'pass_too_short' => 'Parolei jabūt >6 simboliem.',
	'pass_equal_name' => 'Parole nevar sakrist ar lietotājvārdu.',
	'pass_dont_match' => 'Paroles nesakrīt',
	'username_in_use' => 'Tāds lietotājs jau ir!',
	'username_too_short' => 'Vārda garums >4 simboliem!',
	'username_not_valid' => 'Atļautie simboli (A-Z, 0-9) un (_)!',
	'not_good_email' => 'Nederīga epasta adrese',	
	'c_admin_title' => 'Apstiprināt Administratora radīšanu',
	'c_admin_message' => '! '.$lang['userlevels']['1'].' ir ar pilnām privilēģijām, var labot visas ziņas, visus komentārus un izmainīt visus iestādījumus. Tu tiešām vēlies, lai %s kļūst '.$lang['userlevels']['1'].'?',
);


//		Config Page		\\
	$lang['config'] = array (
	'save' => 'Saglabāt uzstādījumus',
	'sitename' => 'Lapas nosaukums',
	'defaultlanguage' => 'Valoda pēc noklusējuma',
	'defaultencoding' => 'Lietot saitā kodējumu',
	'defaultencoding_desc' => 'Norāda, kādu encodingu izmantot (piemēram, utf-8 vai iso-8859-13). Ja nesaproti, kas tas ir, labāk liec mierā! Atstājot tukšu, tiks izmantoti valodas failā norādītie uzstādījumi.',
	'defaulttheme' => 'Tēma',
	'selfreg' => 'Aļaut pašreģistrāciju',
	'selfreg_desc' => 'Šeit tu atļauj apmeklētājiem reģistrēties kā standarta lietotājiem un rakstīt rakstus! (Tas nav komentāros reģistrējies lietotājs!!!)',
	'siteurl' => 'Lapas adrese',
	'header_fileinfo' => 'Failu info',
	'localpath' => 'Lokālais ceļš',
	'debug_options' => 'Atkļūdošanas opcijas',
	'debug' => 'Atkļūdošanas režīms',
	'debug_desc' => 'Parādīt dažādu atkļūdošanas info..',
	'log' => 'Logfaili',
	'log_desc' => 'Glabāt dažādu darbību logfailus',
	'unlink' => 'Atsaistīt failus',
	'unlink_desc' => 'Uz dažiem serveriem, kam ir uzstādīta ļoti stingra _safemode_, var būt nepieciešams eksperimentēt ar šo opciju. Uz lielākās daļas serveru šai opcijai nav efekta',
	'chmod' => 'Chmod failus uz',
	'chmod_desc' => 'Dažiem serveriem nepieciešams chmod  komandu izpildīt kādā īpašā veidā.  Biežākās vērtības ir \'0644\' un \'0755\'. Nemaini, ja nesaproti, ko dari! :P ',
	'header_uploads' => 'Failu augšupielādes uzstādījumi',

	'upload_path' => 'Augšupielādes ceļš',	
	'upload_accept' => 'Pieņemamie failu tipi',			
	'upload_extension' => 'Faila galotne, pēc noklusējuma',
	'upload_save_mode' => 'Pārrakstīt',
	'make_safe' => 'Nodzēst faila vārdu',
	'c_upload_save_mode' => 'Pieaugošs faila nosaukums',
	'max_filesize' => 'Maksimālais faila izmērs',
	'header_datetime' => 'Datums/Laiks',
	'timeoffset_unit' => 'Laika starpība GMT/Lokālais',
	'timeoffset' => 'Laikjoslas starpība (vienības)',
	'header_extra' => 'Dažādi uzstādījumi',
	'wysiwyg' => 'Vienmēr lietot WYSIWYG redaktoru',
	'wysiwyg_desc' => 'Lietot/Nelietot',
	'basic_view' => 'Lietot pamata skatījumu',
	'basic_view_desc' => 'Nosaka, vai  \'Jauns raksts\' atvērsies ar pamata vai paplašinātām iespējām.',
	'def_text_processing' => 'Teksta apstrāde, pēc noklusējuma', 
	'text_processing' => 'Teksta apstrāde',
	'text_processing_desc' => 'Norāda noklusēto teksta apstrādi, ja lietotājs neizmanto iebūvēto wysiwyg redaktoru. \'Pārveidot Linebreaks\' veic tikai _linebreaks_ pārveidi par &lt;br&gt; tagu. <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a> varens, vienkāršs teksta izcelšanas rīks, viegli apgūstams.',
	'none' => 'Neko',
	'convert_br' => 'Pārveidot "enter" par &lt;br /&gt;',
	'textile' => 'Lietot textile',
	'markdown' => 'Lietot Markdown',
	'markdown_smartypants' => 'Lietot Markdown un Smartypants',

	'allowed_cats' => 'Atļautās sadaļas',
	'allowed_cats_desc' => 'Šis lietotājs var iesūtīt ziņas šādās sadaļās',
	'delete_user' => "Dzēst lietotāju",
	'delete_user_desc' => "Ja vēlies, vari dzēst šo lietotāju. Visi viņa raksti paliks, taču viņš vairs nevarēs ielogoties.",
	'delete_user_confirm' => 'Tu vēlies likvidēt %s pieeju. Tiešām?',

	'setup_ping' => 'Pinga Uzstādījumi',
	'ping_use' => 'Pingot jaunumu gaidītājprogrammas',
	'ping_use_desc' => 'Piemēram, weblogs.com.',	
	'ping_urls' => 'URLi pingošanai',
	'ping_urls_desc' => 'Var būt vairāki url, BEZ http daļas!. Atdala ar komatu!',

	'setup_tb' => 'Ciešsaišu uzstādījumi',
	'tb_email' => 'Epasts',
	'tb_email_desc' => 'Ja jā - tiklīdz tiks pievienota kāda ciešsaite, uz norādīto epasta adresi tiks nosūtīts paziņojums.',

	'new_window' => 'Linkus vērt jaunā logā',
	'emoticons' => 'Lietot ģīmīšus',
	'javascript_email' => 'Kodēt epasta adreses?',	
	'new_window_desc' => 'Norāda, vai visi rakstos esošie linki vērsies jaunā logā.',

	'mod_rewrite' => 'Lietot Filesmatch',
	'mod_rewrite_desc' => 'Ja izmanto Apache Filesmatch opciju, lapas www.mysite.com/pivot/entry.php?id=134, pārveidojas par  www.mysite.com/archive/2003/05/30/nice_weather. Ne visi serveri atbalsta šo iespēju.',
	'mod_rewrite_1' => 'Jā, kā /archive/2005/04/28/virsraksts',
	'mod_rewrite_2' => 'Jā, kā /archive/2005-04-28/virsraksts',
	'mod_rewrite_3' => 'Jā, kā /entry/1234',
	'mod_rewrite_4' => 'Jā, kā /entry/1234/virsraksts',

	'search_index' => 'Automātiski atjaunināt meklētāja indeksu',
	'search_index_desc' => 'Norāda, vai meklētājs papildināsies ar jauniem vārdiem pēc katra raksta pievienošanas.',

	'default_allow_comments' => 'Atļaut komentēt rakstus pēc noklusējuma',
	'default_allow_comments_desc' => 'Nosaka, vai raksti drīkst - nedrīkst tikt komentēti.',	

	'maxhrefs' => 'Linku skaits',
	'maxhrefs_desc' => 'Maksimālais linku skaits komentārā. Ja neierobežots - rakstām 0. Nost ar spamu komentāros!',
	'rebuild_threshold' => 'Pārģenerēšanas izpildes ilgums',
	'rebuild_threshold_desc' => 'Tā kā php skriptiem bieži ir ierobežots izpildes ilgums (parasti līdz 30s), šeit, ja tavam serverim šis laiks ir cits - vari nomainīt. Jāmaina, piemēram, tad ja tev ir daudz rakstu, bet, spiežot pārģenerēt, tiek apstrādāta tikai daļa ierakstu.',

	'default_introduction' => 'Noklusētais Ievads - Raksta turpinājums',
	'default_introduction_desc' => 'Šeit var noteikt noklusētās vērtības priekš Ievada un Raksta turpinājuma, kad autors sāk jaunu rakstu. Parasti - tukšs paragrāfs, pārskatāmībai.',

	'upload_autothumb'	=> 'Automātiski veidot sīkattēlus ',
	'upload_thumb_width' => 'Sīkattēla platums',
	'upload_thumb_height' => 'Sīkattēla augstums',
	'upload_thumb_remote' => 'Attālināts apgraizīšanas skripts',
	'upload_thumb_remote_desc' => 'Izmanto, ja serverim nav nepieciešamo bibliotēku apgraizīšanai, norādām savas.',

	'extensions_header' => 'Paplašinājumu direktorija',
	'extensions_desc'   => '- \'Paplašinājumu\' direktorija ir vieta, kur noglabāt tavus papildinājumus Pivotam.
		Nepieciešama, lai būtu drošāk pāriet uz jaunāku versiju! RTFM plašākai informācijai.',
	'extensions_path'   => 'Ceļš uz paplašinājumu direktoriju',

);


//		Webloga Konfigurācija	\\
$lang['weblog_config'] = array (
	'edit_weblog' => 'Labot lapu',
	'edit_blog' => 'Labot lapas',
	'new_weblog' => 'Jauna lapa', 
	'new_weblog_desc' => 'Pievienot jaunu lapu',
	'del_weblog' => 'Dzēst lapu',
	'del_this_weblog' => 'Dzēst šo lapu.',
	'create_new' => 'Izveidot jaunu lapu',
	'subw_heading' => 'Katrai lapai iespējams norādīt izmantojamos Paraugus, kā arī kategorijas, kurās publicēt.',
	'create' => 'Pabeigt',

	'create_1' => 'Radīt / Labot lapu, solis 1 no 3',
	'create_2' => 'Radīt / Labot lapu, solis 2 no 3',
	'create_3' => 'Radīt / Labot lapu, solis 3 no 3',

	'name' => 'Apakšlapas Nosaukums',
	'payoff' => 'Apaksvirsraksts, apraksts',
	'payoff_desc' => 'Šo var izmantot kā lapas īsu aprakstu, apakšvirsrakstu',
	'url' => 'Apakšlapas URL adrese',
	'url_desc' => 'Pivots pats noteiks URL, ja šis lauks netiks aizpildīts. Ja Tava lapa sastāv no frameset (vairākām lapām), vari izmatot šo uzstādījumu.',
	'index_name' => 'Pirmā lapa  (Index)',
	'index_name_desc' => 'Indeksfaila nosaukums. Parasti kaut kas līdzīgs \'index.html\' vai \'index.php\'.',

	'ssi_prefix' => 'SSI uzstādījumi',
	'ssi_prefix_desc' => 'Ja tavs saits lieto SSI (kas gan nav ieteicams), tu vari noteikt Pivotam izmantojamo failu prefiksu - piem. \'index.shtml?p=\'. Ja nezini, ko dari, labāk atstāj šo lauku brīvu!!',

	'front_path' => 'Pirmās lapas ceļš',
	'front_path_desc' => 'Relatīvais vai absolūtais ceļš uz mapīti, kur Pivots novietos saita pirmo lapu.',
	'file_format' => 'Faila nosaukums',
	'entry_heading' => 'Ievades uzstādījumi',
	'entry_path' => 'Ievades ceļš',
	'entry_path_desc' => ' Relatīvais vai absolūtais ceļš uz mapīti, kur Pivots novietos saita atsevišķās ievades lapas (ja izvēlējies neizmantot \'dzīvo ievadi\')',
	'live_comments' => 'Dzīvā ievade',
	'live_comments_desc' => 'Ja izmantosi \'Dzīvo ievadi\', Pivotam nevajadzēs ģenerēt failus priekš katra raksta. Šis ir ieteicamais uzstādījums.',
	'readmore' => '\'Lasīt tālāk\' Teksts',
	'readmore_desc' => 'Šeit redzamais teksts norāda, ka raksts ir garāks, nekā parādās pirmajā lapā. Ja šis lauks netiks aizpildīts, Pivots izmantos Tavas valodas pamatuzstādījumus',

	'arc_heading' => 'Arhīvu uzstādījumi',
	'arc_index' => 'Indeksu fails',
	'arc_path' => 'Arhīvu ceļš',
	'archive_amount' => 'Arhīvu izmērs',
	'archive_unit' => 'Arhīvu tips',
	'archive_format' => 'Arhīvu formāts',
	'archive_none' => 'Bez arhīviem!',
	'archive_weekly' => 'Arhīvu katru nedēļu!',
	'archive_monthly' => 'Ikmēneša arhīvi',
	'archive_yearly' => 'Ikgadējie arhīvi',

	'archive_link' => 'Arhīva saite',
	'archive_linkfile' => 'Arhīva saites fails',
	'archive_order' => 'Arhīvu secība',
	'archive_ascending' => 'Augoša (vecākie pirmie)',
	'archive_descending' => 'Dilstoša (jaunākie pirmie)',

	'templates_heading' => 'Paraugi',
	'frontpage_template' => 'Pirmās lapas paraugs',
	'frontpage_template_desc' => 'Paraugs, kurā norādīts, kā jāizskatās pirmajai lapai.',
	'archivepage_template' => 'Arhīvlapas paraugs',
	'archivepage_template_desc' => ' Paraugs, kurā norādīts, kā jāizskatās arhīvu lapai.',
	'extrapage_template' => 'Papildlapas paraugs',
	'extrapage_template_desc' => 'Paraugs, kas norāda, kā izskatīsies meklēšanas un arhīvu lapa.',	
	'entrypage_template' => 'Ievades lapas paraugs',
	'entrypage_template_desc' => 'Paraugs, kas nosaka, kā izskatīsies atsevišķi raksti.',	

	'shortentry_template' => 'Virsrakstu paraugs',
	'shortentry_template_desc' => 'Norāda kāds izskats būs rakstu virsrakstiem saitā vai arhīvā.',
	'num_entries' => 'Ierakstu skaits',
	'num_entries_desc' => 'Ierakstu skaits, kas būs redzams pirmajā lapā.',
	'offset' => 'Skaits vienā lapā',
	'offset_desc' => 'Norāda, cik ierakstu atradīsies vienā lapā.',
	'comments' => 'Atļaut komentēt?',
	'comments_desc' => 'Nosaka, vai vienkāršiem lietotājiem tiks dota iespēja komentēt.',

	'publish_cats' => 'Publicēt šīs kategorijas',

	'setup_rss_head' => 'RSS un Atom konfigurācija',
	'rss_use' => 'Lietot RSS',
	'rss_use_desc' => 'Norāda, vai tiks automātiski ģenerēts RSS fails priekš šīs lapas.',
	'rss_filename' => 'RSS faila nosaukums',
	'atom_filename' => 'Atom faila nosaukums',
	'rss_path' => 'ceļš',
	'rss_path_desc' => 'Relatīvais vai absolūtais ceļš, kur glabāsies fails.',
//	'rss_size' => 'Ierakstu garums',
//	'rss_size_desc' => 'Cik raksta simbolus ievietot failā?',
	'rss_full' => 'Taisīt pilnas "barotnes"',
	'rss_full_desc' => 'Nosaka, vai iekš Atom un Rss barotnēm likt pilnus rakstus, vai tikai to sākumus.',
	'rss_link' => 'Saite iekš barotnes',
	'rss_link_desc' => 'Norāda, uz kuru lapu norādīs saite iekš barotnes. Ja nekas netiek norādīts, tiek salinkots ar lapu, no kuras nāk barotne.',
	'rss_img' => 'Attēls iekš barotnes', 
	'rss_img_desc' => 'Vari norādīt ar barotni saistīto attēlu. Atstāj tukšu, vai arī norādi pilnu URL.',

	'lastcomm_head' => 'Jaunāko komentāru uzstādījumi',
	'lastcomm_amount' => 'Cik daudz parādīt',
	'lastcomm_length' => 'Saīsināt garumu',
	'lastcomm_format' => 'Formāts',
	'lastcomm_format_desc' => 'Šie uzstādījumi nosaka, kā izskatīsies jaunākie komentāri pirmajā lapā.',
	'lastcomm_nofollow' => 'Pāradresēt refererus',
	'lastcomm_nofollow_desc' => 'Cīņai ar refererspamu, vari lietot komentāros ievietoto linku pāradresēšanu. Līdz ar to spameris negūs lielāku reitingu Googlē, utml.',

	'lastref_head' => 'Jaunāko refereru uzstādījumi',
	'lastref_amount' => 'Cik parādīt',
	'lastref_length' => 'Saīsināt garumu',
	'lastref_format' => 'Formāts',
	'lastref_format_desc' => ' Šie uzstādījumi nosaka, kā izskatīsies jaunākie refereri pirmajā lapā.',
	'lastref_graphic' => 'Lietot attēlus',
	'lastref_graphic_desc' => 'Norāda, vai aizvietot refereri ar atbilstošu ikonu, ja apmeklētājs nācis no plašāk sastopamajiem meklētājservisiem (Google, Yahoo, u.c.)',
	'lastcomm_redirect' => 'Pāradresēt refererus',
	'lastcomm_redirect_desc' => 'Cīņai ar refererspamu, vari lietot komentāros ievietoto linku pāradresēšanu. Līdz ar to spameris negūs lielāku reitingu Googlē, utml.',

	'various_head' => 'Dažādi uzstādījumi',
	'emoticons' => 'Lietot ģīmīšus :) ',
	'emoticons_desc' => 'Nosaka, vai tekstuālie emociju simboli tiks aizstāti ar grafiskajiem.',
	'encode_email_addresses' => 'Slēpt pasta adreses',
	'encode_email_addresses_desc' => 'Nosaka, vai pasta adreses tiks slēptas no spambotiem.',
	'target_blank' => 'Jaunā lapā',
	'xhtml_workaround' => 'XHTML apkārtceļš',
	'target_blank_desc' => 'Ja \'Jā\', visi linki šai lapā vērsies jaunā logā. Ja \'XHTML apkārtceļš\', visām saitēm būs rel="external" atribūts - XHTML joprojām validēsies.',

	'date_head' => 'Datuma parādīšana',
	'full_date' => 'Pilns datums',
	'full_date_desc' => 'Uzrādīsies pilns datums un laiks.',
	'entry_date' => 'Ieraksta laiks',
	'diff_date' => 'Starplaiks',
	'diff_date_desc' => 'Starplaiks  visbiežāk tiek izmantots saistībā ar  \'Ieraksta laiks\'. Ieraksta laiks tiek parādīts katram ierakstam log failā, taču starplaiks parādās tikai tad, ja vairāki ieraksti atšķiras viens no otra.',
	'language' => 'Valoda',
	'language_desc' => 'Norāda, kādā valodā datumi un skaitļi tiks parādīti, kā arī lapas kodējumu (UTF-8, u.c.).',

	'comment_head' => 'Komentēšanas uzstādījumi',
	'comment_sendmail' => 'Sūtīt epastu?',
	'comment_sendmail_desc' => 'Pēc komentāra ievietošanas komentārs var tikt nosūtīts arī uz emailu raksta autoram!.',
	'comment_emailto' => 'Sūtīt kam',
	'comment_emailto_desc' => 'Norādi, kam sūtīsi, vairākas adreses atdalāmas ar komatu.',
	'comment_texttolinks' => 'Teksts uz saitēm',
	'comment_texttolinks_desc' => 'Nosaka, vai tekstā esošie linki kļūs par aktīvām adresēm.',
	'comment_wrap' => 'Saīsināt komentārus pēc raksta',
	'comment_wrap_desc' => 'Lai nemaīnitos lapas izkārtojums, teksts pēc noteikta simbolu skaita tiks pārnests jaunā rindā.',
	'comments_text_0' => 'Komentāru nav',
	'comments_text_1' => 'Jau pirmais komentārs',
	'comments_text_2' => ' komentāri',
	'comments_text_2_desc' => 'Teksts, kas parādās pie noteikta komentāru skaita',

	'comment_pop' => 'Izlecoša komentāru lapa?',
	'comment_pop_desc' => 'Nosaka, vai komentāru lapa parādīsies izlecoša loga veidā, vai iekš esošās lapas.',
	'comment_width' => 'Izlecošā loga platums',
	'comment_height' => 'Izlecošā loga augstums',
	'comment_height_desc' => 'Nosaka platumu un augstumu izlecošajam komentāru logam.',

	'comment_format' => "Komentāru formāts",
	'comment_format_desc' => "Norāda, kā tiks izkārtoti komentāri.",

	'comment_reply' => "Atbildes uz komentāriem",
	'comment_reply_desc' => "Norāda formatējumu, kurš parādīsies, ja apmeklētāji atbildēs uz komentāru.",
	'comment_forward' => "Formāts 'atbildei uz ..' forward ",
	'comment_forward_desc' => "Norāda teksta formatējumu, ja tiek atbildēts uz kādu citu komentāru.",
	'comment_backward' => "Formāts 'atbildei uz ..' backward",
	'comment_backward_desc' => "Norāda teksta formatējumu, ja tiek atbildēts uz kādu citu komentāru.",

	'comment_textile' => 'Atļaut Textile',
	'comment_textile_desc' => 'Ja uzstādījums ir uz \'Jā\', apmeklētāji varēs izmantot <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a> formatēšanas iespējas savos komentāros.',
	'save_comment' => 'Saglabāt komentāru',
	'comment_gravatardefault' => 'Noklusētais Gravatars (Globally recognised Avatar www.gravatar.com )',
	'comment_gravatardefault_desc' => 'Saite uz noklusēto Gravatar attēlu. Sāc ar http://',
	'comment_gravatarhtml' => 'Gravatar HTML',
	'comment_gravatarhtml_desc' => 'HTML kods Gravatara ievietošanai. %img% tiks aizvietots ar saiti uz attēlu.',
	'comment_gravatarsize' => 'Gravatar izmērs',
	'comment_gravatarsize_desc' => 'Gravatara izmērs pikseļos. Pēc noklusējuma 48.',

        'trackback_head' => 'Ciešsaišu uzstādījumi',
	'trackback_sendmail' => 'Sūtīt epastu?',
	'trackback_sendmail_desc' => 'Tiklīdz tiek pievienota jauna ciešsaite, var tikt izsūtīts epasts lapas īpašniekam.',
	'trackback_emailto' => 'Kam sūtīt',
	'trackback_emailto_desc' => 'Norādi adreses, kurām nosūtīt paziņojumu. Ja vairākas - attdali ar komatu.',
	'trackbacks_text_0' => 'Teksts priekš \'nav ciešsaišu\'',
	'trackbacks_text_1' => 'Teksts priekš \'viena ciešsaite\'',
	'trackbacks_text_2' => 'Teksts priekš \'X ciešsaites\'',
	'trackbacks_text_2_desc' => 'Aizpildi, lai noteiktu, kā parādīt ciešaišu skaitu. Atstājot tukšu - Pivots rādīšanai izmantos tavas valodas uzstādījumus.',
	'trackback_pop' => 'Izlecošais logs ciešsaitēm?',
	'trackback_pop_desc' => 'Nosaka, vai ciešsaites tiks parādītas izlecošā logā vai arī jau atvērtajā pārlūka logā.',
	'trackback_width' => 'Izlecošā loga platums',
	'trackback_height' => 'Izlecošā loga augstums',
	'trackback_height_desc' => 'Norādi augstumu un platumu (pikseļos) ciešsaites izlecošajam logam.',
	'trackback_format' => "Ciešsaišu formāts",
	'trackback_format_desc' => "Šis norāda ciešsaišu formatējumu rakstu lapās.",
	'trackback_link_format' => "Ciešsaites saites formāts",
        'save_trackback' => 'Saglabāt ciešsaiti',

	'saved_create' => 'Jauna lapa ir radīta!.',
	'saved_update' => 'Lapa tika radīta.',
	'deleted' => 'Lapa tika izdzēsta.',
	'confirm_delete' => 'Tu dzēsīsi  %1 lapu. Tiešām?',

	'blogroll_heading' => 'Blogroll uzstādījumi',
	'blogroll_id' => 'Blogroll ID #',
	'blogroll_id_desc' => 'Iespējams iekļaut <a href="http://www.blogrolling.com" target="_blank">blogrolling.com</a> blogroll Tavā veblogā. Blogrolling ir serviss, lai parādītu, cik sen atjauninātas esošās es. Ja esi ielogojies iekš blogrolling.com, dodies uz \'get code\', tur atradīsi savu ID #. Tas var izskatīties šādi: 2ef8b42161020d87223d42ae18191f6d', 
	'blogroll_fg' => 'Teksta Krāsa',
	'blogroll_bg' => 'Fona Krāsa',
	'blogroll_line1' => 'Līnijas krāsa1',
	'blogroll_line2' => 'Līnijas krāsa2',
	'blogroll_c1' => 'Krāsa 1',
	'blogroll_c2' => 'Krāsa 2',
	'blogroll_c3' => 'Krāsa 3',
	'blogroll_c4' => ' Krāsa 4',
	'blogroll_c4_desc' => 'Šis nosaka, kā izskatīsies tavs <i>blogroll/<i>. Krāsa attēlo, cik sen links ievietots.',
);


$lang['upload'] = array (
	//		Failu Augšupielāde		\\
	'preview' => 'Saraksta skats',
	'thumbs' => 'Sīkattēlu skats',
	'create_thumb' => '(Radīt sīkattēlu)',
	'title' => 'Faili',
	'thisfile' => 'Augšupielādēt jaunu:failu',
	'button' => 'Augšupielādēt',
	'filename' => 'Faila nosaukums',
	'thumbnail' => 'Sīkattēls',
	'date' => 'Datums',
	'filesize' => 'Faila izmērs',
	'dimensions' => 'Platums x Augstums',		
	'delete_title' => 'Dzēst Attēlu',
	'areyousure' => 'Tiešām vēlies dzēst %s?',
	'picheader' => 'Dzēst attēlu?',
	'create' => 'radīt',
	'edit' => 'labot',

	'insert_image' => 'Ievietot attēlu',
	'insert_image_desc' => 'Lai ievietotu attēlu, tev jāveic tā augšupielāde, vai arī jāizvēlas no jau esošajiem.',
	'insert_image_popup' => 'Attēla ievietošanas logs',
	'insert_image_popup_desc' => 'Lai radītu izlecošo attēlu, tev jāveic tā augšupielāde, vai arī jāizvēlas no jau esošajiem Tad izvēlies tekstu vai sīkattēlu, kas atvērs izlecošo attēlu.',
	'choose_upload' => 'augšupielādēt',
	'choose_select' => 'vai izvēlēties',
	'imagename' => 'Attēla nosaukums',
	'alt_text' => 'Alternatīvais teksts',
	'align' => 'Pielīdzināšana',
	'border' => 'Robeža',
	'pixels' => 'pikseļi',
	'uploaded_as' => 'Nu tavs fails uz servera atroas kā \'%s\'.',
	'not_uploaded' => 'Tavs fails nekur nenokļuva, jo:',
	'center' => 'Centrēt',
	'left' => 'Pa kreisi',
	'right' => 'Pa labi',
	'inline' => 'Iekļaujot',
	'notice_upload_first' => 'Vispirms jāveic attēla augšupielāde. vai arī tas jāizvēlas no jau esošajiem',
	'select_image' => 'Izvēlēties attēlu',
	'select_file' => 'Izvēlēties failu',

	'for_popup' => 'Priekš izlecošajiem logiem',		
	'use_thumbnail' => 'Lietot sīkattēlus',		
	'edit_thumbnail' => 'Labot sīkattēlus',		
	'use_text' => 'Lietot tekstu',		
	'insert_download' => 'Ievietot lejupielādes linku',
	'insert_download_desc' => 'Lai būtu fails, ko lejupielādēt, vispirms jāveic tā augšupielāde, vai arī jāizvēlas kāds no esošajiem. Tad izvēlies, vai par lejupielādes iespēju norādīs teksts, sīkattēls vai ikona.',
	'use_icon' => 'Lietot ikonu',
);


$lang['link'] = array (
	//		Linku ievietošana \\
	'insert_link' => 'Ievietot linku',
	'insert_link_desc' => 'Ievietot linku, ierakstot ceļu apakšā redzamajā lauciņā. Apmeklētāji redzēs saites aprakstu, ja virs saites novietos peles kursoru.',
	'url' => 'URL',
	'title' => 'Virsraksts',
	'text' => 'Teksts',
);


//		Categories		\\
$lang['category'] = array (
	'edit_who' => 'Labot, kas drīkst komentēt iekš \'%s\'',
	'name' => 'Vārds',
	'users' => 'Lietotāji',
	'make_new' => 'Radīt jaunu kategoriju',
	'create' => 'Radīt kategoriju',
	'canpost' => 'Izvēlies lietotājus, kas drīkstēs rakstīt šai kategorijā.',
	'same_name' => 'Šāda kategorija jau eksistē',
	'need_name' => 'Kategorijai vajag nosaukumu',

	'allowed' => 'Atļauts',
	'allow' => 'Atļaut',
	'denied' => 'Aizliegts',
	'deny' => 'Aizliegt',
	'edit' => 'Labot sadaļu',

	'delete' => 'Dzēst kategoriju',
	'delete_desc' => 'Izvēlies \'Jā\', ja vēlies dzēst kategoriju.',

	'delete_message' => 'Šajā Pivota versijā tikai tiks izdzēsts tikai kategorijas nosaukums. Nākamajās versijās varēs izvēlēties, ko darīt ar kategorijā esošajiem rakstiem..',
	'search_index_newctitle'   => 'Indeksēt šo kategoriju',
	'search_index_newcdesc'    => 'Izvēlies \'Nē\' ja vēlies apmeklētājiem liegt meklēšanas iespēju šai kategorijā. (Tas neglābs no Google!)',
	'search_index_editcheader' => 'Indeksēt kategoriju',

	'order' => 'Šķirošanas virziens, kategorijas svarīgums',
	'order_desc' => 'Kategorijas ar zemāku šķirošanas svarīgumu parādīsies augstāk sarakstā. Ja visas kategorijas būs ar vienādu svarīgumu, tās tiks šķirotas alfabētiski',
	'public' => 'Publiska kategorija',
	'public_desc' => 'Ja uzstādīts \'Nē\', šī kategorija būs apskatāma tikai reģistrētiem apmeklētājiem. (Darbojas tikai iekš live pages)',
	'hidden' => 'Slēpta kategorija',
	'hidden_desc' => 'Ja \'Jā\', šī kategorija neparādīsies arhīvos. (Tikai -live pages- gadījumā)',

);


$lang['entries'] = array (
	//		Raksti			\\
	'post_entry' => "Ievietot rakstu",
	'preview_entry' => "Apskatīt pirms ievietošanas",

	'first' => 'Senākie raksti',
	'last' => 'Jaunākie raksti',
	'next' => 'Nākamie',
	'previous' => 'Iepriekšējie',

	'jumptopage' => 'lekt uz lapu (%num%)',
	'filteron' => 'filtrēt pēc (%name%)',
	'filteroff' => 'Nefiltrēt',
	'title' => 'Virsraksts',
	'subtitle' => 'Apakšvirsraksts',
	'introduction' => 'Ievads',
	'body' => 'Ķermenis',
	'publish_on' => 'Publicēt kategorijā',
	'status' => 'Statuss',
	'post_status' => 'Ievietot un:',
	'category' => 'Kategorija',
	'select_multi_cats' => '(Ctrl-click, lai izvēlētos vairākas kategorijas)',
	'last_edited' => "Pēdējā labošana",
	'created_on' => "Rakstīts",		
	'date' => 'Datums',
	'author' => 'Autors',
	'code' => 'Kods',
	'comm' => '# Komentāri',
	'track' => '# Ciešsaites (Track)',
	'name' => 'Vārds',
	'allow_comments' => 'Atļaut komentēt',

	'delete_entry' => "Dzēst rakstu",
	'delete_entry_desc' => "Dzēst rakstu un tā komentārus ",
	'delete_one_confirm' => "Esi pārliecināts dzēsējs?",
	'delete_multiple_confirm' => "Tiešām gribi dzēst šos rakstus?",

	'convert_lb' => 'Pārveidot Linebeaks',
	'always_off' => '(Šis uzstādījums vienmēr ir izslēgts, kad iekš Wysiwyg režīma)',
	'be_careful' => '(Uzmanīgi ar šo uzstādījumu!)',
	'edit_comments' => 'Labot komentārus',
	'edit_comments_desc' => 'Labot šī raksta komentārus',
	'edit_comment' => 'Labot komentāru',
	'delete_comment' => 'Dzēst komentāru',
	'edit_trackback' => 'Labot Ciešsaiti',
	'delete_trackback' => 'Dzēst Ciešsaiti',
	'block_single' => 'Bloķēt IP %s',
	'block_range' => 'Bloķēt IP areālu %s',
	'unblock_single' => 'Atbloķēt IP %s',
	'unblock_range' => 'Atbloķēt IP areālu %s',
	'trackback' => 'Ciešsaišu pings',
	'trackback_desc' => 'Sūtīt Ciešsaišu Pingus uz sekojošu(ām) URL. Raksti katru savā rindiņā, ja gribi lai sūtās uz vairākām adresēm.',
	'keywords' => 'Atslēgvārdi',
	'keywords_desc' => 'Tiek izmantoti vieglākai raksta atrašanai, vai arī, lai norādītu uz citu, ar rakstu saistītu URL.',
	'vialink' => "Caur linku (URL)",
	'viatitle' => "Izmantojot virsrakstu",
	'via_desc' => 'Lieto šo, lai norādītu ar linku uz raksta avotu.',
	'entry_catnopost' => 'Tev nav atļauts publicēties šajā kategorijā:\'%s\'.',
	'entry_saved_ok' => 'Tavs raksts \'%s\' tika veiksmīgi saglabāts.',
	'entry-ping_sent' => 'Ciešsaites pings tika nosūtīts uz \'%s\'.',
);


//		Form Fun		\\
$lang['forms'] = array (
	'c_all' => 'Izvēlēties visu',
	'c_none' => 'Atcelt izvēles',
	'choose' => '- Izvēlies opciju -',
	'publish' => 'Dot statusu \'Publicēt\'',
	'hold' => 'Dot statusu \'Glabāt\'',
	'delete' => 'Dzēst',
	'generate' => 'Publicēt un ģenerēt',

	'with_checked_entries' => "Veikt ar atzīmētajiem rakstiem:",
	'with_checked_files' => "Veikt ar atzīmētajiem failiem:",
	'with_checked_templates' => ' Veikt ar atzīmētajiem Paraugiem:',
);


//		Kļūdas			\\
$lang['error'] = array (
	'path_open' => 'Nevaru atvērt direktoriju. Vai tev ir vajadzīgās piekļuves tiesības?',
	'path_read' => 'Nevaru nolasīt direktoriju. Vai tev ir vajadzīgās piekļuves tiesības?',
	'path_write' => 'Direktorija nav rakstāma. Vai tev ir vajadzīgās piekļuves tiesības?',

	'file_open' => 'Nevaru atvērt failu. Vai tev ir vajadzīgās piekļuves tiesības?',
	'file_read' => 'Nevaru nolasīt failu. Vai tev ir vajadzīgās piekļuves tiesības?',
	'file_write' => 'Nevaru ierakstīt failu. Vai tev ir vajadzīgās piekļuves tiesības?',
);


//		Brīdinājumi			\\
$lang['notice'] = array (
	'comment_saved' => "Komentārs saglabāts.",
	'comment_deleted' => "Komentārs dzēsts.",
	'comment_none' => "Rakstam komentāru nav.",
	'trackback_saved' => "Ciešaite tika saglabāta.",
	'trackback_deleted' => "Ciešsaite tika dzēsta.",
	'trackback_none' => "Rakstam nav ciešsaišu.",
);


// Comments, Karma and voting \\
$lang['karma'] = array (
	'vote' => 'Balso par \'%val%\' :',
	'good' => 'Labs',
	'bad' => 'Slikts',
	'already' => 'Tu jau nobalsoji',
	'register' => 'Tava balss \'%val%\' saņemta',
);


$lang['comment'] = array (
	'register' => 'Tavs komentārs saglabāts.',
	'preview' => 'Tu skaties, kāds varētu izskatīties tavs komentārs. Noklikšķini uz  \'Saglabāt komentāru\' lai tas saglabātos.',
	'duplicate' => 'Tavs komentārs netika saglabāts, jo ir tāds pats, kā iepriekšējais!',
	'no_name' => 'Ievadi savu vārdu (vai grupu) iekš \'Vārds\'-lauka. Lai saglabātu to paliekoši, klikšķini uz \'Ierakstīt komentāru\' .',
	'no_comment' => 'Komentāra lauciņš neaizpildīts!',
	'too_many_hrefs' => 'Pārāk daudz saišu. Nespamo.',
    'email_subject' => '[Comment] Re:',
);


$lang['comments_text'] = array (
	'0' => "Komentāru nav",
	'1' => "%num% komentārs",
	'2' => "%num%  komentāri",
);

$lang['trackbacks_text'] = array (
	'0' => "Ciešsaišu nav",
	'1' => "%num%(a) ciešsaite",
	'2' => "%num% ciešsaites",
);

$lang['weblog_text'] = array (
	// these are used in the weblogs, for the labels related to archives
	'archives' => "Arhīvi",
	'next_archive' => "Nākamais arhīvs",
	'previous_archive' => "Iepriekšējais arhīvs",
	'last_comments' => "Jaunākie komentāri",
	'last_referrers' => "Jaunākie refereri",
	'calendar' => "Kalendārs",
	'links' => "Saites",
	'xml_feed' => "XML barotne (RSS 1.0)",
	'atom_feed' => "XML barone (Atom Feed)",
	'powered_by' => "Darbina",
	'blog_name' => "Lapas vārds",
	'title' => "Virsraksts",
	'excerpt' => "Excerpt",
	'name' => "Vārds",
	'email' => "Epasts",
	'url' => "URL",
	'date' => "Datums",
	'comment' => "Komentārs",
	'ip' => "IP-adrese",		
	'yes' => "Jā",
	'no' => "Nē",
	'emoticons' => "Emocijas",
	'emoticons_reference' => "Info par Emocijām",
	'textile' => "Textile",
	'textile_reference' => "Info par Textile",
	'post_comment' => "Iesūtīt komentāru",
	'preview_comment' => "Aplūkot komentāru",
	'remember_info' => "Atcerēties personisko informāciju?",
	'notify' => "Paziņot",
	'notify_yes' => "'Jā, sūtīt emailu, ja kāds atbild uz rakstu!",
	'register' => "Reģistrēt tavu lietotājvārdu. / Ielogoties.",
	'disclaimer' => "<b>Informācija:</b> Visi html tagi, izņemot &lt,b&gt, and &lt,i&gt, tiks izdzēsti no komentāriem. Linki no tekstā esošajiem URL veidosies automātiski.",
	'search_title' => "Meklēšanas rezultāti",
	'search' => "Meklēt!",
	'nomatches' => "Neko saistībā ar '%name%' neatradu. Mēģini kaut kā savādāk.",
	'matches' => "Saistībā ar '%name%':",
	'about' => "Par",
	'stuff' => "Lietas",
	'linkdump' => "Saišu apkopojums",
	'discreet' => "Slēpt epastu",
	'discreet_yes' => "Jā, apslēpt manu epasta adresi.",
);


$lang['ufield_main'] = array (
	//		Lietotāja lauki		\\
	'title' => 'Labot lietotāja laikus',
	'edit' => 'Labot',
	'create' => 'Izveidot',

	'dispname' => 'Parādīt vārdu',
	'intname' => 'Iekšējais vārds',
	'intname_desc' => 'Tas ir šī lauka vārds, ko tu definēsi iekš paraugiem. Gluži kā username. HTML formas -input- lauka name= atribūts.',
	'size' => 'Lielums',
	'rows' => 'Rindas',
	'cols' => 'Kolonnas',
	'maxlen' => 'Maksimālais garums',
	'minlevel' => 'Minimālais lietotāja līmenis',	
	'filter' => 'Filtrēt pēc',
	'filter_desc' => 'Tu ierobežo, kāda tipa informācija var tikt laukā ierakstīta (skaitļi, jā-nē, teksts, u.c,).',
	'no_filter' => 'Nekas',
	'del_title' => 'Apstiprināt dzēšanu',
	'del_desc' => 'Dzēšot lauku (<b>%s</b>), tiks dzēsti visi dati, ko lietotāji tajā glabāja.',	

	'already' => 'Šis lietotājvārds jau ir izmantots',
	'int' => 'Iekšējam vārdam jābūt garākam par 3 simboliem',
	'short_disp' => 'Rādāmajam vārdam jābūt garākam par 3 simboliem ',
);


$lang['bookmarklets'] = array (
	'bookmarklets' => 'Grāmatzīmes',
	'bm_add' => 'Pievienot grāmatzīmi.',
	'bm_withlink' => 'Piv » Jauns',
	'bm_withlink_desc' => 'Šī grāmatzīme atver logu  ar jaunu ierakstu, kurā ir links uz lapu, kurā to atvēra.',

	'bm_nolink' => 'Piv » Jauns',
	'bm_nolink_desc' => 'Šī grāmatzīme atver logu ar jaunu, tukšu ierakstu.',

	'bookmarklets_info' => 'Grāmatzīmes izmantojam, lai ātrāk ierakstītu jaunus ieraktus, u. tml. Izvēlies savam interneta pārlūkam atbilstošo variantu!',
	'bookmarklets_info_1' => 'Noklikšķini un aizvelc grāmatzīmi uz  \'Links\'-rīku joslu vai arī izmantojot \'Bookmarks\'-pogu.',
	'bookmarklets_info_2' => 'Labais klikšķis uz grāmatzīmes un \'Add to Bookmarks\'.',
);

// Accessibility - These are used for form fields, labels, fieldsets etc.

$lang['accessibility'] = array(
	'search_idname'      => 'Meklēt',
	'search_formname'    => 'Meklēt',
	'search_fldname'     => 'Meklēt',
	'search_placeholder' => 'Ieraksti vārdu(s), pēc kā meklēt',

	'calendar_summary'   => 'Šī tabula parāda kalendāru ar rakstiem, kā arī saitēm (pa datumiem) uz tiem.',
	'calendar_noscript'  => 'Kalendārs piedāvā iespēju piekļūt šī saita rakstiem',
	/*
	2-letter language code, used to designate the principal language used on the site
	see: http://www.oasis-open.org/cover/iso639a.html
	*/

	'lang' => $langname,
) ;


$lang['snippets_text'] = array (
    'word_plural'     => 'vārdi',
    'image_single'    => 'attēls',
    'image_plural'    => 'attēli',
    'download_single' => 'fails',
    'download_plural' => 'faili',
);

$lang['trackback'] = array (
    'register' => 'Tava ciešsaite tika saglabāta.',
    'duplicate' => 'Tava ciešsaite netika saglabāta - izskatās, ka dublējas ar iepriekšējiem ierakstiem.',
    'too_many_hrefs' => 'Pārāk daudz linku. Beidz spamot.',
    'noid'      => 'Nav ciešsaites ID (tb_id)',
    'nourl'     => 'Nav linka (url)',
    'tracked'   => 'Sasaitēts',
    'email_subject' => '[Trackback] Re:',
);

$lang['commentuser'] = array (
    'title'             => 'Pivota lietotāja pieslēgšanās',
    'header'            => 'Pieslēgties kā reģistrētam lietotājam',
    'logout'            => 'Atslēgties.',
    'loggedout'         => 'Atslēdzies',
    'login'             => 'Pieslēgties',
    'loggedin'          => 'Ir pieslēdzies',
    'loggedinas'        => 'Ir pieslēdzies kā',
    'pass_forgot'       => 'Aizmiri paroli?',
    'register_new'      => 'Reģistrēt jaunu lietotājvārdu.',
    'register'          => 'Reģistrēties kā apmeklētājam',
    'register_info'     => 'Aizpildi nepieciešamo informāciju. <strong>Norādi reālu e-pasta adresi</strong>, citādi nesaņemsi linku reģisrācijas apstiprināšanai.',
    'pass_note'         => 'Uzmanību: Lapas īpašnieks var redzēt tavu paroli, <br /> tamdēļ <em>izvēlies</em> to tādu, <br />kā nnekur citur!',
    'show_email'        => 'Rādīt manu e-pastu komentāros',
    'notify'            => 'Paziņot uz e-pastu par klāt nākušiem rakstiem',
    'def_notify'        => 'Noklusētais atbildēšanas paziņojums',
    'register'          => 'Reģistrēties',
    'pass_invalid'      => 'Nepareiza parole',
    'nouser'            => 'Nav tāda lietotāja',
    'change_info'       => 'Šeit vari mainīt savu informāciju.',
    'pref_edit'         => 'Labot savus uzstādījumus',
    'pref_change'       => 'Mainīt uzstādījumus',
    'options'           => 'Iespējas',
    'user_exists'       => 'Tāds lietotājs jau irs.. Izvēlies citu vārdu.',
    'email_note'        => 'Tev ir jānorāda savs epasts, lai mēs kaut daļēji pārliecinātos par tavas personības īstumu. Protams - varēsi izvēlēties nerādīt epastu citiem.',
    'stored'            => 'Izmaiņas tika saglabātas',
    'verified'          => 'Tavs lietotāja konts ir apstiprināts. Pielēdzies..',
    'not_verified'      => 'Šitas kods nav pareizs - tavs konts nav apstiprināts.',
    'pass_sent'         => 'Tava parole tika nosūtīta uz norādīto epastu..',
    'user_pass_nomatch' => 'Šis epasts un lietotāja vārds nav saistīti.',
    'user_stored'       => 'Lieotājs saglabāts',
    'user_stored_failed' => 'Nevar pievienot jaunu lietotāju!!',
    'pass_send'         => 'Nosūtīt paroli',
    'pass_send_desc'    => 'Ja tu aizmirsi savu paroli, aizpildi lietotājvārda un paroles laukus - Pivots nosūtīs paroli uz norādīto adresi.',
    'oops'              => 'Oops',
    'back'              => 'Atpakaļ uz',
    'back_login'        => 'Atpakaļ uz pieslēgšanos',
    'forgotten_pass_mail' => "Tava aizmirstā Pivot lietotāja '%name%' ir: \n\n%pass%\n\nLūdzu citreiz neaizmirsti!\n\nLai pieslēgos, spied:\n %link%",
    'registered'        => "Tu esi reģistrējies kā lietotājs iekš Pivota!",
    'reg_confirmation'  => 'Reģistrācijas apstiprinājums',
    'reg_verify_short'  => 'Apstiprināt reģistrāciju',
    'reg_verify_long'   => "Lai apstiprinātos, spied:\n %s",
    'reg_verification'  => 'Epasts ar informāciju reģistrācijas apstiprināšanai nosūtīts uz %s. Pēc minūtes būs klāt. :)'
);

// A little tool to help you check if the file is correct..
if (count(get_included_files())<2) {

	$groups = count($lang);
	$total = 0;
	foreach ($lang as $langgroup) {
		$total += count($langgroup);
	}
	echo "<h2>Language file is correct!</h2>";
	echo "This file contains $groups groups and a total of $total labels.";

}

?>
