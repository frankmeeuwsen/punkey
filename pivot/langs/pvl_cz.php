<?php
//�esky (Czech / win)

//the above line is needed so that pivot knows how to display it in the user info.
//it also needs to be on the 2rd line.

// Czech translation of Pivot lang file
// Created by Adam Covex P�ibyl (covex@ahoj.fsik.cvut.cz)
// and Josef Klimosz <ok2wo@atlas.cz>
// http://www.pivotlog.net

// version: 1.3 covex
// version: 1.3.2 ok2wo
// version: 1.3.3 covex update to 1.14beta
// allow for different encoding for non-western languages
$encoding="windows-1250";
$langname="cz";


//		General		\\
$lang['general'] = array (
	'yes' => 'Ano',	//affirmative
	'no' => 'Ne',		//negative
	'go' => 'Prove�!',	//proceed

	'minlevel' => 'Nem�te opr�vn�n� ke vstupu do t�to oblasti Pivotu',	
	'email' => 'Email',			
	'url' => 'URL',
	'further_options' => "Dal�� nastaven�",
	'basic_view' => "Z�kladn� pohled",
	'basic_view_desc' => "Prohl�en� nejobvyklej��ch pol�",
	'extended_view' => "Roz���en� pohled",
	'extended_view_desc' => "Prohl�en� v�ech editovateln�ch pol�",
	'select' => "Vybrat",
	'cancel' => "Zru�it",
	'delete' => 'Smazat',
	'welcome' => "V�tejte v %build%.",
	'write' => "Z�pis",
	'write_open_error' => "Chyba z�pisu. Nemohu otev��t soubor pro z�pis",
	'write_write_error' => "Chyba z�pisu. Nemohu zapisovat do souboru",
	'done' => "Hotovo!",
	'shortcuts' => "Zkratky",
	'cantdelete' => "Nen� v�m dovoleno mazat z�znam %title%!",
	'cantdothat' => "Nen� v�m dovoleno toto prov�d�t se z�znamem %title%!",
);


$lang['userlevels'] = array (
		'Superadmin', 'Administr�tor', 'Pokro�il�', 'B�n�', 'Moblogger'
		//  this one might be a bit hard to translate, but basically it's an order of
		//  power or trust.  Superadmin would be the person in charge - no one can do
		//  anything about his decisions. Admin is only regulated by the Superadmin, 
		//  Advanced by the Admin and Superadmin, etc..
		//  Just get the idea of it.
);


$lang['numbers'] = array (
	'��dn�', 'jeden', 'dva', 't�i', '�ty�i', 'p�t', '�est', 'sedm', 'osm', 'dev�t', 'deset', 'jeden�ct', 'dvan�ct', 't�in�ct', '�trn�ct', 'patn�ct', '�estn�ct'
);


$lang['months'] = array (
	'Leden', '�nor', 'B�ezen', 'Duben', 'Kv�ten', '�erven', '�ervenec', 'Srpen', 'Z���', '��jen', 'Listopad', 'Prosinec'
);	

		
$lang['months_abbr'] = array (
	'Led', '�no', 'B�e', 'Dub', 'Kv�', '�er', '�ec', 'Srp', 'Z��', '��j', 'Lis', 'Pro'
);


$lang['days'] = array (
	'Ned�le', 'Pond�l�', '�ter�', 'St�eda', '�tvrtek', 'P�tek', 'Sobota'
);


$lang['days_abbr'] = array (
	'Ne', 'Po', '�t', 'St', '�t', 'P�', 'So'	
);

$lang['days_calendar'] = array (
        'N', 'P', '�', 'S', '�', '�', 'S'
);


$lang['datetime_words'] = array (
	'Rok', 'M�s�c', 'T�den', 'Den', 'Hodina', 'Minuta', 'Sekunda'	//the actual words for them.
);


//		Login Page		\\
$lang['login'] = array (
	'title' => 'P�ihl�en�',
	'name' => 'U�ivatelsk� jm�no',
	'pass' => 'Heslo',
	'remember' => 'Zapamatovat',
	'rchoice' => array (
		'0' => 'Nic',
		'1' => 'Moje jm�no a heslo',
		'2' => 'Chci z�stat p�ihl�en'
	),
	'delete_cookies_desc' => 'Pokud jste si jist, �e pou��v�te spr�vn� jm�no a heslo, <br />ale st�le m�te probl�my s p�ihl�en�m, zkuste smazat cookies dom�ny weblogu.',
	'delete_cookies' => 'Smazat cookies',
	'retry' => '�patn� jm�no/heslo',
	'banned' => 'Desetkr�t se v�m nepoda�ilo p�ihl�sit. IP adresa, ze kter� byly tyto pokusy vedeny, bude 12 hod. zablokov�na.',

);


//		Main Bar		\\
	$lang['userbar'] = array (
	'main' => 'P�ehled',
	'entries' => 'Z�znamy',
	'submit' => 'Nov� z�znam',
	'comments' => 'Koment��e',
	'modify' => 'Zm�nit z�znamy',
	'userinfo' => 'Moje info',
	'u_settings' => 'Moje nastaven�',
	'u_marklet' => 'Bookmarklety',
	'files' => 'Spr�va m�di�',
	'upload' => 'Upload',
	'stats' => 'Statistiky',
	'admin' => 'Administrace',

	'main_title' => 'Glob�ln� p�ehled Pivotu',
	'entries_title' => 'P�ehled z�znam�',
	'submit_title' => 'Nov� z�znam',
	'comments_title' => 'Editace nebo maz�n� koment��e',		
	'modify_title' => 'Zm�na z�znamu',
	'userinfo_title' => 'Prohl�en� osobn�ch informac�',
	'u_settings_title' => 'Editace osobn�ch nastaven�',
	'u_marklet_title' => 'Vytvo�en� bookmarkletu',
	'files_title' => 'Spr�va a upload m�di�',
	'upload_title' => 'Upload soubor�',
	'uploaded_success' => 'Soubor byl uploadov�n',
	'stats_title' => 'Prohl�en� log� a statistik',
	'updatetitles_title' => 'Prohl�en� log� a statistik',
	'admin_title' => 'P�ehled administrace',
	'recent_entries' => 'Posledn� z�znamy',
	'recent_comments' => 'Posledn� koment��e',
);


$lang['adminbar'] = array (
	//		Admin Bar		\\
	//'trebuild' => 'Rebuild all Files', rolled into maintenance
	'seeusers' => 'U�ivatel�',
	'seeconfig' => 'Konfigurace',
	'filemappings' => 'Mapov�n� soubor�',
	'templates' => '�ablony',
	'maintenance' => '�dr�ba',
	'regen' => 'Sestavit v�echny soubory',
	'blogs' => 'Weblogy',
	'categories' => 'Kategorie',
	'verifydb' => 'Zkontroluj datab�zi',
	'buildindex' => 'Sestavit index',
	'buildsearchindex' => 'Sestavit prohled�vac� index',
	'buildfrontpage' => 'Sestavit �vodn� str�nku(y)',
	'sendping' => 'Poslat pingy',


	'backup' => 'Z�loha',
	'description' => 'Popis',
	'conversion' => 'Konverze',
	'seeusers_title' => 'Vytvo�en�, editace a maz�n� u�ivatel�',
	'userfields' => 'Informace o u�ivateli',
	'userfields_title' => 'Vytvo�en�, editace a maz�n� informac� o u�ivateli',
	'seeconfig_title' => 'Editace konfigura�n�ch soubor�',
	'filemappings_title' => 'P�ehled soubor�, vytvo�en�ch Pivotem pro weblogy na tomto webu',
	'templates_title' => 'Vytvo�en�, editace a maz�n� �ablon',
	'maintenance_title' => 'Proveden� b�n� �dr�by soubor� Pivotu',
	'regen_title' => 'Sestaven� soubor� a archiv� generovan�ch Pivotem',
	'blogs_title' => 'Vytvo�en�, editace a maz�n� weblog�',
	'blogs_edit_title' => 'Editace nastaven� weblogu ',
	'categories_title' => 'Vytvo�en�, editace a maz�n� kategori�',	
	'verifydb_title' => 'Kontrola integrity datab�ze',
	'buildindex_title' => 'Sestaven� indexu datab�ze',
	'buildsearchindex_title' => 'Sestaven� indexu pro prohled�v�n� z�znam�',
	'buildfrontpage_title' => 'Sestaven� �vodn� str�nky, posledn�ho archivu a RSS pro v�echny weblogy.',
	'backup_title' => 'Vytvo�en� z�lohy va�ich z�znam�',
	'ipblocks' => 'Blokovan� IP',
	'ipblocks_title' => 'Prohl�en� a editace blokovan�ch IP adres',
	'ipblocks_stored' => 'IP-adresa byla ulo�ena.',
	'ipblocks_store' => 'Ulo�it tyto IP adresy',
	'fileexplore' => 'Prohl�e� soubor�',
	'fileexplore_title' => 'Prohl�en� soubor� (textov�ch i datab�zov�ch)',
	'sendping_title' => 'Pos�l�n� ping� pro obnoven� stopov�n�.',
	'buildindex_start' => 'Vytv���m index. M��e to chvilku trvat, nep�eru�ujte tento proces.',
	'buildsearchindex_start' => 'Vytv���m prohled�vac� index. M��e to chvilku trvat, nep�eru�ujte tento proces.',
	'buildindex_finished' => 'Dokon�eno! Vytvo�en� indexu trvalo %num% sekund',

	'filemappings_desc' => 'N�e m��ete vid�t p�ehled v�ech weblog� t�to instalace Pivotu spole�n� se soubory a �ablonami, pou�it�mi Pivotem k jejich vytvo�en�. Velmi u�ite�n� pro hled�n� p�esn�ho m�sta vzniku probl�m� p�i vytv��en� soubor�.',

);


$lang['templates'] = array (
	'rollback' => 'Vr�tit zp�t',
	'create_template' => 'Vytvo�it �ablonu',
	'create_template_info' => 'Vytvo�en� �ablony Pivotu od za��tku',
	'no_comment' => 'Bez koment��e',
	'comment' => 'Koment��*',
	'comment_note' => '(*Pozn.: Koment��e mohou b�t ulo�eny pouze p�i <b>prvn�m</b> ulo�en� zm�n nebo p�i vytvo�en�)',
	'create' => 'Vytvo�it �ablonu',
	'editing' => 'Editace',
	'filename' => 'Jm�no souboru',
	'save_changes' => 'Ulo�it zm�ny!',
	'save_template' => 'Ulo�it �ablonu!',		
);


//		Admin			\\
// bob notes: Mark made these, i think they should be replaced by the 'adminbar']['xxx_title'] ones
$lang['admin'] = array (
	'seeusers' => 'Vytvo�en�, editace a maz�n� u�ivatel�',
	'seeconfig' => 'Editace konfigura�n�ch soubor�',
	'templates' => 'Vytvo�en�, editace a maz�n� �ablon',
	'maintenance' => 'Proveden� b�n� �dr�by soubor� Pivotu, jako \'Sestaven� soubor�\', \'Ov��en� datab�ze\', \'Sestaven� indexu\' a \'Z�lohov�n�\'.',
	'regen' => 'Sestavit v�echny str�nky generovan� Pivotem',
	'blogs' => 'Vytvo�en�, editace a maz�n� weblog� publikovan�ch Pivotem',
);


//		Maintenace		\\	
$lang['maint'] = array (
	'title' => '�dr�ba',	
	'gen_arc_title' => 'Generov�n� archivu', /* bob notes: redundant, see 'regen' */
	'gen_arc_text' => 'Regenerace v�ech va�ich archiv�', /* bob notes: redundant, see 'regen' */
	'xml_title' => 'Kontrola XML soubor�', /* bob notes: replace with more general 'Verify DB' */
	'xml_text' => 'Kontrola (a pokud je to nutn� i oprava) integrity XML soubor�', /* bob notes: replace with more general 'Verify DB' */
	'backup_title' => 'Z�lohov�n�',
	'backup_text' => 'Vytvo�en� z�lohy v�ech d�le�it�ch soubor� Pivotu',
);


//		Stats and referers		\\
$lang['stats'] = array (
	'show_last' => "Uka� posledn�ch",
	'20ref' => "20 odkaz�",
	'50ref' => "50 odkaz�",
	'allref' => "v�echny odkazy",
	'updateref' => "Aktualizuj mapov�n� titulk� na odkazy",
	'hostaddress' => 'Hostitelsk� adresy (IP)', 
	'whichpage' => 'Jak� str�nka',

	'getting' => 'Z�skej nov� titulky',
	'awhile' => 'M��e to chv�li trvat, nep�eru�ujte tento proces.',
	'firstpass' => 'Prvn� pr�chod',
	'secondpass' => 'Druh� pr�chod',
	'nowuptodate' => 'Va�e mapov�n� odkaz-titulek je aktualizov�no.',
	'finished' => 'Ukon�eno',
);


//		User Info		\\
	$lang['userinfo'] = array (
	'editfields' => 'Editovat polo�ky u�ivatele',
	'desc_editfields' => 'Editace polo�ek pro popis u�ivatel�',
	'username' => 'U�ivatelsk� jm�no',
	'pass1' => 'Heslo',
	'pass2' => 'Heslo (potvrzen�)',
	'email' => 'Email',
	'userlevel' => '�rove� u�ivatele',	
	'userlevel_desc' => '�rove� u�ivatele ur�uje, jak� akce bude m�t u�ivatel povoleny.',
	'language' => 'Jazyk',	
	'edituser' => 'Editovat u�ivatele',  //the link to.. well, edit the user (also the title)
	'edituserinfo' => 'Editovat informace o u�ivateli',
	'newuser' => 'Vytvo�it nov�ho u�ivatele',
	'desc_newuser' => 'Vytvo�en� nov�ho ��tu v Pivotu, umo��uj�c�ho p�isp�vat do weblogu.',
	'newuser_button' => 'Vytvo�it',
	'edituser_button' => 'Zm�nit',
	'pass_too_short' => 'Heslo mus� b�t minim�ln� 6 znak� dlouh�.',
	'pass_dont_match' => 'Hesla se neshoduj�',
	'username_in_use' => 'U�ivatelsk� jm�no ji� existuje',
	'username_too_short' => 'Jm�no mus� m�t alespo� 3 znaky.',
	'username_not_valid' => 'U�ivatelsk� jm�no m��e obsahovat pouze alfanumerick� znaky (A-Z, 0-9) a podtr��tko (_).',
	'not_good_email' => 'Neplatn� emailov� adresa',	
	'c_admin_title' => 'Potvr�te vytvo�en� aministr�tora',
	'c_admin_message' => 'U�ivatel '.$lang['userlevels']['1'].' m� pln� p�istup k Pivotu, m��e editovat v�echny nov� polo�ky, koment��e a m�nit nastaven�. Jste si jist, �e chcete aby %s byl '.$lang['userlevels']['1'].'?',
);


//		Config Page		\\		
	$lang['config'] = array (
	'save' => 'Ulo�it nastaven�',

	'sitename' => 'Jm�no webu',
	'defaultlanguage' => 'P�edvolen� jazyk',
	'siteurl' => 'URL webu',
	'header_fileinfo' => 'Soubor informac�',
	'localpath' => 'M�stn� cesta',
	'debug_options' => 'Mo�nost debugovat',
	'debug' => 'M�d debugov�n�',
	'debug_desc' => 'Uka� sem tam n�hodn� debugovac� informace..',
	'log' => 'Logovac� soubory',
	'log_desc' => 'Uchov�vej logy r�zn�ch aktivit',

	'unlink' => 'Maz�n� soubor�',   
	'unlink_desc' => 'N�kter� servery, pou��vaj�c� safe_mode, mohou vy�adovat pou�it� tohoto nastaven�. Na v�t�in� server� v�ak nebude pot�eba ��dn� zm�na.',// dal jsem pou�it�
	'chmod' => 'Zm�� pr�va soubor� na',
	'chmod_desc' => 'N�kter� servery vy�aduj� zm�nit pr�va vytv��en�ch soubor� na ur�itou hodnotu. Obvykl� hodnoty jsou \'0644\' a \'0755\'. Nem��te tyto hodnoty pokud si nejste jist, co d�l�te.',
	'header_uploads' => 'Nastaven� pro upload',
	'upload_path' => 'Cesta k uploadovan�m soubor�m',	
	'upload_accept' => 'P�ij�man� typy',			
	'upload_extension' => 'P�edvolen� p��pona',
	'upload_save_mode' => 'P�episovat',
	'make_safe' => 'Vy�istit jm�no souboru',
	'c_upload_save_mode' => '��slov�n� soubor�',
	'max_filesize' => 'Maxim�ln� velikost souboru',
	'header_datetime' => 'Datum/�as',
	'timeoffset_unit' => 'Jednotka �asov�ho posunu',
	'timeoffset' => '�asov� posun',
	'header_extra' => 'Dal�� nastaven�',
	'wysiwyg' => 'Pou��vat WYSIWYG editor',
	'wysiwyg_desc' => 'Ur�uje, zda je p�edvolen� editor WYSIWYG. Jednotliv� u�ivatel� si toto nastaven� mohou zm�nit v nastaven� \'Moje informace\'.',
	'def_text_processing' => 'P�edvolen� zp�sob zpracov�n� textu', 
	'text_processing' => 'Zpracov�n� textu',
	'text_processing_desc' => 'Ur�uje zp�sob zpracov�n� textu p�i nepou�it� WYSIWYG editoru. \'Konvertovat konce ��dk�\' ned�l� nic jin�ho, ne� �e m�n� konce ��dk� na  &lt;br&gt;-tag. <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a> je kvalitn�, ale snadno ovladateln� zna�kovac� styl.',
	'none' => 'Nic',
	'convert_br' => 'Konvertuj konce ��dek na &lt;br /&gt;',
	'textile' => 'Textile',

	'setup_ping' => 'Nastaven� pingu',
	'ping_use' => 'Aktualizace stopov�n� pingu',
	'ping_use_desc' => 'Ur�uje, zda stopova�e jako weblogs.com budou automaticky informov�ny Pivotem, pokud p�id�te nov� p��sp�vek. Slu�by jako blogrolling.com jsou na tom p��mo z�visl�.',
	'ping_urls' => 'URL kam pingat',
	'ping_urls_desc' => 'Zde m��ete zadat n�kolik adres, na kter� se bude pos�lat ping. Nevkl�dejte �et�zec http://, jinak to nebude fungovat. Pouze vlo�te adresu - na ka�d� ��dek jednu, nebo je odd�lte znakem roury (|). N�kter� obl�ben� servery:<br /><b>rpc.weblogs.com/RPC2</b> (weblogs.com ping, jeden z nejpou��van�j��ch)<br /><b>pivotlog.net/pinger</b> (pivotlog ping, je�t� nefunk�n�)<br /><b>rcs.datashed.net/RPC2</b> (euro.weblogs.com ping)<br /><b>ping.blo.gs</b> (blo.gs pinger)<br />',

	'new_window' => 'Otev�rat linky v nov�ch oknech',
	'emoticons' => 'Pou��vat emotikony', 
	'javascript_email' => 'K�dovat emailov� adresy?',	
	'new_window_desc' => 'Ur�uje, zda v�echny linky v z�znamech budou otev�rat nov� okno prohl�e�e.',

	'mod_rewrite' => 'Porovn�vej soubory',
	'mod_rewrite_desc' => 'Pokud pou��v�te porovn�v�n� soubor� z Apache, Pivot vytvo�� URL jako www.mysite.com/archive/2003/05/30/nice_weather, m�sto www.mysite.com/pivot/entry.php?id=134. Ne v�echny servery to podporuj�. Pod�vejte se do manu�lu.',

	'search_index' => 'Automaticky aktualizovat prohled�vac� index',
	'search_index_desc' => 'Ur�uje, zda prohled�vac� index bude aktualizov�n poka�d�, kdy� vlo��te nov� z�znam nebo zm�n�te existuj�c�.',

        'default_allow_comments' => 'V�dy povolit koment��e',
        'default_allow_comments_desc' => 'Ur�uje zda z�znamy maj� povoleno vkl�dat koment��e.',

	'default_introduction' => 'P�edvolen� �vod/t�lo',
	'default_introduction_desc' => 'Ur�uje p�edvolen� obsah �vodu a t�la z�znamu. Norm�ln� je to pr�zdn� odstavec.',

	'upload_autothumb'	=> 'Automaticky vytv��et n�hledy',
	'upload_thumb_width' => '���ka n�hledu',
	'upload_thumb_height' => 'V��ka n�hledu',
	'upload_thumb_remote' => 'Extern� o�ez�vac� skript', 
	'upload_thumb_remote_desc' => 'Pokud v� server nem� pot�ebn� knihovny pro o�ez�v�n� obr�zk�, m��ete pou��t extern� skript.',


);


//		Weblog Config	\\
$lang['weblog_config'] = array (
	'edit_weblog' => 'Editovat weblog',
	'edit_blog' => 'Editovat blog',
	'new_weblog' => 'Nov� weblog',
	'new_weblog_desc' => 'P�idat nov� weblog',
	'del_weblog' => 'Smazat weblog',
	'del_this_weblog' => 'Smazat tento weblog.',
	'create_new' => 'Vytvo�it nov� weblog',
	'subw_heading' => 'Pro ka�d� zalo�en� subweblog m��ete nastavit pou�itou �ablonu stejn� jako kategorii, ve kter� m� b�t publikov�n',
	'create' => 'Dokon�it',
	
	'create_1' => 'Vytvo�en� / editace weblogu, krok 1 z 3',
	'create_2' => 'Vytvo�en� / editace weblogu, krok 2 z 3',
	'create_3' => 'Vytvo�en� / editace weblogu, krok 3 z 3',

	'name' => 'Jm�no weblogu',
	'payoff' => 'P��nos',
	'payoff_desc' => 'P��nos m��e b�t pou�it jako podtitulek nebo kr�tk� popis weblogu',
	'url' => 'URL weblogu',
	'url_desc' => 'Pokud nech�te pol��ko pr�zdn�, Pivot s�m ur�� URL weblogu. Pokud pou��v�te weblog jako sou��st r�mc�, nebo jako serverside include, pou�ijte toto nastaven�.',
	'index_name' => '�vodn� str�nka (Index)',
	'index_name_desc' => 'Jm�no index souboru. Obvykle je to n�co jako \'index.html\' nebo \'index.php\'.',

	'ssi_prefix' => 'Prefix SSI',
	'ssi_prefix_desc' => 'Pokud v� webolog pou��v� SSI (co� nen� doporu�eno), pou�ijte nastaven� prefixu jmen soubor� Pivotu pro SSI. Nap�. \'index.shtml?p=\'. Pokud nev�te k �emu nastaven� je, nechte ho pr�zdn�.',

	'front_path' => 'Cesta k �vodn� str�nce',
	'front_path_desc' => 'Relativn� nebo absolutn� cesta, kde Pivot vytvo�� �vodn� str�nku weblogu.',
	'file_format' => 'Jm�no souboru',
	'entry_heading' => 'Nastaven� z�znam�',
	'entry_path' => 'Cesta k z�znam�m',
	'entry_path_desc' => 'Relativn� nebo absolutn� cesta, kde Pivot vytvo�� z�znam str�nky, pokud nepou�ijete  \'�iv� vstupy\'.',
	'live_comments' => '�iv� vstupy',
	'live_comments_desc' => 'Pokud pou��v�te \'�iv� vstupy\', Pivot nebude vytv��et soubory pro jednotliv� z�znamy. Preferovan� nastaven�.',
	'readmore' => '\'V�ce...\'',
	'readmore_desc' => 'Text pou�it� k upozorn�n� na pokra�ov�n� textu z�znamu. Pokud nech�te nastaven� nevypln�n�, Pivot pou�ije nastaven� z p��slu�n�ho jazyka.',
	
	'arc_heading' => 'Nastaven� archivu',
	'arc_index' => 'Soubor indexu',
	'arc_path' => 'Cesta k archiv�m',
	'archive_amount' => 'Po�et archiv�',
	'archive_unit' => 'Typ archiv�',
	'archive_format' => 'Form�t archivu',
	'archive_none' => '��dn� archivy',
	'archive_weekly' => 'T�denn� archivy',
	'archive_monthly' => 'M�s��n� archivy',

	'archive_link' => 'Odkaz na archiv',
	'archive_linkfile' => 'Form�t seznamu archiv�',	
	'archive_order' => 'Po�ad� archiv�',
	'archive_ascending' => 'Vzestupn� (nejstar�� prvn�)',
	'archive_descending' => 'Sestupn� (nejnov�j�� prvn�)',

	'templates_heading' => '�ablony',
	'frontpage_template' => '�ablony �vodn� str�nky',
	'frontpage_template_desc' => '�ablona ur�uj�c� vzhled �vodn� str�nky weblogu.',
	'archivepage_template' => '�ablona str�nky s archivy',
	'archivepage_template_desc' => '�ablona ur�uj�c� vzhled archiv�. M��e b�t stejn� jako \'�ablona �vodn� str�nky\'.',	
	'entrypage_template' => '�ablona z�znamu',
	'entrypage_template_desc' => '�ablona ur�uj�c� vzhled jednotliv�ch z�znam�.',	
	'extrapage_template' => 'Dal�� �ablony',
	'extrapage_template_desc' => '�ablona ur�uj�c� vzhled str�nky pro prohled�v�n� archivu.',

	'shortentry_template' => '�ablona zkr�cen�ho z�znamu',
	'shortentry_template_desc' => '�ablona ur�uj�c� vzhled zkr�cen�ho z�znamu pro weblog a archiv.',	
	'num_entries' => 'Po�et z�znam�',
	'num_entries_desc' => 'Po�et z�znam� subweblogu na �vodn� str�nce.',	
	'offset' => 'Posun',
	'offset_desc' => 'Posun ur�uje po�et z�znam�, kter� budou p�esko�eny p�i generov�n� str�nky. Vyu�it� nap�. pro vytvo�en� seznamu \'P�edchoz� z�znamy\'.',
	'comments' => 'Povolit koment��e?',
	'comments_desc' => 'Ur�uje, zda u�ivatel� budou m�t mo�nost p�ipojovat koment��e k z�znam�m subweblogu',	

	'setup_rss_head' => 'Konfigurace RSS a Atom�',
	'rss_use' => 'Vytv��et kan�ly',
	'rss_use_desc' => 'Ur�uje, zda bude Pivot automaticky generovat kan�ly RSS a Atom pro tento weblog.',
	'rss_filename' => 'Soubor pro RSS',
	'atom_filename' => 'Soubor pro Atom',
	'rss_path' => 'Cesta k RSS kan�lu',
	'rss_path_desc' => 'Relativn� nebo absolutn� cesta k adres��i, kde Pivot vytvo�� soubor pro RSS kan�l.',
//	'rss_size' => 'D�lka z�znamu pro RSS kan�l',	
//	'rss_size_desc' => 'D�lka (po�et znak�) ka�d�ho z�znamu pro RSS kan�l',	
        'rss_full' => 'Vyt��et kompletn� Feeds',
        'rss_full_desc' => 'Ur�uje zda bude Pivot vyt��et kompletn� Atom a RSS feeds. Pokud je nastaveno \'ne\' bude Pivot vytv��et feeds kter� obsahuj� pouze zkr�cen� popis.',

	'lastcomm_head' => 'Nastaven� posledn�ch koment���',
	'lastcomm_amount' => 'Po�et',
	'lastcomm_length' => 'D�lka',
	'lastcomm_format' => 'Form�t',
	'lastcomm_format_desc' => 'Tato nastaven� ur�uj� vzhled seznamu \'Posledn�ch koment���\' na �vodn� str�nce weblogu.',
	'lastcomm_redirect' => 'P�esm�rov�vat odkazy',
	'lastcomm_redirect_desc' => 'Pro ��inn�j�� boj se spamem m��ete odkazy p�esm�rov�vat. To zt�� spamer�m zvy�ov�n� ohodnocen� v Googlu.',

	'lastref_head' => 'Nastaven� posledn�ch odkaz�',
	'lastref_amount' => 'Po�et',
	'lastref_length' => 'D�lka',
	'lastref_format' => 'Form�t',
	'lastref_format_desc' => 'Tato nastaven� ur�uj� vzhled seznamu \'Posledn�ch odkaz�\' na �vodn� str�nce weblogu.',
	'lastref_graphic' => 'Pou��vat grafiku',
	'lastref_graphic_desc' => 'Ur�uje, zda posledn� odkazy budou zv�razn�ny malou ikonkou podle vyhled�va��, p�es kter� n�v�t�vn�ci p�ich�zeji.',
	'lastref_redirect' => 'P�esm�rov�vat odkazy',
	'lastref_redirect_desc' => 'Pro ��inn�j�� boj se spamem m��ete odkazy p�esm�rov�vat. To zt�� spamer�m zvy�ov�n� ohodnocen� v Googlu.',

	'various_head' => 'Dal�� nastaven�',
	'emoticons' => 'Pou��vat emotikony',
	'emoticons_desc' => 'Ur�uje, zda emotikony jako :-) budou p�etransformov�ny na grafick� ekvivalenty.',
	'encode_email_addresses' => 'K�dovat emailov� adresy',
	'encode_email_addresses_desc' => 'Ur�uje, zda emailov� adresy budou k�dov�ny pomoc� javascriptu, aby byly chr�n�ny p�ed spamem.',
	'target_blank' => 'Otev��t v nov�m okn�',
	'xhtml_workaround' => 'XHTML k�dov�n�',  
	'target_blank_desc' => 'Pokud zvol�te \'Ano\', v�echny odkazy vlo�en� do z�znam� se budou otv�rat v nov�m okn� prohl�e�e. Pokud zvol�te \'XHTML k�dov�n�\', budou m�t odkazy p��znak rel="external", co� neporu�� spr�vn� XHTML k�d.',

	'date_head' => 'Nastaven� zobrazen� datumu',
	'full_date' => 'Form�t pln�ho datumu',
	'full_date_desc' => 'Ur�uje form�t pln�ho datumu a �asu. Nej�ast�ji je pou�it na za��tku str�nky s jedn�m z�znamem',
	'entry_date' => '�as z�znamu', 
	'diff_date' => 'Datum zm�ny',
	'diff_date_desc' => '\'Datum zm�ny\' souvis� s \'�asem z�znamu\'. Zat�mco �as je zobrazen u ka�d�ho z�znamu, datum je zobrazeno jen pokud se li�� od p�edchoz�ho.',
	'language' => 'Jazyk',
	'language_desc' => 'Ur�uje, v jak�m jazyce se budou zobrazovat datumy a jak� bude k�dov�n� str�nky (nap�. iso-8859-1, koi8-r atd.).',	

	'comment_head' => 'Nastaven� koment���',
	'comment_sendmail' => 'Pos�lat maily?',
	'comment_sendmail_desc' => 'Pot�, co byl vlo�en koment��, m��e b�t spr�vci weblogu zasl�n mail.',
	'comment_emailto' => 'Poslat na',
	'comment_emailto_desc' => 'Vepi�te adresy, na kter� bude mail odesl�n. V�ce adres odd�lte ��rkou.',
	'comment_texttolinks' => 'Konvertovat text na odkazy',
	'comment_texttolinks_desc' => 'Ur�uje, zda budou URL vlo�en� do textu konvertov�na na klikac� odkazy.',
	'comment_wrap' => 'Zalamovat koment��e po',
	'comment_wrap_desc' => 'Abyste se vyhnuli dlouh�m ��dk�m v koment���ch, kter� by mohly po�kodit vzhled str�nky, ur�ete po�et znak� pro zalomen�.',
	'comments_text_0' => 'ozna�en� pro \'��dn� koment��\'��dn� koment��',  
	'comments_text_1' => 'ozna�en� pro \'jeden koment��\'Jeden koment��',  
	'comments_text_2' => 'ozna�en� pro \'X koment���\'Koment���: ',      
	'comments_text_2_desc' => 'Text, kter� bude indikovat po�et koment��� k z�znamu. Pokud nech�te voln�, Pivot pou�ije nastaven� podle jazyka.',

	'comment_pop' => 'Koment��e v popup okn�?',
	'comment_pop_desc' => 'Ur�uje, zda se koment��e budou zobrazovat v p�vodn�m okn� prohl�e�e nebo v popup (vyskakovac�m) okn�.',
	'comment_width' => '���ka okna',
	'comment_height' => 'V��ka okna',
	'comment_height_desc' => 'Ur�en� rozm�r� popup okna.',
			
	'comment_format' => "Form�t koment���",
	'comment_format_desc' => "Specifikuje zp�sob form�tov�n� koment��� k z�znamu.",

	'comment_textile' => 'Povolit Textile',
	'comment_textile_desc' => 'Pokud nastav�te \'Ano\' , n�v�t�vn�ci mohou v koment���ch pou��vat <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a>.',

	'saved_create' => 'Nov� weblog byl vytvo�en.',
	'saved_update' => 'Weblog byl aktualizov�n.',
	'deleted' => 'Weblog byl smaz�n.',
	'confirm_delete' => 'Chyst�te se smazat weblog %1. Jste si jist?',	

	'blogroll_heading' => 'Nastaven� slu�by Blogroll',
	'blogroll_id' => 'Blogrolling ID #',
	'blogroll_id_desc' => 'Zde m��ete nastavit <a href="http://www.blogrolling.com" target="_blank">blogrolling.com</a> jako slu�bu blogroll. Je to v�born� slu�ba pro spr�vu odkaz�, kter� ukazuje, kdy byly naposled aktualizov�ny. Pokud tuto slu�bu nepou��v�te, jednodu�e nastaven� p�esko�te. Po p�ihl�en� na blogrolling.com zvolte \'get code\', kde najdete odkaz, obsahuj�c� va�e ID. Vypad� n�jak takto: 2ef8b42161020d87223d42ae18191f6d',
	'blogroll_fg' => 'Barva textu',
	'blogroll_bg' => 'Barva pozad�',
	'blogroll_line1' => 'Barva ��dku 1',
	'blogroll_line2' => 'Barva ��dku 2',
	'blogroll_c1' => 'Barva 1',
	'blogroll_c2' => 'Barva 2',
	'blogroll_c3' => 'Barva 3',
	'blogroll_c4' => 'Barva 4',
	'blogroll_c4_desc' => 'Tyto barvy ur�uj�, jak bude vypadat v� blogroll. Barvy 1 a� 4 indikuj� aktu�lnost odkazu.',
);


$lang['upload'] = array (
	//		File Upload		\\
	'preview' => 'Kompletn� seznam',
	'thumbs' => 'N�hled',
	'create_thumb' => '(Vytvo�it n�hled)',
	'title' => 'Soubory',
	'thisfile' => 'Upload nov�ho souboru:',
	'button' => 'Upload',
	'filename' => 'Jm�no souboru',
	'thumbnail' => 'N�hled',
	'date' => 'Datum',
	'filesize' => 'Velikost souboru',
	'dimensions' => '���ka x V��ka',		
	'delete_title' => 'Smazat obr�zek',
	'areyousure' => 'Jste si jisti, �e chcete smazat soubor %s?',
	'picheader' => 'Smazat teto obr�zek?',
	'create' => 'vytvo�it',
	'edit' => 'editovat',

	'insert_image' => 'Vlo�it obr�zek',
	'insert_image_desc' => 'Abyste mohli obr�zek vlo�it, mus�te ho nejd��ve uploadovat nebo vybrat.',
	'insert_image_popup' => 'Vlo�it okno s obr�zkem',
	'insert_image_popup_desc' => 'Abyste mohli vytvo�it okno, mus�te uploadovat obr�zek nebo vybrat ji� exituj�ci. Pot� vyberte text nebo n�hled, kter� bude um�st�n v okn�.',
	'choose_upload' => 'upload',
	'choose_select' => 'nebo vybrat',
	'imagename' => 'Jm�no obr�zku',
	'alt_text' => 'Alternativn� text',
	'align' => 'Zarovnat',
	'border' => 'Okraje',
	'pixels' => 'bod�',
	'uploaded_as' => 'V� soubor byl ulo�en jako \'%s\'.',
	'not_uploaded' => 'V� soubor nebyl ulo�en. Nastala n�sleduj�c� chyba:',
	'center' => 'Centrovat (p�edvolen�)',
	'left' => 'Doleva',
	'right' => 'Doprava',
	'inline' => 'V �ad�',		
	'notice_upload_first' => 'Nej��ve mus�te vybrat nebo uploadovat obr�zek',
	'select_image' => 'Vyberte obr�zek',

	'for_popup' => 'Do popup okna',		
	'use_thumbnail' => 'Pou��t n�hled',		
	'edit_thumbnail' => 'editovat n�hled',		
	'use_text' => 'Pou��t text',		
);


$lang['link'] = array (
	//		Link Insertion \\
	'insert_link' => 'Vlo�it odkaz',
	'insert_link_desc' => 'Vlo�te text do pol��ka n�e. N�v�t�vn�ci va�ich str�nek uvid� tento text p�i p�ejet� my�� p�es odkaz.',
	'url' => 'URL',
	'title' => 'Titulek',
	'text' => 'Text',
);


//		Categories		\\
$lang['category'] = array (
	'edit_who' => 'Zvolte, kdo m��e p�isp�vat do kategorie \'%s\'',
	'name' => 'Jm�no',
	'users' => 'U�ivatel�',
	'make_new' => 'Vytvo�it novou kategorii',
	'create' => 'Vytvo�it kategorii',
	'canpost' => 'Vyberte u�ivatele, kter�m d�te pr�vo p�isp�vat do t�to kategorie',
	'same_name' => 'Kategorie tohoto jm�na ji� existuje',
	'need_name' => 'Kategorie pot�ebuje jm�no',
	
	'allowed' => 'Povoleno',
	'allow' => 'Povolit',
	'denied' => 'Zak�z�no',
	'deny' => 'Zak�zat',
	'edit' => 'Editovat kategorii',
	
	'delete' => 'Smazat kategorii',
	'delete_desc' => 'Zvolte \'Ano\', pokud chcete smazat tuto kategorii',

	'delete_message' => 'V t�to verzi Pivotu budou smaz�ny pouze jm�na kategori�. V p��t�ch verz�ch bude mo�n� vybrat, co se m� prov�st se z�znamy, kter� do t�to kategorie pat��.',
);


$lang['entries'] = array (
	//		Entries			\\
	'post_entry' => "Vlo�it z�znam",
	'preview_entry' => "Prohl�dnout z�znam",

	'first' => 'prvn�',
	'last' => 'posledn�',
	'next' => 'dal��',
	'previous' => 'p�edchoz�',

	'jumptopage' => 'sko�it na str�nku (%num%)',
	'filteron' => 'filtrovat (%name%)',
	'filteroff' => 'filtr vyp.',
	'title' => 'Titulek',
	'subtitle' => 'Podtitulek',
	'introduction' => '�vod',
	'body' => 'T�lo',
	'publish_on' => 'Publikov�no',
	'status' => 'Stav',
	'post_status' => 'Stav vlo�en�',
	'category' => 'Kategorie',
	'select_multi_cats' => '(Ctrl-klik pro v�b�r v�ce kategori�)',
	'last_edited' => "Naposledy zm�n�no",
	'created_on' => "Vytvo�eno",		
	'date' => 'Datum',
	'author' => 'Autor',
	'code' => 'K�d',
	'comm' => 'Po�et kom.',
	'name' => 'Jm�no',
	'allow_comments' => 'Povolit koment��e',
	'convert_lb' => 'Konvertovat konce ��dek',
	'always_off' => '(V�dy vypnuto, pokud pou��v�te WYSIWYG m�d)',
	'be_careful' => '(Pou��vejte opatrn�!)',
	'edit_comments' => 'Editovat koment��e',
	'edit_comments_desc' => 'Editace koment��� k tomuto z�znamu',
	'edit_comment' => 'Editovat koment��',
	'delete_comment' => 'Smazat koment��',
	'block_single' => 'Blokovat IP %s',
	'block_range' => 'Blokovat IP v rozsahu %s',
	'unblock_single' => 'Odblokovat IP %s',
	'unblock_range' => 'Odblokovat IP v rozsahu %s',
	'trackback' => 'Zp�tn� sledov�n�',
);


//		Form Fun		\\
$lang['forms'] = array (
	'c_all' => 'Vybrat v�e',
	'c_none' => 'Zru�it v�b�r',
	'choose' => '- vyberte mo�nost -',
	'publish' => 'Nastavit stav \'publikov�no\'',
	'hold' => 'Nastavit stav \'pozdr�eno\'',
	'delete' => 'Smazat',
	'generate' => 'Publikovat a generovat',

	'with_checked_entries' => "S vybran�mi z�znamy prove�:",
	'with_checked_files' => "S vybran�mi soubory prove�:",
	'with_checked_templates' => 'S vybran�mi �ablonami prove�:',
);


//		Errors			\\
$lang['error'] = array (
	'path_open' => 'nemohu otev��t adres��, zkontrolujte pr�va.',
	'path_read' => 'nemohu ��st adres��, zkontrolujte pr�va.',
	'path_write' => 'nemohu zapisovat do adres��e, zkontrolujte pr�va.',

	'file_open' => 'nemohu otev��t soubor, zkontrolujte pr�va.',
	'file_read' => 'nemohu ��st soubor, zkontrolujte pr�va.',
	'file_write' => 'nemohu zapisovat do souboru, zkontrolujte pr�va.',
);


//		Notices			\\
$lang['notice'] = array (		
	'comment_saved' => "Koment��e byly ulo�eny.",
	'comment_deleted' => "Koment��e byly smaz�ny.",
	'comment_none' => "��dn� koment��e k tomuto z�znamu.",
);


// Comments, Karma and voting \\
$lang['karma'] = array (
	'vote' => 'Hlas� \'%val%\' pro tento z�znam',
	'good' => 'Dobr�',
	'bad' => '�patn�',
	'already' => 'U� jste pro tento z�znam hlasoval/a',
	'register' => 'V� hlas pro \'%val%\' byl zaznamen�n',
);


$lang['comment'] = array (
	'register' => 'V� koment�� byl ulo�en.',
	'preview' => 'Prohl�en� va�eho koment��e. Nezapome�te kliknout na \'Vlo�it koment��\', aby se skute�n� ulo�il.',
	'duplicate' => 'V� koment�� nebyl ulo�en, proto�e je kopi� p�edchoz�ho z�znamu.',
	'no_name' => 'M�li byste vlo�it va�e jm�no nebo p�ezd�vku do pol��ka \'jm�no\'. Nezapome�te kliknout na \'Vlo�it koment��\', aby se skute�n� ulo�il.',
	'no_comment' => 'M�li byste n�co napsat do pol��ka \'koment��\'. Nezapome�te kliknout na \'Vlo�it koment��\', aby se skute�n� ulo�il.',
);


$lang['comments_text'] = array (
	'0' => "��dn� koment��e",
	'1' => "Jeden koment��",
	'2' => "Koment���: %num%", 
);


$lang['weblog_text'] = array (
	// these are used in the weblogs, for the labels related to archives
	'archives' => "Archivy",
	'next_archive' => "Dal�� archiv",
	'previous_archive' => "P�edchoz� archiv",
	'last_comments' => "Posledn� koment��e",
	'last_referrers' => "Posledn� odkazy",
	'calendar' => "Kalend��",
	'links' => "Odkazy",
	'xml_feed' => "XML kan�l (RSS 1.0)",
	'powered_by' => "Vyu��v�",
	'name' => "Jm�no",
	'email' => "Email",
	'url' => "URL",
	'date' => "Datum",		
	'comment' => "Koment��",
	'ip' => "IP-adresa",		
	'yes' => "Ano",
	'no' => "Ne",
	'emoticons' => "Emotikony",
	'emoticons_reference' => "Otev��t seznam emotikon�",
	'textile' => "Textile",
	'textile_reference' => "Otev��t n�pov�du Textile",  
	'post_comment' => "Vlo�it koment��",
	'preview_comment' => "Prohl�dnout koment��",
	'remember_info' => "Zapamatovat osobn� informace?",
	'disclaimer' => "<b>Mal� upozorn�n�:</b> v�echny HTML tagy krom� &lt;b&gt; a &lt;i&gt; budou odstran�ny. Odkazy a maily sta�� vlo�it zaps�n�m prost�ho textu.",	
	'search_title' => "V�sledky hled�n�",
	'search' => "Hledat!",
	'nomatches' => "Pro '%name%' nebylo nic nalezeno. Zkuste vlo�it n�co jin�ho.",
	'matches' => "V�sledky pro '%name%':",
);


$lang['ufield_main'] = array (
	//		Userfields		\\
	'title' => 'Editovat u�ivatelsk� pole',
	'edit' => 'Editovat',
	'create' => 'Vytvo�it',

	'dispname' => 'Zobrazen� jm�no',
	'intname' => 'Vnit�n� jm�no',
	'intname_desc' => 'Vnit�n� jm�no je jm�no polo�ky, kter� bude zobrazeno, pokud po��d�te Pivot o zobrazen� �ablony.',
	'size' => 'Velikost',
	'rows' => '��dky',
	'cols' => 'Sloupce',
	'maxlen' => 'Maxim�ln� d�lka',
	'minlevel' => 'Min. �rove� u�ivatele',	
	'filter' => 'Filtrovat podle',
	'filter_desc' => 'Filtrov�n�m podle t�to polo�ky m��ete omezit vstup, kter� v n�m m��e b�t.',
	'no_filter' => 'Nic',
	'del_title' => 'Potvrdit smaz�n�',
	'del_desc' => 'Smaz�n� pol��ka (<b>%s</b>) u�ivatelsk�ho z�znamu sma�e tak� v�echny z�znamy, kter� v n�m u�ivatel ulo�il, a m�sto n�j bude v �ablon�ch pr�zn� m�sto.',	
	
	'already' => 'Toto jm�no je ji� pou��v�no',
	'int' => 'Vnit�n� jm�no mus� b�t del�� ne� 3 znaky',
	'short_disp' => 'Zobrazen� jm�no mus� b�t del�� ne� 3 znaky',
);


$lang['bookmarklets'] = array (
	'bookmarklets' => 'Bookmarklety',
	'bm_add' => 'P�idat bookmarklet.',
	'bm_withlink' => 'Nov� Pivot',
	'bm_withlink_desc' => 'Bookmarklet otev�e nov� okno se z�znamem, kter� bude obsahovat odkaz na str�nku, ze kter� byl otev�en.',

	'bm_nolink' => 'Nov� Pivot',
	'bm_nolink_desc' => 'Bookmarklet otev�e okno s pr�zdn�m z�znamem.',

	'bookmarklets_info' => 'Bookmarklety m��ete pou��t k rychl�mu vkl�d�n� nov�ch z�znam� do Pivotu. Pro vlo�en� bookmarkletu do va�eho prohl�e�e postupujte n�sledovn� (postup se m��e m�rn� li�it podle pou�it�ho prohl�e�e)',
	'bookmarklets_info_1' => 'Klikn�te a p�et�hn�te bookmarklet do va�� li�ty s odkazy nebo na tla��tko \'Bookmarks\' (Z�lo�ky).',
	'bookmarklets_info_2' => 'Prav�m my��tkem klikn�te na bookmarklet a vyberte \'Add to Bookmarks\' (P�idat do z�lo�ek).',
);


// A little tool to help you check if the file is correct..
if (count(get_included_files())<2) {

	$groups = count($lang);
	$total = 0;
	foreach ($lang as $langgroup) {
		$total += count($langgroup);
	}
	echo "<h2>Jazykov� soubor je v po��dku!</h2>";
	echo "Tento soubor obsahuje $groups skupin a celkem $total n�pis�.";

}

?>