<?php
//Deutsch (German) (Du) (UTF-8)

//the above line is needed so that pivot knows how to display it in the user info.
//it also needs to be on the 2rd line.

// German translation of Pivot lang file
// Translated by: Teebee <tim@teebee.org> (www.teebee.org), and Knut <inbox@etribe.de> (www.etribe.de)
// Complete re-edited and corrected by: Nicole Simon (http://beissholz.de)
//
// Last updated by Daniel <djm@vfblog.com>, 02.06.2006
//
// Beachte: Diese Sprachdatei nutzt die formlosere "Du" Form. Falls Sie die "Sie" Form bevorzugen,
// laden Sie sich die entsprechende Datei von www.pivotstyles.net herunter.


// allow for different encoding for non-western languages
$encoding="utf-8";
$langname="de";


//		General		\\
$lang['general'] = array (
	'yes' => 'Ja',	//affirmative
	'no' => 'Nein',		//negative
	'go' => 'Weiter',	//proceed

    'minlevel' => 'Du hast keine Berechtigung für diesen Bereich von Pivot',
    'email' => 'E-Mail',
	'url' => 'URL',
	'further_options' => "Weitere Optionen",
    'basic_view' => "Standard-Ansicht",
    'basic_view_desc' => "Zeige die einfache Ansicht",
	'extended_view' => "Erweiterte Ansicht",
    'extended_view_desc' => "Alle editierbaren Felder anzeigen",
	'toggle_view' => "Zwischen einfacher und erweiterter Ansicht umschalten.",
    'select' => "Auswählen",
	'cancel' => "Abbrechen",
	'delete' => 'Löschen',
	'edit' => 'Edit',
	'welcome' => "Willkommen zu %build%.",
	'write' => "Schreibe",
	'write_open_error' => "Schreibfehler. Konnte die Datei nicht zum schreiben öffnen.",
	'write_write_error' => "Schreibfehler. Konnte nicht in die Datei schreiben.",
	'done' => "Fertig!",
	'shortcuts' => "Abkürzungen",
    'cantdelete' => "Du hast keine Berechtigung, den Artikel %title% zu löschen!",
    'cantdothat' => "Du hast keine Berechtigung, dies mit dem Artikel %title% zu machen!",
	'cantdeletelast' => "Du kannst den (aller)letzten Eintrag nicht löschen. Erstelle erst einen neuen Eintrag um diesen Eintrag löschen zu können.",
	'more' => "mehr",
);


$lang['userlevels'] = array(
    'Superadmin', 'Administrator', 'Fortgeschritten', 'Normal', 'Moblogger'
		//  this one might be a bit hard to translate, but basically it's an order of
		//  power or trust.  Superadmin would be the person in charge - no one can do
		//  anything about his decisions. Admin is only regulated by the Superadmin,
		//  Advanced by the Admin and Superadmin, etc..
		//  Just get the idea of it.
);


$lang['numbers'] = array(
	'null', 'ein', 'zwei', 'drei', 'vier', 'fünf', 'sechs', 'sieben', 'acht', 'neun', 'zehn', 'elf', 'zwölf', 'dreizehn', 'vierzehn', 'fünfzehn', 'sechszehn'
);


$lang['months'] = array (
	'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'
);


$lang['months_abbr'] = array (
	'Jan', 'Feb', 'Mrz', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'
);


$lang['days'] = array (
	'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'
);


$lang['days_abbr'] = array (
	'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'
);


$lang['days_calendar'] = array (
	'S', 'M', 'D', 'M', 'D', 'F', 'S'
);


$lang['datetime_words'] = array (
	'Jahr', 'Monat', 'Woche', 'Wochentag', 'Stunde', 'Minute', 'Sekunden'	//the actual words for them.
);


//		Login Page		\\
$lang['login'] = array (
	'title' => 'Login',
	'name' => 'Benutzername',
	'pass' => 'Passwort',
	'remember' => 'Pivot soll folgendes beibehalten:',
	'rchoice' => array (
		'0' => 'Nichts',
        '1' => 'Meinen Benutzername und mein Passwort',
		'2' => 'Pivot soll mich ab jetzt automatisch anmelden',
	),
    'delete_cookies_desc' => 'Wenn Du sicher bist, daß Du den korrekten Usernamen und Paßwort eingegeben hast, Du aber Probleme mit dem Einloggen hast, kannst Du versuchen, die Cookies für diese Domain zu löschen:',
    'delete_cookies' => 'Cookies löschen',
    'retry' => 'Falscher Benutzernahme oder falsches Passwort',
    'banned' => 'Dein Einloggen ist 10 mal fehlgeschlagen.  Deine IP wurde für die nächsten 12 Stunden zum Einloggen gesperrt.',

);


//		Main Bar		\\
	$lang['userbar'] = array (
	'main' => 'Übersicht',
	'entries' => 'Artikel',
	'submit' => 'Neuer Artikel',
	'comments' => 'Kommentare',
	'trackbacks' => 'Trackbacks',
	'modify' => 'Artikel bearbeiten',
    'userinfo' => 'Meine persönlichen Informationen',
	'u_settings' => 'Meine Einstellungen',
	'u_marklet' => 'Lesezeichen',
    'files' => 'Media-Dateien',
	'upload' => 'Upload',
	'stats' => 'Statistik',
    'admin' => 'Administration',

    'main_title' => 'Globale Übersicht von Pivot',
	'entries_title' => 'Übersicht der Artikel',
    'submit_title' => 'Neuer Artikel',
	'comments_title' => 'Bearbeite oder lösche Kommentare',
	'trackbacks_title' => 'Bearbeite oder lösche Trackbacks',
	'modify_title' => 'Artikel bearbeiten',
    'userinfo_title' => 'Meine persönlichen Informationen einsehen',
    'u_settings_title' => 'Meine Einstellungen bearbeiten',
    'u_marklet_title' => 'Lesezeichen erstellen',
    'files_title' => 'Verwaltung und Hochladen von Media-Dateien',
    'upload_title' => 'Dateien hochladen',
	'uploaded_success' => 'Datei ist hochgeladen',
	'stats_title' => 'Statistiken und Logfiles ansehen.',
    'updatetitles_title' => 'Statistiken und Logfiles ansehen.',
    'admin_title' => 'Übersicht der Administration',
    'recent_entries' => 'Neueste Artikel',
    'recent_comments' => 'Neueste Kommentare',
);


$lang['adminbar'] = array (
	//		Admin Bar		\\
	'seeusers' => 'Benutzer',
	'seeconfig' => 'Konfiguration',
	'filemappings' => 'Datei Übersicht',
	'templates' => 'Templates',
	'maintenance' => 'Wartung',
    'regen' => 'Alle von Pivot generierten Dateien und Archive neu generieren',
	'blogs' => 'Weblogs',
	'categories' => 'Kategorien',
    'verifydb' => 'Datenbank verifizieren',
    'buildindex' => 'Index neu generieren',
    'buildsearchindex' => 'Such-Index neu generieren',
    'buildfrontpage' => 'Startseite neu generieren',
	'sendping' => 'Sende Pings',
	'backup' => 'Sicherheitskopie',
	'description' => 'Beschreibung',
    'conversion' => 'Konvertierung',
    'seeusers_title' => 'Erstelle, bearbeite und lösche Benutzer ',
    'userfields' => 'Felder des Benutzerkontos',
    'userfields_title' => 'Felder des Benutzerkontos bearbeiten',
	'seeconfig_title' => 'Bearbeite Konfigurationsdatei',
	'filemappings_title' => 'Eine Übersicht aller Dateien die von Ihrem Weblog oder Pivot erstellt wurden',
    'templates_title' => 'Templates erstellen, bearbeiten oder löschen',
    'maintenance_title' => 'Standard-Wartung der Pivot-Dateien durchführen',
    'regen_title' => 'Alle von Pivot erstellten Dateien und Archive neu generieren',
    'blogs_title' => 'Weblogs erstellen, bearbeiten oder löschen',
    'blogs_edit_title' => 'Bearbeite Weblog-Einstellungen für ',
    'categories_title' => 'Kategorien erstellen, bearbeiten oder löschen',
    'verifydb_title' => 'Überprüfen der Integrität Ihrer Datenbank',
    'buildindex_title' => 'Generiert den Index der Datenbank neu',
    'buildsearchindex_title' => 'Erstellt den Such-Index neu, um Suchen in den Artikeln zu ermöglichen',
	'buildfrontpage_title' => 'Erneut die Hauptseite, Archive und den RSS-Feed Ihres Weblogs generieren.',
	'backup_title' => 'Sicherheitskopie Ihrer Artikel erstellen ',
	'backup_config' => 'Sicherheitskopie der Konfigurations Dateien',
	'backup_config_desc' => 'Lade Dir eine Zip Datei herunter, die die Konfigurationsdateien enthält.',
	'ipblocks' => 'IP Blockieren',
	'ipblocks_title' => 'Blockierte IP-Adressen anzeigen und bearbeiten.',
    'ipblocks_stored' => 'IP-addresses sind gespeichert.',
    'ipblocks_store' => 'Diese IP-Adressen speichern',
	'ignoreddomains' => 'Ignorierte Domains',
	'ignoreddomains_title' => 'Ignorierte Domains anschauen und bearbeiten.',
	'ignoreddomains_stored' => 'Die Ignorierten Domains wurden gespeichert.',
	'ignoreddomains_store' => 'Speichere diese Ignorierten Domains',
	'ignoreddomains_asterisk' => 'Einträge die mit einem Sternchen ( * ) markiert sind, werden nur dazu genutzt um Referer zu blocken. Alle anderen Einträge blockieren Referer, Kommentare und Trackbacks gleichermaßen.',
	'ignoreddomains_global' => 'Global blockierte Phrasen (Satzteile).',
	'ignoreddomains_global_desc' => 'Unabhängig von den oben aufgeführten Begriffen kannst Du auch eine aktuelle Liste nutzen, die Dein Pivot vom pivotlog.net Server abrufen kann. Momentan beinhaltet die Liste folgende Phrasen:',
	'ignoreddomains_global_empty' => 'Die Liste der global blockierten Phrasen ist leer',
	'ignoreddomains_global_update' => 'Liste der globalen Phrasen aktualisieren (von pivotlog.net)',
	'ignoreddomains_global_delete' => 'Liste löschen',
	'ignoreddomains_global_success' => 'Die Liste mit globalen Phrasen wurde aktualisiert und gespeichert.',
	'ignoreddomains_global_failed' => 'Die Liste mit globalen Phrasen konnte nicht geladen (und gespeichert) werden. Versuch es bitte später nocheinmal!',
	'ignoreddomains_global_deleted' => 'Die Liste mit globalen Phrasen wurde gelöscht.',
    'fileexplore' => 'Datei-Explorer',
    'fileexplore_title' => 'Dateien ansehen, (sowohl Text- als auch Datenbankdateien',
    'sendping_title' => 'Pings zu Update-Tracker(n) senden.',
    'buildindex_start' => 'Erstelle den Index ... Dies kann etwas dauern, bitte NICHT unterbrechen.',
    'buildsearchindex_start' => 'Erstelle den Such-Index  ... Dies kann etwas dauern, bitte NICHT unterbrechen.',
    'buildindex_finished' => 'Erstellen des Index dauerte %num% Sekunden',

	'filemappings_desc' => 'Nachfolgend siehst Du eine Übersicht aller installierten Weblogs innerhalb dieses Pivots, zusammen mit den von Pivot erstellten Dateien und welche Templates dazu benutzt wurden. Dies kann sehr nützlich bei der Fehlersuche sein, falls Du Probleme beim erstellen der Dateien hast.',

	'debug' => 'Debug Fenster öffnen',

	'latest_pivot_news' => "Das Allerneueste von Pivot",
	'remove_setup_header' => "Pivot installations Skript vorhanden",
	'remove_setup' => "Das Pivot installations Skript 'pivot-setup.php' wurde im Pivot Hauptverzeichnis gefunden. Du solltest Dir im klaren darüber sein, das dies ein potenzielles Sicherheitsrisiko ist. Wir empfehlen das Skript zu löschen oder umzubenennen, so das es nicht von Leuten mit bösen Absichten ausgeführt werden kann.",
	'spamprotection' => "Spam Schutz",
	'spamprotection_title' => "Übersicht über die verschiedenen Tools die dabei helfen Dein Weblog Spam-frei zu halten",
	'spamconfig' => "Spam Einstellungen",
	'spamconfig_title' => "Konfiguriere Spam Schutz Werkzeuge (wie HashCash und SpamQuiz).",
	'seespamconfig_title' => 'Konfiguriere Spam Schutz Werkzeuge',
	'spamwasher' => 'Spam Washer',
	'spamwasher_title' => 'Sucht nach Spam und entfernt ihn aus Deinen Einträgen und Trackbacks.',
	'spamlog' => "Spam Log",
	'spamlog_title' => "Spam Log anschauen und zurücksetzen.",
	'viewspamlog_title' => "Spam Log anzeigen",
);


$lang['templates'] = array (
	'rollback' => 'Rollback',
    'create_template' => 'Template erstellen',
    'create_template_info' => 'Erstelle ein neues Template',
	'no_comment' => 'Keine Kommentare',
	'comment' => 'Kommentar*',
    'comment_note' => '(*Beachte: Kommentare können nur beim <b>ersten</b> Speichern erstellt werden.)',
    'create' => 'Template erstellen',
	'editing' => 'Bearbeiten',
	'filename' => 'Dateiname',
	'save_changes' => 'Änderungen speichern!',
	'save_template' => 'Template speichern!',
	'aux_template' => 'Auxillary Template',
	'sub_template' => 'Subtemplate',
	'standard_template' => 'Normal Template',
	'feed_template' => 'Feed Template',
	'css_template' => 'CSS Datei',
	'txt_template' => 'Text Datei',
	'php_template' => 'PHP Datei',
);


//		Maintenace		\\
$lang['maint'] = array (
	'title' => 'Wartung',
	'gen_arc_title' => 'Archiv generieren', /* bob notes: redundant, see 'regen' */
	'gen_arc_text' => 'Alle Archive erneut generieren', /* bob notes: redundant, see 'regen' */
    'xml_title' => 'Überprüfe XML-Dateien', /* bob notes: replace with more general 'Verify DB' */
    'xml_text' => 'Überprüft  (und falls notwendig repariert) die Integrität der XML-Dateien', /* bob notes: replace with more general 'Verify DB' */
	'backup_title' => 'Sicherheitskopie',
    'backup_text' => 'Fertigt eine Sicherheitskopie aller wichtigen Pivot-Dateien an',
);


//		Spam Protection		\\
$lang['spam'] = array (
	'hc_options' => 'HashCash Optionen',
	'hc' => 'Benutze HashCash',
	'hc_desc' => "HashCash ist ein äußerst Leistungsstarker, absolut unsichtbarer Spamschutz. Es benötigt Javascript um auf dem clientrechner zu funktionieren. Falls das für Dich nicht in Frage kommt, lass es einfach ausgeschaltet.",
	'sq_options' => 'SpamQuiz Optionen',
	'sq' => 'Benutze SpamQuiz',
	'sq_desc' => "Bevor ein Kommentar gesendet wird, müssen Ihre Benutzer eine einfache Frage richtig beantworten, deren Antwort jeder kennt. Dieses verwirrt automatisierte Spam-bots, weil jeder Blogger etwas anderes wählt.",
	'sq_question' => 'Frage',
	'sq_question_desc' => "Beispiel: Was sind die ersten beiden Buchstaben des Wortes 'Spam'?",
	'sq_answer' => 'Antwort',
	'sq_answer_desc' => 'Beispiel: <b>Sp</b>',
	'sq_explain' => 'Erklärung',
	'sq_explain_desc' => 'Beispiel: Um automatisiertem Kommentarspam vorzubeugen, musst Du diese einfache Frage beantworten.',
	'sq_days' => 'Altersgrenze',
	'sq_days_desc' => 'Aktiviert SpamQuiz nur für Einträge die älter sind als die hier angegebene Zahl an Tagen. Normalerweise sind nur ältere Einträge von automatisiertem Kommentarspam betroffen. Von daher ist es nicht unbedingt notwendig Deine täglichen Besucher mit diesem Quizz zu belästigen (die beispielsweise innerhalb einer Woche kommentieren).',
	'ht_options' => 'Härtere Trackback Optionen',
	'ht' => 'Benutze Härteren Trackback',
	'ht_desc' => "Härterer Trackback ist ein Leistungsstarker Trackback Spam Schutz. Es benötigt Javascript um auf dem Clientrechner zu funktionieren. Falls das für Dich nicht in Frage kommt, lass es einfach ausgeschaltet.",
	'tburl_gen' => 'Klicken Sie um eine Trackback URL zu generieren',
	'tburl_gen_note' => 'Achtung: die generierte URL ist nur 15 Minuten gültig und benötigt javascript!',
	'tburl_gen_javascript' => 'Bitte aktiviere Javascript in Deinem Browser um eine Trackback URL generieren zu können.',
	'enable_js_comm' => 'Bitte aktiviere Javascript (und lade diese Seite neu) um Kommentare hinzuzufügen zu können.',
	'empty_log' => 'Spam Log ist leer.',
	'reset_log' => 'Spam Log zurücksetzten',
	'reset_log_done' => 'Spam Log zurückgesetzt',
);


//		Stats and referers		\\
$lang['stats'] = array (
    'show_last' => "Zeige die neuesten",
    '20ref' => "20 Referer",
    '50ref' => "50 Referer",
    'allref' => "alle Referer",
    'updateref' => "Aktualisiere die Referer zu den Titel Listen.",
	'showunblocked' => "nur nicht blockierte Zeilen",
	'showall' => "sowohl blockierte als auch nicht blockierte Zeilen",
    'hostaddress' => 'Host-Adresse (IP-Adresse)',
    'whichpage' => 'welche Seite',

    'getting' => 'Sammle neue Titel',
	'awhile' => 'Der Vorgang kann einige Zeit in Anspruch nehmen, bitte NICHT unterbrechen',
    'firstpass' => 'Erster Durchlauf',
    'secondpass' => 'Zweiter Durchlauf',
    'nowuptodate' => 'Deine Refererliste wurde aktualisiert.',
	'finished' => 'Erledigt',);


//		User Info		\\
	$lang['userinfo'] = array (
	'editfields' => 'Bearbeite Benutzerfelder',
    'desc_editfields' => 'Bearbeite die Eingabefelder, die der Benutzer verwenden kann, um sich selbst zu beschreiben',
	'username' => 'Benutzername',
	'pass1' => 'Passwort',
	'pass2' => 'Passwort (zur Verifikation)',
    'email' => 'E-Mail',
	'nickname' => 'Nickname',
    'userlevel' => 'Benutzer-Level',
    'userlevel_desc' => 'Der Benutzer-Level legt fest, welche Aktionen ein Benutzer in Pivot durchführen darf.',
	'language' => 'Sprache',
	'lastlogin' => 'Letzer Login',
	'edituser' => 'Bearbeite Benutzer',  //the link to.. well, edit the user (also the title)
    'edituserinfo' => 'Bearbeite Benutzer-Informationen',
	'selfreg' => 'Selbstregistrierung',
    'newuser' => 'Neuen Benutzer erstellen',
    'desc_newuser' => 'Einen neuen Account in Pivot eröffnen, um dem Benutzer das Erstellen von Artikel zu ermöglichen.',
    'newuser_button' => 'Erstellen',
	'edituser_button' => 'Ändern',
    'pass_too_short' => 'Passwort muss mindestens 4 Zeichen lang sein.',
	'pass_equal_name' => 'Das Passwort darf nicht gleich dem Usernamen sein.',
    'pass_dont_match' => 'Passwörter stimmen nicht überein',
	'username_in_use' => 'Benutzername existiert bereits',
	'username_too_short' => 'Benutzername muss mindestens 4 Zeichen lang sein.',
    'username_not_valid' => 'Benutzernamen können nur alphanumerische Zeichen (A bis Z, 0 bis 9) und Unterstriche (_) enthalten.',
    'not_good_email' => 'Die eingegebene E-Mailadresse ist ungültig',
    'c_admin_title' => 'Bitte bestätige, das ein Administrator definiert wurde.',
    'c_admin_message' => 'Ein '.$lang['userlevels']['1'].' hat alle Rechte in Pivot, kann alle Artikel und Kommentare verändern, und ist in der Lage alle Einstellungen zu ändern. Bist Du sicher, dass Du aus %s einen  '.$lang['userlevels']['1'].' machen wollen?',
);


//		Config Page		\\
	$lang['config'] = array (
	'save' => 'Einstellungen speichern.',

    'sitename' => 'Seiten-Name',
    'defaultlanguage' => 'Standardsprache',
	'defaultencoding' => 'Kodierung benutzen',
	'defaultencoding_desc' => 'Legt die Kodierung (encoding) fest die benutzt wird (z.B. utf-8 oder iso-8859-1). Du solltest das frei lassen, falls Du nicht weist worum es geht. Für den Fall werden dann die Einstellungen aus der Sprachdatei genutzt.',
	'defaulttheme' => 'Theme',
	'selfreg' => 'Erlaube Selbstregistrierung',
	'selfreg_desc' => 'Wenn hier Ja eingestellt wird ist es den Besuchern erlaubt sich als (normal) Benutzer zu registrieren und damit auch Einträge zu erstellen. (Achtung: Das ist kein "Kommentar" Benutzer, sondern jemand der dann - ähnlich wie Du - Einträge schreiben kann.)',
    'siteurl' => 'Seiten-URL',
    'header_fileinfo' => 'Datei-Info',
	'localpath' => 'Lokaler Pfad',
    'debug_options' => 'Debug-Optionen',
    'debug' => 'Debug-Modus',
    'debug_desc' => 'Gelegentlich Debug-Informationen anzeigen ...',
	'log' => 'Logdateien',
    'log_desc' => 'Logdateien für verschiednene Aktivitäten erstellen',

	'unlink' => 'Unlink Files',
	'unlink_desc' => 'Auf einigen Servern bei denen der furchtbare safe_mode eingeschaltet ist, muss man ein wenig mit dieser Option herumprobieren. Bei den meisten Servern dürfte diese Option aber keine Auswirkungen haben.',
	'chmod' => 'Chmod-Rechte für Dateien',
	'chmod_desc' => 'Einige Server benötigen spezielle Rechte für erstellte Dateien. Mögliche Werte wären \'0644\' und \'0755\'. Wenn Du nicht weist worum es geht ändere hier nichts.',
	'header_uploads' => 'Dateien hochladen',
    'upload_path' => 'Upload-Pfad',
    'upload_accept' => 'Akzeptierte Dateiformate',
    'upload_extension' => 'Standard-Dateiendung',
    'upload_save_mode' => 'Überschreiben',
	'make_safe' => 'In websicheren Dateinamen umwandeln',
	'c_upload_save_mode' => 'Inkrementaler Dateiname',
	'max_filesize' => 'Maximale Dateigröße',
	'header_datetime' => 'Datum/Uhrzeit',
    'timeoffset_unit' => 'Einheit, in der der Zeitunterschied gemessen wird',
	'timeoffset' => 'Zeitunterschied',
	'header_extra' => 'Weitere Einstellungen',
    'wysiwyg' => 'WYSIWYG-Editor standardmäßig einschalten',
    'wysiwyg_desc' => 'Legt fest, ob der WYSIWYG-Editor standardmäßig eingestellt sein soll. Jeder Benutzer kann es in seinen eigenen Einstellungen definieren.',
	'basic_view' => 'Grundansicht benutzen',
	'basic_view_desc' => 'Legt fest ob der \'neue Eintrag\' in der Grundansicht oder in der erweiterten Ansicht geöffnet wird.',
    'def_text_processing' => 'Standardverhalten bei der Texteingabe',
    'text_processing' => 'Texteingabe',
    'text_processing_desc' => 'Legt fest, wie der Benutzer den Text eingeben kann, wenn kein  WYSIWYG-Editor verwendet wird. \'Konvertiere Absätze\' ändert nur die Zeilenumbrüche zu einem &lt;br&gt;-tag. <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a> ist eine mächtige und zugleich einfach zu erlernende Textformatierungssprache.',
    'none' => 'Nichts',
    'convert_br' => 'Konvertiere Absätze zu &lt;br /&gt;',
    'textile' => 'Benutze Textile',
	'markdown' => 'Markdown',
	'markdown_smartypants' => 'Markdown and Smartypants',

	'allowed_cats' => 'erlaubte Kategorien',
	'allowed_cats_desc' => 'Der User ist berechtigt in der gewählten Kategorie Einträge zu erstellen.',
	'delete_user' => "User löschen",
	'delete_user_desc' => "Du kannst diesen User löschen wenn Du wollen. Seine Postings bleiben zwar erhalten, aber er wird nicht mehr in der Lage sein sich einzuloggen.",
	'delete_user_confirm' => 'Du bist dabei den Zugang für %s dauerhaft zu sperren (User wird gelöscht). Bist Du sicher?',

    'setup_ping' => 'Ping-Einstellungen',
    'ping_use' => 'Erlaube Update-Tracker',
    'ping_use_desc' => 'Diese Option sendet Update-Trackern wie z.B. weblogs.com Informationen wenn ein neuer Artikel gepostet wurde. Webseiten wie blogrolling.com brauchen diesen Ping.',
    'ping_urls' => 'URLs die angepingt werden sollen',
    'ping_urls_desc' => 'Du kannst mehrere URLS definieren, an die Du Update-Informationen schicken willst. Schreibe jeden Servernamen OHNE http://, jeweils eine URL pro Zeile, getrennt durch ein "|" Zeichen. Bekannte Server sind z.B.:<br /><b>rpc.weblogs.com/RPC2</b> (weblogs.com pinger, der Bekannteste)<br /><b>pivotlog.net/pinger</b> (pivotlog pinger, noch nicht online)<br /><b>rcs.datashed.net/RPC2</b> (euro.weblogs.com pinger)<br /><b>ping.blo.gs</b> (blo.gs pinger). Für deutschsprachige Artikel ist auch blogg.de interessant.<br />',

	'setup_tb' => 'Trackback Einstellungen',
	'tb_email' => 'Email',
	'tb_email_desc' => 'Wenn eingeschaltet, wird eine Email an diese Adresse geschickt wenn ein Trackback hinzugefügt wurde.',

	'new_window' => 'Links in einem neuen Fenster öffnen',
	'emoticons' => 'Emoticons benutzen',
    'javascript_email' => 'E-Mailadresse codieren',
    'new_window_desc' => 'Legt fest, ob Links in Artikel in einem neuen Fenster geöffnet werden sollen.',

	'mod_rewrite' => 'Filesmatch benutzen',
    'mod_rewrite_desc' => 'Wenn Du den Apache Filesmatch benutzt, wird Pivot Urls wie www.mysite.com/archive/2003/05/30/nice_weather, statt www.mysite.com/pivot/entry.php?id=134 erzeugen. Stelle hier das gewünschte Format ein. Nicht alle Server unterstützen diese Funktion, lies bitte erst die Bedienungsanleitung oder frage bei Deinem Provider nach.',
	'mod_rewrite_1' => 'Ja, /archive/2005/04/28/title_of_entry',
	'mod_rewrite_2' => 'Ja, /archive/2005-04-28/title_of_entry',
	'mod_rewrite_3' => 'Ja, /entry/1234',
	'mod_rewrite_4' => 'Ja, /entry/1234/title_of_entry',

    'default_introduction' => 'Standardvorlage für neuen Artikel (Einleitung / Haupttext)',
    'default_introduction_desc' => 'Dies legt die Standardwerte für Einleitung / Haupttext in einem neuen Artikel fest. Normalerweise ist dieser Eintrag leer.',

        'search_index' => 'Suchindex automatisch aktualisieren',
        'search_index_desc' => 'Dies legt fest, ob der Suchindex automatisch aktualisiert wird, wenn ein neuer Artikel verfasst oder ein alter geändert wird.',

	'default_allow_comments' => 'Kommentieren standartmäßig erlauben',
	'default_allow_comments_desc' => 'Legt fest, ob das Kommentieren von Einträgen per Standart erlaubt ist oder nicht.',

  'maxhrefs' => 'Anzahl an Links',
  'maxhrefs_desc' => 'Die maximale Anzahl an Links die pro Kommentar erlaubt sind. Sehr nützlich um die nervigen Kommentarspammer abzuwimmeln. Trage eine 0 (null) ein um die Beschränkung aufzuheben.',
  'rebuild_threshold' => 'Rebuild Grenzwert',
  'rebuild_threshold_desc' => 'Die Zeit (in Sekunden) die für den Wiederaufbau der Seiten benötigt wird, bevor Pivot die Seite neu lädt. Standartmässig ist 28 eingestellt. Versuche den Wert auf 10 zu senken falls Du Probleme mit dem Wiederaufbau der Seite hast.',
	'default_introduction' => 'Grundeinstellungen für Einleitung/Inhalt',
	'default_introduction_desc' => 'Das legt die Standartwerte für die Einleitung und den Inhalt für neue Einträge fest, wenn ein Autor einen solchen verfasst. Normalerweise wird das Feld leer gelassen.',

    'upload_autothumb'    => 'Automatische Thumbnails',
    'upload_thumb_width' => 'Thumbnail-Breite',
    'upload_thumb_height' => 'Thumbnail-Höhe',
    'upload_thumb_remote' => 'Externes cropping-Script',
    'upload_thumb_remote_desc' => 'Wenn Dein Server nicht die notwendigen Bibliotheken für automatisierte Imagebeschneidung installiert hat, kannst Du ein externes cropping Script verwenden.',

	'extensions_header' => 'Extensions Verzeichnis',
	'extensions_desc'   => 'Das \'extensions\' Verzeichnis in dem die Erweiterungen zu Pivot gespeichert werden. Das vereinfacht das Uptaden erheblich. Für mehr Informationen lesen sie die Dokumentation.',
	'extensions_path'   => 'Extensions Verzeichnis Pfad',

);


//		Weblog Config	\\
$lang['weblog_config'] = array (
	'edit_weblog' => 'Weblog bearbeiten',
    'edit_blog' => 'Weblogs bearbeiten',
    'new_weblog' => 'Neues Weblog',
	'new_weblog_desc' => 'Neues Weblog hinzufügen',
	'del_weblog' => 'Weblog löschen',
	'del_this_weblog' => 'Dieses Weblog löschen.',
	'create_new' => 'Neues Weblog erstellen',
    'subw_heading' => 'Du kannst für jedes Subweblog festlegen, welches Templates es benutzten soll und welche Kategorien in diesem veröffentlicht werden sollen.',
	'create' => 'Fertig',

    'create_1' => 'Erstellen  / Ändern Weblog, Schritt 1 von 3',
    'create_2' => 'Erstellen  / Ändern Weblog, Schritt 2 von 3',
    'create_3' => 'Erstellen  / Ändern Weblog, Schritt 3 von 3',

    'name' => 'Name des Weblogs',
	'payoff' => 'Untertitel',
	'payoff_desc' => 'Gib hier einen Untertitel oder Slogan ein.(kann freigelassen werden)',
	'url' => 'URL zum Weblog',
    'url_desc' => 'Pivot wird die URL selber bestimmen, wenn Du dieses Eingabefeld nicht ausfüllst. Wenn Dein Weblog zum Beispiel Teil eines Framesets oder eines server-side-includes ist, kannst du dieses hier festlegen.',
	'index_name' => 'Hauptseite (Index)',
	'index_name_desc' => 'Der Dateiname der Index-Datei. Meistens so etwas wie \'index.html\' oder \'index.php\'.',

    'ssi_prefix' => 'SSI-Prefix',
    'ssi_prefix_desc' => 'Wenn Dein Weblog SSI benutzt (was nicht empfehlenswert ist) kannst Du dieses Eingabefeld benutzen um die SSI-Dateinamen für die Pivotdateien anzugeben. \'index.shtml?p=\'. Lass dieses Eingabefeld einfach leer, es sei denn Du weist genau was Du tust.',

	'front_path' => 'Pfad zur Hauptseite',
    'front_path_desc' => 'Der relative oder absolute Pfad, wo Pivot die Hauptseite dieses Weblogs generiert soll.',
	'file_format' => 'Dateiname',
    'entry_heading' => 'Artikel-Einstellungen',
    'entry_path' => 'Artikel-Pfad',
    'entry_path_desc' => 'Der relative oder absolute Pfad, wo Pivot die Artikelseiten generieren soll.',
    'live_comments' => 'Live-Artikel',
    'live_comments_desc' => 'Wenn Du \'Live-Artikel\' benutzt, muß Pivot nicht für jeden Beitrag eine neue Seite erstellen. Diese Einstellung wird empfohlen.',
    'readmore' => '\'weiterlesen\'-Text',
    'readmore_desc' => 'Der Text des Links, der angezeigt wird, um den gesamten Artikel zu lesen',

    'arc_heading' => 'Archiv-Einstellungen',
    'arc_index' => 'Index-Datei',
    'arc_path' => 'Archiv-Pfad',
	'archive_amount' => 'Anzahl der Archive',
    'archive_unit' => 'Archiv-Art',
    'archive_format' => 'Archiv-Format',
	'archive_none' => 'Keine Archive',
	'archive_weekly' => 'Archive pro Woche',
	'archive_monthly' => 'Archive pro Monat',
	'archive_yearly' => 'Archive pro Jahr',

    'archive_link' => 'Archiv-Link',
    'archive_linkfile' => 'Archive-Linkformat',
    'archive_order' => 'Archive-Sortierung',
    'archive_ascending' => 'Aufsteigend (älteste zuerst)',
    'archive_descending' => 'Absteigend (neueste zuerst)',

	'templates_heading' => 'Templates',
    'frontpage_template' => 'Template für die Hauptseite',
    'frontpage_template_desc' => 'Das Template, welches das Layout der Hauptseite bestimmt.',
    'archivepage_template' => 'Template für die Archivseite',
    'archivepage_template_desc' => 'Das Template, welches das Layout der Archivseiten bestimmt. Dies kann das gleiche sein wie das Ihrer Hauptseite.',
    'entrypage_template' => 'Template für einen kompletten Artikel',
    'entrypage_template_desc' => 'Das Template, welches das Layout für die Anzeige eines einzelnen Artikel bestimmt.',
	'extrapage_template' => 'Extra Template',
	'extrapage_template_desc' =>  'Das Template, welches das Layout für die Anzeige des Archives und der search.php bestimmt.',

    'shortentry_template' => 'Template für den \'Kurzeintrag\'',
    'shortentry_template_desc' => 'Das Template, welches das Layout eines \'Kurzeintrag\' bestimmt, so wie es in dem Weblog oder in den Archiven angezeigt wird, quasi die Kurzversion inklusive eines möglichen \'weiterlesen\' .',
    'num_entries' => 'Anzahl der Artikel',
    'num_entries_desc' => 'Die Zahl der Artikel, die auf der Hauptseite gezeigt werden sollen',
	'offset' => 'Offset',
	'offset_desc' => 'Wenn Offset eine Zahl zugewiesen wird, wird diese Anzahl an Einträgen beim generieren der Seite übersprungen. Das kann dazu benutzt werden um z.B. eine \'frühere Einträge\' -Liste zu erstellen.',
	'comments' => 'Kommentare erlauben?',
    'comments_desc' => 'Legt fest, ob generell Kommentare zu Ihren Beiträge geschrieben werden können. (Du kannst in den erweiterten Einstellungen für jeden Artikel festlegen, ob hierfür Kommentare erlaubt sind oder nicht.) ',

	'publish_cats' => 'veröffentliche diese Kategorien',

    'setup_rss_head' => 'RSS und Atom-Konfiguration',
    'rss_use' => 'Erzeuge Feeds',
    'rss_use_desc' => 'Legt fest, ob Pivot automatisch einen RSS-Feed und einen Atom-Feed für Ihrer Seite generiert.',
    'rss_filename' => 'RSS-Dateiname',
    'atom_filename' => 'Atom-Dateiname',
    'rss_path' => 'Feed-Pfad',
    'rss_path_desc' => 'Der relative oder absolute Pfad in dem Pivot die Feed-Dateien erzeugen soll.',
//    'rss_size' => 'Länge der Feed-Artikel',
//    'rss_size_desc' => 'Die Länge eines Artikel (in Zeichen) in den Feed-Dateien.',
	'rss_full' => 'erstelle vollständige Feeds',
	'rss_full_desc' => 'Bestimmt, ob Pivot vollständige Atom oder RSS Feeds erstellen soll. Wenn \'nein\' eingestellt ist, wird Pivot Feeds erstellen die nur kurze Beschreibungen enthalten. Dadurch werden die Feeds unbrauchbarer.',
	'rss_link' => 'Feed Link',
	'rss_link_desc' => 'Der Link der mit dem Feed gesendet wird, um auf die Hauptseite zu verlinken. Wenn Du dieses Feld leer lässt, wird Pivot den Weblog Index als Link versenden.',
	'rss_img' => 'Feed Image',
	'rss_img_desc' => 'Du kannst eine Grafik angeben die mit dem Feed gesendet wird. Bei einigen Feed Lesern wird das Bild zusammen mit dem Feed angezeigt. Lass das Feld leer oder gib eine komplette URL an.',

  'lastcomm_head' => 'Einstellungen für "Neueste Kommentare"',
    'lastcomm_amount' => 'Anzahl der zu zeigenden Kommentare',
    'lastcomm_length' => 'Schneide ab nach wieviel Zeichen',
	'lastcomm_format' => 'Format',
    'lastcomm_format_desc' => 'Diese Einstellungen verändern die Ausgabe \'Neueste Kommentare\' ([[last_comments]]) ',
  	'lastcomm_nofollow' => 'Benutze \'Nofollow\'',
      'lastcomm_nofollow_desc' => 'Um den Refererspam zu bekämpfen, kannst du das Attribut rel="nofollow" an alle Links in Kommentaren und Verweisen hinzufügen. Dadurch wird verhindert, dass die Spammer einen besseren Pagerank bei Google bekommen.',

    'lastref_head' => 'Einstellungen für "Neueste Referer"',
    'lastref_amount' => 'Anzahl der zu zeigenden Referer',
    'lastref_length' => 'Schneide ab nach wieviel Zeichen',
	'lastref_format' => 'Format',
    'lastref_format_desc' => 'Diese Einstellungen verändern die Ausgabe \'Neueste Referer\' ([[last_referrers]])',
	'lastref_graphic' => 'benutze Grafiken',
	'lastref_graphic_desc' => 'Hier wird eingestellt ob in der Refererliste Grafiken benutzt werden sollen um die bekanntesten Suchmaschinen anzuzeigen, von denen die Besucher kommen.',
	'lastref_redirect' => 'Referer umleiten',
	'lastref_redirect_desc' => 'Um den Refererspam zu bekämpfen, kannst Du die ausgehenden Links umleiten. Dadurch wird verhindert das die Spammer einen besseren PageRank bei Google erreichen.',

	'various_head' => 'Weitere Einstellungen',
	'emoticons' =>  'Emoticons verwenden',
    'emoticons_desc' => 'Legt fest, ob zum Beispiel :-) zu einen Smiley konvertiert wird.',
    'encode_email_addresses' => 'E-Mailadressen codieren',
    'encode_email_addresses_desc' => 'Entscheidet ob E-Mailadressen als Javascript geschützt werden, um das automatisierte Sammeln von E-Mailadressen durch sog. Spam-Harvester zu vermeiden.',
    'target_blank' => 'Neues Fenster',
    'xhtml_workaround' => 'XHTML-Workaround',
    'target_blank_desc' => 'Falls \'Ja\', werden alle Links in Ihren Artikeln in einem neuen Fenster geöffnet. Wähle \'XHTML workaround\' damit alle Links einen rel="external" erhalten, um ein \'wellformed XML\' zu erreichen.',

    'date_head' => 'Einstellungen der Datumsausgabe ',
	'full_date' => 'Schreibweise des vollständigen Datums',
    'full_date_desc' => 'Legt das Format fest, in dem das komplette Datum (full date) dargestellt wird. Wird meistens am Start eines einzelnen Artikels dargestellt',
    'entry_date' => 'Artikel-Datum (entry_date)',
    'diff_date' => 'Alternatives Datum (diff_date)',
    'diff_date_desc' => 'Das alternative Datum wird meistens zusammen mit dem Artikeldatum verwendet. Im Gegensatz zum Artikeldatum wird das alternative Datum nur angezeigt, wenn sich das Datum des Tages von dem vorherigen Artikel unterscheidet.',
	'language' => 'Sprache',
	'language_desc' => 'Die Sprache die festlegt in welcher Sprache die Daten und Zahlen ausgegeben werden sollen, und auch das \"page\'s charset encoding\" (z.B. iso-8859-1 oder koi8-r) bestimmt.',

    'comment_head' => 'Kommentar-Einstellungen',
	'comment_sendmail' => 'Benachrichtigung bei neuen Kommentaren',
    'comment_sendmail_desc' => 'Versendet automatisch eine E-Mail bei neuen Kommentaren',
    'comment_emailto' => 'E-Mailadressen der Empfänger',
    'comment_emailto_desc' => 'An welche Adresse(n) sollen die Kommentarbenachrichtigungen geschickt werden? Mehrere Adressen bitte durch ein Komma (,) trennen.',
	'comment_texttolinks' => 'Text zu Links konvertieren',
    'comment_texttolinks_desc' => 'Bestimmt ob eingegebene URL\'s und Mailadressen automatisch zu klickbaren Links konvertiert werden.',
	'comment_wrap' => 'Automatischer Zeilenumbruch',
	'comment_wrap_desc' => 'Um das Layout zu schützen wird eine Zeile des Kommentartextes nach einer gewissen Anzahl von Zeichen automatisch umgebrochen',
    'comments_text_0' => 'Text für \'kein Kommentar\'',
    'comments_text_1' => 'Text für \'einen Kommentar\'',
	'comments_text_2' => 'Text für \'X Kommentare\'',
    'comments_text_2_desc' => 'Der Text der benutzt wird um die Zahl der Kommentare anzuzeigen, wenn Du nichts eingibst wird Pivot die Standardtexte benutzen.',

	'comment_pop' => 'Kommentare in einem Popup anzeigen?',
	'comment_pop_desc' => 'Kommentare werden als Popup geöffnet.',
	'comment_width' => 'Breite des Popups',
	'comment_height' => 'Höhe des Popups',
    'comment_height_desc' => 'Legt Breite und Höhe des Kommentar-Popups fest.',

    'comment_format' => "Format der Komments",
    'comment_format_desc' => "Legt fest, wie Kommentare auf den Artikelseiten angezeigt werden sollen.",

    'comment_reply' => "Format von 'antworten ..'",
	'comment_reply_desc' => "Legt das Format des Links fest, den die Besucher benutzen können um auf einen bestimmten Kommentar zu antworten.",
	'comment_forward' => "Format von 'Antwort von ..'",
	'comment_forward_desc' => "Legt das Format des Textes fest, der angezeigt wird wenn ein Kommentar kommentiert (erwidert) wurde.",
	'comment_backward' => "Format von 'Antwort an ..'",
	'comment_backward_desc' => "Legt das Format des Textes fest, der angezeigt wird wenn der Kommentar eine Antwort auf einen anderen Kommentar ist.",

    'comment_textile' => 'Textile erlauben',
    'comment_textile_desc' => 'Wenn dieses mit \'Ja\' eingestellt wird können Besucher <a href="http://www.textism.com/tools/textile/" target="_blank">Textile</a>  in ihren Kommentaren verwenden.',
	'save_comment' => 'Kommentar speichern',
	'comment_gravatardefault' => 'voreingestellter Gravatar',
	'comment_gravatardefault_desc' => 'URL zu dem voreingestellten Gravatar Bild. Beginnt mit http://',
	'comment_gravatarhtml' => 'Gravatar HTML',
	'comment_gravatarhtml_desc' => 'HTML um einen Gravatar einzubinden. %img% wird dabei durch die URL des Bildes ersetzt.',
	'comment_gravatarsize' => 'Gravatar Größe',
	'comment_gravatarsize_desc' => 'Größe (in Pixel) des Gravatars. Vorgabewert ist 48.',

    'trackback_head' => 'Trackback Einstellungen',
	'trackback_sendmail' => 'Email senden?',
	'trackback_sendmail_desc' => 'Nachdem ein Trackback palziert wurde, kann eine Email an den Verwalter dieses Weblogs gesendet werden.',
	'trackback_emailto' => 'Email an',
	'trackback_emailto_desc' => 'Hier wird die Emailadresse angegeben an welche die Mail geschickt wird. Mehrere Adressen werden duch ein Komma getrennt.',
    'trackbacks_text_0' => 'Text für \'keine trackbacks\'',
	'trackbacks_text_1' => 'Text für \'ein trackback\'',
	'trackbacks_text_2' => 'Text für \'X trackbacks\'',
	'trackbacks_text_2_desc' => 'Der Text der benutzt wird um die Anzahl der Trackbacks anzuzeigen. Wenn Du diese Felder leer lässt, wird Pivot die standartmäsig eingestellten Texte verwenden, die in den Spracheinstellungen angegeben sind.',
	'trackback_pop' => 'Trackbacks Popup?',
	'trackback_pop_desc' => 'Hier wird eingestellt ob die Trackbackseite (oder\'einzel Eintrag\') in einem Popup oder in dem original Browserfenster angezeigt werden soll.',
	'trackback_width' => 'Popup Breite',
	'trackback_height' => 'Popup Höhe',
	'trackback_height_desc' => 'Lege die Höhe und Breite des Trackback pop-up´s fest (in Pixel).',
	'trackback_format' => "Format des Trackbacks",
	'trackback_format_desc' => "Das legt das Format der Trackbacks auf den Eintragseiten fest.",
	'trackback_link_format' => "Format des Trackback Links",
        'save_trackback' => 'Trackback speichern',

	'saved_create' => 'Das neue Weblog wurde erstellt.',
	'saved_update' => 'Das Weblog wurde erfolgreich modifiziert.',
	'deleted' => 'Das Weblog wurde gelöscht.',
	'confirm_delete' => 'Du willst das Weblog %1 löschen. Bist Du sicher?',

	'blogroll_heading' => 'Blogroll Einstellungen',
	'blogroll_id' => 'Blogrolling ID #',
    'blogroll_id_desc' => 'Optional kannst Du eine <a href="http://www.blogrolling.com" target="_blank">blogrolling.com</a>-Blogroll in Deinem Weblog verwenden. Blogrolling ist ein guter Dienst um eine Linkliste zu erstellen, die automatisch Updates anzeigt. Wenn Du das nicht willst, kannst Du diese Eingabe überspringen. Andernfalls logge Dich sich bitte bei blogrolling.com ein, schaue nach \'get code\' und suche nach Links die Deine blogroll\' ID beinhalten - sie sollten Aussehen wie dieses hier: 2ef8b42161020d87223d42ae18191f6d',
    'blogroll_fg' => 'Text-Farbe',
    'blogroll_bg' => 'Hintergrund-Farbe',
    'blogroll_line1' => 'Farbe Line 1',
    'blogroll_line2' => 'Farbe Line 2',
    'blogroll_c1' => 'Farbe 1',
    'blogroll_c2' => 'Farbe 2',
    'blogroll_c3' => 'Farbe 3',
    'blogroll_c4' => 'Farbe 4',
    'blogroll_c4_desc' => 'Diese Farben legen fest, wie Dein Blogroll aussehen wird. Farbe 1  bis 4 zeigt visuell an, wie lang es her ist seit ein Link geupdated wurde.',
);


$lang['upload'] = array (
	//		File Upload		\\
    'preview' => 'Vorschau der vollständigen Liste',
    'thumbs' => 'Thumbnail-Vorschau',
	'create_thumb' => '(Thumbnail erstellen)',
	'title' => 'Dateien',
	'thisfile' => 'Neue Datei hochladen:',
	'button' => 'Hochladen',
	'filename' => 'Dateiname',
	'thumbnail' => 'Thumbnail',
	'date' => 'Datum',
	'filesize' => 'Größe',
	'dimensions' => 'Breite x Höhe',
	'delete_title' => 'Bild löschen',
    'areyousure' => 'Bist Du sicher, das Du die Datei %s löschen willst?',
	'picheader' => 'Dieses Bild löschen?',
	'create' => 'erstellen',
	'edit' => 'bearbeiten',

	'insert_image' => 'Bild einfügen',
    'insert_image_desc' => 'Um ein Bild einzufügen musst Du erst ein Bild hochladen oder ein bereits hochgeladenes Bild auswählen',
    'insert_image_popup' => 'Popup-Bild einfügen',
    'insert_image_popup_desc' => 'Um ein Popup-Bild einzufügen musst Du erst ein Bild hochladen oder ein bereits hochgeladenes Bild auswählen. Danach wählst Du einen Text oder einen Thumbnail, welcher als Trigger für das Popup dienen soll.',
	'choose_upload' => 'hochladen',
    'choose_select' => 'oder auswählen',
	'imagename' => 'Name des Bildes',
	'alt_text' => 'Alternativer Text',
    'align' => 'Ausrichtung',
    'border' => 'Rahmen',
    'pixels' => 'Pixelstärke',
    'uploaded_as' => 'Deine Datei wurde als \'%s\' hochgeladen.',
    'not_uploaded' => 'Deine Datei wurde nicht hochgeladen. Folgende Fehler traten auf:',
    'center' => 'Zentrieren (Standard)',
	'left' => 'Links',
	'right' => 'Rechts',
	'inline' => 'Inline',
    'notice_upload_first' => 'Du musst zuerst ein Bild hochladen oder auswählen',
    'select_image' => 'Bild auswählen',
	'select_file' => 'Datei auswählen',

	'for_popup' => 'Für das Popup',
	'use_thumbnail' => 'Verwende Thumbnail',
    'edit_thumbnail' => 'Bearbeite Thumbnail',
	'use_text' => 'Verwende Text',
	'insert_download' => 'Download erstellen',
	'insert_download_desc' => 'Um einen Download zu erstellen, musst Du eine Datei hochladen, oder eine bereits hochgeladene Datei auswählen. Dann wählst Du einen Text, ein Icon, oder ein Thumbnail um den Download auszulösen.',
	'use_icon' => 'Verwende Icon',
);


$lang['link'] = array (
	//		Link Insertion \\
    'insert_link' => 'Einen Link einfügen',
    'insert_link_desc' => 'Füge einen Link hinzu indem Du einen in das Feld unten eingeben. Besucher der Seite werden diesen Text sehen, wenn sie mit der Maus über das Bild gehen.',
	'url' => 'URL',
	'title' => 'Titel',
	'text' => 'Text',
);


//		Categories		\\
$lang['category'] = array (
    'edit_who' => 'Festlegen wer in dieser Kategorie \'%s\' Artikel erstellen darf.',
	'name' => 'Name',
	'users' => 'Benutzer',
	'make_new' => 'Neue Kategorie erstellen',
	'create' => 'Kategorie erstellen',
    'canpost' => 'Bitte die Benutzer auswählen, die in dieser Kategorie Artikel verfassen dürfen',
    'same_name' => 'Eine Kategorie mit diesem Namen existiert bereits',
	'need_name' => 'Bitte gib einen Namen ein',

	'allowed' => 'Erlaubt',
    'allow' => 'Erlauben',
    'denied' => 'Verweigert',
    'deny' => 'verweigern',
	'edit' => 'Kategorie bearbeiten',

	'delete' => 'Kategorie löschen',
    'delete_desc' => 'Wähle \'Ja\', um diese Kategorie zu löschen',

    'delete_message' => 'In dieser Version wird nur der Kategoriename gelöscht, in späteren Versionen kannst Du im gleichen Moment auch entscheiden, was mit den Artikel aus dieser Kategorie geschehen soll.',
	'search_index_newctitle'   => 'Indexiere diese Kategorie',
	'search_index_newcdesc'    => 'Nur dann auf \'Nein\' setzen, wenn Du nicht willst das Besucher Deiner Seite in dieser Kategorie suchen können.',
	'search_index_editcheader' => 'Index Kategorie',

	'order' => 'Sortier Reihenfolge',
	'order_desc' => 'Kategorien mit einer niedrigeren Zahl werden weiter oben in der Liste angezeigt. Wenn Du gleiche Nummern vergibst, werden sie Alphabetisch geordnet',
	'public' => 'öffentliche Kategorie',
	'public_desc' => 'Wenn hier \'Nein\' ausgewählt wird, ist diese Kategorie nur für registrierte Besucher sichtbar. (das gilt nur für die live Pages)',
	'hidden' => 'Kategorie verbergen',
	'hidden_desc' => 'Wenn hier \'Ja\' ausgewählt wird, wird der Eintrag in den Archiv Listen versteckt. (das gilt nur für die live Pages)',

);


$lang['entries'] = array (
	//		Entries			\\
    'post_entry' => "Artikel veröffentlichen",
	'preview_entry' => "Vorschau",
	'edit_entry' => "Artikel bearbeiten",
	'edit_entry_desc' => "diesen Artikel bearbeiten",

	'first' => 'erster',
	'last' => 'letzter',
    'next' => 'nächster',
	'previous' => 'vorheriger',

    'jumptopage' => 'springe zu Seite (%num%)',
    'filteron' => 'Filter auf (%name%)',
    'filteroff' => 'Filter aus',
	'title' => 'Titel',
	'subtitle' => 'Untertitel',
    'introduction' => 'Einleitung Artikel',
    'body' => 'Haupttext',
    'publish_on' => 'Veröffentlicht am',
	'status' => 'Status',
    'post_status' => 'Status des Artikels',
	'category' => 'Kategorie',
    'select_multi_cats' => '(STRG + Klick um mehrere Kategorien auszuwählen)',
	'last_edited' => "Zuletzt bearbeitet am",
	'created_on' => "Erstellt am",
	'date' => 'Datum',
	'author' => 'Verfasser',
	'code' => 'Code',
	'comm' => '# Kommentare',
	'track' => '# Track',
	'name' => 'Name',
	'allow_comments' => 'Kommentare erlauben',

    'delete_entry' => "Eintrag löschen",
	'delete_entry_desc' => "Lösche diesen Eintrag und die dazugehörigen Kommentare ",
	'delete_one_confirm' => "Willst Du diesen Eintrag wirklich löschen?",
	'delete_multiple_confirm' => "Willst Du diese Einträge wirklich löschen?",

    'convert_lb' => 'Zeilenumbrüche konvertieren',
    'always_off' => '(Wenn Du im WYSIWYG-Modus arbeitest ist dies standardmässig ausgeschaltet)',
	'be_careful' => '(Sei hiermit vorsichtig!)',
	'edit_comments' => 'Kommentare bearbeiten',
	'edit_comments_desc' => 'Die Kommentare zu diesem Artikel bearbeiten',
	'edit_comment' => 'Kommentare bearbeiten',
	'delete_comment' => 'Kommentar löschen',
	'report_comment' => 'Kommentar melden',
	'edit_trackback' => 'Trackback bearbeiten',
	'edit_trackback_desc' => 'Die Trackbacks bearbeiten die zu diesem Artikel eingetragen wurden.',
	'delete_trackback' => 'Trackback löschen',
	'report_trackback' => 'Trackback melden',
	'block_single' => 'IP %s blockieren',
    'block_range' => 'IP-Bereich %s blockieren',
	'unblock_single' => 'Blockierung der IP %s aufheben',
    'unblock_range' => 'Blockierung des IP-Bereiches %s aufheben',
    'trackback' => 'Trackback-Ping',
	'trackback_desc' => 'Sende einen Trackback Pings an folgende URL(s). Um an mehrere URL´s zu senden, ist jede URL auf eine separate Zeile zu setzen.',
	'keywords' => 'Keywords',
	'keywords_desc' => 'Hier können Stichwörter angegeben werden, um den Eintrag zu finden, oder um die non-crufty URL für diesen Artikel zu setzen. (das ist eine URL ohne Sonder -z.B. Frage- zeichen. Also: www.mysite.com/archive/2003/05/30/nice_weather, statt www.mysite.com/pivot/entry.php?id=134)',
	'vialink' => "Via Link",
	'viatitle' => "Via Titel",
	'via_desc' => 'Benutze dies, um einen Link auf die Quelle des Eintrages zu setzen.',
	'entry_catnopost' => 'Du bist nicht berechtigt in der Kategorie:\'%s\' zu posten.',
	'entry_saved_ok' => 'Dein Eintrag \'%s\' wurde erfolgreich gespeichert.',
	'entry_ping_sent' => 'Ein Trackback Ping wurde gesendet an: \'%s\'.',
	'encoding_warning' => 'Du hast diesen Artikel in %s geschrieben, obwohl mindestens eines Deiner Weblogs eine andere Codierung verwendet. Um Probleme wie dieses zu vermeiden solltest Du sicherstellen, das alle Nutzer und Weblogs die gleiche Codierung nutzen.',
);


//		Form Fun		\\
$lang['forms'] = array (
    'c_all' => 'alles auswählen',
    'c_none' => 'nichts auswählen',
    'choose' => '- Wähle eine Option -',
    'publish' => 'Status auf \'veröffentlicht\' setzen',
    'hold' => 'Status auf \'Noch nicht veröffentlichen\' setzen',
	'delete' => 'Löschen',
    'generate' => 'Veröffentlichen und generieren',

    'with_checked_entries' =>   "Mit den selektierten Artikel:",
    'with_checked_files' =>     "Mit den selektierten Dateien:",
    'with_checked_templates' => "Mit den selektierten Templates:",
);


//		Errors			\\
$lang['error'] = array (
	'path_open' => 'Kann Verzeichnis nicht öffnen, kontrolliere die Rechte.',
    'path_read' => 'Kann Verzeichnis nicht lesen, kontrolliere die Rechte.',
    'path_write' => 'Kann in Verzeichnis nicht schreiben, kontrolliere die Rechte.',

    'file_open' => 'Kann Datei nicht öffnen, kontrolliere die Rechte.',
    'file_read' => 'Kann Datei nicht lesen, kontrolliere die Rechte.',
    'file_write' => 'Kann nicht in Datei schreiben, kontrolliere die Rechte.',
);


//		Notices			\\
$lang['notice'] = array (
    'comment_saved' => "Der Kommentar wurde gespeichert.",
    'comment_deleted' => "Der Kommentar wurde gelöscht.",
	'comment_none' => "Dieser Artikel hat keine Kommentare.",
    'trackback_saved' => "Der Trackback wurde gespeichert.",
	'trackback_deleted' => "Der Trackback wurde gelöscht.",
	'trackback_none' => "Dieser Eintrag hat keine Trackbacks.",
);


// Comments, Karma and voting \\
$lang['karma'] = array (
	'vote' => 'Wähle \'%val%\' für diesen Artikel',
	'good' => 'Gut',
	'bad' => 'Schlecht',
    'already' => 'Du hast schon für diesen Artikel gestimmt.',
    'register' => 'Deine Wahl für \'%val%\' wurde registriert',
);


$lang['comment'] = array (
	'register' => 'Dein Kommentar wurde gespeichert.',
    'preview' => 'Du siehst Dir die Vorschau an, wähle \'Beitrag veröffentlichen\' um den Kommentar zu speichern.',
    'duplicate' => 'Dein Kommentar wurde nicht gespeichert, weil er identisch mit einem anderen Kommentar ist',
    'no_name' => 'Bitte gib Deinen Name oder einen Alias ein. Denke daran, den Kommentar zu veröffentlichen.',
    'no_comment' => 'Du musst im \'Kommentar\'-Eingabefeld etwas eingeben.',
	'too_many_hrefs' => 'Die maximale Anzahl an Hyperlinks wurde erreicht. Hör auf zu Spammen!',
    'email_subject' => '[Kommentar] Re:',
    'oneclickdelete' => "Löschen mit einem klick.",
    'oneclickreport' => "Löschen und melden mit einem klick.",
);


$lang['comments_text'] = array (
    '0' => "Kein Kommentar",
	'1' => "%num% Kommentar",
	'2' => "%num% Kommentare",
);

$lang['trackbacks_text'] = array (
	'0' => "Kein Trackback",
	'1' => "%num% Trackback",
	'2' => "%num% Trackbacks",
);

$lang['weblog_text'] = array (
	// these are used in the weblogs, for the labels related to archives
	'archives' => "Archive",
	'next_archive' => "Nächstes Archiv",
	'previous_archive' => "Vorheriges Archiv",
    'last_comments' => "Neueste Kommentare",
    'last_referrers' => "Neueste Verweise:",
	'calendar' => "Kalender",
	'links' => "Links",
    'xml_feed' => "XML-Feed (RSS 1.0)",
	'atom_feed' => "XML: Atom Feed",
	'powered_by' => "Powered by",
	'blog_name' => "Weblog Name",
	'title' => "Title",
	'excerpt' => "Zitat",
	'name' => "Name",
    'email' => "E-Mail",
	'url' => "URL",
	'date' => "Datum",
	'comment' => "Kommentar",
	'ip' => "IP-Adresse",
	'yes' => "Ja",
	'no' => "Nein",
	'emoticons' => "Emoticons",
    'emoticons_reference' => 'Zeige Emoticons-Übersicht',
	'textile' => 'Textile',
    'textile_reference' => 'Zeige Textile-Übersicht',
    'post_comment' => "Kommentar veröffentlichen",
	'preview_comment' => "Vorschau",
	'remember_info' => "Persönliche Informationen speichern?",
    'notify' => "Benachrichtigen",
	'notify_yes' => "Ja, ich möchte eine Email wenn jemand antwortet.",
    'register' => "Benutzername Registrieren / Einloggen",
    'disclaimer' => "Alle HTML-Tags außer &lt;b&gt; und &lt;i&gt; werden aus Deinem Kommentar entfernt. Links erstellst Du einfach durch Eingabe einer URL oder der Mailadresse.",
	'search_title' => "Suchergebnisse",
    'search' => "Suche",
    'nomatches' => "Kein Treffer für '%name%'. Bitte etwas anderes versuchen.",
    'matches' => "Treffer für '%name%':",
	'about' => "Über",
	'stuff' => "Zeugs",
	'linkdump' => "Linkdump",
	'discreet' => "E-mail verbergen",
	'discreet_yes' => "Ja, verberge meine E-mail Adresse.",
);


$lang['ufield_main'] = array (
	//		Userfields		\\
    'title' => 'Benutzerfelder bearbeiten',
	'edit' => 'bearbeiten',
	'create' => 'erstellen',

	'dispname' => 'Name anzeigen',
    'intname' => 'Interner Name',
    'intname_desc' => 'Der interne Name ist der Name, der erscheint, wenn Du Pivot sagst er soll ihn in einem Template anzeigen',
	'size' => 'Größe',
	'rows' => 'Reihen',
	'cols' => 'Spalten',
	'maxlen' => 'Maximale Länge',
    'minlevel' => 'Minimaler Benutzer-Level',
    'filter' => 'Filter auf',
    'filter_desc' => 'Mit dieser Option kannst Du die Art der Eingabe eingrenzen',
	'no_filter' => 'kein Filter',
    'del_title' => 'Löschen bestätigen',
    'del_desc' => 'Durch das Löschen dieses Benutzerfeldes (<b>%s</b>) werden auch alle Daten die von Benutzern hier eingegeben wurden gelöscht, sowie alle Stellen im Template von denen auf dieses Eingabefeld zurückgegriffen wird.',

	'already' => 'Der Name wird bereits benutzt.',
    'int' => 'Der interne Name muss länger als 3 Zeichen sein',
	'short_disp' => 'Der angezeigte Name muss länger als 3 Zeichen sein',
);


$lang['bookmarklets'] = array (
	'bookmarklets' => 'Lesezeichen',
	'bm_add' => 'Lesezeichen hinzufügen.',
    'bm_withlink' => 'Piv » Neu',
    'bm_withlink_desc' => 'Dieses Lesezeichen öffnet ein neues Fenster bereit zur Eingabe eines neuen Artikels inklusive eines Links auf die Seite, von der es geöffnet wurde.',

    'bm_nolink' => 'Piv » Neu',
    'bm_nolink_desc' => 'Dieses Lesezeichen öffnet ein Fenster mit einem leeren neuen Artikel.',

    'bookmarklets_info' => 'Lesezeichen können zum schnellen Erstellen von neuen Artikeln verwendet werden. Um ein Lesezeichen zu Deinem Browser hinzuzufügen verwende bitte eine der folgenden Optionen: (Text kann unterschiedlich je nach verwendetem Browser lauten)',
    'bookmarklets_info_1' => 'Ziehe mit einem Klick das Lesezeichen auf Deine Favoriten-Symbolleiste in Deinem Browsers.',
    'bookmarklets_info_2' => 'Klicke mit der rechten Maustaste auf das Lesezeichen und wähle \'Lesezeichen hinzufügen\'.',
);

// Accessibility - These are used for form fields, labels, fieldsets etc.
// for Web Content Accessibility Guidelines & 508 compliancy issues.
// see: http://bobby.watchfire.com/bobby/html/en/index.jsp
// JM =*=*= 2004/10/04
// 2004/11/25 =*=*= JM - minor correction for tim
$lang['accessibility'] = array(
	'search_idname'      => 'Suche',
	'search_formname'    => 'Suche nach Wörter in Einträgen dieser Webseite.',
	'search_fldname'     => 'Gib die zu suchenden Wörter hier ein:',
	'search_placeholder' => 'Suchstring eingeben',

	'calendar_summary'   => 'Diese Tabelle stellt einen Kalender dar. Wenn an einem Tag Einträge vorhanden sind, wird dieser Tag mit den Einträgen verlinkt.',
	'calendar_noscript'  => 'Der Kalender bietet die Möglichkeit auf die Einträge in diesem Weblog zuzugreifen.',
	/*
	2-letter language code, used to designate the principal language used on the site
	see: http://www.oasis-open.org/cover/iso639a.html
	*/

	'lang' => $langname,
) ;


$lang['snippets_text'] = array (
    'word_plural'     => 'Wörter',
    'image_single'    => 'Bild',
    'image_plural'    => 'Bilder',
    'download_single' => 'Datei',
    'download_plural' => 'Dateien',
);

$lang['trackback'] = array (
    'register' => 'Dein Trackback wurde gespeichert.',
    'duplicate' => 'Dein Trackback wurde nicht gespeichert, da es den anschein hat das er bereits existiert.',
    'too_many_hrefs' => 'Die maximale Anzahl an Links wurde erreicht. Hör auf zu Spammen!',
    'noid'      => 'Keine TrackBack ID (tb_id)',
    'nourl'     => 'Keine URL (url)',
    'tracked'   => 'Tracked',
    'email_subject' => '[Trackback] neuer Trackback',
);

$lang['commentuser'] = array (
    'title'             => 'Pivot Benutzer Login',
    'header'            => 'Als ein registrierter Besucher anmelden.',
    'logout'            => 'Abmelden.',
    'loggedout'         => 'Abgemeldet',
    'login'             => 'Anmelden',
    'loggedin'          => 'Angemeldet',
    'loggedinas'        => 'Angemeldet als',
    'pass_forgot'       => 'Passwort vergessen?',
    'register_new'      => 'Neuen Benutzername registrieren.',
    'register'          => 'Als Besucher registrieren',
    'register_info'     => 'Bitte trage die folgenden Informationen ein. <strong>Stelle sicher das Du eine gültige Email Adresse angibst</strong>, da wir eine Bestätigung\'s Email an diese Adresse senden.',
    'pass_note'         => 'Beachte: Der Inhaber der Webseite hat die Möglichkeit Dein Passwort einzusehen... <br /> Nutze <em>kein</em> Passwort<br /> welches Du auch für andere Webseiten / Accounts nutzt!',
    'show_email'        => 'Zeige meine Email Adresse bei Kommentaren.',
    'notify'            => 'Benachrichtige mich bei neuen Einträgen.',
    'def_notify'        => 'Standart Benachrichtigung für Antworten',
    'register'          => 'Registrieren',
    'pass_invalid'      => 'Falsches Passwort',
    'nouser'            => 'Benutzername nicht gefunden..',
    'change_info'       => 'Hier kannst Du Deine Informationen ändern.',
    'pref_edit'         => 'Einstellungen berbeiten',
    'pref_change'       => 'Einstellungen ändern',
    'options'           => 'Optionen',
    'user_exists'       => 'Benutzer existiert bereitss.. Bitte wähle einen anderen Namen.',
    'email_note'        => 'Du musst eine Email Adresse angeben, da es ansonsten unmöglich ist Deinen Account freizuschalten. Du kannst jederzeit angeben das Deine Email Adresse für keine anderen Besucher sichtbar ist.',
    'stored'            => 'Einstellungen wurden gespeichert',
    'verified'          => 'Dein Account wurde freigeschaltet. Bitte melde Dich an..',
    'not_verified'      => 'Der Code scheint falsch zu sein. Du kannst leider nicht freigeschaltet werden.',
    'pass_sent'         => 'Dein Passwort wurde an Deine Email Adresse gesendet..',
    'user_pass_nomatch' => 'Der Benutzername und Die Email Adresse passen nicht zusammen..',
    'user_stored'       => 'Benutzer gespeichert!',
    'user_stored_failed' => 'Konnte den neuen Benutzer nicht speichern!!',
    'pass_send'         => 'Sende Passwort',
    'pass_send_desc'    => 'Wenn Du Dein Passwort vergessen hast, trag Deinen Benutzernamen und Deine Email Adresse ein, und Pivot wird Dir Dein Passwort zuschicken.',
    'oops'              => 'Oops',
    'back'              => 'Zurück',
    'back_login'        => 'Zurück zur Anmeldung',
    'forgotten_pass_mail' => 'Dein vergessenes Passwort für Pivot \'%name%\' lautet: \n\n%pass%\n\nBitte vergiss es nicht gleich wieder... :)\n\nUm sich anzumelden klicke den folgenden Link:\n %link%',
    'registered'        => 'Du hast Dich als neuer Nutzer Registriert.\'%s\'',
    'reg_confirmation'  => 'Bestätigung der Registrierung',
    'reg_verify_short'  => 'Bestätige Deinen Account.',
    'reg_verify_long'   => 'Um Deinen Account zu bestätigen klicke bitte folgenden Link:\n %s',
    'reg_verification'  => 'Die E-mail Bestätigung wurde gesendet an %s. Innerhalb der nächsten Minute müsste bei Dir eine E-mail eingehen, in der Du Deinen Account bestätigen kannst.',
);

// A little tool to help you check if the file is correct..
if (count(get_included_files())<2) {

	$groups = count($lang);
	$total = 0;
	foreach ($lang as $langgroup) {
		$total += count($langgroup);
	}
	echo "<h2>Language file is correct!</h2>";
	echo "This file contains $groups groups and a total of $total labels.";

}

?>
