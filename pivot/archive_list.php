<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------

/* example usage:

Whereever you wish to include a list of the archives, include the following
bit of code in one of your PHP files:

<?php

$archive_format= "<a href='%url%'>%st_monname% %st_year%</a><br />";
$archive_weblog="my_weblog";

include "pivot/archive_list.php";

?>

If you omit the $archive_format, it will use the formatting you've set in the
weblog's configuration. If you don't know the weblog's internal name (for the
$archive_weblog setting), you can look it up in Administration � File Mappings.

*/

// --------------------

// Lamer protection
$currentfile = basename(__FILE__);
require dirname(__FILE__)."/lamer_protection.php";

chdir("pivot");
include "pv_core.php";

// suppress debug messages..
$Cfg['debug'] = 0;

if (isset($Weblogs[$archive_weblog])) {
	$Current_weblog = $archive_weblog;
} else {
	$Current_weblog = key($Weblogs);
}

if (isset($archive_format) && (strlen($archive_format)>1) ) {
	$Weblogs[$Current_weblog]['archive_linkfile'] = $archive_format;
}

echo snippet_archive_list();

?>
