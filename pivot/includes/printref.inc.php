<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under 
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------


$old_errrep = error_reporting(E_ERROR);

// lamer protection
if (strpos($pivot_path,"ttp://")>0) {	die('no');}
$scriptname = basename((isset($_SERVER['PATH_INFO'])) ? $_SERVER['PATH_INFO'] : $_SERVER['PHP_SELF']);
if ($scriptname=="printref.inc.php") { die('no'); }
if (!isset($_GET)) { 
	$checkvars = array_merge($HTTP_GET_VARS , $HTTP_POST_VARS, $HTTP_SERVER_VARS, $HTTP_COOKIE_VARS);
} else {
	$checkvars = array_merge($_GET , $_POST, $_SERVER, $_COOKIE);
}
if ( (isset($checkvars['pivot_url'])) || (isset($checkvars['log_url'])) || (isset($checkvars['pivot_path'])) ) {
	die('no');
}
// end lamer protection


// -- Functions --
if (!function_exists('cut')) {
	
	function cut($str, $maxlength) {
		
		$str = str_replace('http://', '', $str);
		$str = str_replace('www.', '', $str);
	
		if (strlen($str) > $maxlength) { $str = substr($str, 0, $maxlength). "&hellip;"; }
	
		return stripslashes($str);
	}

		
	function disp_title($text, $extra) {
		global $titles;
	
		if (strpos($text, "?")) { $text=substr($text,0, strpos($text, "?") ); }
		$text=str_replace("/index.php", "", $text);
		$text=str_replace("/index.html", "", $text);
		$text=str_replace("/index.htm", "", $text);
		$text=str_replace("/index.shtml", "", $text);
		if ((strlen($text) - strrpos($text, "/")) == 1 ) { $text=substr($text,0,(strlen($text)-1)); }
	
		if (isset($titles[$text])) {
			if ($extra) {
				return $titles[$text]." - ". stripslashes(htmlentities($text));
			} else {
				return $titles[$text];
			}
		} else {
			$text = stripslashes(htmlentities($text));
			return $text;
		}
	
	}
	
	function disp_href($text) {
		$text = stripslashes(htmlentities($text));
		return $text;
	}
	
	
	function add_title($key, $value) {
		global $titles;
	
		if (strpos($key, "?")) {
			$key=substr($key,0, strpos($key, "?") );
		}
		$key=str_replace("%7E", "~", $key);
		
		if ((strlen($key) - strrpos($key, "/")) == 1 ) {
			$key=substr($key,0,(strlen($key)-1));
		}
		
		$titles[$key]=$value;
	
	}
	
	function ignore($ref) {
		global $ignore;
	
		if (count($ignore)==0) { 
			return;
		}
		
		foreach($ignore as $check) {
			if (!(strpos($ref, trim($check))===FALSE)) { 
				return TRUE;
			}
		}
		return FALSE;
	}
	
	
	
} // end if(function_exists)









// Main..

if (file_exists(realpath($pivot_path). '/pv_core.php')) {
	include_once( realpath($pivot_path). '/pv_core.php');
}


// graphical markers for search engines..
$ident_graph['google'] = "<img src='".$pivot_url."pics/ico_g.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[G]' /> ";
$ident_graph['alltheweb'] = "<img src='".$pivot_url."pics/ico_av.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[Al]' /> ";
$ident_graph['vivisimo'] = "<img src='".$pivot_url."pics/ico_v.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[V]' /> ";
$ident_graph['altavista'] = "<img src='".$pivot_url."pics/ico_av.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[Av]' /> ";
$ident_graph['aol'] = "<img src='".$pivot_url."pics/ico_ao.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[Ao]' /> ";
$ident_graph['lycos'] = "<img src='".$pivot_url."pics/ico_l.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[L]' /> ";
$ident_graph['msn'] = "<img src='".$pivot_url."pics/ico_m.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[M]' /> ";
$ident_graph['mysearch'] = "<img src='".$pivot_url."pics/ico_my.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[My]' /> ";
$ident_graph['yahoo'] = "<img src='".$pivot_url."pics/ico_y.png' style='border: 0px; vertical-align: bottom;' width='16' height='16' alt='[Y]' /> ";


// textual markers for search engines
$ident['google'] = "[G] ";
$ident['alltheweb'] = "[Al] ";
$ident['vivisimo'] = "[V] ";
$ident['altavista'] = "[Av] ";
$ident['aol'] = "[Ao] ";
$ident['lycos'] = "[L] ";
$ident['msn'] = "[M] ";
$ident['mysearch'] = "[My] ";
$ident['yahoo'] = "[Y] ";



if ($Weblogs[$weblog]['lastref_format']!="") {
	$format = $Weblogs[$weblog]['lastref_format'];
} else {
	$format = "%hour24%:%minute% <a href='%url%'>%title%</a><br />";
}


$current_date= date("Y-m-d-H-i");

$rows = 0;
$list = "";


// get the file with the last referrers..
if (isset($lastreferrers)) {
	$aAll= array_reverse($lastreferrers);
} else {
	if (file_exists("db/ser_last_referrers.php")) {
		$aAll = array_reverse(load_serialize("db/ser_last_referrers.php", TRUE));
	} else {
		$aAll = array_reverse(load_serialize($pivot_path."db/ser_last_referrers.php", TRUE));
	}
}


// get the file with the titles and parse it.
if (file_exists($pivot_path."db/ser_referer_titles.php")) {
	$titles = load_serialize($pivot_path."db/ser_referer_titles.php", TRUE);
}

// get the file with the domains to ignore..
if (file_exists($pivot_path.'db/ignored_domains.txt.php')) {
	$ignore = file($pivot_path.'db/ignored_domains.txt.php');
} else if (file_exists($pivot_path.'db/ignored_domains.txt')) {
	$ignore = file($pivot_path.'db/ignored_domains.txt');
}




//initialize some variables..
$referrer_date = (isset($Cfg['referrer_date'])) ? $Cfg['referrer_date'] : "%hour24%.%minute%";

if (!isset($maxrows) || !is_numeric($maxrows) ) {
	if (isset($Weblogs[$Current_weblog]['lastref_amount'])  && ($Weblogs[$Current_weblog]['lastref_amount']>0) ) {
		$maxrows = $Weblogs[$Current_weblog]['lastref_amount'];
	} else {
		$maxrows = 15;
	}
}

if (!isset($maxlength) || !is_numeric($maxlength) ) {
	if (isset($Weblogs[$Current_weblog]['lastref_length'])  && 	($Weblogs[$Current_weblog]['lastref_length']>0) ) {
	$maxlength = $Weblogs[$Current_weblog]['lastref_length'];
	} else {
		$maxlength = 15;
	}
}


// -- main --

$dupes= Array();

$list = "";

for ($i=0;($i<count($aAll) && $rows<$maxrows);$i++)
{
	$one_row = $aAll[$i];

	// check if it's in the ignore domains list..
	if (ignore($one_row['referer'])) {
		continue;
	}
	
	$title=disp_title($one_row['referer'], FALSE);	
	
	if (strpos($one_row['referer'], '.google.')) { 
		preg_match("~q=([a-zA-Z0-9%+.-]*)~is", $one_row['referer'], $match);
		@$match = urldecode($match[1]);
		$match = str_replace("%22", "'", str_replace("+", " ", $match));
		$title = $ident['google'] . $match; 
		add_title($one_row['referer'],$title);
	}	

	else if (strpos($one_row['referer'], '.lycos.')) { 
		preg_match("~query=([a-zA-Z0-9%+.-]*)~is", $one_row['referer'], $match);
		@$match = urldecode($match[1]);
		$match = str_replace("%22", "'", str_replace("+", " ", $match));
		$title = $ident['lycos'] . $match; 
		add_title($one_row['referer'],$title);
	}	

	else if (strpos($one_row['referer'], 'aolsearch.')) { 
		preg_match("~query=([a-zA-Z0-9%+.-]*)~is", $one_row['referer'], $match);
		@$match = urldecode($match[1]);
		$match = str_replace("%22", "'", str_replace("+", " ", $match));
		$title = $ident['aol'] . $match;
		add_title($one_row['referer'],$title);
	}	

	else if (strpos($one_row['referer'], 'mysearch.')) { 
		preg_match("~searchfor=([a-zA-Z0-9%+.-]*)~is", $one_row['referer'], $match);
		@$match = urldecode($match[1]);
		$match = str_replace("%22", "'", str_replace("+", " ", $match));
		$title = $ident['mysearch'] . $match;
		add_title($one_row['referer'],$title);
	}	

	else if (strpos($one_row['referer'], 'search.msn')) { 
		preg_match("~q=([a-zA-Z0-9%+.-]*)~is", $one_row['referer'], $match);
		@$match = urldecode($match[1]);
		$match = str_replace("%22", "'", str_replace("+", " ", $match));
		$title = $ident['msn'] . $match;
		add_title($one_row['referer'],$title);
	}	

	else if (strpos($one_row['referer'], '.altavista.')) { 
		preg_match("~q=([a-zA-Z0-9%+.-]*)~is", $one_row['referer'], $match);
		@$match = urldecode($match[1]);
		$match = str_replace("%22", "'", str_replace("+", " ", $match));
		$title = $ident['altavista'] . $match;
		add_title($one_row['referer'],$title);
	}	
	
	else if (strpos($one_row['referer'], '.yahoo.')) { 
		preg_match("~p=([a-zA-Z0-9%+.-]*)~is", $one_row['referer'], $match);
		@$match = urldecode($match[1]);
		$match = str_replace("%22", "'", str_replace("+", " ", $match));
		$title = $ident['yahoo'] . $match;
		add_title($one_row['referer'],$title);
	}	


	if (strlen($one_row['referer']) && (!strpos($one_row['referer'], $_SERVER['HTTP_HOST'])) && (!in_array($title, $dupes)) )
	{
		$date= format_date($one_row['date'], $referrer_date);
		if ( (strlen($date)>3) && (strlen($title)>3) ) {
			$item = $format;
			
			$item = str_replace("%url%", disp_href($one_row['referer']), $item);
			$item = str_replace("%title%", cut($title, $maxlength), $item);
			$item = format_date($one_row['date'], $item);
			
			$list .= $item;
			$dupes[]=$title;
		}
		$rows++;

	}
}

$Current_weblog = $weblog;

// maybe add autoredirects
if (isset($Weblogs[$Current_weblog]['lastref_redirect'])  && ($Weblogs[$Current_weblog]['lastref_redirect']==1) ) {
	$redirect = TRUE;
} else {
	$redirect = FALSE;
}

$list = targetblank($list, $redirect);

if (isset($Weblogs[$Current_weblog]['lastref_graphic'])  && ($Weblogs[$Current_weblog]['lastref_graphic']==1) ) {

	//perhaps use the small icons for search engines.
	$list = str_replace($ident, $ident_graph, $list);

}

print($list);

error_reporting($old_errrep);





?>