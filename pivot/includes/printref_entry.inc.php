<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under 
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------



$this_ident1= "/entry.php?id=".$db->entry['code'];

list($yr,$mo,$da,$ho,$mi)=split("-",$db->entry['date']);
$name = safe_string($db->entry['title']);
$this_ident2 = "archive/$yr/$mo/$da/".$name;

//echo "this page is: $this_ident1 or $this_ident2 ";


$old_errrep = error_reporting(E_ERROR);

$current_date= date("Y-m-d-H-i");

$rows = 0;
//$list = "";


// get the file with the last referrers..
if (isset($lastreferrers)) {
	$aAll= array_reverse($lastreferrers);
} else {

	if (file_exists("db/ser_last_referrers.php")) {
		$aAll = array_reverse(load_serialize("db/ser_last_referrers.php", TRUE));
	} else {
		$aAll = array_reverse(load_serialize($pivot_dir."db/ser_last_referrers.php", TRUE));
	}
}


// get the file with the titles and parse it.
$titles = load_serialize($pivot_dir."db/ser_referer_titles.php", TRUE);

//echo("[titles: " . count($titles). "]<br />\n" );


//initialize some variables..
$referrer_date = (isset($Cfg['referrer_date'])) ? $Cfg['referrer_date'] : "%hour24%.%minute%";

if (isset($HTTP_GET_VARS['show'])) {
	$maxrows = $HTTP_GET_VARS['show'];
} else {
	if (isset($Cfg['referrer_show'])) {
		$maxrows= $Cfg['referrer_show'];	
	} else {
		$maxrows = 16;
	}
}

if (isset($HTTP_GET_VARS['maxlength'])) {
  $maxlength = $HTTP_GET_VARS['maxlength'];
} else {
	if (isset($Cfg['referrer_maxlength'])) {
		$maxlength= $Cfg['referrer_maxlength'];	
	} else {
		$maxlength = 60;
	}
}


function cut($str)
{
	global $maxlength; 

	$str = str_replace('http://', '', $str);
	$str = str_replace('www.', '', $str);

	if (strlen($str) > $maxlength) { $str = substr($str, 0, $maxlength). ".."; }

	return stripslashes($str);
}


function tidy_refer($text) {

	if (strpos($text, "?")) { $text=substr($text,0, strpos($text, "?") ); }
	$text=str_replace("/index.php", "", $text);
	$text=str_replace("/index.html", "", $text);
	$text=str_replace("/index.htm", "", $text);
	$text=str_replace("/index.shtml", "", $text);
	if ((strlen($text) - strrpos($text, "/")) == 1 ) { $text=substr($text,0,(strlen($text)-1)); }
	return $text;
}

function disp_title($text, $extra) {
	global $titles;

	if (isset($titles[$text])) {
		if ($extra) {
			return $titles[$text]." - ". stripslashes(htmlentities($text));
		} else {
			return $titles[$text];
		}
	} else {
		$text = stripslashes(htmlentities($text));
		return $text;
	}

}




function disp_href($text) {
	$text = stripslashes(htmlentities($text));
	return $text;
}


function add_title($key, $value) {
	global $titles;

	if (strpos($key, "?")) {
		$key=substr($key,0, strpos($key, "?") );
	}
	$key=str_replace("%7E", "~", $key);
	
	if ((strlen($key) - strrpos($key, "/")) == 1 ) {
		$key=substr($key,0,(strlen($key)-1));
	}
	
	$titles[$key]=$value;

}

$dupes= Array();

//  && $rows<$maxrows

for ($i=0;($i<count($aAll));$i++)
{
	$one_row = $aAll[$i];

	


	if (strpos($one_row['referer'], '.google.')) { 
		preg_match("~q=([a-zA-Z0-9%+]*)~is", $one_row['referer'], $match);
		@$match=urldecode($match[1]);
		$match=str_replace("+", " ", $match);
		$match=str_replace("%22", "'", $match);
		$title="[G] ".$match; 
		add_title($one_row['referer'],$title);
	}	

	if (strpos($one_row['referer'], '.lycos.')) { 
		preg_match("~query=([a-zA-Z0-9%+]*)~is", $one_row['referer'], $match);
		@$match=urldecode($match[1]);
		$match=str_replace("+", " ", $match);
		$match=str_replace("%22", "'", $match);
		$title="[L] ".$match; 
		add_title($one_row['referer'],$title);
	}	

	// http://search.msn.be/results.asp?q=dagboek%2Butrecht&origq=dagboek&FORM=IE4&v=1&cfg=SMCSP&nosp=0&thr=
	if (strpos($one_row['referer'], 'search.msn')) { 
		preg_match("~q=([a-zA-Z0-9%+]*)~is", $one_row['referer'], $match);
		@$match=urldecode($match[1]);
		$match=str_replace("+", " ", $match);
		$match=str_replace("%22", "'", $match);
		$title="[M] ".$match; 
		add_title($one_row['referer'],$title);
	}	

	// http://nl.altavista.com/web/results?q=Sanne+Lesley&kgs=1&kls=1&avkw=qtrp
	if (strpos($one_row['referer'], '.altavista.')) { 
		preg_match("~q=([a-zA-Z0-9%+]*)~is", $one_row['referer'], $match);
		@$match=urldecode($match[1]);
		$match=str_replace("+", " ", $match);
		$match=str_replace("%22", "'", $match);
		$title="[A] ".$match; 
		add_title($one_row['referer'],$title);
	}	
	
	//http://search.yahoo.com/search?p=lesley+sanne
	if (strpos($one_row['referer'], '.yahoo.')) { 
		preg_match("~p=([a-zA-Z0-9%+]*)~is", $one_row['referer'], $match);
		@$match=urldecode($match[1]);
		$match=str_replace("+", " ", $match);
		$match=str_replace("%22", "'", $match);
		$title="[Y] ".$match; 
		add_title($one_row['referer'],$title);
	}	


	if (strlen($one_row['referer']) &&	(!strpos($one_row['referer'],  $HTTP_SERVER_VARS['HTTP_HOST'])) )
	{
		
		$refer = tidy_refer($one_row['referer']);

		if ( strlen($one_row['referer'])>3 && ( strpos($one_row['request_uri'], $this_ident1)  || strpos($one_row['request_uri'], $this_ident2) ) ) {
				@$list[$refer]++;
		}

	}
}




if (isset($list)) {
	asort($list);
	$list = array_reverse($list);


	$i=0;
	$output="";

	foreach($list as $ref => $count) {

			$title=disp_title($ref, FALSE);

			//$date= format_date($one_row['date'], $referrer_date);
			//$list .= $date;
			$output .= "<a ";
			$output .= "title=\"".disp_title($ref, TRUE)."\" ";
			$output .= "href=\"".disp_href($ref)."\">";
			$output .= cut($title);
			$output .= "</a> ($count)<br />";
			
			$i++;
			if ($i>10) {break; }

	}

} else {

	$output="";
}

error_reporting($old_errrep);

?>