<?php
// +----------------------------------------------------------------------+
// | gTrackBack - PHP port of Standalone TrackBack                        |
// | Author: Timo Gnambs <timo@gnambs.at>                                 |
// +----------------------------------------------------------------------+
// +----------------------------------------------------------------------+
// | based on: tb-standalone - Standalone TrackBack v1.3                  |
// | Author: Benjamin & Mena Trott                                        |
// |         (see http://www.movabletype.org/docs/tb-standalone.html)     |
// +----------------------------------------------------------------------+
// +----------------------------------------------------------------------+
// | License: Artistic License                                            |
// |          (see http://www.perl.com/language/misc/Artistic.html)       |
// +----------------------------------------------------------------------+
// v0.41 (2004/01/31)




##### CONFIG start #####

$data_dir = "../../db/trackback";        //path to trackback files to be generated
$rss_dir = "./rss";                      //path to rss files to be generated
$generate_rss = 0;                       //create rss file
$rss_length = 15;                        //max number of rss items
$rss_link = "http://your.blog.com/";     //link to your blog for rss file
$blog_name = "Name of your blog";        //name of your blog
$header = "header.txt";                  //or false (-> uses built in default function)
$footer = "footer.txt";                  //or false (-> uses built in default function)
$password = "**CHANGE**";                //password to delete trackbacks
$email = 'your@email.com';               //if set, an email will be sent to this address

$lang_data = array(
                "noid"      => "No TrackBack ID (tb_id)",
                "nourl"     => "No URL (url)",
                "noping"    => "No ping URL",
                "tburl"     => "TrackBack URL for this entry",
                "delete"    => "[DELETE]",
                "yoursite"  => "Is this your site?",
                "todelete"  => " to delete pings.",
                "login"     => "Log in",
                "logout"    => "Log out",
                "sendping"  => "Send a ping",
                "unaut"     => "You are not authorized",
                "pinged"    => "Ping successfully sent",
                "notpinged" => "Error: Couldn�t ping url",
                "invalid"   => "Invalid login",
                "loggedout" => "Logged out",
                "tracked"   => "Tracked",
                "pass"      => "Password",
                "email_subject" => "[Trackback] new Trackback",
                "email_txt" => "Someone left a new trackback on your website:"
            );

#### CONFIG end ####




$me = 'http://'.getVar("HTTP_HOST", "server").getVar("PHP_SELF", "server");
$mode = getVar("__mode");
$tb_id = getVar("tb_id");

switch($mode) {
    
    /* got pinged -> save ping and answer */
    default:
        if(!$tb_id = mungeTbId($tb_id)) respondExit($lang_data["noid"], 1);
        
        $i = array(
                "title"     => urldecode(getVar("title")),
                "excerpt"   => urldecode(getVar("excerpt")),
                "url"       => urldecode(getVar("url")),
                "blog_name" => urldecode(getVar("blog_name")),
                "timestamp" => time()
              );

        if(empty($i["url"])) respondExit($lang_data["nourl"], 1);
        if(strlen($i["excerpt"]) > 255) $i["excerpt"] = substr($i["excerpt"], 0, 251)."...";

        /* remove line break */
        foreach($i as $key=>$value) {
            $i[$key] = str_replace("\n", "<br />", $value);
            $i[$key] = str_replace("\r", "", $i[$key]);            
        }
        
        /* save ping to storage file */
        $data = unserialize(loadData($tb_id, $data_dir));
        if(!is_array($data)) $data = array();
        array_unshift($data, $i);

        storeData($tb_id, serialize($data), $data_dir);
        
        /* build rss file */
        if($generate_rss) {
            $rss = generateRss($tb_id, $data, $rss_length);
            storeData($tb_id, $rss, $rss_dir);
        }
       
        /* send email */
        if($email) sendEmail(& $email, & $i);   
       
        /* success msg for sender  */
        respondExit();
    break;
        
    /* display trackback url and existing trackbacks */
    case "list":
        if(!$tb_id = mungeTbId($tb_id)) respondExit($lang_data["noid"], 1);
        
        print $header ? fromFile($header) : tplHeader();
        printf("<div class=\"url\">%s:<div class=\"ping-url\">%s?tb_id=%s</div></div>",
                    $lang_data["tburl"],
                    $me,
                    $tb_id
               );
        
        /* load existing tbs */
        $data = unserialize(loadData($tb_id, $data_dir));
        if(!is_array($data)) $data = array();
        
        $logged_in = isLoggedIn();
        
        for($i = 0; $i < sizeof($data); $i++) {
            printf(tplTrackback(),
                    $data[$i]["url"],
										stripslashes($data[$i]["title"]),
										!empty($data[$i]["blog_name"]) ? stripslashes($data[$i]["blog_name"]) : "",
										!empty($data[$i]["excerpt"]) ? stripslashes($data[$i]["excerpt"]) : "", 
                    strftime("%B %d, %Y %I:%M %p", $data[$i]["timestamp"]),
                    $logged_in ? "<a class=\"delete\" href=\"$me?__mode=delete&tb_id=$tb_id&index=$i\">".$lang_data["delete"]."</a>" : ""
            );
        }
        
        if(!$logged_in) {
            printf('<div align="right">[%s <a href="%s?__mode=login&amp;tb_id=%s">%s</a>%s]</div>',
                    $lang_data["yoursite"],
                    $me,
                    $tb_id,
                    $lang_data["login"],
                    $lang_data["todelete"]
            );
        } else {
            printf('<div align="right">[<a href="%s?__mode=send_form&amp;tb_id=%s">%s</a>] [<a href="%s?__mode=logout&amp;tb_id=%s">%s</a>]</div>',
                    $me,
                    $tb_id,
                    $lang_data["sendping"],
                    $me,
                    $tb_id,
                    $lang_data["logout"]
            );
        }
        
        print $footer ? fromFile($footer) : tplFooter();
    break;
    
    case "delete":
        if(!isLoggedIn()) die($lang_data["unaut"]);
        if(!$tb_id = mungeTbId($tb_id)) respondExit($lang_data["noid"], 1);
    
        $data = unserialize(loadData($tb_id, $data_dir));
        $index = getVar("index");
        array_splice($data, empty($index) ? 0 : $index, 1);
        storeData($tb_id, serialize($data), $data_dir);
        
        /* build rss file anew */
        if($generate_rss) {
            $rss = generateRss($tb_id, $data, $rss_length);
            storeData($tb_id, $rss, $rss_dir);
        }
        
        header("Location: $me?__mode=list&tb_id=$tb_id");
    break;
    
    /* display rss file */
    case "rss":
        if(!$tb_id = mungeTbId($tb_id)) respondExit($lang_data["noid"], 1);
        respondExit(loadData($tb_id, $rss_dir), false);
    break;
    
    /* send ping to url */
    case "send_ping":
        $qs = array(
            "title"     => urlencode(stripslashes(getVar("title", "post"))),
            "url"       => urlencode(stripslashes(getVar("url", "post"))),
            "blog_name" => urlencode(stripslashes(getVar("blog_name", "post"))),
            "excerpt"   => urlencode(stripslashes(getVar("excerpt", "post")))
        );
              
        $ping = stripslashes(getVar("ping_url", "post"));
        $ping = parse_url($ping);

        /* no url to ping -> stop */
        if(empty($ping)) {
            print $header ? fromFile($header) : tplHeader();
            print pingFormExit($lang_data["noping"], $qs);
            print $footer ? fromFile($footer) : tplFooter();
            break;
        }

        /* send ping */
        unset($c);
        while(list($key,$value) = each($qs)) {
            if(!empty($c)) $c .= "&";
            $c .= "$key=".rawurlencode($value);
        }
        //$res = sendToHost($ping["host"], $ping["path"], "$c&".$ping["query"]);
        $res = sendToHost($ping["host"], $ping["path"].'?'.$ping["query"], $c);
        
        $qs["ping_url"] = $ping["host"].$ping["path"]."?".$ping["query"];
        if(!preg_match("#^http://|https://#", $qs["ping_url"])) $qs["ping_url"] = "http://".$qs["ping_url"];
        if(!$res[0]) {
            print $header ? fromFile($header) : tplHeader();
            print pingFormExit("HTTP error: ".$res[1], $qs);
            print $footer ? fromFile($footer) : tplFooter();
            break;
        }
        
        $e = preg_match("#<error>(\d+)</error>#s", $res[1], $matches);
        print $header ? fromFile($header) : tplHeader();
        print ($e && !$matches[1][0]) ? pingFormExit($lang_data["pinged"]) : pingFormExit($lang_data["notpinged"], $qs);
        print $footer ? fromFile($footer) : tplFooter();
    break;
    
    /* display form to send tb */
    case "send_form":
        if(!isLoggedIn()) die("<a href=\"$me?__mode=login\">".$lang_data["unaut"]."</a>");
        print $header ? fromFile($header) : tplHeader();
        print pingFormExit();
        print $footer ? fromFile($footer) : tplFooter();
    break;
    
    /* display login form */
    case "login":
        print $header ? fromFile($header) : tplHeader();
        print loginForm($tb_id);
        print $footer ? fromFile($footer) : tplFooter();
    break;
    
    /* login */
    case "do_login":
        $key = getVar("key", "post");

        /* invalid login */
        if($key != $password) {
            print $header ? fromFile($header) : tplHeader(); 
            print loginForm($tb_id, $lang_data["invalid"]);
            
        /* valid login */
        } else {
            setcookie("key", md5($key));  
            if(!empty($tb_id)) {
                header("Location: $me?__mode=list&tb_id=$tb_id");
                exit;
            } else {
                print $header ? fromFile($header) : tplHeader(); 
                print "Logged in";
            }
        }
        
        print $footer ? fromFile($footer) : tplFooter();
    break;
    
    /* logout */
    case "logout":
        /* delete cookie*/
        setcookie("key", "", time() - 3600);
        
        /* return to trackbacks */
        if(!empty($tb_id)) {
            header("Location: $me?__mode=list&tb_id=$tb_id");
            exit;
        /* display form again*/
        } else {
            print $header ? fromFile($header) : tplHeader(); 
            print loginForm($tb_id, $data_lang["loggedout"]);
            print $footer ? fromFile($footer) : tplFooter(); 
        }
    break;
}


/**
* Checks current PHP version against a given string
*   
* @authors  (c) July 2002 <akov@relex.ru>, modified August 2002 Timo Gnambs <timo@gnambs.at>
* @param   string   $version    test version (e.g. 4.1.1)
* @return  bool
*/

function checkPhpVersion($version) {
    $testVer = intval(str_replace(".", "", $version));
    $curVer = intval(str_replace(".", "", phpversion()));
    if($curVer >= $testVer) return true;
    return false;
}


/**
* Determine var depending on php version
*
* @param   string   $name       var name
* @param   string   $method     gpc (get, post) / server / cookie
* @return  mixed
* @static  bool     $mode
* @see     checkPhpVersion()
*/

function getVar($name, $method = "gp") {
    global $HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_SERVER_VARS, $HTTP_COOKIE_VARS;
    static $mode;

    $mode = checkPhpVersion("4.1.0");

    if($mode) {
        switch(strtolower($method)) {
            default:
            case "gp":
            case "cookie":
                return $_REQUEST[$name];
                break;
            case "post":
                return $_POST[$name];
                break;
            case "server":
                return $_SERVER[$name];
                break;
        }
    } else {
        switch(strtolower($method)) {
            default:
            case "gp":
                return !empty($HTTP_GET_VARS[$name]) ? $HTTP_GET_VARS[$name] : $HTTP_POST_VARS[$name];
                break;
            case "post":
                return $HTTP_POST_VARS[$name];
                break;
            case "server":
                return $HTTP_SERVER_VARS[$name];
                break;
            case "cookie":
                return $HTTP_COOKIE_VARS[$name];
                break;
        }
    }
}


/**
* Transform id: remove special chars
*
* @param  string $tb_id     trackback id
* @return string            modified trackback id
*/

function mungeTbId($tb_id) {
    if(empty($tb_id)) return false;
    else return $tb_id = preg_replace("#\W#", "_", $tb_id);
}


/**
* Generates RSS file
* 
* @param   string   $tb_id     trackback id
* @param   array    $data      already existing rss entries
* @param   int      $limit     total number of rss entries to keep
* @return  string
* @global  string   $rss_link  link of rss file
*/

function generateRss($tb_id, $data, $limit = 15) {
    global $rss_link;
    
    $rss = '<rss version="0.92">
                <channel>
            ';
    $rss .= "<title>TB: $tb_id</title>\n";
    $rss .= "<link>$rss_link</link>\n";

    $max = $limit ? ((sizeof($data) > $limit) ? $limit : sizeof($data)) : sizeof($data);
    for($i = 0; $i < $max; $i++) {
        $rss .= sprintf("<item>%s%s%s</item>\n",
                            xml("title", $data[$i]["title"]),
                            xml("link", $data[$i]["url"]),
                            xml("description", $data[$i]["excerpt"])
                );
    }
    $rss .= '
                </channel>
            </rss>';
    return $rss;
}


/**
* Print result and exit
*
* @param   bool    $error     print error
* @param   string  $msg       addtional text to display
* @param   bool
*/

function respondExit($msg = "", $error = false) {
    header("Content-Type: text/xml\n\n");
    print "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
    
    print "<response>\n";
    if($error) printf("<error>1</error>\n%s\n",
                xml("message", $msg)
               );
    else printf("<error>0</error>\n%s",
                $msg
         );
    print "</response>\n";
    
    exit;
    return false;
}


/**
* Load existing tb data from file 
*
* @param   string   $tb_id     trackback id
* @return  string
* @see     from_file()
*/

function loadData($tb_id, $path) {
    $tb_file = sprintf("%s%s%s.stor",
                        $path,
                        (substr(PHP_OS, 0, 3) == "WIN") ? chr(92) : chr(47),
                        $tb_id
               );
    $data = trim(fromFile($tb_file));
    return $data;
}


/**
* Save tb object to file
*
* @param   string   $tb_id     trackback id
* @param   mixed    $data      object to store
* @param   string   $file      file to save to
* @return  bool
*/

function storeData($tb_id, $data, $file) { 
    $tb_file = sprintf("%s%s%s.stor",
                        $file,
                        (substr(PHP_OS, 0, 3) == "WIN") ? chr(92) : chr(47),
                        $tb_id
               );
    $fp = fopen($tb_file, "w");
    $result = fputs($fp, $data);
    fclose($fp);
    return $result ? true : false;
}


/**
* Returns content of a file
*
* @param   string   $file    filename
* @return  string
*/

function fromFile($file) {
    return file_exists($file) ? @implode("\n", file($file)) : "";
}


/**
* Check if user is logged in
*
* @return   bool
* @global   $password
*/

function isLoggedIn() {
    global $password;
    $key = getVar("key", "cookie");
    return ($key === md5($password)) ? true : false;
}


/**
* Build template for trackback entry
*
* @return   string
* @global   array   $lang_data
*/

function tplTrackback() {
    global $lang_data;
    
    return '
        <a target="new" href="%s">%s</a><br />
        <div class="head">&#187;  %s</div>
        <div class="excerpt">"%s"</div>
        <div class="footer">'.$lang_data["tracked"].': %s %s</div>
    ';
}


/**
* Builds login form
*
* @param   string   $tb_id  trackback id
* @param   string   $msg    optional message
* @return  string
* @global  array    $lang_data
*/

function loginForm($tb_id, $msg = "") {
    global $lang_data;
    
    if(!empty($msg)) $str = "<p>$msg</p>";
    $str .= '
        <form method="post">
            <input type="hidden" name="__mode" value="do_login" />
            '.$lang_data["pass"].': <input name="key" type="password" />
            <input type="submit" value="Log in" />
        ';
    $str .= sprintf('<input type="hidden" name="tb_id" value="%s" />',
                $tb_id
            );
    return $str .= "</form>";
}


/**
* Builds form to ping external url
*
* @param   string   $msg      additional message to display
* @param   array    $values   values of input fields
* @return  string
* @global  $blog_name
*/

function pingFormExit($msg = "", $values = array()) {
    global $blog_name;
    
    if(!empty($msg)) $str = "<p>$msg</p>";
    $str .= '
        <h2>Send a TrackBack ping</h2>
        <form method="post">
            <input type="hidden" name="__mode" value="send_ping" />
            <table border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td><label for="ping_url">TrackBack Ping URL</label>:</td>
        ';
    $str .= sprintf("<td><input id=\"ping_url\" name=\"ping_url\" size=\"60\" value=\"%s\" /></td>",
                    urldecode(stripslashes($values["ping_url"]))
            );
    $str .= '
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><label for="title">Title:</label</td>
            ';
    $str .= sprintf("<td><input id=\"title\" name=\"title\" size=\"35\" value=\"%s\" /></td>",
                    htmlentities(urldecode(stripslashes($values["title"])))
    );
    $str .= '
                </tr>
                <tr>
                    <td><label for="blog_name">Blog name:</label></td>
            ';
    $str .= sprintf("<td><input id=\"blog_name\" name=\"blog_name\" size=\"35\" value=\"%s\" /></td>",
                    !empty($values["blog_name"]) ? htmlentities(urldecode(stripslashes($values["blog_name"]))) : $blog_name
    );
    $str .= '
                </tr>
                <tr>
                    <td><label for="excerpt">Excerpt:</label></td>
            ';
    $str .= sprintf("<td><input id=\"excerpt\" name=\"excerpt\" size=\"60\" maxlength=\"250\" value=\"%s\" /></td>",
            htmlentities(urldecode(stripslashes($values["excerpt"])))
    );
    
    $str .= '
                </tr>
                <tr>
                    <td><label for="url">Permalink URL:</label></td>
            ';
    $str .= sprintf("<td><input id=\"url\" name=\"url\" size=\"60\" value=\"%s\" /></td>",
            urldecode(stripslashes($values["url"]))
    );
    $str .= '
                </tr>
            </table>
            <input type="submit" value="Send">
        </form>
    ';
    return $str;
}


/**
* Page header
*
* @return   string
*/

function tplHeader() {
    return '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html>
          <head>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
            <title>TrackBack Display</title>
            <style type="text/css">
                body            { background-color:#FFF; margin: 0px 0px 0px 0px; padding: 0px 10px 0px 10px; color:#333; text-align:center; }
                A               { font-family:verdana, arial; font-size:10px; color: #333; text-decoration: underline; background-color:#FFF; } 
                A:link		{ color: #333; text-decoration: underline; background-color:#FFF;} 
                A:visited	{ color: #333; text-decoration: underline; background-color:#FFF;} 
                A:active	{ color: #CC9966; background-color:#FFF;} 
                A:hover		{ color: #CC9966; background-color:#FFF;} 
                A.delete        { font-family:verdana, arial; font-size:10px; color: #CC3300;  background-color:#FFF; font-weight:bold;} 
                A.delete:link	{ color: #CC3300; background-color:#FFF;} 
                A.delete:visited{ color: #CC3300; background-color:#FFF;} 
                A.delete:active	{ color: #CC9966; background-color:#FFF;} 
                A.delete:hover	{ color: #CC9966; background-color:#FFF;} 
                #body           { background-color:#FFF; font-family:georgia, verdana, arial, sans-serif; font-size:11px; line-height:15px; color:#333; padding:10px; text-align:left; }
                .ping-url, .head { font-family:verdana, arial, sans-serif; color:#333; font-size:10px; padding-bottom:5px; }
                .footer         { font-family:verdana, arial, sans-serif; color:#333; font-size:10px; padding-bottom:5px; margin-bottom:15px; }
                .excerpt        { font-family:georgia, verdana, arial, sans-serif; color:#333; font-size:11px; padding-bottom:5px; padding-left:10px; padding-right:10px; }
                .url            { font-family:georgia, verdana, arial, sans-serif; color:#333; background-color:#EEE; font-size:11px; padding:5px; margin-bottom: 15px; border-top: 1px solid #CCC; border-bottom: 1px solid #CCC; }
            </style>
          </head>
          <body>
            <div id="body">  
    ';
}


/**
* Page footer
*
* @return   string
*/

function tplFooter() {
    return '
            <hr size="1" color="#CCC">	
            <div align="center" class="header">
                <a class="trackback" href="http://www.movabletype.org/docs/mtmanual_trackback.html#trackback" target="_blank">What is TrackBack?</a>
            </div>
          </body>
        </html>
    ';
}


/**
* Send data to host
*
* @param   string   $host   hostname
* @param   string   $path   file path
* @param   string   $data   data to send
* @return  array
*/

function sendToHost($host, $path, $data) {
    if(!$fp = fsockopen($host, 80, $errno, $errnum)) {
      return array(false, "$errstr ($errnum)");
    
    } else {
        fputs($fp, "POST $path HTTP/1.1\r\n");
        fputs($fp, "Host: $host\r\n");
        fputs($fp, "User-Agent: gTrackback\r\n");
        fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
        fputs($fp, "Content-length: ". strlen($data) ."\r\n");
        fputs($fp, "Connection: close\n\n");
        fputs($fp, $data);
        while(!feof($fp)) {
            $res .= fgets($fp, 128);
        }
        fclose($fp);
        return array(true, $res);
    }
}


/**
 * Send an email to specified address
 *
 * @param  string $email        email address
 * @param  array  $data         trackback data
 * @global array  $lang_data
 * @global string $me
 */
function sendEmail($email, $data) {
    global $lang_data, $me;
    
    $headers = "From: <$email>\n";
    $headers .= "Reply-To: <$email>\n";
    $headers .= "X-Sender: <$email>\n";
    $headers .= "X-Mailer: gTrackback\n"; //mailer
    $headers .= "X-Priority: 3\n"; //1 UrgentMessage, 3 Normal
    $headers .= "Return-Path: <$email>\n";
    $headers .= "MIME-Version: 1.0 \n";
    $headers .= "Content-type: text/plain \n";


    $msg = sprintf("%s: %s\n\nBlog: %s\nTitle: %s\nExcerpt: %s\nLink: %s\n",
                    $lang_data['email_txt'],
                    $me,
                    strip_tags($data['blog_name']),
                    strip_tags($data['title']),
                    strip_tags($data['excerpt']),
                    strip_tags($data['url'])
    );

    mail($email, $lang_data['email_subject'], $msg, $headers);
}


/**
* Build xml tag
*
* @param   string   $tag     xml tag name
* @param   string   $value   value of tag
* @return  string
*/

function xml($tag, $value) {
    return sprintf("<%s>%s</%s>\n",
                    $tag,
                    htmlspecialchars($value),
                    $tag
            );
}
?>
