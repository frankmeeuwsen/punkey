<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under 
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------


// set the path:
$emot_path= "includes/emot/";

// standard ones:
//$emot[':)']="e_01.gif";
$emot[':-)']="e_01.gif";
//$emot[':(']="e_18.gif";
$emot[':-(']="e_18.gif";
//$emot[':I']="e_86.gif";
$emot[':-I']="e_86.gif";
//$emot[':D']="e_52.gif";
$emot[':-D']="e_52.gif";
$emot[':-p']="e_69.gif";
$emot[':-P']="e_69.gif";
//$emot[':@']="e_13.gif";
$emot[':-@']="e_13.gif";
//$emot[':S']="e_103.gif";
$emot[':-S']="e_103.gif";
$emot[';)']="e_121.gif";
$emot[';-)']="e_121.gif";
//$emot[':.']="e_112.gif";
$emot[':-.']="e_112.gif";
$emot[':,']="e_112.gif";
$emot[':-,']="e_112.gif";
$emot[":'("]="e_28.gif";
$emot[":'-("]="e_28.gif";
$emot[":�-("]="e_28.gif";
$emot[":,("]="e_28.gif";
$emot[":,-("]="e_28.gif";
$emot[":')"]="e_10.gif";
$emot[":'-)"]="e_10.gif";
$emot[":'D"]="e_11.gif";
$emot[":'-D"]="e_11.gif";
//$emot[':?']="e_129.gif";
$emot[':-?']="e_129.gif";

// special ones:
$emot['8-O']="e_43.gif";
$emot['X-S']="e_26.gif";
$emot['[-(']="e_168.gif";
$emot['(joker)']="e_75.gif";
$emot[':0)']="e_75.gif";
$emot['(jokerp)']="e_92.gif";
$emot[':0p']="e_92.gif";
$emot['(devil)']="e_41.gif";
$emot['(devilh)']="e_58.gif";
$emot['(angel)']="e_07.gif";
$emot['(angelh)']="e_24.gif";
$emot['(cow)']="e_73.gif";
$emot['3:-O']="e_73.gif";
$emot['(monkey)']="e_48.gif";
$emot['(pig)']="e_31.gif";

// and some more :)
$emot['>-D']="e_02.gif";
$emot[':-/']="e_03.gif";
$emot['(blush)']="e_04.gif";
$emot['(santa)']="e_05.gif";
$emot[':-\\']="e_06.gif";
$emot['l-)']="e_08.gif";
$emot['X-(']="e_09.gif";
$emot['%-)']="e_12.gif";
$emot['I-)']="e_14.gif";
$emot[':-[']="e_15.gif";
$emot['>-(']="e_16.gif";
$emot['>-)']="e_17.gif";
$emot['(h)']="e_53.gif";


?>