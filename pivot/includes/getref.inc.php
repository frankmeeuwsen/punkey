<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------


$old_errrep = error_reporting(E_ERROR);

// lamer protection
if (strpos($pivot_path,"ttp://")>0) {	die('no');}
$scriptname = basename((isset($HTTP_SERVER_VARS['PATH_INFO'])) ? $HTTP_SERVER_VARS['PATH_INFO'] : $HTTP_SERVER_VARS['PHP_SELF']);
if ($scriptname=="getref.inc.php") { die('no'); }
if (!isset($_GET)) {
	$checkvars = array_merge($HTTP_GET_VARS , $HTTP_POST_VARS, $HTTP_SERVER_VARS, $HTTP_COOKIE_VARS);
} else {
	$checkvars = array_merge($_GET , $_POST, $_SERVER, $_COOKIE);
}
if ( (isset($checkvars['pivot_url'])) || (isset($checkvars['log_url'])) || (isset($checkvars['pivot_path'])) ) {
	die('no');
}
// end lamer protection

DEFINE('INPIVOT', TRUE);

if (file_exists(realpath($pivot_path). '/pvlib.php')) {
	include_once( realpath($pivot_path). '/pvlib.php');
}
if (file_exists(realpath($pivot_path). '/modules/module_lang.php')) {
	include_once( realpath($pivot_path). '/modules/module_lang.php');
}

// Start the timer:
$starttime=getmicrotime();

LoadDefLanguage();


function path(){
	global $HTTP_SERVER_VARS;

	// Xitami does not support ['path_info'], so we set it for those users.
	if (!(isset($HTTP_SERVER_VARS['PATH_INFO'])) || (strlen($HTTP_SERVER_VARS['PATH_INFO'])<2) ) {
		if ( (isset($HTTP_SERVER_VARS['SCRIPT_NAME'])) && (strlen($HTTP_SERVER_VARS['SCRIPT_NAME'])>2) ) {
			$HTTP_SERVER_VARS['PATH_INFO']=$HTTP_SERVER_VARS['SCRIPT_NAME'];
		}
	}
	$current_path = (isset($HTTP_SERVER_VARS['PATH_INFO'])) ? $HTTP_SERVER_VARS['PATH_INFO'] : $HTTP_SERVER_VARS['PHP_SELF'];

	// for now. log_url assumes '../'. Mark disapproves ;)
	$log_url= dirname(dirname($current_path))."/";
	$log_url=str_replace("\\", "", $log_url);
	if (strpos($log_url, "php.exe") > 0) {
		$log_url= substr($log_url, strpos($log_url, "php.exe")+7);
	}

	if ($log_url=="//")  { $log_url="/"; }

	return "tp://".$HTTP_SERVER_VARS['HTTP_HOST'].$log_url;


}


function too_old($date,$numdays){

	$limit = date("Y-m-d-H-i-s", mktime (0,0,0,date("m")  ,date("d")-$numdays ,date("Y")));

	if ($limit < $date) {
		return FALSE;
	} else {
		return TRUE;
	}

}

/*
$bouncesites= array('www.domain.org', 'www.domain.com');

foreach ($bouncesites as $site) {
if ( (isset($HTTP_SERVER_VARS['HTTP_REFERER'])) && (strpos($HTTP_SERVER_VARS['HTTP_REFERER'], $site)) ) {
header("Location: ".$HTTP_SERVER_VARS['HTTP_REFERER']);
die();
}
}

*/

// set the current path:
$path = path();

//
// In safe mode, this won't work.. We need to change the owner of this file.
//
if (!($ips= @file($pivot_path.'db/ignored_ips.txt'))) {
	$ips= array();
}


if ( (!in_array($HTTP_SERVER_VARS['REMOTE_ADDR'], $ips)) && 
	(isset($HTTP_SERVER_VARS['HTTP_REFERER'])) && 
	(!strpos($HTTP_SERVER_VARS['HTTP_REFERER'], $path)) ) {

	$ref_file = $pivot_path."db/ser_last_referrers.php";
	$rows_file = 250;
	$rows_site = 20;
	$rows_days = 5;

	$write = TRUE;

	$lastreferrers =	load_serialize($ref_file, TRUE);

	$REMOTE_HOST = @gethostbyaddr($HTTP_SERVER_VARS['REMOTE_ADDR']);

	$lastreferrers[] = array(
	'date' => format_date("", "%year%-%month%-%day%-%hour24%-%minute%"),
	'referer' => $HTTP_SERVER_VARS['HTTP_REFERER'] ,
	'agent' => $HTTP_SERVER_VARS['HTTP_USER_AGENT'] ,
	'remote_host' => $REMOTE_HOST ,
	'remote_addr' => $HTTP_SERVER_VARS['REMOTE_ADDR'],
	'request_uri' => $HTTP_SERVER_VARS['REQUEST_URI']
	);

	if ( (count($lastreferrers)>$rows_file) ) {
		$lastreferrers = array_slice($lastreferrers, -$rows_file, $rows_file);
	}

	if ( (too_old($lastreferrers[0]['date'], $rows_days))) {
		array_shift ($lastreferrers);
		array_shift ($lastreferrers);
	}

	save_serialize($ref_file, $lastreferrers);

}

error_reporting($old_errrep);

if (!defined("__SILENT__")) {
	printf("<!-- getref ");
	if (isset($write)) { echo "(write)"; }
	echo "(".timetaken('int').")";
	printf("-->");
}

?>