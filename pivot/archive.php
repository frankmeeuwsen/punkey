<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------

// First line defense.
if (file_exists(dirname(__FILE__)."/first_defense.php")) {
	include_once(dirname(__FILE__)."/first_defense.php");
	block_refererspam();
}


define('__SILENT__', TRUE);
// apparently also defined in pv_core
// $marco (who hates php notices)
// define('INPIVOT', TRUE);
define('LIVEPAGE', TRUE);

include_once("pv_core.php");
include_once("modules/module_userreg.php");


// convert encoding to UTF-8
i18n_array_to_utf8($Pivot_Vars, $dummy_variable);

$live_output = TRUE;

if (isset($Pivot_Vars['w']) && !empty($Pivot_Vars['w'])) {
	$Pivot_Vars['w'] = weblog_from_para($Pivot_Vars['w']);
}
if (isset($Pivot_Vars['c']) && !empty($Pivot_Vars['c'])) {
	$Pivot_Vars['c'] = category_from_para($Pivot_Vars['c']);
}

if (!isset($Pivot_Vars['w'])) {
	$override_weblog = "";
	// Check if we can determine which (or at least one) weblog this category 
	// belongs to. It might belong to multiple weblogs, and here we check to see
	// it the referer can be matched to a weblog's homepage. 
	$referer = str_replace("http://".$Pivot_Vars['HTTP_HOST'], "", $Pivot_Vars['HTTP_REFERER']);
	if ($referer!="") {
	    foreach($Weblogs as $weblogkey => $weblog) {
		$filename = fixpath($Paths['pivot_url'] .  $weblog['front_path'] . $weblog['front_filename']);
		if ( ($referer == $filename) || 
			($referer == str_replace("/index.php", "/", $filename)) ||
			($referer == str_replace("/index.php", "/", $filename)) ) {
		    $override_weblog = $weblogkey;
		}
	    }
	}
	// Referer didn't reveal anything (or was empty):
	// 1) Match weblog against category if possible else
	// 2) Just select the first weblog.
	if (empty($override_weblog)) {
		if (!empty($Pivot_Vars['c'])) {
			$in_weblogs = find_weblogs_with_cat($Pivot_Vars['c']);
			if (count($in_weblogs) != 0) {
				$override_weblog = $in_weblogs[0];
			}
		} else {
			reset($Weblogs);
			$override_weblog = key($Weblogs);
		} 
	} 

	$Pivot_Vars['w'] = $override_weblog;
} else if (!empty($Pivot_Vars['w'])) {
	// Checking if the weblog exists.
	if (!isset($Weblogs[$Pivot_Vars['w']])) {
		piv_error("Weblog doesn't exist","Selected weblog \"".
		htmlspecialchars($Pivot_Vars['w'])."\" doesn't exists.");
	} 
	// Both weblog and category has some value - checking if they match
	if (!empty($Pivot_Vars['c'])) {
		$in_weblogs = find_weblogs_with_cat($Pivot_Vars['c']); 
		 	     
		if (!in_array($Pivot_Vars['w'],$in_weblogs)) { 
			piv_error("Category/weblog mismatch", 'Category "'.htmlspecialchars($Pivot_Vars['c']).
			"\" doesn't belong to selected weblog (".htmlspecialchars($Pivot_Vars['w']).")."); 
		}
	}
}

if (!isset($Pivot_Vars['c'])) {
	$Pivot_Vars['c']="";
} elseif (!empty($Pivot_Vars['c'])) {
	// Checking if category exists
	$cats = cfg_getarray('cats');
	if (!in_array($Pivot_Vars['c'],$cats)) {
		piv_error("Category doesn't exist","Selected category \"".
		htmlspecialchars($Pivot_Vars['c'])."\" doesn't exists.");
	} 
}

if (!isset($Pivot_Vars['u'])) {
	$Pivot_Vars['u']="";
}

if (!isset($Pivot_Vars['t'])) {
	if (!empty($Pivot_Vars['w'])) {
		$Pivot_Vars['t'] = basename($Weblogs[$Pivot_Vars['w']]['extra_template']);
	} else {
		$Pivot_Vars['t'] = "";
	}
} else {
	$Pivot_Vars['t'] = basename($Pivot_Vars['t']);
}

$output = generate_live_page($Pivot_Vars['w'], $Pivot_Vars['c'], $Pivot_Vars['t'], $Pivot_Vars['u']);

echo $output;



add_hook("getref", "pre");
execute_hook("getref", "pre", $hook_output);


?>
