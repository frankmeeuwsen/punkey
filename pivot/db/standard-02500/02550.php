<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:2550;s:4:"date";s:16:"2002-01-08-21-23";s:4:"user";s:6:"Punkey";s:5:"title";s:28:"Eerste Flash virus? "A proof";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-01-08-21-23";s:9:"edit_date";s:16:"2002-01-08-21-23";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:583:"<b><a href="http://www.theregister.co.uk/content/56/23594.html">Eerste Flash virus?</a> </b>

"A proof of concept virus which has the potential to infect Shockwave Flash (SWF) files on Web sites has been discovered.

The SWF/LFM-926 virus, which could infect surfers if they download and open the Flash file on their computer, is the first of its kind, according to antivirus vendor Sophos. Simply viewing a Web site of Flash movie fails to cause infection, early tests suggest."

Meer info kun je ook <a href="http://www.sophos.com/virusinfo/analyses/swflfm926.html">hier</a> vinden";s:4:"body";s:0:"";}