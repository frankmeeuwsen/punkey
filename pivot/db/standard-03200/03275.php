<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:3275;s:4:"date";s:16:"2002-04-03-12-59";s:4:"user";s:6:"Punkey";s:5:"title";s:24:"Dispatches From Ramallah";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-04-03-12-59";s:9:"edit_date";s:16:"2002-04-03-12-59";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1101:"<a href="http://www.alternet.org/story.html?StoryID=12762">Ooggetuigenverslagen</a> uit Ramallah:

"Hours ago, Israeli tanks shelled St. Mary Church in Bethlehem, killing Father Jack (54) and injuring five sisters"

"I need milk for my two month old baby; I need medicine because I have diabetes; I need medicine for my high blood pressure; I'm scared -- I'm alone trapped in a restaurant in Ramallah and the Israeli snipers are on the rooftop of the building I'm in; I don't know what to do -- my children are scared"

"We woke up again with the sounds of the tanks."

"Yesterday, 5 policemen were just sitting in their place and the Israelis shot them. It was so clear there was no resistance. All the blood was on the floor, not high on the walls. It was clear they were sitting on the floor when they were killed. They showed it on TV. All of them were shot in the head when they were sitting on the floor."

"A 55-year old woman by the name of Widad Majed Nimr Safwan was shot dead by Israeli snipers as she left the Ramallah hospital this morning where she came for treatment of her broken leg."";s:4:"body";s:0:"";}