<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:1782;s:4:"date";s:16:"2005-04-03-10-46";s:12:"introduction";s:4488:"Heerlijke zondagochtend met het zonnetje op de bol. Nu zit ik nog in de serre, maar over een paar maanden in de achtertuin van ons nieuwe huis :-) hehehehe. 

*Spin Awards*
Jullie hebben "al kunnen lezen":http://www.rhinofly.nl/frank-ly/index.cfm?Fuseaction=frankly.search&trefwoord=&auteur=&maand=04 dat ik vrijdag bij de jureringsdag van de Spin Awards ben geweest. Leuk om die verschillende cases te horen en te weten te komen hoe succesvol ze zijn geweest. Niet iedereen is even scheutig met die cijfers. Logisch, want de opdrachtgever wil dat niet altijd laten weten. Ben je werkzaam in online marketing of heb je iets te maken met reclame campagnes, dan kun je hier niet wegblijven. Veel goede cases, mooi materiaal en interessante mensen te ontmoeten. Komende donderdag ben ik ook op de uitreiking van de Spin Awards namens Frank-ly. Daar zullen (als de UMTS kaarten gaan werken) live updates zijn van de uitslag

*Frshdjs.com*
Niet iedereen weet dit, maar ik ben ook verantwoordelijk voor de bouw en continuering van de site "frshdjs.com":http://www.frshdjs.com. Deze regionale DJ-site heb ik met twee vrienden, "Da Huge":http://www.dahuge.com en "DJ Mark Green":http://markgreen.web-log.nl/, opgezet in het voorjaar van 2000. Sindsdien zijn er zo'n 700 DJ's ingeschreven en honderden artikelen geschreven. Echter, ik kan het niet meer opbrengen om me nog actief met de site te bemoeien. Het wordt te veel, ik ben te druk met andere zaken en het bouwen en onderhouden van een website interesseert me niet meer zo veel als vroeger. Dus heb ik de knoop doorgehakt en besloten te stoppen met frshdjs.com. Dat wil niet zeggen dat de site weg gaat. Als er in het publiek ontwikkelaars zijn die de site willen onderhouden en wellicht vernieuwen (hard nodig), laat het weten. De site is geheel in Cold Fusion gebouwd met een SQL database er achter.

*Moleskine*
Sinds enkele weken heb ik op aanraden van een collega een Moleskine notebook gekocht. Een papieren notebook. En ik moet zeggen, het is een heerlijk ding. Het ziet er relatief simpel uit, maar het briljante zit in de details. Zo kun je de Moleskine eenmaal open op een pagina, helemaal plat leggen. Dus de pagina's bollen niet op naar het midden. Verder is het papier niet behandeld met allerlei zuren. Dat betekent een andere schrijfervaring. Het schrijft veel "zachter" en prettiger dan op normaal papier
Andere extra's: Een elastiek die om het boekje kan om alles bij elkaar te houden en een extra opbergvakje achterin om losse papiertjes en ideen in te verzamelen.
Meer extra's: een complete cultbeweging op Internet met geweldige sites over het gebruik van de Moleskine, hacks en foto's
* "Moleskinerie":http://www.moleskinerie.com/
* "Moleskine hacks":http://www.43folders.com/2004/11/post.html
* "Meer Moleskine hacks":http://wiki.43folders.com/index.php/Moleskine_Hacks in een Wiki-project
* "The wandering Moleskine Project":http://octolan.com/journey/: Een Moleskine gaat de wereld over en scans worden op deze pagina verzameld
* Flickr "Moleskinerie fotopool":http://flickr.com/groups/36521985904@N01/ en de "Moleskine public tag":http://flickr.com/photos/tags/moleskine/
* "Google groups":http://groups-beta.google.com/group/Moleskinerie

Volgens mij heeft een merk als "Ahrend":http://www.ahrenddirect.nl/indexahd.html toch niet zo'n fanclub voor iets normaals als een schrijfblok...

*Punkey, TPG en VT Wonen*
De verhuizing brengt ook een hoop administratieve rompslomp met zich mee. Zoals adreswijzigingen doorgeven. Nu kan dat tegenwoordig bij de TPG online waarvoor "wederom hulde":http://www.punkey.com/pivot/entry.php?id=1767. Bij "het formulier":http://www.tpgpost.nl/voorthuis/verhuisservice/index.php staat een invoerveld :VT Wonen actiecode. Aangezien wij abonnee zijn op dat blad, denk ik: Ha! Korting! Mooi! Maar ik heb geen idee waar ik die code vandaan moet halen. Bij de TPG site staat niets, op de VT Wonen site staat niets en in het blad is ook niets te vinden. Dus maar een mailtje gestuurd naar VT Wonen. Natuurlijk hoor ik daar noooooit meer iets van. Het is nu al bijna een week geleden en ik hoor niets van die koekenbakkers.
Wat schetst dan ook mijn verbazing als ik ga zoeken bij Google op "vt wonen actie tpg post":http://www.google.nl/search?hl=nl&q=vt+wonen+actie+tpg+post&btnG=Google+zoeken&meta=. Inderdaad...mijn eigen post over TPG post. Zie [[popup:vtwonengoogle.jpg:het screenshot::1:inline]] voor het nageslacht. Ja, zo kom ik er natuurlijk nooit achter...";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:23:"Sunday morning teanotes";}s:12:"publish_date";s:16:"2005-04-03-09-46";s:9:"edit_date";s:16:"2005-12-30-23-56";s:5:"title";s:23:"Sunday morning teanotes";s:8:"subtitle";s:27:"Moleskine en de Spin Awards";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:2:{i:0;a:8:{s:4:"name";s:10:"Mark Green";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:13:"80.126.74.237";s:4:"date";s:16:"2005-04-03-11-14";s:7:"comment";s:61:"Dank voor je inzet Frank. We spreken elkaar hier nog over :-)";s:10:"registered";i:0;s:6:"notify";s:3127:"N;}i:1;a:8:{s:4:"name";s:3:"Bob";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"82.92.14.145";s:4:"date";s:16:"2005-04-03-18-32";s:7:"comment";s:21:"Moleskine = TEH rock!";s:10:"registered";i:0;s:6:"notify";N;}i:2;a:8:{s:4:"name";s:10:"Mark Green";s:5:"email";s:0:"";s:3:"url";s:18:"www.djmarkgreen.nl";s:2:"ip";s:13:"80.126.74.237";s:4:"date";s:16:"2005-04-03-22-02";s:7:"comment";s:96:"Uhhh, de link naar mijn website klopt trouwens niet... Ik heb zo'n cheapy dingentje weet je nog.";s:10:"registered";i:0;s:6:"notify";N;}i:3;a:8:{s:4:"name";s:6:"punkey";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:13:"213.84.86.142";s:4:"date";s:16:"2005-04-03-22-06";s:7:"comment";s:14:"Oeps...nu wel!";s:10:"registered";i:0;s:6:"notify";N;}i:4;a:8:{s:4:"name";s:17:"Baby Grandpa zelf";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:11:"62.58.1.130";s:4:"date";s:16:"2005-04-05-15-42";s:7:"comment";s:610:"Dus als ik het goed begrijp is dat Moleskine notebook  gewoon te vergelijken met de papieren agenda die ik van de baas heb gehad? Kun je ook plat neerleggen zonder die bekende bolling in de pagina's en er zit ook een touwtje in. Hij is alleen niet van Moleskine. Zukke dingen kun je toch gewoon bij de Hema kopen? Wat is er nu zo speciaal aan Moleskine? Of is dit trendverantwoord een beetje de 'Rolls Royce' onder de agenda's cq notitieblokken? In ieder geval wel een verheugend artikel, want inderdaad: gewoon effe lekker opschrijven wat je nog moet doen en niet moet vergeten is nog altijd het handigst! ;-)";s:10:"registered";i:0;s:6:"notify";N;}i:5;a:8:{s:4:"name";s:6:"punkey";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"83.161.26.32";s:4:"date";s:16:"2005-04-05-15-47";s:7:"comment";s:235:"Je weet het he...altijd trendverantwoord zijn.:-)
Maar ik zal hem je wel eens laten zien en dan mag je er een poppetje in tekenen. Wedden dat je Yoesoef Blokhak veel soepeler en intuitiever tekent dan in zo'n derderangs Hema blokje?!?";s:10:"registered";i:0;s:6:"notify";N;}i:6;a:8:{s:4:"name";s:17:"Dimitrie Morrison";s:5:"email";s:29:"depmorrison"nospam"@zonnet.nl";s:3:"url";s:0:"";s:2:"ip";s:13:"212.78.163.78";s:4:"date";s:16:"2005-04-11-13-33";s:7:"comment";s:198:"Hallo Punkey,

Ben je eral achter waar die VT-Wonen actiecode voor dient en waar je hem kan vinden? Ook ik heb een abonnenment op de VT-Wonen. Alvast bedankt voor je reactie.

Groet,

Dimitrie";s:10:"registered";i:0;s:6:"notify";N;}i:7;a:8:{s:4:"name";s:5:"Ellen";s:5:"email";s:20:"post@ellendegraaf.nl";s:3:"url";s:26:"http://www.ellendegraaf.nl";s:2:"ip";s:14:"80.242.237.106";s:4:"date";s:16:"2005-04-13-10-56";s:7:"comment";s:461:"Hey! Heb net een mailtje van de TPG teruggekregen:


----------

De actie over VT-wonen is een actie die in een aantal proefgebieden loopt.
Om de klanten te attenderen om de verhuisserservice via internet te regelen, liggen op de postkantoren in de regio Twente en Den Haag flyers. Op deze flyers staat een actiecode. Als je dan deze actie invult en de service via internet afsluit, ontvangt de klant 1 blad VT-wonen.

----------


Das nou jammer ...";s:10:"registered";i:0;s:6:"notify";s:1:"1";}}}";}s:3127:"N;}i:1;a:8:{s:4:"name";s:3:"Bob";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"82.92.14.145";s:4:"date";s:16:"2005-04-03-18-32";s:7:"comment";s:21:"Moleskine = TEH rock!";s:10:"registered";i:0;s:6:"notify";N;}i:2;a:8:{s:4:"name";s:10:"Mark Green";s:5:"email";s:0:"";s:3:"url";s:18:"www.djmarkgreen.nl";s:2:"ip";s:13:"80.126.74.237";s:4:"date";s:16:"2005-04-03-22-02";s:7:"comment";s:96:"Uhhh, de link naar mijn website klopt trouwens niet... Ik heb zo'n cheapy dingentje weet je nog.";s:10:"registered";i:0;s:6:"notify";N;}i:3;a:8:{s:4:"name";s:6:"punkey";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:13:"213.84.86.142";s:4:"date";s:16:"2005-04-03-22-06";s:7:"comment";s:14:"Oeps...nu wel!";s:10:"registered";i:0;s:6:"notify";N;}i:4;a:8:{s:4:"name";s:17:"Baby Grandpa zelf";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:11:"62.58.1.130";s:4:"date";s:16:"2005-04-05-15-42";s:7:"comment";s:610:"Dus als ik het goed begrijp is dat Moleskine notebook  gewoon te vergelijken met de papieren agenda die ik van de baas heb gehad? Kun je ook plat neerleggen zonder die bekende bolling in de pagina's en er zit ook een touwtje in. Hij is alleen niet van Moleskine. Zukke dingen kun je toch gewoon bij de Hema kopen? Wat is er nu zo speciaal aan Moleskine? Of is dit trendverantwoord een beetje de 'Rolls Royce' onder de agenda's cq notitieblokken? In ieder geval wel een verheugend artikel, want inderdaad: gewoon effe lekker opschrijven wat je nog moet doen en niet moet vergeten is nog altijd het handigst! ;-)";s:10:"registered";i:0;s:6:"notify";N;}i:5;a:8:{s:4:"name";s:6:"punkey";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"83.161.26.32";s:4:"date";s:16:"2005-04-05-15-47";s:7:"comment";s:235:"Je weet het he...altijd trendverantwoord zijn.:-)
Maar ik zal hem je wel eens laten zien en dan mag je er een poppetje in tekenen. Wedden dat je Yoesoef Blokhak veel soepeler en intuitiever tekent dan in zo'n derderangs Hema blokje?!?";s:10:"registered";i:0;s:6:"notify";N;}i:6;a:8:{s:4:"name";s:17:"Dimitrie Morrison";s:5:"email";s:29:"depmorrison"nospam"@zonnet.nl";s:3:"url";s:0:"";s:2:"ip";s:13:"212.78.163.78";s:4:"date";s:16:"2005-04-11-13-33";s:7:"comment";s:198:"Hallo Punkey,

Ben je eral achter waar die VT-Wonen actiecode voor dient en waar je hem kan vinden? Ook ik heb een abonnenment op de VT-Wonen. Alvast bedankt voor je reactie.

Groet,

Dimitrie";s:10:"registered";i:0;s:6:"notify";N;}i:7;a:8:{s:4:"name";s:5:"Ellen";s:5:"email";s:20:"post@ellendegraaf.nl";s:3:"url";s:26:"http://www.ellendegraaf.nl";s:2:"ip";s:14:"80.242.237.106";s:4:"date";s:16:"2005-04-13-10-56";s:7:"comment";s:461:"Hey! Heb net een mailtje van de TPG teruggekregen:


----------

De actie over VT-wonen is een actie die in een aantal proefgebieden loopt.
Om de klanten te attenderen om de verhuisserservice via internet te regelen, liggen op de postkantoren in de regio Twente en Den Haag flyers. Op deze flyers staat een actiecode. Als je dan deze actie invult en de service via internet afsluit, ontvangt de klant 1 blad VT-wonen.

----------


Das nou jammer ...";s:10:"registered";i:0;s:6:"notify";s:1:"1";}}}";s:3127:"N;}i:1;a:8:{s:4:"name";s:3:"Bob";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"82.92.14.145";s:4:"date";s:16:"2005-04-03-18-32";s:7:"comment";s:21:"Moleskine = TEH rock!";s:10:"registered";i:0;s:6:"notify";N;}i:2;a:8:{s:4:"name";s:10:"Mark Green";s:5:"email";s:0:"";s:3:"url";s:18:"www.djmarkgreen.nl";s:2:"ip";s:13:"80.126.74.237";s:4:"date";s:16:"2005-04-03-22-02";s:7:"comment";s:96:"Uhhh, de link naar mijn website klopt trouwens niet... Ik heb zo'n cheapy dingentje weet je nog.";s:10:"registered";i:0;s:6:"notify";N;}i:3;a:8:{s:4:"name";s:6:"punkey";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:13:"213.84.86.142";s:4:"date";s:16:"2005-04-03-22-06";s:7:"comment";s:14:"Oeps...nu wel!";s:10:"registered";i:0;s:6:"notify";N;}i:4;a:8:{s:4:"name";s:17:"Baby Grandpa zelf";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:11:"62.58.1.130";s:4:"date";s:16:"2005-04-05-15-42";s:7:"comment";s:610:"Dus als ik het goed begrijp is dat Moleskine notebook  gewoon te vergelijken met de papieren agenda die ik van de baas heb gehad? Kun je ook plat neerleggen zonder die bekende bolling in de pagina's en er zit ook een touwtje in. Hij is alleen niet van Moleskine. Zukke dingen kun je toch gewoon bij de Hema kopen? Wat is er nu zo speciaal aan Moleskine? Of is dit trendverantwoord een beetje de 'Rolls Royce' onder de agenda's cq notitieblokken? In ieder geval wel een verheugend artikel, want inderdaad: gewoon effe lekker opschrijven wat je nog moet doen en niet moet vergeten is nog altijd het handigst! ;-)";s:10:"registered";i:0;s:6:"notify";N;}i:5;a:8:{s:4:"name";s:6:"punkey";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"83.161.26.32";s:4:"date";s:16:"2005-04-05-15-47";s:7:"comment";s:235:"Je weet het he...altijd trendverantwoord zijn.:-)
Maar ik zal hem je wel eens laten zien en dan mag je er een poppetje in tekenen. Wedden dat je Yoesoef Blokhak veel soepeler en intuitiever tekent dan in zo'n derderangs Hema blokje?!?";s:10:"registered";i:0;s:6:"notify";N;}i:6;a:8:{s:4:"name";s:17:"Dimitrie Morrison";s:5:"email";s:29:"depmorrison"nospam"@zonnet.nl";s:3:"url";s:0:"";s:2:"ip";s:13:"212.78.163.78";s:4:"date";s:16:"2005-04-11-13-33";s:7:"comment";s:198:"Hallo Punkey,

Ben je eral achter waar die VT-Wonen actiecode voor dient en waar je hem kan vinden? Ook ik heb een abonnenment op de VT-Wonen. Alvast bedankt voor je reactie.

Groet,

Dimitrie";s:10:"registered";i:0;s:6:"notify";N;}i:7;a:8:{s:4:"name";s:5:"Ellen";s:5:"email";s:20:"post@ellendegraaf.nl";s:3:"url";s:26:"http://www.ellendegraaf.nl";s:2:"ip";s:14:"80.242.237.106";s:4:"date";s:16:"2005-04-13-10-56";s:7:"comment";s:461:"Hey! Heb net een mailtje van de TPG teruggekregen:


----------

De actie over VT-wonen is een actie die in een aantal proefgebieden loopt.
Om de klanten te attenderen om de verhuisserservice via internet te regelen, liggen op de postkantoren in de regio Twente en Den Haag flyers. Op deze flyers staat een actiecode. Als je dan deze actie invult en de service via internet afsluit, ontvangt de klant 1 blad VT-wonen.

----------


Das nou jammer ...";s:10:"registered";i:0;s:6:"notify";s:1:"1";}}}";}}