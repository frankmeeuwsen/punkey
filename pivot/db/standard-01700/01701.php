<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:1701;s:4:"date";s:16:"2005-02-13-20-24";s:12:"introduction";s:1958:"In a previous post, I referred to The Big Project which makes me really uncomfortable about doing all the other little things that keep coming in. "Mike Sale":http://www.punkey.com/pivot/entry.php?id=1687#mike_sale-0502102324 has some excellent remarks on that topic and I would like to extend the conversation a bit about this subject. Because it is true. Even the Biggest of Biggest Projects start with a small, controllable Next Action. Isolating can be a good option, but the Project is suffering from "progressive insight" meaning we face new challenges as we go along. New user scenario's, changed features, uncertainties about new features. These should have been faced before we started but right now, we are facing this and nothing can change that. Ofcourse it is a good lesson for a next project. So what does this have to do with isolating? I can isolate like hell, but when I plug in again, there are changes in the (voice) mail and people on my desk asking for assistance because of changing point-of-views. 
You might say this is a projectmanagement-problem which cannot be solved by GTD and I agree. This has more to do with the way Projectmanagement was handled and I am the first to admit that. But with that as a given, I am looking how I can make the best of it with the techniques at hand.
"Breakdown" might be a good alternative and I will try that out coming days. 
Corie also has an excellent remark about making a little list. But if you read the above, you will find that this list can be of no use just one hour after the start of the day ;-)
I do feel Katherine also touched a nerve. I am not familiar with Covey's techniques but heard a lot about it. I will look into this some more in the future
I sure hope to find the time to read some pages in David's book again. I think it will get me kickstarted again. 

_Did you have similar experiences? Please let me know and tell me how you faced them. Maybe they will help me!_";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2005-02-13-20-11";s:9:"edit_date";s:16:"2005-02-13-20-24";s:5:"title";s:50:"Extending the conversation about many Next Actions";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:2:{i:0;a:8:{s:4:"name";s:5:"Corie";s:5:"email";s:0:"";s:3:"url";s:48:"http://higherproductivity.com/euphoria/index.php";s:2:"ip";s:13:"68.35.116.199";s:4:"date";s:16:"2005-02-14-02-06";s:7:"comment";s:568:"wow... I had no idea how large this problem really is!

How much input do you have in this project?  Can you talk to the others and express your concerns?

It may be that the best use of everyone's time is to stop working, and go back to planning.  I'm sure someone will say that you can't in order to meet deadlines and milestones, but if  plans and goals are constantly changing, it's going to be difficult to impossible to meet those goals anyway.

And if you do go back to planning, there are some good tips on project planning in David's book.

Good luck!";s:10:"registered";i:0;s:6:"notify";s:1659:"N;}i:1;a:8:{s:4:"name";s:9:"Mike Sale";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"148.87.1.170";s:4:"date";s:16:"2005-02-14-04-14";s:7:"comment";s:1456:"I'm betting that your project is suffering from a grand vision. It is likely trying to solve many problems for many different stakeholders, share key, core data and processes across departments, LOBs, or maybe even partners. 

One word: FOCUS

Focus for your project means rescoping. In GTD-speak, you need to get back to reviewing �what done means� and renegociate your commitments as needed. My suggestion is that you rescope what �done� means to something you KNOW that you can deliver on schedule, that you KNOW has a real return on the customer�s investment in your work.

At a higher altitude, this might mean that you renegociate what you deliver when, and what �done� means (i.e. milestone or final deliverable). 

This means taking all those NAs and slapping them over to �Someday� or �Maybe� (yes, I keep separate lists) and pushing all these new discoveries into a new track (at least one project). Make sure your Someday list is grouped by project!

You then need to get hardcore about your hard landscape and the conversations you engage in to protect your ability to execute. Capture the potential stuff into your �someday� list, and review weekly to see what you may be able to pull into your space for the following week.

Also look hard at being an enabler from a distance. Make substantive use of the @Waiting For list with weekly notes asking for progress and using your Weekly Review to review and process these accordingly.";s:10:"registered";i:0;s:6:"notify";N;}}}";}s:1659:"N;}i:1;a:8:{s:4:"name";s:9:"Mike Sale";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"148.87.1.170";s:4:"date";s:16:"2005-02-14-04-14";s:7:"comment";s:1456:"I'm betting that your project is suffering from a grand vision. It is likely trying to solve many problems for many different stakeholders, share key, core data and processes across departments, LOBs, or maybe even partners. 

One word: FOCUS

Focus for your project means rescoping. In GTD-speak, you need to get back to reviewing �what done means� and renegociate your commitments as needed. My suggestion is that you rescope what �done� means to something you KNOW that you can deliver on schedule, that you KNOW has a real return on the customer�s investment in your work.

At a higher altitude, this might mean that you renegociate what you deliver when, and what �done� means (i.e. milestone or final deliverable). 

This means taking all those NAs and slapping them over to �Someday� or �Maybe� (yes, I keep separate lists) and pushing all these new discoveries into a new track (at least one project). Make sure your Someday list is grouped by project!

You then need to get hardcore about your hard landscape and the conversations you engage in to protect your ability to execute. Capture the potential stuff into your �someday� list, and review weekly to see what you may be able to pull into your space for the following week.

Also look hard at being an enabler from a distance. Make substantive use of the @Waiting For list with weekly notes asking for progress and using your Weekly Review to review and process these accordingly.";s:10:"registered";i:0;s:6:"notify";N;}}}";s:1659:"N;}i:1;a:8:{s:4:"name";s:9:"Mike Sale";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"148.87.1.170";s:4:"date";s:16:"2005-02-14-04-14";s:7:"comment";s:1456:"I'm betting that your project is suffering from a grand vision. It is likely trying to solve many problems for many different stakeholders, share key, core data and processes across departments, LOBs, or maybe even partners. 

One word: FOCUS

Focus for your project means rescoping. In GTD-speak, you need to get back to reviewing �what done means� and renegociate your commitments as needed. My suggestion is that you rescope what �done� means to something you KNOW that you can deliver on schedule, that you KNOW has a real return on the customer�s investment in your work.

At a higher altitude, this might mean that you renegociate what you deliver when, and what �done� means (i.e. milestone or final deliverable). 

This means taking all those NAs and slapping them over to �Someday� or �Maybe� (yes, I keep separate lists) and pushing all these new discoveries into a new track (at least one project). Make sure your Someday list is grouped by project!

You then need to get hardcore about your hard landscape and the conversations you engage in to protect your ability to execute. Capture the potential stuff into your �someday� list, and review weekly to see what you may be able to pull into your space for the following week.

Also look hard at being an enabler from a distance. Make substantive use of the @Waiting For list with weekly notes asking for progress and using your Weekly Review to review and process these accordingly.";s:10:"registered";i:0;s:6:"notify";N;}}}";}}