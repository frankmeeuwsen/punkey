<?php /* pivot */ die(); ?>a:14:{s:4:"code";i:1151;s:12:"introduction";s:2916:"Vandaag de hele dag tussen allerlei drukke werkzaamheden door even de <a href="http://www.bloglines.com/">Bloglines-feeds</a>
bijgehouden. Het mooie is dat ik in met 1 klik een berichtje in een
aparte folder kan zetten om later nog eens te bekijken. Allemaal
extreem loggable materiaal. Maar om nu allemaal weer losse logjes te
maken is ook zo wat. En aangezien in Pivot de "timed publish" nog niet
werkt, maak ik dan voor deze ene keer een soort van
linkdump-a-gogo-postje. Dus alleen maar links en korte beschrijvingen.
Zie maar wat je er mee doet.<br  />
<ul>
  <li><a title="" href="http://www.bootleg-objects.com/objects.htm">Bootleg objects</a>: Casemodding extraordinaire!</li> <li><a title="" href="http://www.engadget.com/entry/5719585515194684/">Live concert CD duplicator</a>: Je eigen CD-business in the back of a van, kapitalisme roeleert!</li>
  <li><a title="" href="http://www.retropod.com/">RetroPodTM iPod Case</a>: A weather-proof, future-proof iPod case. Handmade from a vintage Sports Walkman.&nbsp;
  </li><li><a title="" href="http://www.outfoxed.org/">Outfoxed</a>: De volgende anti-Amerika film na Fahrenheit 9/11 en Supersize Me. Deze keer over het rechtse Fox TV Network</li>
  <li>En de <a title="" href="http://www.nytimes.com/2004/07/11/magazine/11FOX.html">New York Times</a> schrijft direct over Guerrilla Filmmaking (inloggen met universele/login)</li>
  <li><a title="" href="http://projects.c505.com/ascii_index.html">ASCII Rock videos!</a> Nee maar HOE cool is dat!</li>
  <li><a title="" href="http://wired.com/news/technology/0,1282,64129,00.html">Video Chat Via Transparent Desktop Overlay</a>:
Nee maar HOE coolER is DAT! Check die pix! "In this world of 6 billion
people, it's hard to imagine that no one else had the same idea"</li>
  <li><a href="http://www.blueserker.com/html/modules.php?op=modload&amp;name=News&amp;file=article&amp;sid=157">IcyPole</a>:
There�s a new software application for Bluetooth-enabled cellphones and
PDAs called IcyPole which is supposed to alert you whenever somebody
else with music you might be interested in is in the general vicinity.
Maar dan moet er dus wel iemand zijn met genoeg MP3's op zijn telefoon
en die software geinstalleerd en Bluetooth aanzetten. Wordt vast een
knaller in Nederland :-)</li>
  <li><a title="" href="http://www.aikarin.com/mlp/customs/103.html">My Little Borg Pony</a>: All Cuteness will be assimilated.</li>
  <li>Tired: Flash mobs - Wired: <a title="" href="http://weblog.r-win.com/archives/001900.html">iPod playlist parties</a></li>
  <li><a title="" href="http://www.photoloco.com/ry_gangs.html#">Gang Photo's</a>: Behoorlijke heavy foto's van streetgangs in LA</li>
  <li><a title="" href="http://english.chosun.com/w21data/html/news/200407/200407110024.html">3 Megapixel cameratelefoon</a>: Je wilt het. Zeg het maar eerlijk. Je wilt toch gewoon zo'n telefoon....</li>
</ul>";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:7:"default";}s:4:"date";s:16:"2004-07-12-22-38";s:12:"publish_date";s:16:"2004-07-12-22-38";s:9:"edit_date";s:16:"2004-07-12-23-16";s:5:"title";s:24:"Uberluxe Linkdump-a-gogo";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"0";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"comments";a:2:{i:0;a:6:{s:4:"name";s:3:"Bob";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:12:"82.92.14.145";s:4:"date";s:16:"2004-07-13-11-19";s:7:"comment";s:46:"Hoor ik daar iemand een subtiele hint droppen?";}i:1;a:6:{s:4:"name";s:20:"Ome Punkstah himself";s:5:"email";s:0:"";s:3:"url";s:0:"";s:2:"ip";s:14:"213.84.172.170";s:4:"date";s:16:"2004-07-13-11-20";s:7:"comment";s:32:"Dat was niet subtiel bedoeld ;-)";}}}