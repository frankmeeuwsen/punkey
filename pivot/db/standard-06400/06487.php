<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:6487;s:4:"date";s:16:"2003-10-15-20-06";s:4:"user";s:7:"Da_Huge";s:5:"title";s:10:"Die Huub!!";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-10-15-20-06";s:9:"edit_date";s:16:"2003-10-15-20-06";s:8:"category";a:1:{i:0;s:5:"Music";}s:12:"introduction";s:253:"Hij heeft behoorlijk wat geflikt in Breda zeg, zelfs de journalist (die notabene bij mij zijn kaartjes heeft gekocht) schrijft een goed stuk over meneer Huub.
En dan nog <a href="http://www.dedijk.nl">het gastenboek </a>van de Dijk, bijzonder geestig!!!";s:4:"body";s:3703:"Een gevallen held

Door Willem Reijn

Woensdag 15 oktober 2003 - VIA het prikbord op de website van Mezz kon ik op het allerlaatste moment toch nog twee kaartjes voor het concert van De Dijk bemachtigen. Een hele meevaller, want voor we goed en wel terug waren van vakantie, bleek het concert in de Bredase poptempel al uitverkocht.


Dus op vrijdagavond ons naar Mezz gespoed. Merkwaardig: de fietskelder was dicht en gesloten. Waarom? Zou die kelder in een overdekt zwembad veranderen bij een flinke regenbui? Geen mededeling op het hek, gewoon pech. 

Nou en?, zult u zeggen. Maar onlangs zette mijn vrouw haar fiets tien minuten in het rek achter de Hema en toen was die vrij nieuwe fiets alweer gestolen. Ja, van de politie is het inmiddels zo vertrouwde standaardbriefje dat er bij gebrek aan aanwijzingen weer eens geen onderzoek zal worden ingesteld, etc. etc, maar dat de plisie bij nieuwe gegevens onmiddellijk op speurtocht zal gaan. Nog nooit meegemaakt. 

De gewone agent laat de burgers overigens gewoon weten dat hij precies weet waar, wanneer en door wie fietsen worden gestolen, maar dat hij er van zijn baas wegens andere prioriteiten niets aan mag doen. Enfin, weer een paar honderd euro voor een nieuwe verzekering door de plee gespoeld. 

Maar we kwamen dus voor Huub van der Lubbe, de rockende dichter met zijn vrienden. Het concert in de stampvolle Mezz was top. Er mogen 650 mensen kijken en meer gaan er ook echt niet in, constateerde ik. Maar je staat lekker dicht bij het podium en het geluid is perfect. 

Het concert was ook leuker dan we begin dit jaar bezochten in 013 in Tilburg. Toen was de Irak-oorlog net uitgebroken en Huub en zijn maten speelden een wat somber programma. 

De Dijk opende in Breda met &#8216;We beginnen nu pas echt&#8217;, een favoriet waarbij ik rillingen op de rug voel - en daarmee was de sfeer voor de avond gezet. Van der Lubbe vertelde over zijn avonturen in &#8216;De Klapcot&#8217;, waarmee hij meteen verried de stad ook weer niet echt te kennen. Ik hoef u natuurlijk weinig over het concert te vertellen, want voor het verslag van het concert heeft deze krant goedingewijde recensenten ingehuurd. 

Wat ik wel aardig vond, was dat de band na afloop zoveel tijd nam voor de signeersessie. Een leuke manier om tijdens het verdienen van een paar euro rechtstreeks contact te onderhouden met de trouwe en uitgebreide fanschare. Kijk, dit zijn nog jongens met beide poten op de grond, zonder allures. 

Gezellig stonden we een pilske te pakken aan de toog van het Mezz-caf�, tot Huub van der Lubbe plots een flats in zijn gezicht kreeg. De klap in zijn gezicht was het antwoord op een greep in de boezem van een vrouw, zo begrepen we alras. En voor deze vrouw steek ik graag beide handen in het vuur. Twee uur hadden we hem enthousiast en zelfs emotioneel staan toejuichen, hij had die rillingen op mijn rug gezongen - en toen bleek Huub van der Lubbe plots geen dichtende romanticus maar een graaier als de nieuwe gouverneur van Californi�. De volgende ochtend las ik in deze krant het bericht dat de politie dankzij attente omstanders een man in de kraag hadden gepakt die een meisje onzedelijk had vastgepakt. De zwerver, zo treffend omschreven als een &#8216;man zonder vaste woon- of verblijfplaats&#8217; was ingesloten. 

Zo bezien valt zo&#8217;n klets nog reuze mee. Maar de vermelding in de recensie dat Huub van der Lubbe er ��n fan minder mee heeft, is bezijden de waarheid. Want ook ik zag in ��n klap mijn Held van zijn Sokkel af donderen. En in het gastenboek op de site van De Dijk is de verwarring nu algemeen: de ongelovigen versus de twijfelaars. 

Het goede van de avond was wel dat de fietsen dit keer niet gestolen waren.";}