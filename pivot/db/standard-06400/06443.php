<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:6443;s:4:"date";s:16:"2003-10-06-16-54";s:4:"user";s:6:"Punkey";s:5:"title";s:26:"Google telt zoekopdrachten";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-10-06-16-54";s:9:"edit_date";s:16:"2003-10-06-16-54";s:8:"category";a:1:{i:0;s:7:"default";}s:12:"introduction";s:692:"<a title="Frequent Search Engine Users, Google Is Watching and Counting" href="http://www.nytimes.com/2003/10/06/technology/06goog.html?ex=1066017600&en=6b48de61370e996b&ei=5062&partner=GOOGLE">Frequent Search Engine Users, Google Is Watching and Counting</a>

The Web search service Google has quietly started placing a counter on its home page for a small number of its most frequent users. 

Most Google users do not have it, but a select few now have a no-frills counter that with each search clicks higher, noting "You have done 479 searches," or whatever the actual number. 

Als je dus geen teller op de Googlesite ziet kun je met een gerust hart zeggen:"Big Brother is ignoring me!!!"";s:4:"body";s:0:"";}