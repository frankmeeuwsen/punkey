<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:6960;s:4:"date";s:16:"2005-10-05-09-53";s:12:"introduction";s:392:"Since GTD is considered the geeks way to personal productivity, it is common sense the system and it's details are discussed on online mailinglists. Since I am a member of two of the large mailinglists, 43Folders and GTD@Yahoo (and still lurking the lists), I feel it's nice to discuss a bit the similarities and differences of the two so you can decide for yourself which one suits you best.";s:4:"body";s:3786:"h4. 43Folders

This mailinglist is closely related to the blogfather of Personal productivity: Merlin Mann and his weblog "43folders.com":http://www.43folders.com. 

*Sort of topics*
The list topics vary wildly and GTD is discussed not all of the time. Topics range from the use of specific software for tasks and filing to the use of pens and why you shouldn't keep them in your pocket. When GTD is discussed it is not the system itself but more how to use it, what sort of software is best or how people have made their own scripts and software for their own needs. I feel it is a perfect companion of the "43Folders Wiki":http://wiki.43folders.com/index.php/Main_Page as far the range of topics discussed

*Traffic*
With 3664 members, the traffic is very high. The number of discussions per thread can run up to 20+ easily! It is recommended to make your own filing-rule in you emailclient. I have a filter in Gmail that checks the subject for [43F Group] and archives the mail automatically with their own label. The big advantage with Gmail is that discussionthreads are sorted automagically so I can quickly scan the subjects and just read what interests me. Remember, you don't _have to_ read everything there is...

*Tone of voice*
I feel the members of the mailinglist know each other very well and I think some of them meet eachother in real life as well. The overall tone of voice is very friendly, kid-safe and helpfull towards eachother. Sometimes the discussion can get a bit technical when members start to talk about different kind of scripts.

*Where to find*
You can subscribe for free and find the archive at "Googlegroups":http://groups.google.com/group/43Folders?. You can also find a "wide range of webfeeds":http://groups.google.com/group/43Folders/feeds for this group. You will need a Google-account to subscribe. If you need a Gmail-account, feel free to "contact me":mailto:punkey@gmail.com and I will give you one.

h4. GTD@Yahoo

I was recently pointed to this group by Daly de Gagne (thanks again!). As they say "for themselves":http://finance.groups.yahoo.com/group/Getting_Things_Done/: This group is for members to share their experiences, insights, suggestions and wisdom using David Allen's Getting Things Done system. 

*Topics*
I am a member of the group since a week and find the discussions very usefull. Topics are about GTD and implementation. The theory is discussed more than on 43Folders and practical implementation is referred to the book instead of the DIY-vibe that hangs in the 43Folders-group. One of the hot topics on this group is the exchange of the GTD...Fast Audio CD's in MP3 format. 

*Traffic*
The traffic is not as high as 43Folders. I find this one group I can follow very well. There are 2963 members at the time of this writing. Also, the number of discussions per thread is not that high. Sometimes it has 10+ mails on one thread but most of them are 4-6 mails per discussion. I also made a label and filter in Gmail for this group for easy retrieval and filing.

*Tone of voice*
This is very similar to 43Folders. Very friendly and easy going. Not as technical as 43Folders but more personal to me. I have read more personal stories and successes on GTD implementation on this group than on any other group or weblog. Very stimulating and inspirational

*Where to find*
You can find all the information on "GTD@Yahoo":http://finance.groups.yahoo.com/group/Getting_Things_Done/ and you can subscribe directly with "this emailadress":mailto:Getting_Things_Done-subscribe@yahoogroups.com. Also check out "the file-section":http://finance.groups.yahoo.com/group/Getting_Things_Done/files/ of the group for interesting reading material. You will need a Yahoo-login to become a member of the group.";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2005-10-05-09-01";s:9:"edit_date";s:16:"2005-10-05-10-05";s:5:"title";s:41:"Online GTD Discussiongroups, a comparison";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:1:{i:0;a:8:{s:4:"name";s:13:"Daly de Gagne";s:5:"email";s:21:"daly_de_gagne@shaw.ca";s:3:"url";s:0:"";s:2:"ip";s:11:"24.76.4.127";s:4:"date";s:16:"2005-10-05-21-58";s:7:"comment";s:262:"Frank, thanks for the kind words about GTD@Yahoo . Our members have made the group what it is. I'm glad that you've joined us, and hope you'll feel free to share on the group about your GTD experiences and what's happening with your blog.

Best wishes,

Daly";s:10:"registered";i:0;s:6:"notify";N;}}}