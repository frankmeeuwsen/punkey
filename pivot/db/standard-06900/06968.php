<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:6968;s:4:"date";s:16:"2005-10-23-21-19";s:12:"introduction";s:757:"Everyone already trying to get a 'mind like water' should recognize this quote:
<blockquote>Just four weeks ago, my desk was a shrine to disorder. It was cluttered with letters, takeaway menus, MiniDiscs and dead batteries, while stacks of printouts lay everywhere. A skyscraper of stuff towered in my inbox, untouched for months.

Now it is pristine. My intray is empty. My desk is clear. A smile is on my lips and I am getting things done. All thanks to one book.
</blockquote>

You can read the excellent article "Giving thanks to David Allen's cult time-management credo":http://technology.guardian.co.uk/weekly/story/0,16376,1595595,00.html for more personal success with GTD

(Thanks to "Fred Zelders":http://www.fredscapes.nl for the pointer)";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2005-10-23-21-18";s:9:"edit_date";s:16:"2005-10-24-10-07";s:5:"title";s:57:"Giving thanks to David Allen's cult time-management credo";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}