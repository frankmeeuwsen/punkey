<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:6905;s:4:"date";s:16:"2005-07-02-18-20";s:12:"introduction";s:1524:"[[image:mph.jpg::center:0]]
*Officieel*
"Live8 Live":http://www.Live8Live.com
"Make Poverty History":http://www.makepovertyhistory.org/
"The ONE campaign":http://www.theonecampaign.org/
"ONE campaign weblog":http://politicaltechnology.com/one/blogs/one_blog/
"Uitleg over de 8 Milleniumdoelstellingen":http://www.first8.org/first8.html
"live8live.nl":http://www.live8live.nl/

*Blogosfeer*
"Campagne weblog":http://campaignerblogs.typepad.com/makepovertyhistory/
"Wat is de G8":http://en.wikipedia.org/wiki/31st_G8_summit
"Live8 op Technorati":http://live8.technorati.com/
"Live8 op del.icio.us":http://del.icio.us/tag/live8
"Live8 op Flickr.com":http://www.flickr.com/photos/tags/live8/
"Live8 op Wikipedia":http://en.wikipedia.org/wiki/Live8
"BBC SMS blog":http://news.bbc.co.uk/1/hi/entertainment/music/4638399.stm
"Loggers against poverty":http://www.loggersagainstpoverty.com/
"Continu update van setlijst artiesten":http://weblog.roelonline.net/archives/003427.php
"Live8 Insider weblog":http://www.live8insider.com/ (_werkelijk_ live alles te volgen!)

Deze laatste weblog is ook erg bijzonder. Terwijl ik de Black Eyed Peas zit te kijken, zie ik al foto's op de weblog verschijnen. Ik heb er snel weer een foto van gemaakt even "online":http://www.flickr.com/photos/punkey/23052525/ gezet. Geweldig en indrukwekkend initiatief. Vergeet ook niet je "naam en foto":http://www.live8live.com/live8/imageHome.do achter te laten om de 8 grootste leiders duidelijk te maken dat er iets moet gebeuren...";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:8:"Rambling";}s:12:"publish_date";s:16:"2005-07-02-18-17";s:9:"edit_date";s:16:"2005-07-02-22-27";s:5:"title";s:20:"Make Poverty History";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}