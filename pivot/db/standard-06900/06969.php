<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:6969;s:4:"date";s:16:"2005-10-30-21-59";s:12:"introduction";s:1330:"Thanks to my reminder to check the productupdate-page from Netcentrics I just noticed that the "GTD Outlook addin":http://www.punkey.com/pivot/entry.php?id=6952 will "get a hotfix update":http://gtdsupport.netcentrics.com/learn/productupdates.php tomorrow for a really annoying problem:
<blockquote>
This hotfix will include the following fixes:
* 1979 � [Other] Update Task Action Error
* 1980 - [Action] Paths in File and Save for Reference are truncated using ... causing Read Restriction error
* 2149 - [Other] CTRL+ALT+j doesn't populate action with Projects when creating new Project
* 2150 - [Action] Can't Show Modal form when Non-Modal error
* 2151 - [Installation] Outlook is still running and cannot install error even though Outlook is closed and not running in memory
* 2152 - [Other] Default Outlook Reminder for Appointment is not used on Defer
</blockquote>
Excellent! One of the most annoying problems right now is the truncating of folderpaths. It makes it terrible to file emails and when in a flow of getting In to Emty, it leaves me with dozens and dozens of unfiled mails in my @Action or @Waiting folder. Where they don't belong. They need to be in shared folders on my office network. Now I hope one of the next updates will also have separate shortcut keys for Send, Send+Delegate and Send+File.";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2005-10-30-21-51";s:9:"edit_date";s:16:"2005-10-30-21-59";s:5:"title";s:36:"GTD Outlook add-in hotfix coming up!";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}