<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:4578;s:4:"date";s:16:"2002-11-11-11-47";s:4:"user";s:6:"Punkey";s:5:"title";s:24:"Nieuw produkt Macromedia";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-11-11-11-47";s:9:"edit_date";s:16:"2002-11-11-11-47";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1557:"<img src="http://www.frshdjs.com/punkey/spul/end_hassle_125.gif" border="0" align="right" hspace="5" vspace="5">Macromedia brengt vandaag een nieuw produkt op de markt om het beheer van websites te vergemakkelijken: <a href="http://www.macromedia.com/software/contribute/" target="_blank">Contribute</a>.

Met Contribute is het mogelijk om snel en eenvoudig kleine, statische sites te updaten zonder technische kennis van de scriptingtaal.
Contribute biedt de volgende voordelen
1. Tijdsbesparing en workflow-verbetering: Door de ingebouwde rechtenstructuur kan in een kortere tijd meerdere mensen binnen een organisatie de site updaten
2. Geen kennis van HTML noodzakelijk: Door middel van drie stappen kan een pagina worden geupdate: Browse, edit, publish
3. Makkelijke setup: Als je gewend bent met een tekstverwerker te werken, kun je direct met Contribute aan de slag
4. Codebescherming: Binnen de rechtenstructuur zijn ingebouwde beveiliging voor het plaatsen van illustraties en teksten. De onderliggende HTML code blijft beschermd voor de gebruiker
5. Dreamweaver MX integratie: In Dreamweaver MX kun je zelf templates maken, welke in Contribute zijn te gebruiken.

Macromedia heeft een feature tour <a href="http://www.macromedia.com/software/contribute/productinfo/features/flash_feature_tour/index.html" target="_blank">online</a> gezet, waarin je kunt zien wat Contribute is en wat het doet. Het <a href="http://www.macromedia.com/desdev/contribute/" target="_blank">Developmentcenter</a> is ook geupdate met pagina's informatie over Contribute.";s:4:"body";s:0:"";}