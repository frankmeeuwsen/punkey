<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:4527;s:4:"date";s:16:"2002-11-04-22-51";s:4:"user";s:8:"janneman";s:5:"title";s:17:"The Empathy Belly";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-11-04-22-51";s:9:"edit_date";s:16:"2002-11-04-22-51";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:656:""The Empathy Belly" <a href="http://www.empathybelly.org/">Pregnancy Simulator</a> lets you know what it feels like to be pregnant! It is a multi-component, weighted "garment" that will through medically accurate simulation -- enable men, women, teenage girls and boys, experience over 20 symptoms and effects of pregnancy, including:

- Weight gain of 30 pounds (13.6 kg.)
- Fetal kicking and stroking movements
- Shallow breathing and shortness of breath
- Increased blood pressure, pulse and body temperature
- Bladder pressure and frequency of urination
- Low backaches; shift in center of gravity; waddling
- Fatigue, irritability, and much, much more";s:4:"body";s:0:"";}