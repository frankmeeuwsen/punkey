<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5503;s:4:"date";s:16:"2003-04-08-00-26";s:4:"user";s:6:"Punkey";s:5:"title";s:24:"Trackback...nog een keer";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-04-08-00-26";s:9:"edit_date";s:16:"2003-04-08-00-26";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:2393:"<p>Opa Reet <a href="http://www.babygrandpa.com">snapt het nog steeds niet helemaal</a>, terwijl ik het hem toch echt een keer heb uitgelegd. Goed nog een keer dan...</p>

<p>1. Ik zie een postje bij Opa's weblog<br />
2. Geinig postje, wil ik zelf wat over schrijven<br />
3. En tegelijkertijd aan Opa laten weten dat ik er iets over heb geschreven. Dat kan ik op verschillende manieren doen<br />
3a. Bij hem langsgaan met bier en vertellen dat ik een aanvulling op zijn post heb gemaakt op mijn eigen weblog<br />
3b. Hem bellen, zeggen dat ik met bier langskom en tegelijkertijd vertellen dat ik een aanvulling op zijn post heb gemaakt op mijn eigen weblog<br />
3c. Een bericht in zijn comments achterlaten. En zeggen dat het hem bier kost.<br />
3d. Trackback. <br />
4. Ik kies voor trackback. Elk postje van Opa heeft een unieke Trackback-url. Deze is te vinden door op de link "trackback" te klikken bij Opa's postje en de link te bekijken. Bijvoorbeeld: http://www.punkey.com/mt-tb.cgi/470 (surf hier niet naartoe, want dat werkt niet)<br />
5. Nu ga ik in mijn MT administratie een nieuwe entry maken en helemaal onderin, bij het veld "URL's to ping" vul ik bovengenoemde URL in (http://www.punkey.com/mt-tb.cgi/470 dus)<br />
6. Ik maak mijn postje<br />
7. Ik klik op Publish<br />
8. Nu wordt via die URL (http://www.punkey.com/mt-tb.cgi/470) een belletje gestuurd naar het postje van Opa en gezegd:"Hey! Punkey heeft ook hier over wat geschreven! Check het! En neem bier voor hem mee"<br />
8a. Dat laatste stukje is trouwens een feature request<br />
9. Opa krijgt een mailtje dat hij is getrackbackt<br />
10. Opa brengt bier naar Punkey voor de duidelijke uitleg.</p>

<p>Nu is het zo dat met de bookmarklets van Movable Type ook een trackback URL is te vinden. Ga bijvoorbeeld maar eens naar de trackback weblog van Movable Type en selecteer een <a href="http://www.movabletype.org/trackback/archives/000300.html#000300">individuele post</a><br />
Kies nu met je rechtermuisknop "MT it!" of via de linksbalk "Post to MT" en je ziet dat er meteen een "Ping TrackBack URL" optie staat.<br />
Helaas werkt deze optie niet altijd even goed. Bij ons en bij Opa wil hij nog wel eens haperen. Kwestie van op de to do zetten om eens naar te kijken.</p>

<p>Ik hoop dat het nu wel een beetje duidelijker is geworden. Anders moet ik het je nog maar eens met een biertje uitleggen.</p>";s:4:"body";s:0:"";}