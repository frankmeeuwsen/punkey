<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:4093;s:4:"date";s:16:"2002-08-26-14-16";s:4:"user";s:6:"Punkey";s:5:"title";s:36:"Words and music from the Lowlands...";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-08-26-14-16";s:9:"edit_date";s:16:"2002-08-26-14-16";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:7101:"Man man man....wat een begin van mijn vakantie. Drie dagen Lowlands, drie dagen plezier en een tent met binnenzwembad. De vlammenschoenen staat in een emmer water te ontmodderen, de reservetent hang buiten te drogen vanwege het onder water liggen en de was gaat zo het masjien in. Ik heb mijn eerste normale ontbijt weer op zonder een Bacardi Cola voor de verandering.

Ik merk ook dat op Kazaa veel Lowlandsgangers zijn te vinden, want er wordt praktisch alleen maar Lowlands-muziek van mijn PC geupload....
Maar eerst even een recensie van het afgelopen weekend :-)

<b>Vrijdag</b>
Heerlijk op tijd gearriveerd, geen al te grote drukte, perfect festivalweertje, dus tijd zat om de tent op te zetten. Mooi plekje gevonden op camping 2, tegenover de toiletten en de douches. Toen waren we nog even vergeten dat het een traditie is om elke ochtend om 6.00 uur in de badhuizen een drumconcert te geven met 200 man...
Samen met QT, nichtje Steef, vrind Nick en vrindin Marlies de enorme 5 persoonstent opgezet. Onze eigen koepeltent bleef dus netjes ingepakt!

Tent opgezet, op naar het festivalterrein! Maar eerst even rustig indrinken met wat biertjes en het eerste bandje checken:<a href="http://www.floggingmolly.com/"> Flogging Molly</a>. Ierse Folkpunk in de beste Dropkick Murphy's traditie. Goeie muziek om maar per keer twee bier p.p. te halen. Hoef je maar half zo vaak te lopen toch?
Na Flogging Molly even het terrein een beetje verkend, wat gechilled bij de Higher Ground loungepyramide (opeengestapelde bankstellen) en daarna een stukje Def P and the Beatbusters meegepikt. Welgeteld het stukje:"Dankjewel Lowlands, jullie waren fantastisch en tot volgend jaar! Tot ziens!" Dus maar een biertje halen voor de schrik en een kijkje nemen bij A. Dat zoog, dus maar weer wat bier halen.

Ondertussen was in Charlie het bandje Rival Schools begonnen. Ik had online al wat nummers gehoord, maar live viel het toch wel wat tegen.
Tijd om wat te eten! Ondertussen naar Brainpower gewaggeld en ook daar nog net het laatste nummer meegekregen. Dan maar in de Alpha-tent wachten tot Nickelback begint...Biertje? Nou lekker!
Nickelback begint met een gigantisch vuurwerk. Nou ja, na 10 bier leek het gigantisch. Welgeteld drie nummers staan kijken en luisteren en dan besluiten dat Nickelback live best wel kut is.
Terug naar de danstent Bravo waar Timo Maas zijn set is begonnen. QT roept iets dat ze bier gaat halen en ik op mijn plek moet blijven staan. Maar na een paar minuten ben ik dat natuurlijk alweer vergeten en ga ik zelf een biertje halen. Waarna ik vergeet waar we ook al weer stonden. En dus QT kwijt. Nou ja, dan nog maar een biertje nemen en een Doner Kebab. Nog wat rondgestruind op het terrein, biertje gedronken, mooi boek gekocht over 10 jaar Lowlands en terug naar de tent. Waar iedereen vredig lag te slapen.

<b>Zaterdag</b>
Zaterdag begint rustig. Ontbijtje, drankje. Wel wat verontrustend naar de lucht kijken:"Als we het maar droog houden..." en ondertussen beloven niet meteen al aan het bier te beginnen. Dat is nog gelukt ook!
De ochtend beginnen we met een dansvoorstelling (!!!) in de Golftent. Kan me nou niet echt boeien, dus maar even CDtjes kijken in de Lowlands-CD-shop.
Toen waren we van plan om naar Scala te gaan, een vrouwenkoor die allerlei covers zingen, maar dat is om de een of andere reden niet gelukt. Waarschijnlijk algeheel rondloopgeneuzel en muntjes-haal-problematiek. En de regen, want ondertussen was het wel flink gaan plenzen.

Tijd om de beuk erin te gooien! <a href="http://www.peterpanspeedrock.nl/">Peter Pan Speedrock</a>! Hupsakee! Knallen! Een geweldige show!
Terug naar de Alpha voor de Lowlands Allstars. Een flink aantal nederlandse bands die ooit op LL hebben gestaan komen allemaal een liedje zingen. Leuk! Rude Boy, Def P, Di-Rect, Orphanage (denken we) en Hallo Venray hebben we gezien. Want toen was het tijd voor....<a href="http://www.beef.nl">Beef</a>! Open up the chopshop! Op een overvol veld voor het Dommelsch-podium (Waarom heet dat niet gewoon het Delta-podium...) krijgen ze wat straaltjes zonneschijn door de enorme hoosbuien. Topshit optreden!

Na Beef was het tijd voor de Raket-rendez-vous met <a href="http://www.mijnkopthee.nl">Bob</a>, <a href="http://www.charis.nl">Charis</a> en <a href="http://e-liner.hoevenet.nl/">Eline</a>. Erg gezellig! Jammer dat er niet meer loggers zijn op komen dagen, maar dat scheelde wel weer in de muntjes ;-)
Want de bierprijs viel wel mee (1,75) maar toch is zo'n festival in zijn totaliteit absurd duur!
Met hen dan ook naar de Alpha gelopen voor Incubus, maar in de Alpha tent weer teruggegaan, want QT was alles en iedereen kwijt en je vriendinnetje laat je natuulijk niet alleen staan! Had ik trouwens al verteld dat ik ondertussen tot op het bot natgeregend was? Dus maar even opdrogen bij de zwoele Braziliaanse klanken van Zuco 103. Maar dan toch weer de regen door voor Korn. De klapper van die dag. Wat kunnen die lui een show wegzetten zeg....Ze speelde maar twee of drie nummers van de nieuwe CD, verder voornamelijk hitjes, maar het dak ging er compleet vanaf!

Na Korn waren we echt op, dus terug naar de tent. En wat vinden we daar? We hebben een binnenzwembad in de tent! Onze medetentbewoners waren al naar de auto gevlucht om daar te overnachten. Helaas konden wij daar niet meer bij. Gelukkig was de tent zo gebouwd, dat de binnentent waar ons luchtbed lag niet onder water stond, dus met heeeeel voorzichting te zitten en heeeel stil liggen zijn we de nacht doorgekomen.

<b>Zondag</b>
Zondagochtend alles opgeruimd wat er nog op te ruimen viel. De tent is naar de klote, kleren gelukkig aardig droog en het bleek dat we precies in een afvoerkanaaltje van het water lagen. Vandaar dat die plek nog vrij was vrijdagochtend...
Het zonnetje schijnt, tijd voor leuke muziek! Helaas ging het hopseflopsen niet door, maar om 1 uur stond Dreadlock Pussy overtuigend de laatste slaap uit mijn ooghoeken te beuken. Hierna een verassend mooi optreden gezien van <a href="http://ubl.artistdirect.com/music/artist/bio/0,,1119705,00.html?artist=Hawksley+Workman">Hawksley Workman & The Wolves</a> in de India tent. Mooie popliedjes op een zonnige zondagmiddag. Heerlijk!

Later die middag eigenlijk pas de echte topper gezien: <a href="http://www.jayathecat.com">Jaya The Cat</a>. Raggende punkrock a la Rancid en The Clash met de dubreggaeska van de Long Beach Dub Allstars. Helemaal geweldig. Check ze zeker als ze weer in Nederland zijn! De Charlietent ging helemaal wild en het was ook voor het eerst dat weekend dat er gecrowdsurft werd. Tegen alle regeltjes in. Ik hoop dan ook dat mijn foto is gelukt van een crowdsurfer precies onder het verbodsbord voor het crowdsurfen.
Na Jaya nog even wat Underworld meegepikt, maar dat was gewoon te saai voor woorden. Een kwartier lang 4 bliepjes en wat geneuzel....tijd om naar huis te gaan...

Lowlands jaargang 10. Het was weer geweldig! Volgend jaar weer? Mja, ik denk het wel, maar dan wel met een camper of een bungalow in Six Flags. Geen tentje meer. Na 15 jaar festivallen ben ik dat nu toch wel een beetje beu geworden.";s:4:"body";s:0:"";}