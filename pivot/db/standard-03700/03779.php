<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:3779;s:4:"date";s:16:"2002-06-24-23-40";s:4:"user";s:6:"Punkey";s:5:"title";s:21:"About the Blogosphere";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-06-24-23-40";s:9:"edit_date";s:16:"2002-06-24-23-40";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1090:"Een geweldig leuk en leesbaar <a href="http://www.webword.com/interviews/hiler.html">interview</a> met John Hiller, schrijver van <a href="http://www.microcontentnews.com/">Microcontent News</a>

Een paar soundbites:

"Overall, I find the usability of most blogging software (aka blogware) to be extremely high - especially when compared to traditional content management software."

"I think the key is to blog about your passions.  If you're really passionate about something, it shows."

"Links are the glue of the blogosphere."

"If you want to hit #1 on Blogdex or Daypop, Friday, Saturday, and Sunday are good days to post something.  Weekends tend to be very low traffic.  If you publish on a Saturday night, your post gets linked to on Sunday and you'll be #1 on Monday, which can be a high traffic day."

"New community tools will continue to emerge: more powerful (and personalized) blog indices, distributed conversation trackers, referral trackers, rss tools, weblog neighborhood tracking, meme tracking, social network exploration tools..."

Zeer de moeite van het lezen waard!";s:4:"body";s:0:"";}