<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:4991;s:4:"date";s:16:"2003-02-02-15-35";s:4:"user";s:6:"Punkey";s:5:"title";s:26:"Yet another version of the";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-02-02-15-35";s:9:"edit_date";s:16:"2003-02-02-15-35";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:2756:"<A href="http://blogs.it/0100198/2003/02/01.html#a641">Yet another version of the Matrix: Revisited</A>
<P><A href="http://slashdot.org/article.pl?sid=03/02/01/158252">Warner Brothers Announce The Matrix: Special Edit</A> [<A href="http://slashdot.org/">Slashdot</A>]</P>
<P>On April 29th, Warner Brothers will release a <A href="http://dvd.ign.com/articles/384/384353p1.html">two-disc special edition of <I>The Matrix</I></A>, just in time for <I>The Matrix Reloaded</I>'s release in theaters. The special edition DVD will include all of the features from the original DVD release, <I>The Matrix Revisited</I>, and new features as well. Retail will be $27.95, and here's the rundown on the special features:</P>
<P><STRONG>Disc One:</STRONG></P>
<LI>Commentary by Carrie-Anne Moss, visual effects supervisor John Gaeta, and editor Zach Staenberg.
<LI>Isolated score track with commentary by Don Davis.
<LI><I>Making the Matrix</I> documentary.
<LI><I>Follow the White Rabbit</I> branching featurettes.
<LI><I>Take the Red Pill</I> effects documentaries.
<LI><I>What Is Bullet Time?</I> anatomy of Bullet Time.
<LI><I>What Is the Concept?</I> music montage that shows the progression from concept to final effect.<BR><BR><B>Disc Two:</B><BR>
<LI><I>The Matrix Revisited</I>
<LI>Six behind-the-scenes featurettes about <I>The Matrix</I>.
<LI><I>The Art Revisited</I>: multiple still galleries.
<LI><I>The Animatrix: The Second Renaissance Parts 1 and 2</I>
<LI><I>Preload: On the Set of Reloaded</I>
<LI><I>What Is the Game</I> videogame teaser.
<LI>Marilyn Manson music video.
<LI><I>The Matrix: Reloaded</I> music sampler.
<LI><I>The Animatrix</I> trailer.
<LI>Theatrical trailers and teasers.
<LI>TV Spots.
<LI>TheMatrix.com website preview.<BR><BR><B><A href="" target=_self></A>DVD ROM:</B><BR>
<LI>Interactive screenplay with storyboards, clips, and more.
<LI><I>Are You the One?</I> game.
<LI><I>From Strip to Screen</I> essay.
<LI><I>Everybody Loves Kung-Fu Fighting!</I> martial-arts movie featurette.
<LI>A sci-fi film retrospective, and more.
<LI>Website link.</LI>
<P>Along with the announcement of the <A href="http://dvd.ign.com/articles/384/384353p1.html">special edition DVD of <I>The Matrix</I></A>, new details have emerged about <I>The Animatrix</I> DVD that will hit stores on June 3rd. The DVD will retail for $24.95 and include a techno soundtrack featuring Juno Reactor, Death in Vegas, DJ Shadow and more. Also, it will include all nine <I>Animatrix</I> shorts that begin airing on <A href="http://www.thematrix.com/">TheMatrix.com</A> next week. </P>
<P><STRONG>[NOTE: The Animatrix is the anime series set in The Matrix.]</STRONG><BR></P>
<DIV align=right>[via <A href="http://blogs.it/0100198/">Marc's Voice</A>]
<DIV></DIV></DIV>";s:4:"body";s:0:"";}