<?php /* pivot */ die(); ?>a:13:{s:4:"code";i:6881;s:4:"date";s:16:"2004-03-13-13-47";s:4:"user";s:6:"Punkey";s:5:"title";s:35:"Onderzoek agressie Bredase portiers";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2004-03-13-13-47";s:9:"edit_date";s:16:"2004-03-13-13-47";s:8:"category";a:1:{i:0;s:11:"Streetstyle";}s:12:"introduction";s:799:"Herriner je je nog dat ik vorig jaar augustus om onverklaarbare redenen uit het Kerkplein in breda ben geknikkerd? Lees anders <a href="http://www.punkey.com/archives/2003/08/006317.html">dit stukje</a> nog eens door uit augustus vorig jaar. Nu blijkt afgelopen week een onderzoek te zijn ingesteld naar de portiers van dit nachtcafe. Ik copieer het artikel schaamteloos uit BN/De Stem omdat je je anders weer moet registreren. En dat zuigt. 

<b>Onderzoek agressie Bredase portiers</b>

Dinsdag 9 maart 2004 - BREDA � De politie onderzoekt of portiers van bar-dancing Kerkplein in Breda te agressief omgaan met uitgaanspubliek. Binnen een paar maanden zijn vijf aangiften wegens mishandeling tegen hen gedaan. Politievoorlichter R. van Kuik: �Wij onderzoeken nu of dit een structureel probleem is.�";s:4:"body";s:2556:"Eigenaar L. Meijer van Kerkplein is niet onder de indruk: �Ik heb nog nooit iemand gesproken die zegt terecht een klap te hebben gehad. Onze beveiligingsmensen hebben bovendien een prima verhouding met de politie.�

De 22-jarige Bredase student M. van Nispen deed in de nacht van zaterdag op zondag aangifte van mishandeling door twee portiers en de bedrijfsleider van Kerkplein.

Hij moest in het ziekenhuis aan zijn verwondingen worden behandeld. De portiers deden op hun beurt aangifte tegen Van Nispen, die volgens hen geslagen zou hebben.

De student ontkent dat. Hij zegt door de bedrijfsleider ruw te zijn geknepen toen hij via een verkeerde trap zijn jas wilde halen. �Ik ben rustig gebleven, heb mijn jas via de normale weg gehaald en heb bij het weggaan gezegd dat ik er nooit meer zou komen. Toen kwam de bedrijfsleider me achterna en kneep op straat met zijn arm mijn keel dicht. Ik stikte bijna en zwaaide met mijn armen. Daarna gooiden de twee portiers me op de grond en begonnen me te trappen.�

L. Mulders (22), die niet gedronken had omdat zij nog moest rijden, was getuige van het incident.

�Die jongen spuugde bloed, maar werd toch weer tegen de grond gewerkt door de portiers.�

De portiers belden de politie. Die voerde Van Nispen geboeid af en meldde de pers dat een 22-jarige wegens mishandeling was aangehouden.

Van Nispen is ontzet. �Ik heb nota bene aangifte gedaan. De politie heeft mijn gezicht nog schoongemaakt en een foto gemaakt van mijn verwondingen.� Van Nispen heeft twee hechtingen in zijn lip, last met slikken en heeft pijn in rug en buik vanwege de trappen die hij zegt te hebben gekregen toen hij op de grond lag. Hij zegt nooit eerder problemen te hebben gehad. �Ik wil niet dat deze mannen hier zo mee wegkomen.� Getuige L. Mulders: �Je staat gewoon machteloos. Het is zo onterecht dat iemand zomaar wordt geslagen en getrapt voor je ogen. Ik had het gevoel dat de portiers een reden zochten om iemand te pakken.� Eigenaar Meijer van Kerkplein heeft van de politie de vraag gekregen om videobeelden te leveren van het betreffende voorval. �Die moeten we nu allemaal weer op gaan zoeken. Zonde van het werk en de tijd�, vindt Meijer. Hoe het zit met de andere vier aangiften wegens mishandeling kon politievoorlichter Van Kuik gisteren niet zeggen. Het onderzoek kan nog wel even duren. �Dit zijn vaak lastige zaken met veel tegenstrijdige verklaringen.�

(Bron: <a href="http://www.bndestem.nl/regioportal/BNS/1,3112,1318-Archief-Zoeken!Innieuws!__2048034_,00.html?ArchiefID=2048034">BN/De Stem</a> 9 maart 2004)";s:8:"comments";a:1:{i:0;a:6:{s:4:"name";s:7:" Entity";s:5:"email";s:1:" ";s:2:"ip";s:14:" 213.17.86.130";s:4:"date";s:16:"2004-03-14-14-43";s:3:"url";s:1:" ";s:7:"comment";s:202:"Ik hoop dat die opgefokte gorilla's van Meijer en Meijer eens aangepakt worden. 

Trieste figuren.

Bij de Graanbeurs heeft een portier (die piraat :P) eens een jongen letterlijk naar buiten gevoetbalt.";}}}