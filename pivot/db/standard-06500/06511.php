<?php /* pivot */ die(); ?>a:13:{s:4:"code";i:6511;s:4:"date";s:16:"2003-10-21-12-43";s:4:"user";s:7:"Da_Huge";s:5:"title";s:27:"Voor de jongeren onder ons!";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-10-21-12-43";s:9:"edit_date";s:16:"2003-10-21-12-43";s:8:"category";a:1:{i:0;s:5:"Humor";}s:12:"introduction";s:159:"Voor iedereen die denkt dat ze nog jong zijn, eh,
helaas. Open je ogen. De meeste studenten die dit jaar beginnen op de
universiteit zijn geboren in .... 1985.";s:4:"body";s:1818:"Zij hebben nooit: " We are the world, we are the children... of "Sunday bloody
Sunday " gezongen. 

Ze zijn te jong om zich de explosie van de Challenger te herinneren.

Voor hen hebben AIDS en werkloosheid altijd bestaan.

Een Twix heeft nooit een Raider geheten.

Ze hebben nog nooit gedemonstreerd tegen kernraketten.

Ze hebben nooit met een Atari of Commodore 64 gespeeld, ze hebben zelfs nog
nooit van Pac-Man of Pong gehoord!

Ze hebben nog nooit een 5 1/4 inch diskette gezien.

De CD kwam op de markt toen zij net een jaar oud waren. Ze hebben nooit een
single van Doe Maar gehad.

Ze weten niet wat het is om op de lange golf naar de radio te luisteren.

De meesten weten niet hoe vroeger de TV's eruit zagen, ze kunnen zelf niet
verklaren hoe ze zonder afstandsbediening kunnen functioneren en hoe je
zwart-wit kunt kijken.

Ze hebben nog nooit "Startrek", "Wicky de Viking", "Maja de bij" gekeken.

Ze denken dat James Bond altijd Pierce Brosnan is geweest.

Een studiebeurs die geen lening is en waar je niet voor hoeft te presteren is
voor hen een utopie.

Ze denken nooit aan de tune van "Jaws" als ze in zee zwemmen.

Ze denken de een telefoon "met draad" science fiction is.

Rolschaatsen hebben voor hen altijd wieltjes op 1 rij gehad.

Voor hen is Michael Jackson altijd al wit geweest.

Ze weten niet dat Travolta kan dansen (op zaterdagavond).

Ze weten niet wat voor klootzak JR is (en wie op hem geschoten heeft). 

Ze denken dat "Charlie's Angels" en Mission Impossible gewoon films zijn die
het afgelopen jaar zijn uitgekomen.

Ze weten niet dat de regisseur Ron Howard Richie speelde in Happy Days.

Ze denken dat Picasso een naam van een auto is.

Ze weten niet beter dan dat den Uyl een vogel is.

En dan te bedenken dat dit de mensen zijn die nu gaan studeren. Zij zijn de
jongeren van nu.";s:8:"comments";a:8:{i:0;a:6:{s:4:"name";s:5:" Daan";s:5:"email";s:1:" ";s:2:"ip";s:13:" 213.84.33.68";s:4:"date";s:16:"2003-10-21-13-06";s:3:"url";s:1:" ";s:7:"comment";s:319:"Zeg opa!! Ik ken echt wel de Raider, heb een CD van Doe Maar (comeback!!), heb mijn hele jeugd Pac-Man gespeeld, thuis hadden wij nog een TV zonder afstandsbediening, Startrek komt nog op TV, rolschaatsen zijn juist hartstikke populair, en dat van die Happy Days gast wist ik (maar dat is meer interesse)... nou U weer.";}i:1;a:6:{s:4:"name";s:7:" Punkey";s:5:"email";s:18:" punkey@punkey.com";s:2:"ip";s:15:" 213.84.172.170";s:4:"date";s:16:"2003-10-21-13-12";s:3:"url";s:1:" ";s:7:"comment";s:30:"@Daan: Ja maar hou oud ben je?";}i:2;a:6:{s:4:"name";s:5:" T-Jo";s:5:"email";s:1:" ";s:2:"ip";s:16:" 131.155.224.248";s:4:"date";s:16:"2003-10-21-13-32";s:3:"url";s:1:" ";s:7:"comment";s:223:"Volgens mij gelden voor de meesten uit 1985 alleen de stellingen over de demonstratie, de studiebeurs en evt ook Mission Impossible. Vooral dankzij alle herhalingen op tv weliswaar, maar toch. Ik ben zelf van bouwjaar 1982.";}i:3;a:6:{s:4:"name";s:6:" peter";s:5:"email";s:1:" ";s:2:"ip";s:14:" 195.35.148.32";s:4:"date";s:16:"2003-10-21-16-20";s:3:"url";s:23:" http://www.shapeamp.nl";s:7:"comment";s:63:"Haha! En hoe ouder je wordt hoe meer je de jongeren onderschat.";}i:4;a:6:{s:4:"name";s:11:" Verbal Jam";s:5:"email";s:1:" ";s:2:"ip";s:14:" 146.50.171.89";s:4:"date";s:16:"2003-10-21-23-11";s:3:"url";s:24:" http://www.verbaljam.nl";s:7:"comment";s:60:"En dan hebben we het nog niet eens over de 80-jarige oorlog!";}i:5;a:6:{s:4:"name";s:6:" Djasp";s:5:"email";s:18:" jasper@luiken.net";s:2:"ip";s:16:" 213.204.211.116";s:4:"date";s:16:"2003-10-22-12-22";s:3:"url";s:29:" http://www.jereinsteonzin.nl";s:7:"comment";s:252:"Ik weet er nog wel een paar: Ze zijn te jong om te weten dat Linda de Mol is doorgebroken op Sky Channel met een kinderprogramma (waar Inspector Gadget een cartoonserie was, en geen film) met DJ Kat. (en dus weten ze ook niet dat Dj Kat een pop was...)";}i:6;a:6:{s:4:"name";s:15:" Dr Strangelove";s:5:"email";s:21:" pfea1964@hotmail.com";s:2:"ip";s:13:" 212.187.37.8";s:4:"date";s:16:"2003-10-22-23-56";s:3:"url";s:1:" ";s:7:"comment";s:141:"Aaah, Sky Channel en Music Box. Voor het eerst 24/7 tv in Nederland. Grote metalen emmers met deksel als vuilnisbak langs de kant van de weg.";}i:7;a:6:{s:4:"name";s:5:" Flep";s:5:"email";s:19:" floris@funbreak.nl";s:2:"ip";s:15:" 130.89.123.226";s:4:"date";s:16:"2003-10-25-22-50";s:3:"url";s:23:" http://sms.funbreak.nl";s:7:"comment";s:127:"Tsja, ik ben van 83, ben vorig jaar officieel gaan studeren en weet vrijwel alles. Waarschijnlijk net voor de kritieke grens ;)";}}}