<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:6591;s:4:"date";s:16:"2003-11-14-11-51";s:4:"user";s:6:"Punkey";s:5:"title";s:23:"CNet neemt mp3.com over";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-11-14-11-51";s:9:"edit_date";s:16:"2003-11-14-11-51";s:8:"category";a:1:{i:0;s:5:"Music";}s:12:"introduction";s:826:"Zojuist in de mail ontvangen:
"<a href="http://www.cnet.com">CNET Networks</a>, Inc announced today that it has acquired certain assets of <a href="http://www.MP3.com">MP3.com</a>, Inc.

Please be advised that on Tuesday, December 2, 2003 at 12:00 PM PST the MP3.com website will no longer be accessible in its current form. CNET Networks, Inc. plans to introduce a new MP3 music service in the near future.
[..]
MP3.com is not transferring your personal information to CNET Networks, Inc. or any other third party.

On behalf of all of us at MP3.com we thank you for your patronage and continued support. It has been a privilege to host one of the largest and most diverse collections of music in the world. MP3.com wishes to express its sincere thanks to each of you for making us your premier destination for music online."";s:4:"body";s:0:"";}