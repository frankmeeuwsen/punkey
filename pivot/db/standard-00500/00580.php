<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:580;s:4:"date";s:16:"2001-05-27-11-00";s:4:"user";s:6:"Punkey";s:5:"title";s:32:"Het was een hele inspiratievolle";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-05-27-11-00";s:9:"edit_date";s:16:"2001-05-27-11-00";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:2245:"Het was een hele inspiratievolle avond. Interessante mensen gesproken zoals <a href="http://www.evhead.com">Evan</a>, Cami, <a href="http://www.curry.com">Adam</a>, <a href="http://www.scripting.com">Dave</a>, <a href="http://Www.nieske.net/nieske.html">Nieske</a>, <a href="http://class6f.manilasites.com/">Peter Ford</a> en <a href="http:// owrede.khm.de ">Oliver</a>. Veel ideeen en richtingen opgedaan. Het is niet te vergelijken met de andere weblogmeetings. Op de een of andere manier ging het hier dieper, intenser, abstracter dan op een nederlandse <a href="http://weblogmeeting.mrgreen.nl">meeting</a>.

De gespreksonderwerpen varieerden enorm. Van intelligente contentsystemen naar de integratie van weblogs in bedrijven. Verschrikkelijk abstract, maar niet minder boeiend, was Olivers betoog over de socio-psychologische aspecten van communicatie tussen mensen. Veel onderwerpen hebben me wel behoorlijk aan het denken gezet. Voornamelijk alle verschillende invalshoeken die ik overal hoor en lees, die steeds op hetzelfde neerkomen: mensen hebben via internet de kracht om heel makkelijk te communiceren. Weblogs zijn het medium.
Erg interessant was (weer) Olivers verhaal over het feit dat vaste gewoontes het leven vergemakkelijken en je minder laten nadenken over ouder worden en de dood. Weblogs zijn daar ook een onderdeel van. Vooral de eindconclusie was het beste (copyright Evan en ondergetekende):

<b>Use Blogger, live longer.</b>

We hebben ook een leuke discussie gehad over de definitie van een weblog. We zijn er niet echt uitgekomen. Een van de mooiste is: weblogs are a metatext to the Internet.  Covert het niet helemaal, maar wel mooi. Ik vind mijn definitie nog steeds de moeite waard: weblogs zijn de onderbuik van het Internet.

Nog een paar soundbites:
- Congratulations to the estonian cameraman for winning the songcontest
- Oliver: 'This is a freakshow!
- Jan-Willem, mail me de 'hidden footage' van de vorige weblogmeeting!
- Nieske, leuk je te hebben ontmoet! Misschien tot vrijdag?
- Mark singing operasongs to us :-)
- Hearing about the new featus in Blogger! Nog een weekje of 2 geduld!
- Where's the spellchecker? (Yes Evan, it IS in your sourcecode!)

Erg de moeite waard en wordt zeer zeker vervolgd!";s:4:"body";s:0:"";}