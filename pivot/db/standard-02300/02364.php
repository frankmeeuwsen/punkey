<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:2364;s:4:"date";s:16:"2001-12-04-15-25";s:4:"user";s:6:"Punkey";s:5:"title";s:31:"Doen jullie ook mee? Onderwerp:";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-12-04-15-25";s:9:"edit_date";s:16:"2001-12-04-15-25";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:2857:"Doen jullie ook mee?

Onderwerp: Amnesty International: Teken voor een coalitie voor mensenrechten
Van: stop.martelen@amnesty.nl
Aan: punkey@punkey.com

De Oezbeekse dichter Yusuf Dzhumaev is op 23 oktober 2001gearresteerd voor het plaatsen van gedichten op een islamitische website. In de gevangenis is hij gemarteld.

Yusuf Dzhumaev is niet het enige slachtoffer van vermeende terrorismebestrijding. Ook in andere landen worden fundamentele mensenrechten geschonden bij het bestrijden van terrorisme. Zoals in Afghanistan waar vluchtelingen vastzitten omdat voor hen de Pakistaanse grens niet wordt geopend.

Amnesty International vindt dat terrorismebestrijding nooit een excuus mag zijn om mensenrechten te schenden. Daarom roept Amnesty International alle wereldleiders op
om de mensenrechten in dit conflict te respecteren, om een coalitie voor mensenrechten te vormen.

Steunt u deze oproep? Stuur deze mail dan door naar <a href="mailto: oproep@amnesty.nl?subject:Ik teken">oproep@amnesty.nl </a>(subject: ik teken). U zet daarmee uw handtekening onder de oproep aan wereldleiders.

Om deze oproep kracht te geven is zo veel mogelijk steun van de Nederlandse bevolking nodig. Stuur deze mail daarom door naar vrienden en bekenden.

De eerste handtekeningen worden op 10 december 2001, de Internationale Dag van de Rechten van de Mens, aangeboden aan vertegenwoordigers van het parlement. Dit gebeurt tussen 13.00 en 14.00 op de Dam in Amsterdam. De actie loopt tot 31 december.

Wilt u meer informatie over de actie kijk dan op <a href="http://www.amnesty.nl">www.amnesty.nl</a> of bel naar 020 626 4436 en vraag naar de Afdeling Actie.

Bedankt!

Amnesty International

Iedereen die tekent verklaart met die handtekening:

1) ten diepste te zijn geschokt door de aanslagen van 11 september
2) dit soort daden te beschouwen als extreme schendingen van de rechten van de mens, die moeten worden voorkomen en bestreden en waarvan daders moeten worden berecht
3) dat regeringen die zich verbinden in de strijd tegen het terrorisme maatregelen moeten nemen om alle ernstige schendingen van de rechten van de mens overal ter wereld
tegen te gaan
4) erop aan te dringen dat regeringen:
- in hun eigen acties ter bestrijding van extreem geweld de mensenrechten en het internationaal humanitair recht in acht nemen
- geen wapens leveren aan legers en andere strijdgroepen die zich schuldig maken aan schendingen van mensenrechten en het internationaal humanitair recht
- maatregelen steunen om straffeloosheid voor ernstige schendingen van de mensenrechten en het internationaal humanitair recht terug te dringen, zoals bekrachtiging
van het Statuut van het Internationaal Strafhof
- zorgen dat mensen die hun land ontvluchten omdat lijf en leden worden bedreigd, fatsoenlijk worden opgevangen en niet worden teruggestuurd als ze vervolging te vrezen
hebben";s:4:"body";s:0:"";}