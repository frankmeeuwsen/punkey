<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:1674;s:4:"date";s:16:"2005-02-02-15-21";s:12:"introduction";s:823:"OK, this is buggin' me (hehehe) for quite some time now. I have the "GTD Outlook Add in":http://www.davidco.com/productDetail.php?id=63 installed on Outlook 2003. I also have the normal Outlook bars (standard, formatting) and the Lookout Searchbar (worth an evaluation! "It's free!":http://www.microsoft.com/downloads/details.aspx?FamilyID=09b835ee-16e5-4961-91b8-2200ba31ea37&DisplayLang=en)
When I open a new Task, i would like my toolbars to stay in a specific position. But no matter what I try or what I do, the positions of the toolbars _al-ways_ change. This is _so_ annoying! Toolbars switch positions, slide al the way to the right, position themselves in three (3!!) rows. Does anyone have a solution for this? 
And yes, I have to use Outlook at work, so switching to Thunderbird is not a valid option, sorry...";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2005-02-02-15-15";s:9:"edit_date";s:16:"2005-02-02-15-21";s:5:"title";s:28:"Buggy GTD add-in in Outlook?";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:2:{i:0;a:8:{s:4:"name";s:4:"Phil";s:5:"email";s:30:"phil.rodemann@conagrafoods.com";s:3:"url";s:0:"";s:2:"ip";s:14:"204.76.113.254";s:4:"date";s:16:"2005-02-02-16-36";s:7:"comment";s:205:"I have had the same issue with the GTD add-in. I was not able to find a solution but it has stopped doing it. My belief is that it's related to the same bug that doesn't close the Outlook mailbox properly.";s:10:"registered";i:0;s:6:"notify";s:1553:"N;}i:1;a:8:{s:4:"name";s:6:"Robert";s:5:"email";s:26:"robert@internetwebzone.com";s:3:"url";s:0:"";s:2:"ip";s:14:"216.63.184.222";s:4:"date";s:16:"2005-02-02-17-01";s:7:"comment";s:495:"I have been able to put the bars in the correct place and have them stay there. (I use GTD, Lookout, PlanPlus 3 (FranklinCovey), Anagram, Cloudmark Safety Bar and X1 add ins.) Try this:
1. Reboot
2. Open Outlook
3. Open New Task
4. Set menu bars the way you want them.
5. Close the task by hitting the X in the uppper left hand corner.
6. Open a new task, and it should be the way you want it. If it doesn't work, contact me and I will try to help you some more.
Have a great day!
Robert";s:10:"registered";i:0;s:6:"notify";s:1:"1";}i:2;a:8:{s:4:"name";s:4:"John";s:5:"email";s:24:"donotsolicitme@gmail.com";s:3:"url";s:0:"";s:2:"ip";s:13:"66.207.106.86";s:4:"date";s:16:"2005-02-05-07-08";s:7:"comment";s:606:"I thought you were going to talk about how annoying it is that you can't shut down Outlook after installing lookout search & the GTD toolbar.  Speaking of which, you're right about Lookout, it's totally worth trying.

About positioning your toolbar, I know that if you want to force a window to retain a given state you have to open the window, position it the way you like, hold down the shift key while clicking on the X in the top right corner.   Then re-open the window.  

It's basically what Robert just said, but if his approach doesn't work, try using the shift key as described above.

John.";s:10:"registered";i:0;s:6:"notify";N;}}}";}s:1553:"N;}i:1;a:8:{s:4:"name";s:6:"Robert";s:5:"email";s:26:"robert@internetwebzone.com";s:3:"url";s:0:"";s:2:"ip";s:14:"216.63.184.222";s:4:"date";s:16:"2005-02-02-17-01";s:7:"comment";s:495:"I have been able to put the bars in the correct place and have them stay there. (I use GTD, Lookout, PlanPlus 3 (FranklinCovey), Anagram, Cloudmark Safety Bar and X1 add ins.) Try this:
1. Reboot
2. Open Outlook
3. Open New Task
4. Set menu bars the way you want them.
5. Close the task by hitting the X in the uppper left hand corner.
6. Open a new task, and it should be the way you want it. If it doesn't work, contact me and I will try to help you some more.
Have a great day!
Robert";s:10:"registered";i:0;s:6:"notify";s:1:"1";}i:2;a:8:{s:4:"name";s:4:"John";s:5:"email";s:24:"donotsolicitme@gmail.com";s:3:"url";s:0:"";s:2:"ip";s:13:"66.207.106.86";s:4:"date";s:16:"2005-02-05-07-08";s:7:"comment";s:606:"I thought you were going to talk about how annoying it is that you can't shut down Outlook after installing lookout search & the GTD toolbar.  Speaking of which, you're right about Lookout, it's totally worth trying.

About positioning your toolbar, I know that if you want to force a window to retain a given state you have to open the window, position it the way you like, hold down the shift key while clicking on the X in the top right corner.   Then re-open the window.  

It's basically what Robert just said, but if his approach doesn't work, try using the shift key as described above.

John.";s:10:"registered";i:0;s:6:"notify";N;}}}";s:1553:"N;}i:1;a:8:{s:4:"name";s:6:"Robert";s:5:"email";s:26:"robert@internetwebzone.com";s:3:"url";s:0:"";s:2:"ip";s:14:"216.63.184.222";s:4:"date";s:16:"2005-02-02-17-01";s:7:"comment";s:495:"I have been able to put the bars in the correct place and have them stay there. (I use GTD, Lookout, PlanPlus 3 (FranklinCovey), Anagram, Cloudmark Safety Bar and X1 add ins.) Try this:
1. Reboot
2. Open Outlook
3. Open New Task
4. Set menu bars the way you want them.
5. Close the task by hitting the X in the uppper left hand corner.
6. Open a new task, and it should be the way you want it. If it doesn't work, contact me and I will try to help you some more.
Have a great day!
Robert";s:10:"registered";i:0;s:6:"notify";s:1:"1";}i:2;a:8:{s:4:"name";s:4:"John";s:5:"email";s:24:"donotsolicitme@gmail.com";s:3:"url";s:0:"";s:2:"ip";s:13:"66.207.106.86";s:4:"date";s:16:"2005-02-05-07-08";s:7:"comment";s:606:"I thought you were going to talk about how annoying it is that you can't shut down Outlook after installing lookout search & the GTD toolbar.  Speaking of which, you're right about Lookout, it's totally worth trying.

About positioning your toolbar, I know that if you want to force a window to retain a given state you have to open the window, position it the way you like, hold down the shift key while clicking on the X in the top right corner.   Then re-open the window.  

It's basically what Robert just said, but if his approach doesn't work, try using the shift key as described above.

John.";s:10:"registered";i:0;s:6:"notify";N;}}}";}}