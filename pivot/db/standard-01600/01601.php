<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:1601;s:4:"date";s:16:"2004-12-08-18-56";s:12:"introduction";s:390:"I got a question how I configured "Slogger":http://www.kenschutte.com/firefoxext/ to make RSS-feeds (we call them webfeeds in the Netherlands, but oh well). When I wrote the mail, I thought to myself: Why not share it with everyone? So if you use Slogger and would like to make RSS feeds of your saved pages wo you can read them in your feedreader. Check this little tutorial with pictures!";s:4:"body";s:2451:"First off, make sure you have "the latest version":http://www.kenschutte.com/firefoxext/
If you use the latest version, go to All settings. 

[[image:rsstutorial_1.jpg::center:0]]

You can add  a profile. Choose Add and name it "RSS"

[[image:rsstutorial_2.jpg::center:0]]

In the tabs General and Save Pages fill in your own preferences. In the tab logfile, I choose a single name for the logfile like "sloggerLog.xml"

[[image:rsstutorial_3.jpg::center:0]]

The entry is build as follows

<pre>
<item>
      <title>$title</title>
      <link>C:\path.to\datadirectory\$datadir\$htmlfile</link>
      <comments>$datadir/$htmlfile</comments>
      <description>$host</description>
      <content:encoded><![CDATA[$host]]></content:encoded>
      <dc:subject>default</dc:subject>
      <dc:date>$year-$month-$dayT$hour:$minute$second+01:00</dc:date>
</item>
</pre>

In <link> I made a hard link to the directory where my saved pages are. Make sure you type the path to your own directory!!! In the <description> I just added the host. No real reason...
Make sure you tick the checkbox "Escape XML characters..."
Check [[popup:rsstutorial_4.jpg:this screenshot::0:inline]]

In the tab "logfile head/foot" I have the following
<pre>
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" 
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" 
xmlns:admin="http://webns.net/mvcb/" 
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
xmlns:content="http://purl.org/rss/1.0/modules/content/" 
xmlns:creativeCommons="http://backend.userland.com/creativeCommonsRssModule" 
xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" version="2.0">
  <channel>
    <title>Local saved pages</title>
    <link>$datadir</link>
    <description>Via slogger</description>
    <dc:language>en-us</dc:language>
    <dc:creator />
    <dc:rights>Copyright 2004</dc:rights>
    <dc:date>$year-$month-$dayT$hour:$minute$second+01:00</dc:date>
    <sy:updatePeriod>hourly</sy:updatePeriod>
    <sy:updateFrequency>1</sy:updateFrequency>
    <sy:updateBase>2000-01-01T12:00+00:00</sy:updateBase>
</pre>

In the footer I have 

<pre>
  </channel>
</rss>
</pre>

[[image:rsstutorial_5.jpg::center:0]]

Logfile style and Filters I just leave as is. 

So there you have it! Easy huh? In your feedreader, add the link to you xml file and you're done!

Happy slogging!";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2004-12-08-18-46";s:9:"edit_date";s:16:"2004-12-09-00-35";s:5:"title";s:34:"Using Slogger as a webfeed-builder";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:1:{i:0;a:9:{s:4:"name";s:6:"miguel";s:5:"email";s:18:"foloolk3@potran.gu";s:3:"url";s:56:"http://scripts.cgispy.com/forums/forum.cgi?user=maida2yc";s:2:"ip";s:12:"68.87.64.104";s:4:"date";s:16:"2006-03-14-15-54";s:7:"comment";s:45:"Follow your dreams, you can reach your goals.";s:10:"registered";i:0;s:6:"notify";s:0:"";s:8:"discreet";s:0:"";}}}