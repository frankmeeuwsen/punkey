<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:184;s:4:"date";s:16:"2000-11-26-11-36";s:4:"user";s:6:"Punkey";s:5:"title";s:33:"Web Used for Window-Shopping, Not";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2000-11-26-11-36";s:9:"edit_date";s:16:"2000-11-26-11-36";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:919:"<a href="http://www.internetnews.com/ec-news/article/0,,4_518541,00.html">Web Used for Window-Shopping, Not Buying</a>
Daar sta je dan met je pas geopende winkel op Internet. De duurste consultants van de grootste bureaus hebben je geadviseerd om alle soorten betalingen aan te nemen, logistiek is ook alles tiptop in orde, en dan lees je:
"According to the study:
65 percent are sticking with the old-fashioned method as their primary form of shopping - researching and buying at a local retailer.
56 percent use the Web to compare prices.
46 percent go online to compare models or brands.
46 percent use the Internet to obtain gift ideas.
44 percent are surfing to find a particular gift.
21 percent of respondents characterize their shopping behavior as researching online and buying locally.
5 percent chose researching and buying products online as the best description of their shopping behavior. "

Da's balen...";s:4:"body";s:0:"";}