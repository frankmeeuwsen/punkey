<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:6006;s:4:"date";s:16:"2003-06-24-10-07";s:4:"user";s:7:"Da_Huge";s:5:"title";s:58:"3500 Euro binnengehaald voor het Koningin Wilhelmina Fonds";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-06-24-10-07";s:9:"edit_date";s:16:"2003-06-24-10-07";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:6265:"<p>Eigenlijk was ik van plan om een gedegen en officieel stuk te schrijven als conclusie van 2Dj's>1Case, Dj's tegen kanker.<br />
Maar omdat het evenement zo'n enorm succes is geworden en de positiviteit en gezelligheid zo overweldigend was wil ik er toch een persoonlijke noot aan hangen.<br />
22 Juni stond voor mij in het teken van de strijd tegen kanker, een strijd die voor veel mensen dichtbij komt of dichtbij is geweest, zo ook voor mij.<br />
30 April is mijn moeder overleden aan de gevolgen van kanker, ze is bijna 4 jaar ziek geweest.<br />
Als een vorm van verwerking en therapie vatte ik het idee op om een dj middag te organiseren die 2 doelen diende: laten zien dat er in Breda op een zondagmiddag een leuk feestje gebouwd kon worden en als 2e doel zoveel mogelijk geld inzamelen voor de strijd tegen kanker en deze te doneren aan het Koningin Wilhelmina Fonds.<br />
Het idee speelde al langer, maar in de eerste week van juni hakte ik samen met Ronald van caf� De Speeltuin de knoop door en zetten we de datum op de 22ste.<br />
Toen kon het echte werk beginnen, de dj's waren in principe al geregeld, de locatie was vastgesteld en ik had voor mezelf al een invulling gegeven aan hoe het uit de verf moest komen.</p>

<p>Ik zal de details besparen van het voorbereiden, maar wel kan ik zeggen dat dit meer dan voorspoedig verliep, horeca Breda werkte volledige belangenloos mee in alle vormen.<br />
Het was zelfs zo dat 4 dagen voor het evenement ik nog een telefoontje kreeg van een bedrijf dat gratis geluid, licht en een podium beschikbaar stelde.<br />
Binnen 3 weken hadden we dus alles tot in de puntjes kunnen regelen.<br />
Inmiddels had ik al mijn media bronnen aangeboord en 90% reageerde meteen positief en plaatste artikelen, noemde het in hun radioprogramma en vermeldde het op hun website.<br />
Daarnaast had ik de toonaangevende figuren in de bredase dancescene ingelicht en zij maakten hun omgeving enthousiast om zondagmiddag de tuin van cafe de Speeltuin te vullen.</p>

<p>Afgelopen zondag was het uur U dus aangebroken, de hele week leefde ik al toe naar het evenement en om half twaalf stond ik voor de deur om de handen uit de mouwen te steken om de tuin helemaal om te toveren tot een tuin waarin mensen konden dansen, lekker praten met vrienden en relaxen als ze te lang hadden gedanst.<br />
Tot op de seconde haalden we het net zodat om 16:00 stipt de eerste bezoekers binnen konden komen.<br />
Om 16:30 stond er al een aanzienlijk aantal mensen in de tuin en er was geen afwachtende houding te merken, de mensen stonden met een big smile al meteen dansend in de tuin.<br />
De middag ging bijzonder snel aan mij voorbij, maar enkele hoogtepunten zijn mij zeer zeker bijgebleven.<br />
Zo zomers dat dj El Nino begon zo lekker zette dj Mark Green de sfeer voort en de inmiddels toegestroomde mensen stonden allemaal te dansen.<br />
Een korte stroomstoring kon Dj Mo Franco niet van zijn stuk brengen en MC Stanford maakte samen met het publiek de 5 stille minuten tot een ware samenhang die misschien zelfs niet misstaan had in het flower power tijdperk.<br />
Dj Barry draaide de soundtrack van deze middag, I Got So Much Love To Give en werkelijk de hele tuin zong dit nummer mee.<br />
Zelf mocht ik ook nog draaien en bracht het publiek in stelling voor dj C-Mon.<br />
Toen was het tijd voor het muzikale huzarenstukje van de middag, de verrassing die ik voor het enthousiaste publiek in petto had werd met luid gejuich ontvangen, een unieke en eenmalige back2back set van Dj Cor Fijneman en Lucien Foort.<br />
Voordat het zover was kon ik het publiek toespreken over het idee achter de middag en daarbij de gemeente Breda aanspreken over het weigeren van de vergunning.<br />
Het viel me op dat het publiek aandachtig luisterde naar wat ik te vertellen had en enthousiaste en positief reageerde op het initiatief.<br />
Toen kon ik ook de cheque uitreiken aan Adrienne Verboom van het KWF Breda.<br />
Op dat moment hadden we slechts aan de kaartverkoop al 1750 euro verdiend, later bleek dat we dit verdubbeld hebben tot een uiteindelijke opbrengst van 3500 euro.<br />
Het bedrag werd met luid gejuich verwelkomt en twee bezoekers namen de collectebus spontaan in de hand om nog eens flink rond te gaan (wat dus flink resultaat heeft opgeleverd!).<br />
Zelfs de dj's gaven tijdens het draaien een flink bedrag ondanks dat ze zelfs al helemaal gratis hun kunsten kwamen vertonen.<br />
Om 22:00 mochten ze allebei nog een kleine toegift geven, maar toen was het mooi geweest, tot enthousiasme van het publiek besloten Lucien en Cor om binnen hun set nog voort te zetten zodat iedereen nog verder kon feesten.</p>

<p>De middag straalde van positiviteit en het had de sfeer van een mooi festival, en dat alles in het centrum van Breda.<br />
De hele middag en avond is er geen wanklank gevallen, ondanks het feit dat het publiek zo ontzettend gemeleerd was.<br />
Alle rangen en standen, alle leeftijden en alle kleuren waren aanwezig en hebben gezorgd voor een evenement wat volgens velen als het beste en meest positieve housefeest van Breda de geschiedenisboeken in zal gaan.</p>

<p>Ik ben zelf dan ook bijzonder trots dat we naast deze zo ontzettend positieve sfeer en saamhorigheid met 450 mensen hebben gezorgd voor een zeer mooi bedrag van 3500 euro.<br />
Dit bedrag zal worden overgemaakt aan de Kankerbestrijding, het Koningin Wilhelmina Fonds.</p>

<p>2Dj's>1Case draag ik op aan mijn moeder, Toos de Graaf, mijn inspiratie bron.</p>

<p>Deze mensen en organisaties maakten 2Dj's>1Case tot een groot succes:<br />
Speeltuin (vooral Ronald en Brian), TMR Rental Light & Sound, Caf� Dinges, Catering van Ham, De Graanbeurs, Magik Store<br />
Tunes Ville, Rhinofly, Spektrum, El Nino, Mark Green, Mo-Franco, Dj Barry, C-Mon, Cor Fijneman, Lucien Foort, Stanley Foort, Dirk Jan Zwartveld, Guus de Graaf, Iris Van Zomeren, Frank Meeuwsen, Helie Schmetz,al het barpersoneel, Cedric the bouncer, Petra de Goede, Jessica en al het enthousiaste party publiek van Breda!</p>

<p>Foto's zijn te vinden op www.frshdjs.com, www.dancelinx.com, www.housemama.net en www.bruisendbreda.nl<br />
Er zal op www.frshdjs.com voor 4 juli een korte documentaire te zien zijn gemaakt door Frshdjs.com en Punkey.com.</p>";s:4:"body";s:0:"";}