<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:1466;s:4:"date";s:16:"2004-10-17-16-30";s:12:"introduction";s:1200:""Visiting the website is dating; getting a daily e-mail is going steady -- but subscribing to an RSS feed, well, that is like getting married to a news source," <a href="http://www.wired.com/news/technology/0,1282,65347,00.html" title="">Jason Calacanis, founder of Weblogs.com, says</a>. "It's really the highest commitment you can make." 
"What's going on from my point of view is that RSS is growing up into a real publishing medium," said Scott Rafer, president and CEO of Feedster. "Therefore there's going to be advertising."

Pheedo <a href="http://www.pheedo.info/archives/000211.html" title="">gooit</a> er nog een paar mooie cijfers tegenaan:
The numbers below only include stats on blogs with RSS and are estimates based on our own research and data provided by Technorati. They do not include the feed traffic from traditional online publications like CNet, Forbes or Yahoo. 
Blogosphere is doubling every: 5 months 
Number of post created per day. 21,600 
Number of feeds added per day: 3,700
Percent of blogs with feeds: 31%
Number of content feeds: 3.1 million 
RSS consumers: 3.8 million 
<b>RSS Ad impressions: 2004 - 77 million, 2005 - 308 Million, 2006 - 1.2 billion</b>";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:7:"default";}s:12:"publish_date";s:16:"2004-10-17-16-30";s:9:"edit_date";s:16:"2004-10-17-15-48";s:5:"title";s:29:"RSS Feeds Hunger for More Ads";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"1";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}