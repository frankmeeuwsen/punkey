<?php /* pivot */ die(); ?>a:13:{s:4:"code";i:5847;s:4:"date";s:16:"2003-05-26-10-53";s:4:"user";s:7:"Da_Huge";s:5:"title";s:12:"Uit de inbox";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-05-26-10-53";s:9:"edit_date";s:16:"2003-05-26-10-53";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:4894:"<p>Met veel genoegen delen wij je mee dat Miles Kessler (4th dan) de Workshop "Aikido and the practice of Inquiry" geeft in het weekend van 30 en  31 Augustus 2003 in Breda, Gaffelstraat 30 (in samenwerking met BACHS dojo van Jan Lensen). <br />
De Workshop is gelimiteerd tot 25 personen. Gezien de aard van de workshop dient men voor beide dagen in te schrijven. Plaatsen worden verkregen op basis van "wie het eerst registreert en betaalt, is verzekerd van plaats".</p>

<p>Iedereen, ongeacht stijl en ranking, is van harte welkom ! <br />
 <br />
Vervolg praktische details, zie verder in deze email.<br />
Heb je nog vragen na het lezen van deze email, schrijf Carolina van Haperen naar maramind@hotmail.com of bel naar telefoonnr 020-6123964; we staan je graag te woord. <br />
 <br />
 <br />
De Workshop is bijzonder omdat je Aikido in vele facetten zal onderzoeken. Miles Kessler geeft de volgende inhoudelijke informatie:<br />
"Aikido and the practice of Inquiry </p>

<p>Aikido is an art that develops a wide range physical ability and technical skill, yet it is said to be founded on deep spiritual truths. With time and effort a person can achieve satisfactory development on a physical level. But what about the so-called "spiritual" side of the art? What is the deeper spiritual potential that Aikido has to offer?</p>

<p>Why the practice of Inquiry?</p>

<p>It must be understood that the deeper spiritual aspects of Aikido do not exist within our normal mundane consciousness. A consciousness dominated by likes and dislikes, desires and fears, repetitive patterns, and conditioned behavior. Within such a mental framework the principles and deeper aspects of Aikido are rarely allowed to arise. If this is so, then how does one unlock Aikido's deeper potential? It is to this dilemma that the practice of Inquiry is directed.</p>

<p>To penetrate to the depths of Aikido we must first come to know ourselves, to know our own minds. It is within our minds that the blocks preventing us from experiencing the richness and depth of Aikido exist.</p>

<p>Through the practice of Inquiry one can come to know their inner limitations. Thus allowing one to challenge the psychological barriers leading to a new understanding of ones experience.</p>

<p>With Inquiry, one learns through direct and repeated experience how to open to and allow the principles of Aikido to arise in their Being. Thus leading to a deeper understanding of the spiritual dimensions of the art. Such an understanding will then create the possibility for the principles of the Aikido relationship to functionally manifest in ones practice, as well as all aspects of ones life.</p>

<p>The very act of questioning is the beginning of a process that turns ones mind away from its conditioned patterns, and towards the unlimited potential of the unknown. And it is from within the unknown that the principles, if allowed, will emerge to inform our place in relationship to others.</p>

<p>The Practice<br />
This workshop will be a combination of traditional Aikido training and the practice of Inquiry. However, the emphasis will not be on the technical side of the art, but rather Aikido's inner principles. The Inquiry practice will be an exploration of how the principles arise in our being. The Aikido practice will focus on a functional application of the understanding attained in Inquiry. Each of the two practices supporting, and reinforcing, the other.</p>

<p>Two distinct practices of mind and body, which inform, and reflect, the other. Until the boundaries separating mind and body, fall away, and the two practices merge functionally together into one unified practice of Aikido." </p>

<p>Background of Miles Kessler:<br />
Miles Kessler, Aikikai 4th Dan, spent 8 years in Iwama, Japan, studying under Saito Sensei Shihan, 9th Dan. Miles began his Aikido career in 1985 in Dallas, Texas, where he studied for 4 years. He moved to Japan to study Aikido full time from 1989-1997 under Saito Sensei. </p>

<p>Miles founded the Mito International Aikido Club in Japan where he was the chief instructor until his departure in 1997. Also during this time he instructed Aikido at the Mito Social Welfare Center from 1994-1997.</p>

<p>Since leaving Japan in 1997 Miles has continued his exploration by teaching and conducting seminars in various dojos around the world, such as Burma, Florida, Holland, Italy, New Zealand, Denmark, Switzerland, England, Scotland, Israel and Germany, to name a few. </p>

<p>In addition to his Aikido background, Miles has also been seriously practicing Mindfulness and Insight meditation under the renowned meditation teacher Sayadaw U Pandita of Birma.   Since 1998 he has spent a total of a three years of monastic life on intensive meditation retreats.   </p>

<p>Miles is committed to the exploration of the human condition and its relationship in the world.</p>";s:4:"body";s:0:"";s:8:"comments";a:1:{i:0;a:6:{s:4:"name";s:21:" Carolina van Haperen";s:5:"email";s:21:" maramind@hotmail.com";s:2:"ip";s:13:" 195.99.36.14";s:4:"date";s:16:"2003-10-03-11-13";s:3:"url";s:27:" http://aimushin.tripod.com";s:7:"comment";s:458:"Bredase Aikido Chen Hsin School:
In Breda, Gaffelstraat 30, is de Aikido Chen Hsin School van Jan Lensen gevestigd. Op dinsdag en donderdagavonden vinden de Aikido en de Chen Hsin lessen plaats. Het is een unieke combinatie van japanse en chinese principles, en biedt diepgang in de principles in het algemeen. Wil je meer weten over de school, neem dan contact op met Jan Lensen via emailadres Jan1.lensen@planet.nl of bel hem op telefoonnummer 0161-431126.";}}}