<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5837;s:4:"date";s:16:"2003-05-23-09-16";s:4:"user";s:7:"Da_Huge";s:5:"title";s:25:"Programmeur Mezz stapt op";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-05-23-09-16";s:9:"edit_date";s:16:"2003-05-23-09-16";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:3001:"<p>Meneer Beeskow stapt zoals verwacht op, maar probeert een hoop goed te praten terwijl hij zelf vanaf de opening oogkleppen heeft opgehad.<br />
als je verder klikt <a href="http://www.bndestem.nl/regioportal/BNS/0,2622,1314-Voorpagina__1612369_,00.html">een stuk uit de krant </a>met mijn reactie erbij.</p>

<p>Bovendien wordt de programmering die hij bij Mezz voor ogen heeft door te weinig mensen gewaardeerd. Met name de meer experimentele acts trekken te weinig bezoekers. "Hier is er meer een straight rockpubliek", zegt Beeskow. <br />
 <br />
***Wat een grote onzin kraamt meneer Beeskow uit, als hij op een normale manier meer credible acts zou boeken en geen rare acts waar niemand van gehoord heeft...<br />
 <br />
Daarbij komt dat de samenwerking met Bredase culturele groeperingen volgens de programmeur onvoldoende van de grond komt. "De sfeer is te afwachtend, niet initiatiefrijk. <br />
 <br />
**BS nummer twee, meneer Beeskow heeft genoeg initiatieven aangereikt gekregen, van zowel de culturele groeperingen in Breda alswel de back-up uit de commissie die hij talloze keren passeerde, hij snijdt zichzelf in de vingers door dat te claimen.<br />
Hij heeft voor het openen van de Mezz gesprekken gehad met alle groeperingen en stromingen in Breda en heeft daar bijzonder weinig gedaan, het enige goede resultaat is Spektrum, maar dat komt omdat deze mensen zelf zich helemaal de pleuris hebben gewerkt en al een groot verleden hadden met hun techno feestjes.<br />
 </p>

<p>Hierin verschilt Breda volgens Beeskow van grote steden zoals Rotterdam. "Daar staan ze in de rij om dingen te organiseren." <br />
 <br />
**Meneer geilt wel erg op Rotterdam, vandaar dat hij een hoop Bredase dance dj's heeft gepasseerd om Rotterdams nietszeggende acts neer te zetten.<br />
 <br />
Ook de strubbelingen in de organisatie hebben invloed gehad op Beeskows besluit: "Het is redelijk onrustig geweest. Dat werkt mee.<br />
 <br />
**Dat had hij naar zijn commissie moeten luisteren!<br />
(die namen zijn bij jullie bekend).<br />
 <br />
Beeskow vindt zelf dat hij zich voldoende heeft ingezet om de popcultuur in Breda van de grond te krijgen. Mezz-directeur Frank Zijlmans is het daar niet mee eens. Volgens hem had er meer ingezeten. Niettemin zegt Zijlmans het jammer te vinden dat zijn programmeur vertrekt. <br />
 <br />
**Is het geen afgedwongen ontslag??<br />
Zijlmans laat zich volgens mij niet helemaal uit.<br />
 <br />
"Maar ik heb respect voor zijn beslissing." <br />
 <br />
Ziek<br />
Beeskow legt zijn functie per 1 augustus neer. De bedoeling is dat zijn opvolger dan al aan de slag is. De sollicitatieprocedure gaat zo snel mogelijk van start. <br />
Mezz kampt met nog andere personele problemen. De horecamanager zit al sinds 21 maart ziek thuis. De officemanager van Mezz is al vier weken ziek. Volgens Zijlmans is er geen enkele relatie tussen de ziekte van de twee personeelsleden en hun werk bij het poppodium. Mezz telt in totaal zeven betaalde krachten.</p>";s:4:"body";s:0:"";}