<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5853;s:4:"date";s:16:"2003-05-26-23-26";s:4:"user";s:6:"Punkey";s:5:"title";s:63:"Sort of like &apos;Shutdown&apos; on the &apos;Start&apos; menu";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-05-26-23-26";s:9:"edit_date";s:16:"2003-05-26-23-26";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:387:"<p><P>While pulling money out of a tiny, no-name ATM in my neighborhood last night, I found what may have been the most thoughtless UI text of all time:</P><br />
<P><PRE>Press Enter to Exit</PRE><br />
<P></P><br />
<P>Accept this, grasshopper, and enlightenment is yours.</P><br />
<DIV align=right>[via <A href="http://www.veen.com/jeff/">Jeffrey Veen</A>]<br />
<DIV></DIV></DIV></p>";s:4:"body";s:0:"";}