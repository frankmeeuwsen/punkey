<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:4678;s:4:"date";s:16:"2002-12-01-23-20";s:4:"user";s:6:"Punkey";s:5:"title";s:19:"Blogs and Addiction";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-12-01-23-20";s:9:"edit_date";s:16:"2002-12-01-23-20";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:664:""I wanted to show just how addictive blogging was, so I started off my talk with three questions:
<ul>
<li>How many people here blog on a regular basis?
<li>How many people here read a blog on a regular basis?
<li>How many people smoke cigarettes on a regular basis?
</ul>
The answers stunned me: probably half of the people blogged, and 2/3 to 4/5 of them read blogs.  And out of sixty or so people, only three of them smoked!  And to think, nicotine is the controlled substance?!  Well at least, blogging isn't hazardous to your health... right?"

<a href="http://www.microcontentnews.com/entries/20021125-2168.htm">Microcontent News, a Corante.com Microblog</a>";s:4:"body";s:0:"";}