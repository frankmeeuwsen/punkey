<?php /* pivot */ die(); ?>a:13:{s:4:"code";i:6612;s:4:"date";s:16:"2003-11-23-11-53";s:4:"user";s:6:"Punkey";s:5:"title";s:14:"Hoor-wederhoor";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-11-23-11-53";s:9:"edit_date";s:16:"2003-11-23-11-53";s:8:"category";a:1:{i:0;s:9:"Marketing";}s:12:"introduction";s:2853:"<img src="http://www.punkey.com/media/SnagIt_1.jpg" border="0" align="left" hspace="3" vspace="3">Ik hoopte al dat er een reactie op zou komen. En <a href="http://www.heijblok.com/">Mark</a> is er snel bij. Op <a href="http://www.punkey.com/archives/2003/11/006992.html">mijn post</a> over Michael Moore reageert hij met een link naar een <a href="http://www.michaelmoore.com/words/wackoattacko/">open brief</a> van Moore zelf die een aantal punten tegenspreekt.
Ik heb het gelezen en een aantal zaken vergeleken. Wat me opvalt is dat Moore met name ingaat op het verhaal over de bank waar je een geweer krijgt, Lockheed  Martin en de rol van Charlton Heston. Wat hij echter NIET tegenspreekt is hoe hij zijn film heeft gemaakt. Hij geeft toe dat er verschillende beelden van Heston zijn gebruikt. Maar dat is nu juist het punt! Ik kan ook een ander beeld van Michael Moore, JP Balkenende of Dick Advocaat wegzetten, als ik maar net de juiste quotes en soundbites op een juiste manier knip en plak. En er maar net de juiste dramatische wending aan geeft. Want dat is wat Moore doet. 
Het verhaal over het zoontje van Tamarla Owens, die een zesjarig klasgenootje neerschiet met een gestolen geweer van zijn crackdealende oom wordt ook niet tegengesproken. De oom wordt niet achterna gezeten door Moore op de manier zoals hij probeert Dick Clark belachelijk te maken.

Kijk, ik ben er niet op uit om de onderste steen boven te krijgen in dit conflict,  voor zover het een conflict is. Maar wat ik wel wil aantonen is dat zowel links als rechts gebruik maken van dezelfde propagandistische middelen om hun boodschap over te krijgen. Bekende methoden hierin zijn: 
- Het selectief weglaten van zinnen en woorden, 
- Dramatische wendingen aan beelden geven, de juiste muziek onder de beelden. 
- Delen weglaten, delen verheerlijken 
- Het consequent belachelijk maken van "de ander".
(Zie ook deze <a href="http://nl.wikipedia.org/wiki/Spin_doctor">WikiPedia</a> en <a href="http://www.google.com/search?sourceid=mozclient&ie=utf-8&oe=utf-8&q=define%3A+propaganda">Google</a>


Wil je meer lezen over dit issue? Bekijk dan de volgende sites eens en trek je eigen conclusie.
Michael Moore: <a href="http://www.michaelmoore.com/words/wackoattacko/">How to Deal with the Lies and the Lying Liars When They Lie about "Bowling for Columbine"</a>
<a href="http://www.hardylaw.net/Truth_About_Bowling.html">Bowling For Columbine: Documentary or Fiction?</a>
<a href="http://www.mooreexposed.com/">Moore Exposed</a>
<a href="http://www.opinionjournal.com/forms/printThis.html?id=110003233/">Wall Street Journal: UnMoored from reality</a>
<a href="http://forums.alternet.org/guest/motet?show+-uh9Vny">Forum</a> op AlterNet over de film
<a href="http://www.moorelies.com/">Moore Lies</a>

Er is vast veel meer te vinden. Doe er je voordeel mee dat er een Google is :-)";s:4:"body";s:0:"";s:8:"comments";a:2:{i:0;a:6:{s:4:"name";s:5:" Mark";s:5:"email";s:1:" ";s:2:"ip";s:13:" 213.84.3.133";s:4:"date";s:16:"2003-11-23-21-13";s:3:"url";s:24:" http://www.heijblok.com";s:7:"comment";s:1977:"Exact wat ik denk, Frank.. Alles wat ik lees of zie is van de tweede hand. Op Indymedia stond eens een stuk over een betoging bij de Blauwe Aanslag jaren terug. In het stuk werdt gesuggereerd dat agenten zonder enige aanleiding begonnen te rammen. Dat was pertinent niet waar, want ik had zelfs ruzie met een medebetoger die -samen met anderen- stenen gooide en de agenten voor Nazi's uitmaakte.

Hetzelfde geld voor ongeveer alle verhalen over motorongelukken. Ik ben -jammer genoeg- meerdere keren aanwezig geweest bij ongelukken, en kranten schrijven maar ��n ding. 

Dat er werdt geraced bijvoorbeeld. Motorracen, want ik inderdaad ook doe (op Zolder, Assen.. whatever..) is het bijhouden van rondetijden op een baan. Hoe is dit mogelijk als er VERKEERSLICHTEN zijn???

Of dat de bocht berucht is. Mensen die de bocht kennen, gaan juist NIET onderuit, want die weten waar ze moeten rijden. Mensen die de artikelen schrijven willen natuurlijk dat veel mensen het lezen, en vaak hebben zij geen idee hoe verraderlijk een weg kan zijn als je deze hard neemt. Een scheur of hobbel in de weg bepaalt of je het haalt of dat je onderuit glijd. Zo heel simpel ligt het.

Mensen hebben niet de interesse en tijd om uit te zoeken hoe iets werkelijk zit en gaan af op wat de kranten of het nieuws schrijft. En daarmee vormen ze hun beeld. Het mooiste voorbeeld voor mijzelf vind ik de Marrokanen. Nog nooit in Den Haag heb ik ook maar een Marrokaan iets verkeerds tegen mij horen, zien of voelen doen. Geen woorden naar mijn hoofd, geen stomp op mijn neus.. niks. En ik ben al twee keer lastig gevallen door Blanke Nederlanders. Maar ja, mijn vriendin wordt wel belaagd door enge Buitenlanders. Dus voordat ik haar kende was ik inderdaad minder "racistisch" als dat ik nu ben geworden.

Ik denk dat het nieuws best vaak WEL klopt, maar haal geen dingen aan die in de Telegraaf staan of de Metro. Dat zijn geen kranten meer, dat zijn -in mijn ogen- papieren om de mensen dom te houden.";}i:1;a:6:{s:4:"name";s:7:" DeFeCt";s:5:"email";s:1:" ";s:2:"ip";s:12:" 62.58.37.65";s:4:"date";s:16:"2003-11-24-09-13";s:3:"url";s:34:" http://www.digitalculture.nl/cult";s:7:"comment";s:469:"Wat mij het meeste tegen staat is dat het word weggezet als een documentaire, daar heeft ie immers ook de oscar voor gekregen. En dat is het dus overduidelijk niet, want zoals de vandale zegt: "een op documenten, feiten berustende film".

Dat die amerikanen helemaal geshuffeld zijn als het om het bezit van wapens gaat geloof ik best, maar dat het aan mij wordt gepresenteerd alsof ik naar feiten zit te kijken, daar voel ik me toch aardig genaaid door meneer Moore...";}}}