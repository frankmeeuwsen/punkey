<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5939;s:4:"date";s:16:"2003-06-12-23-33";s:4:"user";s:6:"Punkey";s:5:"title";s:37:"BZK start landelijke personendatabase";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-06-12-23-33";s:9:"edit_date";s:16:"2003-06-12-23-33";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:1268:"<p>Het 'Agentschap BPR' van het ministerie van Binnenlandse Zaken gaat een landelijke database samenstellen met een selectie uit de gemeentelijke basisadministraties. 'landelijke raadpleegbare deelverzameling' (LRD) luidt de naam, en haar eerste klanten zijn het Centrum voor Werk en Inkomen (CWI) en de politie. </p>

<p>Het <a href="http://www.netkwesties.nl/editie63/artikel2.html">volledige artikel</a><br />
 meldt onder meer:<br />
"De politie gaat de database gebruiken voor het het 'lik-op-stuk beleid' zoals verschillende media vorige week meldden. Alle politieagenten krijgen toegang, vaak ook mobiel, want veel politiewagens zijn uitgerust met dataterminals, en in een aantal gemeenten lopen projecten met gprs- en wap-toegang voor ambulante agenten"</p>

<p>"De databasecommunicatie is gebaseerd op XML volgens de SOAP-specificaties, een open standaard. Op dit moment loopt er nog een discussie onder welke voorwaarden het systeem ook over het internet te benaderen zal zijn. "</p>

<p>En de leukste:</p>

<p>"Er mag gebruikt worden gemaakt van zogenaamde wildcards: de zoekopdracht op de naam jan* (met sterretje) zal bijvoorbeeld ook Janssen, Jansen, <b>Janneman</b> enzovoorts, opleveren. "</p>

<p>Al met al weer lekker Big Brother nieuws nietwaar?</p>";s:4:"body";s:0:"";}