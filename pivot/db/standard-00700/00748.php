<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:748;s:4:"date";s:16:"2001-07-06-15-46";s:4:"user";s:6:"Punkey";s:5:"title";s:32:"Ad-free-blogspot Dit is een leuk";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-07-06-15-46";s:9:"edit_date";s:16:"2001-07-06-15-46";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1603:"Ad-free-blogspot

Dit is een leuk initiatief:
Blogspot is een plaats waar je je weblog kunt plaatsen als je zelf geen ruimte hebt. Probleem: De service wordt gerund door advertenties. Voor 9 dollar is je site een jaar lang adfree.
<a href="http://ruthienation.blogspot.com/?/2001_07_01_ruthienation_archive.html">Ruthie</a> heeft het idee opgevat om voor iemand anders die 9 bucks te betalen. Dit heeft ze gedaan vanuit het verhaal wat ze eens heeft gehoord over een drive-through:
<i>About a year ago, I read an article in the <a href="http://www.seattletimes.com/">Seattle Times</a> about a day in the life of a Bellevue drive-through espresso stand. (Yes, we have those.) A woman made it to the front of the line and paid for the next person's espresso: down the whole road the rest of the day, each person paid for the next person. Any one of them could have realized that he had a free espresso and left, and everybody else would have paid the same amount but they didn't. All day, paying for the next person. I've always remembered this story as more than just a human interest tale it obviously meant more, and it wasn't just the amount, or the generosity it was the spirit of community, being part of something neat (if trivial) an anonymous (of sorts) support of the people who shared something in common.</i>

Zij heeft nu dus de rekening betaalt voor
<a href="http://49-forever.blogspot.com/
">49 Forever</a>. Die was blij verrast en zag het idee wel zitten en heeft de rekening voor <a href="http://www.wayno.blogspot.com/">The Way of Wayno</a> betaald.

Zal mij benieuwen hoe dit afloopt...";s:4:"body";s:0:"";}