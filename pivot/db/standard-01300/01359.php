<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:1359;s:4:"date";s:16:"2004-09-23-23-08";s:12:"introduction";s:873:"De nieuwe RSSautodiscovery functionaliteit in Firefox is erg fijn, maar
is wat mij betreft iets te eenzijdig. Je kunt namelijk een RSS feed
alleen in de <a href="http://www.mozilla.org/products/firefox/live-bookmarks.html">Live Bookmarks</a> van Firefox zelf plaatsen. En die zijn erg beperkt in functionaliteit (check ook mijn commentaar in <a href="http://www.emerce.nl/nieuws.jsp?id=373258">dit&nbsp; Emerce-artikel</a>)<br  />
Gelukkig zijn er mensen die hier iets aan doen. En zo ontstaat "<a href="http://projects.koziarski.net/fyr/">Feed Your Reader</a>".
Hiermee wordt de RSS-autodiscovery in Firefox uitgebreid met de
mogelijkheid om de RSS feed automatisch in de feedreader naar keuze toe
te voegen. Bij mij Newzcrawler, maar kan ook in bijv Feeddemon of
Feedreader.<br  />
<br  />
(Via <a href="http://www.Blogosphereradio.com">Blogosphereradio.com</a>)";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:7:"default";}s:12:"publish_date";s:16:"2004-09-23-23-03";s:9:"edit_date";s:16:"2004-09-24-00-34";s:5:"title";s:16:"Feed Your Reader";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"0";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}