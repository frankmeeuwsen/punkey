<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:1378;s:4:"date";s:16:"2004-09-26-23-00";s:12:"introduction";s:1609:"Jeremy Allaire <a href="http://www.paidcontent.org/contentnext/jeremy_allaire/the_state_of_rss.php">beschrijft in Paidcontent.org</a>
exact ook mijn gedachten over RSS. Doordat ik dit formaat steeds meer
gebruik om informatie te lezen en te ontvangen, verzamel ik ook veel
meer. Op dit moment heb ik zowel voor prive als voor zakelijke
doeleinden, niet alleen meer emailmappen, maar ook een Furl-account met
bookmarks, mijn eigen bookmarks (al wordt dat steeds minder) en in
Newzcrawler bewaar ik artikelen voor specifieke doeleinden (nog eens
loggen, trends, marketing, RSS etc). Die laatste kan ik ook wel weer
als RSS feed openbaar stellen, maar ik vraag me af of ik dat wel moet
doen.&nbsp; Want zo verleg je het probleem alleen maar. Waarom zou ik
8-10 RSS feeds moeten hebben met content die ik een keer heb gelezen en
die wellicht later nog eens interessant kan zijn?<br  />
Wederom verwijs ik naar <a href="http://www.glassdog.com/archives/2004/08/25/whats_wrong_with_feed_readers.html">een artikel</a>
wat ik een tijd geleden las over RSS readers. Wat nodig is, is een
programma/service die dwarsverbanden kan leggen, meta-informatie kan
geven, betere zoekfunctionaliteit etc. De huidige readers zijn door
techneuten gemaakt, programmeurs. En (dus?) niet in staat om aan de
natuurlijke behoefte van een gebruiker te voldoen, zie bovenstaande
functionaliteiten.<br  />
Hoe gaan anderen hier mee om? Hoe verzamel je je informatie? En hoe sla
je het op? Is dit wel met software op te lossen, of is het ook een <i>mindstate</i> en dus aan te leren? Dat was een retorische vraag...</p>";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:7:"default";}s:12:"publish_date";s:16:"2004-09-26-23-00";s:9:"edit_date";s:16:"2004-09-26-22-46";s:5:"title";s:16:"The state of RSS";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"0";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:2:{i:0;a:6:{s:4:"name";s:6:"Tjarko";s:5:"email";s:0:"";s:3:"url";s:23:"http://www.ditadres.com";s:2:"ip";s:15:"213.201.143.114";s:4:"date";s:16:"2004-09-27-09-40";s:7:"comment";s:64:"Ik voel een projectje aankomen... en daar ben ik wel voor in :-)";}i:1;a:6:{s:4:"name";s:6:"Jeroen";s:5:"email";s:0:"";s:3:"url";s:21:"http://braintags.com/";s:2:"ip";s:13:"194.149.212.7";s:4:"date";s:16:"2004-09-27-11-18";s:7:"comment";s:416:"Ik denk niet dat de oplossing te vinden is in een tool die organisatie aanbengt in RSS feeds. Het probleem is anmelijk veel groter: ik wil ook mijn e-mail, mijn telefoontjes, mijn chatlogs, mijn mediastreams, mijn losse idee�n en alle andere (toekomstige) binnenkomende informatie verwerken. Op dit moment is er zover ik weet alleen maar een papieren systeem die dat voor me kan doen: http://speakeasy.org/~lion/nb/.";}}}