<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:1374;s:4:"date";s:16:"2004-09-26-17-42";s:12:"introduction";s:1084:"Sinds een aantal dagen heb ik de standaard RSS en Atom feed uit Pivot doorgeschakeld naar <a href="http://www.feedburner.com/">Feedburner</a>.
Hiermee is het mogelijk om een feed voor alle RSS versies te
configureren en nog mooier: statistieken te zien over het gebruik van
je feed. Aangezien ik het pas een aantal dagen compleet heb
geconfigureerd zijn de cijfers nog niet zo geweldig. Dus die hou je nog
van me tegoed. Maar Marco Derksen van Marketingfacts.nl heeft wel <a href="http://www.mediafact.nl/comments.php?id=5430_0_1_0_C">zijn cijfers online</a>
gezet. Zo krijg je een beetje een idee wat Feedburner voor je kan doen.
Maar hoe configureer je Pivot nu zo dat alles netjes blijft werken en
dat je lezers geen nieuwe URL hoeven te gebruiken? Met name die laatste
vraag heeft me bezig gehouden, maar gelukkig ben ik dankzij <a href="http://www.adverto.nl/comments.php?id=147_0_1_0_C">Adverto.nl</a>
tot de oplossing gekomen. Wil je ook met Feedburner aan de slag en
gebruik je Pivot? Lees dan de volgende how-to zodat je niet dezelfde
denkfouten maakt als ik.</p>";s:4:"body";s:2541:"1. In Pivot staan je feeds standaard op rss.xml en atom.xml. Deze gebruiken we dan ook even als voorbeeld.<br  />
2. Hernoem in Pivot je rss-feed naar bijvoorbeeld feedburner.xml. Je Atomfeed kun je laten staan.<br  />
3. Maak nu in Feedburner een account aan en maak een nieuwe feed. Gebruik als feed URL het adres naar feedburner.xml<br  />
4. Nu komt het tricky gedeelte. Zorg dat je een goede FTP-client hebt die ook hidden files kan laten zien op een server (zoals <a href="http://sourceforge.net/projects/filezilla/">Filezilla</a>) of check of je ook via een webbased controlpanel op je server kan komen.<br  />
5. In een texteditor maak je een bestand genaamd ".htaccess". Je ziet
dat het een punt voor zijn naam heeft en geen extensie. Let hier goed
op als je het bestand opslaat in je texteditor! Hernoem het eventueel
naar .htaccess als er automatisch .txt achter wordt geplakt.<br  />
6. In dit bestand zet je de volgende regel:<br  />
Redirect /rss.xml http://feeds.feedburner.com/JENAAM<br  />
Redirect /atom.xml http://feeds.feedburner.com/JENAAM<br  />
<br  />
Bij mij is het bijvoorbeeld<br  />
Redirect /rss.xml http://feeds.feedburner.com/Punkey<br  />

Redirect /atom.xml http://feeds.feedburner.com/Punkey<br  />
<br  />
Sla het bestand op en upload het in de root van je server waar ook de RSS en Atom bestanden uit Pivot worden geplaatst<br  />
7. Let ook meteen even op of je lees- en schrijfrechten van Pivot nog
goed staan. Om een of andere reden waren deze na het uploaden van
.htaccess lichtelijk om zeep. Kijk in de <a href="http://www.pivotlog.net/help/help_uploading.php">Pivot handleiding</a> voor meer informatie<br  />
8. Ga nu in je Pivot-beheer naar de templates en open de frontpage template<br  />
9. Pas het stukje in de &lt;head&gt; tag aan waar [[rss_autodiscovery]] en [[atom_autodiscovery]] staat en zet hier<br  />
&lt;link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/Punkey"/&gt;<br  />
&lt;link rel="alternate" type="application/atom+xml" title="Atom" href="http://feeds.feedburner.com/Punkey"/&gt;

Let op dat je hier je eigen URL's plaatst :-)<br  />
10. Opslaan en rebuild je frontpage.<br  />
<br  />
Wat gebeurt er nu precies in stap 6? Hiermee worden je lezers van je
standaard RSS- en Atom feed automagisch geredirect naar je
Feedburner-feed. Zo hoeven zij dus geen nieuwe URL in te voeren en is
iedereen gelukkig.<br  />
<br  />
Heb je aanvullingen? Vragen? Stel ze en ik doe mijn best ze te beantwoorden.</p>";s:8:"category";a:1:{i:0;s:7:"default";}s:12:"publish_date";s:16:"2004-09-26-17-22";s:9:"edit_date";s:16:"2004-09-26-17-45";s:5:"title";s:41:"Hoe configureer je Pivot voor Feedburner?";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"0";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}