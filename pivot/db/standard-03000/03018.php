<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:3018;s:4:"date";s:16:"2002-03-04-21-16";s:4:"user";s:8:"janneman";s:5:"title";s:16:"Battleground God";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-03-04-21-16";s:9:"edit_date";s:16:"2002-03-04-21-16";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:926:"Congratulations!
You have been awarded the TPM medal of distinction! This is our second highest award for outstanding service on the <a href="http://www.philosophers.co.uk/games/god.htm">intellectual battleground</a>.

The fact that you progressed through this activity being hit only once and biting no bullets suggests that your beliefs about God are well thought out and almost entirely internally consistent.

The direct hit you suffered occurred because one set of your answers implied a logical contradiction. At the bottom of this page, we have reproduced the analysis of your direct hit. You would have bitten bullets had you responded in ways that required that you held views that most people would have found strange, incredible or unpalatable. However, this did not occur which means that despite the direct hit you qualify for our second highest award. A good achievement!

Ik weet wel dat ik intelectueel ben :-)";s:4:"body";s:0:"";}