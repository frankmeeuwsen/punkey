<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:1807;s:4:"date";s:16:"2005-05-19-08-29";s:12:"introduction";s:1379:"[[image:zoomtextarea.jpg::left:0]]Taken shamelessly from the "excellent Lifehacker-blog":http://www.lifehacker.com/software/productivity/download-of-the-day-zoom-textarea-firefox-user-script-104004.php:
The Zoom Textarea user script lets you enlarge textareas on any web page for more glorious room to type - so handy for writing forum posts, weblog comments or posts, and web-based email.

The Zoom Textarea user script requires the Firefox Greasemonkey extension (at least version 0.3.) If you're a Firefox user without Greasemonkey, here's how to install the script:

# In Firefox, click "Install Greasemonkey" from "here":http://greasemonkey.mozdev.org/. Be sure to allow "greasemonkey.mozdev.org" to install software in your browser.
# Restart Firefox.
# Right click on this link: "zoomtextarea.user.js":http://diveintogreasemonkey.org/download/zoomtextarea.user.js. 
# Click "Install User Script." Press OK.
# Refresh any page with a text area on it, and you will see a zoom in and zoom out button above it.

If you spend a good part of your day typing into web pages, Zoom Textarea will quickly become indispensable for your writable web.
"Case study: Zoom Textarea":http://diveintogreasemonkey.org/casestudy/zoomtextarea.html [Dive into Greasemonkey]

*Update:* It also makes the font in the textarea itself bigger, so the effect is not that great....hmmm..";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2005-05-19-08-24";s:9:"edit_date";s:16:"2005-05-19-09-12";s:5:"title";s:19:"Zoom your textarea!";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:1:{i:0;a:8:{s:4:"name";s:6:"Marcus";s:5:"email";s:20:"marcus@vorwaller.net";s:3:"url";s:26:"http://marcusvorwaller.com";s:2:"ip";s:11:"68.0.17.114";s:4:"date";s:16:"2005-06-04-13-35";s:7:"comment";s:163:"Try this one--it lets you resize textareas as if they were windows (drag the corner or side). Works great - http://www.extensionsmirror.nl/index.php?showtopic=2796";s:10:"registered";i:0;s:6:"notify";s:1:"1";}}}