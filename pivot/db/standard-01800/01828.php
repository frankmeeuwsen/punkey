<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:1828;s:4:"date";s:16:"2005-06-05-14-23";s:12:"introduction";s:1529:"While processing my Inbox, I came across a note from Courtney Brigham, the Senior PR Manager from the Evernote corp. Thank you Courtney!
You might remember I did a "Evernote vs. OnFolio showdown":http://www.punkey.com/pivot/entry.php?id=1790 where Evernote was defeated at the last moment bij OnFolio. But, Evernote is new and improved! To quote Courtney's message:

"[...]I wanted to let you know that an EverNote beta update is now available and share some of its latest features and improvements with you. 

A few highlights include: Extensive category improvements; Backup support; Full screen support; New keyboard shortcuts (a new total of 80); File Import / Export; New date finder; Improved Outlook email transfers; Improved graphics support; Scanner support; Other improvements and bug fixes. My favorite enhancement is the new category icons (over 50), where you can easily assign icons to identifiable individual categories, such as Web Clips, Business, Personal, Ink Notes, and more.

For a complete overview of the release notes, please see: "http://www.evernote.com/en/products/evernote/releasenotes.php":http://www.evernote.com/en/products/evernote/releasenotes.php"

[[image:evernote_copy.jpg::center:0]]

Well, I have to say, it looks impressive. But sorry Courtney, OnFolio works like a charm for me. It has it's crazy behaviours like eating all my RAM, but the possibilities to collect are just right for me. I will keep an eye on Evernote to see where it goes. But for now, I'm sticking to OnFolio.";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2005-06-05-14-14";s:9:"edit_date";s:16:"2005-06-05-14-23";s:5:"title";s:16:"EverNote upgrade";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}