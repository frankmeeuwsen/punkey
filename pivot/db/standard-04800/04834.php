<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:4834;s:4:"date";s:16:"2003-01-07-13-38";s:4:"user";s:8:"janneman";s:5:"title";s:22:"Killing me Microsoftly";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-01-07-13-38";s:9:"edit_date";s:16:"2003-01-07-13-38";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1389:"Halftime.
Your football team is behind--way, way behind--and there's a feeling in the locker room of heavy, clotted gloom. Everyone slouches on the floor against lockers and benches. Doom-induced lethargy pervades the place. Even the towels are too limp to swat at a teammate's derriere. And then the coach appears. Moving purposefully to the center of the room, he eyes the despairing players. He rubs his hands together as if they were kindling for inspiration.

At this point, the coach can:
- Deliver a rousing, emotion-laced speech exhorting the players to press on in the face of tremendous adversity and daunting odds, or
- Cue up a PowerPoint presentation on the six keys to victory, including bulleted items such as "Proper blocking and tackling," "Exhibiting a winning attitude," "Turning weaknesses into strengths" and "Don't focus on the scoreboard," along with a multi-media photo montage of memorable game-winning plays set to the soundtrack of "Rudy."

Which approach is more likely to send the team back onto the field poised for a comeback?
Your answer instantly drop-kicks you into one of two camps:

- Those who believe in the power of a freewheeling address, full of digressions and personal chemistry, to change hearts and minds most effectively.

- Those who believe in <a href="http://www.chicagotribune.com/news/showcase/chi-0301050396jan05.story">PowerPoint</a>...";s:4:"body";s:0:"";}