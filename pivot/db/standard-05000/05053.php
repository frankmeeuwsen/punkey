<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5053;s:4:"date";s:16:"2003-02-12-21-50";s:4:"user";s:6:"Punkey";s:5:"title";s:12:"Konfabulator";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-02-12-21-50";s:9:"edit_date";s:16:"2003-02-12-21-50";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1568:"<a href="http://eatorange.com/archives/mac_os_x/000127.php">Eat Orange</a> schrijft erover: <i>After a long wait and great anticipation - <a href="http://kmirror.deskmod.com/">Konfabulator</a>, the desktop widget application for Mac OS X was released today. </i>

Wired News <a href="http://www.wired.com/news/mac/0,2125,57620,00.html">bericht</a>:<i>Konfabulator boasts an open-programming interface, which allows people with a little knowledge of JavaScript and XML, or a willingness to learn, to create their own applications.

"The goal is to let people create whatever they want," said Rose by phone from his home in Redwood City, California. "You can make it do whatever you want it to do, and to look how you want it to look. The possibilities are really endless." </i>

Ik heb het vanmiddag bij ons op kantoor even op de Mac geinstalleerd en bekeken en het ziet er inderdaad erg slick-n-slide uit. De <a href="http://kmirror.deskmod.com/workshop/Konfabulator_Reference.pdf">XML reference</a> (PDF-alert)zien er niet verschrikkelijk ingewikkeld uit, ik snapte het in elk geval redelijk snel :-)

Weet iemand of er ook iets dergelijks voor de PC is? <a href="http://www.screenweaver.com">Screenweaver</a> komt in mijn ogen nog het dichtst in de buurt, maar dat is weer Flash georienteerd. Al supporten ze in de laatste versie ook COM-koppelingen voor bijvoorbeeld joysticks.

Mooie ontwikkelingen. Ik hoop er snel meer van te zien!

(update: over de PC versie, inderdaad, waarom geen <a href="http://www.markme.com/dhumphreys/archives/000786.html">Director</a>?)";s:4:"body";s:0:"";}