<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:338;s:4:"date";s:16:"2001-03-13-12-58";s:4:"user";s:6:"Punkey";s:5:"title";s:25:"What the Hell is Blogger?";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-03-13-12-58";s:9:"edit_date";s:16:"2001-03-13-12-58";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1602:"<a href="http://www.marketingprofs.com/Marketscope/blogger.asp">What the Hell is Blogger?</a>
Eric Norlin, Een van de oprichters van <a href="http://www.Personalization.com">Personalization.com</a>, heeft een relatief briljant stuk geschreven over <a href="http://www.Blogger.com">Blogger</a>. Je weet wel, de tool die we gebruiken. In plaats van het te omschrijven, een paar quotes, om de feel te krijgen van het stuk

"From 0 users to 40,000 users in less than 9 months; growing at a rate of 300 users per day; an annualized growth rate of over 270%; dollars spent on advertising and marketing-zero. "

"At base, Blogger is the doorway to a fundamental Internet truth: there are two Internets. One Internet is the brochure-ware of the corporate world. This is the Internet of the everyday. On this net, corporations talk of "customer satisfaction," "providing unparalleled X," and "caring about our employees." It is the Internet of corporate PR departments. And, quite frankly, it sucks.

The second Internet is one rarely seen. Your children know it. Students see it. The IT guy in the basement that nobody talks to at company picnics-he's there. It is the net of storytellers, techno-geeks and eccentrics. [...] If you spend any time at all on this side of the net, you come to find that it embodies a quality that the corporate net will never touch: enthusiasm."

"Blogger opens the doorway to harvesting the one great untouched asset of your company: every employee's ability to interact"

Ben je geinteresseerd in marketing? Al dan niet een-op-een? Check dit stuk, en check personalization.com!";s:4:"body";s:0:"";}