<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:969;s:4:"date";s:16:"2001-08-21-10-14";s:4:"user";s:6:"Punkey";s:5:"title";s:29:"Top 20 replies by programmers";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-08-21-10-14";s:9:"edit_date";s:16:"2001-08-21-10-14";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:852:"Top 20 replies by programmers when their programs do not work:

20. "That's weird..."
19. "It's never done that before."
18. "It worked yesterday."
17. "How is that possible?"
16. "It must be a hardware problem."
15. "What did you type in wrong to get it to crash?"
14. "There is something funky in your data."
13. "I haven't touched that module in weeks!"
12. "You must have the wrong version."
11. "It's just some unlucky coincidence."
10. "I can't test everything!"
9. "THIS can't be the source of THAT."
8. "It works, but it hasn't been tested."
7. "Somebody must have changed my code."
6. "Did you check for a virus on your system?"
5. "Even though it doesn'tw work, how does it feel?
4. "You can't use that version on your system."
3. "Why do you want to do it that way?"
2. "Where were you when the program blew up?"
1. "It works on my machine."";s:4:"body";s:0:"";}