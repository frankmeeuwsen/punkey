<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:922;s:4:"date";s:16:"2001-08-08-09-18";s:4:"user";s:6:"Punkey";s:5:"title";s:25:"Het einde van de vakantie";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-08-08-09-18";s:9:"edit_date";s:16:"2001-08-08-09-18";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1492:"Het einde van de vakantie

Tja, ik merkte zojuist op het station dat de vakantie weer ver voorbij is: het wordt weer drukker, gebronsde mensen staan sterke verhalen op te hangen over wat ze dit jaar wel niet hebben gedaan. Maar het mooiste tafereel was toch wel het volgende: 2 mannen, ergens rond de 40, staan hun vakantie te bespreken. Let wel: het zijn ambtenaren. Ambtenaar 1 is nog redelijk in orde zo op het eerste gezicht, maar ambtenaar 2 heeft een uitzonderlijk hoog Edgar van <a href="http://people.zeelandnet.nl/jgorren/eschiong.htm">Debiteuren Crediteuren </a>gehalte: kalend, vlassig snorretje, slobberpantalon en oud samsonitekoffertje met een pen en 3 bruine boterhammen met kaas.

Ambtenaar 1:'dit jaar lekker op vakantie geweest, berg beklommen in de Pyreneeen, flink actief gedaan. Wat gesnorkeld, en voor het eerst deltavliegen gedaan. Ik ben er weer klaar voor. Wat heb jij gedaan?'
Ambtenaar 2 (met vale jas en lullig koffertje in de hand):'Met de caravan naar Frankrijk. Was toch ook niet makkejk hoor...'
Amb.1:'Oh? Hoe dat zo?'
Amb.2:' Veel file he...'

Ik ving het treurig voor woorden. Ik ben blij dat ik nog op vakantie ga in september. Kan ik me nu nog verkneukelen om al die stakkers die een overbetaalde vakantie op zo'n kutcamping hebben gehad met hun (ongetwijfeld) doodongelukkige vrouw en irritant puberende kinderen.

Laat mij dan maar lekker op <a href="http://www.pashnit.com/roads/cal/Hwy1BigSur.htm">The Pacific Highway </a>1 cruisen met mijn schatje...";s:4:"body";s:0:"";}