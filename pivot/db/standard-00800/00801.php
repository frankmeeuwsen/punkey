<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:801;s:4:"date";s:16:"2001-07-13-00-06";s:4:"user";s:6:"Punkey";s:5:"title";s:26:"In navolging van mijn post";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-07-13-00-06";s:9:"edit_date";s:16:"2001-07-13-00-06";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:805:"In navolging van mijn post over EvdZ en mijn verhaaltje over Douglas Rushkoff's open source boek, hier wat quotes uit een <a href="http://www.guardian.co.uk/Archive/Article/0,4273,4219757,00.html">artikel</a>, geschreven door Douglas voor The Guardian:
<i>The internet is for amateurs.

The point is to do what we do online because we love it - whether or not someone agrees to pay us

The journalists who have interviewed me [...] can't see it as anything but a covert business plan.

The interactive mediaspace is [...] an opportunity to play and collaborate.

We've forgotten what made this medium so truly sexy to begin with. But don't worry, we still have it in our power to be reborn as unqualified amateurs. Then we can fall in love all over again.
</i>

Puzzelstukjes vallen langzaam in elkaar....";s:4:"body";s:0:"";}