<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:3664;s:4:"date";s:16:"2002-06-05-22-42";s:4:"user";s:8:"janneman";s:5:"title";s:19:"Het is weer tijd...";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-06-05-22-42";s:9:"edit_date";s:16:"2002-06-05-22-42";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:660:"...voor het driemaandelijkse culturele uitstapje. Deze keer een gedicht van Ingmar Heytze.

<b>Sta op en wankel</b>

Sta op en wankel - uche uche-
naar de gootsteen. Wat een nacht.
Nog steeds alleen. Te veel gedronken.
Met de taxi thuisgebracht.

Hoe doet iedereen dat toch,
in liefde vallen op de dansvloer,
niet alleen, maar allebei,
dus zij op hem en hij op haar,
getweeen, samen, met elkaar,
consensus dus -
vooruit, ik geef het toe,
ik heb onafgebroken woorden
met mezelf, zaai kruimels twijfels uit
en dansend op een spijkerbed
steek ik mijn eigen ego lek:

ik blijf een eenzaam twistgesprek.

<a href="http://www.epibreren.com/heytze/">Ingmar Heytze</a>";s:4:"body";s:0:"";}