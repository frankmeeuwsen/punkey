<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:2873;s:4:"date";s:16:"2002-02-21-22-20";s:4:"user";s:6:"Punkey";s:5:"title";s:68:"The Village Voice: Nation: Press Clips: War Riddles by Cynthia Cotts";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2002-02-21-22-20";s:9:"edit_date";s:16:"2002-02-21-22-20";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:529:"<a href="http://villagevoice.com/issues/0208/cotts.php"> Ten Questions the Media Can't Answer</a>

Where is Osama bin Laden?
How many civilians did they kill in New York?
How many civilians have we killed in Afghanistan?
Is the air safe to breathe in Lower Manhattan?
Who's to blame for the kidnapping of Danny Pearl?
Would the U.S. kill a journalist during wartime?
Do the terrorists hate us because we're gay?
Who's to blame for Al Qaeda drug profits?
Is Indonesia a terrorist state?
Should the U.S. continue to support Israel?";s:4:"body";s:0:"";}