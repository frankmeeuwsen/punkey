<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:7072;s:4:"date";s:16:"2006-10-01-22-27";s:12:"introduction";s:1235:"Mindjet invites us for a "1-hour Webinar":http://www.webex.com/web-seminars/view_event/669437426 with The Man, David Allen. On Tuesday, October 10, 2006, 10:00 AM PDT (that's 7 PM for me on the same day...) you can join this free webseminar right from your browser. From the raving invitation: 
<blockquote>Join us for a scintillating hour with Allen as he reviews best practices to increase your productivity, both at home and at work. Find out how technology - in particular Mindmapping and web conferencing - are powerful tools that Allen considers part of anyone's productivity 'bag of tricks'. Leave this web seminar with new ideas and concepts on how to increase your productivity in the four key areas of Allen's model
* Externalizing information and capturing notes
* Defining current reality
* Generating new ideas and perspectives
* Organizing information

We all know we should probably be more thoughtful, creative, focused, and organized - join us for this 60 minute web seminar with the person that Fast Company cites as "...one of the most influential thinkers on productivity" to find out how!
</blockquote>

I already registered, maybe "you should too":http://www.webex.com/web-seminars/view_event/669437426!";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2006-10-01-22-22";s:9:"edit_date";s:16:"2006-10-01-22-32";s:5:"title";s:39:"Join a free webseminar with David Allen";s:8:"subtitle";s:0:"";s:4:"user";s:5:"admin";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}