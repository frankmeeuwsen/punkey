<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:7027;s:4:"date";s:16:"2006-05-03-20-54";s:12:"introduction";s:2174:"[[image:56832765_86a232bc54_m.jpg::left:0]]Previously I mentioned a new GTD-related blog called "Blackbeltproductivity.net":http://www.blackbeltproductivity.net/. This didn't stay unnoticed and the guys from the blog, Jason Echols and Michael Ramm, decided to invite me for "an upcoming series":http://www.blackbeltproductivity.net/blog/category/black-belts-series/ on their blog called "The Black Belt Series". I am going to write for them throughout the year about GTD and how it has affected me. Well, what an honour for a dutch cheesehead as myself! I must say, I am in very good company! Check out the other authors:
   1. Emory Lundberg ("http://kvet.ch":http://kvet.ch/)
   2. Matt Cornell, of "Matt�s Idea Blog":http://ideamatt.blogspot.com/
   3. bsag, developer of "Tracks":http://www.rousette.org.uk/projects/, a a web-based application to help you implement GTD� methodology
   4. Jason Womack, David Allen Co. Employee & "Evangelist":http://www.davidco.com/blogs/jason/
   5. Punkey, of "What�s the next action":http://www.punkey.com/gtd/ (hey that's me!)
   6. GTD Wannabe, of "GTD Wannabe":http://gtdwannabe.blogspot.com/
   7. pooks, author and blogger at "planet pooks":http://planetpooks.wordpress.com/
   8. Allen Hall, of "searching4arcadia":http://searching4arcadia.wordpress.com/
   9. John Winstanley, of "MyPalmLife":http://mypalmlife.com/index.php

Not too bad 'ey? I look forward to this weekend. I have planned some thinking about the given topics and some rough outlines of what I am going to write. Now if only the sun can stay away for a couple of hours so I am not tempted to go outside and catch some first rays of sun this year...
And one other thing...I am starting fresh tomorrow with a _planned_ Weekly Review...woohoo....I scheduled 4 hours in the morning for myself to really do some reviewing. I hope it's gonna work. One thing I will be focussing on is _when_ I get distracted, I will try to find out _what's_ distracting me and _why_. With that information, I can perhaps help myself better focussing on the Weekly Review in the time to come. 

(Photo courtesy of "Jackol":http://www.flickr.com/photos/jackol/56832765/)";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2006-05-03-20-38";s:9:"edit_date";s:16:"2006-05-03-20-54";s:5:"title";s:33:"I'm Black Belt! Well...sort of...";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}