<?php /* pivot */ die(); ?>a:17:{s:4:"code";i:7009;s:4:"date";s:16:"2006-02-18-19-42";s:12:"introduction";s:1386:"[[image:label.jpg::left:0]]I use "Gmail":http://www.gmail.com more and more as my primary emailapp for everything I do besides my work (like "a weblogmagazine":http://www.aboutblank.nl , "weblogawards":http://www.dutchbloggies.nl, "eventblogs":http://www.eventblogs.nl). Since I mail a lot of requests that need some kind of answer or followup I made a label called @waiting in Gmail. This is where I put all my emails that I have send and need an answer from the receiver. Every week or so (..cough...Weekly review checkup...cough...) I check the list and see what's still out in the open. The great thing about Gmail is that any answer that arrives drops in your mailbox with the label attached to it. So I see immediately the emails I have been waiting for and I can easily remove the label if necessary.
But one thing that Gmail misses is an easy way _to label a message when sending it_. Now I need to go to "Sent Mail" and manually label the message. No big deal, but when I'm on a roll with email (sending 20-30 messages at a time) some might slip through the cracks. So it would be great if you could label a message directly when you are in "writing-mode" in Gmail. Not when you're in "I-just-sent-the-mail-let's-relax-mode". So maybe someone from Google is reading this blog and might see this desparate plea for more comfort and ease-of-use in a otherwise excellent mailapp!";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:3:"GTD";}s:12:"publish_date";s:16:"2006-02-18-19-24";s:9:"edit_date";s:16:"2006-02-18-19-42";s:5:"title";s:30:"I need a new feature in Gmail!";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"2";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";s:8:"comments";a:6:{i:0;a:9:{s:4:"name";s:5:"Niels";s:5:"email";s:19:"niels.bom@gmail.com";s:3:"url";s:0:"";s:2:"ip";s:12:"81.59.21.204";s:4:"date";s:16:"2006-02-19-18-03";s:7:"comment";s:136:"You can sort of use
http://g04.com/misc/GmailTipsComplete.html#Tip-03
to do this, just cc yourself with the note and you're done.

N";s:10:"registered";i:0;s:6:"notify";s:1:"1";s:8:"discreet";s:1:"1";}i:1;a:9:{s:4:"name";s:15:"Marshall Sontag";s:5:"email";s:24:"marshall@fishoilblog.com";s:3:"url";s:30:"http://live.marshallsontag.com";s:2:"ip";s:12:"65.33.198.86";s:4:"date";s:16:"2006-02-19-18-09";s:7:"comment";s:101:"Use this form to suggest your feature to Gmail!

https://services.google.com/inquiry/gmail_suggest/";s:10:"registered";i:0;s:6:"notify";N;s:8:"discreet";N;}i:2;a:9:{s:4:"name";s:5:"Frank";s:5:"email";s:16:"punkey@gmail.com";s:3:"url";s:33:"http://www.whatsthenextaction.com";s:2:"ip";s:12:"82.92.60.180";s:4:"date";s:16:"2006-02-19-19-32";s:7:"comment";s:76:"Niels, Marshall, thanks for the tips. I will notify those guys at Google :-)";s:10:"registered";i:0;s:6:"notify";N;s:8:"discreet";N;}i:3;a:9:{s:4:"name";s:3:"Dan";s:5:"email";s:20:"pritchett4@gmail.com";s:3:"url";s:0:"";s:2:"ip";s:13:"24.170.253.75";s:4:"date";s:16:"2006-02-19-22-30";s:7:"comment";s:203:"How about putting @waitingfor at the end of the email somewhere, then cc a copy to yourself, and use a filter to add it to the @waitingfor category. A bit of a work around, but should be possible.

Dan";s:10:"registered";i:0;s:6:"notify";N;s:8:"discreet";s:1:"1";}i:4;a:9:{s:4:"name";s:2:"dl";s:5:"email";s:19:"punkey@zebrazil.com";s:3:"url";s:0:"";s:2:"ip";s:13:"200.144.11.83";s:4:"date";s:16:"2006-02-20-17-14";s:7:"comment";s:332:"Dan is right! Hold your horses!

Yup you can cc to your address with a plus:

punkeypivot+wait@gmail.com

THEN add a filter to label all email that matches this to send to your @waiting for

More details here:
http://www.extremetech.com/article2/0,1697,1865178,00.asp 

I use it to send myself notes. Very quick. 


DL";s:10:"registered";i:0;s:6:"notify";N;s:8:"discreet";N;}i:5;a:9:{s:4:"name";s:5:"Frank";s:5:"email";s:16:"punkey@gmail.com";s:3:"url";s:33:"http://www.whatsthenextaction.com";s:2:"ip";s:12:"82.92.60.180";s:4:"date";s:16:"2006-02-21-17-29";s:7:"comment";s:170:"Hey all, thanks for the tips on using the +label trick in Gmail. Works like a charm for me! I can recommend using the tip Dan and dl give here if you want to use this too";s:10:"registered";i:0;s:6:"notify";N;s:8:"discreet";N;}}}