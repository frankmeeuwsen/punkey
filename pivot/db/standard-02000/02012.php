<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:2012;s:4:"date";s:16:"2001-10-10-11-34";s:4:"user";s:8:"janneman";s:5:"title";s:24:"New TV Shows Inspired By";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2001-10-10-11-34";s:9:"edit_date";s:16:"2001-10-10-11-34";s:8:"category";a:1:{i:0;s:11:"Old Blogger";}s:12:"introduction";s:1365:"New TV Shows Inspired By The Internet

1. Modem, She Wrote - Each week, our intrepid detective tries to solve the ultimate mystery: why her modem won't ever connect at 56k.
2. Micro-CHiPs - Ponch and Jon now patrol the Information Superhighway.
3. Carly's Angels - Chief exec Carly Fiorina instructs her team of three vixen market analysts on how to prop up HP's sagging stock price.
4. Hawaii 6.0 - An upgraded version of the classic series. Steve McGarrett goes surfing for bad guys online.
5. T. J. Hacker - A retired cop, with an uncanny resemblance to James T. Kirk, takes up computer hacking to track down the miscreants who canceled his TV show.
6. The Excel Files - Inexplicable things are happening to the data in Microsoft Excel spreadsheets. Can this puzzle be solved? The truth is out there.
7. The AOL-Team - Each week, AOL, Time Warner, Netscape, and Mr. T unite to promote corporate mergers and make the world safe for capitalism.
8. Magnum, PC - This series about a crime-solving personal computer that goes by the code name Deep Blue is based in beautiful Hawaii.
9. The Incredible Bulk - The exciting adventures of Windows, which just keeps growing and growing.
10. Buffy the Virus Slayer - Buffy and her fearless gang of antivirus definitions stalk and kill VBS files--no small feat while wearing a halter top and high-heeled boots.

by Dawny-Ray";s:4:"body";s:0:"";}