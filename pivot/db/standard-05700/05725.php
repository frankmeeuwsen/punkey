<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5725;s:4:"date";s:16:"2003-05-08-15-25";s:4:"user";s:6:"Punkey";s:5:"title";s:27:"Even Apeldoorn schrijven...";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-05-08-15-25";s:9:"edit_date";s:16:"2003-05-08-15-25";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:6917:"<p>Het onderstaande zijn citaten uit brieven die mensen aan verzekeringsmaatschappijen stuurden. Dit is dus allemaal echt geschreven!!! </p>

<p>(met dank aan <a href="http://www.babygrandpa.com">opa bibs</a>)</p>

<p>Bij thuiskomst reed ik per ongeluk een verkeerde oprit in en ramde ik een boom die daar bij mij niet staat. </p>

<p>De jongen was overal en nergens op straat, ik moest meerdere bochten nemen voordat ik hem raakte. </p>

<p>Toen ik de kruising naderde, verhief zich daar een hek om mijn vrije zicht te belemmeren. </p>

<p>Wie mijn portemonnee gestolen heeft kan ik u niet zeggen, aangezien er van mijn familie niemand in de buurt was. </p>

<p>Ik reed door de weide. Plotseling kwamen er van links en rechts meerdere voertuigen. Ik wist niet meer waar ik heen moest en toen knalde het van voren en van achteren. </p>

<p>Intussen is het loopgips van mijn rechterarm verwijderd. </p>

<p>Ik verwijderde mij van de rand van de straat, wierp een blik op mijn schoonmoeder en reed vervolgens het talud af. </p>

<p>Uw computer heeft mij een kind toebedeeld. Maar ik heb helemaal geen kind. En al helemaal niet van uw computer. </p>

<p>(uit een proces-verbaal) De achtervolgde sprong te water en dook ondanks meermaals herhaalde sommatie niet meer op. </p>

<p>Bovendien heb ik voor mijn eerste ongeluk en na mijn laatste schadevrij gereden. </p>

<p>Een voetganger kwam plotseling van de stoep en verdween vervolgens zonder een woord te zeggen onder mijn auto. </p>

<p>Ik heb zoveel formulieren moeten invullen, dat ik veel liever had gehad dat mijn geliefde man helemaal niet was gestorven. </p>

<p>Mijn motorfiets moest, net als ikzelf, vanwege ernstige schade weggesleept worden. </p>

<p>Ik overreed een man. Hij gaf zelf toe schuldig te zijn, omdat hem dit al een keer eerder gebeurd was. </p>

<p>Ik ben nog nooit van een ongeval weggevlucht. Integendeel. Ik moest altijd weggedragen worden. </p>

<p>De tennisbal kwam elegant en zuiver aan (geslagen door mijn dochter). Helaas heb ik mijn hoofd in plaats van het racket er voor gehouden. </p>

<p>Volgens de taxatie van de expert zal de schade tussen de 250.000 en een kwart miljoen bedragen. </p>

<p>Tijdens het dansen van de bekende Holladihia-Hoppsassa sprong ik overmoedig omhoog, waarbij ik mijn danspartner stevig ondersteunde. Daarbij kwam het kelderplafond sneller op mij af dan ik verwachtte. </p>

<p>In Uw schrijven van 26.6 over de nieuwe eigen bijdrage heeft u mij allervriendelijkst tot mejuffrouw bevorderd, wat echter in samenhang met mijn voornaam Henk helaas aanleiding tot pijnlijke vermoedens kan geven. </p>

<p>Ik reed eerst met mijn auto tegen de vangrail, sloeg toen over de kop en knalde tenslotte tegen een boom. Toen verloor ik de macht over het stuur. </p>

<p>Met de wettelijk ter plaatse toegestane maximum snelheid botste ik op een vrouw die mij tegen alle geldende voorschriften tegemoet kwam. </p>

<p>De andere wagen was absoluut onzichtbaar en toen verdween hij. </p>

<p>De andere auto kwam met de mijne in botsing zonder mij zijn bedoeling kenbaar te maken. </p>

<p>In hoog tempo naderde de telegraafpaal mij. Ik begon te zigzaggen maar ondanks dat raakte de telegraafpaal mij tegen de radiator. </p>

<p>Nog voor ik hem aanreed was ik er al van overtuigd, dat deze oude man nooit de overkant van de straat zou bereiken. </p>

<p>Omdat de voetganger niet beslissen kon naar welke kant hij moest wegrennen, reed ik over hem heen. </p>

<p>Na veertig jaar schadevrij te hebben gereden viel ik achter het stuur in slaap. </p>

<p>Een onzichtbaar voertuig kwam uit het niets, ramde tegen mij aan en verdween spoorloos. </p>

<p>Ik had de hele dag planten ingekocht. Toen ik de kruising naderde, groeide er plotseling een flinke struik in mijn gezichtsveld en ik kon het andere voertuig totaal niet meer zien. </p>

<p>Toen ik een vlieg wilde doodslaan raakte ik een lichtmast. </p>

<p>In eerste instantie heb ik tegen de politie gezegd dat ik niet gewond was, maar toen ik mijn hoed afzette bemerkte ik de schedelbreuk. </p>

<p>Toen ik met mijn auto van de weg raakte, werd ik eruit geslingerd en kwam ik in het weiland terecht. Een paar koeien vonden mij later in mijn gat. </p>

<p>Ik zag een treurig gezicht langzaam voorbij zweven en toen sloeg de man met een harde klap op het dak van mijn auto. </p>

<p>De getuigen van de aanrijding zijn tegen de achterkant aangeniet. </p>

<p>Kort en goed: Als ik het geld niet binnen 8 dagen ontvang, zie ik er geheel vanaf! </p>

<p>Mijn bruid heeft de agenten, die het ongeval kwamen opnemen, alles laten zien wat ze maar wilden zien....... </p>

<p>Ik dacht dat het raam openstond. Het was echter gesloten, wat ik pas bemerkte toen ik mijn hoofd naar buiten stak. </p>

<p>Laat u het mij a.u.b. weten indien u deze brief niet heeft ontvangen. </p>

<p>Mij treft het ongeval geen enkele blaam. De oorzaak was die jonge dame in de minirok. Als u een man bent is een verdere verklaring overbodig; als u een vrouw bent, begrijpt u het sowieso niet. </p>

<p>Moet ik mijn man dan eerst ombrengen voordat ik het geld krijg? </p>

<p>Uw argumenten zijn werkelijk zwak. Voor zulke uitvluchten moet u toch werkelijk een dommer iemand zoeken, maar die zult u bijna niet kunnen vinden. </p>

<p>Ik schrijf u vandaag voor de eerste en de laatste keer. Mocht blijken dat u niet antwoord, schrijf ik u direct weer. </p>

<p>Wij hebben geen inkomsten uit de melkveehouderij. Met de dood van mijn man is het laatste rund van het erf verdwenen. </p>

<p>Toen ik op de rem wilde trappen, was deze er niet. </p>

<p>Ik wilde de straat oversteken. Er kwam een auto van links recht op mij af. Ik dacht dat hij voor mij langs wilde en deed weer een stap terug. Hij wilde echter achter mij langs. Toen ik dat merkte deed ik snel weer twee passen vooruit. Maar de bestuurder had ook gereageerd en wilde nu toch voor mij langs. Hij stopte en draaide zijn raampje open. Woedend riep hij mij toe: "Blijf toch een keertje stilstaan!" Nou, dat deed ik ook inderdaad en toen reed hij mij aan. </p>

<p>Ik heb geen levensverzekering nodig. Ik zou graag willen dat iedereen echt treurig is als ik eenmaal sterf. </p>

<p>Ik wil mijn kind niet laten inenten. Mijn vriendin heeft haar kind ook laten inenten en daarna is het uit het raam gevallen. </p>

<p>Mijn auto reed eenvoudig rechtuit verder, wat er in een bocht over het algemeen toe leidt dat men de weg verlaat. </p>

<p>Alle rekeningen die ik ontvang, betaal ik nooit onmiddellijk, daar mij daarvoor eenvoudig het geld ontbreekt. De rekeningen worden integendeel in een grote trommel gestort waar ik er iedere maand geblinddoekt drie uit trek. Deze rekeningen betaal ik dan prompt. Ik verzoek u dan ook te wachten tot het lot u getroffen heeft. </p>

<p>Mijn dochter heeft haar voet verstuikt, omdat dat stomme vrouwvolk het verdomt fatsoenlijk schoeisel te dragen. </p>

<p>Meteen na de dood van mijn man ben ik weduwe geworden.</p>";s:4:"body";s:0:"";}