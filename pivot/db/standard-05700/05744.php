<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5744;s:4:"date";s:16:"2003-05-12-00-14";s:4:"user";s:7:"Da_Huge";s:5:"title";s:9:"Verwerken";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-05-12-00-14";s:9:"edit_date";s:16:"2003-05-12-00-14";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:2683:"<p>Raar eigenlijk, afgelopen maanden heb ik zo veel gepiekerd over hoe het zou zijn als mijn moeder er niet meer is. Nu is dat zo en ik weet het nog steeds niet. Nog altijd heb ik geen idee hoe ik me moet voelen of hoe ik me uberhaupt voel.</p>

<p>Heel raar is het, want ik besef me juist heel goed dat mama er niet meer is, toch leiden versprekingen vaak weer tot een pijnlijk besef en verdriet. Ik weet ook niet of het echt erg is wanneer ik zeg dat ik naar mijn ouders ga, terwijl alleen mijn pa eenzaam in huis is. De herinneringen worden ook vaak verdrongen door het beeld van mijn ma toen ze net overleden was en de beelden van de kist. Maanden geleden heb ik met de camera van Cor F. wat beelden geschoten in het huis van mijn ouders, vooral van mij ma om een duidelijke herinnering te hebben. Nu twijfel ik heel erg of ik Cor moet vragen om zijn camera te lenen om alles op een videoband ofzo te zetten, ik ben ontzettend bang dat dit te confronterend is.<br />
Vandaag ben ik ook voor het eerst weer gaan werken, ik moet zeggen dat alles in ��n grote waas gaat, afgelopen dagen ben ik wel wat uitgeweest en iedereen was harstikke lief en aardig om me te condoleren en af en toe vast te pakken, maar toch, ik weet niet goed wat ik ermee aan moet.<br />
Ik weet dat ik verder moet en besef me maar al te goed dat ik aan het begin sta van een hele nieuwe periode, maar toch, de leegte en de pijn is zo groot.<br />
Over vijf weken gaan we de as van mijn moeder begraven op een mooie plek vlakbij het graf van mijn opa, dit was de wens van mijn moeder omdat ze wist dat ik graag een plekje wilde hebben om naartoe te gaan.<br />
Nu weet ik echt nog niet hoe ik me daar zal gedragen, want gisteren zijn we zoals we dat besproken hadden voor de eerste keer naar de beeldentuin in Meersel Dreef geweest en hebben daar een kaarsje voor mijn moeder gebrand.<br />
Ik vind dat zelf heel erg fijn, in stilte mijn moeder gedenken, maar als ik dan mijn vader zie en een gebroken man zie lopen die zo leeg is en verlangt naar die lieve vrouw aan zijn zijde breekt mijn hart.<br />
Het zal nog moeilijk genoeg worden de komende tijd, want ik weet dat leven verder gaat en ik zelf ook verder moet, maar hoe ik dat ga doen, samen met Iris, daar moet ik nog even goed over nadenken.</p>

<p>Gisteren heb ik ook nog wat opgeschreven, zal voor veel mensen misschien erg <i>corny</i> zijn, maar zo voel ik het wel degelijk.</p>

<p><i>Met open handen op zoek naar een zucht<br />
Met mijn gezicht in de wind staren naar de lucht<br />
Vol verwachting en vol verlangen<br />
Waar ben je nu, kijk je met me mee?<br />
Ik geloof het wel, je volgt me op de trap van mij leven, tree na tree.</i></p>";s:4:"body";s:0:"";}