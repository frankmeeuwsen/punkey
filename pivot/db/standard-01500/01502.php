<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:1502;s:4:"date";s:16:"2004-10-25-12-07";s:12:"introduction";s:1297:"Gisteren is de ipod-tool <a href="http://www.dopplerradio.net/" title="">Dopplerradio</a> in 1.0 versie uitgekomen. Even de "What's new" van de site:

- Drumroll please, the number one on the wishlist: support for Windows Media Player playlists
- Support for video feeds! (RC1 did download video streams but did not add them to playlists. 1.0 Gold does) 
- You can subscribe to your bloglines subscriptions. Doppler will update your subscriptions list from your bloglines account
- History support. Doppler now keeps track of what you downloaded and stores this in a history.xml file in your downloads folder. You can manage the history from within Doppler by right clicking on the feedsubscription.
- You can now export and import your feedsubscriptions as an OPML file.
- And in general: it looks, feels and works better :-)

Ben je ook wat met podcasting bezig, kijk eens naar <a href="http://www.dopplerradio.net/" title="Doppler : Podcasting redefined">DopplerRadio</a>. Ik vind het persoonlijk een stuk lekkerder werken dan de iPodder-tool die overal wordt geplugd. Met name de videosupport is erg boeiend en die ga ik zeker eens uitproberen. Ik hoorde in de Daily Source Code al een berichtje van Phil Torrone van Engadget die hierover aan het denken zijn. Boeiende ontwikkelingen!";s:4:"body";s:0:"";s:8:"category";a:1:{i:0;s:7:"default";}s:12:"publish_date";s:16:"2004-10-25-12-03";s:9:"edit_date";s:16:"2004-10-25-12-07";s:5:"title";s:21:"Dopplerradio 1.0 uit!";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"1";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}