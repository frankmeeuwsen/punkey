<?php /* pivot */ die(); ?>a:16:{s:4:"code";i:1556;s:4:"date";s:16:"2004-11-20-20-18";s:12:"introduction";s:1869:"<a href="http://www.wired.com/wired/archive/12.11/sample.html" title="">De Wired CD</a>. Baby Grandpa heeft er eerder deze week al over geschreven en gisteren heb ik eindelijk eens de tijd gehad om de CD te luisteren. Aangezien mijn MP3 speler nog steeds in de garage ligt ben ik weer aangewezen op ouderwetsche CD-spelers. De muziek die wordt aangeboden via het <a href="http://creativecommons.org/license/sampling?lang=en" title="">sampling plus</a> en noncommercial sampling plus license [<a href="http://creativecommons.org/license/sampling?lang=en" title="">uitleg</a>] is zo verschrikkelijk de moeite waard om <a href="http://creativecommons.org/wired/" title="">te downloaden</a> en te beluisteren. Ik heb deze week nog wat links van <a href="http://www.babygrandpa.com/" title="">Baby G</a> gekregen over het hele Creative Commons principe. Dit naar aanleiding van een gesprek wat we hadden onder het genot van een kickass indonesisch etentje.

[[image:commmns.jpg::center:0]]

Ik word meer en meer een voorstander van het Creative Commons principe. Niet omdat ik als consument er minder of niets voor hoef te betalen, maar met name omdat de artiest zelf er ook beter van wordt. Platenmaatschappijen kunnen nog steeds een rol hebben in het Creatve Commons principe. Maar meer als faciliterende partij waar artiesten zelf kunnen shoppen welke diensten ze van een maatschappij afnemen. Dit geldt niet alleen voor muziek maar ook voor boeken, video en foto's. De <a href="http://creativecommons.org/" title="">site van Creative Commons</a> is een zekere must-read voor de komende tijd! Deze weblog heeft trouwens ook een Creative Commons licentie namelijk de <a href="http://creativecommons.org/licenses/by-nc-sa/1.0/" title="">Attribution-NonCommercial-ShareAlike 1.0</a> licentie. 
Ik quote even de mail die ik heb ontvangen van Baby G over dit onderwerp...";s:4:"body";s:2769:""Ik ben gisteren nog even aan de snuffel gegaan om je wat verder te informeren over de mogelijkheden die het net de artiest biedt om toch een normaal salaris cent te verdienen. Ik heb een paar superlinks op de kop weten te tikken via Downhill Battle.
 
Allereerst is het statement van <a href="http://www.downhillbattle.org" title="">www.downhillbattle.org</a> zelf glashelder:
 
<i>"Five major record labels have a monopoly that's bad for musicians and music culture, but now we have an opportunity to change that. We can use tools like filesharing to strengthen independent labels and end the major label monopoly.  How do musicians get paid for downloads? Simple: collective licensing lets people download unlimited music for a flat monthly fee ($5-$10) and the money goes to musicians and labels according to popularity. This solution preserves the cultural benefits of p2p, gets musicians way more money, and levels the playing field. Our
plan is to explain how the majors really work, develop software to make filesharing stronger, rally public support for a legal p2p compensation
system, and connect independent music scenes with the free culture movement".
</i> 

The New York Times heeft nog een geweldig stuk online staan over '<a href="http://www.nytimes.com/2004/06/25/opinion/25MCLE.html?ex=1403496000&en=833ac0f87453f807&ei=5007&partner=USERLAND" title="">How musicians and record labels can get paid from p2p filesharing</a>'.
 
Meer mooie details over dit onderwerp lees je hier: <a href="http://www.eff.org/share/collective_lic_wp.php" title="">http://www.eff.org/share/collective_lic_wp.php</a> 
Je kan er ook de .pdf files van downen.
 
Vervolgens is er een glashelder statement te vinden dat het overduidelijk maakt waarom de consument af moet van de major record labels. Lees hier: <a href="http://www.downhillbattle.org/reasons/" title="">http://www.downhillbattle.org/reasons/</a>
 
Daarna gaan we over naar een prachtig (maar lang) geschreven relaas van rockstar Steve Albini die, als je helemaal naar beneden scrollt, een geweldig rekenoverzicht geeft van de manier waarop de artiest anno nu door de majors wordt uitgeknepen. Lees hier: <a href="http://www.negativland.com/albini.html" title="">http://www.negativland.com/albini.html</a>
 
 
Tot slot kun je nog luisteren naar het betoog van Lawrence Lissig, die op de OSCON van 2000
meer dan een half uur lang uitlegde hoe het precies zit met muziek, internet, samplegebruik,
creativiteit en de onmogelijkheid om het verleden niet te verweven in het heden. Downloaden hoeft
hier niet: de site begint met loaden en na een seconde of 20 begint de speech als je klikt op 'continue': http://randomfoo.net/oscon/2002/lessig/free.html
 
Zeer zeker voer om over na te denken.";s:8:"category";a:1:{i:0;s:7:"default";}s:12:"publish_date";s:16:"2004-11-20-20-04";s:9:"edit_date";s:16:"2004-11-20-20-18";s:5:"title";s:44:"Sample the future. Rip, sample, mash, share.";s:8:"subtitle";s:0:"";s:4:"user";s:6:"punkey";s:10:"convert_lb";s:1:"1";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:8:"keywords";s:0:"";s:7:"vialink";s:0:"";s:8:"viatitle";s:0:"";}