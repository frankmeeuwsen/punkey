<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5228;s:4:"date";s:16:"2003-03-14-13-29";s:4:"user";s:7:"Da_Huge";s:5:"title";s:14:"Style Wars DVD";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-03-14-13-29";s:9:"edit_date";s:16:"2003-03-14-13-29";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:1556:"<p>DVD includes over 3 1/2 hours of extras: new interviews with the <br />
artists, hundreds of trains, remastered soundtrack and lots more.</p>

<p><i>2-DVD SET HITS STORES APRIL 22ND. </i></p>

<p>Disc 1 <br />
- Style Wars (1983, 70 minutes, color/b&w, 16mm, 1.33:1 original ratio) <br />
- Over 23 minutes of original outtake footage <br />
- Feature commentary by director-producer Tony Silver and producer Henry Chalfant <br />
- Interview with Tony Silver and Henry Chalfant (2002) <br />
- Interview with Style Wars editors Victor Kanefsky and Sam Pollard (2002) <br />
- English subtitles for the hearing impaired <br />
- Digitally remastered 5.1 Dolby soundtrack featuring classic tracks from Grandmaster Flash, The Treacherous Three, Trouble Funk, The Fearless Four, Rammellzee vs. K-Rob and more... </p>

<p>Disc 2 <br />
- 32 artist galleries including new interviews, trains and rare photos of: Blade, Cap, Cey, Crash, Crazy Legs, Daze, Dez, Dondi, <br />
Doze, Duro, Duster, Frosty Freeze, IZ the Wiz, Kase 2, Kel First, Ken Swift, Lee, Mare139, Min One, Noc 167, Paze (Erni), Pink, Quik, Rammellzee, Revolt, Sach, Seen TC5, Seen UA, Shy 147, Skeme, Tracy 168, and Zephyr <br />
- Tributes to Dondi and Shy 147 <br />
- Guest interviews: Fab 5 Freddy, Goldie, Guru, DJ Red Alert, and photographer Martha Cooper <br />
- "DESTROY ALL LINES" 30 min. loop of over 200 whole cars and burners <br />
- Featuring music from: Def Jux recording artists El-P, RJD2 and Aesop Rock, plus Mr. Wiggles, Kimson "Angola-Red" Albert, and Darryl Jenifer</p>";s:4:"body";s:0:"";}