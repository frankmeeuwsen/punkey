<?php /* pivot */ die(); ?>a:13:{s:4:"code";i:5624;s:4:"date";s:16:"2003-04-22-09-51";s:4:"user";s:7:"Da_Huge";s:5:"title";s:38:"Zangeressen moeten meer jazz luisteren";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-04-22-09-51";s:9:"edit_date";s:16:"2003-04-22-09-51";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:1460:"<p>Hmmm, de laatste tijd hoor ik steeds vaker hetzelfde patroon bij hedendaagse zangeressen. Vergeef me mijn terminologie, maar met de uithalen die zangeressen tegenwoordig om de zin produceren slaan ze volgens mij totaal de plank mis. Ik zal het proberen uit te leggen.</p>

<p>Dit moet me van het hart.<br />
Gisteren trad bij Barend & Van Dorp Ilse De Lange op.<br />
Zonder twijfel ��n van de beste zangeressen uit Nederland.<br />
Met haar natuurlijk snik in haar stem en grote stembereik brengt ze op een onavolgbare wijze mooie songs ten gehore.<br />
<i>Toch</i> klopt er iets niet, net als meerdere Nederlandse zangeressen (Do, Trijntje) heeft Ilse er een handje van om OM de zin haar stem te verheffen.<br />
Het is moeilijk uit te leggen, maar het valt mij op dat in veel nummers de zangeressen hun gevoel willen uitdrukken in stemvolume (en dan vooral een stemverheffing).<br />
Zangeressen zouden eens wat meer naar jazz moeten luisteren, zet eens een cd op van Chet Baker, deze man gebruikt zijn trompet als stemgeluid en legt vaak op een zeer gevoelige manier stemmingen en emoties neer.<br />
Ik hoop dat zangeressen eens wat meer gaan proberen om met meer gevoel en terughoudendheid hun songs gaan neerzetten, want de emoties passen niet bij het dergelijk gebruiken van de stem.<br />
Het zijn zulke terugkerende golfbewegingen dat het erg saai wordt.</p>

<p>Waarschijnlijk snapt niemand wat van dit verhaal, maar ik moest het even kwijt.</p>";s:4:"body";s:0:"";s:8:"comments";a:1:{i:0;a:6:{s:4:"name";s:5:" Arja";s:5:"email";s:23:" cira_arice@hotmail.com";s:2:"ip";s:14:" 62.58.205.201";s:4:"date";s:16:"2003-09-23-19-36";s:3:"url";s:1:" ";s:7:"comment";s:400:"Een late reactie misschien maar toch wil ik even reageren. Niet om ertegen in te gaan - integendeel ik ben het helemaal eens met dit verhaal.
Ik zing zelf ook en het liefst zing ik jazz en blues omdat er een veel subtielere emotie onder ligt die niet gaat om het 'laten zien wat je kan'. Het gaat er meer om de emotie van het nummer weer te geven.Ik ben blij dat sommige mensen er ook zo over denken.";}}}