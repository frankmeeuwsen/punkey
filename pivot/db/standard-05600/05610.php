<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5610;s:4:"date";s:16:"2003-04-20-00-04";s:4:"user";s:6:"Punkey";s:5:"title";s:16:"Licensed to kill";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-04-20-00-04";s:9:"edit_date";s:16:"2003-04-20-00-04";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:1554:"<p>Ik ben geen fanatieke anti-roker, maar ik vind <a title="Licensed to Kill, Inc - About Our Company" href="http://www.licensedtokill.biz/about.html">dit</a> toch wel erg grappig en "to the point"</p>

<p>"By taking such a name, Licensed to Kill, Inc clearly identifies what it is: a company that has been given the explicit permission by the state to manufacture and market tobacco products in a way that each year kills over 400,000 Americans and 4.5 million other persons worldwide. In short, a company that profits off of some of the world's most deadly brands."</p>

<p>Briljant zijn ook de merknamen: Genocide, Chain, Serial Killer.</p>

<p>Doet me denken aan een oude Denis Leary skit (check <a href="http://www.endor.org/leary/#Smoke">hier</a> de tekst) waarin hij het <a href="http://www.endor.org/leary/#Drugs">heeft</a> over "You could have cigarrets that come in a black pack, with a skull and a cross bone on the front, called tumors" en ook die worden gewoon verkocht...</p>

<p>Het bedrijf bestaat ook echt. Hierbij een quote van <a href="http://home-1.tiscali.nl/~kuifje/">Esc</a> (bij gebrek aan Trackback)</p>

<p>"De Amerikaanse activist Ralph Nader haalt weer de krantenkoppen. Zijn actiegroep Essential Action heeft in Virginia een tabaksfirma opgericht en laten registreren onder de naam Licensed To Kill. Volgens een artikel in USA Today wil de actiegroep zo laten zien dat niemand, geen overheid en geen wet, een bedrijf tegenhoudt dat als intentie heeft te doden. Als het om geld verdienen gaat, is in de VS alles mogelijk."</p>";s:4:"body";s:0:"";}