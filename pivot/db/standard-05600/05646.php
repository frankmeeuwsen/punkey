<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5646;s:4:"date";s:16:"2003-04-24-11-08";s:4:"user";s:7:"Da_Huge";s:5:"title";s:20:"USA: meer kernbommen";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-04-24-11-08";s:9:"edit_date";s:16:"2003-04-24-11-08";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:1245:"<p>De USA heeft niet genoeg aan 10.000 kernwapens. Daarom zal de productie weer opgestart worden, deze keer in het Los Alamos National Laboratory.</p>

<p>Gisteren is door Los Alamos bekend gemaakt dat, voor het eerst in 14 jaar, er een plutonium kern gemaakt is. Dat is het hart van moderne kernwapens. In 1989 werd de productie gestaakt door de FBI nadat er milieuproblemen waren bij de Rocky Flats fabriek.</p>

<p>De eerste vier jaar zullen in Los Alamos 6 kernkoppen per jaar gemaakt worden. Daarna gaat het tempo omhoog: 10 per jaar. Per 2018 komt een hele nieuwe fabriek in werking die maar liefst 500 stuks per jaar kan afleveren.</p>

<p>Deze plannen zijn in lijn met de idee�n van de regering Bush om door middel van meer nucleaire wapens de veiligheid van de USA te waarborgen.</p>

<p>Vanwege radioactief verval kunnen de kernwapens onverwacht onbruikbaar worden. De USA heeft al 5000 kernkoppen in reserve, maar wil in staat zijn om sneller en meer kernkoppen te fabriceren dan welk ander land ook.</p>

<p><i>Godverse ******lijers, vallen wij nu ook met zijn allen de VS aan omdat ze een bedreiging vormen voor de wereldvrede?<br />
Achterlijke rednecks.</i></p>

<p><a href="http://frontpage.fok.nl/news.fok?id=27990">Bron</a></p>";s:4:"body";s:0:"";}