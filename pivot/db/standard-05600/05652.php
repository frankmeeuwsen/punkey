<?php /* pivot */ die(); ?>a:12:{s:4:"code";i:5652;s:4:"date";s:16:"2003-04-24-21-34";s:4:"user";s:6:"Punkey";s:5:"title";s:23:"Nog meer internetnieuws";s:6:"status";s:7:"publish";s:14:"allow_comments";s:1:"1";s:10:"convert_lb";i:1;s:12:"publish_date";s:16:"2003-04-24-21-34";s:9:"edit_date";s:16:"2003-04-24-21-34";s:8:"category";a:1:{i:0;s:16:"oldskool_archief";}s:12:"introduction";s:747:"<p><a title="Google Acquires Applied Semantics" href="http://www.google.com/press/pressrel/applied.html">Google Acquires Applied Semantics</a></p>

<p> Google announced that it acquired Applied Semantics, producer of software applications for the online advertising. Applied Semantics' products and engineering team will strengthen Google's search and advertising programs, including its fast-growing content-targeted advertising offering. </p>

<p>[..]<br />
Applied Semantics' products are based on its patented CIRCA technology, <i>which understands, organizes, and extracts knowledge from websites and information repositories in a way that mimics human thought and enables more effective information retrieval.</i> </p>

<p>Interesting...</p>";s:4:"body";s:0:"";}