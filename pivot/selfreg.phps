<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under 
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------

// If you don't want the self-registered users to have access to all
// categories, uncomment the line below (and list only the categories
// the user is allowed to post in).
// $selfreg_cats = array("Category1","Category2");
//
// END USER CONFIGURATION

// don't access directly..
if(!defined('INPIVOT')){ header("Location: index.php?func=selfreg"); }

global $Pivot_Vars, $Cfg, $Users;
PageHeader(lang('userinfo','selfreg'), 0);
if (!$Cfg['selfreg']) {
    echo "<div style='padding: 20px;'>";
    echo "<h1>Disabled</h1></div>";
    PageFooter();
    exit;
}
$setupstepn = 5;
$Setupstep1 = array(
	array('heading',lang('userinfo','selfreg'),'', 8, '', '2', ''),
	array('step', '', '', 7, '1', '', ''),
	array('username', lang('userinfo','username'), '', 0, '', '', ''),
	array('pass1', lang('userinfo','pass1'), '', 1, '', '', 'maxlength="15"'),
	array('pass2', lang('userinfo','pass2'), '', 1, '', '', 'maxlength="15"'),
	array('email', lang('userinfo','email'), '', 0, '', '', ''),
	array('nick', lang('userinfo','nickname'), '', 0, '', '', ''),	
);
	
if(!isset($Pivot_Vars['step'])){
	$setupstepn = 1;	
}elseif($Pivot_Vars['step'] == 1){
        if(isset($Users[$Pivot_Vars['username']])){
                $Setupstep1[2][2] = lang('userinfo','username_in_use');
                $Piverr++;
        }

	if(strlen($Pivot_Vars['username']) < 3){
		$Setupstep1[2][2] = lang('userinfo', 'username_too_short');
		$Piverr++;
	}
	
	if((trim(strtolower($Pivot_Vars['pass1']))) != (trim(strtolower($Pivot_Vars['pass2'])))) {
		$Setupstep1[4][2] = lang('userinfo', 'pass_dont_match');;
		$Piverr++;		
	}

	if( (trim(strtolower($Pivot_Vars['pass1']))) == (trim(strtolower($Pivot_Vars['username'])))) {
		$Setupstep1[4][2] = lang('userinfo', 'pass_equal_name');
		$Piverr++;		
	}
	
	if((strlen($Pivot_Vars['pass1'])) < 4 ){
		$Setupstep1[3][2] = lang('userinfo', 'pass_too_short');
		$Piverr++;		
	}	
	
	if((strlen($Pivot_Vars['pass1'])) > 14 ){
		$Setupstep1[3][2] = 'Password can be 14 characters at most. You know you will have to remember this, right?';
		$Piverr++;		
	}	
	
	if(!(isemail($Pivot_Vars['email']))) {
		$Setupstep1[5][2] = lang('userinfo', 'not_good_email');
		$Piverr++;		
	}
	
	if($Piverr > 0){
		$setupstepn = 1;
		$Setupstep1[2][4] = $Pivot_Vars['username'];
		$Setupstep1[5][4] = $Pivot_Vars['email'];
		$Setupstep1[6][5] = $Pivot_Vars['nick'];
				
	
	}else{
		
		$setupstepn = 2;
			
		$Users[$Pivot_Vars['username']]['pass'] = md5($Pivot_Vars['pass1']);
		$Users[$Pivot_Vars['username']]['email'] = $Pivot_Vars['email'];
		$Users[$Pivot_Vars['username']]['nick'] = $Pivot_Vars['nick'];
		$Users[$Pivot_Vars['username']]['userlevel'] = 1;
		$Users[$Pivot_Vars['username']]['language'] = $Cfg['deflang'];

                $Cfg['users'] .= '|'.$Pivot_Vars['username'];

                // set the categories.,
                if (!isset($selfreg_cats)) $selfreg_cats = cfg_getarray('cats');
                foreach ($selfreg_cats as $category) {
                        // add the user..
                        $allowed_users = explode("|", $Cfg[ 'cat-'.$category]);
                        $allowed_users[] = $Pivot_Vars['username'];
                        $Cfg[ 'cat-'.$category ] = implode("|", $allowed_users);
                }


		PutUserInfo();
	}
}

$var = "Setupstep{$setupstepn}";
if($setupstepn==1){
	$next = lang('general','go');
}else{

	SaveSettings();
	redirect('index.php');

}

//it's nice to use it as a variable so we can look at $BigAssArray[$i][0] for a list of variables to save in config.
StartForm('selfreg');
StartTable($caption);
DisplaySettings($$var);
EndForm($next, 1);
PageFooter();	

?>

