// Cookie handling routines

function getCookieVal (offset) {
	var endstr = document.cookie.indexOf (";", offset);
	if (endstr == -1) { endstr = document.cookie.length; }
	return unescape(document.cookie.substring(offset, endstr));
	}

function GetCookie (name) {
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	while (i < clen) {
		var j = i + alen;
		if (document.cookie.substring(i, j) == arg) {
			return getCookieVal (j);
			}
		i = document.cookie.indexOf(" ", i) + 1;
		if (i == 0) break; 
		}
	return null;
	}

function DeleteCookie (name,path,domain) {
	if (GetCookie(name)) {
		document.cookie = name + "=" +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		"; expires=Thu, 01-Jan-70 00:00:01 GMT";
		}
	}

function SetCookie (name,value,expires,path,domain,secure) {
	expires = new Date;
	expires.setMonth(expires.getMonth()+9);
  document.cookie = name + "=" + escape (value) +
    ((expires) ? "; expires=" + expires.toGMTString() : "") +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    ((secure) ? "; secure" : "");
	}
// End of Cookie handling routines.	


var THEME_KEY = "stheme";	// this key is used to store the selected theme.
var DEFAULT_THEME = "graphite";	// this is the default theme for first time visitors.
var TEMPLATEDIR = "/pivot/templates/"; // path to pivot template dir

// the main routine.

function changeStyleSheet() {
	var s = GetCookie(THEME_KEY);	// check for cookie theme.
	if (s == null) s = DEFAULT_THEME;	// if no cookie then set up default theme. 
	document.getElementById('colors').href = TEMPLATEDIR+s+".css";	// this bit does all the work rewriting the stylesheet link href.
}

function setTheme(name) {
	DeleteCookie(THEME_KEY);					
	SetCookie(THEME_KEY, name);
	document.getElementById('colors').href = TEMPLATEDIR+name+".css";	// this bit does all the work rewriting the stylesheet link href.
	self.location = self.location;	// simple trick to reload the current document
}
	
changeStyleSheet(); // call the main routine.
