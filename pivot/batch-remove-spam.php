<?php


/*
==================================================================
     PIVOT BATCH SPAM REMOVER

Creator:                Hans Fredrik Nordhaug
E-mail:                 hans@nordhaug.priv.no
License:                GPL

Changes:

0.1 - 2006.03.20: Initial release. 
0.2 - 2006.03.21: Added password protection. Now also removes deleted 
  trackbacks from last trackbacks. Removed some trackback related 
  foreach-warnings. Minor code clean-up.

==================================================================

For more info launch the script in a browser and read the help page.
*/

// START CONFIGURATION

// A blank password ("") will disable the script completely.
$password = "spamsucks";

// END CONFIGURATION

$version = "0.2 (2006.03.20)";

$dir = dirname(__FILE__);
if (file_exists($dir."/pivot/pv_core.php")) {
    $pivot_dir = $dir."/pivot";
    $pivot_url = "pivot";
} elseif (file_exists("pv_core.php")) {
    $pivot_dir = $dir;
    $pivot_url = ".";
} else {
    die("Didn't find the Pivot directory!");
}

$footer = '
</div> <!-- End of body -->
</body>
</html>';

if (!function_exists('file_get_contents')) {
    function file_get_contents($filename, $use_include_path = 0) {
        $file = @fopen($filename, 'rb', $use_include_path);
        if ($file) {
            if ($fsize = @filesize($filename)) {
                $data = fread($file, $fsize);
            } else {
                while (!feof($file)) {
                    $data .= fread($file, 1024);
                }
            }
            fclose($file);
        }
        return $data;
    }
}

if (!function_exists('file_put_contents')) {
    function file_put_contents($filename, $data) {
        $file = @fopen($filename, 'wb');
        if ($file) {
            $size = fwrite($file, $data);
            fclose($file);
        }
        return $size;
    }
}

echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Pivot Batch Spam Remover '.$version.'</title>
<style type="text/css">
.warn { background-color: yellow }
.error { background-color: red }
body {
	padding: 10px 4px;
}
h1, h2 {
	text-align: center;
	font-family: tahoma, verdana, geneva, arial, helvetica, sans-serif;
}
#content {
        border: 1px dashed #666;
        margin-left: 201px;
        margin-top: 4px;
	padding-left: 4px;
	padding-right: 4px;
}
#header {
        background: #314e8c;
        padding: 8px;
        background-image: url(http://pivotlog.net/pics/banner.jpg);
}
#leftcolumn {
        left: 10px;
        position: absolute;
        width: 200px;
        margin-top: 4px;
}
</style>
</head>
<body>
<div id="header">
<span><a href="http://pivotlog.net/"><img src="http://pivotlog.net/pics/logo.png" 
	width="246" height="71" border="0" alt="pivot" /></a></span>
</div>
<div id="leftcolumn">
<h2>Menu</h2>
 <ul>
 <li><a href="'.$_SERVER["PHP_SELF"].'">Frontpage</a>
 <li><a href="?action=help">Help</a>
 </ul>
</div>
<div id="content">
<h1>Pivot Batch Spam Remover</h1>
';

if (empty($password)) {
    echo '
    <h2 class="error">Password setting is blank - the scipt is disabled</h2>
    <p>Open the file in a text editor and give a value for $password in
    the head of the file - look for CONFIGURATION.</p>';
} elseif (isset($_REQUEST["submit"]) && ($_REQUEST["password"] != $password)) {
    echo '
    <h2 class="error">Wrong Password</h2>';
} elseif ($_REQUEST["submit"] == "Remove Spam") {
    printheader("Removed");
    spam();
} else if ($_REQUEST["submit"] == "List Spam") {
    printheader("Listing");
    echo form("Remove Spam");
    spam();
} elseif ($_REQUEST["action"] == "help"){
    echo '
    <h2>Help</h2>
    <p style="text-align: center; text-width: 80%"> [ 
    <a href="?action=help#warn">A warnings</a> | 
    <a href="?action=help#known_lim">Known limitations</a> ]</p>
    <p>This PHP script batch removes spam from comments and trackbacks</p>
    <p>It has it\'s <a href="http://forum.pivotlog.net/viewtopic.php?t=9572">own thread</a> 
    in the Pivot forum.  If you still have any questions after
    reading this page, ask your questions there.</p>
    <a name="warn"></a>
    <h3>A warnings</h3>
    <p class="warn">The script has no undo-function.
    So if you are paranoid or just wants to be on the safe side: <em>Back-up the
    db-folder.</em> This script is still work in progress. </p>
    <a name="known_lim"></a>
    <h3>Known limitations</h3>
    <ul>
    <li>Does not support regular expressions (regexp) to find the spammy
    comments/trackbacks.
    <li>If you have a lot of entries, the script might not work
    (because of the time-out limit in PHP). I have still not added
    the reload trick that Pivot uses.
    </ul>
    '; 
} else {
    if (isset($_REQUEST["action"])) {
	echo '<p class="error">Unknown action.</p>';
    }
    echo '    <p>This PHP script batch removes spam from comments and
    trackbacks.</p>
    <p>Read more on the <a href="?action=help">help page</a>.</p>
    '.form("List Spam").'
    <h3>About</h3>
    <p>This script is written by Hans Nordhaug - "hansfn" in the 
    <a href="http://forum.pivotlog.net/">Pivot Forums</a>. 
    This is version '.$version.'. I wrote it as a reply to the recent spam
    wave - read more in the Pivot Forums - 
    <a href="http://forum.pivotlog.net/viewtopic.php?t=9528">Finally some
    friendly spam</a>.</p>
    ';
}

echo $footer;

function printheader($text) {
    if ($_REQUEST["what"] == "Comments") {
        echo '<h2>'.$text.' Spam from comments</h2>';
    } elseif ($_REQUEST["what"] == "Trackbacks") {
        echo '<h2>'.$text.' Spam from trackbacks</h2>';
    } elseif ($_REQUEST["what"] == "Both") {
        echo '<h2>'.$text.' Spam from comments and trackbacks</h2>';
    } else {
        echo '<p class="error">What spam to remove not given.</p>';
        echo $footer;
        exit(1);
    }
}

function form($text) {
    $output = '
    <form action="'.$_SERVER['PHP_SELF'].'" method="GET">
    <table>
    <tr><td>Look in: </td><td>';
    if (!empty($_REQUEST["what"])) {
        $output .= '<input name="what" readonly="readonly" value="'.$_REQUEST["what"].'">';
    } else {
        $output .=
    '<select name="what">
    <option>Comments</option>
    <option>Trackbacks</option>
    <option>Both</option>
    </select>';
    }
    $output .= ' </td></tr>
    <tr><td>Spam text: </td><td>';
    if (!empty($_REQUEST["spamword"])) {
        $output .= '<input name="spamword" readonly="readonly" value="'.$_REQUEST["spamword"].'">';
    } else {
        $output .= '<input name="spamword" value="">';
    }
    $output .= ' </td></tr>
    <tr>';
    if (!empty($_REQUEST["password"])) {
        $output .= '<td colspan="2"><input name="password" type="hidden" value="'.$_REQUEST["password"].'">';
    } else {
        $output .= '<td>Password: </td><td><input name="password" value="">';
    }
     $output .= '
    </td></tr>
    <tr><td colspan="2">
    <input type="submit" name="submit" value="'.$text.'" />
    </td></tr>
    </table>
    </form>';
    if ($text == "Remove Spam") {
        $output .= '
    <p>The form values above are read-only. Go back in your browser to change them.</p>';
    } else {
        $output .= '
    <p>Sapm text (above) can match anything in the comment/trackback - name,
    IP, URL, comment/trackback and so on. This makes it quite poweful...</p>';
    }
    return $output;
}

function spam() {    
    global $pivot_dir, $pivot_url;
    $dir = $pivot_dir."/db/";
    $entrydirs = array();
    $entries = array();
    // Finding all entries (as files)
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            $path = $dir.$file;
            if (is_dir($path) && (strpos($path,"standard-") > 0)) {
                $path .= "/";
                array_push($entrydirs,$path);
                $entrydh = opendir($path);
                while (($file = readdir($entrydh)) !== false) {
                    $entrypath = $path.$file;
                    if (is_file($entrypath) && (strpos($entrypath,"index-") === false)) {
                        array_push($entries,$entrypath);
                    }
                }
                closedir($entrydh);
            }
        }
    }
    closedir($dh);
    sort($entries);
    $die_string = "<?php /* pivot */ die(); ?>";
    if (($_REQUEST["what"] == "Trackbacks") || ($_REQUEST["what"] == "Both")) {
        $trackbacks = true;
    } else {
        $trackbacks = false;
    }
    if (($_REQUEST["what"] == "Comments") || ($_REQUEST["what"] == "Both")) {
        $comments = true;
    } else {
        $comments = false;
    }
    $tot_com_spam = 0;
    $tot_tb_spam = 0;
    if ($_REQUEST["submit"] == "Remove Spam") {
        if ($comments) {
            $last_comms = unserialize(str_replace($die_string,"", 
                file_get_contents($pivot_dir."/db/ser_lastcomm.php")));
        }
        if ($trackbacks) {
            $last_tracks = unserialize(str_replace($die_string,"", 
                file_get_contents($pivot_dir."/db/ser_lasttrack.php")));
        }
    }
    foreach ($entries as $file) {
        if (!is_readable($file) || !is_writable($file)) {
            echo "$file isn't readable/writable.\n";
            continue;
        }
        $data = unserialize(str_replace($die_string,"",file_get_contents($file)));
        $entry_id = basename($file,".php");
        foreach ($data as $key => $value) {
            if (($key == "trackbacks") && (count($data["trackbacks"])>0) && $trackbacks) {
                foreach ($data["trackbacks"] as $tbid => $tb) {
                    $text = implode(" ", $tb);
                    if (strpos(strtolower($text),strtolower($_REQUEST["spamword"])) !== false) {
                        $output .= "<tr><td>$entry_id</td><td>Trackback</td><td>$text</td></tr>\n";
                        $tot_tb_spam ++;
                        unset($data["trackbacks"][$tbid]);
                        //remove the trackback from last_trackbacks if it's in there..
                        if (($_REQUEST["submit"] == "Remove Spam") && (count($last_tracks)>0)) {
                            foreach ($last_tracks as $lt_key => $last_track) {
                                if ( ($last_track['code'] == $data['code']) &&
                                ($last_track['name'] == $tb['name']) &&
                                ($last_track['date'] == $tb['date'])){
                                    unset($last_tracks[$lt_key]);
                                }
                            }
                        }
                    }
                }
            } 
            if (($key == "comments") &&  (count($data["comments"])>0) && $comments) {
                foreach ($data["comments"] as $comid => $com) {
                    $text = implode(" ", $com);
                    if (strpos(strtolower($text),strtolower($_REQUEST["spamword"])) !== false) {
                        $output .= "<tr><td>$entry_id </td><td>Comment</td><td>$text</td></tr>\n";
                        $tot_com_spam ++;
                        unset($data["comments"][$comid]);
                        //remove the comment from last_comments if it's in there..
                        if (($_REQUEST["submit"] == "Remove Spam") && (count($last_comms)>0)) {
                            foreach ($last_comms as $lc_key => $last_comm) {
                                if ( ($last_comm['code'] == $data['code']) &&
                                ($last_comm['name'] == $com['name']) &&
                                ($last_comm['date'] == $com['date'])){
                                    unset($last_comms[$lc_key]);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($_REQUEST["submit"] == "Remove Spam") {
            file_put_contents($file, $die_string.serialize($data));
        }
    }
    if ($_REQUEST["submit"] == "Remove Spam") {
        if ($trackbacks) {
            file_put_contents($pivot_dir."/db/ser_lastcomm.php", $die_string.serialize($last_comms) );
        }
        if ($comments) {
            file_put_contents($pivot_dir."/db/ser_lasttrack.php", $die_string.serialize($last_tracks) );
        }
    }
    if ($_REQUEST["submit"] == "Remove Spam") {
        if ($trackbacks) {
            echo "<p>Removed $tot_tb_spam Spam Trackbacks</p>\n";
        }
        if ($comments) {
            echo "<p>Removed $tot_com_spam Spam Comments</p>\n";
        }
        echo '<p>Go to your <a href="'.
        $pivot_url.'/index.php?menu=admin&amp;func=admin&amp;do=maintenance">Pivot admin</a> and 
        execute <em>Rebuild the Index</em> and then <em>Rebuild All Files</em>
        or go to the <a href="'.$_SERVER["PHP_SELF"].'">frontpage</a> to remove more spam before
        rebuilding.</p>';
    } elseif ($_REQUEST["submit"] == "List Spam") {
        if ($trackbacks) {
            echo "<p>Found $tot_tb_spam Spam Trackbacks</p>\n";
        }
        if ($comments) {
            echo "<p>Found $tot_com_spam Spam Comments</p>\n";
        }
        if ($tot_com_spam > 0 || $tot_tb_spam > 0) {
            echo '
        <h3>Complete Spam Listing for "'.$_REQUEST["spamword"].'"</h3>
        <table>
        <tr><th>Entry</th><th>Type</th><th>Text</th></tr>'.$output."</table>\n";
        }
    }
}

?>
