<?php


// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under 
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------


$maxtime = 20; // 10 seconds..
$maxaccess = 10; // max 10 accesses during this period.

$floodfile = dirname(dirname(__FILE__))."/db/floodfile.txt";
$floods = load_floodfile();
save_floodfile($floods);
check_flood($floods);

function load_floodfile() {
	global $floodfile;

	
	if (file_exists($floodfile)) {
		$file = file($floodfile);	
	} else {
		$file = array();
	}
	
	foreach($file as $access) {
		$access = explode(":", trim($access));	
		$floods[] = $access;
	}

	return $floods;
	
}

function save_floodfile($floods) {
	global $floodfile, $maxtime, $maxaccess;
	
	$time = mktime();
	
	$floods[] = array( $time, $_SERVER['REMOTE_ADDR']);
	
	$fp = fopen($floodfile,"w");
	
	foreach($floods as $access) {
		if (($time - $access[0])<=$maxtime) {
			fwrite($fp, sprintf("%s:%s\n", $access[0], $access[1]));
		}
	}
	
	fclose($fp);
	
}


function check_flood($floods) {
	global $maxtime, $maxaccess;
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$time = mktime();
	
	$match = 0;
	if (is_array($floods)) {
		foreach($floods as $access) {
			if( ($ip == $access[1]) && (($time - $access[0])<=$maxtime) ) {
				$match++;
			}
		} 	
	}
		
	if ($match>$maxaccess) {
		header('Status: 403 forbidden',403);
		header('Content-type: text/plain');
		exit( "Settle down, Beavis!\n\nPlease wait $maxtime seconds, and try again." );
	}
	
}


?>