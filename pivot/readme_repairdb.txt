
Pivot db-repair tool 0.4..

You can use this tool if you're having problems rebuilding or reindexing your 
pivot files. It will attempt to fix the files it can, and the files it can not 
fix will be renamed so Pivot can skip them. Use the tool as following:

1) Make a backup of your pivot/ folder. Nothing is going to break, but
   having a current backup is always a good thing..
2) Unzip the file and upload the five files to your pivot/ folder
3) chmod repair_counter.txt to 777
4) open /pivot/repair_db1.php in your browser, and see if it gets to the
   'done' part.
5) do the same with /pivot/repair_db2.php, /pivot/repair_db3.php and 
   /pivot/repair_db4.php.
6) if necessary, repeat steps 4) and 5) until all three get to 'done'.
7) open /pivot/repair_db_cleanup.php in your browser. This will add a
   .bak extension to the files that couldn't be read.
8) remove the repair_db scripts from your pivot folder.

Do NOT remove the .bak files. I am working on a tool that will be a lot less 
fussy, so it will likely be able to repair at least part of these files.

------ 
