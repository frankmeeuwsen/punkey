<?php
$access =
  array(
    "stat" =>
    array(
      "newvisits" => 1, "newcount" => 0, "totalvisits" => 26183, "totalcount" => 13045, 
      "ext" =>
      array(
        "nl" => 5661, "numeric" => 4006, "com" => 2314, "net" => 305, "be" => 400, "jp" => 50, "se" => 5, "dk" => 4, "au" => 7, "it" => 8, "tw" => 4, "ca" => 11, "de" => 174, "fi" => 2, "at" => 7, "tr" => 3, "ch" => 17, "nu" => 1, "sa" => 1, "fr" => 8, "edu" => 9, "cx" => 1, "aw" => 1, "nz" => 3, "lu" => 1, "do" => 1, "pl" => 6, "gov" => 1, "uk" => 13, "ae" => 2, "org" => 4, "no" => 1, "br" => 2, "il" => 1, "gr" => 1, "hu" => 1, "uy" => 1, "ru" => 1, "us" => 2, "ee" => 1, "pt" => 1, "yu" => 1, "ar" => 1, "th" => 1
      ),
      "browser" =>
      array(
        "firefox" => 1565, "explorer" => 5146, "other" => 433, "libwww" => 1, "camino" => 54, "netscape" => 89, "mozilla" => 253, "galeon" => 38, "safari" => 322, "aol" => 28, "konqueror" => 24, "opera" => 104, "java" => 8, "netcaptor" => 10, "firebird" => 12, "Epiphany" => 3, "Elinks" => 1
      ),
      "os" =>
      array(
        "windowsxp" => 5019, "other" => 409, "windows2k" => 1066, "macosx" => 536, "linux" => 266, "windows98" => 470, "windowsme" => 140, "macppc" => 53, "palm" => 2, "windowsnt" => 99, "sun" => 1, "windows2003" => 14, "windows" => 8, "windows95" => 4, "netbsd" => 1, "freebsd" => 2, "windowsce" => 1
      ),
      "robot" =>
      array(
        "frontier" => 1, "robot" => 202, "blogpulse" => 1, "google" => 1696, "msnbot" => 574, "waypath" => 15, "yahoo" => 1773, "zyborg" => 61, "psbot" => 2, "alexa" => 28, "larbin" => 8, "zeus" => 1, "vagabondo" => 56, "gigabot" => 481, "wget" => 2, "popdex" => 4, "php" => 2, "almaden" => 5, "shareware" => 1, "blogbot" => 1, "baidu" => 33, "pythonurl" => 1, "exabot" => 4, "httpclient" => 1, "genome" => 1
      )
    ),
    "page" =>
    array(
      "Voorpagina" =>
      array(
        "count" => 55, "uri" => "/"
      ),
      "Entry pagina" =>
      array(
        "count" => 185, "uri" => "/entry.php"
      ),
      "Pivot Entry (06 - 2004)" =>
      array(
        "count" => 6835, "uri" => "/entry.php"
      ),
      "Pivot Entry (07 - 2004)" =>
      array(
        "count" => 11741, "uri" => "/entry.php"
      ),
      "Pivot Entry (08 - 2004)" =>
      array(
        "count" => 4040, "uri" => "/entry.php"
      ),
      "Pivot Entry (09 - 2004)" =>
      array(
        "count" => 3327, "uri" => "/entry.php"
      )
    ),
    "time" =>
    array(
      "last" => "1096016210", 
      "hour" =>
      array(
        2, 3, 0, 0, 11, 1, 1, 1, 11, 10, 18, 24, 9, 9, 4, 6, 34, 25, 5, 18, 3, 8, 8, 5
      ),
      "wday" =>
      array(
        283, 298, 277, 444, 240, 58, 193
      ),
      "day" =>
      array(
        600, 232, 268, 201, 149, 206, 204, 232, 194, 216, 154, 215, 97, 255, 569, 442, 201, 193, 283, 298, 277, 444, 240, 58, 141, 100, 210, 631, 249, 259, 225
      ),
      "month" =>
      array(
        0, 0, 0, 0, 55, 2912, 10358, 6630, 6228, 0, 0, 0
      )
    ),
    "last" =>
    array(
      array(
        "time" => "1095933248", "ip" => "131.211.222.91", "dns" => "91pc222.sshunet.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://weblog.fok.nl/blog/4465", "script" => "/entry.php", "script_name" => "Pivot Entry (07 - 2004)", "id" => 12946, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095933370", "ip" => "198.87.83.123", "dns" => "www.syndic8.com", "agent" => "Syndic8/1.0 (http://www.syndic8.com/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12947, "visits" => 1, "ext" => "com", "browser" => "other", "os" => "other"
      ),
      array(
        "time" => "1095933607", "ip" => "213.84.172.170", "dns" => "213-84-172-170.adsl.xs4all.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; (R1 1.5); .NET CLR 1.1.4322)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12948, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095933623", "ip" => "194.151.106.66", "dns" => "194.151.106.66", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12949, "visits" => 1, "ext" => "numeric", "browser" => "explorer", "browser_note" => "6.0", "os" => "windows2k"
      ),
      array(
        "time" => "1095933964", "ip" => "131.155.228.46", "dns" => "t-27-46.athome.tue.nl", "agent" => "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/125.5 (KHTML, like Gecko) Safari/125.9", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12951, "visits" => 1, "ext" => "nl", "browser" => "safari", "browser_note" => "125.9", "os" => "macosx"
      ),
      array(
        "time" => "1095933966", "ip" => "66.249.65.109", "dns" => "66.249.65.109", "agent" => "Mediapartners-Google/2.1", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12952, "visits" => 2, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095934726", "ip" => "62.235.229.143", "dns" => "ppp-62-235-229-143.tiscali.be", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12953, "visits" => 1, "ext" => "be", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095936936", "ip" => "213.84.172.170", "dns" => "213-84-172-170.adsl.xs4all.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.1.4322)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12954, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windows2k"
      ),
      array(
        "time" => "1095937581", "ip" => "82.136.194.186", "dns" => "82-136-194-186-mx.xdsl.tiscali.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Hotbar 4.5.1.0; .NET CLR 1.1.4322)", "referer" => "http://www.google.nl/search?hl=nl&amp;ie=UTF-8&amp;q=code+voor+gratis+credits+geen+oplichters%21&amp;lr=", "script" => "/entry.php", "script_name" => "Pivot Entry (06 - 2004)", "id" => 12955, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095937705", "ip" => "217.102.215.91", "dns" => "sv4.60195.ip.nltree.nl", "agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/0.10", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12956, "visits" => 1, "ext" => "nl", "browser" => "firefox", "browser_note" => "0.10", "os" => "windowsxp"
      ),
      array(
        "time" => "1095938845", "ip" => "213.150.147.93", "dns" => "213.150.147.93", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)", "referer" => "http://www.bloglog.nl/search.php?tanga=NSFW&amp;page=2", "script" => "/entry.php", "script_name" => "Pivot Entry (08 - 2004)", "id" => 12957, "visits" => 1, "ext" => "numeric", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095939302", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12950, "visits" => 4, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1095939402", "ip" => "62.131.44.1", "dns" => "ip3e832c01.speed.planet.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12958, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095939438", "ip" => "207.46.98.77", "dns" => "207.46.98.77", "agent" => "msnbot/0.3 (+http://search.msn.com/msnbot.htm)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (08 - 2004)", "id" => 12959, "visits" => 1, "ext" => "numeric", "robot" => "msnbot", "robot_note" => "0.3"
      ),
      array(
        "time" => "1095939651", "ip" => "66.196.90.229", "dns" => "lj1213.inktomisearch.com", "agent" => "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (06 - 2004)", "id" => 12960, "visits" => 1, "ext" => "com", "robot" => "yahoo"
      ),
      array(
        "time" => "1095940043", "ip" => "66.180.233.5", "dns" => "66.180.233.5", "agent" => "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.2.1; aggregator:Rojo; http://rojo.org/) Gecko/20021130", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12961, "visits" => 1, "ext" => "numeric", "browser" => "mozilla", "browser_note" => "1.2.1", "os" => "linux", "os_note" => "i686"
      ),
      array(
        "time" => "1095941882", "ip" => "213.84.22.41", "dns" => "visualspace.xs4all.nl", "agent" => "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.6) Gecko/20040206 Firefox/0.8", "referer" => "http://www.google.nl/search?q=moviesondemand&amp;sourceid=mozilla-search&amp;cr=countryNL&amp;lr=lang_nl&amp;start=0&amp;start=0&amp;ie=utf-8&amp;oe=utf-8", "script" => "/entry.php", "script_name" => "Pivot Entry (06 - 2004)", "id" => 12963, "visits" => 1, "ext" => "nl", "browser" => "firefox", "browser_note" => "0.8", "os" => "macosx"
      ),
      array(
        "time" => "1095942903", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12962, "visits" => 2, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1095943693", "ip" => "66.196.91.131", "dns" => "lj1351.inktomisearch.com", "agent" => "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (07 - 2004)", "id" => 12964, "visits" => 1, "ext" => "com", "robot" => "yahoo"
      ),
      array(
        "time" => "1095944706", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12965, "visits" => 1, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1095944855", "ip" => "82.101.197.37", "dns" => "82.101.197.37", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12966, "visits" => 1, "ext" => "numeric", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095945687", "ip" => "207.46.98.77", "dns" => "207.46.98.77", "agent" => "msnbot/0.3 (+http://search.msn.com/msnbot.htm)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (08 - 2004)", "id" => 12967, "visits" => 1, "ext" => "numeric", "robot" => "msnbot", "robot_note" => "0.3"
      ),
      array(
        "time" => "1095947886", "ip" => "66.196.90.61", "dns" => "lj1045.inktomisearch.com", "agent" => "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (07 - 2004)", "id" => 12970, "visits" => 1, "ext" => "com", "robot" => "yahoo"
      ),
      array(
        "time" => "1095948155", "ip" => "213.84.194.121", "dns" => "tangram.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12968, "visits" => 2, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windows2k"
      ),
      array(
        "time" => "1095949325", "ip" => "193.67.103.254", "dns" => "193.67.103.254", "agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/0.10", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12972, "visits" => 20, "ext" => "numeric", "browser" => "firefox", "browser_note" => "0.10", "os" => "windowsxp"
      ),
      array(
        "time" => "1095950103", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12969, "visits" => 3, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1095950409", "ip" => "66.196.90.72", "dns" => "lj1056.inktomisearch.com", "agent" => "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (06 - 2004)", "id" => 12974, "visits" => 1, "ext" => "com", "robot" => "yahoo"
      ),
      array(
        "time" => "1095950458", "ip" => "206.225.81.32", "dns" => "206.225.81.32", "agent" => "booch_1.0.7 tankvit@e-mail.ru", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12973, "visits" => 6, "ext" => "numeric", "browser" => "other", "os" => "other"
      ),
      array(
        "time" => "1095950690", "ip" => "66.249.65.109", "dns" => "66.249.65.109", "agent" => "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12971, "visits" => 4, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095951635", "ip" => "80.65.116.172", "dns" => "ip116-172.dsl.introweb.nl", "agent" => "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; nl-nl) AppleWebKit/125.4.2 (KHTML, like Gecko) Safari/125.9", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12975, "visits" => 1, "ext" => "nl", "browser" => "safari", "browser_note" => "125.9", "os" => "macosx"
      ),
      array(
        "time" => "1095952149", "ip" => "66.196.90.140", "dns" => "lj1124.inktomisearch.com", "agent" => "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (07 - 2004)", "id" => 12977, "visits" => 1, "ext" => "com", "robot" => "yahoo"
      ),
      array(
        "time" => "1095953705", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12976, "visits" => 2, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1095954134", "ip" => "209.237.238.178", "dns" => "crawl28-public.alexa.com", "agent" => "ia_archiver", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12978, "visits" => 20, "ext" => "com", "robot" => "alexa"
      ),
      array(
        "time" => "1095954423", "ip" => "213.84.200.128", "dns" => "onslow.xs4all.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 1.0.3705)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12979, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095955385", "ip" => "217.123.147.141", "dns" => "cp516354-a.dbsch1.nb.home.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)", "referer" => "http://www.bloglog.nl/index.php?viewblog=&amp;tijd=1092348000&amp;page=2", "script" => "/entry.php", "script_name" => "Pivot Entry (08 - 2004)", "id" => 12980, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095955929", "ip" => "66.249.65.109", "dns" => "66.249.65.109", "agent" => "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12982, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095957180", "ip" => "66.249.64.142", "dns" => "66.249.64.142", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (08 - 2004)", "id" => 12983, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095959064", "ip" => "66.249.65.109", "dns" => "66.249.65.109", "agent" => "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12984, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095959103", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12981, "visits" => 3, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1095960294", "ip" => "82.92.66.125", "dns" => "a82-92-66-125.adsl.xs4all.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12985, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095960789", "ip" => "81.173.33.202", "dns" => "81.173.33.202", "agent" => "pMachine Pro2.3 PHP/4.3.8", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12987, "visits" => 3, "ext" => "numeric", "robot" => "php", "robot_note" => "4.3.8"
      ),
      array(
        "time" => "1095960905", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12988, "visits" => 1, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1095960924", "ip" => "206.225.81.32", "dns" => "206.225.81.32", "agent" => "booch_1.0.7 tankvit@e-mail.ru", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12986, "visits" => 10, "ext" => "numeric", "browser" => "other", "os" => "other"
      ),
      array(
        "time" => "1095961886", "ip" => "66.249.65.109", "dns" => "66.249.65.109", "agent" => "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12989, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095962720", "ip" => "66.249.64.199", "dns" => "66.249.64.199", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (08 - 2004)", "id" => 12990, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095963631", "ip" => "81.70.143.51", "dns" => "dc51468f33.adsl.wanadoo.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12991, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095964888", "ip" => "62.238.32.221", "dns" => "kbl-tnz221.zeelandnet.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://www.mediafact.nl/weblog.php?id=P5418", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12992, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095966873", "ip" => "66.249.65.109", "dns" => "66.249.65.109", "agent" => "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12993, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095967354", "ip" => "67.89.107.171", "dns" => "ip67-89-107-171.z107-89-67.customer.algx.net", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://www.google.com/search?as_q=&amp;num=100&amp;hl=en&amp;ie=UTF-8&amp;btnG=Google+Search&amp;as_epq=fast+times+at+hero+high&amp;as_oq=&amp;as_eq=&amp;lr=&amp;as_ft=i&amp;as_filetype=&amp;as_qdr=all&amp;as_nlo=&amp;as_nhi=&amp;as_occt=any&amp;as_dt=i&amp;as_sitesearch=&amp;safe=images", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12994, "visits" => 1, "ext" => "net", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095967729", "ip" => "80.57.193.131", "dns" => "g193131.upc-g.chello.nl", "agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/0.10", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12995, "visits" => 1, "ext" => "nl", "browser" => "firefox", "browser_note" => "0.10", "os" => "windowsxp"
      ),
      array(
        "time" => "1095968913", "ip" => "213.224.52.70", "dns" => "d5e03446.kabel.telenet.be", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12996, "visits" => 4, "ext" => "be", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095969364", "ip" => "62.69.162.178", "dns" => "pip0-8.ilse.nl", "agent" => "Mozilla/3.0 (INGRID/3.0 MT; webcrawler@NOSPAMexperimental.net; http://aanmelden.ilse.nl/?aanmeld_mode=webhints)", "referer" => "http://www.mediafact.nl/comments.php?id=5418_0_1_0_C", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12997, "visits" => 1, "ext" => "nl", "robot" => "robot"
      ),
      array(
        "time" => "1095970523", "ip" => "207.46.98.110", "dns" => "207.46.98.110", "agent" => "msnbot/0.3 (+http://search.msn.com/msnbot.htm)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (07 - 2004)", "id" => 12999, "visits" => 1, "ext" => "numeric", "robot" => "msnbot", "robot_note" => "0.3"
      ),
      array(
        "time" => "1095971395", "ip" => "80.61.83.20", "dns" => "ip503d5314.speed.planet.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)", "referer" => "http://www.mediafact.nl/comments.php?id=5418_0_1_0_C", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 12998, "visits" => 3, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095971477", "ip" => "198.81.26.15", "dns" => "cache-ntc-aa10.proxy.aol.com", "agent" => "Mozilla/4.0 (compatible; MSIE 5.0; MSN 2.5; AOL 8.0; Windows 98; DigExt)", "referer" => "http://aolsearch.aol.com/aol/search?invocationType=bottomsearchbox.%2Faol%2Fadd.jsp&amp;query=punkey+monkey", "script" => "/entry.php", "script_name" => "Pivot Entry (07 - 2004)", "id" => 13000, "visits" => 1, "ext" => "com", "browser" => "aol", "browser_note" => "8.0", "os" => "windows98"
      ),
      array(
        "time" => "1095972684", "ip" => "80.126.233.160", "dns" => "a80-126-233-160.adsl.xs4all.nl", "agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/0.10", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13001, "visits" => 1, "ext" => "nl", "browser" => "firefox", "browser_note" => "0.10", "os" => "windowsxp"
      ),
      array(
        "time" => "1095973163", "ip" => "213.51.74.170", "dns" => "cc413210-a.deven1.ov.home.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; AtHome033)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13002, "visits" => 2, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095974190", "ip" => "66.249.66.206", "dns" => "66.249.66.206", "agent" => "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13003, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095974666", "ip" => "80.242.40.184", "dns" => "ip-80-242-40-184.aramiska-arc.aramiska.net", "agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/0.10", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13004, "visits" => 1, "ext" => "net", "browser" => "firefox", "browser_note" => "0.10", "os" => "windowsxp"
      ),
      array(
        "time" => "1095975348", "ip" => "212.123.176.188", "dns" => "212.123.176.188", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)", "referer" => "http://www.mediafact.nl/weblog.php?id=P5418", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13006, "visits" => 1, "ext" => "numeric", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095975792", "ip" => "66.249.66.169", "dns" => "66.249.66.169", "agent" => "Mediapartners-Google/2.1", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13005, "visits" => 2, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095979871", "ip" => "212.129.171.171", "dns" => "amf-kkp-2bab.mxs.adsl.euronet.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13007, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1095980022", "ip" => "66.180.233.5", "dns" => "66.180.233.5", "agent" => "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.2.1; aggregator:Rojo; http://rojo.org/) Gecko/20021130", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13008, "visits" => 1, "ext" => "numeric", "browser" => "mozilla", "browser_note" => "1.2.1", "os" => "linux", "os_note" => "i686"
      ),
      array(
        "time" => "1095982695", "ip" => "213.201.136.20", "dns" => "213.201.136.20", "agent" => "Mozilla/5.0", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13009, "visits" => 3, "ext" => "numeric", "browser" => "mozilla", "browser_note" => "5.0", "os" => "other"
      ),
      array(
        "time" => "1095993070", "ip" => "207.46.98.77", "dns" => "207.46.98.77", "agent" => "msnbot/0.3 (+http://search.msn.com/msnbot.htm)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13010, "visits" => 11, "ext" => "numeric", "robot" => "msnbot", "robot_note" => "0.3"
      ),
      array(
        "time" => "1095996610", "ip" => "66.249.64.36", "dns" => "66.249.64.36", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13011, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1095999450", "ip" => "66.196.101.90", "dns" => "fj5007.inktomisearch.com", "agent" => "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13012, "visits" => 1, "ext" => "com", "robot" => "yahoo"
      ),
      array(
        "time" => "1096004514", "ip" => "195.86.248.34", "dns" => "195.86.248.34", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)", "referer" => "http://www.mediafact.nl/weblog.php", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13013, "visits" => 1, "ext" => "numeric", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1096005607", "ip" => "24.173.210.90", "dns" => "rrcs-24-173-210-90.sw.biz.rr.com", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (07 - 2004)", "id" => 13014, "visits" => 1, "ext" => "com", "browser" => "explorer", "browser_note" => "6.0", "os" => "windows2k"
      ),
      array(
        "time" => "1096005835", "ip" => "213.10.173.1", "dns" => "ipd50aad01.speed.planet.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)", "referer" => "http://www.mediafact.nl/weblog.php?id=P5418", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13015, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1096007017", "ip" => "66.249.64.142", "dns" => "66.249.64.142", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13016, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096007421", "ip" => "66.249.64.129", "dns" => "66.249.64.129", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13017, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096008148", "ip" => "82.161.31.133", "dns" => "wolters-ict.demon.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13018, "visits" => 2, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1096008653", "ip" => "66.249.64.160", "dns" => "66.249.64.160", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13019, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096008859", "ip" => "66.249.64.204", "dns" => "66.249.64.204", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13020, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096008877", "ip" => "66.249.64.35", "dns" => "66.249.64.35", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13021, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096009000", "ip" => "66.249.64.145", "dns" => "66.249.64.145", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13022, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096009111", "ip" => "66.249.64.49", "dns" => "66.249.64.49", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13023, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096009407", "ip" => "82.74.73.245", "dns" => "cc515571-a.zwoll1.ov.home.nl", "agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040614 Firefox/0.9", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13024, "visits" => 1, "ext" => "nl", "browser" => "firefox", "browser_note" => "0.9", "os" => "windowsxp"
      ),
      array(
        "time" => "1096009480", "ip" => "213.84.172.170", "dns" => "213-84-172-170.adsl.xs4all.nl", "agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/0.10", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13025, "visits" => 1, "ext" => "nl", "browser" => "firefox", "browser_note" => "0.10", "os" => "windowsxp"
      ),
      array(
        "time" => "1096010607", "ip" => "66.249.64.16", "dns" => "66.249.64.16", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13027, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096010625", "ip" => "145.78.21.6", "dns" => "145.78.21.6", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 4.0)", "referer" => "http://www.mediafact.nl/weblog.php", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13028, "visits" => 1, "ext" => "numeric", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsnt", "os_note" => "4.0"
      ),
      array(
        "time" => "1096011699", "ip" => "66.249.64.145", "dns" => "66.249.64.145", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13029, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096011865", "ip" => "139.18.2.81", "dns" => "info015.informatik.uni-leipzig.de", "agent" => "findlinks/0.89 (+http://wortschatz.uni-leipzig.de/findlinks/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (08 - 2004)", "id" => 13031, "visits" => 1, "ext" => "de", "browser" => "other", "os" => "other"
      ),
      array(
        "time" => "1096012378", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13026, "visits" => 2, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1096012622", "ip" => "66.249.64.204", "dns" => "66.249.64.204", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13032, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096012961", "ip" => "80.115.212.3", "dns" => "wk1.60550.ip.nltree.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)", "referer" => "http://www.google.nl/search?hl=nl&amp;ie=UTF-8&amp;q=reebok+icecream&amp;btnG=Zoeken&amp;lr=", "script" => "/entry.php", "script_name" => "Pivot Entry (06 - 2004)", "id" => 13033, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windows2k"
      ),
      array(
        "time" => "1096013402", "ip" => "66.249.64.131", "dns" => "66.249.64.131", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13034, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096013690", "ip" => "194.109.237.55", "dns" => "a194-109-237-55.adsl.xs4all.nl", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FTDv3 Browser)", "referer" => "http://www.mediafact.nl/weblog.php?id=P5418", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13035, "visits" => 1, "ext" => "nl", "browser" => "explorer", "browser_note" => "6.0", "os" => "windowsxp"
      ),
      array(
        "time" => "1096013699", "ip" => "66.249.64.50", "dns" => "66.249.64.50", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13030, "visits" => 3, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096014058", "ip" => "66.249.64.199", "dns" => "66.249.64.199", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13036, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096014545", "ip" => "66.249.64.38", "dns" => "66.249.64.38", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13039, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096014815", "ip" => "66.249.64.142", "dns" => "66.249.64.142", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13040, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096015195", "ip" => "66.249.64.135", "dns" => "66.249.64.135", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13037, "visits" => 2, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096015323", "ip" => "66.249.64.131", "dns" => "66.249.64.131", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13041, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096015335", "ip" => "66.249.64.16", "dns" => "66.249.64.16", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (06 - 2004)", "id" => 13042, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096015460", "ip" => "194.151.106.66", "dns" => "194.151.106.66", "agent" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13044, "visits" => 1, "ext" => "numeric", "browser" => "explorer", "browser_note" => "6.0", "os" => "windows2k"
      ),
      array(
        "time" => "1096015751", "ip" => "66.249.64.79", "dns" => "66.249.64.79", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13045, "visits" => 1, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      ),
      array(
        "time" => "1096015982", "ip" => "80.127.168.236", "dns" => "maildrop01vda.xs4all.nl", "agent" => "NetNewsWire/1.0.8 (Mac OS X; http://ranchero.com/netnewswire/)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13038, "visits" => 2, "ext" => "nl", "browser" => "other", "os" => "macosx"
      ),
      array(
        "time" => "1096016210", "ip" => "66.249.64.55", "dns" => "66.249.64.55", "agent" => "Googlebot/2.1 (+http://www.google.com/bot.html)", "referer" => "http://www.punkey.com/", "script" => "/entry.php", "script_name" => "Pivot Entry (09 - 2004)", "id" => 13043, "visits" => 2, "ext" => "numeric", "robot" => "google", "robot_note" => "2.1"
      )
    ),
    "referer" =>
    array(
      "www.logt.cc/" => 722, "blo.gs/" => 1, "rcs.datashed.net/" => 2, "mail.rhinofly.nl/exchange/frank/Postvak%20IN/" => 1, "www.google.es/" => 1, "www.google.nl/" => 148, "uk.f139.mail.yahoo.com/ym/" => 1, "webmail.ladot.com/horde/imp/" => 1, "webmail2.ashosting.nl/modules/email/" => 1, "www.thenotsodailycrap.nl/" => 9, "64.4.26.250/cgi-bin/" => 1, "us.f409.mail.yahoo.com/ym/" => 1, "www.nedstatbasic.net/" => 8, "www.spunk.nl/articles/" => 1, "www.tikky.nl/" => 2, "www.google.it/" => 3, "www.punkey.com/" => 8863, "www.rg-koege.dk/" => 1, "www.technorati.com/cosmos/" => 2, "www.news-update.co.uk/" => 2, "www.alhetnieuws.nl/nieuws/" => 7, "blokje.free.fr/" => 10, "www.worldlingo.com/wl/" => 1, "www.bloglines.com/" => 108, "search.sympatico.msn.ca/pass/" => 1, "www.altavista.com/web/" => 4, "www.babygrandpa.com/" => 7, "search.msn.nl/" => 20, "www.bloglog.nl/" => 684, "www.google.ro/" => 4, "bgcounter.com/" => 1, "www.bloglog.nl/sidebar/" => 3, "search.yahoo.com/" => 3, "www.google.be/" => 18, "www.bloglines.com/blog/" => 5, "blog.bitflux.ch/" => 1, "www.google.com/" => 61, "www.google.se/" => 3, "zoek.lycos.nl/cgi-bin/" => 2, "www.aboutblank.nl/" => 47, "www.aboutblank.nl/pivot/" => 12, "www.gwx.nl/" => 29, "www.beuntje.com/" => 10, "www.nederflash.nl/blog/archives/2004/07/13/" => 17, "weblog.fok.nl/blog/" => 325, "www.morg.nl/" => 25, "www.nederflash.nl/blog/" => 1, "weblog.fok.nl/" => 356, "www.google.com.tr/" => 1, "nl.altavista.com/web/" => 3, "gathering.tweakers.net/forum/list_message/" => 1, "thomper.blogspot.com/2004/07/" => 8, "rss.xiffy.nl/" => 24, "www.gracioso.net/cms/" => 5, "www.upsalt.com/" => 12, "www.blogsnow.com/t/" => 11, "xiffy.nl/" => 16, "www.jereinsteonzin.nl/" => 55, "weblog.fok.nl/blog/cat/" => 17, "www.schaap.tv/" => 41, "www.schaap.tv/archive/2004/07/13/" => 3, "xiffy.nl/weblog/" => 3, "www.dictatuur.nl/" => 21, "www.de-leau.com/" => 5, "nederflash.nl/" => 5, "cinner.com/" => 58, "www.popdex.com/" => 4, "cinner.com/weblog/archive/2004/07/14/" => 7, "web.mencia.nl/leko/blog/journal/" => 11, "www.davidrietveld.nl/" => 19, "blokje.blogspot.com/" => 6, "ergernis.blogspot.com/" => 9, "www.hornstra.com/" => 112, "www.hornstra.com/archive/2004/07/14/" => 7, "www.vanduijn.com/" => 14, "thomper.blogspot.com/" => 2, "www.weblog.miwian.nl/" => 15, "www.weblog.miwian.nl/pivot/" => 2, "www.blogsnow.com/" => 2, "www.davidrietveld.nl/pvt/" => 4, "braintags.com/" => 8, "braintags.com/archives/2004/07/22/" => 1, "www.ferket.com/jvlt/" => 13, "www.ferket.com/" => 6, "www.cinner.com/weblog/archives/" => 4, "counter19.bravenet.com/" => 1, "www.mediafact.nl/" => 25, "www.chantalcoolsma.nl/" => 6, "www.xs4all.nl/~goshogun/kurai/" => 1, "www.beuntje.com/weblog/" => 2, "www.denniz.net/" => 3, "fuckhedz.com/" => 17, "fuckhedz.com/item/" => 8, "www.bareuh.nl/" => 20, "www.sepi.be/" => 5, "www.daypop.com/" => 4, "www.phact.nl/" => 7, "www.jeroensangers.com/" => 1, "www.onverklaarbaarbewoond.nl/" => 4, "www.highfly.be/" => 25, "www.onverklaarbaarbewoond.nl/pivot/" => 1, "www.kurtminnen.be/" => 18, "www.kurtminnen.be/tafelzoetstof/" => 2, "www.wimpie.net/" => 14, "www.highfly.be/blog/pivotnew/" => 1, "mohrn.mista.be/archives/" => 2, "www.cabanier.be/" => 12, "home.planet.nl/" => 10, "www.rozig.com/" => 42, "www.rozig.com/pivot/" => 3, "home.planet.nl/~aikem005/" => 4, "kamikaatje.web-log.nl/" => 27, "mohrn.mista.be/" => 12, "www.fromfrats.com/tftc/" => 26, "www.fromfrats.com/tftc/archives/" => 3, "mohrn.mista.be/pivot/" => 1, "tomadde.web-log.nl/" => 2, "www.zeekoeienzijngeweldig.nl/" => 5, "www.jeroensangers.com/archives/2004/07/" => 2, "brechtjesblogje.blogspot.com/" => 10, "www.prosperousinternet.com/" => 1, "joliesworld.blogspot.com/" => 12, "homepage.mac.com/didotcicero/iblog/C18150098/E1570090663/" => 2, "homepage.mac.com/didotcicero/" => 11, "users.skynet.be/chipsandcookies/" => 2, "users.skynet.be/" => 8, "weblogs.bnn.nl/action/" => 1, "www.artificialwomen.com/" => 9, "brechtjesblogje.blogspot.com/2004/07/" => 2, "braintags.com/archives/2004/07/" => 2, "weblogs.bnn.nl/" => 10, "kroon.web-log.nl/" => 8, "www.scrapedfeeds.com/" => 10, "weblog.plasticthinking.org/pphlogger/" => 1, "www.annefloor.nl/pivot/" => 1, "weblog.plasticthinking.org/" => 21, "weblog.plasticthinking.org/item/2004/7/20/" => 11, "weblog.plasticthinking.org/category/" => 1, "weblogs.bnn.nl/ceecee/" => 2, "weblog.plasticthinking.org/item/" => 3, "axmo.com/" => 5, "www.wimpie.net/pivot/" => 1, "www.alhetnieuws.nl/" => 1, "www.smintjes.be/pivot/" => 9, "www.jnloco.be/" => 14, "www.google.com.vn/" => 5, "www.smintjes.be/" => 2, "mystack.com/" => 1, "www.alhetnieuws.nl/nieuws/171/" => 1, "sidebar.bloglog.nl/" => 2, "www.ferket.com/jvlt/pivot/" => 1, "mail.tangoe.com/exchange/forms/IPM/NOTE/" => 1, "www.google.ca/" => 3, "www.zeekoeienzijngeweldig.nl/archief/" => 1, "www.google.co.uk/" => 3, "dutch.hopto.org/" => 5, "weblog.plasticthinking.org/blog/" => 1, "www.feedster.com/" => 1, "www.weblogzonderhaast.nl/" => 2, "www.google.com.br/" => 5, "www.izito.nl/search/" => 1, "www.alexgray.com:8383/Xa6ac999998cccf989a9a94ba5195/" => 1, "search.xtramsn.co.nz/" => 2, "thedavidlawrenceshow.com/" => 6, "www.ohjawel.nl/" => 2, "ad-rag.com/" => 36, "www.google.co.jp/" => 5, "www.schaap.tv/archives/" => 3, "www.phact.nl/kek/archives/" => 1, "www.nieuwnieuws.com/feeder/" => 4, "weblog.plasticthinking.org/archive/1/" => 2, "www.nieuwnieuws.com/" => 270, "search-dyn.tiscali.nl/" => 1, "weblog.plasticthinking.org/item/2004/7/10/" => 2, "fuckhedz.com/archive/1/" => 1, "www.google.co.th/" => 2, "www.star-cash.de/cgi-bin/board/" => 1, "s4.statcounter.com/project/standard/" => 1, "www.housekamp.nl/" => 1, "www.lukol.com:2007/" => 1, "www.search.com/" => 1, "search.msn.be/" => 1, "development.bloglog.nl/" => 9, "www.badlog.nl/" => 9, "dani.zenidhost.net/" => 2, "rss.wurfl.com/" => 2, "www.zeekoeienzijngeweldig.nl/bbclone/" => 1, "www.cnxsite.be/" => 1, "mobile.bloglog.nl/" => 2, "82.94.15.142:5335/system/pages/" => 1, "www.djiezus.com/" => 4, "www.oorgasme.nl/" => 2, "www.ppheadlines.be/pph/" => 2, "www.badlog.be/" => 2, "zoeken.wanadoo.nl/" => 1, "loglands.nl/" => 1, "bluebirdanimatie.jouwpagina.nl/" => 3, "aolsearch.aol.com/aol/" => 13, "loglands.nl/pivot/includes/" => 1, "suche.fireball.de/cgi-bin/" => 1, "www.86866.net/bbs/" => 1, "www.woot.com/forum/" => 9, "www.gwx.nl/pivot/" => 1, "www.lognest.nl/" => 1, "search.msn.co.jp/" => 1, "www.devilfinder.com/" => 1, "uk.search.yahoo.com/search/" => 1, "www.weblogzonderhaast.nl/index.php/" => 3, "www.cabanier.be/blog/pivot/" => 1, "www.hornstra.com/archives/" => 1, "www.stealth.nl/usr/" => 1, "www.badlog.nl/pivot/" => 5, "www.onderhuids.com/" => 2, "www.onderhuids.com/comments/" => 4, "search.msn.com/" => 2, "www.sitemeter.com/" => 2, "172.31.110.10:15871/cgi-bin/" => 1, "www.google.nl./" => 1, "www.nadinek.de/" => 1, "www.mywebsearch.com/jsp/" => 2, "search.msn.nl/pass/" => 1, "www.mephio.nl/" => 1, "65.54.184.250/cgi-bin/" => 2, "www.wimpie.net/archives/" => 1, "s8.ixquick.com/do/" => 1, "www.punkblood.com/" => 1, "www.google.pl/" => 1, "www.icq.com/search/" => 1, "morg.nl/archief/" => 1, "www.google.at/" => 2, "a9.com/" => 1, "www.rickdekikker.com/" => 1, "www.google.com.mx/" => 1, "www.dornhege.com/" => 1, "www.rhinofly.nl/frank-ly/" => 2, "www.feedburner.com/fb/a/" => 1, "homepage.mac.com/huismans/" => 2, "www.punkey.comhttp:/www.punkey.com/pivot/" => 1, "www.lhotka.net/WeBlog/" => 1
    )
  );
?>