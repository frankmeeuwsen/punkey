<?php
/* File:  show_detailed.php
 * Summary: Display the detailed stats of BBClone
 * 
 * Description:
 * 
 * License:
 * 
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/show_global.php,v 1.50 2004/01/18 11:18:02 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

if (is_readable("constants.php")) require_once("constants.php");
else return;
$j = array($BBC_CONFIG_FILE, $BBC_LIB_PATH."selectlang.php", $BBC_LIB_PATH."os.php", $BBC_LIB_PATH."robot.php",
           $BBC_LIB_PATH."browser.php", $BBC_LIB_PATH."extension.php", $BBC_LIB_PATH."html.php", $BBC_ACCESS_FILE);
for ($i = 0, $k = count($j); $i < $k; $i++) {
  if (is_readable($j[$i])) require_once($j[$i]);
  else return err_msg($j[$i]);
}

// Auxilliary Functions

// cumulative hits in different periods of time
function histcalc($array) {
  $result = 0;

  if (is_array($array)) {
    for ($i = 0; $i < count($array); $i++) $result += $array[$i];
  }
  return $result;
}

// This is used in the show_top_pages to class page with usort().
function sort_page_count($page_a, $page_b) {
  if ($page_a["count"] === $page_b["count"]) return 0;
  return ($page_a["count"] > $page_b["count"]) ? -1 : 1;
}

// Referer link generation
function refgen($ref) {
global $_;

  if ($ref == "not_specified") $str = "<i>". $_["gstat_Not_specified"]."</i>";
  else {
    $ref = (strpos($ref, "://") !== false) ? substr(strstr($ref, "://"), 3) : $ref;
    $ref_name = (($slash = strpos($ref, "/")) !== false) ? substr($ref, 0, $slash) : $ref;
    $str = "<a href=\"http://$ref\">$ref_name</a>";
  }
  return $str;
}

// Summary's header
function rank_head($cat, $i18n, $flag = 0) {
  global $_;

  return "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\">\n"
        ."<tr>\n"
        ."<td colspan=\"".(!empty($flag) ? 3 : 4)."\" align=\"center\">\n"
        ."<b>".sprintf($_[$i18n], $cat)."</b>\n"
        ."</td>\n"
        ."</tr>\n";
}

function list_item($icon, $item, $item_score, $total_score) {
  global $BBC_IMAGES_PATH;

  return "<tr>\n"
        .(!empty($icon) ?
         "<td>\n<img src=\"".$BBC_IMAGES_PATH.$icon."\" height=\"14\" width=\"14\" alt=\"".$item."\" title=\""
        .$item."\">\n</td>\n" : "")
        ."<td>\n"
        .$item."&nbsp;\n"
        ."</td>\n"
        ."<td>\n"
        ."<div align=\"right\">\n"
        .$item_score."&nbsp;\n"
        ."</div>\n"
        ."</td>\n"
        ."<td>\n"
        ."<div align=\"right\">\n"
        .(sprintf("%.2f%%", (round(10000 * $item_score / $total_score) / 100)))."\n"
        ."</div>\n"
        ."</td>\n"
        ."</tr>\n";
  }

// Summary for rankings
function rank_sum($cat, $flag = 0) {
  global $_;

  return "<tr>\n"
        .(!empty($flag) ? "" : "<td>\n</td>\n")
        ."<td>\n"
        ."<b>".$_["gstat_Total"]."</b>\n"
        ."</td>\n"
        ."<td>\n"
        ."<b>$cat&nbsp;</b>\n"
        ."</td>\n"
        ."</tr>\n"
        ."</table>\n";
}

// Main Functions

function show_access() {
  global $_, $access;

  $text = "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\">\n"
         ."<tr><td colspan=\"2\" align=\"center\">\n"
         ."<b>".$_["gstat_Accesses"]."</b>\n"
         ."</td></tr>\n"
         ."<tr><td>\n"
         ."<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\">\n"."<tr>\n"
         ."<td>\n"
         .$_["gstat_Total_visits"]."&nbsp;&nbsp;\n"
         ."</td>\n"
         ."<td align=\"left\">\n<div align=\"right\">\n"
         .(!empty($access["stat"]["totalvisits"]) ? $access["stat"]["totalvisits"]."\n" : "0\n")
         ."</div>\n"
         ."</td></tr>\n<tr><td>\n".$_["gstat_Total_unique"]."&nbsp;&nbsp;\n"
         ."</td>\n"
         ."<td align=\"left\">\n<div align=\"right\">\n"
         .(!empty($access["stat"]["totalcount"]) ? $access["stat"]["totalcount"]."\n" : "0\n")
         ."</div>\n"
         ."</td></tr>\n<tr><td>\n".$_["gstat_New_visits"]."&nbsp;&nbsp;\n"
         ."</td>\n"
         ."<td align=\"left\">\n<div align=\"right\">\n"
         .(!empty($access["stat"]["newvisits"]) ? $access["stat"]["newvisits"]."\n" : "0\n")
         ."</div>\n"
         ."</td></tr>\n<tr><td>\n".$_["gstat_New_unique"]."&nbsp;&nbsp;\n"
         ."</td>\n"
         ."<td align=\"left\">\n<div align=\"right\">\n"
         .(!empty($access["stat"]["newcount"]) ? $access["stat"]["newcount"]."\n" : "0\n")
         ."</div>\n"
         ."</td>\n"
         ."</tr>\n";

  if (!empty($access["stat"]["blacklisted"])) {
    $text .= "<tr>\n"
            ."<td>\n"
            .$_["gstat_Blacklisted"]."&nbsp;&nbsp;\n"
            ."</td>\n"
            ."<td align=\"left\">\n<div align=\"right\">\n"
            .$access["stat"]["blacklisted"]."\n"
            ."</div>\n"
            ."</td>\n"
            ."</tr>\n";
  }
  $text .= "<tr><td>&nbsp;"
          ."</td></tr>\n<tr><td>\n".$_["tstat_Last_year"]."&nbsp;&nbsp;\n"
          ."</td>\n"
          ."<td align=\"left\">\n<div align=\"right\">\n"
          .(!empty($access["time"]["month"]) ? histcalc($access["time"]["month"])."\n" : "0\n")
          ."</div>\n"
          ."</td></tr>\n<tr><td>\n".$_["tstat_Last_month"]."&nbsp;&nbsp;\n"
          ."</td>\n"
          ."<td align=\"left\">\n<div align=\"right\">\n"
          .((!empty($access["time"]["month"])) ? histcalc($access["time"]["day"])."\n" : "0\n")
          ."</div>\n"
          ."</td></tr>\n<tr><td>\n".$_["tstat_Last_week"]."&nbsp;&nbsp;\n"
          ."</td>\n"
          ."<td align=\"left\">\n<div align=\"right\">\n"
          .(!empty($access["time"]["wday"]) ? histcalc($access["time"]["wday"])."\n" : "0\n")
          ."</div>\n"
          ."</td></tr>\n<tr><td>\n".$_["tstat_Last_day"]."&nbsp;&nbsp;\n"
          ."</td>\n"
          ."<td align=\"left\">\n<div align=\"right\">\n"
          .(!empty($access["time"]["wday"]) ? histcalc($access["time"]["hour"])."\n" : "0\n")
          ."</div></td></tr>\n"
          ."</table>\n"
          ."</td>\n"
          ."</tr>\n"
          ."</table>\n";
  return $text;
}

function show_os() {
  global $_, $access, $BBC_IMAGES_PATH, $BBC_MAXOS, $os;

  $os_tab = isset($access["stat"]["os"]) ? $access["stat"]["os"] : array();

  for ($os_total = 0; list(, $os_score) = each($os_tab); $os_total += $os_score);
  
  arsort($os_tab);
  reset($os_tab);
  
  $text = rank_head($BBC_MAXOS, "gstat_Operating_systems");

  for ($k = 0; (list($os_type, $os_score) = each($os_tab)) && ($k < $BBC_MAXOS); $k++) {
    $os[$os_type]["title"] = str_replace("other", $_["misc_other"], $os[$os_type]["title"]);

    $text .= list_item($os[$os_type]["icon"], $os[$os_type]["title"], $os_score, $os_total);
  }

  $text .= rank_sum($os_total);
  return $text;
}

function show_browser() {
  global $_, $access, $BBC_IMAGES_PATH, $BBC_MAXBROWSER, $browser;

  $browser_tab = isset($access["stat"]["browser"]) ? $access["stat"]["browser"] : array();

  for ($browser_total = 0; list(, $browser_score) = each($browser_tab); $browser_total += $browser_score);

  arsort($browser_tab);
  reset($browser_tab);

  $text = rank_head($BBC_MAXBROWSER, "gstat_Browsers");

  for ($k = 0; (list($browser_type, $browser_score) = each($browser_tab)) && ($k < $BBC_MAXBROWSER); $k++) {
    $browser[$browser_type]["title"] = str_replace("other", $_["misc_other"], $browser[$browser_type]["title"]);

    $text.= list_item($browser[$browser_type]["icon"], $browser[$browser_type]["title"], $browser_score,
            $browser_total);
  }

  $text .= rank_sum($browser_total);
  return $text;
}

function show_extension() {
  global $_, $access, $BBC_IMAGES_PATH, $BBC_MAXEXTENSION, $extension;

  $ext_tab = isset($access["stat"]["ext"]) ? $access["stat"]["ext"] : array();

  for ($ext_total = 0; list(, $ext_score) = each($ext_tab); $ext_total += $ext_score);

  arsort($ext_tab);
  reset($ext_tab);

  $text = rank_head($BBC_MAXEXTENSION, "gstat_n_first_extensions");

  for ($k = 0; (list($ext, $ext_score) = each($ext_tab)) && ($k < $BBC_MAXEXTENSION); $k++) {
    $text .= list_item("ext_".$ext.".png", $extension[$ext], $ext_score, $ext_total);
  }

  $text .= rank_sum($ext_total);
  return $text;
}

function show_robot() {
  global $_, $access, $BBC_IMAGES_PATH, $BBC_MAXROBOT, $robot;

  $robot_tab = isset($access["stat"]["robot"]) ? $access["stat"]["robot"] : array();

  for ($robot_total = 0; list(,$robot_score) = each($robot_tab); $robot_total += $robot_score);

  arsort($robot_tab);
  reset($robot_tab);

  $text = rank_head($BBC_MAXROBOT, "gstat_Robots");

  for ($k = 0; (list($robot_type, $robot_score) = each($robot_tab)) && ($k < $BBC_MAXROBOT); $k++) {
    $text .= list_item($robot[$robot_type]["icon"], $robot[$robot_type]["title"], $robot_score, $robot_total);
  }

  $text .= rank_sum($robot_total);
  return $text;
}

function show_top_pages() {
  global $_, $access, $BBC_MAXPAGE;

  $page_tab = isset($access["page"]) ? $access["page"] : array();

  for ($page_total = 0; list(, $page_elem) = each($page_tab); $page_total += $page_elem["count"]);

  uasort($page_tab, "sort_page_count");
  reset($page_tab);

  $text = rank_head($BBC_MAXPAGE, "gstat_n_first_pages", 1);

  for ($k = 0; (list($page_name, $page_elem) = each($page_tab)) && ($k < $BBC_MAXPAGE); $k++) {
    $page_name = ($page_name == "index") ? $_["navbar_Main_Site"] : $page_name;

    $text .= list_item("", "<a href=\"".$page_elem["uri"]."\">$page_name</a>", $page_elem["count"], $page_total);
  }

  $text .= rank_sum($page_total, 1);
  return $text;
}

function show_top_origins() {
  global $_, $access, $BBC_MAXORIGIN;

  $referer_tab = isset($access["referer"]) ? $access["referer"] : array();

  for ($referer_total = 0; list(, $referer_score) = each($referer_tab); $referer_total += $referer_score);

  arsort($referer_tab);
  reset($referer_tab);

  $text = rank_head($BBC_MAXORIGIN, "gstat_n_first_origins", 1);

  for ($k = 0; ($k < $BBC_MAXORIGIN) && (list($referer_name, $referer_score) = each($referer_tab)); $k++) {
    $text .= list_item("", refgen($referer_name), $referer_score, $referer_total);
  }

  $text .= rank_sum($referer_total, 1);
  return $text;
}

// MAIN

echo bbc_html_document_begin()
    .bbc_topbar()

    ."<div align=\"$BBC_GENERAL_ALIGN_STYLE\">\n"
    ."<table border=\"0\" cellpadding=\"20\" cellspacing=\"0\">\n"
    ."<tr>\n"
    ."<td valign=\"top\">\n"
    .show_robot()
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .show_os()
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .show_browser()
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .show_extension()
    ."</td>\n"
    ."</tr>\n"
    ."</table>\n"
    ."<table border=\"0\" cellpadding=\"20\" cellspacing=\"0\">\n"
    ."<tr>\n"
    ."<td valign=\"top\">\n"
    .show_access()
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .show_top_pages()
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .show_top_origins()
    ."</td>\n"
    ."</tr>\n"
    ."</table>\n"
    ."</div>\n"
    .bbc_copyright()
    ."<br />\n"
    .bbc_topbar(0, 1)
    .bbc_html_document_end();
?>