<?php
/* File:  show_detailed.php
 * Summary:    Display the detailed stats of bbclone
 * 
 * Description:
 * 
 * License:
 * 
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/show_detailed.php,v 1.42 2004/02/15 19:39:12 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

if (is_readable("constants.php")) require_once("constants.php");
else return;
$j = array($BBC_CONFIG_FILE, $BBC_LIB_PATH."selectlang.php", $BBC_LIB_PATH."os.php", $BBC_LIB_PATH."robot.php",
           $BBC_LIB_PATH."browser.php", $BBC_LIB_PATH."extension.php", $BBC_LIB_PATH."html.php", $BBC_ACCESS_FILE);
for ($i = 0, $k = count($j); $i < $k; $i++) {
  if (is_readable($j[$i])) require_once($j[$i]);
  else return err_msg($j[$i]);
}

// Functions

$fields = explode(",", str_replace(" ", "", $BBC_DETAILED_STAT_FIELDS));
$nb_fields = sizeof($fields);
$fields_title = array("id" => $_["dstat_ID"], "time" => $_["dstat_Time"], "visits" => $_["dstat_Visits"],
                "ext" => $_["dstat_Extension"], "dns" => $_["dstat_DNS"], "ip" => "IP", "referer" => $_["dstat_From"],
                "os" => $_["dstat_OS"], "browser" => $_["dstat_Browser"], "script_name" => "Last Page");

function show_connect_field($connect, $field) {

  global $BBC_IMAGES_PATH, $extension, $os, $browser, $robot, $_;

  switch ($field) {

    case "id":
      $id = $connect[$field];
      return "<div align=\"right\">"."$id\n"."</div>";
      break;

    case "time":
      $time = str_replace(" ", "&nbsp;", date("j M, H:i:s", $connect["time"]));
      return "<div align=\"right\">"."$time\n"."</div>";
      break;

    case "visits":
      $visits = $connect[$field];
      return "<div align=\"right\">"."$visits\n"."</div>";
      break;

    case "ext":
      $ext = $connect["ext"];
      return "<img src=\"".$BBC_IMAGES_PATH."ext_".$connect["ext"].".png\" height=\"14\" width=\"14\" alt=\""
      .$extension[$ext]."\" title=\"".$extension[$ext]."\">&nbsp;".$extension[$ext];
      break;

    case "dns":
      $str = $connect["dns"];
      if (strlen($str) > 60) $str = "...".substr($str, -60);
      return $str;
      break;

    case "referer":

      $str = (strpos($connect["referer"], "://") !== false) ? substr(strstr($connect["referer"], "://"), 3) : "unknown";
      $str = (($slash = strpos($str, "/")) !== false) ? substr($str, 0, $slash) : $str;

      if (trim($str) != "unknown") {
        if ((strlen($str)) > 60) $str = "...".substr($str, -60);
        return "<a href=\"".$connect["referer"]."\">$str</a>";
      }
      else $str = "&nbsp;";
      return $str;
      break;

    case "browser":
      if (empty($connect["robot"])) {
        $browser_type = $connect["browser"];
        $browser[$browser_type]["title"] = str_replace("other", $_["misc_other"], $browser[$browser_type]["title"]);
        $text = "<img src=\"".$BBC_IMAGES_PATH.$browser[$browser_type]["icon"]
               ."\" height=\"14\" width=\"14\" alt=\"".$browser[$browser_type]["title"]
               ."\" title=\"".$browser[$browser_type]["title"]
               ."\">&nbsp;".str_replace(" ", "&nbsp;", $browser[$browser_type]["title"]);
        if (!empty($connect["browser_note"])) $text .= "&nbsp;".$connect["browser_note"];
        return $text;
      }
      else return show_connect_field($connect, "robot");
      break;

    case "os":
      if (empty($connect["robot"])) {
        $os_type = $connect["os"];
        $os[$os_type]["title"] = str_replace("other", $_["misc_other"], $os[$os_type]["title"]);
        $text = "<img src=\"".$BBC_IMAGES_PATH.$os[$os_type]["icon"]
               ."\" height=\"14\" width=\"14\" alt=\"".$os[$os_type]["title"]."\" title=\"".$os[$os_type]["title"]
               ."\">&nbsp;".str_replace(" ", "&nbsp;", $os[$os_type]["title"]);
        if (!empty($connect["os_note"])) $text .= "&nbsp;".$connect["os_note"];
        return $text;
      }
      else return show_connect_field($connect,"robot");
      break;

    case "robot":
      $robot_type = $connect["robot"];
      $text = "<img src=\"". $BBC_IMAGES_PATH.$robot[$robot_type]["icon"]
             ."\" height=\"14\" width=\"14\" alt=\"".$robot[$robot_type]["title"]."\" title=\"".$robot[$robot_type]["title"]
             ."\">&nbsp;".str_replace(" ", "&nbsp;", $robot[$robot_type]["title"]);
      if (!empty($connect["robot_note"])) $text .= "&nbsp;".$connect["robot_note"];
      return $text;
      break;
    // add "script_name" to $BBC_DETAILED_STAT_FIELDS if you want see the last visited page in detailed stats
    case "script_name":
      $title = ($connect["script_name"] == "index") ? $_["navbar_Main_Site"] : $connect["script_name"];

      if ((strlen($title)) > 60) $str = "...".substr($title, -60);
      return "<a href=\"".$connect["script"]."\">$title</a>";
      break;

    default:
      return $connect[$field];
  }
}

// Determine the color of the connection inside the detailed stats table
function bbc_connect_code_color($connect) {
  global $BBC_MAXTIME;

  if ((time() - $connect["time"]) < $BBC_MAXTIME) return "#f6f9f2";
  // else, it is red if it is a robot
  elseif (!empty($connect["robot"])) return "#f9e8e8";
  // or blue if something else
  else return "#f4f7f9";
}

// MAIN

echo bbc_html_document_begin()
    .bbc_topbar()

    ."<p align=\"$BBC_GENERAL_ALIGN_STYLE\"><i>\n";

echo $_["dstat_Visible_accesses"].": $BBC_MAXVISIBLE,\n"
    ."<span style=\"color:green\">".$_["dstat_green_rows"]
    .":</span> ".$_["dstat_last_visit"]." < $BBC_MAXTIME ".$_["misc_second_unit"].",\n"
    ."<span style=\"color:blue\">".$_["dstat_blue_rows"]
    .":</span> ". $_["dstat_last_visit"]." > $BBC_MAXTIME ".$_["misc_second_unit"].",\n"
    ."<span style=\"color:red\">".$_["dstat_red_rows"].":</span> ".$_["dstat_robots"].".\n"
    ."</i></p>\n"
    ."<div align=\"$BBC_GENERAL_ALIGN_STYLE\">\n"
    ."<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" width=\"100%\"><tr>\n";

for ($l = 0; $l < $nb_fields; $l++) echo "<td><b>".$fields_title[$fields[$l]]."</b></td>\n";

$nb_access = isset($access["last"]) ? count($access["last"]) : 0;

for ($k = $nb_access - 1; $k >= max(0, $nb_access - $BBC_MAXVISIBLE); $k--) {
  echo "<tr bgcolor=\"".bbc_connect_code_color($access["last"][$k])."\">\n";
  for($l = 0; $l < $nb_fields; $l++) {
    $cell = show_connect_field($access["last"][$k], $fields[$l]);
    echo "<td>".(empty($cell) ? "&nbsp;" : $cell)."</td>\n";
  }
  echo "</tr>\n";
}

echo "</table>\n"
    ."</div>\n"

    .bbc_copyright()
    ."<br />\n"
    .bbc_topbar(0, 1)
    .bbc_html_document_end();
?>