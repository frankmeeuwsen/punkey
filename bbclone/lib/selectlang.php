<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/selectlang.php,v 1.27 2004/02/15 19:39:13 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* File: selectlang.php
 * Summary: Determine the more appropriated language according to
 *  the browser setting, the selected query, by modifying
 *  $BBC_LANGUAGE
 * Description:
 * Prerequired: conf/config.php
 */

// The code language table
if (is_readable($BBC_LIB_PATH."codelang.php")) require_once($BBC_LIB_PATH."codelang.php");
else die(err_msg($BBC_LIB_PATH."codelang.php"));

// if the language comes from a query, we will use it. Else we look for the preferred one of the
// browser. Should this fail too we will use the default specified in config.php.
if ((isset($HTTP_GET_VARS["lng"])) && (is_string($HTTP_GET_VARS["lng"]))) $tmp = $HTTP_GET_VARS["lng"];
elseif ((isset($HTTP_POST_VARS["lng"])) && (is_string($HTTP_POST_VARS["lng"]))) $tmp = $HTTP_POST_VARS["lng"];
elseif ((!empty($HTTP_SERVER_VARS["HTTP_ACCEPT_LANGUAGE"])) && (is_string($HTTP_SERVER_VARS["HTTP_ACCEPT_LANGUAGE"]))) {
  $tmp = $HTTP_SERVER_VARS["HTTP_ACCEPT_LANGUAGE"];

  if (($comma = strpos($tmp, ",")) !== false) $tmp = substr($tmp, 0, $comma);
  if ((($dash = strpos($tmp, "-")) !== false) && (!isset($lang_tab[$tmp]))) $tmp = substr($tmp, 0, $dash);
}
else {
  if (isset($lng)) unset($lng);
  $tmp = $BBC_LANGUAGE;
}

if (isset($lang_tab[$tmp])) {
  $lng = $tmp;
  $BBC_LANGUAGE = $lng;
}
else $lng = $BBC_LANGUAGE;

if (is_readable($BBC_LANGUAGE_PATH.$BBC_LANGUAGE.".php")) require_once($BBC_LANGUAGE_PATH.$BBC_LANGUAGE.".php");
elseif (is_readable($BBC_LANGUAGE_PATH."en.php")) {
  err_msg($BBC_LANGUAGE_PATH.$BBC_LANGUAGE.".php");
  require_once($BBC_LANGUAGE_PATH."en.php");
}
else die(err_msg($BBC_LANGUAGE_PATH."en.php"));
?>