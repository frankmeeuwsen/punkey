<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/robot.php,v 1.56 2004/02/17 21:58:00 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:    robot.php
 * Summary: contain the list of all possible robots and also different rules to
 *          classify them.
 */

$robot = array(
  "acoon" => array(
    "icon" => "robot_robot.png",
    "title" => "Acoon Robot",
    "rule" => array(
      "Acoon[ \-]*Robot" => ""
    )
  ),
  "alexa" => array(
    "icon" => "robot_alexa.png",
    "title" => "Alexa",
    "rule" => array(
      "ia_archive" => ""
    )
  ),
  "almaden" => array(
    "icon" => "robot_robot.png",
    "title" => "IBM Crawler",
    "rule" => array(
      "www\.almaden\.ibm\.com/cs/crawler" => ""
    )
  ),
  "altavista" => array(
    "icon" => "robot_altavista.png",
    "title" => "Altavista",
    "rule" => array(
      "Scooter[ /\-]*[a-z]*([0-9.]+)" => "\\1",
    )
  ),
  "amphetameme" => array(
    "icon" => "robot_robot.png",
    "title" => "Amphetameme",
    "rule" => array(
      "amphetameme[ \-]*crawler" => ""
    )
  ),
  "appie" => array(
    "icon" => "robot_robot.png",
    "title" => "Appie",
    "rule" => array(
      "appie[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "ask" => array(
    "icon" => "robot_askjeeves.png",
    "title" => "Ask Jeeves",
    "rule" => array(
      "Ask[ \-]*Jeeves" => "",
    )
  ),
  "axel" => array(
    "icon" => "robot_robot.png",
    "title" => "Axel",
    "rule" => 
    array(
      "axel" => ""
    )
  ),
  "baidu" => array(
    "icon" => "robot_baidu.png",
    "title" => "Baidu",
    "rule" => array(
      "Baiduspider" => ""
    )
  ),
  "blogbot" => array(
    "icon" => "robot_robot.png",
    "title" => "BlogBot",
    "rule" => array(
      "Blog[ \-]*Bot" => ""
    )
  ),
  "blogosphere" => array(
    "icon" => "robot_robot.png",
    "title" => "Blogosphere",
    "rule" => array(
      "Blogosphere" => ""
    )
  ),
  "blogpulse" => array(
    "icon" => "robot_robot.png",
    "title" => "Blogpulse",
    "rule" => array(
      "Blogpulse" => ""
    )
  ),
  "blogtick" => array(
    "icon" => "robot_robot.png",
    "title" => "BlogTick",
    "rule" => array(
      "BlogTickServer" => ""
    )
  ),
  "bobby" => array(
    "icon" => "robot_bobby.png",
    "title" => "Bobby",
    "rule" => array(
      "bobby[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "boitho" => array(
    "icon" => "robot_robot.png",
    "title" => "Boitho",
    "rule" => array(
      "Boitho\.com[ \-]*robot" => ""
    )
  ),
  "browserspy" => array(
    "icon" => "robot_robot.png",
    "title" => "BrowserSpy",
    "rule" => array(
      "BrowserSpy" => ""
    )
  ),
  "bruno" => array(
    "icon" => "robot_robot.png",
    "title" => "Bruno",
    "rule" => array(
      "Bruno" => ""
    )
  ),
  "claymont" => array(
    "icon" => "robot_claymont.png",
    "title" => "Claymont",
    "rule" => array(
      "claymont\.com" => ""
    )
  ),
  "cosmos" => array(
    "icon" => "robot_robot.png",
    "title" => "Cosmos",
    "rule" => array(
      "cosmos" => ""
    )
  ),
  "csscheck" => array(
    "icon" => "robot_css.png",
    "title" => "CSSCheck",
    "rule" => array(
      "CSS_Validator" => "",
      "CSSCheck" => ""
    )
  ),
  "custo" => array(
    "icon" => "robot_robot.png",
    "title" => "Custo",
    "rule" => array(
      "Custo[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "d4x" => array(
    "icon" => "robot_d4x.png",
    "title" => "Downloader for X",
    "rule" => array(
      "Downloader for X[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "doctorhtml" => array(
    "icon" => "robot_robot.png",
    "title" => "DoctorHTML",
    "rule" => array(
      "Doctor[ \-]*HTML" => ""
    )
  ),
  "dsns" => array(
    "icon" => "robot_robot.png",
    "title" => "DSNS Scanner",
    "rule" => array(
      "DSNS" => ""
    )
  ),
  "emailsiphon" => array(
    "icon" => "robot_robot.png",
    "title" => "Email Siphon",
    "rule" => array(
      "Email[ \-]*Siphon" => ""
    )
  ),
  "euroseek" => array(
    "icon" => "robot_euroseek.png",
    "title" => "EuroSeek",
    "rule" => array(
      "Arachnoidea" => ""
    )
  ),
  "exabot" => array(
    "icon" => "robot_exabot.png",
    "title" => "ExaBot",
    "rule" => array(
      "NG[ /]+([0-9.]+)" => "\\1",
      "Exalead[ \-]*NG" => ""
    )
  ),
  "exactseek" => array(
    "icon" => "robot_robot.png",
    "title" => "ExactSeek",
    "rule" => array(
      "ExactSeek[ \-]*Crawler" => ""
    )
  ),
  "excite" => array(
    "icon" => "robot_excite.png",
    "title" => "Excite",
    "rule" => array(
      "Architext[ \-]*Spider" => ""
    )
  ),
  "fast" => array(
    "icon" => "robot_fast.png",
    "title" => "Fast",
    "rule" => array(
      "fast[ \-]*webcrawler[ /]+([0-9.]+)" => "\\1",
      "crawler@fast\.no" => ""
    )
  ),
  "favorg" => array(
    "icon" => "robot_robot.png",
    "title" => "FavOrg",
    "rule" => array(
      "FavOrg" => ""
    )
  ),
  "fdse" => array(
    "icon" => "robot_robot.png",
    "title" => "FDSE Robot",
    "rule" => array(
      "FDSE[ \-]*robot" => ""
    )
  ),
  "firefly" => array(
    "icon" => "robot_firefly.png",
    "title" => "Firefly",
    "rule" => array(
      "Firefly" => ""
    )
  ),
  "friend" => array(
    "icon" => "robot_friend.png",
    "title" => "Friend",
    "rule" => array(
      "www\.friend\.fr" => ""
    )
  ),
  "frontier" => array(
    "icon" => "robot_robot.png",
    "title" => "Frontier",
    "rule" => array(
      "Frontier[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "gaisbot" => array(
    "icon" => "robot_robot.png",
    "title" => "Gaisbot",
    "rule" => array(
      "Gaisbot[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "galaxy" => array(
    "icon" => "robot_galaxy.png",
    "title" => "GalaxyBot",
    "rule" => array(
      "GalaxyBot[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "genome" => array(
    "icon" => "robot_robot.png",
    "title" => "Genome Machine",
    "rule" => array(
      "Genome[ \-]*Machine" => ""
    )
  ),
  "getright" => array(
    "icon" => "robot_getright.png",
    "title" => "GetRight",
    "rule" => array(
      "GetRight[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "gigabot" => array(
    "icon" => "robot_robot.png",
    "title" => "GigaBot",
    "rule" => array(
      "GigaBot" => ""
    )
  ),
  "girafabot" => array(
    "icon" => "robot_girafa.png",
    "title" => "Girafabot",
    "rule" => array(
      "Girafabot" => ""
    )
  ),
  "google" => array(
    "icon" => "robot_google.png",
    "title" => "GoogleBot",
    "rule" => array (
      "googlebot[ /]+([0-9.]+)" => "\\1",
      "Mediapartners[ \-]*Google[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "grub" => array(
    "icon" => "robot_grub.png",
    "title" => "Grub",
    "rule" => array(
      "grub[ \-]*client[ /\-]+([0-9.]+)" => "\\1",
    )
  ),
  "gulliver" => array(
    "icon" => "robot_robot.png",
    "title" => "Gulliver",
    "rule" => array(
      "Gulliver" => ""
    )
  ),
  "gziptester" => array(
    "icon" => "robot_robot.png",
    "title" => "Gzip Tester",
    "rule" => array(
      "gzip[ \-]*tester" => "",
      "GIDZip[ \-]*Test" => ""
    )
  ),
  "htdig" => array(
    "icon" => "robot_htdig.png",
    "title" => "ht://Dig",
    "rule" => array(
      "htdig[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "httpclient" => array(
    "icon" => "robot_robot.png",
    "title" => "HTTPClient",
    "rule" => array(
      "HTTP[ \-]*Client" => ""
    )
  ),
  "httperf" => array(
    "icon" => "robot_robot.png",
    "title" => "HTTPerf",
    "rule" => array(
      "httperf[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "indylibrary" => array(
    "icon" => "robot_robot.png",
    "title" => "Indy Library",
    "rule" => array(
      "Indy[ \-]*Library" => ""
    )
  ),
  "infoseek" => array(
    "icon" => "robot_robot.png",
    "title" => "Infoseek",
    "rule" => array(
      "Infoseek" => ""
    )
  ),
  "inktomi" => array(
    "icon" => "robot_inktomi.png",
    "title" => "Inktomi",
    "rule" => array(
      "slurp@inktomi\.com" => ""
    )
  ),
  "jigsaw" => array(
    "icon" => "robot_jigsaw.png",
    "title" => "Jigsaw",
    "rule" => array(
      "Jigsaw[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "lachesis" => array(
    "icon" => "robot_robot.png",
    "title" => "Lachesis",
    "rule" => array(
      "lachesis" => ""
    )
  ),
  "larbin" => array(
    "icon" => "robot_robot.png",
    "title" => "Larbin",
    "rule" => array(
      "larbin" => ""
    )
  ),
  "linkcheck" => array(
    "icon" => "robot_linkcheck.png",
    "title" => "Linkcheck",
    "rule" => array(
      "check[ \-]*link[ /]+([0-9.]+)" => "\\1",
      "Link[ \-]*Chec(k|ker)[ /]+([0-9.]+)" => "\\2",
      "Link[ \-]*Chec(k|ker)" => "",
      "Link[ \-]*Valet" => "",
      "link[ \-]*validator" => "",
    )
  ),
  "linkssql" => array(
    "icon" => "robot_robot.png",
    "title" => "Links SQL",
    "rule" => array(
      "links sql" => ""
    )
  ),
  "look" => array(
    "icon" => "robot_look.png",
    "title" => "Look",
    "rule" => array(
      "www\.look\.com" => ""
    )
  ),
  "loop" => array(
    "icon" => "robot_loop.png",
    "title" => "LOOP",
    "rule" => array(
      "NetResearchServer[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "looksmart" => array(
    "icon" => "robot_looksmart.png",
    "title" => "Looksmart",
    "rule" => array(
      "looksmart-sv-fw" => ""
    )
  ),
  "lotkyll" => array(
    "icon" => "robot_robot.png",
    "title" => "Lotkyll",
    "rule" => array(
      "Lotkyll" => ""
    )
  ),
  "lwp" => array(
    "icon" => "robot_robot.png",
    "title" => "lwp",
    "rule" => array(
      "lwp(-trivial|::simple)[ /]+([0-9.]+)" => "\\2"
    )
  ),
  "magpierss" => array(
    "icon" => "robot_robot.png",
    "title" => "MagpieRSS",
    "rule" => array(
      "MagpieRSS" => ""
    )
  ),
  "mailsweeper" => array(
    "icon" => "robot_robot.png",
    "title" => "Mail Sweeper",
    "rule" => array(
      "Mail[ \-]*Sweeper" => ""
    )
  ),
  "marvin" => array(
    "icon" => "robot_robot.png",
    "title" => "Marvin",
    "rule" => array(
      "Marvin" => ""
    )
  ),
  "mercator" => array(
    "icon" => "robot_robot.png",
    "title" => "Mercator",
    "rule" => array(
      "Mercator" => ""
    )
  ),
  "metager" => array(
    "icon" => "robot_metager.png",
    "title" => "MetaGer",
    "rule" => array(
      "MetaGer" => ""
    )
  ),
  "mirago" => array(
    "icon" => "robot_robot.png",
    "title" => "Mirago",
    "rule" => array(
      "Mirago" => ""
    )
  ),
  "momspider" => array(
    "icon" => "robot_robot.png",
    "title" => "MomSpider",
    "rule" => array(
      "momspider" => ""
    )
  ),
  "msnbot" => array(
    "icon" => "robot_msn.png",
    "title" => "MSNBot",
    "rule" => array(
      "MSNBOT[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "msproxy" => array(
    "icon" => "robot_robot.png",
    "title" => "MSProxy",
    "rule" => array(
      "MSProxy[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "mswebdav" => array(
    "icon" => "robot_robot.png",
    "title" => "MS-WebDAV",
    "rule" => array(
      "Microsoft[ \-]*WebDAV[ \-]*MiniRedir" => ""
    )
  ),
  "naverbot" => array(
    "icon" => "robot_naverbot.png",
    "title" => "NaverBot",
    "rule" => array(
      "NaverBot[/ \-]+([0-9.]+)" => "\\1",
      "dloader(NaverRobot)" => ""
    )
  ),
  "netcraft" => array(
    "icon" => "robot_netcraft.png",
    "title" => "Netcraft",
    "rule" => array(
      "netcraft" => ""
    )
  ),
  "netmechanic" => array(
    "icon" => "robot_netmechanic.png",
    "title" => "NetMechanic",
    "rule" => array(
      "NetMechanic[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "netoskop" => array(
    "icon" => "robot_robot.png",
    "title" => "Netoskop",
    "rule" => array(
      "netoskop" => ""
    )
  ),
  "nutchorg" => array(
    "icon" => "robot_robot.png",
    "title" => "NutchOrg",
    "rule" => array(
      "NutchOrg[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "obidosbot" => array(
    "icon" => "robot_robot.png",
    "title" => "ObidosBot",
    "rule" => array(
      "obidos[ \-]*bot" => ""
    )
  ),
  "organica" => array(
    "icon" => "robot_robot.png",
    "title" => "Organica",
    "rule" => array(
      "crawler@organica\.us" => ""
    )
  ),
  "overture" => array(
    "icon" => "robot_overture.png",
    "title" => "Overture",
    "rule" => array(
      "Overture[ \-]*WebCrawler" => ""
    )
  ),
  "php" => array(
    "icon" => "robot_php.png",
    "title" => "PHP",
    "rule" => array(
      "PHP[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "plsearch" => array(
    "icon" => "robot_plsearch.png",
    "title" => "PlanetSearch",
    "rule" => array(
      "fido[ /]+([0-9.]+) Harvest" => "PlanetSearch"
    )
  ),
  "poodle" => array(
    "icon" => "robot_robot.png",
    "title" => "Poodle predictor",
    "rule" => array(
      "Poodle[ \-]*predictor" => ""
    )
  ),
  "pompos" => array(
    "icon" => "robot_pompos.png",
    "title" => "Pompos",
    "rule" => array(
      "Pompos[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "popdex" => array(
    "icon" => "robot_robot.png",
    "title" => "Popdexter",
    "rule" => array(
      "Popdexter" => ""
    )
  ),
  "powermarks" => array(
    "icon" => "robot_robot.png",
    "title" => "Powermarks",
    "rule" => array(
      "Powermarks[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "proxyhunter" => array(
    "icon" => "robot_robot.png",
    "title" => "ProxyHunter",
    "rule" => array(
      "ProxyHunter" => ""
    )
  ),
  "psbot" => array(
    "icon" => "robot_robot.png",
    "title" => "PsBot",
    "rule" => array(
      "psbot" => ""
    )
  ),
  "pxys" => array(
    "icon" => "robot_robot.png",
    "title" => "PXYS",
    "rule" => array(
      "pxys" => ""
    )
  ),
  "pythonurl" => array(
    "icon" => "robot_robot.png",
    "title" => "Python-url",
    "rule" => array(
      "Python[ \-]*urllib" => ""
    )
  ),
  "quepasa" => array(
    "icon" => "robot_quepasa.png",
    "title" => "Quepasa",
    "rule" => array(
      "Quepasa[ \-]*Creep" => ""
    )
  ),
  "robozilla" => array(
    "icon" => "robot_robot.png",
    "title" => "Robozilla",
    "rule" => array(
      "Robozilla" => ""
    )
  ),
  "scrubby" => array(
    "icon" => "robot_scrubby.png",
    "title" => "Scrubby",
    "rule" => array(
      "Scrubby[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "shareware" => array(
    "icon" => "robot_robot.png",
    "title" => "Shareware",
    "rule" => array(
      "Program[ \-]*Shareware[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "singingfish" => array(
    "icon" => "robot_singingfish.png",
    "title" => "SingingFish",
    "rule" => array(
      "asterias[ /]+([0-9.]+)" => "\\1",
      "asterias" => ""
    )
  ),
  "sirobot" => array(
    "icon" => "robot_robot.png",
    "title" => "SiroBot",
    "rule" => array(
      "sirobot" => ""
    )
  ),
  "steeler" => array(
    "icon" => "robot_robot.png",
    "title" => "Steeler",
    "rule" => array(
      "Steeler[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "surfcontrol" => array(
    "icon" => "robot_robot.png",
    "title" => "SurfControl",
    "rule" => array(
      "SurfControl" => ""
    )
  ),
  "surveybot" => array(
    "icon" => "robot_robot.png",
    "title" => "SurveyBot",
    "rule" => array(
      "SurveyBot" => ""
    )
  ),
  "szukacz" => array(
    "icon" => "robot_szukacz.png",
    "title" => "Szukacz",
    "rule" => array(
      "Szukacz[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "teleport" => array(
    "icon" => "robot_teleport.png",
    "title" => "Teleport",
    "rule" => array(
      "Teleport[ \-]*Pro" => ""
    )
  ),
  "timbobot" => array(
    "icon" => "robot_robot.png",
    "title" => "timboBot",
    "rule" => array(
      "timboBot" => ""
    )
  ),
  "turnitin" => array(
    "icon" => "robot_turnitin.png",
    "title" => "TurnitinBot",
    "rule" => array(
      "TurnitinBot[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "urlcontr" => array(
    "icon" => "robot_robot.png",
    "title" => "URL Control",
    "rule" => array(
      "MS[ \-]*URL[ \-]*Control" => ""
    )
  ),
  "ultraseekt" => array(
    "icon" => "robot_robot.png",
    "title" => "Ultraseek",
    "rule" => array(
      "Ultraseek" => ""
    )
  ),
  "vagabondo" => array(
    "icon" => "robot_wise-guys.png",
    "title" => "Vagabondo",
    "rule" => array(
      "Vagabondo[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "validator" => array(
    "icon" => "robot_validator.png",
    "title" => "Validator",
    "rule" => array(
      "Validator[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "vindex" => array(
    "icon" => "robot_vindex.png",
    "title" => "Vindex",
    "rule" => array(
      "Vindex[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "voila" => array(
    "icon" => "robot_voila.png",
    "title" => "VoilaBot",
    "rule" => array(
      "VoilaBot[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "watson" => array(
    "icon" => "robot_addy.png",
    "title" => "Dr.Watson",
    "rule" => array(
      "Watson[ /]+([0-9.]+)" => "\\1",
      "watson\.addy\.com" => "",
    )
  ),
  "waypath" => array(
    "icon" => "robot_robot.png",
    "title" => "Waypath Scout",
    "rule" => array(
      "Waypath[ \-]*Scout" => ""
    )
  ),
  "webcollage" => array(
    "icon" => "robot_robot.png",
    "title" => "Webcollage",
    "rule" => array(
      "webcollage" => ""
    )
  ),
  "websiteman" => array(
    "icon" => "robot_websquash.png",
    "title" => "WebsiteMon",
    "rule" => array(
      "Website[ \-]*Monitor" => ""
    )
  ),
  "websquash" => array(
    "icon" => "robot_websquash.png",
    "title" => "Websquash",
    "rule" => array(
      "websquash\.com" => ""
    )
  ),
  "webzip" => array(
    "icon" => "robot_webzip.png",
    "title" => "WebZIP",
    "rule" => array(
      "Web[ \-]*ZIP[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "wget" => array(
    "icon" => "robot_wget.png",
    "title" => "Wget",
    "rule" => 
    array(
      "Wget[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "wmp" => array(
    "icon" => "robot_robot.png",
    "title" => "WMP",
    "rule" => array(
      "WMP" => ""
    )
  ),
  "wwgrapevine" => array(
    "icon" => "robot_wwgrapevine.png",
    "title" => "WWgrapevine",
    "rule" => 
    array(
      "wwgrapevine[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "xenu" => array(
    "icon" => "robot_robot.png",
    "title" => "Xenu Link Sleuth",
    "rule" => array(
      "Xenu[ \-]*Link[ \-]*Sleuth" => ""
    )
  ),
  "yahoo" => array(
    "icon" => "robot_yahoo.png",
    "title" => "Yahoo",
    "rule" => array(
      "YahooSeeker[ /]+([0-9.]+)" => "\\1",
      "Yahoo! Slurp;" => ""
    )
  ),
  "yandex" => array(
    "icon" => "robot_yandex.png",
    "title" => "Yandex",
    "rule" => array(
      "Yandex" => ""
    )
  ),
  "zao" => array(
    "icon" => "robot_robot.png",
    "title" => "Zao",
    "rule" => array(
      "Zao[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "zealbot" => array(
    "icon" => "robot_zeal.png",
    "title" => "ZealBot",
    "rule" => array(
      "ZealBot" => ""
    )
  ),
  "zeus" => array(
    "icon" => "robot_robot.png",
    "title" => "Zeus",
    "rule" => array(
      "Zeus" => ""
    )
  ),
  "zyborg" => array(
    "icon" => "robot_zyborg.png",
    "title" => "WiseNutBot",
    "rule" => array(
      "Zyborg[ /]+([0-9.]+)" => "\\1",
    )
  ),
// Catch up for things we don't know by now
  "robot" => array(
    "icon" => "robot_robot.png",
    "title" => "Robot",
    "rule" => array(
      "robot" => "",
      "crawler" => "",
      "spider" => ""
    )
  )
);
?>