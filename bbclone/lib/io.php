<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/io.php,v 1.28 2004/02/15 19:39:13 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* File:   io.php
 * Summary:   Contains all the function related to input-output actions
 * Description:
 */

// Parse all counter files of var/ and return an array of N rows, 
// with N as amount of new connections, sorted in increasing time of connection.
// The counters files are emptied afterwards.

function counter_to_array() {
  global $HTTP_SERVER_VARS, $BBC_CACHE_PATH, $BBC_COUNTER_PREFIX, $BBC_COUNTER_SUFFIX, $BBC_SEP, $BBC_OWN_REFER,
         $BBC_IGNORE_REFER, $BBC_COUNTER_NB_FIELDS, $BBC_ACCESS_LAST_FIELDS, $BBC_COUNTER_FILES;

  for ($i = 0, $nb_new_entry = 0; $i < $BBC_COUNTER_FILES; $i++) {
    $file_name = $BBC_CACHE_PATH.$BBC_COUNTER_PREFIX.$i. $BBC_COUNTER_SUFFIX;

    if (!is_readable($file_name)) {
      err_msg($file_name);
      $no_acc = 1;
      continue;
    }
    else ($file = file($file_name));

    for ($j = 0, $nb_entry = 0, $amax = count($file); $j < $amax; $j++) {
      $tmp = str_replace("\n","",$file[$j]);
      $tmp = explode($BBC_SEP, $tmp);
      if (($trimmed = trim($tmp[4])) != "unknown") {
        $test = substr(strstr($trimmed, "://"), 3);
        $slash = strpos($test, "/");
        // make the host always appear in lowercase
        $host_raw = strtolower(($slash !== false ? substr($test, 0, $slash) : $test));
        $host = (($port = strpos($host_raw, ":")) !== false ? substr($host_raw, 0, $port) : $host_raw);
        $uri = ($slash !== false ? substr($test, $slash) : "/");
        $uri = ereg_replace("[.]+(/|$)", "\\1", ereg_replace("[/]+", "/", $uri));
        // unless we find something special we use this as new default
        $tmp[4] = "http://".$host_raw.$uri;

        if (($host == strtolower(htmlentities($HTTP_SERVER_VARS["SERVER_NAME"], ENT_QUOTES))) &&
            (empty($BBC_OWN_REFER))) {
          $tmp[4] = "unknown";
        }
        // checking for matching hosts which we have to ignore
        if (!empty($BBC_IGNORE_REFER)) {
          $ign_ref = explode(",", $BBC_IGNORE_REFER);

          for ($i = 0, $bmax = count($ign_ref); $i < $bmax; $i++) {
            if (strpos($host, trim($ign_ref[$i])) !== false) $tmp[4] = "unknown";
          }
        }
      }
      // Avoiding ill formed counter file (possible attack !?)
      if ((empty($tmp[0])) || (!ereg("^[0-9]+$",$tmp[0]))) continue;

      for ($k = 0; ($k < $BBC_COUNTER_NB_FIELDS); $k++) {
        $counter_array[$nb_new_entry][($BBC_ACCESS_LAST_FIELDS[$k])] = $tmp[$k];
      }
      $nb_new_entry++;
    }
    // reset the counter files
    if (is_writable($file_name)) fclose(fopen($file_name, "wb"));
    else (empty($no_acc) ? err_msg($file_name, "write") : "");
  }
  // Sorting $counter_array in increasing time: the older first.
  if (!empty($counter_array)) usort($counter_array, "sort_time_sc");
  else $counter_array = array();
  return $counter_array;
}

function array_to_str($tab) {
// This function return a string description of an array.
// Format (_ == space):
// |_array(
// |__key1 => scal1, key2 => scal2, ...
// |__key3 =>
// |___array(
// |____key1 => scal1, ...
// |___),
// |__key4 => scal4, ...
// |_)

  static $indent = "";

  $oldindent = $indent;
  $indent   .= "  ";
  $sep       = "";

  $text = $indent."array(\n";
  $last_is_array = false;
  $k = 0;

  foreach ($tab as $key => $val) {
    // The separator treatment
    if (($last_is_array) || (is_array($val) && ($k !== 0))) {
      $text .= $sep."\n";
    }
    else $text .= $sep;

      // The key treatment
    if (is_int($key)) {
      if ($key !== $k) {
        $text .= (((is_array($val)) || ($k === 0) || ($last_is_array)) ? "$indent  " : "")
                ."$key =>"
                .((is_array($val)) ? "\n" : " ");
      }
      else $text .= ($k === 0) ? (is_array($val) ? "" : "$indent  ") : "";
    }
    else {
      $text .= (((is_array($val)) || ($k === 0) || ($last_is_array)) ? "$indent  " : "")
              ."\"$key\" =>"
              .((is_array($val)) ? "\n" : " ");
    }

    // The value treatment
    $last_is_array = false;
    if (is_array($val)) {
      $text .= array_to_str($val);
      $last_is_array = true;
      $sep = ",";
    }
    else {
      $text .= (is_int($val) ? $val : "\"$val\"");
      $sep = ", ";
    }
    $k++;
  }
  $text .= "\n$indent)";
  $indent = $oldindent;
  return $text;
}

function save_access() {
// Updates access.php with the latest data
  global $access, $BBC_ACCESS_FILE;

  $fp = fopen($BBC_ACCESS_FILE, "a+");
  if (flock($fp, LOCK_EX)) {
    fseek($fp, 0);
    ftruncate($fp, 0);
    fwrite($fp, "<?php\n\$access =\n".array_to_str($access).";\n?>");
    fflush($fp);
    flock($fp, LOCK_UN);
  }
  fclose($fp);
}
?>