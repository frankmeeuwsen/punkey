<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/extension.php,v 1.14 2004/01/18 00:37:05 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or  
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

/* File:  extension.php
 * Summary:  contain the list of all the country
 * Description:
 * Warning: the language must be loaded before the use of this file.
 */

$extension = array(
"numeric" => "$_[ext_IP]", "com" => "$_[ext_com]",
"net" => "$_[ext_net]", "edu" => "$_[ext_edu]",
"biz" => "$_[ext_biz]", "info" => "$_[ext_info]",
"jp" => "$_[ext_jp]", "us" => "$_[ext_us]",
"uk" => "$_[ext_uk]", "de" => "$_[ext_de]",
"mil" => "$_[ext_mil]", "ca" => "$_[ext_ca]",
"it" => "$_[ext_it]", "au" => "$_[ext_au]",
"org" => "$_[ext_org]", "nl" => "$_[ext_nl]",
"fr" => "$_[ext_fr]", "tw" => "$_[ext_tw]",
"gov" => "$_[ext_gov]", "fi" => "$_[ext_fi]",
"br" => "$_[ext_br]", "se" => "$_[ext_se]",
"es" => "$_[ext_es]", "no" => "$_[ext_no]",
"mx" => "$_[ext_mx]", "kr" => "$_[ext_kr]",
"ch" => "$_[ext_ch]", "dk" => "$_[ext_dk]",
"be" => "$_[ext_be]", "at" => "$_[ext_at]",
"nz" => "$_[ext_nz]", "ru" => "$_[ext_ru]",
"pl" => "$_[ext_pl]", "za" => "$_[ext_za]",
"unknown" => "$_[ext_unknown]","ar" => "$_[ext_ar]",
"il" => "$_[ext_il]", "sg" => "$_[ext_sg]",
"arpa" => "$_[ext_arpa]", "cz" => "$_[ext_cz]",
"hu" => "$_[ext_hu]", "hk" => "$_[ext_hk]",
"pt" => "$_[ext_pt]", "tr" => "$_[ext_tr]",
"gr" => "$_[ext_gr]", "cn" => "$_[ext_cn]",
"ie" => "$_[ext_ie]", "my" => "$_[ext_my]",
"th" => "$_[ext_th]", "cl" => "$_[ext_cl]",
"co" => "$_[ext_co]", "is" => "$_[ext_is]",
"uy" => "$_[ext_uy]", "ee" => "$_[ext_ee]",
"in" => "$_[ext_in]", "ua" => "$_[ext_ua]",
"sk" => "$_[ext_sk]", "ro" => "$_[ext_ro]",
"ae" => "$_[ext_ae]", "id" => "$_[ext_id]",
"su" => "$_[ext_su]", "si" => "$_[ext_si]",
"hr" => "$_[ext_hr]", "ph" => "$_[ext_ph]",
"lv" => "$_[ext_lv]", "ve" => "$_[ext_ve]",
"bg" => "$_[ext_bg]", "lt" => "$_[ext_lt]",
"yu" => "$_[ext_yu]", "lu" => "$_[ext_lu]",
"nu" => "$_[ext_nu]", "pe" => "$_[ext_pe]",
"cr" => "$_[ext_cr]", "int" => "$_[ext_int]",
"do" => "$_[ext_do]", "cy" => "$_[ext_cy]",
"pk" => "$_[ext_pk]", "cc" => "$_[ext_cc]",
"tt" => "$_[ext_tt]", "eg" => "$_[ext_eg]",
"lb" => "$_[ext_lb]", "kw" => "$_[ext_kw]",
"to" => "$_[ext_to]", "kz" => "$_[ext_kz]",
"na" => "$_[ext_na]", "mu" => "$_[ext_mu]",
"bm" => "$_[ext_bm]", "sa" => "$_[ext_sa]",
"zw" => "$_[ext_zw]", "kg" => "$_[ext_kg]",
"cx" => "$_[ext_cx]", "pa" => "$_[ext_pa]",
"gt" => "$_[ext_gt]", "bw" => "$_[ext_bw]",
"mk" => "$_[ext_mk]", "gl" => "$_[ext_gl]",
"ec" => "$_[ext_ec]", "lk" => "$_[ext_lk]",
"md" => "$_[ext_md]", "py" => "$_[ext_py]",
"bo" => "$_[ext_bo]", "bn" => "$_[ext_bn]",
"mt" => "$_[ext_mt]", "fo" => "$_[ext_fo]",
"ac" => "$_[ext_ac]", "pr" => "$_[ext_pr]",
"am" => "$_[ext_am]", "pf" => "$_[ext_pf]",
"ge" => "$_[ext_ge]", "bh" => "$_[ext_bh]",
"ni" => "$_[ext_ni]", "by" => "$_[ext_by]",
"sv" => "$_[ext_sv]", "ma" => "$_[ext_ma]",
"ke" => "$_[ext_ke]", "ad" => "$_[ext_ad]",
"zm" => "$_[ext_zm]", "np" => "$_[ext_np]",
"bt" => "$_[ext_bt]", "sz" => "$_[ext_sz]",
"ba" => "$_[ext_ba]", "om" => "$_[ext_om]",
"jo" => "$_[ext_jo]", "ir" => "$_[ext_ir]",
"st" => "$_[ext_st]", "vi" => "$_[ext_vi]",
"ci" => "$_[ext_ci]", "jm" => "$_[ext_jm]",
"li" => "$_[ext_li]", "ky" => "$_[ext_ky]",
"gp" => "$_[ext_gp]", "mg" => "$_[ext_mg]",
"gi" => "$_[ext_gi]", "sm" => "$_[ext_sm]",
"as" => "$_[ext_as]", "tz" => "$_[ext_tz]",
"ws" => "$_[ext_ws]", "tm" => "$_[ext_tm]",
"mc" => "$_[ext_mc]", "sn" => "$_[ext_sn]",
"hm" => "$_[ext_hm]", "fm" => "$_[ext_fm]",
"fj" => "$_[ext_fj]", "cu" => "$_[ext_cu]",
"rw" => "$_[ext_rw]", "mq" => "$_[ext_mq]",
"ai" => "$_[ext_ai]", "pg" => "$_[ext_pg]",
"bz" => "$_[ext_bz]", "sh" => "$_[ext_sh]",
"aw" => "$_[ext_aw]", "mv" => "$_[ext_mv]",
"nc" => "$_[ext_nc]", "ag" => "$_[ext_ag]",
"uz" => "$_[ext_uz]", "tj" => "$_[ext_tj]",
"sb" => "$_[ext_sb]", "bf" => "$_[ext_bf]",
"kh" => "$_[ext_kh]", "tc" => "$_[ext_tc]",
"tf" => "$_[ext_tf]", "az" => "$_[ext_az]",
"dm" => "$_[ext_dm]", "mz" => "$_[ext_mz]",
"mo" => "$_[ext_mo]", "vu" => "$_[ext_vu]",
"mn" => "$_[ext_mn]", "ug" => "$_[ext_ug]",
"tg" => "$_[ext_tg]", "ms" => "$_[ext_ms]",
"ne" => "$_[ext_ne]", "gf" => "$_[ext_gf]",
"gu" => "$_[ext_gu]", "hn" => "$_[ext_hn]",
"al" => "$_[ext_al]", "gh" => "$_[ext_gh]",
"nf" => "$_[ext_nf]", "io" => "$_[ext_io]",
"gs" => "$_[ext_gs]", "ye" => "$_[ext_ye]",
"an" => "$_[ext_an]", "aq" => "$_[ext_aq]",
"tn" => "$_[ext_tn]", "ck" => "$_[ext_ck]",
"ls" => "$_[ext_ls]", "et" => "$_[ext_et]",
"ng" => "$_[ext_ng]", "sl" => "$_[ext_sl]",
"bb" => "$_[ext_bb]", "je" => "$_[ext_je]",
"vg" => "$_[ext_vg]", "vn" => "$_[ext_vn]",
"mr" => "$_[ext_mr]", "gy" => "$_[ext_gy]",
"ml" => "$_[ext_ml]", "ki" => "$_[ext_ki]",
"tv" => "$_[ext_tv]", "dj" => "$_[ext_dj]",
"km" => "$_[ext_km]", "dz" => "$_[ext_dz]",
"im" => "$_[ext_im]", "pn" => "$_[ext_pn]",
"qa" => "$_[ext_qa]", "gg" => "$_[ext_gg]",
"bj" => "$_[ext_bj]", "ga" => "$_[ext_ga]",
"gb" => "$_[ext_gb]", "bs" => "$_[ext_bs]",
"va" => "$_[ext_va]", "lc" => "$_[ext_lc]",
"cd" => "$_[ext_cd]", "gm" => "$_[ext_gm]",
"mp" => "$_[ext_mp]", "gw" => "$_[ext_gw]",
"cm" => "$_[ext_cm]", "ao" => "$_[ext_ao]",
"er" => "$_[ext_er]", "ly" => "$_[ext_ly]",
"cf" => "$_[ext_cf]", "mm" => "$_[ext_mm]",
"td" => "$_[ext_td]", "iq" => "$_[ext_iq]",
"kn" => "$_[ext_kn]", "sc" => "$_[ext_sc]",
"cg" => "$_[ext_cg]", "gd" => "$_[ext_gd]",
"nr" => "$_[ext_nr]", "af" => "$_[ext_af]",
"cv" => "$_[ext_cv]", "mh" => "$_[ext_mh]",
"pm" => "$_[ext_pm]", "so" => "$_[ext_so]",
"vc" => "$_[ext_vc]", "bd" => "$_[ext_bd]",
"gn" => "$_[ext_gn]", "ht" => "$_[ext_ht]",
"la" => "$_[ext_la]", "lr" => "$_[ext_lr]",
"mw" => "$_[ext_mw]", "pw" => "$_[ext_pw]",
"re" => "$_[ext_re]", "tk" => "$_[ext_tk]",
"bi" => "$_[ext_bi]", "bv" => "$_[ext_bv]",
"fk" => "$_[ext_fk]", "gq" => "$_[ext_gq]",
"sd" => "$_[ext_sd]", "sj" => "$_[ext_sj]",
"sr" => "$_[ext_sr]", "sy" => "$_[ext_sy]",
"tp" => "$_[ext_tp]", "um" => "$_[ext_um]",
"wf" => "$_[ext_wf]", "yt" => "$_[ext_yt]",
"zr" => "$_[ext_zr]"
);
?>