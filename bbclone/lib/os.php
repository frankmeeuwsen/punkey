<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/os.php,v 1.35 2004/02/15 19:39:13 joku Exp $
 *
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:    os.php
 * Summary: contain the list of all the possible Operating Systems (OS) and also
 *          the different rules to classify them.
 */

$os = array(
  "aix"=> array(
    "icon"=> "os_aix.png",
    "title" => "AIX",
    "rule" => array(
      "aix" => ""
    )
  ),
  "atheos" => array(
    "icon" => "os_atheos.png",
    "title" => "AtheOS",
    "rule" => array(
      "atheos" => ""
    )
  ),
  "amiga" => array(
    "icon" => "os_amiga.png",
    "title" => "AmigaOS",
    "rule" => array(
      "Amiga[ ]*OS[ /]([0-9.]+)" => "\\1",
      "amiga" => ""
    )
  ),
  "beos" => array(
    "icon" => "os_be.png",
    "title" => "BeOS",
    "rule" => array(
      "beos[ a-z]*([0-9.]+)" => "\\1",
      "beos" => ""
    )
  ),
  "darwin" => array(
    "icon" => "os_darwin.png",
    "title" => "Darwin",
    "rule" => array(
      "Darwin[ ]*([0-9.]+)" => "\\1",
      "Darwin" => ""
    )
  ),
  "digital" => array(
    "icon" => "os_digital.png",
    "title" => "Digital",
    "rule" => array(
      "OSF([0-9.]+)[ ]*V4" => "\\1"
    )
  ),
  "freebsd" => array(
    "icon" => "os_freebsd.png",
    "title" => "FreeBSD",
    "rule" => array(
     "free[ \-]*bsd[ /]([a-z0-9.]+)" => "\\1",
     "free[ \-]*bsd" => ""
    )
  ),
  "hpux" => array(
    "icon" => "os_hp.png",
    "title" => "HPUX",
    "rule" => array(
      "hp[ \-]*ux[ /]([a-z0-9.]+)" => "\\1",
    )
  ),
  "irix" => array(
    "icon" => "os_irix.png",
    "title" => "IRIX",
    "rule" => array(
      "irix[0-9]*[ /]([0-9.]+)" => "\\1", 
      "irix" => ""
    )
  ),
  "linux" => array(
    "icon" => "os_linux.png",
    "title" => "Linux",
    "rule" => array(
      "mdk for ([0-9.])+" => "MDK \\1",
      "linux[ /\-]([a-z0-9.]+)" => "\\1",
      "linux" => ""
    )
  ),
  "macosx" => array(
    "icon" => "os_macosx.png",
    "title" => "MacOS X",
    "rule" => array(
      "Mac[ ]*OS[ ]*X" => ""
    )
  ),
  "macppc" => array(
    "icon" => "os_macppc.png",
    "title" => "MacOS PPC",
    "rule" => array(
      "Mac_PowerPC" => "",
      "macintosh[[:alnum:][:punct:] ]*ppc" => ""
    )
  ),
  "netbsd" => array(
    "icon" => "os_netbsd.png",
    "title" => "NetBSD",
    "rule" => array(
      "net[ \-]*bsd[ /]([a-z0-9.]+)" => "\\1",
      "net[ \-]*bsd" => ""
    )
  ),
  "os2" => array(
    "icon" => "os_os2.png",
    "title" => "OS/2 Warp",
    "rule" => array(
      "warp[ /]+([0-9.]+)" => "\\1",
      "os[ /]*2" => "",
    )
  ),
  "openbsd" => array(
    "icon" => "os_openbsd.png",
    "title" => "OpenBSD",
    "rule" => array(
      "open[ \-]*bsd[ /]([a-z0-9.]+)" => "\\1",
      "open[ \-]*bsd" => ""
    )
  ),
  "openvms" => array(
    "icon" => "os_openvms.png",
    "title" => "OpenVMS",
    "rule" => array(
      "Open[ \-]*VMS[ /]([a-z0-9.]+)" => "\\1",
      "Open[ \-]*VMS" => ""
    )
  ),
  "palm" => array(
    "icon" => "os_palm.png",
    "title" => "PalmOS",
    "rule" => array(
      "Palm[ \-]*OS[ /]+([0-9.]+)" => "\\1",
      "Palm[ \-]*OS" => ""
    )
  ),
  "photon" => array(
    "icon" => "os_qnx.png",
    "title" => "QNX Photon",
    "rule" => array(
      "photon" => ""
    )
  ),
  "sun" => array(
    "icon" => "os_sun.png",
    "title" => "SunOS",
    "rule" => array(
      "sun[ \-]*os[ /]+([0-9.]+)" => "\\1",
      "sun[ \-]*os" => ""
    )
  ),
  "symbian" => array(
    "icon"  => "os_symbian.png",
    "title" => "Symbian OS",
    "rule"  => array(
      "Symbian" => ""
    )
  ),
  "tru64" => array(
    "icon" => "os_tru64.png",
    "title" => "Tru64",
    "rule" => array(
      "OSF([0-9.]+)[ ]*V5" => "\\1"
    )
  ),
  "unixware" => array(
    "icon" => "os_sco.png",
    "title" => "UnixWare",
    "rule" => array(
      "unixware[ /]+([0-9.]+)" => "\\1",
      "unixware" => ""
    )
  ),
  "windows2003" => array(
    "icon" => "os_windowsxp.png",
    "title" => "Windows 2003",
    "rule" => array(
      "Wi(n|ndows)[ \-]*2003" => "",
      "wi(n|ndows)[ \-]*nt[ /]*5\.2" => ""
    )
  ),
  "windows2k" => array(
    "icon" => "os_windows.png",
    "title" => "Windows 2000",
    "rule" => array(
      "wi(n|ndows)[ \-]*nt[ /]*5\.0" => "",
      "wi(n|ndows)[ \-]*2000" => ""
    )
  ),
  "windows95" => array(
    "icon" => "os_windows.png",
    "title" => "Windows 95",
    "rule" => array(
      "wi(n|ndows)[ \-]*95" => "",
    )
  ),
  "windowsce" => array(
    "icon" => "os_windows.png",
    "title" => "Windows CE",
    "rule" => array(
      "wi(n|ndows)[ \-]*ce" => ""
    )
  ),
  "windowsme" => array(
    "icon" => "os_windows.png",
    "title" => "Windows ME",
    "rule" => array(
      "win 9x 4\.90" => "",
      "wi(n|ndows)[ \-]*me" => "",
    )
  ),
  "windowsxp" => array(
    "icon" => "os_windowsxp.png",
    "title" => "Windows XP",
    "rule" => array(
      "Windows XP" => "",
      "wi(n|ndows)[ \-]*nt[ /]*5\.1" => ""
    )
  ),
// The following ones are catch ups, they got to stay here.
  "bsd" => array(
    "icon" => "os_bsd.png",
    "title" => "BSD",
    "rule" => array(
      "bsd" => ""
    )
  ),
  "mac" => array(
    "icon" => "os_mac.png",
    "title" => "MacOS",
    "rule" => array(
      "mac" => ""
    )
  ),
  "windowsnt" => array(
    "icon" => "os_windows.png",
    "title" => "Windows NT",
    "rule" => array(
      "wi(n|ndows)[ \-]*nt[ /]*([0-4][0-9.]+)" => "\\2",
      "wi(n|ndows)[ \-]*nt" => "",
    )
  ),
  "windows98" => array(
    "icon" => "os_windows.png",
    "title" => "Windows 98",
    "rule" => array(
      "wi(n|ndows)[ \-]*98" => "",
    )
  ),
  "windows" => array(
    "icon" => "os_windows.png",
    "title" => "Windows",
    "rule" => array(
      "wi(n|n32|ndows)" => ""
    )
  ),
// things we don't know by now
  "other" => array(
    "icon" => "os_question.png",
    "title" => "other",
    "rule" => array(
      "." => ""
    )
  )
);
?>