<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/html.php,v 1.69 2004/02/15 19:39:13 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* File:   html.php
 * Summary:   contains various things about the html layout
 * Description:
 */

// The $font_size variable defined the possible size that fonts can take.
$BBC_FONT_SIZE = array("7pt", "8pt", "9pt", "10pt", "12pt", "14pt", "16pt");

// The link of the navigation bar 
$url_query = !empty($lang_tab[$lng]) ? "?lng=$lng" : "";

if (!empty($BBC_MAINSITE)) $navbar_title[$BBC_MAINSITE] = $_["navbar_Main_Site"];
if (!empty($BBC_SHOW_CONFIG)) $navbar_title["show_config.php$url_query"] = $_["navbar_Configuration"];

$navbar_title["show_global.php$url_query"] = $_["navbar_Global_Stats"];
$navbar_title["show_detailed.php$url_query"] = $_["navbar_Detailed_Stats"];
$navbar_title["show_time.php$url_query"] = $_["navbar_Time_Stats"];

// Begin of all the bbclone file
function bbc_html_document_begin() {
  global $BBC_VERSION, $BBC_IMAGES_PATH, $BBC_FONT_SIZE, $BBC_TEXT_SIZE, $BBC_TITLE_SIZE, $BBC_SUBTITLE_SIZE, $_;

  // Work around braindead default charset in Apache 2 (Thanks Martin Halachev!)
  if (!headers_sent()) header("Content-type: text/html; charset=".$_["global_charset"]);

  $text  = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
          ."<html>\n"
          ."<head>\n"
          ."<title>BBClone Stats v". $BBC_VERSION. "</title>\n"
          ."<link rel=\"shortcut icon\" href=\"".$BBC_IMAGES_PATH."favicon.ico\">\n"
          ."<meta http-equiv=\"pragma\" content=\"no-cache\">\n"
          ."<meta http-equiv=\"Content-Type\" content=\"text/html; charset=$_[global_charset]\">\n"
          ."<style type=\"text/css\">\n"
  // Body styles
          ."  body {margin: 0px}\n"
  // General links styles
          ."  a:hover {text-decoration: none; color: #43A246}\n"
          ."  a {text-decoration: underline}\n"
  // Navbar styles
          ."  a.navbar {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: "
          .$BBC_FONT_SIZE[$BBC_SUBTITLE_SIZE]."; text-decoration: underline; color: #838ba0}\n"
          ."  a.navbar:hover  {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: "
          .$BBC_FONT_SIZE[$BBC_SUBTITLE_SIZE]."; text-decoration: none; color: #43A246}\n"
          ."  p.navbar {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: "
          .$BBC_FONT_SIZE[$BBC_SUBTITLE_SIZE]."; color: #98a3d1; font-weight: bold; margin: 0px; padding: 0px}\n"
  // Titlebar styles
          ."  p.titlebar {color: #707a9e; font-weight: bold; font-size: ".$BBC_FONT_SIZE[$BBC_TITLE_SIZE]."}\n"
  // Misc styles
          ."  p {font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; font-size: "
          .$BBC_FONT_SIZE[$BBC_TEXT_SIZE]."}\n"
          ."  td {font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; font-size: "
          .$BBC_FONT_SIZE[$BBC_TEXT_SIZE]."}\n"
  // time stats
          ."  .graph {font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; font-size: 7pt; "
          ."vertical-align: top}\n"
          ."  .capt {font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; font-size: 8pt}\n"
          ."</style>\n"
          ."</head>\n\n"
          ."<body bgcolor=\"#ffffff\" link=\"#ff0000\" vlink=\"#ff0000\">\n"
  // BBClone Copyleft notice
          ."<!-- ############################################# -->\n"
          ."<!-- #                                           # -->\n"
          ."<!-- #             BBClone v ".$BBC_VERSION."            # -->\n"
          ."<!-- #            http://bbclone.de/             # -->\n"
          ."<!-- #        Licensed under the GNU/GPL         # -->\n"
          ."<!-- #      (c)2001-2004 The BBClone team        # -->\n"
          ."<!-- #                                           # -->\n"
          ."<!-- ############################################# -->\n\n";
  return $text;
}

// End of all the bbclone html document
function bbc_html_document_end() {
  $text = "</body>\n"
         ."</html>\n";
  return $text;
}

// Return the navigation toolbar
//  if set to 0 $lang_sel turns off the navbar and $on_bottom the title
function bbc_topbar($lang_sel = 1, $on_bottom = 0) {
  global $_, $BBC_TITLEBAR, $HTTP_SERVER_VARS, $BBC_GENERAL_ALIGN_STYLE, $BBC_IMAGES_PATH, $lang_tab, $lng,
         $navbar_title;

  $server = !empty($HTTP_SERVER_VARS["SERVER_NAME"]) ? htmlentities($HTTP_SERVER_VARS["SERVER_NAME"], ENT_QUOTES) :
            "noname";
  $head = str_replace("%DATE", date($_["global_date_format"]), str_replace("%SERVER", $server, $BBC_TITLEBAR));
  $self = htmlentities($HTTP_SERVER_VARS["PHP_SELF"], ENT_QUOTES);

  $str = (empty($lang_sel) ? "" : "<form method=\"post\" action=\"".str_replace("index.php", "", $self)."\">\n")
         ."<table border=\"0\" cellspacing=\"1\" cellpadding=\"2\" width=\"100%\" bgcolor=\"#d1daf4\">\n"
         ."<tr align=\"$BBC_GENERAL_ALIGN_STYLE\">\n"
         ."<td height=\"26\" width=\"100%\" valign=\"middle\">\n"
         ."<p class=\"navbar\">\n";

  $sep = "";
  reset($navbar_title);


  while (list($url, $title) = each($navbar_title)) {
    $str .= "$sep<a class=\"navbar\" href=\"$url\">$title</a>";
    $sep = "&nbsp;|\n";
  }

  if (!empty($lang_sel)) {
    $str .= "&nbsp;|&nbsp;\n";
    $jscode = "onchange=\"if (this.selectedIndex>0){location.href='$self?lng=' + "
             ."this.options[this.selectedIndex].value;}\"";
    $str .= "<select name=\"lng\" $jscode>\n"
            ."<option value=\"\"".(empty($lng) ? " selected" : "").">Language</option>\n";
    while (list($lang_id, $lang_name) = each($lang_tab)) {
      $str .= "<option value=\"$lang_id\"".(($lng == $lang_id) ? " selected" : "").">$lang_name</option>\n";
    }
    $lang_tab_lng = empty($lang_tab[$lng]) ? "" : $lang_tab[$lng];
    $str .= "</select>\n"
           ."&nbsp;<input type=\"submit\" value=\"Go!\">\n";
  }

  $str .= "</p>\n"
         ."</td>\n"
         ."</tr>\n"
         ."</table>\n"
         .((!empty($on_bottom)) ? "" :
          "<table border=\"0\" cellspacing=\"1\" cellpadding=\"2\" width=\"100%\" bgcolor=\"#e8edf9\">\n"
         ."<tr>\n"
         ."<td width=\"100%\" align=\"$BBC_GENERAL_ALIGN_STYLE\">\n"
         ."<p class=\"titlebar\">\n"
         ."$head\n"
         ."</p>\n"
         ."</td>\n"
         ."</tr>\n"
         ."</table>\n")
         .(empty($lang_sel) ? "" : "</form>\n");

  return $str;
}

function bbc_copyright() {
  global $BBC_IMAGES_PATH, $BBC_VERSION, $BBC_GENERAL_ALIGN_STYLE, $_;

  return "<p align=\"$BBC_GENERAL_ALIGN_STYLE\"><a href=\"http://bbclone.de\">"
        ."BBClone v".$BBC_VERSION."</a> &copy; ".$_["global_bbclone_copyright"]
        ." <a href=\"http://www.gnu.org/copyleft/gpl.html\">GPL</a>. \n"
        ." <a href=\"http://validator.w3.org/check/referer\"><img src=\"".$BBC_IMAGES_PATH
        ."valid-html401.png\" height=\"15\" width=\"80\" "
        ."border=\"0\" alt=\"Valid HTML4.01!\" title=\"Valid HTML4.01!\" align=\"middle\"></a>\n"
        ." <a href=\"http://jigsaw.w3.org/css-validator/check/referer\"><img src=\""
        .$BBC_IMAGES_PATH."vcss.png\" height=\"15\" width=\"80\" "
        ."border=\"0\" alt=\"Valid CSS!\" title=\"Valid CSS!\" align=\"middle\"></a></p>\n";
}

// Plot a positive sequence of integers $y in function of a sequence $x
// (whatever it is) in a box of size [$width * $height] in pixels
// Note: the width is only an indication. The real width is always a bit greater
// than $width (html sucks).
function bbc_plot($x, $y, $width, $height) {
  global $BBC_IMAGES_PATH;

  // Graphical elements
  $bar = $BBC_IMAGES_PATH."plot_bar.png";
  $bar_top = $BBC_IMAGES_PATH."plot_bar_top.png";
  $bar_bottom = $BBC_IMAGES_PATH."plot_bar_bottom.png";
  $bar_empty = $BBC_IMAGES_PATH."plot_bar_empty.png";
  $lblue = $BBC_IMAGES_PATH."plot_color_edf7ff.png";
  $hline = $BBC_IMAGES_PATH."plot_hline.png";
  $white = $BBC_IMAGES_PATH."plot_color_white.png";
  $label = $BBC_IMAGES_PATH."plot_int_";
  $xtick = $BBC_IMAGES_PATH."plot_xtick.png";

  // Various sizes
  $nb_x = count($x);
  $nb_y = count($y);

  if (!empty($x)) $nb = min($nb_x, $nb_y);
  else $nb = $nb_y;

  $bar_width = max(3, floor($width / $nb)) - 2;

  // Finding the maxima
  for ($k = 0, $max_y = 0; $k < $nb; $max_y = max($y[$k],$max_y), $k++);
  // The height of one unit
  $unit_height = $max_y ? (0.8 * ($height / $max_y)) : 0;

  // THE GRAPH
  $text  = "<table><tr><td width=\"$width\" height=\"$height\"><table border=\"0\" cellspacing=\"0\" "
          ."cellpadding=\"0\">\n"
  // Drawing the bar
          ."<tr bgcolor=\"#edf7ff\">\n";
  for ($k = 0; $k < $nb; $k++) {
    $bar_top_height = max(4, round(10 * $bar_width / 50));
    $bar_height = round($y[$k] * $unit_height);
    $text .= "<td valign=\"bottom\" align=\"center\" width=\"".($bar_width + 2)."\">\n";

    if ($y[$k]) {
      if ($y[$k] > 1000) $numb = substr(($y[$k] / 1000) ,0 , 3)."k";
      else $numb = $y[$k];

      $text .= "<span class=\"graph\">$numb</span><br />\n"
              ."<img src=\"$bar_top\" alt=\"$numb\" title=\"$numb\" height=\"$bar_top_height\" width=\"$bar_width\"><br />\n"
              ."<img src=\"$bar\" alt=\"$numb\" title=\"$numb\" height=\"$bar_height\" width=\"$bar_width\"><br />\n"
              ."<img src=\"$bar_bottom\" alt=\"$numb\" title=\"$numb\" height=\"$bar_top_height\" width=\"$bar_width\"><br />\n";
    }
    else {
      $text .= "<img src=\"$bar_empty\" alt=\"0\" title=\"0\" height=\"$bar_top_height\" width=\"$bar_width\"><br />\n";
    }
    $text .= "</td>\n";
  }
  $text .= "</tr>\n";

  // Adding the $x label
  $text .= "<tr>\n";
  for ($k = 0; $k < $nb; $k++) {
    $text .= "<td valign=\"bottom\" align=\"center\" height=\"17\">\n"
            ."<span class=\"".(ereg("^[0-9]+$", $x[$k]) ? "graph" : "capt")."\">".$x[$k]."</span>\n"
            ."</td>\n";
  }
  $text .= "</tr>\n"
          ."</table></td></tr></table>\n";
  return $text;
}
?>