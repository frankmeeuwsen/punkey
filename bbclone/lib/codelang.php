<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/codelang.php,v 1.27 2004/02/15 19:39:13 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* File:   codelang.php
 * Summary:     gives the code table of the languages (e.g. "en" for "english")   
 * Description:
 */

$lang_tab = array(
"bg"    => "Bulgarian",
"ca"    => "Catalan",
"cs"    => "Czech",
"zh-cn" => "Chinese (Simp.)",
"zh-tw" => "Chinese (Trad.)",
"da"    => "Danish",
"nl"    => "Dutch",
"en"    => "English",
"fi"    => "Finnish",
"fr"    => "French",
"de"    => "German",
"it"    => "Italian",
"ja"    => "Japanese",
"lt"    => "Lithuanian",
"pl"    => "Polish",
"pt-br" => "Portuguese (Br.)",
"ro"    => "Romanian",
"ru"    => "Russian",
"es"    => "Spanish",
"se"    => "Swedish",
"tr"    => "Turkish",
"uk"    => "Ukrainian"
);
?>