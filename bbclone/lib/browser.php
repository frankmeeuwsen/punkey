<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/lib/browser.php,v 1.33 2004/02/17 21:58:00 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:   browser.php
 * Summary:   contain the list of all the possible browsers and
 *              also the different rules to classify them.
 */

// The browser table
// Note: 
// * each key of the "rule" array describe the regexp to apply to detect the browser,
//   the correspond value is for the title to display from this regexp.
// * "title" is used in the global sorting.

$browser = array(
  "abrowse" => array(
    "icon" => "browser_abrowse.png",
    "title" => "ABrowse",
    "rule" => array(
      "abrowse[ /\-]+([0-9.]+)" => "\\1",
      "abrowse" => ""
    )
  ),
  "aol" => array(
    "icon" => "browser_aol.png",
    "title" => "AOL",
    "rule" => array(
      "aol[ /\-]+([0-9.]+)" => "\\1",
      "aol[ /\-]*browser" => ""
    )
  ),
  "beonex" => array(
    "icon" => "browser_beonex.png",
    "title" => "Beonex",
    "rule" => array(
      "beonex/([0-9.]+)" => "\\1",
    )
  ),
  "camino" => array(
    "icon" => "browser_camino.png",
    "title" => "Camino",
    "rule" => array(
      "camino/([0-9.]+)" => "\\1"
    )
  ),
  "chimera" => array(
    "icon" => "browser_chimera.png",
    "title" => "Chimera",
    "rule" => array(
      "chimera/([0-9.]+)" => "\\1",
    )
  ),
  "columbus" => array(
    "icon" => "browser_columbus.png",
    "title" => "Columbus",
    "rule" => array(
      "columbus[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "curl" => array(
    "icon" => "browser_curl.png",
    "title" => "Curl",
    "rule" => array(
      "curl[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "dillo" => array(
    "icon" => "browser_dillo.png",
    "title" => "Dillo",
    "rule" => array(
      "dillo/([0-9.]+)" => "\\1",
    )
  ),
  "Elinks" => array(
    "icon" => "browser_links.png",
    "title" => "ELinks",
    "rule" => array(
      "ELinks[ /]*\(([0-9.]+)" => "\\1"
    )
  ),
  "Epiphany"  => array(
    "icon"  => "browser_epiphany.png",
    "title" => "Epiphany",
    "rule"  => array(
      "Epiphany/([0-9.]+)" => "\\1"
    )
  ),
  "firebird"  => array(
    "icon"  => "browser_firebird.png",
    "title" => "Firebird",
    "rule"  => array(
      "Firebird/([0-9.]+)" => "\\1"
    )
  ),
  "firefox"  => array(
    "icon"  => "browser_firefox.png",
    "title" => "Firefox",
    "rule"  => array(
      "Firefox/([0-9.]+)" => "\\1"
    )
  ),
  "galeon" => array(
    "icon" => "browser_galeon.png",
    "title" => "Galeon",
    "rule" => array(
      "galeon/([0-9.]+)" => "\\1",
    )
  ),
  "ibrowse" => array(
    "icon" => "browser_ibrowse.png",
    "title" => "IBrowse",
    "rule" => array(
      "ibrowse[ /]+([0-9.]+)" => "\\1",
      "ibrowse" => ""
    )
  ),
  "icab" => array(
    "icon" => "browser_icab.png",
    "title" => "iCab",
    "rule" => array(
      "icab/([0-9.]+)" => "\\1",
    )
  ),
  "java" => array(
    "icon" => "browser_java.png",
    "title" => "Java",
    "rule" => array(
      "java[ /]+([0-9.]+)" => "\\1",
      "java" => ""
    )
  ),
  "lotus" => array(
    "icon" => "browser_lotus.png",
    "title" => "Lotus Notes",
    "rule" => array(
      "Lotus[ \-]*Notes[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "k-meleon" => array(
    "icon" => "browser_k-meleon.png",
    "title" => "K-Meleon",
    "rule" => array(
      "K-Meleon[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "konqueror" => array(
    "icon" => "browser_konqueror.png",
    "title" => "Konqueror",
    "rule" => array(
      "konqueror/([0-9.]+)" => "\\1"
    )
  ),
  "links" => array(
    "icon" => "browser_links.png",
    "title" => "Links",
    "rule" => array(
      "Links[ /]*\(([0-9.]+)" => "\\1"
    )
  ),
  "lynx" => array(
    "icon" => "browser_lynx.png",
    "title" => "Lynx",
    "rule" => array(
      "lynx/([0-9a-z.]+)" => "\\1"
    )
  ),
  "mbrowser" => array(
    "icon" => "browser_mbrowser.png",
    "title" => "mBrowser",
    "rule" => array(
      "mBrowser[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "mosaic" => array(
    "icon" => "browser_mosaic.png",
    "title" => "Mosaic",
    "rule" => array(
      "mosaic[ /]+([0-9.]+)" => "\\1",
    )
  ),
  "nautilus" => array(
    "icon" => "browser_nautilus.png",
    "title" => "Nautilus",
    "rule" => array(
      "nautilus/([0-9.]+)" => "\\1",
      "gnome[ \-]*vfs/([0-9.]+)" => "\\1",
    )
  ),
  "netcaptor" => array(
    "icon" => "browser_netcaptor.png",
    "title" => "Netcaptor",
    "rule" => array(
      "netcaptor[ /]([0-9.]+)" => "\\1",
      "netcaptor" => ""
    )
  ),
  "netpositive" => array(
    "icon" => "browser_netpositive.png",
    "title" => "NetPositive",
    "rule" => array(
      "netpositive[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "omniweb" => array(
    "icon" => "browser_omniweb.png",
    "title" => "OmniWeb",
    "rule" => array(
      "omniweb/[ a-z]*([0-9.]+)$" => "\\1"
    )
  ),
  "opera" => array(
    "icon" => "browser_opera.png",
    "title" => "Opera",
    "rule" => array(
      "opera[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "plink" => array(
    "icon" => "browser_plink.png",
    "title" => "PLink",
    "rule" => array(
      "PLink[ /]([0-9a-z.]+)" => "\\1"
    )
  ),
  "phoenix" => array(
    "icon" => "browser_phoenix.png",
    "title" => "Phoenix",
    "rule" => array(
      "Phoenix/([0-9.]+)" => "\\1"
    )
  ),
  "safari" => array(
    "icon" => "browser_safari.png",
    "title" => "Safari",
    "rule" => array(
      "safari/([0-9.]+)" => "\\1",
      "safari" => ""
    )
  ),
  "staroffice" => array(
    "icon" => "browser_staroffice.png",
    "title" => "StarOffice",
    "rule" => array(
      "staroffice[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "voyager" => array(
    "icon" => "browser_voyager.png",
    "title" => "Voyager",
    "rule" => array(
      "voyager[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "w3m" => array(
    "icon" => "browser_w3m.png",
    "title" => "w3m",
    "rule" => array(
      "w3m/([0-9.]+)" => "\\1"
    )
  ),
  "webtv" => array(
    "icon" => "browser_webtv.png",
    "title" => "Webtv",
    "rule" => array(
      "webtv[ /]+([0-9.]+)" => "\\1",
      "webtv" => ""
    )
  ),
// Catch up for the originals. they got to stay in that order.
  "libwww" => array(
    "icon" => "browser_libwww.png",
    "title" => "libWWW",
    "rule" => array(
      "libww(w|w-perl|w-FM)[ /]+([0-9.]+)" => "\\2",
      "libww(w|w-perl|w-FM)" => ""
    )
  ),
  "explorer" => array(
    "icon" => "browser_explorer.png", 
    "title" => "Explorer",
    "rule" => array(
      "MSIE[ /]+([0-9.]+)" => "\\1"
    )
  ),
  "netscape" => array(
    "icon" => "browser_netscape.png",
    "title" => "Netscape",
    "rule" => array(
      "netscape[0-9]*/([0-9.]+)" => "\\1",
      "mozilla/([0-4][0-9.]+)" => "\\1"
    )
  ),
  "mozilla" => array(
    "icon" => "browser_mozilla.png",
    "title" => "Mozilla",
    "rule" => array(
      "mozilla/[5-9][0-9.]+[[:print:] ]+rv:([0-9a-z.]+)" => "\\1",
      "mozilla/([5-9][0-9a-z.]+)" => "\\1"
    )
  ),
// Things we don't know by now
  "other" => array(
    "icon" => "browser_question.png",
    "title" => "other",
    "rule" => array(
      "." => ""
    )
  )
);
?>