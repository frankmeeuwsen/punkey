<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/constants.php,v 1.46 2004/02/16 20:18:07 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* File:   constants.php
 * Summary:   Contain the important variables of all the bbclone files.
 * Description:
 * Remark: Macros ared defined with an underscore as first character;
 *         Variables with an alphabetical letter as first character.
 */

// $BBC_ACCESS_FILE is the file which stores, in the variable $last_access,
// the last accesses to the tagged pages.
//
// It contains:
// * in the field $access["last"]: all the connections made before the last $maxtime seconds
//   (from the execution time of log_processor.php)
//   and the number of older connection necessary to achieve a total of $maxvisible connections.
//   This is a php file which records the variable $access_stat.
// * in the field $access["stat"]: the global statistics on all the connections
//  
// Each row of $access["last"] is a subarray representing a connection,
// and containing the following fields (recorded in $BBC_ACCESS_LAST_FIELDS):
// "time"         => the unix connection time,
// "ip"           => the IP,
// "dns"          => the hostname,
// "agent"        => $HTTP_USER_AGENT,
// "referer"      => $HTTP_REFERER,
// "script"       => $REQUEST_URI (the last calling page),
// "script_name"  => page title (either user defined or automatically generated)
// "id"           => index of the connection
// "visits"       => visits, i.e. number of connections during the last $maxtime seconds, 
// "os"           => detected OS
// "os_note"      => additional note about the detected os (version, ...)
// "browser"      => detected browser
// "browser_note" => additional note about the detected browser (version, ...)
// "ext"          => detected extension
// "robot"        => the type of the detected robot
// "robot_note"   => additional note about the detected robot (version, ...)
//
// Note: the fields "time", "ip", "dns", "agent", "referer" and "script" correspond
// in order to the data recorded by mark_page.php into the nine main counters (*.inc) of var/

// BBClone's location relative from where it's been called
$BBC_ROOT_PATH = (!empty($BBCLONE_DIR) ? $BBCLONE_DIR : "");

// The version number of BBCLONE
$BBC_VERSION = "0.33.5p3";

// Path handling, change only if you exactly know what you're doing
$BBC_LIB_PATH  = $BBC_ROOT_PATH."lib/";
$BBC_LANGUAGE_PATH = $BBC_ROOT_PATH."language/";
$BBC_CACHE_PATH  = $BBC_ROOT_PATH."var/";
$BBC_DOC_PATH  = $BBC_ROOT_PATH."doc/";
$BBC_IMAGES_PATH = $BBC_ROOT_PATH."images/";
$BBC_CONF_PATH = $BBC_ROOT_PATH."conf/";
$BBC_CONFIG_FILE = $BBC_CONF_PATH."config.php";
$BBC_LOG_PROCESSOR = $BBC_ROOT_PATH."log_processor.php";

// name of the counter files
$BBC_COUNTER_PREFIX = "counter";
$BBC_COUNTER_SUFFIX = ".inc";
// how many fields they contain
$BBC_COUNTER_NB_FIELDS = 7;
// Amount of counter files
$BBC_COUNTER_FILES = 10;

// Interval in sec which the counter gets updated
$BBC_AUTO_UPDATE = 30;

// Global separator
$BBC_SEP = chr(173);

// access file related
$BBC_ACCESS_FILE = $BBC_CACHE_PATH."access.php";
$BBC_ACCESS_LAST_FIELDS = array("time", "ip", "dns", "agent", "referer", "script", "script_name", "id", "visits", "os",
                                "os_note", "browser", "browser_note", "ext");

// Global error handling for checking files'/directories' usabilitiy
function err_msg($item, $state = "read") {
  global $BBC_NO_STRING, $BBC_VERSION;

  echo (empty($BBC_NO_STRING) ? "\n<!-- BBClone v ".$BBC_VERSION." KO: "
      .(((substr($item, (strrpos($item, ".") + 1)) == "php") || (substr($item, (strrpos($item, ".") + 1)) == "inc") ||
       (substr($item, (strrpos($item, ".") + 1)) == "htalock")) ?
       "file" : "directory")." $item is ".(($state == "write") ? "read-only" : "inaccessible")." -->\n" : "");
}
?>