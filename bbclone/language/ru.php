<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/ru.php,v 1.4 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: ru.php
* Summary: contains a russian translation table by Alexey Rozenvaser alexroz@mailru.com
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "windows-1251",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "������� BBClone - �������� ",
"global_yes" => "��",
"global_no" => "���",

// The error messages
"error_cannot_see_config" => 
"�������� �������� BBClone ����������.",

// Address Extensions (see lib/extension.php)
"ext_other" => "������", "ext_com" => "������������",
"ext_net" => "Networks", "ext_edu" => "���������������",
"ext_biz" => "������������ �����������", "ext_info" => "��������������",
"ext_jp" => "������", "ext_us" => "���",
"ext_uk" => "������", "ext_de" => "��������",
"ext_mil" => "US Military", "ext_ca" => "������",
"ext_it" => "������", "ext_au" => "���������",
"ext_org" => "�������������� �����������", "ext_nl" => "����������",
"ext_fr" => "�������", "ext_tw" => "�������",
"ext_gov" => "�����������������", "ext_fi" => "���������",
"ext_br" => "Brazil", "ext_se" => "������",
"ext_es" => "�������", "ext_no" => "��������",
"ext_mx" => "�������", "ext_kr" => "�����",
"ext_ch" => "���������", "ext_dk" => "�����",
"ext_be" => "�������", "ext_at" => "�������",
"ext_nz" => "����� ��������", "ext_ru" => "��",
"ext_pl" => "������", "ext_za" => "���",
"ext_unknown" => "�����������", "ext_ar" => "���������",
"ext_il" => "�������", "ext_sg" => "��������",
"ext_arpa" => "Mistakes", "ext_cz" => "������� ����������",
"ext_hu" => "�������", "ext_hk" => "�������",
"ext_pt" => "����������", "ext_tr" => "������",
"ext_gr" => "������", "ext_cn" => "�����",
"ext_ie" => "��������", "ext_my" => "��������",
"ext_th" => "�������", "ext_cl" => "����",
"ext_co" => "��������", "ext_is" => "��������",
"ext_uy" => "�������", "ext_ee" => "�������",
"ext_in" => "�����", "ext_ua" => "�������",
"ext_sk" => "��������", "ext_ro" => "�������",
"ext_ae" => "������������ �������� �������", "ext_id" => "���������",
"ext_su" => "����", "ext_si" => "��������",
"ext_hr" => "��������", "ext_ph" => "���������",
"ext_lv" => "������", "ext_ve" => "���������",
"ext_bg" => "��������", "ext_lt" => "�����",
"ext_yu" => "���������", "ext_lu" => "����������",
"ext_nu" => "Niue", "ext_pe" => "����",
"ext_cr" => "����� ����", "ext_int" => "������������� �����������",
"ext_do" => "������������� ����������", "ext_cy" => "����",
"ext_pk" => "��������", "ext_cc" => "��������� �������",
"ext_tt" => "�������� � ������", "ext_eg" => "������",
"ext_lb" => "�����", "ext_kw" => "������",
"ext_to" => "Tonga", "ext_kz" => "���������",
"ext_na" => "�������", "ext_mu" => "��������",
"ext_bm" => "�������", "ext_sa" => "���������� ������",
"ext_zw" => "��������", "ext_kg" => "����������",
"ext_cx" => "Christmas Island", "ext_pa" => "������",
"ext_gt" => "���������", "ext_bw" => "��������",
"ext_mk" => "���������", "ext_gl" => "����������",
"ext_ec" => "�������", "ext_lk" => "��� �����",
"ext_md" => "�������", "ext_py" => "��������",
"ext_bo" => "�������", "ext_bn" => "������",
"ext_mt" => "������", "ext_fo" => "Faroe Islands",
"ext_ac" => "Ascension Island", "ext_pr" => "������ ����",
"ext_am" => "�������", "ext_pf" => "����������� ���������",
"ext_ge" => "������", "ext_bh" => "�������",
"ext_ni" => "���������", "ext_by" => "��������",
"ext_sv" => "���������", "ext_ma" => "�������",
"ext_ke" => "�����", "ext_ad" => "������",
"ext_zm" => "������", "ext_np" => "�����",
"ext_bt" => "�����", "ext_sz" => "���������",
"ext_ba" => "Bosnia and Herzegovina", "ext_om" => "����",
"ext_jo" => "��������", "ext_ir" => "����",
"ext_st" => "Sao Tome and Principe", "ext_vi" => "Virgin Islands (U.S.)",
"ext_ci" => "Ivory Coast", "ext_jm" => "������",
"ext_li" => "�����������", "ext_ky" => "��������� �������",
"ext_gp" => "���������", "ext_mg" => "����������",
"ext_gi" => "���������", "ext_sm" => "San Marino",
"ext_as" => "American Samoa", "ext_tz" => "��������",
"ext_ws" => "�����", "ext_tm" => "������������",
"ext_mc" => "������", "ext_sn" => "�������",
"ext_hm" => "Heard and Mc Donald Islands", "ext_fm" => "����������",
"ext_fj" => "�����", "ext_cu" => "����",
"ext_rw" => "������", "ext_mq" => "���������",
"ext_ai" => "Anguilla", "ext_pg" => "Papua New Guinea",
"ext_bz" => "�����", "ext_sh" => "St. Helena",
"ext_aw" => "�����", "ext_mv" => "��������",
"ext_nc" => "����� ���������", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "����������", "ext_tj" => "�����������",
"ext_sb" => "���������� �������", "ext_bf" => "������� ����",
"ext_kh" => "��������", "ext_tc" => "Turks and Caicos Islands",
"ext_tf" => "French Southern Territories", "ext_az" => "�����������",
"ext_dm" => "��������", "ext_mz" => "��������",
"ext_mo" => "Macau", "ext_vu" => "�������",
"ext_mn" => "��������", "ext_ug" => "������",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "�����", "ext_gf" => "French Guiana",
"ext_gu" => "����", "ext_hn" => "��������",
"ext_al" => "�������", "ext_gh" => "����",
"ext_nf" => "Norfolk Island", "ext_io" => "British Indian Ocean Territory",
"ext_gs" => "South Georgia and the South Sandwich Islands", "ext_ye" => "�����",
"ext_an" => "����������", "ext_aq" => "����������",
"ext_tn" => "�����", "ext_ck" => "Cook Islands",
"ext_ls" => "������", "ext_et" => "�������",
"ext_ng" => "�������", "ext_sl" => "Sierra Leone",
"ext_bb" => "��������", "ext_je" => "������",
"ext_vg" => "Virgin Islands (British)", "ext_vn" => "�������",
"ext_mr" => "����������", "ext_gy" => "������",
"ext_ml" => "����", "ext_ki" => "��������",
"ext_tv" => "Tuvalu", "ext_dj" => "�������",
"ext_km" => "��������� �������", "ext_dz" => "�����",
"ext_im" => "Isle of Man", "ext_pn" => "Pitcairn",
"ext_qa" => "�����", "ext_gg" => "Guernsey",
"ext_bj" => "�����", "ext_ga" => "�����",
"ext_gb" => "United Kingdom", "ext_bs" => "��������� �������",
"ext_va" => "�������", "ext_lc" => "Saint Lucia",
"ext_cd" => "�����", "ext_gm" => "������",
"ext_mp" => "Northern Mariana Islands", "ext_gw" => "������-�����",
"ext_cm" => "�������", "ext_ao" => "������",
"ext_er" => "�������", "ext_ly" => "�����",
"ext_cf" => "Central African Republic", "ext_mm" => "Myanmar",
"ext_td" => "���", "ext_iq" => "����",
"ext_kn" => "Saint Kitts and Nevis", "ext_sc" => "����������� �������",
"ext_cg" => "�����", "ext_gd" => "�������",
"ext_nr" => "Nauru", "ext_af" => "����������",
"ext_cv" => "Cape Verde", "ext_mh" => "���������� �������",
"ext_pm" => "St. Pierre and Miquelon", "ext_so" => "������",
"ext_vc" => "St. Vincent and the Grenadines", "ext_bd" => "���������",
"ext_gn" => "������", "ext_ht" => "�����",
"ext_la" => "����", "ext_lr" => "�������",
"ext_mw" => "������", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "�������", "ext_bv" => "Bouvet Island",
"ext_fk" => "������������ (�����������) �������", "ext_gq" => "�������������� ������",
"ext_sd" => "�����", "ext_sj" => "Svalbard and Jan Mayen Islands",
"ext_sr" => "�������", "ext_sy" => "�����",
"ext_tp" => "��������� �����", "ext_um" => "United States Minor Outlying Islands",
"ext_wf" => "Wallis and Futuna Islands", "ext_yt" => "Mayotte",
"ext_zr" => "����", "ext_IP" => "�������� IP",

// Miscellaneous translations
"misc_other" => "������",
"misc_unknown" => "�����������",
"misc_second_unit" => "���.",

// The Navigation Bar
"navbar_Main_Site" => "������� ����",
"navbar_Configuration" => "������������",
"navbar_Global_Stats" => "����� ����������",
"navbar_Detailed_Stats" => "��������� ����������",
"navbar_Time_Stats" => "���������� �� �������",
"navbar_Link_Stats" => "���������� �� �������",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "�����",
"dstat_Visits" => "���������",
"dstat_Extension" => "����",
"dstat_DNS" => "����� ����������",
"dstat_From" => "�� ������",
"dstat_OS" => "��",
"dstat_Browser" => "�������",
"dstat_New_access" => "����� ����������",
"dstat_Elapsed_time" => "�������� �����",
"dstat_No_new_access" => "������� ����� �������",
"dstat_Visible_accesses" => "��������� ���������",
"dstat_green_rows" => "������� ������",
"dstat_blue_rows" => "����� ������",
"dstat_red_rows" => "������� ������",
"dstat_last_visit" => "���������� ����",
"dstat_robots" => "������",

// Global stats words
"gstat_Accesses" => "����������",
"gstat_Total_visits" => "����� �����������",
"gstat_Total_unique" => "����� ����������",
"gstat_New_visits" => "����� �����������",
"gstat_New_unique" => "����� ����������",
"gstat_Blacklisted" => "�� �����������",
"gstat_Operating_systems" => "������� %d ��",
"gstat_Browsers" => "������� %d ���������",
"gstat_n_first_extensions" => "������� %d ������� ������� ������",
"gstat_Robots" => "������� %d �������",
"gstat_n_first_pages" => "������� %d ���������� �������",
"gstat_n_first_origins" => "������� %d ����������",
"gstat_Total" => "�����",
"gstat_Not_specified" => "�� ����������",

// Time stats words
"tstat_Su" => "��",
"tstat_Mo" => "��",
"tstat_Tu" => "��",
"tstat_We" => "��",
"tstat_Th" => "��",
"tstat_Fr" => "��",
"tstat_Sa" => "��",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Apr",
"tstat_May" => "May",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Oct",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dec",
"tstat_Last_day" => "�������",
"tstat_Last_week" => "�� ���� ������",
"tstat_Last_month" => "� ���� ������",
"tstat_Last_year" => "� ���� ���� (���������)",

// Configuration page words and sentences
"config_Variable_name" => "��� ����������",
"config_Variable_value" => "�������� ����������",
"config_Explanations" => "���������",

"config_bbc_mainsite" =>
 "URL ������ �����.<br>
���� �� ��������, �� URL �� �������� � ������������� ������ ���� ������� bbclone.<br>
<br>
<i>��������:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"����������, ����� �� ���������� ������ show_config.php ��� ���",

"config_bbc_titlebar" =>
"���������, ������� ����� ������������ � ������������� ������ ����
������� bbclone.<br>

����������� ����������������� �������:<br>
<ul>
<li>%SERVER:&nbsp; ��� �������,
<li>%DATE: ������� ����.
</ul>
���� HTML ���� ��������.",

"config_bbc_language" =>
"����, ������� �� ������ ������������. �� ��������� - ����������.<br>
������ � ��������� ������ �� ������ �������
<a href=\"http://bbclone.de\">������ download</a> ����� BBClone.",

"config_bbc_maxtime" =>
"������ ������� (� ��������) ����� 2�� ������� ����������� � ���������� ����������� IP/Agent.<br>
�������� �� ���������: 1800 ���.",

"config_bbc_maxvisible" =>
"������� ��������� �������� � ��������� ����������?
�� ��������� 100. �������� 500.",

"config_bbc_maxos" =>
"������� �� �������� � ����� ����������?",

"config_bbc_maxbrowser" =>
"������� ��������� �������� � ����� ����������?",

"config_bbc_maxextension" =>
"������� ���������� �������� � ����� ����������?",

"config_bbc_maxrobot" =>
"������� ������� �������� � ����� ����������?",

"config_bbc_maxpage" =>
"������� ������� �������� � ����� ����������?",

"config_bbc_maxorigin" =>
"������� ���������� �������� � ����� ����������?",

"config_bbc_ignoreip" =>
"����� IP ������ (��� �������) �� ������ ������������?<br>
<i>������:</i> IP ����� ��� ������� , ������ IP ����� ��� �������<br>
���������� ������� \",\" \"local\" ����� ������ IP.",


"config_bbc_ignore_refer" =>
"���� �� ������ ��������� ������, � �� ������ ����� ��� ����������� 
� ��������, �������� �� �����. ������ � ������ ������������� � ���� ����������
����� ��������� �� �������������. ������:<br>
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";",

"config_bbc_own_refer" =>
"���� ���� ���� ����������, ��� ���������� ��������� 
� ������� �� ������� ����������� ������ ����� bbclone
����� �������� � ������ ����������� ��� 
http://www.���-������.com/
������������ ��� �������, ���� �� �� ������ ���� bbclone 
��������� ������ �� ��������� �������� � ���������� 
������ ����� (�������� �� ����������� ������������).",

"config_bbc_no_string" =>
"BBClone ������� �����������, ����������� ������� 
��������� �������, � ������ HTML ���� ������ 
�� ����� �������. ����� (����� ������) ���� ��� (�����������) 
�� ��������� �������� ��������� ���������� HTML DOCTYPE
� ���p����� �������� � cookies.
���������� ������������ ����������� ��� �������������� 
��������� BBClone ���������� \$BBC_NO_STRING = 0.<br>
�� ��� ���������� ������ �����, ����� �� ��������� 
��������� � ��������� BBClone, ������������� �������� �������� ����� ��
\$BBC_NO_STRING = 1",

"config_bbc_detailed_stat_fields" =>
"���������� \$BBC_DETAILED_STAT_FIELDS ���������� �������, �������
����� ���������� � show_detailed.php.<br>
��������� �������� ��������:<br>
<ul>
<li>\"id\" - ���������� ����� ����������, </li>
<li>\"time\" - ����� ���������,</li>
<li>\"visits\" - ���-�� ������������� �������, </li>
<li>\"dns\" - ����� ���������� ����������, </li>
<li>\"referer\" - ��� ����� � �������� � ��� ������ ����������, </li>
<li>\"os\" - ������������ �������,</li>
<li>\"browser\" - �������, </li>
<li>\"ext\" - ����� ������� ������</li>
</ul>
����� ������� �������� ��������.<br>
�������������� ������� ����� ��������� �� �������� ����������.<br>
<br>
<i>�������:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"����� �� ������ ���������� ����� ������������ �� �������� ��������.
��������� �������� ���������:\"left\", \"right\", \"center\" ",

"config_bbc_title_size" =>
"������ ������ ��������� �� 0 (���������) �� 6 (�������)",

"config_bbc_subtitle_size" =>
"������ ������ ������������ �� 0 (���������) �� 6 (�������)",

"config_bbc_text_size" =>
"������ ������ ������������� ������ �� 0 (���������) �� 6 (�������)"

);
?>