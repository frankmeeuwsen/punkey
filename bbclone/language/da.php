<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/da.php,v 1.15 2004/02/15 19:39:12 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: da.php
* Summary: contains a danish translation table
* Author: Jonathan Fromer, jf(at)sof.dk
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d-m-Y",

// Global translation
"global_bbclone_copyright" => "The BBClone team - Licensed under the",
"global_yes" => "ja",
"global_no" => "nej",

// The error messages
"error_cannot_see_config" =>
"Du har ikke tilladelse til at se BBClone konfigurationen p&aring; denne server.",

// Address Extensions (see lib/extension.php)
"ext_oth" => "Andre", "ext_com" => "Kommerciel",
"ext_net" => "Netv&aelig;rk","ext_edu" => "Uddannelse",
"ext_biz" => "Business","ext_info" => "Information",
"ext_jp" => "Japan", "ext_us" => "De Forenede Stater",
"ext_uk" => "Storbritannien","ext_de" => "Tyskland",
"ext_mil" => "De Forenede Staters milit&aelig;r", "ext_ca" => "Canada",
"ext_it" => "Italien", "ext_au" => "Australien",
"ext_org" => "Organisationer", "ext_nl" => "Holland",
"ext_fr" => "Frankrig", "ext_tw" => "Taiwan",
"ext_gov" => "De Forenede Staters regering", "ext_fi" => "Finland",
"ext_br" => "Brasilien","ext_se" => "Sverige",
"ext_es" => "Spanien", "ext_no" => "Norge",
"ext_mx" => "Mexico", "ext_kr" => "Korea",
"ext_ch" => "Schweiz", "ext_dk" => "Danmark",
"ext_be" => "Belgien", "ext_at" => "&Oslash;strig",
"ext_nz" => "New Zealand","ext_ru" => "Rusland",
"ext_pl" => "Polen", "ext_za" => "Sydafrika",
"ext_unknown" => "Ukendt", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapore",
"ext_arpa" => "Fejl", "ext_cz" => "Tjekkiet",
"ext_hu" => "Ungarn", "ext_hk" => "Hongkong",
"ext_pt" => "Portugal", "ext_tr" => "Tyrkiet",
"ext_gr" => "Gr&aelig;kenland","ext_cn" => "Kina",
"ext_ie" => "Irland", "ext_my" => "Malaysia",
"ext_th" => "Thailand", "ext_cl" => "Chile",
"ext_co" => "Colombia", "ext_is" => "Island",
"ext_uy" => "Uruguay", "ext_ee" => "Estland",
"ext_in" => "Indien", "ext_ua" => "Ukraine",
"ext_sk" => "Slovakiet","ext_ro" => "Rum&aelig;nien",
"ext_ae" => "Forenede Arabiske Emirater", "ext_id" => "Indonesien",
"ext_su" => "Sovjetunionen", "ext_si" => "Slovenien",
"ext_hr" => "Kroatien", "ext_ph" => "Filippinerne",
"ext_lv" => "Letland", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgarien","ext_lt" => "Litauen",
"ext_yu" => "Jugoslavien","ext_lu" => "Luxemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica","ext_int" => "Internationale organisationer",
"ext_do" => "Dominikanske Republik", "ext_cy" => "Cypern",
"ext_pk" => "Pakistan", "ext_cc" => "Kokos&oslash;erne",
"ext_tt" => "Trinidad og Tobago", "ext_eg" => "Egypten",
"ext_lb" => "Libanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudi-Arabien",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kirgizistan",
"ext_cx" => "Jule&oslash;en","ext_pa" => "Panama",
"ext_gt" => "Guatemala","ext_bw" => "Botswana",
"ext_mk" => "Makedonien","ext_gl" => "Gr&oslash;nland",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "F&aelig;r&oslash;erne",
"ext_ac" => "Ascenci&oacute;n","ext_pr" => "Puerto Rico",
"ext_am" => "Armenien", "ext_pf" => "Fransk Polynesien",
"ext_ge" => "Georgien", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua","ext_by" => "Hviderusland",
"ext_sv" => "El Salvador","ext_ma" => "Marokko",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnien-Hercegovina", "ext_om" => "Oman",
"ext_jo" => "Jordan", "ext_ir" => "Iran",
"ext_st" => "Sao Tom� og Pr�ncipe", "ext_vi" => "Amerikanske Jomfru&oslash;erne",
"ext_ci" => "Elfenbenskysten","ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein","ext_ky" => "Cayman &oslash;erne",
"ext_gp" => "Guadeloupe","ext_mg" => "Madagaskar",
"ext_gi" => "Gibraltar","ext_sm" => "San Marino",
"ext_as" => "Amerikansk Samoa","ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Heard & Mc Donald &oslash;erne", "ext_fm" => "Mikronesien",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Nua Guinea",
"ext_bz" => "Belize", "ext_sh" => "Saint Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldiverne",
"ext_nc" => "Ny Caledonien","ext_ag" => "Antigua og Barbuda",
"ext_uz" => "Uzbekistan","ext_tj" => "Tadzjikistan",
"ext_sb" => "Salamon&oslash;erne", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodia", "ext_tc" => "Turks & Caicos &oslash;erne",
"ext_tf" => "Franske Sydlige Territorier", "ext_az" => "Azerbajdzjan",
"ext_dm" => "Dominica", "ext_mz" => "Mo�ambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongoliet","ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Fransk Guyana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albanien", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk &oslash;en", "ext_io" => "British Indian Ocean Territory",
"ext_gs" => "Georgia & Sandwich &oslash;erne", "ext_ye" => "Yemen",
"ext_an" => "Hollandske Antiller", "ext_aq" => "Antarktis",
"ext_tn" => "Tunisien", "ext_ck" => "Cook &oslash;erne",
"ext_ls" => "Lesotho", "ext_et" => "Etiopien",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Britiske Jomfru&oslash;erne", "ext_vn" => "Vietnam",
"ext_mr" => "Mauretanien","ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comorerne","ext_dz" => "Algeriet",
"ext_im" => "Isle of Man","ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Storbritannien","ext_bs" => "Bahamas",
"ext_va" => "Vatikanstaten","ext_lc" => "Saint Lucia",
"ext_cd" => "Demokratiske Republik Congo", "ext_gm" => "Gambia",
"ext_mp" => "Northern Mariana &oslash;erne", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Cameroun", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libyen",
"ext_cf" => "Centralafrikanske Republik", "ext_mm" => "Myanmar",
"ext_td" => "Tchad", "ext_iq" => "Irak",
"ext_kn" => "Saint Christopher og Nevis", "ext_sc" => "Seychellerne",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Kap Verde","ext_mh" => "Marshall&oslash;erne",
"ext_pm" => "Saint Pierre og Miquelon", "ext_so" => "Somalia",
"ext_vc" => "Saint Vincent og Grenadinerne", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "R�union", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet &oslash;en",
"ext_fk" => "Falklands&oslash;erne", "ext_gq" => "&Aelig;kvatorial Guinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard og Jan Mayen",
"ext_sr" => "Surinam", "ext_sy" => "Syrien",
"ext_tp" => "&Oslash;ttimor", "ext_um" => "De forenede Staters mindre &oslash;er i Oceanien og Vestindien",
"ext_wf" => "Wallis og Futuna &oslash;erne", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "IP",

// Miscellaneoux translations
"misc_other" => "Andre",
"misc_unknown" => "Ukendt",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Hoved Side",
"navbar_Configuration" => "Konfiguration",
"navbar_Global_Stats" => "Generel Statistik",
"navbar_Detailed_Stats" => "Detaljeret Statistik",
"navbar_Time_Stats" => "Historik",
"navbar_Link_Stats" => "Link Statistik",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Tidspunkt",
"dstat_Visits" => "Bes&oslash;g",
"dstat_Extension" => "Topdom&aelig;ne",
"dstat_DNS" => "V&aelig;rtsnavn",
"dstat_From" => "Fra",
"dstat_OS" => "OS",
"dstat_Browser" => "Browser",
"dstat_New_access" => "Nye bes&oslash;g",
"dstat_Elapsed_time" => "Forl&oslash;bet tid",
"dstat_No_new_access" => "Ingen nye bes&oslash;g",
"dstat_Visible_accesses" => "Synlige bes&oslash;g",
"dstat_green_rows" => "gr&oslash;nne r&aelig;kker",
"dstat_blue_rows" => "bl&aring; r&aelig;kker",
"dstat_red_rows" => "r&oslash;de r&aelig;kker",
"dstat_last_visit" => "senest bes&oslash;gt",
"dstat_robots" => "robotter",

// Global stats words

"gstat_Accesses" => "Bes&oslash;gende",
"gstat_Total_visits" => "Samlede hits",
"gstat_Total_unique" => "Samlede unikke hits",
"gstat_New_visits" => "Nye hits",
"gstat_New_unique" => "Nye unikke hits",
"gstat_Blacklisted" => "Blacklistet",
"gstat_Operating_systems" => "Top %d Operativsystemer",
"gstat_Browsers" => "Top %d Browsere",
"gstat_n_first_extensions" => "%d f&oslash;rste topdom&aelig;ner",
"gstat_Robots" => "Top %d Robotter",
"gstat_n_first_pages" => "%d f&oslash;rste sider",
"gstat_n_first_origins" => "%d f&oslash;rste oprindelser",
"gstat_Total" => "I alt",
"gstat_Not_specified" => "Ikke specificeret",

// Time stats words
"tstat_Su" => "S&oslash;n",
"tstat_Mo" => "Man",
"tstat_Tu" => "Tir",
"tstat_We" => "Ons",
"tstat_Th" => "Tor",
"tstat_Fr" => "Fri",
"tstat_Sa" => "L&oslash;r",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Apr",
"tstat_May" => "Maj",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Okt",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dec",

"tstat_Last_day" => "Sidste d&oslash;gn",
"tstat_Last_week" => "Sidste uge",
"tstat_Last_month" => "Sidste m&aring;ned",
"tstat_Last_year" => "Sidste &aring;r",

// Configuration page words and sentences

"config_Variable_name" => "Variabel navn",
"config_Variable_value" => "Variabel v&aelig;rdi",
"config_Explanations" => "Forklaring",

"config_bbc_mainsite" =>
"URL'en for din webside.<br>
Hvis du ikke definerer denne vil der ikke vises et link til din hovedside i navigationsbaren p� nogen af BBClone siderne.<br>
<br>
<i>Eksempel:</i><br>
\$BBC_MAINSITE = \"http://www.minside.dk/et_eller_andet/\".",

"config_bbc_show_config" =>
"Bestemmer om show_config.php skal vises eller ej",

"config_bbc_titlebar" =>
"Titlen p&aring; alle BBClone siderne.<br>
Flg. makroer kan bruges:<br>
<ul>
<li>%SERVER: servernavn,
<li>%DATE: nuv&aelig;rende dato.
</ul>
HTML tags m&aring; ogs&aring; bruges.",

"config_bbc_language" =>
"Det sprog du gerne vil bruge til BBClone. Som standard er valgt engelsk.<br>
For at finde ud af hvilke sprog der kan v&aelig;lges iblandt, se 
<a href=\"http://bbclone.de\">download sektionen</a> p&aring; BBClone 
websitet.",

"config_bbc_maxtime" =>
"Tidsinterval (i sekunder) mellem 2 opt&aelig;llinger fra samme IP/AGENT.<br>
Standard v&aelig;rdien er: 1800s (30 minutter)",

"config_bbc_maxvisible" =>
"Hvor mange individuelle punkter vil du se i den detaljerede statistik?<br>
Standard er 100 og lad v&aelig;re med at s&aelig;tte den h&oslash;jere end 500 (hvilket alligevel er ubrugelig)",

"config_bbc_maxos" =>
"Hvor mange OS'er vil du se klassificeret i den generelle statistik?",

"config_bbc_maxbrowser" =>
"Hvor mange browsere vil du se klassificeret i den generelle statistik?",

"config_bbc_maxextension" =>
"Hvor mange topdom&aelig;ner vil du se klassificeret i den generelle statistik?",

"config_bbc_maxrobot" =>
"Hvor mange robotter vil du se klassificeret i den generelle statistik?",

"config_bbc_maxpage" =>
"Hvor mange sider vil du se klassificeret i den generelle statistik?",

"config_bbc_maxorigin" =>
"Hvor mange oprindelser vil du se klassificeret i den generelle statistik?",

"config_bbc_ignoreip" =>
"Hvilke IP adresser (eller subnet) vil du ignorere?<br>
<i>Format:</i> &lt;IP adresse eller subnet&gt;, &lt;en anden IP adresse eller subnet&gt;<br>
Inds&aelig;t et komma \",\" mellem hver IP. Normalv&aelig;rdien er oftest \"lokal\" IP.",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"Variablen \$BBC_DETAILED_STAT_FIELDS bestemmer hvilke felter der visses under show_detailed.php.<br>
Mulige feltnavne er:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extension)<br>
R&aelig;kkef&oslash;lgen af feltnavnene er vigtig.<br>
Ikke-eksisterende felter vil mangle p� statestiksiden.<br>
<br>
<i>Eksempler:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, ext\"<br>",

"config_bbc_general_align_style" =>
"Med denne variabel kan du s&aelig;tte align style for hovedstatistiksidens indhold. 
Mulige v&aelig;rdier er \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"St&oslash;rrelsen af titlerne fra 0 (mindst) til 6 (st&oslash;rst)",

"config_bbc_subtitle_size" =>
"St&oslash;rrelsen af undertitlerne fra 0 (mindst) til 6 (st&oslash;rst)",

"config_bbc_text_size" =>
"St&oslash;rrelsen af almindeligt tekst fra 0 (mindst) til 6 (st&oslash;rst)"

);
?>