<?php
/*
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/language/fi.php,v 1.5 2004/02/15 19:39:12 joku Exp $
 *
 * Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
 * Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
 *
 * File: fi.php
 * Summary: contain a Finnish translation table
 * Description: Finnish translation. Joidenkin maiden nimet voi olla hiukka hatusta. 
 * Author: Ville Pohjanheimo (vpohjanheimo on hotmail)
 */

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "Y/m/d",

// Global translation
"global_bbclone_copyright" => "BBclone tiimi - Lisenssi on",
"global_yes" => "kyll�",
"global_no" => "ei",

// The error messages
"error_cannot_see_config" =>
"Sinulle ei ole annettu oikeuksia tarkastella BBClonen asetuksia t�ll� palvelimella.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Muu", "ext_com" => "Kaupallinen",
"ext_net" => "Verkko", "ext_edu" => "Oppilaitos",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japani", "ext_us" => "Yhdysvallat",
"ext_uk" => "Iso-Britannia","ext_de" => "Saksa",
"ext_mil" => "USAn armeija","ext_ca" => "Kanada",
"ext_it" => "Italia", "ext_au" => "Australia",
"ext_org" => "Organisaatio", "ext_nl" => "Alankomaat",
"ext_fr" => "Ranska", "ext_tw" => "Taiwan",
"ext_gov" => "Hallitus","ext_fi" => "Suomi",
"ext_br" => "Brasilia", "ext_se" => "Ruotsi",
"ext_es" => "Espanja", "ext_no" => "Norja",
"ext_mx" => "Meksiko", "ext_kr" => "Korea",
"ext_ch" => "Sweitsi", "ext_dk" => "Tanska",
"ext_be" => "Belgia", "ext_at" => "It�valta",
"ext_nz" => "Uusi Seelanti","ext_ru" => "Ven�j�",
"ext_pl" => "Puola", "ext_za" => "Etel�-Afrikka",
"ext_unknown" => "Tuntematon","ext_ar" => "Argentiina",
"ext_il" => "Israel", "ext_sg" => "Singapori",
"ext_arpa" => "Arpanet", "ext_cz" => "Tsekki",
"ext_hu" => "Unkari", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugali","ext_tr" => "Turkki",
"ext_gr" => "Kreikka", "ext_cn" => "Kiina",
"ext_ie" => "Irlanti", "ext_my" => "Malesia",
"ext_th" => "Thaimaa", "ext_cl" => "Chile",
"ext_co" => "Kolumbia", "ext_is" => "Islanti",
"ext_uy" => "Uruguay", "ext_ee" => "Eesti",
"ext_in" => "Intia", "ext_ua" => "Ukraina",
"ext_sk" => "Slovakia", "ext_ro" => "Romania",
"ext_ae" => "Yhdistyneet Arabi Emiraatit","ext_id" => "Indonesia",
"ext_su" => "Neuvostoliitto","ext_si" => "Slovenia",
"ext_hr" => "Kroatia", "ext_ph" => "Philippiinit",
"ext_lv" => "Latvia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgaria", "ext_lt" => "Liettua",
"ext_yu" => "Jugoslavia", "ext_lu" => "Luxemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Kansainv�linen",
"ext_do" => "Dominikaaninen Tasavalta","ext_cy" => "Kypros",
"ext_pk" => "Pakistan", "ext_cc" => "Cocos (Keeling) Saaret",
"ext_tt" => "Trinidad ja Tobago", "ext_eg" => "Egypti",
"ext_lb" => "Libanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakhstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudi Arabia",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Joulusaaret", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Makedonia", "ext_gl" => "Gr�nlanti",
"ext_ec" => "Ecuadori", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "F�rsaaret",
"ext_ac" => "Ascension Saaret", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenia", "ext_pf" => "Ranskan Polynesia",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Marokko",
"ext_ke" => "Kenia", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepali",
"ext_bt" => "Bhutan", "ext_sz" => "Swazimaa",
"ext_ba" => "Bosnia Herzegowina","ext_om" => "Oman",
"ext_jo" => "Jordania", "ext_ir" => "Iran",
"ext_st" => "Sao Tome ja Principe", "ext_vi" => "Neitsyt Saaret (U.S.)",
"ext_ci" => "Norsunluurannikko","ext_jm" => "Jamaika",
"ext_li" => "Liechtenstein", "ext_ky" => "Cayman Saaret",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltari", "ext_sm" => "San Marino",
"ext_as" => "Amerikan Samoa", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monako", "ext_sn" => "Senegal",
"ext_hm" => "Heardin and Mc Donaldin Saaret", "ext_fm" => "Micronesia",
"ext_fj" => "Fiji", "ext_cu" => "Kuuba",
"ext_rw" => "Ruanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua New Guinea",
"ext_bz" => "Belize", "ext_sh" => "St. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldiivit",
"ext_nc" => "Uusi Caledonia", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Solomon Islands", "ext_bf" => "Burkina Faso",
"ext_kh" => "Kamputsea", "ext_tc" => "Turks and Caicos Saaret",
"ext_tf" => "Ranskan etel�iset territoriot", "ext_az" => "Azerbaijan",
"ext_dm" => "Dominica", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Ranskan Guiana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk Saaret", "ext_io" => "Britannian Intian valtameren territoriot",
"ext_gs" => "Etel�-Georgia ja Etel�-Sandwichin saaret", "ext_ye" => "Jemen",
"ext_an" => "Alankomaiden Antilles","ext_aq" => "Etel�manner",
"ext_tn" => "Tunisia", "ext_ck" => "Cook Saaret",
"ext_ls" => "Lesotho", "ext_et" => "Etiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Neitsyt Saaret (Britannia)","ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Man-saari", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Yhdistyneet kuningaskunnat","ext_bs" => "Bahamas",
"ext_va" => "Vatikaani", "ext_lc" => "Saint Lucia",
"ext_cd" => "Kongo", "ext_gm" => "Gambia",
"ext_mp" => "Pohjoiset Marianan saaret", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Kameron", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libya",
"ext_cf" => "Keskiafrikan tasavalta", "ext_mm" => "Myanmar",
"ext_td" => "Chad", "ext_iq" => "Iraq",
"ext_kn" => "Saint Kitts ja Nevis", "ext_sc" => "Seychellit",
"ext_cg" => "Kongo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistani",
"ext_cv" => "Cape Verde", "ext_mh" => "Marshall Saaret",
"ext_pm" => "St. Pierre ja Miquelon", "ext_so" => "Somalia",
"ext_vc" => "St. Vincent ja Grenadiinit", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet saari",
"ext_fk" => "Falklandit (Malvinas)", "ext_gq" => "P�iv�ntasaajan Guinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard ja Jan Mayen Islands",
"ext_sr" => "Suriname", "ext_sy" => "Syyria",
"ext_tp" => "It�-Timor", "ext_um" => "Yhdysvaltojen Minor Outlying Islands",
"ext_wf" => "Wallis ja Futuna Islands", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numeerinen",

// Miscellaneoux translations
"misc_other" => "Muu",
"misc_unknown" => "Tuntematon",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Seurattu sivusto",
"navbar_Configuration" => "Asetukset",
"navbar_Global_Stats" => "Kokonaistilastot",
"navbar_Detailed_Stats" => "K�yntikertakoht. tilastot",
"navbar_Time_Stats" => "Aikatilastot",
"navbar_Link_Stats" => "Linkkitilastot",

// Detailed stats words
"dstat_ID" => "Nro",
"dstat_Time" => "Aika",
"dstat_Visits" => "Lkm",
"dstat_Extension" => "P��te",
"dstat_DNS" => "Osoite",
"dstat_From" => "L�hdesivu",
"dstat_OS" => "K�ytt�j�rjestelm�",
"dstat_Browser" => "Selain",
"dstat_New_access" => "Uusia vierailuja",
"dstat_Elapsed_time" => "Kulunut aika",
"dstat_No_new_access" => "Ei uusia vierailuja",
"dstat_Visible_accesses" => "Listassa vierailuja",
"dstat_green_rows" => "vihre� rivi",
"dstat_blue_rows" => "sininen rivi",
"dstat_red_rows" => "punainen rivi",
"dstat_last_visit" => "viimeinen vierailu",
"dstat_robots" => "robotteja",

// Global stats words

"gstat_Accesses" => "Vierailuja",
"gstat_Total_visits" => "Yhteens�",
"gstat_Total_unique" => "Eri osoitteista",
"gstat_New_visits" => "Uusia",
"gstat_New_unique" => "Uusista osoitteista",
"gstat_Blacklisted" => "Mustalla listalla",
"gstat_Operating_systems" => "Top %d k�ytt�j�rjestelm��",
"gstat_Browsers" => "Top %d selainta",
"gstat_n_first_extensions" => "Top %d p��tett�",
"gstat_Robots" => "Top %d robottia",
"gstat_n_first_pages" => "Top %d sivua",
"gstat_n_first_origins" => "Top %d viittaavaa sivua",
"gstat_Total" => "Yhteens�",
"gstat_Not_specified" => "Ei m��ritelty",

// Time stats words
"tstat_Su" => "Su",
"tstat_Mo" => "Ma",
"tstat_Tu" => "Ti",
"tstat_We" => "Ke",
"tstat_Th" => "To",
"tstat_Fr" => "Pe",
"tstat_Sa" => "La",

"tstat_Jan" => "Tam",
"tstat_Feb" => "Hel",
"tstat_Mar" => "Maa",
"tstat_Apr" => "Huh",
"tstat_May" => "Tou",
"tstat_Jun" => "Kes",
"tstat_Jul" => "Hei",
"tstat_Aug" => "Elo",
"tstat_Sep" => "Syy",
"tstat_Oct" => "Lok",
"tstat_Nov" => "Mar",
"tstat_Dec" => "Jou",

"tstat_Last_day" => "Viimeisen vuorokauden aikana",
"tstat_Last_week" => "Viimeisen viikon aikana",
"tstat_Last_month" => "Viimeisen kuukauden aikana",
"tstat_Last_year" => "Viimeisen vuoden aikana",

// Configuration page words and sentences

"config_Variable_name" => "Muuttuja",
"config_Variable_value" => "Muuttujan arvo",
"config_Explanations" => "Kuvaus",

"config_bbc_mainsite" =>
"Seuratun sivuston osoite.<br>
Jos t�m� on tyhj�, bbclonen sivuille ei tule sivun osoitetta ja Seurattu sivusto -linkki ei toimi.<br>
<br>
<i>Esimerkiksi:</i><br>
\$BBC_MAINSITE = \"http://www.sivu.fi/jossain/\".",

"config_bbc_show_config" =>
"M��rittelee ovatko show_config.php:n asetukset julkisia.",

"config_bbc_titlebar" =>
"Kaikilla bbclonen luomilla sivuilla n�kyv� otsikko.<br>
Seuraavia muuttujia voi k�ytt��:<br>
<ul>
<li>%SERVER : palvelimen nimi,
<li>%DATE : p�iv�m��r�.
</ul>
HTML-koodi on my�s ok.",

"config_bbc_language" =>
"Mill� kielell� tilastot n�ytet��n. Oletus on englanti.<br>
Lis�tietoja saatavilla olevista kielist� l�yd�t BBClonen virallisten sivujen
lataus eli <a href=\"http://bbclone.de\">download osiosta</a>.",

"config_bbc_maxtime" =>
"Yhden IP-osoite/selain -yhdistelm�n vierailujen pienin mahdollinen aikaero (sekuntia), jotta k�ynti
laskettaisin.<br>
Oletuksena on standardin omainen 1800s",

"config_bbc_maxvisible" =>
"Kuinka monta merkint�� n�ytet��n k�yntikertakohtaisissa tilastoissa?<br>
Oletus on 100. �l� aseta sit� suuremmaksi kuin 500 (joka olisikin aika tarpeetonta)",

"config_bbc_maxos" =>
"Kuinka monta k�ytt�j�rjestelm�� listataan kokonaistilastot sivulla?",

"config_bbc_maxbrowser" =>
"Kuinka monta selainta listataan kokonaistilastot sivulla?",

"config_bbc_maxextension" =>
"Kuinka monta p��tett� listataan kokonaistilastot sivulla?",

"config_bbc_maxrobot" =>
"Kuinka monta robottia listataan kokonaistilastot sivulla?",

"config_bbc_maxpage" =>
"Kuinka monta sivua listataan kokonaistilastot sivulla?",

"config_bbc_maxorigin" =>
"Kuinka monta l�hdesivua listataan kokonaistilastot sivulla?",

"config_bbc_ignoreip" =>
"IP-osoitteet tai aliverkot, joita ei lasketa.<br>
<i>Muoto:</i> &lt;IP-osoite tai palvelinosoite&gt;, &lt;seuraava IP-osoite tai palvelinosoite&gt;<br>
Lis�� pilkku \",\" jokaisen osoitteen v�liin. Oletuksena on useimmat \"paikalliset\" osoitteet.",

"config_bbc_ignore_refer" =>
"Jos py�rit�t useampaa palvelinta, etk� halua listata niist� t�lle sivulle tulevia viittauksia, 
voit listata t�h�n noiden palvelinten osoitteet. Viittaukset k�sitell��n \"ei m��riteltyn�\"
ja sivuosumia ei t�m�n takia huku. K�yt� seuraavaa syntaksia:<br />
\$BBC_IGNORE_REFER = \"www.palvelin1.org, toinen.palvelin2.org, viel�kin.palvelin3.org\";<br />
ja niin edelleen.",

"config_bbc_own_refer" =>
"Jos t�m�n valinnan arvo on tosi(1), kaikki t�lt� palvelimelta tulevat viittaukset
n�kyv�t viittauslistassa pelk�ll� osoitteella http://www.omapalvelin.fi/ 
(siis palvelimesi nimell�).
T�m� on k�yt�nn�llist� jos et halua bbclonen linkitt�v�n esim. hallinnollisille
tai muillle mieluummin piilossa pysyville sivuille.",

"config_bbc_no_string" => 
"BBClonen kuittausviesti-kommentti sivun l�hdekoodissa voi h�irit� joitain
foorumi tai sis�ll�nhallinta ohjelmia. Jos sivujasi vaivaavat kummalliset
tyhj�t sivut tai muutama \"header already sent by\" viestit t�m� valinta
voi saada sivusi taas toimintakuntoon.",

"config_bbc_detailed_stat_fields" =>
"K�yntikertakohtaisten tilastojen sivun eli show_detailed.php:n sarakkeet.<br>
Mahdolliset sarakkeet:<br>

\"id\", \"time\", \"visits\", \"dns\" (palvelimen nimi), \"referer\", \"os\", \"browser\", \"ext\" (p��te)<br>

Sarakkeiden nimien j�rjestys on merkitsev�.<br>

Olemattomien sarakkeiden nimi� ei listata sivulla.<br>
<br>
<i>Esimerkki:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, hostname\"<br>",

"config_bbc_general_align_style" =>
"Sivut voi asemoida joko vasempaan tai oikeaan reunaan tai keskitt��.
Muuttujan arvo voi olla \"left\", \"right\" tai \"center\"",

"config_bbc_title_size" =>
"Otsikon suhteellinen koko: 0 (pienin), 6 (suurin)",

"config_bbc_subtitle_size" =>
"Alaotsikoiden suhteellinen koko: 0 (pienin), 6 (suurin)",

"config_bbc_text_size" =>
"Leip�tekstin suhteellinen koko:  0 (pienin), 6 (suurin)"

);
?>