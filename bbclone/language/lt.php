<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/lt.php,v 1.12 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: lt.php
* Summary: contains a lithuanian translation table
* Author: Vilius Simonaitis <maumas98@yahoo.com>
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "windows-1257",

// Date format (used with date() )
"global_date_format" => "Y/m/d",

// Global translation
"global_bbclone_copyright" => "BBClone komanda - Licenzijuota pagal",
"global_yes" => "taip",
"global_no" => "ne",

// The error messages
"error_cannot_see_config" =>
"Jums n�ra leista matyti BBClone konfig�racijos �iame serveryje.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Kita", "ext_com" => ".com",
"ext_net" => ".net", "ext_edu" => ".edu",
"ext_biz" => "Verslas", "ext_info" => "Informacin�s Tarnybos",
"ext_jp" => "Japonija", "ext_us" => "JAV",
"ext_uk" => "Jungtin�s Karalyst�s", "ext_de" => "Vokietija",
"ext_mil" => "Jungtini� Taut� karin� tarnyba", "ext_ca" => "Kanada",
"ext_it" => "Italija", "ext_au" => "Australija",
"ext_org" => ".org", "ext_nl" => "Netherlands",
"ext_fr" => "Pranc�zija", "ext_tw" => "Taivan",
"ext_gov" => "Vyriausyb� (.gov)", "ext_fi" => "Suomija",
"ext_br" => "Brazilija", "ext_se" => "�vedija",
"ext_es" => "Ispanija", "ext_no" => "Norvegija",
"ext_mx" => "Meksika", "ext_kr" => "Kor�ja",
"ext_ch" => "�veicarija", "ext_dk" => "Danija",
"ext_be" => "Belgija", "ext_at" => "Austrija",
"ext_nz" => "Naujoji Zelandija", "ext_ru" => "Rusijos federacija",
"ext_pl" => "Lenkija", "ext_za" => "Piet� Afrika",
"ext_unknown" => "Ne�inoma", "ext_ar" => "Argentina",
"ext_il" => "Izraelis", "ext_sg" => "Singap�ras",
"ext_arpa" => "Klaidos", "ext_cz" => "�ekijos respublika",
"ext_hu" => "Vangrija", "ext_hk" => "Hong-Kongas",
"ext_pt" => "Portugalija", "ext_tr" => "Turkija",
"ext_gr" => "Graikija", "ext_cn" => "Kinija",
"ext_ie" => "Airija", "ext_my" => "Malaizija",
"ext_th" => "Tailandas", "ext_cl" => "�il�",
"ext_co" => "Kolombija", "ext_is" => "Islandija",
"ext_uy" => "Urugvajus", "ext_ee" => "Estija",
"ext_in" => "Indija", "ext_ua" => "Ukraina",
"ext_sk" => "Slovakija", "ext_ro" => "Rumunija",
"ext_ae" => "Jungtiniai arab� emiratai", "ext_id" => "Indonezija",
"ext_su" => "Saviet� S�junga", "ext_si" => "Slov�nija",
"ext_hr" => "Kroatija", "ext_ph" => "Filipinai",
"ext_lv" => "Latvija", "ext_ve" => "Venesuela",
"ext_bg" => "Bulgarija", "ext_lt" => "Lietuva",
"ext_yu" => "Jugoslavija", "ext_lu" => "Liuksemburgas",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Kosta Rika", "ext_int" => "Tarptautin�s Organizacijos",
"ext_do" => "Dominikos Respublika", "ext_cy" => "Kipras",
"ext_pk" => "Pakistanas", "ext_cc" => "Kokoso (Keeling) Salos",
"ext_tt" => "Trinidadas ir Tobagas", "ext_eg" => "Egiptas",
"ext_lb" => "Lebanonas", "ext_kw" => "Kuveitas",
"ext_to" => "Tongas", "ext_kz" => "Kazakstanas",
"ext_na" => "Namibija", "ext_mu" => "Mauricijus",
"ext_bm" => "Bermudai", "ext_sa" => "Saudo Arabija",
"ext_zw" => "Zimbabv�", "ext_kg" => "Kirgistanas",
"ext_cx" => "Kal�d� Salos", "ext_pa" => "Panama",
"ext_gt" => "Gvatemala", "ext_bw" => "Botsvana",
"ext_mk" => "Makedonija", "ext_gl" => "Greenland'as",
"ext_ec" => "Ekvadoras", "ext_lk" => "�ri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paragvajus",
"ext_bo" => "Bolivija", "ext_bn" => "Brun�jus",
"ext_mt" => "Malta", "ext_fo" => "Faraon� Salos",
"ext_ac" => "Prisik�limo Sala", "ext_pr" => "Puerto Rikas",
"ext_am" => "Armenija", "ext_pf" => "Pranc�z� Polinezija",
"ext_ge" => "Georgija", "ext_bh" => "Bachrainas",
"ext_ni" => "Nikaragva", "ext_by" => "Belarus",
"ext_sv" => "El Salvadoras", "ext_ma" => "Marokas",
"ext_ke" => "Kenia", "ext_ad" => "Andora",
"ext_zm" => "Zambija", "ext_np" => "Nepalas",
"ext_bt" => "Bhutanas", "ext_sz" => "Swaziland'as",
"ext_ba" => "Bosnija and Hercogovina", "ext_om" => "Omanas",
"ext_jo" => "Jordanija", "ext_ir" => "Iranas",
"ext_st" => "Sao Tome and Principe", "ext_vi" => "Virginijos Salos (U.S.)",
"ext_ci" => "Ivory Coast", "ext_jm" => "Jamaika",
"ext_li" => "Liechten�teinas", "ext_ky" => "Caiman� Salos",
"ext_gp" => "Gvadelup�", "ext_mg" => "Madagaskaras",
"ext_gi" => "Gibraltaras", "ext_sm" => "San Marinas",
"ext_as" => "Amerikos Samoa", "ext_tz" => "Tanzanija",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistanas",
"ext_mc" => "Monakas", "ext_sn" => "Senegalas",
"ext_hm" => "Heard ir Mc Donald Salos", "ext_fm" => "Mikronezija",
"ext_fj" => "Fid�i", "ext_cu" => "Kuba",
"ext_rw" => "Rovanda", "ext_mq" => "Martinika",
"ext_ai" => "Angila", "ext_pg" => "Papua Naujoji Gvin�ja",
"ext_bz" => "Beliz�", "ext_sh" => "�v. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldivai",
"ext_nc" => "Naujoji Caledonija", "ext_ag" => "Antigva ir Barbuda",
"ext_uz" => "Uzbekistanas", "ext_tj" => "Taikistanas",
"ext_sb" => "Saliamono Salos", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambod�a", "ext_tc" => "Turks and Caicos Islands",
"ext_tf" => "Pranc�z� Piet� teritorijos", "ext_az" => "Azerbaid�ianas",
"ext_dm" => "Dominika", "ext_mz" => "Mozambikas",
"ext_mo" => "Makau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolija", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Nigeris", "ext_gf" => "Pranc�z� Gujana",
"ext_gu" => "Guama", "ext_hn" => "Honduras",
"ext_al" => "Albanija", "ext_gh" => "Gana",
"ext_nf" => "Norfolk'o Sala", "ext_io" => "Brit� Indijos Vandenyno teritorijos",
"ext_gs" => "Piet� Georgija ir Piet� Buterbrodo Salos", "ext_ye" => "Jemenas",
"ext_an" => "Skandinav� Antilai", "ext_aq" => "Antarktika",
"ext_tn" => "Tunisija", "ext_ck" => "Gegut�s Salos",
"ext_ls" => "Lesotas", "ext_et" => "Etiopija",
"ext_ng" => "Nigerija", "ext_sl" => "Siera Leonas",
"ext_bb" => "Barbadosas", "ext_je" => "D�ersis",
"ext_vg" => "Virginijos Salos (Brit�)", "ext_vn" => "Vietnamas",
"ext_mr" => "Mauritanija", "ext_gy" => "Gujana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Komorosas", "ext_dz" => "Algerija",
"ext_im" => "Vyro sala", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Beninas", "ext_ga" => "Gabonas",
"ext_gb" => "Jungtin�s Karalyst�s", "ext_bs" => "Bahamai",
"ext_va" => "Vatikanas", "ext_lc" => "�ventoji Liucija",
"ext_cd" => "Kongas", "ext_gm" => "Gambija",
"ext_mp" => "�iaurin�s Marijanos Salos", "ext_gw" => "Gvin�ja-Bissau",
"ext_cm" => "Kamer�nas", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libija",
"ext_cf" => "Centrin�s Afrikos Respublika", "ext_mm" => "Myanmar",
"ext_td" => "�iadas", "ext_iq" => "Irakas",
"ext_kn" => "Saint Kitts and Nevis", "ext_sc" => "Sei�eliai",
"ext_cg" => "Kongas", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistanas",
"ext_cv" => "Cape Verde", "ext_mh" => "Mar�alo Salos",
"ext_pm" => "�v. Pierre ir Miquelon", "ext_so" => "Somalia",
"ext_vc" => "�v. Vincent ir Grenadines", "ext_bd" => "Banglade�as",
"ext_gn" => "Gvin�ja", "ext_ht" => "Haitis",
"ext_la" => "Laosas", "ext_lr" => "Liberija",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Island",
"ext_fk" => "Falkland Islands (Malvinas)", "ext_gq" => "Ekvatorin� Gvin�ja",
"ext_sd" => "Sudanas", "ext_sj" => "Svalbard and Jan Mayen Islands",
"ext_sr" => "Surinamas", "ext_sy" => "Sirija",
"ext_tp" => "Ryt� Tim�ras", "ext_um" => "United States Minor Outlying Islands",
"ext_wf" => "Wallis and Futuna Islands", "ext_yt" => "Mayotte",
"ext_zr" => "Zairas", "ext_IP" => "Skaitinis",

// Miscellaneoux translations
"misc_other" => "Kita",
"misc_unknown" => "Ne�inoma",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Titulinis",
"navbar_Configuration" => "Konfig�racija",
"navbar_Global_Stats" => "Globali Statistika",
"navbar_Detailed_Stats" => "Detali Statistika",
"navbar_Time_Stats" => "Laikmatis",
"navbar_Link_Stats" => "Nuorodos",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Laikas",
"dstat_Visits" => "Apsilankymai",
"dstat_Extension" => "I�pl�timas",
"dstat_DNS" => "Hostname'as",
"dstat_From" => "I� kur",
"dstat_OS" => "OS",
"dstat_Browser" => "Nar�ykl�",
"dstat_New_access" => "Nauji apsilankymai",
"dstat_Elapsed_time" => "Elapsed time",
"dstat_No_new_access" => "Nauj� apsilankym� n�ra",
"dstat_Visible_accesses" => "Matoma u�klaus�",
"dstat_green_rows" => "�alios eilut�s",
"dstat_blue_rows" => "m�lynos eilut�s",
"dstat_red_rows" => "raudonos eilut�s",
"dstat_last_visit" => "paskutinis apsilankymas",
"dstat_robots" => "paie�kos sistemos",

// Global stats words

"gstat_Accesses" => "U�klausos",
"gstat_Total_visits" => "Viso apsilankym�",
"gstat_Total_unique" => "Viso unikali�",
"gstat_New_visits" => "Nauji apsilankymai",
"gstat_New_unique" => "Nauji unikal�s",
"gstat_Blacklisted" => "Juodasis s�ra�as",
"gstat_Operating_systems" => "Operacin�s sistemos",
"gstat_Browsers" => "Nar�ykl�s",
"gstat_n_first_extensions" => "%d pirm� pl�tini�",
"gstat_Robots" => "Paie�kos sistemos",
"gstat_n_first_pages" => "%d pirm� puslapi�",
"gstat_n_first_origins" => "%d pirm� nuorod�",
"gstat_Total" => "Viso",
"gstat_Not_specified" => "Nenurodyta",

// Time stats words
"tstat_Su" => "Sek",
"tstat_Mo" => "Pir",
"tstat_Tu" => "Ant",
"tstat_We" => "Tre",
"tstat_Th" => "Ket",
"tstat_Fr" => "Pen",
"tstat_Sa" => "�e�",

"tstat_Jan" => "Sau",
"tstat_Feb" => "Vas",
"tstat_Mar" => "Kov",
"tstat_Apr" => "Bal",
"tstat_May" => "Geg",
"tstat_Jun" => "Bir",
"tstat_Jul" => "Lie",
"tstat_Aug" => "Rugp",
"tstat_Sep" => "Rugs",
"tstat_Oct" => "Spa",
"tstat_Nov" => "Lap",
"tstat_Dec" => "Gruo",

"tstat_Last_day" => "Pastaraj� dien�", 
"tstat_Last_week" => "Pastaraj� savit�", 
"tstat_Last_month" => "Pastaraj� men�s�", 
"tstat_Last_year" => "Pastaraisiais metais",

// Configuration page words and sentences

"config_Variable_name" => "Kintamojo vardas",
"config_Variable_value" => "Kintamojo reik�m�",
"config_Explanations" => "Paai�kinimas",

"config_bbc_mainsite" =>
"J�s� svetain�s adresas.<br>
Jei paliksite tu��i�, �is j�s� svetain�s adresas nepasirodys BBClone valdymo meniu skiltyje.
<br>
<br> 
<i>Pavyzdys:</i><br>
\$BBC_MAINSITE = \"http://www.svetaine.lt/katalogas/\".",

"config_bbc_show_config" =>
"Nusako, ar konfig�racija gali b�ti matoma i� show_config.php",

"config_bbc_titlebar" =>
"Tekstas, atsirandantis antra�t�je, visuose BBClone puslapiuose.<br> 
Galima naudoti tokius kintamuosius:<br>
<ul>
<li>%SERVER - serverio adresas, 
<li>%DATE - dabartin� data.
</ul>
Taip pat galima naudoti ir HTML.",

"config_bbc_language" =>
"Kalba, kuri� nor�tum�te naudoti. Pagal nutyl�jim� yra naudojama angl� kalba.<br>
Jei norite su�inoti � kokias kalbas yra i�verstas BBClone, �vilgtel�kite �
<a href=\"http://bbclone.de\">download sekcij�</a>.", 

"config_bbc_maxtime" =>
"Laiko intervalas (sekund�mis) tarp dviej� skirting� tos pa�ios IP/AGENT poros apsilankym�.<br>
Pagal nutyl�jim� yra : 1800 sekund�i�",

"config_bbc_maxvisible" => 
"Kiek �ra�� nor�tum�te matyti Detalioj Statistikoj<br>
Pagal nutyl�jim� yra 100. Nenustakin�kite daugiau 500 �ra�� (be to taip daryti netikslinga :).",

"config_bbc_maxos" =>
"Kiek daugiausiai Operacini� Sistem� nor�tum�te matyti globaliog statistikoj?",

"config_bbc_maxbrowser" =>
"Kiek daugiausiai nar�ykli� nor�tum�te matyti globaliog statistikoj?",

"config_bbc_maxextension" =>
"Kiek pl�tini� norite matyti Globalioj Statistikoj",

"config_bbc_maxrobot" =>
"Kiek daugiausiai paie�kos robot� nor�tum�te matyti globaliog statistikoj?",

"config_bbc_maxpage" =>
"Kiek puslapi� norite matyti Globalioj Statistikoj",

"config_bbc_maxorigin" =>
"Kiek nuorod� norite matyti Globalioj Statistikoj",

"config_bbc_ignoreip" =>
"Kuriuos IP adresus arba potinklius norite ignoruoti<br>
<i>Formatas:</i> &lt;IP adresas arba potinklis&gt;, &lt;kitas IP adresas or potinklis&gt;<br>
D�kite kablel� \",\" tarp kiekvieno IP. Pagal nutyl�jim� yra ignoruojami vidinio tinklo (\"LAN\") IP adresai.",

"config_bbc_ignore_refer" =>
"Jei turite kelias svetaines ir nenorite matyti lanytoj�, vaik�tan�i� i� vienos svetain�s � kit� kaip nauj� lankytoj�,
tuomet �ia galite �ra�yti j� adresus. Nuoroda i� kur at�jo lankytojas bus laikoma kaip \"nenurodyta\",
bet lankytojai bus toliau sekami. �tai pavysdys kaip tai rek�t� padaryti:<br />
\$BBC_IGNORE_REFER = \"www.hostas1.org, another.hostas2.org, darvienas.hostas3.org\";<br />
ir t.t.",

"config_bbc_own_refer" =>
"Jei u�d�ta varnel�, visos nuorodos i� serverio, kuriame veikia bbclone,
yra nustatomos kaip http://www.manoservakas.com (kur \"manosrevakas\" yra j�s� serveris).
Tai yra naudinga, jei nenorite kad bbclone rodyt� nuorodas i� administracini� svetaini�, apsaugot� svetain�s viet� ar kit� nuorod�, kurias nor�tum�te nusl�pti.",

"config_bbc_no_string" => 
"BBClone �ra�o komentar� � HTML kod�, tamm kad indokuot� savo buvim�.
Nors tai yra palyginti saugu, kartais tai gali pakenkti tam tikr� forum� ar turinio valdymo sistemoms.
Jei j�s matote tu��i� puslap� arba gaunate prane�im� \"header already sent by\", turite nuokomentuoti �i� �ym�, kad j�s� svetain� toliau veikt�.",

"config_bbc_detailed_stat_fields" =>
"Kintamasis \$BBC_DETAILED_STAT_FIELDS kokie laukai bus rodomi Detalioj Statistikoj (\"show_detailed.php\").<br>
Galimi tokie stulpeliai:<br> 
\"id\", \"time\" (laikas), \"visits\" (apsilankymai), \"dns\" (potinklio vardas), \"referer\" (nuorda i� kur at�jo), \"os\" (operacin� sistema), \"browser\" (nar�kl�), \"ext\" (pl�tinys)<br>
Stulpeli� tvarka yra svarbi.<br>
Stulpeliai, kuri� nepamin�site, Detalioje Statistikoje nebus rodomi.<br>
<br>
<i>Pavyzd�iai:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Galite nustatyti statistikos puslapio lygiavim�.
Galimos reik�m�s yra tokios: \"left\", \"right\", \"center\"",

"config_bbc_title_size" => 
"Antra��i� �rifto dydis gali b�ti nuo 0 (ma�iausias) iki 6 (did�iausias)",

"config_bbc_subtitle_size" =>
"Lauk� pavadinimai gali b�ti nuo 0 (ma�iausias) iki 6 (did�iausias)",

"config_bbc_text_size" => 
"Paprastas tekstas gali b�ti nuo 0 (ma�iausias) iki 6 (did�iausias)"

);
?>