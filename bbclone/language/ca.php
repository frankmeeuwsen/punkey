<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/ca.php,v 1.13 2004/02/15 19:39:12 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: ca.php
* Summary: Cont� la taula de traducci� al catal�
*
* Traduit al Catal� per:
* Gregori Arjona Toledo 
* garjona@menta.net
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "l' Equip de BBClone - llicenciat sota la",
"global_yes" => "si",
"global_no" => "no",

// The error messages
"error_cannot_see_config" =>
"No es pot mostrar la configuraci� de BBClone en aquest servidor.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Altres", "ext_com" => "Comercial",
"ext_net" => "Xarxes", "ext_edu" => "Educaci�",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Jap�", "ext_us" => "Estats Units",
"ext_uk" => "Regne unit", "ext_de" => "Alemanya",
"ext_mil" => "Exercit dels estats units", "ext_ca" => "Canada",
"ext_it" => "Italia", "ext_au" => "Australia",
"ext_org" => "Organitzacions", "ext_nl" => "Holanda",
"ext_fr" => "Fran�a", "ext_tw" => "Taiwan",
"ext_gov" => "Gobern estats units", "ext_fi" => "Finlandia",
"ext_br" => "Brasil", "ext_se" => "Suecia",
"ext_es" => "Espanya", "ext_no" => "Noruega",
"ext_mx" => "Mexic", "ext_kr" => "Corea",
"ext_ch" => "Suissa", "ext_dk" => "Dinamarca",
"ext_be" => "B�lgica", "ext_at" => "Austria",
"ext_nz" => "Nova zelanda", "ext_ru" => "Rusia",
"ext_pl" => "Polonia", "ext_za" => "Sud Africa",
"ext_unknown" => "Desconegut", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapur",
"ext_arpa" => "Errors", "ext_cz" => "Rep�blica checa",
"ext_hu" => "Hungr�a", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turqu�a",
"ext_gr" => "Grecia", "ext_cn" => "China",
"ext_ie" => "Irlanda", "ext_my" => "Malasia",
"ext_th" => "Tailandia", "ext_cl" => "Chile",
"ext_co" => "Colombia", "ext_is" => "Islandia",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ucrania",
"ext_sk" => "Eslovaquia", "ext_ro" => "Ruman�a",
"ext_ae" => "Emirats �rabs", "ext_id" => "Indonesia",
"ext_su" => "Uni� sovi�tica", "ext_si" => "Eslovenia",
"ext_hr" => "Croacia", "ext_ph" => "Filipines",
"ext_lv" => "Letonia", "ext_ve" => "Vene�ola",
"ext_bg" => "Bulgaria", "ext_lt" => "Lituania",
"ext_yu" => "Yugoslavia", "ext_lu" => "Luxemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Organitzacions",
"ext_do" => "Republica Dominicana", "ext_cy" => "Xipre",
"ext_pk" => "Pakistan", "ext_cc" => "Illas Cocos",
"ext_tt" => "Trinitat i Tobago", "ext_eg" => "Egipte",
"ext_lb" => "Liban", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakhist�n",
"ext_na" => "Namibia", "ext_mu" => "Mauritania",
"ext_bm" => "Bermudes", "ext_sa" => "Arabia Saud�",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Illes de Navidad", "ext_pa" => "Panam�",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Tierra Verde",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Islas Faro",
"ext_ac" => "Islas de la ascensi�n", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenia", "ext_pf" => "Polinesia francesa",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Marruecos",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnia Herzegovina", "ext_om" => "Oman",
"ext_jo" => "Jordan", "ext_ir" => "Iran",
"ext_st" => "San Tome y Principe", "ext_vi" => "Islas V�rgenes (U.S.)",
"ext_ci" => "Ivory Coast", "ext_jm" => "Jamaica",
"ext_li" => "Lichenstein", "ext_ky" => "Islas Caim�n",
"ext_gp" => "Guadalupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Samoa Americana", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Islas Heard and Mc Donald", "ext_fm" => "Micronesia",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Ruanda", "ext_mq" => "Martinica",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Nueva Guinea",
"ext_bz" => "Belize", "ext_sh" => "Santa Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldivas",
"ext_nc" => "New Caledonia", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Islas Salom�n", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodia", "ext_tc" => "Islas Turks and Caicos",
"ext_tf" => "Territorios del sur de francia", "ext_az" => "acerbay�n",
"ext_dm" => "Dominica", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Nigeria", "ext_gf" => "Guinea francesa",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk Island", "ext_io" => "Territorio brit�nico del oceano �ndico",
"ext_gs" => "Sur de Georgia y islas Sandwich", "ext_ye" => "Yemen",
"ext_an" => "Antillas Holandesas", "ext_aq" => "Antarctica",
"ext_tn" => "Tunez", "ext_ck" => "Islas Cook",
"ext_ls" => "Lesotho", "ext_et" => "Etiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leona",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Islas v�rgenes(British)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Isla de Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Gab�n", "ext_bs" => "Bahamas",
"ext_va" => "Estado vaticano", "ext_lc" => "Santa Lucia",
"ext_cd" => "Congo", "ext_gm" => "Gambia",
"ext_mp" => "Islas Marianas del norte", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Camer�n", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libia",
"ext_cf" => "Republica centro africana", "ext_mm" => "Myanmar",
"ext_td" => "Chad", "ext_iq" => "Irak",
"ext_kn" => "San Kitts y Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afganist�n",
"ext_cv" => "Cape Verde", "ext_mh" => "Islas Marshall",
"ext_pm" => "San Pierre y Miquelon", "ext_so" => "Somalia",
"ext_vc" => "San Vincente y las Granadinas", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Islas Bouvet",
"ext_fk" => "Islas Falkland (Malvinas)", "ext_gq" => "Guinea ecuatorial",
"ext_sd" => "Sudan", "ext_sj" => "islas Svalbard y Jan Mayen",
"ext_sr" => "Suriname", "ext_sy" => "Syria",
"ext_tp" => "Timor este", "ext_um" => "Islas menores de los estados unidos",
"ext_wf" => "Wallis y islas Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Adr. IP",

// Miscellaneoux translations
"misc_other" => "Altres",
"misc_unknown" => "Desconegut",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Lloc principal",
"navbar_Configuration" => "Configuraci�",
"navbar_Global_Stats" => "Estad�stiques globals",
"navbar_Detailed_Stats" => "Estad�stiques detallades",
"navbar_Time_Stats" => "Estad�stiques temporals",
"navbar_Link_Stats" => "Estad�stiques d'enlla�os",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Data",
"dstat_Visits" => "Visites",
"dstat_Extension" => "Extensio",
"dstat_DNS" => "Hostname",
"dstat_From" => "prov� de",
"dstat_OS" => "S.O.",
"dstat_Browser" => "Navegador",
"dstat_New_access" => "Nous accesos",
"dstat_Elapsed_time" => "Temps de lapse",
"dstat_No_new_access" => "Accesos antics",
"dstat_Visible_accesses" => "accesos visibles",
"dstat_green_rows" => "Files verdes",
"dstat_blue_rows" => "Files blaves",
"dstat_red_rows" => "Files vermelles",
"dstat_last_visit" => "�ltima visita",
"dstat_robots" => "robots",

// Global stats words

"gstat_Accesses" => "Accesos",
"gstat_Total_visits" => "Visites totals",
"gstat_Total_unique" => "Totals �niques",
"gstat_New_visits" => "Noves visites",
"gstat_New_unique" => "Noves �niques",
"gstat_Blacklisted" => "Bloquejades",
"gstat_Operating_systems" => "Sistemes operatius",
"gstat_Browsers" => "Navegadors",
"gstat_n_first_extensions" => "%d primeres extensions",
"gstat_Robots" => "Robots",
"gstat_n_first_pages" => "%d primers p�gines",
"gstat_n_first_origins" => "%d primers origens",
"gstat_Total" => "Total",
"gstat_Not_specified" => "sense especificar",

// Time stats words
"tstat_Su" => "Dium",
"tstat_Mo" => "Dill",
"tstat_Tu" => "Dima",
"tstat_We" => "Dime",
"tstat_Th" => "Dijo",
"tstat_Fr" => "Dive",
"tstat_Sa" => "Disa",

"tstat_Jan" => "Gen",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Abr",
"tstat_May" => "Mai",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Ago",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Oct",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Des",

"tstat_Last_day" => "�ltim d�a",
"tstat_Last_week" => "�ltima setmana",
"tstat_Last_month" => "�ltim mes",
"tstat_Last_year" => "�ltim any",

// Configuration page words and sentences

"config_Variable_name" => "Nom de la variable",
"config_Variable_value" => "Valor de la variable",
"config_Explanations" => "Explicacions",

"config_bbc_mainsite" =>
"L'adre�a web del teu site esta buida.<br>
Si est� buida, questa URL no apareixer� a la barra de navegaci� de totes les p�gines de BBClone.<br><br>
<br>
<i>Exemple:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"Determina si la configuraci� ha de ser visible o no desde show_config.php",

"config_bbc_titlebar" =>
"Texte que apareixer� a la barra de titol de totes les pagines de BBClone.<br>
Les macros admeses son:<br>
<ul>
<li>%SERVER: nom del servidor,
<li>%DATE: data actual.
</ul>
Les etiquetes HTML son permeses.",

"config_bbc_language" =>
"El llenguatge que vols utilitzar. Per defecte es l'angl�s.<br>
Per saber quins llenguatges hi han disponibles mireu a la seccio de 
<a href=\"http://bbclone.de\">descarregues</a> del site de BBClone",

"config_bbc_maxtime" =>
"L'interval de temps (en segons) entre dos visites diferents per la mateixa IP.<br>
El valor per defectes es de: 1800 s",

"config_bbc_maxvisible" =>
"Quantes entrades vols veure a les estad�stiques detallades?<br>
El valor per defecte es 100 ,no canviar aquest valor per sobre de 500.",

"config_bbc_maxos" =>
"Quantes Systemes operatius vols veure clasificades en les estad�stiques Globals?",

"config_bbc_maxbrowser" =>
"Quantes Navegadors vols veure clasificades en les estad�stiques Globals?",

"config_bbc_maxextension" =>
"Quantes extensions vols veure clasificades en les estad�stiques Globals?",

"config_bbc_maxrobot" =>
"Quantes Robots vols veure clasificades en les estad�stiques Globals?",

"config_bbc_maxpage" =>
"Quantes p�gines vols veure classificades en les estad�stiques Globals?",

"config_bbc_maxorigin" =>
"Quants origens vols veure classificats en les estad�stiques Globals?",

"config_bbc_ignoreip" =>
"Quina adre�a IP (o subxarxa) vols que sigui ignorada?<br>
<i>Format:</i> &lt;adre�a IP o subxarxa&gt;, &lt;un altre adre�a IP o subxarxa&gt;<br>
Inserta una coma \",\" entre cada IP. Per defectes les IP \"locals\" mes utilitzades.",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"La viariable \$BBC_DETAILED_STAT_FIELDS determina els camps a visualitzar en 
show_detailed.php.<br>
Posibles noms de camps:<br>
\"id\", \"time\", \"visits\", \"dns\" (nom del host), \"referer\", \"os\", \"browser\", \"ext\" (extensi�)<br>
L'ordre dels camps es important.<br>
<br>
<i>Exemples:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Aqu� pots canviar la alineacio de la pagina principal d'estad�stiques. 
Els posibles valors son aquests \"left\" (Esquerra), \"right\" (Dreta), \"center\" (Centrat)",

"config_bbc_title_size" =>
"Tamany de la lletra dels titols, valors entre 0 (el mes petit) i 6 (el mes gran)",

"config_bbc_subtitle_size" =>
"Tamany de la lletra dels subtitols, valors entre 0 (el mes petit) i 6 (el mes gran)",

"config_bbc_text_size" =>
"Tamany de la lletra del texte simple, valors entre 0 (el mes petit) i 6 (el mes gran)"

);
?>