<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/se.php,v 1.13 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: se.php
* Summary: Contains a Swedish translation table
* Description: v1.0
* Author: Erik Englund, erkend00@student.umu.se
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d-m-Y",

// Global translation
"global_bbclone_copyright" => "BBClone-teamet - licensierad under ",
"global_yes" => "ja",
"global_no" => "nej",

// The error messages
"error_cannot_see_config" =>
"Du har inte tillst&aring;nd att se BBClone-konfigurationen p&aring; den h&auml;r servern.",

// Address Extensions (see lib/extension.php)

"ext_other" => "Annan", "ext_com" => "Kommersiell",
"ext_net" => "N&auml;tverk", "ext_edu" => "Utbildning",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japan", "ext_us" => "F&ouml;renta staterna",
"ext_uk" => "F&ouml;renade kungariket", "ext_de" => "Tyskland", 
"ext_mil" => "F&ouml;renta staternas milit&auml;r", "ext_ca" => "Kanada",
"ext_it" => "Italien", "ext_au" => "Australien",
"ext_org" => "Organisationer", "ext_nl" => "Nederl&auml;nderna",
"ext_fr" => "Frankrike", "ext_tw" => "Taiwan", 
"ext_gov" => "F&ouml;renta staternas regering", "ext_fi" => "Finland",
"ext_br" => "Brasilien", "ext_se" => "Sverige", 
"ext_es" => "Spanien", "ext_no" => "Norge", 
"ext_mx" => "Mexico", "ext_kr" => "Korea",
"ext_ch" => "Schweiz", "ext_dk" => "Danmark",
"ext_be" => "Belgien", "ext_at" => "&Ouml;sterrike",
"ext_nz" => "Nya Zeeland", "ext_ru" => "Ryssland", 
"ext_pl" => "Polen", "ext_za" => "Sydafrika",
"ext_unknown" => "Ok&auml;nd", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapore",
"ext_arpa" => "Fel", "ext_cz" => "Tjeckien",
"ext_hu" => "Ungern", "ext_hk" => "Hongkong",
"ext_pt" => "Portugal", "ext_tr" => "Turkiet",
"ext_gr" => "Grekland", "ext_cn" => "Kina",
"ext_ie" => "Irland", "ext_my" => "Malaysia",
"ext_th" => "Thailand", "ext_cl" => "Chile",
"ext_co" => "Colombia", "ext_is" => "Island",
"ext_uy" => "Uruguay", "ext_ee" => "Estland", 
"ext_in" => "Indien", "ext_ua" => "Ukraina",
"ext_sk" => "Slovakien", "ext_ro" => "Rum&auml;nien",
"ext_ae" => "F&ouml;renade Arabemiraten", "ext_id" => "Indonesien",
"ext_su" => "Sovjetunionen", "ext_si" => "Slovenien", 
"ext_hr" => "Kroatien", "ext_ph" => "Filippinerna",
"ext_lv" => "Lettland", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgarien", "ext_lt" => "Litauen",
"ext_yu" => "Jugoslavien", "ext_lu" => "Luxemburg", 
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Internationella organisationer",
"ext_do" => "Dominikanska republiken", "ext_cy" => "Cypern",
"ext_pk" => "Pakistan", "ext_cc" => "Kokos&ouml;arna", 
"ext_tt" => "Trinidad och Tobago", "ext_eg" => "Egypten",
"ext_lb" => "Libanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudiarabien",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kirgizistan",
"ext_cx" => "Jul&ouml;n", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "f.d. jugoslaviska republiken Makedonien", "ext_gl" => "Gr&ouml;nland",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei", 
"ext_mt" => "Malta", "ext_fo" => "F&auml;r&ouml;arna",
"ext_ac" => "Ascension", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenien", "ext_pf" => "Franska Polynesien",
"ext_ge" => "Georgien", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Vitryssland",
"ext_sv" => "El Salvador", "ext_ma" => "Marocko",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal", 
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnien och Hercegovina", "ext_om" => "Oman",
"ext_jo" => "Jordanien", "ext_ir" => "Iran",
"ext_st" => "S�o Tom� och Pr�ncipe", "ext_vi" => "Amerikanska Jungfru&ouml;arna",
"ext_ci" => "Elfenbenskusten", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Cayman&ouml;arna",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagaskar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino", 
"ext_as" => "Amerikanska Samoa", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Heard&ouml;n och McDonald&ouml;arna", "ext_fm" => "Mikronesien",
"ext_fj" => "Fiji", "ext_cu" => "Kuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Nua Guinea",
"ext_bz" => "Belize", "ext_sh" => "Saint Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldiverna",
"ext_nc" => "Nya Kaledonien", "ext_ag" => "Antigua och Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tadzjikistan",
"ext_sb" => "Salomon&ouml;arna", "ext_bf" => "Burkina Faso",
"ext_kh" => "Kambodja", "ext_tc" => "Turks- och Caicos&ouml;arna",
"ext_tf" => "De franska territorierna i s&ouml;dra Indiska oceanen", "ext_az" => "Azerbajdzjan",
"ext_dm" => "Dominica", "ext_mz" => "Mo�ambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongoliet", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat", 
"ext_ne" => "Niger", "ext_gf" => "Franska Guyana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albanien", "ext_gh" => "Ghana", 
"ext_nf" => "Norfolk&ouml;n", "ext_io" => "Brittiska territoriet i Indiska Oceanen",
"ext_gs" => "Sydgeorgien och Sydsandwich&ouml;arna", "ext_ye" => "Yemen",
"ext_an" => "Nederl&auml;ndska Antillerna", "ext_aq" => "Antarktis",
"ext_tn" => "Tunisien", "ext_ck" => "Cook&ouml;arna",
"ext_ls" => "Lesotho", "ext_et" => "Etiopien",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Brittiska Jungfru&ouml;arna", "ext_vn" => "Vietnam", 
"ext_mr" => "Mauretanien", "ext_gy" => "Guyana", 
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Komorerna", "ext_dz" => "Algeriet",
"ext_im" => "Isle of Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Storbritannien", "ext_bs" => "Bahamas",
"ext_va" => "Heliga stolen", "ext_lc" => "Saint Lucia",
"ext_cd" => "Demokratiska republiken Kongo", "ext_gm" => "Gambia", 
"ext_mp" => "Nordmarianerna", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Kamerun", "ext_ao" => "Angola", 
"ext_er" => "Eritrea", "ext_ly" => "Libyen",
"ext_cf" => "Centralafrikanska republiken", "ext_mm" => "Myanmar", 
"ext_td" => "Tchad", "ext_iq" => "Irak", 
"ext_kn" => "Saint Christopher och Nevis", "ext_sc" => "Seychellerna",
"ext_cg" => "Kongo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Kap Verde", "ext_mh" => "Marshall&ouml;arna",
"ext_pm" => "Saint Pierre och Miquelon", "ext_so" => "Somalia", 
"ext_vc" => "Saint Vincent och Grenadinerna", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia", 
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "R�union", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet&ouml;n", 
"ext_fk" => "Falklands&ouml;arna", "ext_gq" => "Ekvatorialguinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard och Jan Mayen",
"ext_sr" => "Surinam", "ext_sy" => "Syrien",
"ext_tp" => "&Ouml;sttimor", "ext_um" => "F&ouml;renta staternas mindre &ouml;ar i Oceanien och V&auml;stindien",
"ext_wf" => "Wallis och Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numerisk",


// Miscellaneous translations
"misc_other" => "Annan",
"misc_unknown" => "Ok&auml;nd",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Huvudsidan",
"navbar_Configuration" => "Konfiguration",
"navbar_Global_Stats" => "Global statistik",
"navbar_Detailed_Stats" => "Detaljerad statistik",
"navbar_Time_Stats" => "Tidsstatistik",
"navbar_Link_Stats" => "L&auml;nkstatistik",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Tid",
"dstat_Visits" => "Bes&ouml;k",
"dstat_Extension" => "Toppdom&auml;n",
"dstat_DNS" => "Hostname",
"dstat_From" => "Fr&aring;n",
"dstat_OS" => "OS",
"dstat_Browser" => "Webbl&auml;sare",
"dstat_New_access" => "Nya bes&ouml;k",
"dstat_Elapsed_time" => "F&ouml;rfluten tid",
"dstat_No_new_access" => "Inga nya bes&ouml;k",
"dstat_Visible_accesses" => "Synliga bes&ouml;k",
"dstat_green_rows" => "gr&ouml;na rader",
"dstat_blue_rows" => "bl&aring; rader",
"dstat_red_rows" => "r&ouml;da rader",
"dstat_last_visit" => "senaste bes&ouml;k",
"dstat_robots" => "robotar",

// Global stats words

"gstat_Accesses" => "Bes&ouml;k",
"gstat_Total_visits" => "Totala bes&ouml;k",
"gstat_Total_unique" => "Totala unika",
"gstat_New_visits" => "Nya bes&ouml;k",
"gstat_New_unique" => "Nya unika",
"gstat_Blacklisted" => "Svartlistade",
"gstat_Operating_systems" => "Operativsystem",
"gstat_Browsers" => "Webbl&auml;sare",
"gstat_n_first_extensions" => "%d f&ouml;rsta toppdom&auml;nerna",
"gstat_Robots" => "Robotar",
"gstat_n_first_pages" => "%d f&ouml;rsta sidorna",
"gstat_n_first_origins" => "%d f&ouml;rsta ursprungen",
"gstat_Total" => "Totalt",
"gstat_Not_specified" => "Ospecificerade",

// Time stats words
"tstat_Su" => "S&ouml;n",
"tstat_Mo" => "M&aring;n",
"tstat_Tu" => "Tis",
"tstat_We" => "Ons",
"tstat_Th" => "Tor",
"tstat_Fr" => "Fre",
"tstat_Sa" => "L&ouml;r",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Apr",
"tstat_May" => "Maj",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Okt",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dec",

"tstat_Last_day" => "Senaste dagen",
"tstat_Last_week" => "Senaste veckan",
"tstat_Last_month" => "Senaste m&aring;naden",
"tstat_Last_year" => "Senaste &aring;ret",

// Configuration page words and sentences

"config_Variable_name" => "Variabelnamn",
"config_Variable_value" => "Variabelv&auml;rde",
"config_Explanations" => "F&ouml;rklaringar",

"config_bbc_mainsite" =>
"URL till din webbsida.<br>
Om den &auml;r tom kommer denna URL inte visas i navigationsf&auml;ltet p&aring; alla BBClone-sidor.<br>
<br>
<i>Exempel:</i><br>
\$BBC_MAINSITE = \"http://www.minwebbsida.com/n&aring;gonstans/\".",

"config_bbc_show_config" =>
"Best&auml;m om konfigurationen ska vara synlig eller inte genom show_config.php.",

"config_bbc_titlebar" =>
"Titeln som visas i titelf&auml;ltet och som &auml;r synlig i alla BBClone-sidor.<br>
Fungerande makros &auml;r:<br>
<ul>
<li>%SERVER: servernamn,
<li>%DATE: nuvarande datum.
</ul>
HTML-taggar &auml;r ocks&aring; till&aring;tna.",

"config_bbc_language" =>
"Spr&aring;ket du vill anv&auml;nda. Standard &auml;r engelska.<br>
F&ouml;r att se de tillg&auml;ngliga spr&aring;ken, se 
<a href=\"http://bbclone.de\">download section</a> p&aring; BBClone-hemsidan.",

"config_bbc_maxtime" =>
"Tidsintervall (i sekunder) mellan 2 olika bes&ouml;k fr&aring;n samma IP/AGENT.<br>
Standard &auml;r den uppkommande standarden: 1800 s.",

"config_bbc_maxvisible" =>
"Hur m&aring;nga poster vill du se i den detaljerade statistiken?<br>
Standard &auml;r 100. S&auml;tt inte h&ouml;gre &auml;n 500 (vilket &auml;r oanv&auml;ndbart i alla fall).",

"config_bbc_maxos" =>
"How many OS do you want to see specified in the global stats?",

"config_bbc_maxbrowser" =>
"How many browsers do you want to see specified in the global stats?",

"config_bbc_maxextension" =>
"Hur m&aring;nga toppdom&auml;ner vill du se klassificerade i den globala statistiken?",

"config_bbc_maxrobot" =>
"How many robots do you want to see specified in the global stats?",

"config_bbc_maxpage" =>
"Hur m&aring;nga sidor vill du se klassificerade i den globala statistiken?",

"config_bbc_maxorigin" =>
"Hur m&aring;nga ursprung vill du se klassificerade i den globala statistiken?",

"config_bbc_ignoreip" =>
"Vilka IP-adresser (eller subn&auml;t) vill du ignorera?<br>
<i>Format:</i> &lt;IP-adress eller subn&auml;t&gt;, &lt;annan IP-adress eller subn&auml;t&gt;<br>
S&auml;tt in ett komma \",\" mellan varje IP. Standard &auml;r de flesta \"lokala\" IP.",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"Variabeln \$BBC_DETAILED_STAT_FIELDS best&auml;mmer f&auml;lten som ska visas i 
show_detailed.php.<br>
M&ouml;jliga f&auml;lt &auml;r:<br>
\"id\", \"time\", \"visits\", \"dns\" (v&auml;rdnamn), \"referer\", \"os\", \"browser\", \"ext\" (toppdom&auml;n)<br>
Ordningen p&aring; f&auml;ltnamnen &auml;r viktig.<br>
Icke-existerande f&auml;lt kommer att fattas p&aring; statistiksidan.<br>
<br>
<i>Exempel:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"H&auml;r kan du best&auml;mma stilen p&aring; huvudstatistiksidan. 
M&ouml;jliga v&auml;rden &auml;r \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"Storleken p&aring; titeln mellan 0 (minsta) och 6 (h&ouml;gsta).",

"config_bbc_subtitle_size" =>
"Storleken p&aring; undertiteln mellan 0 (minsta) och 6 (h&ouml;gsta).",

"config_bbc_text_size" =>
"Storleken p&aring; den enkla texten mellan 0 (minsta) och 6 (h&ouml;gsta)."

);
?>