<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/es.php,v 1.12 2004/02/15 19:39:12 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: es.php
* Summary: Contiene la tabla de traducci�n al espa�ol
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "El equipo de BBClone - licenciado bajo la",
"global_yes" => "si",
"global_no" => "no",

// The error messages
"error_cannot_see_config" =>
"no puedes ver la configuracion de BBClone en este servidor.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Otros", "ext_com" => "Comercial",
"ext_net" => "Redes", "ext_edu" => "Educaci�n",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Jap�n", "ext_us" => "Estados Unidos",
"ext_uk" => "Reino unido", "ext_de" => "Alemania",
"ext_mil" => "Ejercito estados unidos", "ext_ca" => "Canada",
"ext_it" => "Italia", "ext_au" => "Australia",
"ext_org" => "Organizaciones", "ext_nl" => "Holanda",
"ext_fr" => "Francia", "ext_tw" => "Taiwan",
"ext_gov" => "Gobierno estados unidos", "ext_fi" => "Finlandia",
"ext_br" => "Brasil", "ext_se" => "Sweden",
"ext_es" => "Espa�a", "ext_no" => "Noruega",
"ext_mx" => "Mejico", "ext_kr" => "corea",
"ext_ch" => "Suiza", "ext_dk" => "Dinamarca",
"ext_be" => "B�lgica", "ext_at" => "Austria",
"ext_nz" => "Nueva zelanda", "ext_ru" => "Rusia",
"ext_pl" => "Polonia", "ext_za" => "Sud Africa",
"ext_unknown" => "Desconocido", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapur",
"ext_arpa" => "Errores", "ext_cz" => "Rep�blica checa",
"ext_hu" => "Hungr�a", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turqu�a",
"ext_gr" => "Grecia", "ext_cn" => "China",
"ext_ie" => "Irlanda", "ext_my" => "Malasia",
"ext_th" => "Tailandia", "ext_cl" => "Chile",
"ext_co" => "Colombia", "ext_is" => "Islandia",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ucrania",
"ext_sk" => "Eslovaquia", "ext_ro" => "Ruman�a",
"ext_ae" => "Emiratos �rabes", "ext_id" => "Indonesia",
"ext_su" => "Uni�n sovi�tica", "ext_si" => "Eslovenia",
"ext_hr" => "Croacia", "ext_ph" => "Filipinas",
"ext_lv" => "Letonia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgaria", "ext_lt" => "Lituania",
"ext_yu" => "Yugoslavia", "ext_lu" => "Luxemburgo",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Organizaciones",
"ext_do" => "Republica Dominicana", "ext_cy" => "Cyprus",
"ext_pk" => "Pakistan", "ext_cc" => "Islas Cocos",
"ext_tt" => "Trinidad y Tobago", "ext_eg" => "Egipto",
"ext_lb" => "Lebanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakhist�n",
"ext_na" => "Namibia", "ext_mu" => "Mauritania",
"ext_bm" => "Bermudas", "ext_sa" => "Arabia Saud�",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Islas de Navidad", "ext_pa" => "Panam�",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Tierra Verde",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Islas Faro",
"ext_ac" => "Islas de la ascensi�n", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenia", "ext_pf" => "Polinesia francesa",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Marruecos",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnia Herzegovina", "ext_om" => "Oman",
"ext_jo" => "Jordan", "ext_ir" => "Iran",
"ext_st" => "San Tome y Principe", "ext_vi" => "Islas V�rgenes (U.S.)",
"ext_ci" => "Ivory Coast", "ext_jm" => "Jamaica",
"ext_li" => "Lichenstein", "ext_ky" => "Islas Caim�n",
"ext_gp" => "Guadalupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Samoa Americana", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Islas Heard and Mc Donald", "ext_fm" => "Micronesia",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Ruanda", "ext_mq" => "Martinica",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Nueva Guinea",
"ext_bz" => "Belize", "ext_sh" => "Santa Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldivas",
"ext_nc" => "New Caledonia", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Islas Salom�n", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodia", "ext_tc" => "Islas Turks and Caicos",
"ext_tf" => "Territorios del sur de francia", "ext_az" => "acerbay�n",
"ext_dm" => "Dominica", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Nigeria", "ext_gf" => "Guinea francesa",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk Island", "ext_io" => "Territorio brit�nico del oceano �ndico",
"ext_gs" => "Sur de Georgia y islas Sandwich", "ext_ye" => "Yemen",
"ext_an" => "Antillas Holandesas", "ext_aq" => "Antarctica",
"ext_tn" => "Tunez", "ext_ck" => "Islas Cook",
"ext_ls" => "Lesotho", "ext_et" => "Etiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leona",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Islas v�rgenes(British)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Isla de Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Gab�n", "ext_bs" => "Bahamas",
"ext_va" => "Estado vaticano", "ext_lc" => "Santa Lucia",
"ext_cd" => "Congo", "ext_gm" => "Gambia",
"ext_mp" => "Islas Marianas del norte", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Camer�n", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libia",
"ext_cf" => "Republica centro africana", "ext_mm" => "Myanmar",
"ext_td" => "Chad", "ext_iq" => "Irak",
"ext_kn" => "San Kitts y Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afganist�n",
"ext_cv" => "Cape Verde", "ext_mh" => "Islas Marshall",
"ext_pm" => "San Pierre y Miquelon", "ext_so" => "Somalia",
"ext_vc" => "San Vincente y las Granadinas", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Islas Bouvet",
"ext_fk" => "Islas Falkland (Malvinas)", "ext_gq" => "Guinea ecuatorial",
"ext_sd" => "Sudan", "ext_sj" => "islas Svalbard y Jan Mayen",
"ext_sr" => "Suriname", "ext_sy" => "Syria",
"ext_tp" => "Timor este", "ext_um" => "Islas menores de los estados unidos",
"ext_wf" => "Wallis y islas Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numerico",

// Miscellaneoux translations
"misc_other" => "Otras",
"misc_unknown" => "Desconocido",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Sitio principal",
"navbar_Configuration" => "Configuraci�n",
"navbar_Global_Stats" => "Estadi�sticas globales",
"navbar_Detailed_Stats" => "Estad�sticas detalladas",
"navbar_Time_Stats" => "Estad�siticas temporales",
"navbar_Link_Stats" => "estad�sitcas de enlaces",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Tiempo",
"dstat_Visits" => "Visitas",
"dstat_Extension" => "Extension",
"dstat_DNS" => "Hostname",
"dstat_From" => "proviniente de",
"dstat_OS" => "S.O.",
"dstat_Browser" => "Navegador",
"dstat_New_access" => "Nuevos accesos",
"dstat_Elapsed_time" => "Tiempo de lapso",
"dstat_No_new_access" => "Accesos no nuevos",
"dstat_Visible_accesses" => "accesos visibles",
"dstat_green_rows" => "Filas verdes",
"dstat_blue_rows" => "Filas azules",
"dstat_red_rows" => "Filas rojas",
"dstat_last_visit" => "�ltima visita",
"dstat_robots" => "robots",

// Global stats words

"gstat_Accesses" => "Accesos",
"gstat_Total_visits" => "visitas totales",
"gstat_Total_unique" => "Totales �nicas",
"gstat_New_visits" => "Nuevas visitas",
"gstat_New_unique" => "Nuevas �nicas",
"gstat_Blacklisted" => "Bloqueadas",
"gstat_Operating_systems" => "sistemas operativos",
"gstat_Browsers" => "Navegadores",
"gstat_n_first_extensions" => "%d primeras extensiones",
"gstat_Robots" => "Robots",
"gstat_n_first_pages" => "%d primeras p�ginas",
"gstat_n_first_origins" => "%d primeros origenes",
"gstat_Total" => "Total",
"gstat_Not_specified" => "sin especificar",

// Time stats words
"tstat_Su" => "Dom",
"tstat_Mo" => "Lun",
"tstat_Tu" => "Mar",
"tstat_We" => "Mie",
"tstat_Th" => "Jue",
"tstat_Fr" => "Vie",
"tstat_Sa" => "Sab",

"tstat_Jan" => "Ene",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Abr",
"tstat_May" => "May",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Ago",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Oct",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dic",

"tstat_Last_day" => "�ltimo d�a",
"tstat_Last_week" => "�ltima semana",
"tstat_Last_month" => "�ltimo mes",
"tstat_Last_year" => "�ltimo a�o",

// Configuration page words and sentences

"config_Variable_name" => "nombre de la variable",
"config_Variable_value" => "Valor de la variable",
"config_Explanations" => "Explicaciones",

"config_bbc_mainsite" =>
"LA direcci�n web de tu sitio est� vac�a.<br>
Si est� vac�a, esta URL no aparecer� en la barra de navegaci�n de todas las p�ginas de BBClone.<br>
<br>
<i>Ejemplo:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"Determine if the configuration must be visible or not through show_config.php",

"config_bbc_titlebar" =>
"The title appearing into the title bar present in all the BBClone pages.<br>
Understandable macros are:<br>
<ul>
<li>%SERVER: server name,
<li>%DATE: the current date.
</ul>
HTML tags are also allowed.",

"config_bbc_language" =>
"The language you want to use. The default is english.<br>
To know the available languages, look at the the 
<a href=\"http://bbclone.de\">download section</a> of the BBClone 
website.",

"config_bbc_maxtime" =>
"Time interval (in seconds) between 2 different visits from a same couple IP/AGENT.<br>
The default is the emerging standard: 1800 s",

"config_bbc_maxvisible" =>
"How many entries do you want to see in the detailed stats?<br>
Default is 100 do not set it higher than 500 (which is useless, anyway)",
"config_bbc_maxos" =>
"How many OS do you want to see specified in the global stats?",

"config_bbc_maxbrowser" =>
"How many browsers do you want to see specified in the global stats?",

"config_bbc_maxextension" =>
"How many extensions do you want to see specified in the global stats?",

"config_bbc_maxrobot" =>
"How many robots do you want to see specified in the global stats?",

"config_bbc_maxpage" =>
"How many pages do want to see classified in the global stats?",

"config_bbc_maxorigin" =>
"How many origins do want to see classified in the global stats?",

"config_bbc_ignoreip" =>
"Which IP adress (or subnet) do you want to ignore?<br>
<i>Format:</i> &lt;IP adress or subnet&gt;, &lt;another IP adress or subnet&gt;<br>
Insert a comma \",\" between each IP. Default is most \"local\" IP.",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"The variable \$BBC_DETAILED_STAT_FIELDS determines the fields to display in the 
show_detailed.php.<br>
Possible field names are:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extension)<br>
The order of the field names is important.<br>
Non-present fields will be missing on the statistics page.<br>
<br>
<i>Examples:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"You can set here the align style of the main statistics page. 
Possible values are \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"The size level of the titles between 0 (smallest) and 6 (highest)",

"config_bbc_subtitle_size" =>
"The size level of the subtitles between 0 (smallest) and 6 (highest)",

"config_bbc_text_size" =>
"The size level of the simple text between 0 (smallest) and 6 (highest)"

);
?>