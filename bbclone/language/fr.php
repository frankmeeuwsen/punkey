<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/fr.php,v 1.13 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: fr.php
* Summary: contains a french translation table of BBClone
*/

$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "L'&Eacute;quipe BBClone - Licence ",
"global_yes" => "oui",
"global_no" => "non",

// The error messages
"error_cannot_see_config" =>
"Vous n'avez pas l'autorisation de visualiser la configuration de BBClone sur ce serveur",

// Address Extensions (see lib/extension.php)
"ext_other" => "Autre", "ext_com" => "Commercial",
"ext_net" => "Net", "ext_edu" => "&Eacute;ducation",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japon", "ext_us" => "Etats Unis",
"ext_uk" => "Angleterre", "ext_de" => "Allemagne",
"ext_mil" => "Arm&eacute;e US", "ext_ca" => "Canada",
"ext_it" => "Italie", "ext_au" => "Australie",
"ext_org" => "Organisations", "ext_nl" => "Pays Bas",
"ext_fr" => "France", "ext_tw" => "Taiwan",
"ext_gov" => "Gouvernement", "ext_fi" => "Finland",
"ext_br" => "Br&eacute;sil", "ext_se" => "Su&egrave;de",
"ext_es" => "Espagne", "ext_no" => "Norv&egrave;ge",
"ext_mx" => "Mexique", "ext_kr" => "Cor&eacute;e",
"ext_ch" => "Suisse", "ext_dk" => "Danemark",
"ext_be" => "Belgique", "ext_at" => "Autriche",
"ext_nz" => "Nouvelle Z&eacute;lande", "ext_ru" => "F&eacute;d&eacute;ration Russe",
"ext_pl" => "Pologne", "ext_za" => "Afrique du Sud",
"ext_unknown" => "Inconnu", "ext_ar" => "Argentine",
"ext_il" => "Isra&euml;l", "ext_sg" => "Singapoure",
"ext_arpa" => "Erreurs", "ext_cz" => "R&eacute;publique Tch&egrave;que",
"ext_hu" => "Hongrie", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turquie",
"ext_gr" => "Gr&egrave;ce", "ext_cn" => "Chine",
"ext_ie" => "Irelande", "ext_my" => "Malaisie",
"ext_th" => "Tha&iuml;lande", "ext_cl" => "Chili",
"ext_co" => "Colombie", "ext_is" => "Islande",
"ext_uy" => "Uruguay", "ext_ee" => "Estonie",
"ext_in" => "Inde", "ext_ua" => "Ukraine",
"ext_sk" => "Slovakie", "ext_ro" => "Roumanie",
"ext_ae" => "Emirats Arabes Unis", "ext_id" => "Indon&eacute;sie",
"ext_su" => "Union Sovi&eacute;tique", "ext_si" => "Slov&eacute;nie",
"ext_hr" => "Croatie", "ext_ph" => "Philippines",
"ext_lv" => "Latvia", "ext_ve" => "V&eacute;n&eacute;zu&eacute;la",
"ext_bg" => "Bulgarie", "ext_lt" => "Lithuanie",
"ext_yu" => "Yougoslavie", "ext_lu" => "Luxembourg",
"ext_nu" => "Niue", "ext_pe" => "Perou",
"ext_cr" => "Costa Rica", "ext_int" => "Organisations Internationales",
"ext_do" => "R&eacute;pl. Dominicaine", "ext_cy" => "Chypre",
"ext_pk" => "Pakistan", "ext_cc" => "Iles Cocos",
"ext_tt" => "Trinidad et Tobago", "ext_eg" => "Egypte",
"ext_lb" => "Lebanon", "ext_kw" => "Koweit",
"ext_to" => "Tonga", "ext_kz" => "Kazakstan",
"ext_na" => "Namibie", "ext_mu" => "Mauritanie",
"ext_bm" => "Bermudes", "ext_sa" => "Arabie Saudite",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Christmas Island", "ext_pa" => "Panama",
"ext_gt" => "Guat&eacute;mala", "ext_bw" => "Botswana",
"ext_mk" => "Mac&eacute;doine", "ext_gl" => "Groenland",
"ext_ec" => "&Eacute;quateur", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldavie", "ext_py" => "Paraguay",
"ext_bo" => "Bolivie", "ext_bn" => "Brunei Darussalam",
"ext_mt" => "Malte", "ext_fo" => "Iles Faroe",
"ext_ac" => "Iles de l'Ascension", "ext_pr" => "Porto Rico",
"ext_am" => "Arm&eacute;nie", "ext_pf" => "Polyn&eacute;sie Fran&ccedil;aise",
"ext_ge" => "G&eacute;orgie", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "B&eacute;larus",
"ext_sv" => "Salvador", "ext_ma" => "Maroc",
"ext_ke" => "Kenya", "ext_ad" => "Andorre",
"ext_zm" => "Zambie", "ext_np" => "N&eacute;pal",
"ext_bt" => "Bhoutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnie Herz&eacute;govine", "ext_om" => "Oman",
"ext_jo" => "Jordan", "ext_ir" => "Iran",
"ext_st" => "Sao Tome and Principe", "ext_vi" => "Iles Vi&egrave;rges (U.S.)",
"ext_ci" => "C&ocirc;te d'Ivoire", "ext_jm" => "Jama&iuml;que",
"ext_li" => "Liechtenstein", "ext_ky" => "Iles Ca&iuml;man",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marin",
"ext_as" => "American Samoa", "ext_tz" => "Tanzanie",
"ext_ws" => "Samoa", "ext_tm" => "Turkm&eacute;nistan",
"ext_mc" => "Monaco", "ext_sn" => "S&eacute;n&eacute;gal",
"ext_hm" => "Iles Heard et Mc Donald", "ext_fm" => "Micron&eacute;sie (&Eacute;tat f&eacute;d.)",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papouasie Nv. Guin&eacute;e",
"ext_bz" => "B&eacute;lize", "ext_sh" => "St. H&eacute;l&eacute;ne",
"ext_aw" => "Aruba", "ext_mv" => "Maldives",
"ext_nc" => "Nouv. Cal&eacute;donie", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "Ouzb&eacute;kistan", "ext_tj" => "Tadjikistan",
"ext_sb" => "Iles Salomon", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodge", "ext_tc" => "Iles Turques et Caicos",
"ext_tf" => "Terr. Fran&ccedil;ais du Sud", "ext_az" => "Azerbaidjan",
"ext_dm" => "Dominique", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolie", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Guin&eacute;e Fran&ccedil;aise",
"ext_gu" => "Ile de Guam", "ext_hn" => "Honduras",
"ext_al" => "Albanie", "ext_gh" => "Ghana",
"ext_nf" => "Iles Norfolk", "ext_io" => "Territoire Anglais",
"ext_gs" => "Georgie du Sud, Iext Sandwich", 
"ext_ye" => "Yemen",
"ext_an" => "Antilles N&eacute;erlandaises", "ext_aq" => "Antartique",
"ext_tn" => "Tunisie", "ext_ck" => "Iles Cook",
"ext_ls" => "LAntilles N&eacute;erlandaisessotho", "ext_et" => "Ethiopie",
"ext_ng" => "NigAntilles N&eacute;erlandaisesria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbades", "ext_je" => "Jersey",
"ext_vg" => "Iles Vi&egrave;rges (Anglaises)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritanie", "ext_gy" => "Guyane",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Alg&eacute;rie",
"ext_im" => "Ile de Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Quatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Grande Bretagne", "ext_bs" => "Bahamas",
"ext_va" => "Vatican", "ext_lc" => "Sainte Lucie",
"ext_cd" => "Congo", "ext_gm" => "Gambie",
"ext_mp" => "Iles Mariannes du Nord", "ext_gw" => "Guin&eacute;-Bissau",
"ext_cm" => "Cameroun", "ext_ao" => "Angola",
"ext_er" => "Erytr&eacute;e", "ext_ly" => "Libyan Arab. Jam.",
"ext_cf" => "R&eacute;publique d'Afrique", "ext_mm" => "Myanmar",
"ext_td" => "Tchad", "ext_iq" => "Irak",
"ext_kn" => "Saint Kitts and Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenade",
"ext_nr" => "Nauru", "ext_af" => "Afganistan",
"ext_cv" => "Cap Vert", "ext_mh" => "Iles Marshall",
"ext_pm" => "St. Pierre et Miquelon", "ext_so" => "Somalie",
"ext_vc" => "Iles Grenadines et St. Vincent", "ext_bd" => "Bangladesh",
"ext_gn" => "Guin&eacute;e", "ext_ht" => "Ha&iuml;ti",
"ext_la" => "Laos", "ext_lr" => "Lib&eacute;ria",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "R&eacute;union", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Iles Bouvet",
"ext_fk" => "Iles Falkland (Malouines)", "ext_gq" => "Guin&eacute;e &Eacute;quatoriale",
"ext_sd" => "Soudan", "ext_sj" => "Iles Svalbard et Jan Mayen",
"ext_sr" => "Surinam", "ext_sy" => "Syrie",
"ext_tp" => "Timor Oriental", "ext_um" => "United States Min. Outl. Islands",
"ext_wf" => "Iles Wallis et Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Za&iuml;re", "ext_IP" => "Numeric",

// Miscellaneoux translations
"misc_other" => "Autre",
"misc_unknown" => "Inconnu",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Site principal",
"navbar_Configuration" => "Configuration",
"navbar_Global_Stats" => "Statistiques Globales",
"navbar_Detailed_Stats" => "Statistiques D&eacute;taill&eacute;es",
"navbar_Time_Stats" => "Statistiques Temporelles",
"navbar_Link_Stats" => "Statistiques Liens",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Temps",
"dstat_Visits" => "Visites",
"dstat_Extension" => "Extension",
"dstat_DNS" => "Hostname",
"dstat_From" => "A partir de",
"dstat_OS" => "SE",
"dstat_Browser" => "Navigateur",
"dstat_New_access" => "Nouveaux acc&egrave;s",
"dstat_Elapsed_time" => "Temps &eacute;coul&eacute;",
"dstat_No_new_access" => "Pas de nouveaux acc&egrave;s",
"dstat_Visible_accesses" => "Acc&egrave;s visibles",
"dstat_green_rows" => "lignes vertes",
"dstat_blue_rows" => "lignes bleues",
"dstat_red_rows" => "lignes rouges",
"dstat_last_visit" => "derni&egrave;re visite",
"dstat_robots" => "robots",

// Global stats words

"gstat_Accesses" => "Acc&egrave;s",
"gstat_Total_visits" => "Visites totales",
"gstat_Total_unique" => "Visites uniques",
"gstat_New_visits" => "Nouvelles visites",
"gstat_New_unique" => "Nouvelles visites uniques",
"gstat_Blacklisted" => "Livre rouge",
"gstat_Operating_systems" => "Syst&egrave;mes d'exploitation",
"gstat_Browsers" => "Navigateurs",
"gstat_n_first_extensions" => "%d premi&egrave;res extensions",
"gstat_Robots" => "Robots",
"gstat_n_first_pages" => "%d premi&egrave;res pages",
"gstat_n_first_origins" => "%d premi&egrave;res origines",
"gstat_Total" => "Total",
"gstat_Not_specified" => "Non specifi&eacute;",

// Time stats words
"tstat_Su" => "Dim",
"tstat_Mo" => "Lun",
"tstat_Tu" => "Mar",
"tstat_We" => "Mer",
"tstat_Th" => "Jeu",
"tstat_Fr" => "Ven",
"tstat_Sa" => "Sam",

"tstat_Jan" => "Jan",
"tstat_Feb" => "F&eacute;v",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Avr",
"tstat_May" => "Mai",
"tstat_Jun" => "Jui",
"tstat_Jul" => "Jui",
"tstat_Aug" => "Aou",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Oct",
"tstat_Nov" => "Nov",
"tstat_Dec" => "D&eacute;c",

"tstat_Last_day" => "Dernier jour",
"tstat_Last_week" => "Derni&egrave;re semaine",
"tstat_Last_month" => "Dernier mois",
"tstat_Last_year" => "Derni&egrave;re ann&eacute;e",

// Configuration page words and sentences

"config_Variable_name" => "Nom de la variable",
"config_Variable_value" => "Valeur de la variable",
"config_Explanations" => "Explications",

"config_bbc_mainsite" =>
"L'URL de votre site web.<br>
Si celle-ci est vide, elle n'appara&icirc;tra pas dans la barre de navigation pr&eacute;sente dans toutes les pages de BBClone.<br>
<i>Exemple:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"D&eacute;termine si le fichier de configuration est visible du monde entier ou non au travers de show_config.php",

"config_bbc_titlebar" =>
"Le titre apparaissant sous la barre de navigation sur toutes les pages de BBClone.<br>
Les macros reconnues sont:<br>
<ul>
<li>%SERVER: le nom du serveur,
<li>%DATE: la date courante.
</ul>
Les tags HTML sont autoris&eacute;s.",

"config_bbc_language" =>
"Le language que vous d&eacute;sirez employer. Par d&eacute;faut, celui-ci est l'anglais.<br>
Pour conna&icirc;tre la liste des langages disponibles, consultez la section <a href=\"http://bbclone.de/\">download</a> du site BBClone.",

"config_bbc_maxtime" =>
"L'intervalle de temps maximal (en secondes) entre deux visites diff&eacute;rentes &agrave; partir d'un m&ecirc;me couple IP/AGENT.<br>
La valeur par d&eacute;faut est: 1800 s",

"config_bbc_maxvisible" =>
"Combien d'entr&eacute;es doivent &ecirc;tre r&eacute;pertori&eacute;es dans les statistiques d&eacute;taill&eacute;es?<br>
Par d&eacute;faut, 100 seront visibles. Il est conseill&eacute; de ne pas d&eacute;passer 500 pour ne pas surcharger les pages.",

"config_bbc_maxos" =>
"Combien de SE (syst&egrave;mes d'exploitation) doivent &ecirc;tre classifi&eacute;s dans les statistiques globales?",

"config_bbc_maxbrowser" =>
"Combien de navigateurs doivent &ecirc;tre classifi&eacute;s dans les statistiques globales?",

"config_bbc_maxextension" =>
"Combien d'extensions doivent &ecirc;tre classifi&eacute;es dans les statistiques globales?",

"config_bbc_maxrobot" =>
"Combien de robots doivent &ecirc;tre classifi&eacute;s dans les statistiques globales?",

"config_bbc_maxpage" =>
"Combien de pages doivent &ecirc;tre classifi&eacute;es dans les statistiques globales?",

"config_bbc_maxorigin" =>
"Combien d'origines doivent &ecirc;tre classifi&eacute;es dans les statistiques globales?",

"config_bbc_ignoreip" =>
"Quelles adresses IP (ou sous-r&eacute;seaux) doivent &ecirc;tre ignor&eacute;es dans les statistiques?<br>
<i>Format:</i> &lt;adresse IP ou sous-r&eacute;seau&gt;, &lt;autre adresse IP ou sous-r&eacute;seau&gt;<br>
Ins&egrave;rez une virgule \",\" entre chaque adresse.",

"config_bbc_ignore_refer" =>
"Ecrivez ici les sites que vous ne voulez pas voir classer dans la liste des premiers referrers. 
Ceux-ci seront enregistr&eacute;s dans la cat&eacute;gorie \"non sp&eacute;cifi&eacute;\" de sorte qu'aucune connection ne soit 
perdue dans les statistiques. Les format &agrave; employer est :<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
et cetera.",

"config_bbc_own_refer" =>
"Si cette option est activ&eacute;e, tous les referrers originaires du serveur sur lequel est ex&eacute;cut&eacute; BBClone 
seront not&eacute; \"http://www.monserveur.com/\" (&agrave; remplacer par le nom de votre serveur) dans le classement 
des referrers. Ceci trouve son int&eacute;r&ecirc;t lorsqu'on veut &eacute;viter l'affichage dans les classements des 
&eacute;ventuelles pages d'administration, des r&eacute;pertoires prot&eacute;g&eacute;s et autre informations cl&eacute;s que vous 
preferrez garder pour vous m&ecirc;me.",

"config_bbc_no_string" => "Habituellement BBClone &eacute;crit un commentaire dans le code html 
de vos pages indiquant son &eacute;tat de fonctionnement. Ceci peut cependant interf&eacute;rer avec 
la configuration d'autres programmes (forums, content management system, ...). Si vous &ecirc;tes 
confront&eacute; &agrave; une page blanche ou &agrave; une s&eacute;rie de message d'erreurs du type \"header already sent by\"
vous devez d&eacute;commentez cette option pour que tout refonctionnne.",

"config_bbc_detailed_stat_fields" =>
"La variable \$BBC_DETAILED_STAT_FIELDS d&eacute;termine les champs &agrave; afficher dans show_detailed.php.<br>
Les champs permis sont:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extension)<br>
L'ordre d'apparition des champs est important.<br>
Les champs non pr&eacute;sent seront manquants sur la page de statistiques d&eacute;taill&eacute;es.<br>
<br>
<i>Exemples:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Vous pouvez dans cette variable attribuer le style d'alignement global des pages de BBClone. 
Les valeurs possibles sont \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"Le niveau de taille des titres entre 0 (plus petit) et 6 (plus grand)",

"config_bbc_subtitle_size" =>
"Le niveau de taille des soustitres entre 0 (plus petit) et 6 (plus grand)",

"config_bbc_text_size" =>
"Le niveau de taille des textes entre 0 (plus petit) et 6 (plus grand)"
);
?>