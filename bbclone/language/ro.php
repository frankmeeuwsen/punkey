<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/ro.php,v 1.12 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: ro.php
* Summary: contine tabele de traducere in limba romana
* Author: ciprian manea ciprian.manea@welho.com
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-2",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "Echipa BBClone - licenta",
"global_yes" => "da",
"global_no" => "nu",

// The error messages
"error_cannot_see_config" =>
"Nu ai dreptul sa vezi configuratia BBClone a acestui server.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Alt", "ext_com" => "Commercial",
"ext_net" => "Retele", "ext_edu" => "Educational",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japonia", "ext_us" => "Statele Unite",
"ext_uk" => "Marea Britanie", "ext_de" => "Germania",
"ext_mil" => "US Militar", "ext_ca" => "Canada",
"ext_it" => "Italia", "ext_au" => "Australia",
"ext_org" => "Organizatii", "ext_nl" => "Olanda",
"ext_fr" => "Franta", "ext_tw" => "Taiwan",
"ext_gov" => "Guvernamental", "ext_fi" => "Finlanda",
"ext_br" => "Brazilia", "ext_se" => "Suedia",
"ext_es" => "Spania", "ext_no" => "Norvegia",
"ext_mx" => "Mexic", "ext_kr" => "Corea",
"ext_ch" => "Elvetia", "ext_dk" => "Danemarca",
"ext_be" => "Belgia", "ext_at" => "Austria",
"ext_nz" => "Noua Zeelanda", "ext_ru" => "Federatia Rusa",
"ext_pl" => "Polonia", "ext_za" => "Africa de Sud",
"ext_unknown" => "Necunoscut", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapore",
"ext_arpa" => "Greseli", "ext_cz" => "Cehia",
"ext_hu" => "Ungaria", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugalia", "ext_tr" => "Turcia",
"ext_gr" => "Grecia", "ext_cn" => "China",
"ext_ie" => "Irlanda", "ext_my" => "Malaezia",
"ext_th" => "Tailanda", "ext_cl" => "Chile",
"ext_co" => "Columbia", "ext_is" => "Islanda",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ucraina",
"ext_sk" => "Slovacia", "ext_ro" => "Romania",
"ext_ae" => "Emiratele Arabe Unite", "ext_id" => "Indonezia",
"ext_su" => "Uniunea Sovietica", "ext_si" => "Slovenia",
"ext_hr" => "Croatia", "ext_ph" => "Filipine",
"ext_lv" => "Letonia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgaria", "ext_lt" => "Lituania",
"ext_yu" => "Yugoslavia", "ext_lu" => "Luxemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Organizatii Internationale",
"ext_do" => "Republica Dominicana", "ext_cy" => "Cipru",
"ext_pk" => "Pakistan", "ext_cc" => "Insulele Cocos",
"ext_tt" => "Trinidad Tobago", "ext_eg" => "Egipt",
"ext_lb" => "Liban", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakhstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermude", "ext_sa" => "Arabia Saudita",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Insula Craciunului", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Groenlanda",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Insulele Faroe",
"ext_ac" => "Insula Inaltarii",
"ext_pr" => "Porto Rico",
"ext_am" => "Armenia", "ext_pf" => "Polinezia Franceza",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "Salvador", "ext_ma" => "Maroc",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnia si Hertegovina", "ext_om" => "Oman",
"ext_jo" => "Iordania", "ext_ir" => "Iran",
"ext_st" => "Sao Tome si Principe", "ext_vi" => "Insulele Virgine (US)",
"ext_ci" => "Coasta de Fildes", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Insulele Cayman",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Samoa Americana", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Insulele Heard si Mc Donald", "ext_fm" => "Micronezia",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinica",
"ext_ai" => "Angola", "ext_pg" => "Papua Noua Guinee",
"ext_bz" => "Belize", "ext_sh" => "Sfanta Elena",
"ext_aw" => "Aruba", "ext_mv" => "Maldive",
"ext_nc" => "Noua Caledonie", "ext_ag" => "Antigua si Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tadjikistan",
"ext_sb" => "Insulele Solomon", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodgia",
"ext_tc" => "Insulele Turks si Caicos",
"ext_tf" => "Teritoriile Franceze de Sud", "ext_az" => "Azerbaijan",
"ext_dm" => "Dominica", "ext_mz" => "Mozambic",
"ext_mo" => "Macao", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Guiana Franceza",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Insulele Norfolk", "ext_io" => "Teritoriile Engleze din Oceanul Indian",
"ext_gs" => "Georgia de Sud si Insulele Sandwich de Sud", "ext_ye" => "Yemen",
"ext_an" => "Antilele Olandeze", "ext_aq" => "Antarctica",
"ext_tn" => "Tunisia", "ext_ck" => "Insulele Cook",
"ext_ls" => "Lesoto", "ext_et" => "Etiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Insulele Virgine Britanice", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Insula Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Marea Britanie", "ext_bs" => "Bahamas",
"ext_va" => "Vatican", "ext_lc" => "Sfanta Lucia",
"ext_cd" => "Congo", "ext_gm" => "Gambia",
"ext_mp" => "Insulele Mariane de Nord", "ext_gw" => "Guineea-Bissau",
"ext_cm" => "Camerun", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libia",
"ext_cf" => "Republica Central-Africana", "ext_mm" => "Myanmar",
"ext_td" => "Ciad", "ext_iq" => "Irak",
"ext_kn" => "Sf. Kitts si Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afganistan",
"ext_cv" => "Insula Capul Verde", "ext_mh" => "Insulele Marshall",
"ext_pm" => "Sf. Pierre si Miquelon", "ext_so" => "Somalia",
"ext_vc" => "Sf. Vincent si Grenadine", "ext_bd" => "Banglades",
"ext_gn" => "Guineea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Island",
"ext_fk" => "Insulele Falkland (Malvine)", "ext_gq" => "Guineea Ecuatoriala",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard si Insulele Jan Mayen",
"ext_sr" => "Surinam", "ext_sy" => "Siria",
"ext_tp" => "Timorul de Est", "ext_um" => "Insulele Minor Outlying (US)",
"ext_wf" => "Insulele Wallis si Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Zair", "ext_IP" => "Numeric",

// Miscellaneous translations
"misc_other" => "Alt",
"misc_unknown" => "Necunoscut",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Situl Principal",
"navbar_Configuration" => "Configurare",
"navbar_Global_Stats" => "Statistici Generale",
"navbar_Detailed_Stats" => "Statistici Detaliate",
"navbar_Time_Stats" => "Statistici in Timp",
"navbar_Link_Stats" => "Statistici Link",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Timp",
"dstat_Visits" => "Vizite",
"dstat_Extension" => "Extensie",
"dstat_DNS" => "Hostname",
"dstat_From" => "De la",
"dstat_OS" => "OS",
"dstat_Browser" => "Browser",
"dstat_New_access" => "Accesari Noi",
"dstat_Elapsed_time" => "Timp trecut",
"dstat_No_new_access" => "Fara noi accese",
"dstat_Visible_accesses" => "Accesari vizibile",
"dstat_green_rows" => "randuri verzi",
"dstat_blue_rows" => "randuri albastre",
"dstat_red_rows" => "randuri rosii",
"dstat_last_visit" => "ultima vizita",
"dstat_robots" => "roboti",

// Global stats words

"gstat_Accesses" => "Accese",
"gstat_Total_visits" => "Total vizite",
"gstat_Total_unique" => "Total unici",
"gstat_New_visits" => "Vizitatori noi",
"gstat_New_unique" => "Vizitatori noi unici",
"gstat_Blacklisted" => "Trecuti pe lista neagra",
"gstat_Operating_systems" => "Sisteme de operare",
"gstat_Browsers" => "Browsere",
"gstat_n_first_extensions" => "Primele %d extensii",
"gstat_Robots" => "Roboti",
"gstat_n_first_pages" => "Primele %d pagini",
"gstat_n_first_origins" => "Primele %d origini",
"gstat_Total" => "Total",
"gstat_Not_specified" => "Nespecificat",

// Time stats words
"tstat_Su" => "Duminica",
"tstat_Mo" => "Luni",
"tstat_Tu" => "Marti",
"tstat_We" => "Miercuri",
"tstat_Th" => "Joi",
"tstat_Fr" => "Vineri",
"tstat_Sa" => "Sambata",

"tstat_Jan" => "Ian",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Apr",
"tstat_May" => "Mai",
"tstat_Jun" => "Iun",
"tstat_Jul" => "Iul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Oct",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dec",

"tstat_Last_day" => "Ultima zi",
"tstat_Last_week" => "Ultima saptamana",
"tstat_Last_month" => "Ultima luna",
"tstat_Last_year" => "Ultimul an",

// Configuration page words and sentences

"config_Variable_name" => "Nume variabila",
"config_Variable_value" => "Valoare variabila",
"config_Explanations" => "Explicatii",

"config_bbc_mainsite" =>
"Introduceti URL-ul site-ului dvs.<br>
Lasati campul liber daca doriti sa nu apara nici un URL pe barele de navigare ale contorului.<br>
<br>
<i>Exemplu:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"Stabileste daca configuratia va fi vizibila sau nu prin intermediul paginii show_config.php",

"config_bbc_titlebar" =>
"Titlul care apare in toate paginile contorului.<br>
Cuvintele cheie sunt:<br>
<ul>
<li>%SERVER: numele server-ului,
<li>%DATE: data curenta.
</ul>
Codul HTML este de asemenea permis.",

"config_bbc_language" =>
"Limba pe care vrei sa o folosesti. Implicit este engleza.<br>
Pentr a vedea traducerile disponibile, vezi 
<a href=\"http://bbclone.de\">zona download</a> a site-ului BBClone.",

"config_bbc_maxtime" =>
"Intervalul de timp (in secunde) intre 2 vizite diferite originate de aceeasi pereche IP/AGENT.<br>
Valoarea implicita este: 1800 s",

"config_bbc_maxvisible" =>
"Cate intrari vrei sa vezi in statisticile detaliate?<br>
Initial valoarea este 100, dar nu o setati mai mare de 500 (valoarea maxima recomandata)",

"config_bbc_maxos" =>
"How many OS do you want to see specified in the global stats?",

"config_bbc_maxbrowser" =>
"How many browsers do you want to see specified in the global stats?",

"config_bbc_maxextension" =>
"Cate extensii vrei sa fie clasificate in statisiticile generale?",

"config_bbc_maxrobot" =>
"How many robots do you want to see specified in the global stats?",

"config_bbc_maxpage" =>
"Cate pagini vrei sa fie clasificate in statisticile generale?",

"config_bbc_maxorigin" =>
"Cate origini vrei sa fie clasificate in statisticile generale?",

"config_bbc_ignoreip" =>
"Ce adrese IP (sau retele) vrei sa fie ignorate?<br>
<i>Format:</i> &lt;Adresa IP sau retea&gt;, &lt;alta adresa IP sau retea&gt;<br>
Adauga o virgula \",\" intre adresele IP. Implicit este IPul \"local\".",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"Variabila \$BBC_DETAILED_STAT_FIELDS stabileste campurile care vor fi afisate in show_detailed.php.<br>
Nume posibile de campuri sunt:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extensie)<br>
Ordinea in care sunt specificate aceste campuri este importanta.<br>
Campurile nespecificate vor lipsi din pagina de statistici.<br>
<br>
<i>Exemple:</i><br>

\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Aliniamentul paginii principale se poate configura aici. 
Valorile posibile sunt \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"Dimensiunea titlurilor intre 0 (cel mai mic) si 6 (cel mai mare)",

"config_bbc_subtitle_size" =>
"Dimensiunea subtitlurilor intre 0 (cel mai mic) si 6 (cel mai mare)",

"config_bbc_text_size" =>
"Dimensiunea textului intre 0 (cel mai mic) si 6 (cel mai mare)"

);
?>