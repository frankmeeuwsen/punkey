<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/tr.php,v 1.4 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: tr.php
* Summary: Contains a Turkish translation table
* Description: v1.0
* Author: Serkan Sisman, fatcoins@yahoo.com
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "windows-1254",

// Date format (used with date() )
"global_date_format" => "d-m-Y",

// Global translation
"global_bbclone_copyright" => "BBClone tak�m� ad�na Lisansl�d�r.",
"global_yes" => "evet",
"global_no" => "hay�r",

// The error messages
"error_cannot_see_config" =>
"Server'daki BBClone ayarlar�n�n do�ru olmad��� i�in bu sayfay� g�remiyorsunuz.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Di�er", "ext_com" => "Ticari",
"ext_net" => "Net A��", "ext_edu" => "E�itimsel",
"ext_biz" => "��", "ext_info" => "Bilgi",
"ext_jp" => "Japonya", "ext_us" => "A.B.D",
"ext_uk" => "�ngiltere", "ext_de" => "Almanya",
"ext_mil" => "A.B.D Askeri", "ext_ca" => "Kanada",
"ext_it" => "�talya", "ext_au" => "Avusturalya",
"ext_org" => "Kurulu�lar", "ext_nl" => "Hollanda",
"ext_fr" => "Fransa", "ext_tw" => "Tayvan",
"ext_gov" => "H�k�met", "ext_fi" => "Finlandiya",
"ext_br" => "Brezilya", "ext_se" => "Isve�",
"ext_es" => "�spanya", "ext_no" => "Norve�",
"ext_mx" => "Meksika", "ext_kr" => "Kore",
"ext_ch" => "�svi�re", "ext_dk" => "Danimarka",
"ext_be" => "Bel�ika", "ext_at" => "Avusturya",
"ext_nz" => "Yeni Zellanda", "ext_ru" => "Rusya",
"ext_pl" => "Polonya", "ext_za" => "G�ney Afrika",
"ext_unknown" => "Bilinmeyen", "ext_ar" => "Arjantin",
"ext_il" => "�srail", "ext_sg" => "Singapur",
"ext_arpa" => "Hatalar", "ext_cz" => "�ek Cumhuriyeti",
"ext_hu" => "Macaristan", "ext_hk" => "Honk Hong",
"ext_pt" => "Portekiz", "ext_tr" => "T�RK�YE",
"ext_gr" => "Yunanistan", "ext_cn" => "�in",
"ext_ie" => "�rlanda", "ext_my" => "Malezya",
"ext_th" => "Tayland", "ext_cl" => "�ili",
"ext_co" => "Kolombiya", "ext_is" => "�zlanda",
"ext_uy" => "Uruguay", "ext_ee" => "Estonya",
"ext_in" => "Hindistan", "ext_ua" => "Ukrayna",
"ext_sk" => "Slovakya", "ext_ro" => "Romanya",
"ext_ae" => "Birle�ik Arap Emirlikleri", "ext_id" => "Endonezya",
"ext_su" => "Sovyetler Birli�i", "ext_si" => "Slovenya",
"ext_hr" => "H�rvatistan", "ext_ph" => "Filipinler",
"ext_lv" => "Letonya", "ext_ve" => "Venezuella",
"ext_bg" => "Bulgaristan", "ext_lt" => "Litvanya",
"ext_yu" => "Yugoslavya", "ext_lu" => "L�ksemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Kosta Rika", "ext_int" => "Uluslararas� Kurulu�lar",
"ext_do" => "Dominik Cumhuriyeti", "ext_cy" => "G�ney K�br�s",
"ext_pk" => "Pakistan", "ext_cc" => "Kokos Adalar�",
"ext_tt" => "Trinidad ve Tobago", "ext_eg" => "M�s�r",
"ext_lb" => "L�bnan", "ext_kw" => "Kuveyt",
"ext_to" => "Tonga", "ext_kz" => "Kazakistan",
"ext_na" => "Namibya", "ext_mu" => "Maritus",
"ext_bm" => "Bermuda", "ext_sa" => "Suudi Arabistan",
"ext_zw" => "Zimbabve", "ext_kg" => "K�rg�zistan",
"ext_cx" => "Noel Adas�", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botsvana",
"ext_mk" => "Makedonya", "ext_gl" => "Gr�nland Adas�",
"ext_ec" => "Ekvator", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivya", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Faroe Adalar�",
"ext_ac" => "Y�kselme Adas�", "ext_pr" => "Porto Riko",
"ext_am" => "Ermenistan", "ext_pf" => "Frans�z Polonezyas�",
"ext_ge" => "G�rcistan", "ext_bh" => "Bahreyn",
"ext_ni" => "Nikaragua", "ext_by" => "Beyaz Rusya",
"ext_sv" => "El Salvador", "ext_ma" => "Fas",
"ext_ke" => "Kenya", "ext_ad" => "Andor Prensli�i",
"ext_zm" => "Zambiya", "ext_np" => "Nepal",
"ext_bt" => "Butan", "ext_sz" => "Svaziland",
"ext_ba" => "Bosna Hersek", "ext_om" => "Umman",
"ext_jo" => "�rd�n", "ext_ir" => "�ran",
"ext_st" => "Sao Tome ve Principe", "ext_vi" => "Bakire Adalar� (A.B.D)",
"ext_ci" => "Fildi�i Sahilleri", "ext_jm" => "Jamaika",
"ext_li" => "Lihten�tayn", "ext_ky" => "Seymen Adalar�",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagaskar",
"ext_gi" => "Cebelitar�k", "ext_sm" => "San Marino",
"ext_as" => "Amerikan Samoa", "ext_tz" => "Tanzanya",
"ext_ws" => "Samoa", "ext_tm" => "T�rkmenistan",
"ext_mc" => "Monako", "ext_sn" => "Senegal",
"ext_hm" => "Heard ve Mc Donald Adalar�", "ext_fm" => "Mikronezya",
"ext_fj" => "Fiji", "ext_cu" => "K�ba",
"ext_rw" => "Ruanda", "ext_mq" => "Martinik",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Yeni Gine",
"ext_bz" => "Beliz", "ext_sh" => "Aziz Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldivler",
"ext_nc" => "Yeni Kaledonya", "ext_ag" => "Antigua ve Barbuda",
"ext_uz" => "�zbekistan", "ext_tj" => "Tacikistan",
"ext_sb" => "Solomon Adalar�", "ext_bf" => "Burkina Faso",
"ext_kh" => "Kambo�ya", "ext_tc" => "Turks ve Caicos Adalar�",
"ext_tf" => "Frans�z G�ney B�lgeleri", "ext_az" => "Azerbaycan",
"ext_dm" => "Dominik", "ext_mz" => "Mozambik",
"ext_mo" => "Makao", "ext_vu" => "Vanuatu",
"ext_mn" => "Mo�olistan", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Nijer", "ext_gf" => "Frans�z Guyanas�",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Arnavutluk", "ext_gh" => "Gana",
"ext_nf" => "Norfolk Adas�", "ext_io" => "�ngiliz Hint Okyanusu B�lgesi",
"ext_gs" => "G�ney G�rcistan ve G�ney Sandvi� Adalar�", "ext_ye" => "Yemen",
"ext_an" => "Hollanda Antilleri", "ext_aq" => "Antartika",
"ext_tn" => "Tunus", "ext_ck" => "Yemek Adalar�",
"ext_ls" => "Lesoto", "ext_et" => "Etiyopya",
"ext_ng" => "Nijerya", "ext_sl" => "Siera Leon",
"ext_bb" => "Barbados", "ext_je" => "Jerse Adas�",
"ext_vg" => "Bakire Adalar� (�ngiliz)", "ext_vn" => "Vietnam",
"ext_mr" => "Moritanya", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djiboti",
"ext_km" => "Komor Adalar�", "ext_dz" => "Cezayir",
"ext_im" => "Adam Adas�", "ext_pn" => "Pitcairn Adalar�",
"ext_qa" => "Katar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "�ngiltere", "ext_bs" => "Bahama",
"ext_va" => "Vatikan", "ext_lc" => "Aziz Lucia",
"ext_cd" => "Kongo Demoktarik Cum.", "ext_gm" => "Gambiya",
"ext_mp" => "Kuzey Mariana Adalar�", "ext_gw" => "Gine-Bisav",
"ext_cm" => "Kamerun", "ext_ao" => "Angola",
"ext_er" => "Eritre", "ext_ly" => "Libya",
"ext_cf" => "Orta Afrika Cumhuriyeti", "ext_mm" => "Miyanmar",
"ext_td" => "�ad", "ext_iq" => "Irak",
"ext_kn" => "Aziz Kitts ve Nevis", "ext_sc" => "Sey�eller",
"ext_cg" => "Kongo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afganistan",
"ext_cv" => "Cabo Verde", "ext_mh" => "Mar�al Adalar�",
"ext_pm" => "Aziz Pierre ve Miquelon", "ext_so" => "Somali",
"ext_vc" => "Aziz Vincent ve the Grenadines", "ext_bd" => "Banglade�",
"ext_gn" => "Gine", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberya",
"ext_mw" => "Malavi", "ext_pw" => "Palau",
"ext_re" => "Birle�me", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Adas�",
"ext_fk" => "Falkland Adalar� (Malvinas)", "ext_gq" => "Ekvatoryal Gine",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard ve Jan Mayen Adalar�",
"ext_sr" => "Surinam", "ext_sy" => "Suriye",
"ext_tp" => "Do�u Timor", "ext_um" => "A.B.D K���k Uzak Adalar�",
"ext_wf" => "Wallis ve Futuna Adalar�", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire Cumhuriyeti", "ext_IP" => "Say�sal",

// Miscellaneous translations
"misc_other" => "Di�er",
"misc_unknown" => "Bilinmeyen",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Ana Sayfa",
"navbar_Configuration" => "Bi�im",
"navbar_Global_Stats" => "Kapsaml� �statistikler",
"navbar_Detailed_Stats" => "Ayr�nt�l� �statistikler",
"navbar_Time_Stats" => "Zaman �statistikleri",
"navbar_Link_Stats" => "Ba�lant� �statistikleri",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Zaman",
"dstat_Visits" => "Ziyaretler",
"dstat_Extension" => "Uzant�",
"dstat_DNS" => "Sunucu Ad�",
"dstat_From" => "Nereden",
"dstat_OS" => "��letim Sistemi",
"dstat_Browser" => "Taray�c�",
"dstat_New_access" => "Yeni giri�ler",
"dstat_Elapsed_time" => "Ge�en Zaman",
"dstat_No_new_access" => "Yeni Olmayan giri�ler",
"dstat_Visible_accesses" => "G�r�n�r giri�ler",
"dstat_green_rows" => "ye�il sat�rlar",
"dstat_blue_rows" => "mavi sat�rlar",
"dstat_red_rows" => "k�rm�z� sat�rlar",
"dstat_last_visit" => "son ziyaret",
"dstat_robots" => "robotlar",

// Global stats words

"gstat_Accesses" => "Giri�ler",
"gstat_Total_visits" => "Toplam Ziyaretler",
"gstat_Total_unique" => "Toplam Tekil Ziyaretler",
"gstat_New_visits" => "Yeni Ziyaretler",
"gstat_New_unique" => "Yeni Tekil Ziyaretler",
"gstat_Blacklisted" => "Karaliste'dekiler",
"gstat_Operating_systems" => "En y�ksek %d ��letim Sistemi",
"gstat_Browsers" => "En y�ksek %d Taray�c�",
"gstat_n_first_extensions" => "En y�ksek %d Uzant�",
"gstat_Robots" => "En y�ksek %d Robot",
"gstat_n_first_pages" => "En y�ksek %d Ziyaret Edilen Sayfalar",
"gstat_n_first_origins" => "En y�ksek %d Kaynak",
"gstat_Total" => "Toplam",
"gstat_Not_specified" => "Belirlenmemi�",

// Time stats words
"tstat_Su" => "Paz",
"tstat_Mo" => "Pzt",
"tstat_Tu" => "Sal",
"tstat_We" => "�ar",
"tstat_Th" => "Per",
"tstat_Fr" => "Cum",
"tstat_Sa" => "Cmt",

"tstat_Jan" => "Oca",
"tstat_Feb" => "�ub",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Nis",
"tstat_May" => "May",
"tstat_Jun" => "Haz",
"tstat_Jul" => "Tem",
"tstat_Aug" => "Agu",
"tstat_Sep" => "Eyl",
"tstat_Oct" => "Eki",
"tstat_Nov" => "Kas",
"tstat_Dec" => "Ara",

"tstat_Last_day" => "�imdiki g�n",
"tstat_Last_week" => "�imdiki hafta",
"tstat_Last_month" => "�imdiki ay",
"tstat_Last_year" => "�imdiki y�l",

// Configuration page words and sentences

"config_Variable_name" => "De�i�ken ad�",
"config_Variable_value" => "De�i�ken de�eri",
"config_Explanations" => "A��klama",

"config_bbc_mainsite" =>
"Web sitenin adresi.<br>
E�er bo� b�rak�rsan, Adresin BBClone sayfalar�nda g�r�nmez.<br>
<br>
<i>�rnek:</i><br>
\$BBC_MAINSITE = \"http://www.benimwebsunucum.com/herhangi/\".",

"config_bbc_show_config" =>
"Bilgilerin do�ru g�z�kmesi i�in show_config.php 'deki bilgilerin do�ru olup olmad���n� kontrol ediniz .",

"config_bbc_titlebar" =>
"Burdaki ba�l�k b�t�n BBClone sayfalar�nda g�r�necek.<br>
The following macros are recognized:<br>
<ul>
<li>%SERVER: server ad�,
<li>%DATE: �u anki tarih.
</ul>
HTML etiketinin izin verdi�i gibi.",

"config_bbc_language" =>
"Hangi dilde kullanmay� istiyorsan onu se�ebilirsin. varsay�lan dil ingilizce'dir.<br>
mevcut diller hakk�nda bilgi almak istiyorsan�z, BBClone websitesinin <a href=\"http://bbclone.de\">y�kleme 
b�l�m�ne</a> bakabilirsiniz.",

"config_bbc_maxtime" =>
"2 farkl� ip'den fakat ayn� ip-ajan�ndan giri�in belirlenmesi i�in zaman periyodu.<br>
Varsay�lan g�sterim standard�: 1800 s",

"config_bbc_maxvisible" =>
"Ayr�nt�l� istatistiklerinde en fazla ka� giri�in g�r�nmesini istersiniz?<br>
Varsay�lan de�er 100'd�r. L�tfen 500'den fazla bir de�er se�meyin (script a��s�ndan sak�ncal� olabilir)",

"config_bbc_maxos" =>
"Kapsaml� istatistikler sayfas�nda en fazla ka� <b>��letim sistemi</b>'nin g�r�nmesini istersiniz?",

"config_bbc_maxbrowser" =>
"Kapsaml� istatistikler sayfas�nda en fazla ka� <b>Taray�c�</b>'n�n g�r�nmesini istersiniz?",

"config_bbc_maxextension" =>
"Kapsaml� istatistikler sayfas�nda en fazla ka� <b>Uzant�</b>'n�n g�r�nmesini istersiniz?",

"config_bbc_maxrobot" =>
"Kapsaml� istatistikler sayfas�nda en fazla ka� <b>Robot</b>'un g�r�nmesini istersiniz?",

"config_bbc_maxpage" =>
"Kapsaml� istatistikler sayfas�nda en fazla ka� <b>Sayfa istatisti�inin</b>'un g�r�nmesini istersiniz?",

"config_bbc_maxorigin" =>
"Kapsaml� istatistikler sayfas�nda en fazla ka� <b>Kaynak</b> g�r�nmesini istersiniz?",

"config_bbc_ignoreip" =>
"Hangi IP adresinin (veya alt a�) yoksay�lmas�n� istersiniz?<br>
<i>Bi�im:</i> &lt;IP adresi veya alta� , ba�ka IP adresi veya alt a�<br>
Aras�na bir virg�l koyunuz \",\" herbir IP aras�da virg�l olmal�. Varsay�lan �o�unluka \"local\" IP.",

"config_bbc_ignore_refer" =>
"E�er birden fazla siteniz varsa ve baz�lar�n�n sayac�n�z�n kaynak listesinde g�r�nmemesini istiyorsan�z
, sunucu ad'lar�n� buraya ekleyebilirsiniz. Kaynak listeniz bunu g�ze alacak
\"belirlenmemi�\" b�ylece gereksiz hitler listelenmiyecek. A�a��daki bi�im'i kullanabilrsin:<br />
\$BBC_IGNORE_REFER = \"www.sunucu1.org, farkl�.sunucu2.org, farkl�.sunucu3.org\";",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your
server name) in the referrer ranking. This is useful if you don't want
bbclone to list paths to administrative pages, protected directories or other
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its
current state. However this output, though convenient, may interfere with some forums
or content management systems. If you're confronted with a blank page or a couple of \"header
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"Bu de�er \$BBC_DETAILED_STAT_FIELDS kolonlardaki de�erlerin yerlerini
show_detailed.php i�inde de�i�tirebilmenizi sa�lar.<br>
M�mk�n (tan�ml�) kolon isimleri ve a��klamalar�:<br>
<ul>
<li>\"id\" - No de�eri, </li>
<li>\"time\" - Zaman,</li>
<li>\"visits\" - Ziyaretler, </li>
<li>\"dns\" - Sunucu adresleri, </li>
<li>\"referer\" - Kaynak, sayfaya nereden gelindi�i, </li>
<li>\"os\" - ��letim sistemleri,</li>
<li>\"browser\" - Taray�c� (internet gezgini), </li>
<li>\"ext\" - Uzant�lar (hangi �lkeden orijinden gelindi�i.</li>
</ul>
bu d�zen i�in kolon adlar� �ok �nemlidir.<br>
tan�ms�z kolon adlar� istatistik sayfan�zda g�r�nmeyebilir.<br>
<br>
<i>�rnek �al��malar:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Buradaki de�er yard�m� ile Ana �statistik sayfandaki g�r�n�m �eklini de�i�tirebilirsiniz.
Tan�ml� de�erler: \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"Ba�l�klar' boyutu aras�nda 0 (enk���k) ve 6 (enb�y�k)",

"config_bbc_subtitle_size" =>
"Alt ba�l�klar' boyutu aras�nda 0 (enk���k) ve 6 (enb�y�k)",

"config_bbc_text_size" =>
"Basit yaz�lar: boyutu aras�nda 0 (enk���k) ve 6 (enb�y�k)"

);
?>