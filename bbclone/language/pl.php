<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/pl.php,v 1.13 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: pl.php
* Summary: contains a polish translation table
* Description: ver 2.0
* Author: Dariusz Kramin, kramer@irc.pl 
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-2",

// Date format (used with date() )
"global_date_format" => "Y/m/d",

// Global translation
"global_bbclone_copyright" => "Grupa BBClone, wersja polska <a href=mailto:kramer@irc.pl>Dariusz Kramin</a> - Na licencji",
"global_yes" => "tak",
"global_no" => "nie",

// The error messages
"error_cannot_see_config" =>
"Nie posiadasz uprawnie� do ogl�dania konfiguracji statystyk.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Inne", "ext_com" => "Komercyjne",
"ext_net" => "Sieci", "ext_edu" => "Edukacja",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japonia", "ext_us" => "Stany Zjednoczone",
"ext_uk" => "Zjednoczone Kr�lestwo", "ext_de" => "Niemcy",
"ext_mil" => "Wojsko USA", "ext_ca" => "Kanada",
"ext_it" => "W�ochy", "ext_au" => "Australia",
"ext_org" => "Organizacje", "ext_nl" => "Holandia",
"ext_fr" => "Francja", "ext_tw" => "Tajwan",
"ext_gov" => "Rz�dy", "ext_fi" => "Finlandia",
"ext_br" => "Brazylia", "ext_se" => "Szwecja",
"ext_es" => "Hiszpania", "ext_no" => "Norwegia",
"ext_mx" => "Meksyk", "ext_kr" => "Korea",
"ext_ch" => "Szwarcajria", "ext_dk" => "Dania",
"ext_be" => "Belgia", "ext_at" => "Austria",
"ext_nz" => "Nowa Zelandia", "ext_ru" => "Federacja Rosyjska",
"ext_pl" => "Polska", "ext_za" => "Po�udniowa Afryka",
"ext_unknown" => "Nieznany", "ext_ar" => "Argentyna",
"ext_il" => "Izrael", "ext_sg" => "Singapur",
"ext_arpa" => "B��d DNS", "ext_cz" => "Czechy",
"ext_hu" => "W�gry", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugalia", "ext_tr" => "Turcja",
"ext_gr" => "Grecja", "ext_cn" => "Chiny",
"ext_ie" => "Irlandia", "ext_my" => "Malezja",
"ext_th" => "Tailandia", "ext_cl" => "Czile",
"ext_co" => "Kolumbia", "ext_is" => "Islandia",
"ext_uy" => "Urugwaj", "ext_ee" => "Estonia",
"ext_in" => "Indie", "ext_ua" => "Ukraina",
"ext_sk" => "S�owacja", "ext_ro" => "Rumunia",
"ext_ae" => "Zjednoczone Emiraty Arabskie", "ext_id" => "Indonezja",
"ext_su" => "Zwi�zek Radziecki", "ext_si" => "S�owenia",
"ext_hr" => "Chorwacja", "ext_ph" => "Filipiny",
"ext_lv" => "�otwa", "ext_ve" => "Wenezuela",
"ext_bg" => "Bu�garia", "ext_lt" => "Litwa",
"ext_yu" => "Jugos�awia", "ext_lu" => "Luxemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Kostaryka", "ext_int" => "Organizacje",
"ext_do" => "Dominikana", "ext_cy" => "Cypr",
"ext_pk" => "Pakistan", "ext_cc" => "Wyspy Kokosowe",
"ext_tt" => "Trinidad i Tobago", "ext_eg" => "Egipt",
"ext_lb" => "Lebanon", "ext_kw" => "Kuwejt",
"ext_to" => "Tonga", "ext_kz" => "Kazahstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermudy", "ext_sa" => "Arabia Saudyjska",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kirgistan",
"ext_cx" => "Wyspy Boego Narodzenia", "ext_pa" => "Panama",
"ext_gt" => "Gwatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Grenlandia",
"ext_ec" => "Ekwador", "ext_lk" => "Sri Lanka",
"ext_md" => "Mo�dawia", "ext_py" => "Paragwaj",
"ext_bo" => "Boliwia", "ext_bn" => "Brunea",
"ext_mt" => "Malta", "ext_fo" => "Wyspy Faroe",
"ext_ac" => "Wyspa Ascension", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenia", "ext_pf" => "Polinezja Fracuska",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nikaragua", "ext_by" => "Bia�oru�",
"ext_sv" => "Salwador", "ext_ma" => "Maroko",
"ext_ke" => "Kenja", "ext_ad" => "Andora",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bo�nia i Hercegowina", "ext_om" => "Oman",
"ext_jo" => "Jordania", "ext_ir" => "Iran",
"ext_st" => "Sao Tome", "ext_vi" => "Wyspy Dziewicze (U.S.)",
"ext_ci" => "Wybrze�e Ko�ci S�oniowej", "ext_jm" => "Jamajka",
"ext_li" => "Lichtenstein", "ext_ky" => "Kajmany",
"ext_gp" => "Gwadelupa", "ext_mg" => "Madagaskar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Samoa Ameryka�ska", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monako", "ext_sn" => "Senegal",
"ext_hm" => "Wyspu Mc Donalda", "ext_fm" => "Mikronezja",
"ext_fj" => "Fiji", "ext_cu" => "Kuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinika",
"ext_ai" => "Anguila", "ext_pg" => "Papua Nowa Gwinea",
"ext_bz" => "Belize", "ext_sh" => "Wyspa �wi�tej Heleny",
"ext_aw" => "Aruba", "ext_mv" => "Malediwy",
"ext_nc" => "Nowa Kaledonia", "ext_ag" => "Antigua i Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Wyspy Salomona", "ext_bf" => "Burkina Faso",
"ext_kh" => "Kamboda", "ext_tc" => "Wyspy Kaiko",
"ext_tf" => "Francuskie Terytorium Po�udniowe", "ext_az" => "Azerbejd�an",
"ext_dm" => "Dominika", "ext_mz" => "Mozambik",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Gujana Francuska",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Wyspy Norfolk", "ext_io" => "Brytyjskie Terytorium Oceanu Indyjskiego",
"ext_gs" => "Po�udniowa Georgia i Wyspy Sandwitch", "ext_ye" => "Jemen",
"ext_an" => "Antyle Holenderskie", "ext_aq" => "Antarktyka",
"ext_tn" => "Tunezja", "ext_ck" => "Wyspy Cooka",
"ext_ls" => "Lesotho", "ext_et" => "Etiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Brytyjskie Wyspy Dziewicze", "ext_vn" => "Vietnam",
"ext_mr" => "Mauretania", "ext_gy" => "Gujana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Komory", "ext_dz" => "Algeria",
"ext_im" => "Wyspa Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Katar", "ext_gg" => "Wyspa Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Zjednoczone Kr�lestwo Brytyjskie", "ext_bs" => "Bahamy",
"ext_va" => "Watykan", "ext_lc" => "�wi�ta ucja",
"ext_cd" => "Kongo", "ext_gm" => "Gambia",
"ext_mp" => "P�nocne Wyspy Mariana", "ext_gw" => "Gwinea-Bissau",
"ext_cm" => "Kamerun", "ext_ao" => "Angola",
"ext_er" => "Erytrea", "ext_ly" => "Libia",
"ext_cf" => "Republika Afyki", "ext_mm" => "Myanmar",
"ext_td" => "Czad", "ext_iq" => "Irak",
"ext_kn" => "Saint Kitts i Nevis", "ext_sc" => "Seszele",
"ext_cg" => "Kongo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afganistan",
"ext_cv" => "Cape Verde", "ext_mh" => "Wyspy Marshalla",
"ext_pm" => "St. Pierre i Miquelon", "ext_so" => "Somalia",
"ext_vc" => "St. Vincent i Grenadines", "ext_bd" => "Bangladesz",
"ext_gn" => "Gwinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Island",
"ext_fk" => "Falklandy", "ext_gq" => "Gwinea R�wnikowa",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard i wyspy Jan Mayen",
"ext_sr" => "Surinam", "ext_sy" => "Syria",
"ext_tp" => "Wschodni Timor", "ext_um" => "Pozaterytorialne wyspy USA",
"ext_wf" => "Wallis i wyspy Futuna", "ext_yt" => "Majott",
"ext_zr" => "Zair", "ext_IP" => "Bez revDNS",

// Miscellaneoux translations
"misc_other" => "Inne",
"misc_unknown" => "Nieznany",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Strona G��wna",
"navbar_Configuration" => "Konfiguracja",
"navbar_Global_Stats" => "G��wna statystyka",
"navbar_Detailed_Stats" => "Statystyka szczeg�owa",
"navbar_Time_Stats" => "Statystyka czasowa",
"navbar_Link_Stats" => "Statystyka link�w",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Czas",
"dstat_Visits" => "Wizyt",
"dstat_Extension" => "Rozszerzenie",
"dstat_DNS" => "Hostname",
"dstat_From" => "OD",
"dstat_OS" => "System",
"dstat_Browser" => "Przegl�darka",
"dstat_New_access" => "Nowe",
"dstat_Elapsed_time" => "Wykorzystany czas",
"dstat_No_new_access" => "Brak nowych odwiedzin",
"dstat_Visible_accesses" => "Widoczne odwiedziny",
"dstat_green_rows" => "zielone rz�dy",
"dstat_blue_rows" => "niebieskie rz�dy",
"dstat_red_rows" => "czerwone rz�dy",
"dstat_last_visit" => "ostatnie wizyty",
"dstat_robots" => "roboty",

// Global stats words

"gstat_Accesses" => "Dost�py",
"gstat_Total_visits" => "Wszytkie wizyty",
"gstat_Total_unique" => "Unikalne adresy",
"gstat_New_visits" => "Nowe wizyty",
"gstat_New_unique" => "Nowe unikalne adresy",
"gstat_Blacklisted" => "Z czarnej listy",
"gstat_Operating_systems" => "Systemy operacyjne",
"gstat_Browsers" => "Przegl�darki",
"gstat_n_first_extensions" => "%d pierwszych rozszerze�",
"gstat_Robots" => "Roboty",
"gstat_n_first_pages" => "%d pierwszych stron",
"gstat_n_first_origins" => "%d pierwszych �r�de�",
"gstat_Total" => "Razem",
"gstat_Not_specified" => "Nie wyszczeg�lnione",

// Time stats words
"tstat_Su" => "Nie",
"tstat_Mo" => "Pon",
"tstat_Tu" => "Wto",
"tstat_We" => "�ro",
"tstat_Th" => "Czw",
"tstat_Fr" => "Pi�",
"tstat_Sa" => "Sob",

"tstat_Jan" => "Sty",
"tstat_Feb" => "Lut",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Kwi",
"tstat_May" => "Maj",
"tstat_Jun" => "Cze",
"tstat_Jul" => "Lip",
"tstat_Aug" => "Sie",
"tstat_Sep" => "Wrz",
"tstat_Oct" => "Pa�",
"tstat_Nov" => "Lis",
"tstat_Dec" => "Gru",

"tstat_Last_day" => "Ostani dzie�",
"tstat_Last_week" => "Ostani tydzie�",
"tstat_Last_month" => "Ostatni miesi�c",
"tstat_Last_year" => "Ostatni rok",

// Configuration page words and sentences

"config_Variable_name" => "Nazwa zmiennej",
"config_Variable_value" => "Warto�� zmiennej",
"config_Explanations" => "Wyja�nienie",

"config_bbc_mainsite" =>
"AdresURL twojej strony.<br>
Je�li nic nie wpisa�e�, to zostanie on pomini�ty podczas wy�wietlania kolejnych stron statystyki.<br>
<br>
<i>Przyk�ad:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"Sprawd� w show_config.php czy chcesz udost�pnia� podgl�d konfiguracji przez WWW",

"config_bbc_titlebar" =>
"Ta linijka jest widoczna we wszystkich stonach statystyki.<br>
Znane makrodefinicje to:<br>
<ul>
<li>%SERVER: nazwa serwera,
<li>%DATE: obecja data.
</ul>
Tagi HTML s� dozwolone.",

"config_bbc_language" =>
"J�zyk kt�rego chcesz u�ywac - domy�lnie angielski.<br>
Reszta j�zyk�w widoczna w 
<a href=\"http://bbclone.de\">tej sekcji</a>",

"config_bbc_maxtime" =>
"Interwa� w sekundach pomi�dzy wizytami z tego samego IP.<br>
Standardowo jest to 1800 s",

"config_bbc_maxvisible" =>
"Ile wierszy ma si� wy�wietla� w statystyce szczeg�owej?<br>
Standardowo 100 - prosz� unika� ustawiana warto�ci wi�kszych ni� 500",

"config_bbc_maxos" =>
"How many OS do you want to see specified in the global stats?",

"config_bbc_maxbrowser" =>
"How many browsers do you want to see specified in the global stats?",

"config_bbc_maxextension" =>
"Ile rozszerze� chcemy rejestrowa� w statystyce g��wnej?",

"config_bbc_maxrobot" =>
"How many robots do you want to see specified in the global stats?",

"config_bbc_maxpage" =>
"Ile stron chcemy widzie� w satystyce g��wnej?",

"config_bbc_maxorigin" =>
"Ile �r�de� mamy rejestrowa� w statystyce g��wnej?",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_ignoreip" =>
"Kt�re adresy IP (lub podsieci) chcez ignorowa�?<br>
<i>Format:</i> &lt;adres IP lub podsieci&gt;, &lt;kolejny adres IP;lub podsieci&gt;<br>
Wstaw przecinek \",\" pomi�dzy ka�dym IP. Standardowo jest to \"lokalny\" IP.",

"config_bbc_detailed_stat_fields" =>
"Zmienna \$BBC_DETAILED_STAT_FIELDS determinuje jakie pola maj� zosta� wy�wietlone w statystyce szczeg�owej 
show_detailed.php.<br>
Mo�liwe pola to:<br>
\"id\", \"time\", \"visits\", \"dns\" (nazwa hosta), \"referer\", \"os\", \"browser\", \"ext\" (rozszerzenie)<br>
Wa�ny jest porz�dek nazw p�l.<br>
Pomini�te pola nie b�d� widoczne w statystyce.<br>
<br>
<i>Przyk�ady:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Mo�esz r�wnie� wyr�wna� ca�� statystyk� na stronie do kraw�dzi lub j� wycentrowa�. 
Mo�liwe ustawienia to \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"Odst�p pomi�dzy nag�wkami na stronie 0 (najmniejszy) i 6 (najwi�kszy)",

"config_bbc_subtitle_size" =>
"Odst�p pomi�dzy nag��wkami 2 stopnia na stronie 0 (najmniejszy) i 6 (najwi�kszy)",

"config_bbc_text_size" =>
"Rozmiar tekstu na stronie 0 (najmniejszy) i 6 (najwi�kszy)"

);
?>