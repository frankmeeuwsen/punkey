<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/zh-cn.php,v 1.13 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: zh-cn.php
* Summary: contains a Chinese Simplified Language translation table
* Description: Translated by Hu ChangSong <gugong_2002@yahoo.com.cn>
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "gb2312",

// Date format (used with date() )
"global_date_format" => "公元20y年m月d日",

// Global translation
"global_bbclone_copyright" => "The BBClone 团队 -- 许可遵从:",
"global_yes" => "是",
"global_no" => "否",

// The error messages
"error_cannot_see_config" =>
"这里不允许查看本服务器的 BBClone 配置。",

// Address Extensions (see lib/extension.php)
"ext_other" => "Other", "ext_com" => "Commercial",
"ext_net" => "Networks", "ext_edu" => "Educational",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japan", "ext_us" => "United States",
"ext_uk" => "United Kingdom", "ext_de" => "Germany",
"ext_mil" => "US Military", "ext_ca" => "Canada",
"ext_it" => "Italy", "ext_au" => "Australia",
"ext_org" => "Organizations", "ext_nl" => "Netherlands",
"ext_fr" => "France", "ext_tw" => "Taiwan",
"ext_gov" => "Government", "ext_fi" => "Finland",
"ext_br" => "Brazil", "ext_se" => "Sweden",
"ext_es" => "Spain", "ext_no" => "Norway",
"ext_mx" => "Mexico", "ext_kr" => "Korea",
"ext_ch" => "Switzerland", "ext_dk" => "Denmark",
"ext_be" => "Belgium", "ext_at" => "Austria",
"ext_nz" => "New Zealand", "ext_ru" => "Russian Federation",
"ext_pl" => "Poland", "ext_za" => "South Africa",
"ext_unknown" => "Unknown", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapore",
"ext_arpa" => "Mistakes", "ext_cz" => "Czech Republic",
"ext_hu" => "Hungary", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turkey",
"ext_gr" => "Greece", "ext_cn" => "China",
"ext_ie" => "Ireland", "ext_my" => "Malaysia",
"ext_th" => "Thailand", "ext_cl" => "Chile",
"ext_co" => "Colombia", "ext_is" => "Iceland",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ukraine",
"ext_sk" => "Slovakia", "ext_ro" => "Romania",
"ext_ae" => "United Arab Emirates", "ext_id" => "Indonesia",
"ext_su" => "Soviet Union", "ext_si" => "Slovenia",
"ext_hr" => "Croatia", "ext_ph" => "Philippines",
"ext_lv" => "Latvia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgaria", "ext_lt" => "Lithuania",
"ext_yu" => "Yugoslavia", "ext_lu" => "Luxembourg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "International Organizations",
"ext_do" => "Dominican Republic", "ext_cy" => "Cyprus",
"ext_pk" => "Pakistan", "ext_cc" => "Cocos (Keeling) Islands",
"ext_tt" => "Trinidad and Tobago", "ext_eg" => "Egypt",
"ext_lb" => "Lebanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakhstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudi Arabia",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Christmas Island", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Greenland",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Faroe Islands",
"ext_ac" => "Ascension Island", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenia", "ext_pf" => "French Polynesia",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Morocco",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnia and Herzegowina", "ext_om" => "Oman",
"ext_jo" => "Jordan", "ext_ir" => "Iran",
"ext_st" => "Sao Tome and Principe", "ext_vi" => "Virgin Islands (U.S.)",
"ext_ci" => "Ivory Coast", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Cayman Islands",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "American Samoa", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Heard and Mc Donald Islands", "ext_fm" => "Micronesia",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua New Guinea",
"ext_bz" => "Belize", "ext_sh" => "St. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldives",
"ext_nc" => "New Caledonia", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Solomon Islands", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodia", "ext_tc" => "Turks and Caicos Islands",
"ext_tf" => "French Southern Territories", "ext_az" => "Azerbaijan",
"ext_dm" => "Dominica", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "French Guiana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk Island", "ext_io" => "British Indian Ocean Territory",
"ext_gs" => "South Georgia and the South Sandwich Islands", "ext_ye" => "Yemen",
"ext_an" => "Netherlands Antilles", "ext_aq" => "Antarctica",
"ext_tn" => "Tunisia", "ext_ck" => "Cook Islands",
"ext_ls" => "Lesotho", "ext_et" => "Ethiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Virgin Islands (British)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Isle of Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "United Kingdom", "ext_bs" => "Bahamas",
"ext_va" => "Vatican City State (Holy See)", "ext_lc" => "Saint Lucia",
"ext_cd" => "Congo", "ext_gm" => "Gambia",
"ext_mp" => "Northern Mariana Islands", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Cameroon", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libya",
"ext_cf" => "Central African Republic", "ext_mm" => "Myanmar",
"ext_td" => "Chad", "ext_iq" => "Iraq",
"ext_kn" => "Saint Kitts and Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Cape Verde", "ext_mh" => "Marshall Islands",
"ext_pm" => "St. Pierre and Miquelon", "ext_so" => "Somalia",
"ext_vc" => "St. Vincent and the Grenadines", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Island",
"ext_fk" => "Falkland Islands (Malvinas)", "ext_gq" => "Equatorial Guinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard and Jan Mayen Islands",
"ext_sr" => "Suriname", "ext_sy" => "Syria",
"ext_tp" => "East Timor", "ext_um" => "United States Minor Outlying Islands",
"ext_wf" => "Wallis and Futuna Islands", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numeric",

// Miscellaneoux translations
"misc_other" => "其 他",
"misc_unknown" => "未 知",
"misc_second_unit" => "秒",

// The Navigation Bar
"navbar_Main_Site" => "我的站点",
"navbar_Configuration" => "配置文件",
"navbar_Global_Stats" => "全 面",
"navbar_Detailed_Stats" => "细 节",
"navbar_Time_Stats" => "时 间",
"navbar_Link_Stats" => "连 接",

// Detailed stats words
"dstat_ID" => "号码",
"dstat_Time" => "时 间",
"dstat_Visits" => "浏览",
"dstat_Extension" => "扩 展",
"dstat_DNS" => "IP 地 址",
"dstat_From" => "转 自（域 名）",
"dstat_OS" => "操 作 系 统",
"dstat_Browser" => "浏 览 器",
"dstat_New_access" => "新的访问",
"dstat_Elapsed_time" => "共耗时",
"dstat_No_new_access" => "没有新的访问",
"dstat_Visible_accesses" => "当前可以查看的访问数",
"dstat_green_rows" => "绿色行",
"dstat_blue_rows" => "蓝色行",
"dstat_red_rows" => "红色行",
"dstat_last_visit" => "最后的访问时间",
"dstat_robots" => "robots",

// Global stats words

"gstat_Accesses" => "存 取 记 录",
"gstat_Total_visits" => "访问总数",
"gstat_Total_unique" => "总的唯一的",
"gstat_New_visits" => "新的访问",
"gstat_New_unique" => "新的唯一的",
"gstat_Blacklisted" => "黑 名 单",
"gstat_Operating_systems" => "操 作 系 统",
"gstat_Browsers" => "浏 览 器",
"gstat_n_first_extensions" => "首 %d 范围排列",
"gstat_Robots" => "Robots",
"gstat_n_first_pages" => "首 %d 页面排列",
"gstat_n_first_origins" => "首 %d 来源排列",
"gstat_Total" => "合 计",
"gstat_Not_specified" => "没有详细说明",

// Time stats words
"tstat_Su" => "星期天",
"tstat_Mo" => "星期一",
"tstat_Tu" => "星期二",
"tstat_We" => "星期三",
"tstat_Th" => "星期四",
"tstat_Fr" => "星期五",
"tstat_Sa" => "星期六",

"tstat_Jan" => "一月",
"tstat_Feb" => "二月",
"tstat_Mar" => "三月",
"tstat_Apr" => "四月",
"tstat_May" => "五月",
"tstat_Jun" => "六月",
"tstat_Jul" => "七月",
"tstat_Aug" => "八月",
"tstat_Sep" => "九月",
"tstat_Oct" => "十月",
"tstat_Nov" => "十一",
"tstat_Dec" => "十二",

"tstat_Last_day" => "这一天",
"tstat_Last_week" => "这一周",
"tstat_Last_month" => "这一个月",
"tstat_Last_year" => "这一年",

// Configuration page words and sentences

"config_Variable_name" => "变 量 名 称",
"config_Variable_value" => "变 量 值",
"config_Explanations" => "详 细 解 释",

"config_bbc_mainsite" =>
"您的站点的 URL 。<br>
如果为空，“主站点”（navbar_Main_Site 的内容） 不会在所有的 BBClone 页面的导航栏上显示。<br>
<br>
<i>例子：</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"决定 BBClone 的配置文件是否可以通过show_config.php这个页面，而被显示出来，",

"config_bbc_titlebar" =>
"这个标题的内容会在所有 BBClone 页面的标题栏显示出来。<br>
可以理解的宏有：<br>
<ul>
<li>%SERVER: 服务器名字；
<li>%DATE: 当前时间。
</ul>
允许 HTML 标签。",

"config_bbc_language" =>
"您想使用的语言。默认的是英文。<br>
想知道有哪些可用的语言，可以浏览： 
BBClone 站点的 <a href=\"http://bbclone.de\">Download</a> 段。",

"config_bbc_maxtime" =>
"在两个来自同一对 IP地址/代理 的两个不同的访问之间的时间间隔(以秒为单位)。<br>
我们缺省的是标准的: 1800 秒。",

"config_bbc_maxvisible" =>
"您想在细节（detailed stats）栏看到多少个行 ？<br>
默认是 100 行；您的设置不得多于 500 （多了是没有用的，总之）。",

"config_bbc_maxos" =>
"How many OS do you want to see specified in the global stats?",

"config_bbc_maxbrowser" =>
"How many browsers do you want to see specified in the global stats?",

"config_bbc_maxextension" =>
"在全面（global stats）栏最多可以看到多少范围（extension）分类个数？",

"config_bbc_maxrobot" =>
"How many robots do you want to see specified in the global stats?",

"config_bbc_maxpage" =>
"在全面（global stats）栏最多可以看到多少页面（page）分类个数？",

"config_bbc_maxorigin" =>
"在全面（global stats）栏最多可以看到多少来源（origins）分类个数？",

"config_bbc_ignoreip" =>
"您想忽略（不作纪录）来自哪个 IP 地址（或者子网）的访问？<br>
<i>格式：</i> &lt;IP adress or subnet&gt;, &lt;another IP adress or subnet&gt;<br>
在每个 IP 之间插入一个逗号“,”。</br>默认的是大多数情况下的本地的 IP 地址。 ",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"变量 \$BBC_DETAILED_STAT_FIELDS 决定在这个页面（show_detailed.php）显示多少列。<br>
可能的列的名字有：<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extension)<br>
列的名字的顺序是重要的。<br>
不存在的列的名字将不会在 统计（statistics）页面显示。<br>
<br>
<i>例子：</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"这里，您可以设置主要的 统计（statistics）页面的排列风格。 
Possible values are \"left（居左）\", \"right（居右）\", \"center（居中）\"",

"config_bbc_title_size" =>
"标题（title）的大小级别，在0（最小）和6（最大）之间。",

"config_bbc_subtitle_size" =>
"副标题（subtitle）的大小级别，在0（最小）和6（最大）之间。",

"config_bbc_text_size" =>
"简单文本（simple text）的大小级别，在0（最小）和6（最大）之间。"

);
?>