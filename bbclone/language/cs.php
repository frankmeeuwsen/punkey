<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/cs.php,v 1.9 2004/02/15 19:39:12 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: cs.php
* Summary: contains a czech translation table
* Translated by: Josef Pinc <josefpinc@atlas.cz>
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-2",

// Date format (used with date() )
"global_date_format" => "d.m.Y",

// Global translation
"global_bbclone_copyright" => "BBClone t�m - ���eno pod licenc�",
"global_yes" => "ano",
"global_no" => "ne",

// The error messages
"error_cannot_see_config" =>
"Nem�te opr�vn�n� k prohl��en� konfigura�n�ho souboru BBClone.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Jin�", "ext_com" => "Komer�n� servery",
"ext_net" => "S��ov� infrastruktura", "ext_edu" => "�kolstv�",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japonsko", "ext_us" => "USA",
"ext_uk" => "Spojen� kr�lovstv�", "ext_de" => "N�mecko",
"ext_mil" => "Vojensk� servery USA", "ext_ca" => "Kanada",
"ext_it" => "It�lie", "ext_au" => "Austr�lie",
"ext_org" => "Nevl�dn� organizace", "ext_nl" => "Holandsko",
"ext_fr" => "Francie", "ext_tw" => "Tchajwan",
"ext_gov" => "Vl�dn� servery USA", "ext_fi" => "Finsko",
"ext_br" => "Braz�lie", "ext_se" => "�v�dsko",
"ext_es" => "�pan�lsko", "ext_no" => "Norsko",
"ext_mx" => "Mexiko", "ext_kr" => "Korea",
"ext_ch" => "�v�carsko", "ext_dk" => "D�nsko",
"ext_be" => "Belgie", "ext_at" => "Rakousko",
"ext_nz" => "Nov� Z�land", "ext_ru" => "Rusko",
"ext_pl" => "Polsko", "ext_za" => "Ji�n� Afrika",
"ext_unknown" => "Nezn�m�", "ext_ar" => "Argentina",
"ext_il" => "Izrael", "ext_sg" => "Singapur",
"ext_arpa" => "S��ov� infrastruktura", "ext_cz" => "�esk� republika",
"ext_hu" => "Ma�arsko", "ext_hk" => "Hongkong",
"ext_pt" => "Portugalsko", "ext_tr" => "Turecko",
"ext_gr" => "�ecko", "ext_cn" => "��na",
"ext_ie" => "Irsko", "ext_my" => "Malajsie",
"ext_th" => "Thajsko", "ext_cl" => "Chile",
"ext_co" => "Kolumbie", "ext_is" => "Island",
"ext_uy" => "Uruguay", "ext_ee" => "Estonsko",
"ext_in" => "Indie", "ext_ua" => "Ukrajina",
"ext_sk" => "Slovensko", "ext_ro" => "Rumunsko",
"ext_ae" => "Spojen� arabsk� emir�ty", "ext_id" => "Indon�zie",
"ext_su" => "Sov�tsk� svaz", "ext_si" => "Slovinsko",
"ext_hr" => "Chorvatsko", "ext_ph" => "Filip�ny",
"ext_lv" => "Litva", "ext_ve" => "Venezuela",
"ext_bg" => "Bulharsko", "ext_lt" => "Loty�sko",
"ext_yu" => "Jugosl�vie", "ext_lu" => "Lucembursko",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Kostarika", "ext_int" => "Mezin�rodn� organizace",
"ext_do" => "Dominik�nsk� republika", "ext_cy" => "Kypr",
"ext_pk" => "P�kist�n", "ext_cc" => "Kokosov� ostrovy",
"ext_tt" => "Trinidad a Tobago", "ext_eg" => "Egypt",
"ext_lb" => "Libanon", "ext_kw" => "Kuvajt",
"ext_to" => "Tonga", "ext_kz" => "Kazachst�n",
"ext_na" => "Namibie", "ext_mu" => "Mauritius",
"ext_bm" => "Bermudy", "ext_sa" => "Saudsk� Ar�bie",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzsko",
"ext_cx" => "V�no�n� ostrov", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Makedonie", "ext_gl" => "Gr�nsko",
"ext_ec" => "Ekv�dor", "ext_lk" => "Sr� Lanka",
"ext_md" => "Mold�vie", "ext_py" => "Paraguay",
"ext_bo" => "Bol�vie", "ext_bn" => "Brunej",
"ext_mt" => "Malta", "ext_fo" => "Faersk� ostrovy",
"ext_ac" => "Ascension", "ext_pr" => "Portoriko",
"ext_am" => "Arm�nie", "ext_pf" => "Francouzsk� Polyn�sie",
"ext_ge" => "Georgie", "ext_bh" => "Bahrajn",
"ext_ni" => "Nikaragua", "ext_by" => "B�lorusko",
"ext_sv" => "Salvador", "ext_ma" => "Maroko",
"ext_ke" => "Ke�a", "ext_ad" => "Andorra",
"ext_zm" => "Zambie", "ext_np" => "Nep�l",
"ext_bt" => "Bh�t�n", "ext_sz" => "Svazijsko",
"ext_ba" => "Bosna a Hercegovina", "ext_om" => "Om�n",
"ext_jo" => "Jord�nsko", "ext_ir" => "Ir�n",
"ext_st" => "Svat� Tom� a Princ�v ostrov", "ext_vi" => "Americk� Panensk� ostrovy",
"ext_ci" => "Pob�e�� slonoviny", "ext_jm" => "Jamajka",
"ext_li" => "Lichten�tejnsko", "ext_ky" => "Kajmansk� ostrovy",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagaskar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Americk� Samoa", "ext_tz" => "Tanz�nie",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenist�n",
"ext_mc" => "Monako", "ext_sn" => "Senegal",
"ext_hm" => "Ostrovy Heard a McDonald", "ext_fm" => "Mikron�sie",
"ext_fj" => "Fid�i", "ext_cu" => "Kuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinik",
"ext_ai" => "Anguilla", "ext_pg" => "Papua - Nov� Guinea",
"ext_bz" => "Belize", "ext_sh" => "Svat� Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maledivy",
"ext_nc" => "Nov� Kaledonie", "ext_ag" => "Antigua a Barbuda",
"ext_uz" => "Uzbekist�n", "ext_tj" => "T�d�ikist�n",
"ext_sb" => "�alamounovy ostrovy", "ext_bf" => "Burkina Faso",
"ext_kh" => "Kambod�a", "ext_tc" => "Ostrovy Turks a Caicos",
"ext_tf" => "Francouzsk� ji�n� teritoria", "ext_az" => "Azerbajd��n",
"ext_dm" => "Dominika", "ext_mz" => "Mozambik",
"ext_mo" => "Macao", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolsko", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Francouzsk� Guyana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Alb�nie", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk", "ext_io" => "Britsk� indickooce�nsk� teritorium",
"ext_gs" => "Ji�n� Georgie a Ji�n� Sandwichovy ostrovy", "ext_ye" => "Jemen",
"ext_an" => "Nizozemsk� Antily", "ext_aq" => "Antarktida",
"ext_tn" => "Tunisko", "ext_ck" => "Cookovy ostrovy",
"ext_ls" => "Lesotho", "ext_et" => "Etiopie",
"ext_ng" => "Nig�rie", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Britsk� Panensk� ostrovy", "ext_vn" => "Vietnam",
"ext_mr" => "Mauret�nie", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "D�ibuti",
"ext_km" => "Komory", "ext_dz" => "Al��rsko",
"ext_im" => "Isle of Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Katar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabun",
"ext_gb" => "Velk� Brit�nie (Spojen� kr�lovstv�)", "ext_bs" => "Bahamy",
"ext_va" => "Vatik�n", "ext_lc" => "Svat� Lucie",
"ext_cd" => "Kongo, Demokratick� republika", "ext_gm" => "Gambie",
"ext_mp" => "Severn� Mariany", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Kamerun", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libye",
"ext_cf" => "St�edoafrick� republika", "ext_mm" => "Barma (Myanmar)",
"ext_td" => "�ad", "ext_iq" => "Ir�k",
"ext_kn" => "Svat� Kitts a Nevis", "ext_sc" => "Seychely",
"ext_cg" => "Kongo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanist�n",
"ext_cv" => "Kapverdy", "ext_mh" => "Marshallovy ostrovy",
"ext_pm" => "Svat� Pierre a Miquelon", "ext_so" => "Som�lsko",
"ext_vc" => "Svat� Vincenc a Grenadiny", "ext_bd" => "Banglad�",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Lib�rie",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "R�union", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet",
"ext_fk" => "Falklandy", "ext_gq" => "Rovn�kov� Guinea",
"ext_sd" => "S�d�n", "ext_sj" => "Ostrovy Svalbard a Jan Mayen",
"ext_sr" => "Surinam", "ext_sy" => "S�rie",
"ext_tp" => "V�chodn� Timor", "ext_um" => "Mal� odlehl� ostrovy pat��c� USA",
"ext_wf" => "Ostrovy Wallis a Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Zair", "ext_IP" => "��seln�",

// Miscellaneoux translations
"misc_other" => "Jin�",
"misc_unknown" => "Nezn�m�",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Hlavn� strana",
"navbar_Configuration" => "Konfigurace",
"navbar_Global_Stats" => "Souhrnn� statistika",
"navbar_Detailed_Stats" => "Podrobn� statistika",
"navbar_Time_Stats" => "�asov� statistika",
"navbar_Link_Stats" => "Statistika odkaz�",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "�as",
"dstat_Visits" => "Nav�t�veno",
"dstat_Extension" => "Dom�na",
"dstat_DNS" => "Jm�no stroje",
"dstat_From" => "Odkud",
"dstat_OS" => "OS",
"dstat_Browser" => "Prohl��e�",
"dstat_New_access" => "Nov�ch p��stup�",
"dstat_Elapsed_time" => "Uplynul� �as",
"dstat_No_new_access" => "��dn� nov� p��stup",
"dstat_Visible_accesses" => "Zobrazovan�ch p��stup�",
"dstat_green_rows" => "zelen� ��dek",
"dstat_blue_rows" => "modr� ��dek",
"dstat_red_rows" => "�erven� ��dek",
"dstat_last_visit" => "posledn� n�v�t�va",
"dstat_robots" => "roboti",

// Global stats words

"gstat_Accesses" => "P��stupy",
"gstat_Total_visits" => "Celkem nav�t�veno",
"gstat_Total_unique" => "Celkem jedine�n� adresy",
"gstat_New_visits" => "Nov� nav�t�veno",
"gstat_New_unique" => "Nov� jedine�n� adresy",
"gstat_Blacklisted" => "Z �ern� listiny",
"gstat_Operating_systems" => "Top %d opera�n�ch syst�m�",
"gstat_Browsers" => "Top %d prohl��e��",
"gstat_n_first_extensions" => "Top %d dom�n",
"gstat_Robots" => "Top %d robot�",
"gstat_n_first_pages" => "Top %d nav�t�ven�ch str�nek",
"gstat_n_first_origins" => "Top %d zdroj�",
"gstat_Total" => "Celkem",
"gstat_Not_specified" => "Neur�eno",

// Time stats words
"tstat_Su" => "Ne",
"tstat_Mo" => "Po",
"tstat_Tu" => "�t",
"tstat_We" => "St",
"tstat_Th" => "�t",
"tstat_Fr" => "P�",
"tstat_Sa" => "So",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Apr",
"tstat_May" => "May",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Oct",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dec",

"tstat_Last_day" => "Posledn� den",
"tstat_Last_week" => "Posledn� t�den",
"tstat_Last_month" => "Posledn� m�s�c",
"tstat_Last_year" => "Posledn� rok",

// Configuration page words and sentences

"config_Variable_name" => "N�zev prom�nn�",
"config_Variable_value" => "Hodnota prom�nn�",
"config_Explanations" => "Vysv�tlivky",

"config_bbc_mainsite" =>
"URL va�� webov� str�nky.<br>
Nech�te-li pr�zdn�, nebude se v naviga�n�m panelu na str�nk�ch BBClone zobrazovat odkaz na domovskou str�nku.<br>
<br>
<i>P��klad:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"Ur�uje, zda bude mo�n� zobrazit konfigura�n� data.",

"config_bbc_titlebar" =>
"Titulek bude zobrazen v naviga�n�m panelu na v�ech BBClone str�nk�ch.<br>
K dispozici jsou n�sleduj�c� prom�nn�:<br>
<ul>
<li>%SERVER: jm�no serveru,
<li>%DATE: aktu�ln� datum.
</ul>
HTML tagy jsou povoleny.",

"config_bbc_language" =>
"Jazyk, kter� chcete pou��vat. Standardn� je nastavena angli�tina.<br>
Podporovan� jazyky jsou k&nbsp;vid�n� v <a href=\"http://bbclone.de\">download sekci</a> na BBClone str�nce.",

"config_bbc_maxtime" =>
"�as (ve vte�in�ch) mezi dv�mi rozd�ln�mi n�v�t�vami se stejnou IP/AGENT kombinac�.<br>
Standardn� je nastaveno: 1800&nbsp;s",

"config_bbc_maxvisible" =>
"Kolik chcete m�t zobrazovan�ch p��stup� v&nbsp;podrobn� statistice?<br>
Standardn� 100 - nenastavujte v�ce ne� 500 (zpomaluje)",

"config_bbc_maxos" =>
"Kolik TOP opera�n�ch syst�m� v&nbsp;souhrnn� statistice?",

"config_bbc_maxbrowser" =>
"Kolik TOP prohl��e�� v&nbsp;souhrnn� statistice?",

"config_bbc_maxextension" =>
"Kolik TOP dom�n (.cz, .com, atd.) v&nbsp;souhrnn� statistice?",

"config_bbc_maxrobot" =>
"Kolik TOP robot� v&nbsp;souhrnn� statistice?",

"config_bbc_maxpage" =>
"Kolik TOP str�nek v&nbsp;souhrnn� statistice?",

"config_bbc_maxorigin" =>
"Kolik TOP zdroj� v souhrnn� statistice?",

"config_bbc_ignoreip" =>
"Kter� IP adresy chcete ignorovat?<br>
<i>Form�t:</i> &lt;IP adresa (nebo pods��)&gt;, &lt;jin� IP adresa&gt;<br>
Seznam IP adres odd�lte ��rkou \",\". Standardn� jsou ignorov�ny \"local\" IP adresy.",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"Prom�nn� \$BBC_DETAILED_STAT_FIELDS ur�uje, kter� sloupce se zobraz� v&nbsp;podrobn� statistice.<br>
Mo�n� sloupce jsou:<br>
\"id\", \"time\" (�as), \"visits\" (nav�t�veno), \"dns\" (jm�no stroje), \"referer\" (zdroj), 
\"os\", \"browser\" (prohl��e�), \"ext\" (dom�na)<br>
Je d�le�it� zachovat po�ad� n�zv� sloupc�.<br>
Neexistuj�c� n�zvy sloupc� nebudou zobrazeny.<br>
<br>
<i>P��klady:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, ext\"<br>",

"config_bbc_general_align_style" =>
"Zp�sob zarovn�n� str�nky se statistikou. 
Mo�n� hodnoty jsou \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"Velikost nadpis�: 0 (nejmen��) a 6 (nejv�t��)",

"config_bbc_subtitle_size" =>
"Velikost nadpis� druh� �rovn�: 0 (nejmen��) a 6 (nejv�t��)",

"config_bbc_text_size" =>
"Velikost textu: 0 (nejmen��) a 6 (nejv�t��)"

);
?>