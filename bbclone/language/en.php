<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/en.php,v 1.15 2004/02/15 19:39:12 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: en.php
* Summary: contains a english translation table
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "Y/m/d",

// Global translation
"global_bbclone_copyright" => "The BBClone team - Licensed under the",
"global_yes" => "yes",
"global_no" => "no",

// The error messages
"error_cannot_see_config" =>
"You are not allowed to see the BBClone configuration on this server.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Other", "ext_com" => "Commercial",
"ext_net" => "Networks", "ext_edu" => "Educational",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japan", "ext_us" => "United States",
"ext_uk" => "United Kingdom", "ext_de" => "Germany",
"ext_mil" => "US Military", "ext_ca" => "Canada",
"ext_it" => "Italy", "ext_au" => "Australia",
"ext_org" => "Organizations", "ext_nl" => "Netherlands",
"ext_fr" => "France", "ext_tw" => "Taiwan",
"ext_gov" => "Government", "ext_fi" => "Finland",
"ext_br" => "Brazil", "ext_se" => "Sweden",
"ext_es" => "Spain", "ext_no" => "Norway",
"ext_mx" => "Mexico", "ext_kr" => "Korea",
"ext_ch" => "Switzerland", "ext_dk" => "Denmark",
"ext_be" => "Belgium", "ext_at" => "Austria",
"ext_nz" => "New Zealand", "ext_ru" => "Russian Federation",
"ext_pl" => "Poland", "ext_za" => "South Africa",
"ext_unknown" => "Unknown", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapore",
"ext_arpa" => "Mistakes", "ext_cz" => "Czech Republic",
"ext_hu" => "Hungary", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turkey",
"ext_gr" => "Greece", "ext_cn" => "China",
"ext_ie" => "Ireland", "ext_my" => "Malaysia",
"ext_th" => "Thailand", "ext_cl" => "Chile",
"ext_co" => "Colombia", "ext_is" => "Iceland",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ukraine",
"ext_sk" => "Slovakia", "ext_ro" => "Romania",
"ext_ae" => "United Arab Emirates", "ext_id" => "Indonesia",
"ext_su" => "Soviet Union", "ext_si" => "Slovenia",
"ext_hr" => "Croatia", "ext_ph" => "Philippines",
"ext_lv" => "Latvia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgaria", "ext_lt" => "Lithuania",
"ext_yu" => "Yugoslavia", "ext_lu" => "Luxembourg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "International Organizations",
"ext_do" => "Dominican Republic", "ext_cy" => "Cyprus",
"ext_pk" => "Pakistan", "ext_cc" => "Cocos (Keeling) Islands",
"ext_tt" => "Trinidad and Tobago", "ext_eg" => "Egypt",
"ext_lb" => "Lebanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakhstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudi Arabia",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Christmas Island", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Greenland",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Faroe Islands",
"ext_ac" => "Ascension Island", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenia", "ext_pf" => "French Polynesia",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Morocco",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnia and Herzegovina", "ext_om" => "Oman",
"ext_jo" => "Jordan", "ext_ir" => "Iran",
"ext_st" => "Sao Tome and Principe", "ext_vi" => "Virgin Islands (U.S.)",
"ext_ci" => "Ivory Coast", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Cayman Islands",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "American Samoa", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Heard and Mc Donald Islands", "ext_fm" => "Micronesia",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua New Guinea",
"ext_bz" => "Belize", "ext_sh" => "St. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldives",
"ext_nc" => "New Caledonia", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Solomon Islands", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodia", "ext_tc" => "Turks and Caicos Islands",
"ext_tf" => "French Southern Territories", "ext_az" => "Azerbaijan",
"ext_dm" => "Dominica", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "French Guiana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk Island", "ext_io" => "British Indian Ocean Territory",
"ext_gs" => "South Georgia and the South Sandwich Islands", "ext_ye" => "Yemen",
"ext_an" => "Netherlands Antilles", "ext_aq" => "Antarctica",
"ext_tn" => "Tunisia", "ext_ck" => "Cook Islands",
"ext_ls" => "Lesotho", "ext_et" => "Ethiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Virgin Islands (British)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Isle of Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "United Kingdom", "ext_bs" => "Bahamas",
"ext_va" => "Vatican City State (Holy See)", "ext_lc" => "Saint Lucia",
"ext_cd" => "Congo", "ext_gm" => "Gambia",
"ext_mp" => "Northern Mariana Islands", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Cameroon", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libya",
"ext_cf" => "Central African Republic", "ext_mm" => "Myanmar",
"ext_td" => "Chad", "ext_iq" => "Iraq",
"ext_kn" => "Saint Kitts and Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Cape Verde", "ext_mh" => "Marshall Islands",
"ext_pm" => "St. Pierre and Miquelon", "ext_so" => "Somalia",
"ext_vc" => "St. Vincent and the Grenadines", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Island",
"ext_fk" => "Falkland Islands (Malvinas)", "ext_gq" => "Equatorial Guinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard and Jan Mayen Islands",
"ext_sr" => "Suriname", "ext_sy" => "Syria",
"ext_tp" => "East Timor", "ext_um" => "United States Minor Outlying Islands",
"ext_wf" => "Wallis and Futuna Islands", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numeric",

// Miscellaneous translations
"misc_other" => "Other",
"misc_unknown" => "Unknown",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Main Site",
"navbar_Configuration" => "Configuration",
"navbar_Global_Stats" => "Global Stats",
"navbar_Detailed_Stats" => "Detailed Stats",
"navbar_Time_Stats" => "Time Stats",
"navbar_Link_Stats" => "Link Stats",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Time",
"dstat_Visits" => "Visits",
"dstat_Extension" => "Extension",
"dstat_DNS" => "Hostname",
"dstat_From" => "From",
"dstat_OS" => "OS",
"dstat_Browser" => "Browser",
"dstat_New_access" => "New accesses",
"dstat_Elapsed_time" => "Elapsed time",
"dstat_No_new_access" => "No new accesses",
"dstat_Visible_accesses" => "Visible accesses",
"dstat_green_rows" => "green rows",
"dstat_blue_rows" => "blue rows",
"dstat_red_rows" => "red rows",
"dstat_last_visit" => "last visit",
"dstat_robots" => "robots",

// Global stats words

"gstat_Accesses" => "Accesses",
"gstat_Total_visits" => "Total Visits",
"gstat_Total_unique" => "Total Unique",
"gstat_New_visits" => "New Visits",
"gstat_New_unique" => "New Unique",
"gstat_Blacklisted" => "Blacklisted",
"gstat_Operating_systems" => "Top %d Operating Systems",
"gstat_Browsers" => "Top %d Browsers",
"gstat_n_first_extensions" => "Top %d Extensions",
"gstat_Robots" => "Top %d Robots",
"gstat_n_first_pages" => "Top %d Visited Pages",
"gstat_n_first_origins" => "Top %d Origins",
"gstat_Total" => "Total",
"gstat_Not_specified" => "Not specified",

// Time stats words
"tstat_Su" => "Sun",
"tstat_Mo" => "Mon",
"tstat_Tu" => "Tue",
"tstat_We" => "Wed",
"tstat_Th" => "Thu",
"tstat_Fr" => "Fri",
"tstat_Sa" => "Sat",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Apr",
"tstat_May" => "May",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Oct",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dec",

"tstat_Last_day" => "Present day",
"tstat_Last_week" => "Present week",
"tstat_Last_month" => "Present month",
"tstat_Last_year" => "Present year",

// Configuration page words and sentences

"config_Variable_name" => "Variable's name",
"config_Variable_value" => "Variable's value",
"config_Explanations" => "Explanation",

"config_bbc_mainsite" =>
"The URL of your website.<br>
If empty, this URL doesn't appear in the navigation bar of all BBClone pages.<br>
<br>
<i>Example:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"Determines whether the data of show_config.php should be displayed or not",

"config_bbc_titlebar" =>
"The title being displayed in the navigation bar of all BBClone pages.<br>
The following macros are recognized:<br>
<ul>
<li>%SERVER: server name,
<li>%DATE: the current date.
</ul>
HTML tags are allowed, too.",

"config_bbc_language" =>
"The language you want to use. The default is english.<br>
To learn about available languages, you may take a look at the 
<a href=\"http://bbclone.de\">download section</a> of the BBClone 
website.",

"config_bbc_maxtime" =>
"Period of time (in seconds) between 2 different visits from the same IP/AGENT combination.<br>
The default is the emerging standard: 1800 s",

"config_bbc_maxvisible" =>
"How many entries do you want to see in the detailed stats?<br>
Default is 100 do not set it higher than 500 (which is useless, anyway)",

"config_bbc_maxos" =>
"How many OS do you want to see specified in the global stats?",

"config_bbc_maxbrowser" =>
"How many browsers do you want to see specified in the global stats?",

"config_bbc_maxextension" =>
"How many extensions do you want to see specified in the global stats?",

"config_bbc_maxrobot" =>
"How many robots do you want to see specified in the global stats?",

"config_bbc_maxpage" =>
"How many pages do you want to see specified in the global stats?",

"config_bbc_maxorigin" =>
"How many origins do you want to see specified in the global stats?",

"config_bbc_ignoreip" =>
"Which IP adress (or subnet) do you want to ignore?<br>
<i>Format:</i> &lt;IP adress or subnet&gt;, &lt;another IP adress or subnet&gt;<br>
Insert a comma \",\" between each IP. Default is most \"local\" IP.",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"The variable \$BBC_DETAILED_STAT_FIELDS determines the columns to display in 
show_detailed.php.<br>
Possible column names are:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extension)<br>
The order of the column names is important.<br>
Non-existing columns will be missing on the stats page.<br>
<br>
<i>Examples:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Here you can set the main stats page's align style. 
Possible values are \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"The titles' size between 0 (smallest) and 6 (largest)",

"config_bbc_subtitle_size" =>
"The subtitles' size between 0 (smallest) and 6 (largest)",

"config_bbc_text_size" =>
"The simple text's size between 0 (smallest) and 6 (largest)"

);
?>