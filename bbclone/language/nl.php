<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvsroot/bbclone/bbclone-0.3x/language/nl.php,v 1.10 2003/10/18 22:23:59 joku Exp
$
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: nl.php
* Summary: contains a dutch translation table
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "Het BBClone team - licentie onder ",
"global_yes" => "ja",
"global_no" => "nee",

// The error messages
"error_cannot_see_config" => "Het is niet toegestaan om de BBClone configuratie op deze server te zien.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Overig", "ext_com" => "Commercieel",
"ext_net" => "Netwerken", "ext_edu" => "Educatie",
"ext_biz" => "Business", "ext_info" => "Informatie",
"ext_jp" => "Japan", "ext_us" => "Verenigde Staten",
"ext_uk" => "Groot Brittani�", "ext_de" => "Duitsland",
"ext_mil" => "VS Militair", "ext_ca" => "Canada",
"ext_it" => "Itali�", "ext_au" => "Australi�",
"ext_org" => "Organisaties", "ext_nl" => "Nederland",
"ext_fr" => "Frankrijk", "ext_tw" => "Taiwan",
"ext_gov" => "Regering", "ext_fi" => "Finland",
"ext_br" => "Brazili�", "ext_se" => "Zweden",
"ext_es" => "Spanje", "ext_no" => "Noorwegen",
"ext_mx" => "Mexico", "ext_kr" => "Korea",
"ext_ch" => "Zwitserland", "ext_dk" => "Denemarken",
"ext_be" => "Belgi�", "ext_at" => "Oostenrijk",
"ext_nz" => "Nieuw Zeeland", "ext_ru" => "Russische Federatie",
"ext_pl" => "Polen", "ext_za" => "Zuid Afrika",
"ext_unknown" => "Onbekend", "ext_ar" => "Argentini�",
"ext_il" => "Isra�l", "ext_sg" => "Singapore",
"ext_arpa" => "Fouten", "ext_cz" => "Tsjechische Republiek",
"ext_hu" => "Hongarije", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turkije",
"ext_gr" => "Griekenland", "ext_cn" => "China",
"ext_ie" => "Ierland", "ext_my" => "Maleisi�",
"ext_th" => "Thailand", "ext_cl" => "Chili",
"ext_co" => "Colombia", "ext_is" => "IJsland",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ukraine",
"ext_sk" => "Slowakije", "ext_ro" => "Roemeni�",
"ext_ae" => "Verenigde Arabische Emiraten", "ext_id" => "Indonesi�",
"ext_su" => "Sovjet Unie", "ext_si" => "Sloveni�",
"ext_hr" => "Kroati�", "ext_ph" => "Filippijnen",
"ext_lv" => "Letland", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgarije", "ext_lt" => "Litanie",
"ext_yu" => "Joegoslavi�", "ext_lu" => "Luxemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Internationale Organisatie",
"ext_do" => "Dominicaanse Republiek", "ext_cy" => "Cyprus",
"ext_pk" => "Pakistan", "ext_cc" => "Cocos (Keeling) Eilanden",
"ext_tt" => "Trinidad en Tobago", "ext_eg" => "Egypte",
"ext_lb" => "Libanon", "ext_kw" => "Koeweit",
"ext_to" => "Tonga", "ext_kz" => "Kazakstan",
"ext_na" => "Namibi�", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudi Arabia",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Christmas Eilanden", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedoni�", "ext_gl" => "Groenland",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Faroe Eilanden",
"ext_ac" => "Ascension Eilanden", "ext_pr" => "Puerto Rico",
"ext_am" => "Armeni�", "ext_pf" => "Frans Polynesia",
"ext_ge" => "Georgi�", "ext_bh" => "Bahrein",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Marokko",
"ext_ke" => "Kenia", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosni� en Herzegovina", "ext_om" => "Oman",
"ext_jo" => "Jordani�", "ext_ir" => "Iran",
"ext_st" => "Sao Tome en Principe", "ext_vi" => "Virgin Eilanden (V.S.)",
"ext_ci" => "Ivoor Kust", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Kaaiman Eilanden",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagaskar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Amerikaans Samoa", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Heard en Mc Donald Eilanden", "ext_fm" => "Micronesi�",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Nieuw Guinea",
"ext_bz" => "Belize", "ext_sh" => "St. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Malediven",
"ext_nc" => "Nieuw Caledonie", "ext_ag" => "Antigua en Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Solomon Eilanden", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodja", "ext_tc" => "Turks en Caicos Eilanden",
"ext_tf" => "Frans Zuidelijk Territorium", "ext_az" => "Azerbeidjaan",
"ext_dm" => "Dominica", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongoli�", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Frans Guiana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albani�", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk Eilanden", "ext_io" => "Brits India Oceaan Territorium",
"ext_gs" => "Zuid Georgie en de Zuid Sandwich Eilanden", "ext_ye" => "Jemen",
"ext_an" => "Nederlandse Antillen", "ext_aq" => "Antarctica",
"ext_tn" => "Tunesi�", "ext_ck" => "Cook Eilanden",
"ext_ls" => "Lesotho", "ext_et" => "Ethiopi�",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Virgin Eilanden (Brits)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritani�", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Isle of Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Engeland", "ext_bs" => "Bahamas",
"ext_va" => "Vaticaan Stad Staat (Heilige Zee)", "ext_lc" => "Saint Lucia",
"ext_cd" => "Kongo", "ext_gm" => "Gambia",
"ext_mp" => "Noordelijke Mariana Eilanden", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Kameroen", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libie",
"ext_cf" => "Centraal Afrikaanse Republiek", "ext_mm" => "Myanmar",
"ext_td" => "Chad", "ext_iq" => "Irak",
"ext_kn" => "Saint Kitts en Nevis", "ext_sc" => "Seychellen",
"ext_cg" => "Kongo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Cape Verde", "ext_mh" => "Marshall Eilanden",
"ext_pm" => "St. Pierre en Miquelon", "ext_so" => "Somali�",
"ext_vc" => "St. Vincent en de Grenadines", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Ha�ti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Eilanden",
"ext_fk" => "Falkland Eilanden (Malvinas)", "ext_gq" => "Equatorial Guinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard en Jan Mayen Eilanden",
"ext_sr" => "Suriname", "ext_sy" => "Syri�",
"ext_tp" => "Oost Timor", "ext_um" => "Verenigde Staten buitenste ondergeschikte Eilanden",
"ext_wf" => "Wallis en Futuna Eilanden", "ext_yt" => "Mayotte",
"ext_zr" => "Za�re", "ext_IP" => "Numeriek",

// Miscellaneoux translations
"misc_other" => "Anders",
"misc_unknown" => "Onbekende",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Hoofd Pagina",
"navbar_Configuration" => "Configuratie",
"navbar_Global_Stats" => "Globale Statistieken",
"navbar_Detailed_Stats" => "Gedetailleerde Statistieken",
"navbar_Time_Stats" => "Tijd Statistieken",
"navbar_Link_Stats" => "Link Statistieken",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Datum/tijd",
"dstat_Visits" => "Bezoek",
"dstat_Extension" => "Extensie",
"dstat_DNS" => "Hostname",
"dstat_From" => "Kwam van",
"dstat_OS" => "Besturingssystemen",
"dstat_Browser" => "Browser",
"dstat_New_access" => "Nieuwe bezoeker(s)",
"dstat_Elapsed_time" => "Verlopen tijd",
"dstat_No_new_access" => "Geen nieuwe bezoekers",
"dstat_Visible_accesses" => "Zichtbare bezoekers",
"dstat_green_rows" => "groen regels",
"dstat_blue_rows" => "blauwe regels",
"dstat_red_rows" => "rode regels",
"dstat_last_visit" => "laatste bezoekers",
"dstat_robots" => "robots",

// Global stats words

"gstat_Accesses" => "Bezoekers",
"gstat_Total_visits" => "Totaal bezoekers",
"gstat_Total_unique" => "Totaal unieke bezoekers",
"gstat_New_visits" => "Nieuwe bezoeker(s)",
"gstat_New_unique" => "Nieuwe unieke bezoeker(s)",
"gstat_Blacklisted" => "Zwarte lijst",
"gstat_Operating_systems" => "Besturingssystemen: Top %d",
"gstat_Browsers" => "Browsers: Top %d",
"gstat_n_first_extensions" => "Extensies: Top %d",
"gstat_Robots" => "Robots: Top %d",
"gstat_n_first_pages" => "Bezochte Pagina's: Top %d",
"gstat_n_first_origins" => "Oorsprong: Top %d",
"gstat_Total" => "Totaal",
"gstat_Not_specified" => "Niet gespecificeerd",

// Time stats words
"tstat_Su" => "Zon",
"tstat_Mo" => "Maan",
"tstat_Tu" => "Din",
"tstat_We" => "Woe",
"tstat_Th" => "Don",
"tstat_Fr" => "Vrij",
"tstat_Sa" => "Zat",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mrt",
"tstat_Apr" => "Apr",
"tstat_May" => "Mei",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Okt",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dec",

"tstat_Last_day" => "Dag",
"tstat_Last_week" => "Week",
"tstat_Last_month" => "Maand",
"tstat_Last_year" => "Jaar",

// Configuration page words and sentences

"config_Variable_name" => "Variabele naam",
"config_Variable_value" => "Variabele waarde",
"config_Explanations" => "Uitleg",

"config_bbc_mainsite" =>
"De URL van de website. Indien leeg, dan verschijnt
deze URL niet in de navigatie balk van alle BBClone pagina's.<br>
<br>
<i>Bijvoorbeeld:</i><br>
\$BBC_MAINSITE =
\"http://www.mijnserver.com/ergens/\".",

"config_bbc_show_config" => "Bepaald of de configuratie zichtbaar is of niet voor show_config.php",

"config_bbc_titlebar" =>
"De titel verschijnt in de titel balk van alle BBClone
pagina's. <br>
Mogelijke macro's zijn:<br>
<ul>
<li>%SERVER: server naam,
<li>%DATE: de huidig datum.
</ul>
HTML opmaak is toegestaan.",

"config_bbc_language" =>
"De taal die u wilt gebruiken. De standaardwaarde is
engels. Voor de mogelijk verkrijgbare talen, kijkt in de te
<a href=\"http://bbclone.de\">downloaden
onderdelen</a> van de BBClone website.",

"config_bbc_maxtime" =>
"Tijd interval (in secondes) tussen 2 verschillende
bezoekers van dezelfde IP/AGENT.<br>
De standaard waarde is: 1800 s",

"config_bbc_maxvisible" =>
"Hoeveel regels wilt u in de gedetailleerde statistieken
zien?<br>
Standaardwaarde is 100 en zet niet hogere dan 500 (wat
nutteloos is, in ieder geval).",

"config_bbc_maxos" =>
"Hoeveel besturingssystemen wilt u gespecificeerd zien in
de globale statistieken?",

"config_bbc_maxbrowser" =>
"Hoeveel browsers wilt u gespecificeerd zien in de
globale statistieken?",

"config_bbc_maxextension" =>
"Hoeveel extensies wilt u gespecificeerd zien in de
globale statistieken?",

"config_bbc_maxrobot" =>
"Hoeveel roboten wilt u gespecificeerd zien in de globale
statistieken?",

"config_bbc_maxpage" =>
"Hoeveel pagina's wilt u gespecificeerd zien in de
globale statistieken?",

"config_bbc_maxorigin" =>
"Hoeveel oorsprongen wilt u gespecificeerd zien in de
globale statistieken?",

"config_bbc_ignoreip" =>
" Welk IP adres (of subnet) wilt u negeren?<br>
<i>Formaat:</i> &lt;IP adres of subnet&gt;, &lt;ander IP
adres of subnet&gt;<br>
Voeg een komma \",\" toe tussen elk IP. Standaardwaarde
is de \"lokale\" IP.",

"config_bbc_ignore_refer" =>
"Als je meerdere web-sites hebt en je wil ze niet in de
oorsprong lijst,
voeg dan hier de host-namen toe. De verwijzigen zullen
dan gezien worden als
\"Niet gespecificeerd\" en de telling zal niet verloren
gaan. Gebruik het volgende
formaat:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org,
yetanother.host3.org\";<br />
en zo verder...",

"config_bbc_own_refer" =>
"Als deze optie gezet is, zal de verwijzing van de server
waar BBClone op draait
afgebeeld worden als http://www.mijnserver.com/
(vervangen door eigen
server naam) in de oorsprong lijst. Dit is handig als
BBClone niet administratieve
pagina's, beveiligde folders en andere zaken die niet
mogen worden vertonen of welke
je voor je zelf wilt houden.",

"config_bbc_no_string" =>
"BBClone schrijft commentaar in de HTML bron als indicatie
van de huidige status.
Deze uitvoering, hoewel praktisch, kan soms forums of andere
inhouds management
systemen be�nvloeden. Als je geconverteerd wordt met een blanke
pagina of een paar
\"header already sent by\" berichten, dien je deze optie te
activeren om het
script weer te laten draaien.",

"config_bbc_detailed_stat_fields" =>
"De variabele \$BBC_DETAILED_STAT_FIELDS bepaald welk
veld wordt vertoond in de show_detailed.php.<br>
Mogelijke veld namen zijn:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostnaam),
\"referer\", \"os\", \"browser\", \"ext\" (extensie)<br>
Het rangschikken van de velden is erg belangrijk.<br>
Niet aanwezige velden zullen op de statistieken pagina's
ontbreken.<br>
<br>
<i>Bijvoorbeeld:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os,
browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os,
dns\"<br>",

"config_bbc_general_align_style" =>
"Hier kunt U de opmaak stijl van de hoofd statistieken
bepalen.
Mogelijke waarde zijn \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"De grote van de titels tussen 0 (kleinste) en 6
(hoogste)",

"config_bbc_subtitle_size" =>
"De grote van de ondertitels tussen 0 (kleinste) en 6
(hoogste)",

"config_bbc_text_size" =>
"De grote van de eenvoudige tekst tussen 0 (kleinste) en
6 (hoogste)"

);
?>