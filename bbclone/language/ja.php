<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/ja.php,v 1.2 2004/02/08 12:34:51 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: ja.php
* Summary: contains a japanese translation table
* Modified by mugen 2004.02.03 (mugen@guitar.jp)
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "euc-jp",

// Date format (used with date() )
"global_date_format" => "Y/m/d",

// Global translation
"global_bbclone_copyright" => "The BBClone team - Licensed under the",
"global_yes" => "yes",
"global_no" => "no",

// The error messages
"error_cannot_see_config" =>
"あなたはこのサーバーの BBClone を見ることを許可されていません",

// Address Extensions (see lib/extension.php)
"ext_other" => "Other", "ext_com" => "Commercial",
"ext_net" => "Networks", "ext_edu" => "Educational",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Japan", "ext_us" => "United States",
"ext_uk" => "United Kingdom", "ext_de" => "Germany",
"ext_mil" => "US Military", "ext_ca" => "Canada",
"ext_it" => "Italy", "ext_au" => "Australia",
"ext_org" => "Organizations", "ext_nl" => "Netherlands",
"ext_fr" => "France", "ext_tw" => "Taiwan",
"ext_gov" => "Government", "ext_fi" => "Finland",
"ext_br" => "Brazil", "ext_se" => "Sweden",
"ext_es" => "Spain", "ext_no" => "Norway",
"ext_mx" => "Mexico", "ext_kr" => "Korea",
"ext_ch" => "Switzerland", "ext_dk" => "Denmark",
"ext_be" => "Belgium", "ext_at" => "Austria",
"ext_nz" => "New Zealand", "ext_ru" => "Russian Federation",
"ext_pl" => "Poland", "ext_za" => "South Africa",
"ext_unknown" => "Unknown", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapore",
"ext_arpa" => "Mistakes", "ext_cz" => "Czech Republic",
"ext_hu" => "Hungary", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turkey",
"ext_gr" => "Greece", "ext_cn" => "China",
"ext_ie" => "Ireland", "ext_my" => "Malaysia",
"ext_th" => "Thailand", "ext_cl" => "Chile",
"ext_co" => "Colombia", "ext_is" => "Iceland",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ukraine",
"ext_sk" => "Slovakia", "ext_ro" => "Romania",
"ext_ae" => "United Arab Emirates", "ext_id" => "Indonesia",
"ext_su" => "Soviet Union", "ext_si" => "Slovenia",
"ext_hr" => "Croatia", "ext_ph" => "Philippines",
"ext_lv" => "Latvia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgaria", "ext_lt" => "Lithuania",
"ext_yu" => "Yugoslavia", "ext_lu" => "Luxembourg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "International Organizations",
"ext_do" => "Dominican Republic", "ext_cy" => "Cyprus",
"ext_pk" => "Pakistan", "ext_cc" => "Cocos (Keeling) Islands",
"ext_tt" => "Trinidad and Tobago", "ext_eg" => "Egypt",
"ext_lb" => "Lebanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakhstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudi Arabia",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kyrgyzstan",
"ext_cx" => "Christmas Island", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Greenland",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldova", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Faroe Islands",
"ext_ac" => "Ascension Island", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenia", "ext_pf" => "French Polynesia",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Morocco",
"ext_ke" => "Kenya", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnia and Herzegovina", "ext_om" => "Oman",
"ext_jo" => "Jordan", "ext_ir" => "Iran",
"ext_st" => "Sao Tome and Principe", "ext_vi" => "Virgin Islands (U.S.)",
"ext_ci" => "Ivory Coast", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Cayman Islands",
"ext_gp" => "Guadeloupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "American Samoa", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Heard and Mc Donald Islands", "ext_fm" => "Micronesia",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Rwanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua New Guinea",
"ext_bz" => "Belize", "ext_sh" => "St. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldives",
"ext_nc" => "New Caledonia", "ext_ag" => "Antigua and Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tajikistan",
"ext_sb" => "Solomon Islands", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodia", "ext_tc" => "Turks and Caicos Islands",
"ext_tf" => "French Southern Territories", "ext_az" => "Azerbaijan",
"ext_dm" => "Dominica", "ext_mz" => "Mozambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "French Guiana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk Island", "ext_io" => "British Indian Ocean Territory",
"ext_gs" => "South Georgia and the South Sandwich Islands", "ext_ye" => "Yemen",
"ext_an" => "Netherlands Antilles", "ext_aq" => "Antarctica",
"ext_tn" => "Tunisia", "ext_ck" => "Cook Islands",
"ext_ls" => "Lesotho", "ext_et" => "Ethiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Virgin Islands (British)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algeria",
"ext_im" => "Isle of Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "United Kingdom", "ext_bs" => "Bahamas",
"ext_va" => "Vatican City State (Holy See)", "ext_lc" => "Saint Lucia",
"ext_cd" => "Congo", "ext_gm" => "Gambia",
"ext_mp" => "Northern Mariana Islands", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Cameroon", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libya",
"ext_cf" => "Central African Republic", "ext_mm" => "Myanmar",
"ext_td" => "Chad", "ext_iq" => "Iraq",
"ext_kn" => "Saint Kitts and Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Cape Verde", "ext_mh" => "Marshall Islands",
"ext_pm" => "St. Pierre and Miquelon", "ext_so" => "Somalia",
"ext_vc" => "St. Vincent and the Grenadines", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet Island",
"ext_fk" => "Falkland Islands (Malvinas)", "ext_gq" => "Equatorial Guinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard and Jan Mayen Islands",
"ext_sr" => "Suriname", "ext_sy" => "Syria",
"ext_tp" => "East Timor", "ext_um" => "United States Minor Outlying Islands",
"ext_wf" => "Wallis and Futuna Islands", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numeric",

// Miscellaneous translations
"misc_other" => "その他",
"misc_unknown" => "不明",
"misc_second_unit" => "秒",

// The Navigation Bar
"navbar_Main_Site" => "主サイト",
"navbar_Configuration" => "設定",
"navbar_Global_Stats" => "グローバル統計",
"navbar_Detailed_Stats" => "詳細統計",
"navbar_Time_Stats" => "時間別統計",
"navbar_Link_Stats" => "リンク統計",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "時間",
"dstat_Visits" => "訪問",
"dstat_Extension" => "分類",
"dstat_DNS" => "ホスト名",
"dstat_From" => "どこから来たか",
"dstat_OS" => "ＯＳ",
"dstat_Browser" => "ブラウザ",
"dstat_New_access" => "新規アクセス",
"dstat_Elapsed_time" => "経過時間",
"dstat_No_new_access" => "新しいアクセスはありません",
"dstat_Visible_accesses" => "アクセス数",
"dstat_green_rows" => "緑",
"dstat_blue_rows" => "青",
"dstat_red_rows" => "赤",
"dstat_last_visit" => "最終訪問",
"dstat_robots" => "検索ロボット",

// Global stats words

"gstat_Accesses" => "アクセス",
"gstat_Total_visits" => "合計",
"gstat_Total_unique" => "固有合計",
"gstat_New_visits" => "新規",
"gstat_New_unique" => "固有新規",
"gstat_Blacklisted" => "ブラックリスト",
"gstat_Operating_systems" => "OS別上位 %d",
"gstat_Browsers" => " ブラウザ別上位 %d",
"gstat_n_first_extensions" => "国コード別上位 %d",
"gstat_Robots" => "検索ロボット別上位 %d",
"gstat_n_first_pages" => "訪問済ページ上位 %d",
"gstat_n_first_origins" => "リファラ上位 %d",
"gstat_Total" => "合計",
"gstat_Not_specified" => "指定無し",

// Time stats words
"tstat_Su" => "日",
"tstat_Mo" => "月",
"tstat_Tu" => "火",
"tstat_We" => "水",
"tstat_Th" => "木",
"tstat_Fr" => "金",
"tstat_Sa" => "土",

"tstat_Jan" => "1月",
"tstat_Feb" => "2月",
"tstat_Mar" => "3月",
"tstat_Apr" => "4月",
"tstat_May" => "5月",
"tstat_Jun" => "6月",
"tstat_Jul" => "7月",
"tstat_Aug" => "8月",
"tstat_Sep" => "9月",
"tstat_Oct" => "10月",
"tstat_Nov" => "11月",
"tstat_Dec" => "12月",

"tstat_Last_day" => "一日",
"tstat_Last_week" => "一週間",
"tstat_Last_month" => "一月間",
"tstat_Last_year" => "一年間",

// Configuration page words and sentences

"config_Variable_name" => "変数名",
"config_Variable_value" => "変数の値",
"config_Explanations" => "説明",

"config_bbc_mainsite" =>
"あなたのサイトのURL<br>
もし空なら、URLはすべてのBBCloneページのナビゲーションバーには表示されません<br>
<br>
<i>例:</i><br>
\$BBC_MAINSITE = \"http://www.mywebhost.com/somewhere/\".",

"config_bbc_show_config" =>
"show_config.php のデータを表示するか",

"config_bbc_titlebar" =>
"すべての BBClone ページのナビゲーションバーに表示されているタイトル<br>
次のマクロが使えます:<br>
<ul>
<li>%SERVER: サーバー名
<li>%DATE: 日付
</ul>
HTMLタグも使えます",

"config_bbc_language" =>
"使いたい言語。デフォルトは英語です。<br>
利用可能な言語については、BBCloneウェブサイト
の<a href=\"http://bbclone.de\">ダウンロード・セクション</a>
を見てください。",

"config_bbc_maxtime" =>
"同一IP又はAgentの場合、再訪問としてカウントする為の時間（秒）間隔<br>
デフォルトは1800秒です。",

"config_bbc_maxvisible" =>
"詳細な統計で表示するエントリー数。デフォルトは100です。
500(役立たない)を越えた設定はしないで下さい。",

"config_bbc_maxos" =>
"グローバル統計中に何種類のOSを表示させるか。",

"config_bbc_maxbrowser" =>
"グローバル統計中に何種類のブラウザを表示させるか。",

"config_bbc_maxextension" =>
"グローバル統計中に何種類の国別コードを表示させるか。",

"config_bbc_maxrobot" =>
"グローバル統計中に何種類の検索ロボットを表示させるか。",

"config_bbc_maxpage" =>
"グローバル統計中に何ページを表示させるか。",

"config_bbc_maxorigin" =>
"グローバル統計中にいくつリファラを表示させるか。",

"config_bbc_ignoreip" =>
"無視するIPアドレス<br>
<i>フォーマット:</i> &lt;IPアドレス又はサブネット&gt;, &lt;他のIPアドレス又はサブネット&gt;<br>
IPの間にコンマ \",\" を挿入。デフォルトは \"local\" IP。",

"config_bbc_ignore_refer" =>
"あなたが複数のサイトを持っていて、それらをリファラの上位リストに加えたく
無いなら、ここにホスト名を追加する事が出来ます。
リファラは\"指定なし\"として扱われるでしょう。ヒット数は変わりません。
次のフォーマットが使えます。<br>
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
という風に。",

"config_bbc_own_refer" =>
"このフラグがセットされたら、bbcloneが稼働しているサーバー自身も http://www.myserver.com/ (あなたのサーバー名に置き換えます) リファラとして表示し、ランキングにカウントします。
これはbbcloneが管理用のページへのパスや保護されたディレクトリをリストする事を望まない場合に有効です。",

"config_bbc_no_string" =>
"BBCloneは現在の状態を示す為にHTMLソースにコメントを書きます。
この出力は便利ですが、いくつかのフォーラムあるいは組織運営の邪魔になるかもしれません。
もし白紙のページやいくつかの\"header already sent by\"というメッセージに直面した場合、このフラグをアンコメントし、スクリプトを再度動かす必要があるでしょう。",

"config_bbc_detailed_stat_fields" =>
"変数 \$BBC_DETAILED_STAT_FIELDS はshow_detailed.phpに表示するための
カラムを決定します。<br>
利用可能なカラム名:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extension)<br>
カラム名の順序は重要です。<br>
存在しないカラムは統計ページに見つける事が出来ないでしょう。<br>
<br>
<i>例:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"主統計ページの整列方法スタイルをセットすることができます。
利用可能なスタイルは \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"タイトルのサイズ 0 (最小) から 6 (最大)",

"config_bbc_subtitle_size" =>
"サブタイトルのサイズ 0 (最小) から 6 (最大)",

"config_bbc_text_size" =>
"本文のサイズ 0 (最小) から 6 (最大)"

);
?>