<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/it.php,v 1.14 2004/02/16 20:18:07 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: it.php
* Summary: contiene una tabella di traduzione in italiano
* Author: Andreas Liebschner, fizban(at)slack-tux.org, Daniele.Raffo(at)inria.fr
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "Il team di BBClone - Distribuito sotto licenza ",
"global_yes" => "si",
"global_no" => "no",

// The error messages
"error_cannot_see_config" =>
"Non sei autorizzato a vedere la configurazione di BBClone su questo server.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Altro", "ext_com" => "Commerciale",
"ext_net" => "Reti", "ext_edu" => "Istituzioni educative USA",
"ext_biz" => "Affari", "ext_info" => "Informativo",
"ext_jp" => "Giappone", "ext_us" => "Stati Uniti",
"ext_uk" => "Regno Unito", "ext_de" => "Germania",
"ext_mil" => "Esercito USA", "ext_ca" => "Canada",
"ext_it" => "Italia", "ext_au" => "Australia",
"ext_org" => "Organizzazioni", "ext_nl" => "Olanda",
"ext_fr" => "Francia", "ext_tw" => "Taiwan",
"ext_gov" => "Governo USA", "ext_fi" => "Finlandia",
"ext_br" => "Brasile", "ext_se" => "Svezia",
"ext_es" => "Spagna", "ext_no" => "Norvegia",
"ext_mx" => "Messico", "ext_kr" => "Corea",
"ext_ch" => "Svizzera", "ext_dk" => "Danimarca",
"ext_be" => "Belgio", "ext_at" => "Austria",
"ext_nz" => "Nuova Zelanda", "ext_ru" => "Federazione Russa",
"ext_pl" => "Polonia", "ext_za" => "Sud Africa",
"ext_unknown" => "Sconosciuto", "ext_ar" => "Argentina",
"ext_il" => "Israele", "ext_sg" => "Singapore",
"ext_arpa" => "Errori", "ext_cz" => "Repubblica Ceca",
"ext_hu" => "Ungheria", "ext_hk" => "Hong Kong",
"ext_pt" => "Portogallo", "ext_tr" => "Turchia",
"ext_gr" => "Grecia", "ext_cn" => "Cina",
"ext_ie" => "Irlanda", "ext_my" => "Malesia",
"ext_th" => "Thailandia", "ext_cl" => "Cile",
"ext_co" => "Colombia", "ext_is" => "Islanda",
"ext_uy" => "Uruguay", "ext_ee" => "Estonia",
"ext_in" => "India", "ext_ua" => "Ucraina",
"ext_sk" => "Slovacchia", "ext_ro" => "Romania",
"ext_ae" => "Emirati Arabi Uniti", "ext_id" => "Indonesia",
"ext_su" => "Unione Sovietica", "ext_si" => "Slovenia",
"ext_hr" => "Croazia", "ext_ph" => "Filippine",
"ext_lv" => "Lettonia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgaria", "ext_lt" => "Lituania",
"ext_yu" => "Yugoslavia", "ext_lu" => "Lussemburgo",
"ext_nu" => "Niue", "ext_pe" => "Per�",
"ext_cr" => "Costa Rica", "ext_int" => "Organizzazioni internazionali",
"ext_do" => "Repubblica Dominicana", "ext_cy" => "Cipro",
"ext_pk" => "Pakistan", "ext_cc" => "Isole Coco",
"ext_tt" => "Trinidad e Tobago", "ext_eg" => "Egitto",
"ext_lb" => "Libano", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kazakistan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Arabia Saudita",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kirghistan",
"ext_cx" => "Isola di Natale", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Macedonia", "ext_gl" => "Groenlandia",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldavia", "ext_py" => "Paraguay",
"ext_bo" => "Bolivia", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Isole Faroe",
"ext_ac" => "Ascension", "ext_pr" => "Porto Rico",
"ext_am" => "Armenia", "ext_pf" => "Polinesia Francese",
"ext_ge" => "Georgia", "ext_bh" => "Bahrein",
"ext_ni" => "Nicaragua", "ext_by" => "Bielorussia",
"ext_sv" => "El Salvador", "ext_ma" => "Marocco",
"ext_ke" => "Kenia", "ext_ad" => "Andorra",
"ext_zm" => "Zambia", "ext_np" => "Nepal",
"ext_bt" => "Butan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnia Erzegovina", "ext_om" => "Oman",
"ext_jo" => "Giordania", "ext_ir" => "Iran",
"ext_st" => "Sao Tom� e Principe", "ext_vi" => "Isole Vergini Americane",
"ext_ci" => "Costa d'Avorio", "ext_jm" => "Giamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Isole Cayman",
"ext_gp" => "Guadalupa", "ext_mg" => "Madagascar",
"ext_gi" => "Gibilterra", "ext_sm" => "San Marino",
"ext_as" => "Samoa Americana", "ext_tz" => "Tanzania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Isole Heard e Mc Donald", "ext_fm" => "Micronesia",
"ext_fj" => "Figi", "ext_cu" => "Cuba",
"ext_rw" => "Ruanda", "ext_mq" => "Martinica",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Nuova Guinea",
"ext_bz" => "Belize", "ext_sh" => "Sant'Elena",
"ext_aw" => "Aruba", "ext_mv" => "Maldive",
"ext_nc" => "Nuova Caledonia", "ext_ag" => "Antigua e Barbuda",
"ext_uz" => "Uzbekistan", "ext_tj" => "Tagikistan",
"ext_sb" => "Isole Salomone", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambogia", "ext_tc" => "Isole Turks e Caicos",
"ext_tf" => "Territori Francesi del Sud-Est", "ext_az" => "Azerbaigian",
"ext_dm" => "Dominica", "ext_mz" => "Mozambico",
"ext_mo" => "Macao", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Nigeria", "ext_gf" => "Guyana Francese",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albania", "ext_gh" => "Ghana",
"ext_nf" => "Isole Norfolk", "ext_io" => "Territori Britannici nell'Oceano Indiano",
"ext_gs" => "Isole Sandwich Meridionali e Georgia Meridionale", "ext_ye" => "Yemen",
"ext_an" => "Antille Olandesi", "ext_aq" => "Antartide",
"ext_tn" => "Tunisia", "ext_ck" => "Isole Cook",
"ext_ls" => "Lesotho", "ext_et" => "Etiopia",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Isole Vergini Britanniche", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritania", "ext_gy" => "Guyana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Gibuti",
"ext_km" => "Comore", "ext_dz" => "Algeria",
"ext_im" => "Isola di Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabon",
"ext_gb" => "Gran Bretagna", "ext_bs" => "Bahamas",
"ext_va" => "Citt� del Vaticano", "ext_lc" => "Santa Lucia",
"ext_cd" => "Congo", "ext_gm" => "Gambia",
"ext_mp" => "Isole Marianne Settentrionali", "ext_gw" => "Guinea Bissau",
"ext_cm" => "Camerun", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libia",
"ext_cf" => "Repubblica Centrafricana", "ext_mm" => "Myanmar",
"ext_td" => "Ciad", "ext_iq" => "Iraq",
"ext_kn" => "Saint Kitts e Nevis", "ext_sc" => "Seychelles",
"ext_cg" => "Congo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Capo Verde", "ext_mh" => "Isole Marshall",
"ext_pm" => "St. Pierre e Miquelon", "ext_so" => "Somalia",
"ext_vc" => "St. Vincent e Grenadines", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberia",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Isola Bouvet",
"ext_fk" => "Isole Falkland (Malvine)", "ext_gq" => "Guinea Equatoriale",
"ext_sd" => "Sudan", "ext_sj" => "Isole Svalbard e Jan Mayen",
"ext_sr" => "Suriname", "ext_sy" => "Siria",
"ext_tp" => "Timor Est", "ext_um" => "Isole Minori degli Stati Uniti",
"ext_wf" => "Isole Wallis e Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numerico",

// Miscellaneoux translations
"misc_other" => "Altro",
"misc_unknown" => "Sconosciuto",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Sito Principale",
"navbar_Configuration" => "Configurazione",
"navbar_Global_Stats" => "Statistiche globali",
"navbar_Detailed_Stats" => "Statistiche dettagliate",
"navbar_Time_Stats" => "Statistiche temporali",
"navbar_Link_Stats" => "Stats dei Links",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Data e ora",
"dstat_Visits" => "Visite",
"dstat_Extension" => "Estensione",
"dstat_DNS" => "Nome host",
"dstat_From" => "Da",
"dstat_OS" => "Sistema Operativo",
"dstat_Browser" => "Browser",
"dstat_New_access" => "Nuovo/i Accesso/i",
"dstat_Elapsed_time" => "Tempo impiegato",
"dstat_No_new_access" => "Nessun nuovo accesso",
"dstat_Visible_accesses" => "Accessi visibili",
"dstat_green_rows" => "righe verdi",
"dstat_blue_rows" => "righe blu",
"dstat_red_rows" => "righe rosse",
"dstat_last_visit" => "ultima visita",
"dstat_robots" => "robots",

// Global stats words

"gstat_Accesses" => "Accessi",
"gstat_Total_visits" => "Visite totali",
"gstat_Total_unique" => "Totali unici",
"gstat_New_visits" => "Nuove visite",
"gstat_New_unique" => "Nuovi unici",
"gstat_Blacklisted" => "Blacklisted",
"gstat_Operating_systems" => "Sistemi operativi",
"gstat_Browsers" => "Browser",
"gstat_n_first_extensions" => "%d prime estensioni",
"gstat_Robots" => "Robot",
"gstat_n_first_pages" => "%d prime pagine",
"gstat_n_first_origins" => "%d primi referenti",
"gstat_Total" => "Totale",
"gstat_Not_specified" => "Non specificato",

// Time stats words
"tstat_Su" => "Dom",
"tstat_Mo" => "Lun",
"tstat_Tu" => "Mar",
"tstat_We" => "Mer",
"tstat_Th" => "Gio",
"tstat_Fr" => "Ven",
"tstat_Sa" => "Sab",

"tstat_Jan" => "Gen",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Apr",
"tstat_May" => "Mag",
"tstat_Jun" => "Giu",
"tstat_Jul" => "Lug",
"tstat_Aug" => "Ago",
"tstat_Sep" => "Set",
"tstat_Oct" => "Ott",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dic",

"tstat_Last_day" => "Ultimo giorno",
"tstat_Last_week" => "Ultima settimana",
"tstat_Last_month" => "Ultimo mese",
"tstat_Last_year" => "Ultimo anno",

// Configuration page words and sentences

"config_Variable_name" => "Nome della variabile",
"config_Variable_value" => "Valore della variabile",
"config_Explanations" => "Spiegazioni",

"config_bbc_mainsite" =>
"L'URL del tuo sito web.<br>
Se vuoto, questo URL non appare nella barra di navigazione di tutte le pagine di BBClone.<br>
<br>
<i>Esempio:</i><br>
\$BBC_MAINSITE = \"http://www.miositoweb.com/daqualcheparte/\".",

"config_bbc_show_config" =>
"Determina se la configurazione debba essere visibile o meno attraverso show_config.php",

"config_bbc_titlebar" =>
"Il titolo che appare nella barra del titolo di tutte le pagine di BBClone.<br>
Le macro disponibili sono:<br>
<ul>
<li>%SERVER : nome del server,
<li>%DATE : la data corrente.
</ul>
Sono consentite le tag HTML.",

"config_bbc_language" =>
"La lingua che vuoi utilizzare. Quella predefinita � english.<br>
Per conoscere le lingue disponibili, guarda: 
<a href=\"http://bbclone.de\">sezione download</a> del sito di BBClone.",

"config_bbc_maxtime" =>
"Intervallo di tempo (in secondi) tra due differenti visite dallo stessa coppia IP/AGENT.<br>
Il valore predefinito �: 1800 s",

"config_bbc_maxvisible" =>
"Quante entrate vuoi vedere nelle statistiche dettagliate?<br>
Il valore predefinito � 100. Non impostare ad un valore maggiore di 500.",

"config_bbc_maxos" =>
"Quanti Sistemi Operativi vuoi vedere elencati nelle statistiche globali?",

"config_bbc_maxbrowser" =>
"Quanti browser vuoi vedere elencati nelle statistiche globali?",

"config_bbc_maxextension" =>
"Quante estensioni vuoi vedere elencate nelle statistiche globali?",

"config_bbc_maxrobot" =>
"Quanti robot vuoi vedere elencati nelle statistiche globali?",

"config_bbc_maxpage" =>
"Quante pagine vuoi vedere elencate nelle statistiche globali?",

"config_bbc_maxorigin" =>
"Quanti referenti vuoi vedere elencati nelle statistiche globali?",

"config_bbc_ignoreip" =>
"Quali indirizzi IP (o di sottorete) vuoi ignorare ?<br>
<i>Formato:</i> &lt;indirizzo IP o di sottorete&gt;, &lt;un altro indirizzo IP o di sottorete&gt;<br>
Inserisci una virgola \",\" tra ogni IP. Il valore predefinito � l'IP \"local\".",

"config_bbc_ignore_refer" =>
"Se gestisci dei siti che non vuoi vedere elencati nella lista di primi referenti, 
puoi inserirli qui. Il referente sar� indicato come \"Non specificato\" e gli hit non andranno perduti. 
Usa il formato seguente:<br />
\$bbc[\"ignore_refer\"] = \"www.sito1.org, altro.sito2.org, unaltro.sito3.org\";<br />
e cos� via.",

"config_bbc_own_refer" =>
"Se questa opzione � impostata, tutti i referenti che hanno origine dal server sul quale gira 
BBClone saranno mostrati come http://www.myserver.com/ (nome simbolico per il tuo server) nella 
tabella dei referenti. Questo � utile se non vuoi che BBClone mostri dei percorsi a pagine di 
amministrazione, directory protette o altro materiale privato.",

"config_bbc_no_string" =>
"BBClone scrive un commento nel sorgente html per indicare il suo stato corrente. Tuttavia questo 
commento, per quanto utile, pu� interferire con alcuni forum o sistemi di content management. Se ottieni 
delle pagine vuote o dei messaggi di \"header gi� inviato\", � necessario attivare questa opzione 
perch� lo script funzioni di nuovo.",

"config_bbc_detailed_stat_fields" =>
"La variabile \$bbc[\"detailed_stats_fields\"] determina i campi da mostrare in show_detailed.php.<br>
I campi possibili sono:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"refer\", \"os\", \"browser\", \"ext\" (estensione)<br>
L'ordine dei campi � importante.<br>
I campi non presenti non saranno mostrati nella pagina delle statistiche.<br>
<br>
<i>Esempi:</i><br>
\$bbc[\"detailed_stats_fields\"] = \"id, time, visits, ext, os, browser\"<br>
\$bbc[\"detailed_stats_fields\"] = \"date, country, browser, os, hostname\"<br>",

"config_bbc_general_align_style" =>
"Puoi impostare qui lo stile di allineamento della pagina principale delle statistiche. 
I valori possibili sono \"left\" (sinistra), \"right\" (destra), \"center\" (centrato)",

"config_bbc_title_size" =>
"La dimensione dei titoli, da 0 (il pi� piccolo) a 6 (il pi� grande)",

"config_bbc_subtitle_size" =>
"La dimensione dei sottotitoli, da 0 (il pi� piccolo) a 6 (il pi� grande)",

"config_bbc_text_size" =>
"La dimensione del testo, da 0 (il pi� piccolo) a 6 (il pi� grande)"

);
?>
