<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/de.php,v 1.23 2004/02/15 19:39:12 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: de.php
* Summary: contains a german translation table
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date())
"global_date_format" => "d.m.Y",

// Global translation
"global_bbclone_copyright" => "Das BBClone Team - Lizensiert unter der",
"global_yes" => "ja",
"global_no" => "nein",

// The error messages
"error_cannot_see_config" =>
"Es ist Ihnen nicht gestattet, die Einstellungen von BBClone auf diesem Server einzusehen.",

// Address Extensions (see lib/extension.php)
"ext_oth" => "Sonstige", "ext_com" => "Unternehmen",
"ext_net" => "Netzwerke", "ext_edu" => "Lehreinrichtungen",
"ext_biz" => "Gewerbe", "ext_info" => "Auskunft",
"ext_jp" => "Japan", "ext_us" => "Vereinigte Staaten",
"ext_uk" => "Gro&szlig;britannien", "ext_de" => "Deutschland",
"ext_mil" => "US Streitkr&auml;fte", "ext_ca" => "Kanada",
"ext_it" => "Italien", "ext_au" => "Australien",
"ext_org" => "Organisationen", "ext_nl" => "Holland",
"ext_fr" => "Frankreich", "ext_tw" => "Taiwan",
"ext_gov" => "Regierungsbeh&ouml;rden", "ext_fi" => "Finnland",
"ext_br" => "Brasilien", "ext_se" => "Schweden",
"ext_es" => "Spanien", "ext_no" => "Norwegen",
"ext_mx" => "Mexiko", "ext_kr" => "Korea",
"ext_ch" => "Schweiz", "ext_dk" => "D&auml;nemark",
"ext_be" => "Belgien", "ext_at" => "&Ouml;sterreich",
"ext_nz" => "Neuseeland", "ext_ru" => "Russische F&ouml;deration",
"ext_pl" => "Polen", "ext_za" => "S&uuml;dafrika",
"ext_unknown" => "Unbekannt", "ext_ar" => "Argentinien",
"ext_il" => "Israel", "ext_sg" => "Singapur",
"ext_arpa" => "Fehler", "ext_cz" => "Tschechien",
"ext_hu" => "Ungarn", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "T&uuml;rkei",
"ext_gr" => "Griechenland", "ext_cn" => "China",
"ext_ie" => "Irland", "ext_my" => "Malaysia",
"ext_th" => "Thailand", "ext_cl" => "Chile",
"ext_co" => "Columbien", "ext_is" => "Island",
"ext_uy" => "Uruguay", "ext_ee" => "Estland",
"ext_in" => "Indien", "ext_ua" => "Ukraine",
"ext_sk" => "Slowakei", "ext_ro" => "Rum&auml;nien",
"ext_ae" => "Vereinigte Arabische Emirate", "ext_id" => "Indonesien",
"ext_su" => "Sowjetunion", "ext_si" => "Slowenien",
"ext_hr" => "Kroatien", "ext_ph" => "Philippinen",
"ext_lv" => "Lettland", "ext_ve" => "Venezuela",
"ext_bg" => "Bulgarien", "ext_lt" => "Litauen",
"ext_yu" => "Jugoslawien", "ext_lu" => "Luxemburg",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Internationale Organisationen",
"ext_do" => "Dominikanische Republik", "ext_cy" => "Zypern",
"ext_pk" => "Pakistan", "ext_cc" => "Kokusinseln",
"ext_tt" => "Trinidad und Tobago", "ext_eg" => "&Auml;gypten",
"ext_lb" => "Libanon", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Kasachstan",
"ext_na" => "Namibia", "ext_mu" => "Mauritius",
"ext_bm" => "Bermuda", "ext_sa" => "Saudi Arabien",
"ext_zw" => "Zimbabwe", "ext_kg" => "Kirgisien",
"ext_cx" => "Weihnachtsinsel", "ext_pa" => "Panama",
"ext_gt" => "Guatemala", "ext_bw" => "Botswana",
"ext_mk" => "Mazedonien", "ext_gl" => "Gr&ouml;nland",
"ext_ec" => "Ecuador", "ext_lk" => "Sri Lanka",
"ext_md" => "Moldawien", "ext_py" => "Paraguay",
"ext_bo" => "Bolivien", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Faroer",
"ext_ac" => "Ascension", "ext_pr" => "Puerto Rico",
"ext_am" => "Armenien", "ext_pf" => "Franz&ouml;sisch Polynesien",
"ext_ge" => "Georgien", "ext_bh" => "Bahrain",
"ext_ni" => "Nicaragua", "ext_by" => "Wei&szlig;ru&szlig;land",
"ext_sv" => "El Salvador", "ext_ma" => "Marokko",
"ext_ke" => "Kenia", "ext_ad" => "Andorra",
"ext_zm" => "Sambia", "ext_np" => "Nepal",
"ext_bt" => "Bhutan", "ext_sz" => "Swaziland",
"ext_ba" => "Bosnien und Herzegowina", "ext_om" => "Oman",
"ext_jo" => "Jordanien", "ext_ir" => "Iran",
"ext_st" => "Sao Tome und Principe", "ext_vi" => "Jungferninseln (US)",
"ext_ci" => "Elfenbeink&uuml;ste", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Kaimaninseln",
"ext_gp" => "Guadalupe", "ext_mg" => "Madagaskar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Amerikanisch Samoa", "ext_tz" => "Tansania",
"ext_ws" => "Samoa", "ext_tm" => "Turkmenistan",
"ext_mc" => "Monaco", "ext_sn" => "Senegal",
"ext_hm" => "Heard und McDonald-Inseln", "ext_fm" => "Mikronesien",
"ext_fj" => "Fiji", "ext_cu" => "Kuba",
"ext_rw" => "Ruanda", "ext_mq" => "Martinique",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Neuguinea",
"ext_bz" => "Belize", "ext_sh" => "St. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Malediwen",
"ext_nc" => "Neukaledonien", "ext_ag" => "Antigua und Barbuda",
"ext_uz" => "Usbekistan", "ext_tj" => "Tadschikistan",
"ext_sb" => "Solomoninseln", "ext_bf" => "Burkina Faso",
"ext_kh" => "Kambodscha", "ext_tc" => "Turks und Caicosinseln",
"ext_tf" => "Franz&ouml;sische S&uuml;dliche Territorien", "ext_az" => "Aserbaidschan",
"ext_dm" => "Dominica", "ext_mz" => "Mo&ccdil;ambique",
"ext_mo" => "Ma&ccdil;ao", "ext_vu" => "Vanuatu",
"ext_mn" => "Mongolien", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "Niger", "ext_gf" => "Franz&ouml;sich Guayana",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Albanien", "ext_gh" => "Ghana",
"ext_nf" => "Norfolk-Inseln", "ext_io" => "Britisches Indische-Ozean-Territorium",
"ext_gs" => "S&uuml;dgeorgien und S&uuml;d-Sandwich-Inseln", "ext_ye" => "Jemen",
"ext_an" => "Niederl&auml;ndische Antillen", "ext_aq" => "Antarktis",
"ext_tn" => "Tunesien", "ext_ck" => "Cook Inseln",
"ext_ls" => "Lesotho", "ext_et" => "&Auml;thiopien",
"ext_ng" => "Nigeria", "ext_sl" => "Sierra Leone",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Jungferninseln (UK)", "ext_vn" => "Vietnam",
"ext_mr" => "Mauritanien", "ext_gy" => "Guayana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibouti",
"ext_km" => "Comoros", "ext_dz" => "Algerien",
"ext_im" => "Insel Man", "ext_pn" => "Pitcairn",
"ext_qa" => "Quatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gabun",
"ext_gb" => "Gro&szlig;britannien", "ext_bs" => "Bahamas",
"ext_va" => "Vatikanstaat (Heiliger Stuhl)", "ext_lc" => "St. Lucia",
"ext_cd" => "Kongo (Demokratische Republik)", "ext_gm" => "Gambia",
"ext_mp" => "N&ouml;rdliche Mariannen-Inseln", "ext_gw" => "Guinea-Bissau",
"ext_cm" => "Kamerun", "ext_ao" => "Angola",
"ext_er" => "Eritrea", "ext_ly" => "Libyen",
"ext_cf" => "Zentralafrikanische Republik", "ext_mm" => "Myanmar",
"ext_td" => "Tschad", "ext_iq" => "Irak",
"ext_kn" => "St. Kitts und Nevis", "ext_sc" => "Seychellen",
"ext_cg" => "Kongo", "ext_gd" => "Grenada",
"ext_nr" => "Nauru", "ext_af" => "Afghanistan",
"ext_cv" => "Cap Verde", "ext_mh" => "Marshall Inseln",
"ext_pm" => "St. Pierre und Miquelon", "ext_so" => "Somalia",
"ext_vc" => "St. Vincent und die Grenadinen", "ext_bd" => "Bangladesh",
"ext_gn" => "Guinea", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Liberien",
"ext_mw" => "Malawi", "ext_pw" => "Palau",
"ext_re" => "Reunion", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Bouvet-Insel",
"ext_fk" => "Falkland-Inseln", "ext_gq" => "&Auml;quatorisch Guinea",
"ext_sd" => "Sudan", "ext_sj" => "Svalbard und Jan-Mayen",
"ext_sr" => "Surinam", "ext_sy" => "Syrien",
"ext_tp" => "Ost-Timor", "ext_um" => "USA kleinere au&szlig;erhalb liegende Inseln",
"ext_wf" => "Wallis und Futuna Inseln", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Numerisch",

// Miscellaneous translations
"misc_other" => "Sonstige",
"misc_unknown" => "unbekannt",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "Hauptseite",
"navbar_Configuration" => "Einstellungen",
"navbar_Global_Stats" => "Allgemeine Statistik",
"navbar_Detailed_Stats" => "Ausf&uuml;hrliche Statistik",
"navbar_Time_Stats" => "Zeitstatistik",
"navbar_Link_Stats" => "Linkstatistik",

// Detailed stats words
"dstat_ID" => "Z&auml;hler",
"dstat_Time" => "Zuletzt",
"dstat_Visits" => "Aufrufe",
"dstat_Extension" => "Kennung",
"dstat_DNS" => "Hostname",
"dstat_From" => "Gekommen von",
"dstat_OS" => "BS",
"dstat_Browser" => "Browser",
"dstat_New_access" => "Neue Zugriffe",
"dstat_Elapsed_time" => "vergangene Zeit",
"dstat_No_new_access" => "keine neuen Zugriffe",
"dstat_Visible_accesses" => "Angezeigte Zugriffe",
"dstat_green_rows" => "gr&uuml;ne Zeilen",
"dstat_blue_rows" => "blaue Zeilen",
"dstat_red_rows" => "rote Zeilen",
"dstat_last_visit" => "letzter Aufruf",
"dstat_robots" => "Robots",

// Global stats words

"gstat_Accesses" => "Zugriffe",
"gstat_Total_visits" => "Aufrufe insgesamt",
"gstat_Total_unique" => "Einmalige Aufrufe",
"gstat_New_visits" => "Neue Aufrufe",
"gstat_New_unique" => "Neue einmalige Aufrufe",
"gstat_Blacklisted" => "Schwarze Liste",
"gstat_Operating_systems" => "Die ersten %d Betriebssysteme",
"gstat_Browsers" => "Die ersten %d Browser",
"gstat_n_first_extensions" => "Die ersten %d Kennungen",
"gstat_Robots" => "Die ersten %d Robots",
"gstat_n_first_pages" => "Die ersten %d Seiten",
"gstat_n_first_origins" => "Die ersten %d Herk&uuml;nfte",
"gstat_Total" => "Insgesamt",
"gstat_Not_specified" => "keine Angaben",

// Time stats words
"tstat_Su" => "So",
"tstat_Mo" => "Mo",
"tstat_Tu" => "Di",
"tstat_We" => "Mi",
"tstat_Th" => "Do",
"tstat_Fr" => "Fr",
"tstat_Sa" => "Sa",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "M&auml;r",
"tstat_Apr" => "Apr",
"tstat_May" => "Mai",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Aug",
"tstat_Sep" => "Sep",
"tstat_Oct" => "Okt",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dez",

"tstat_Last_day" => "Bisheriger Tag",
"tstat_Last_week" => "Bisherige Woche",
"tstat_Last_month" => "Bisheriger Monat",
"tstat_Last_year" => "Bisheriges Jahr",

// Configuration page words and sentences

"config_Variable_name" => "Name der Variable",
"config_Variable_value" => "Wert der Variable",
"config_Explanations" => "Erl&auml;uterung",

"config_bbc_mainsite" =>
"Die URL Ihrer Webseite.<br>
Ist dieser Eintrag leer, wird die URL nicht in der Navigationsleiste der BBClone Seiten angezeigt.<br>
<br>
<i>Beispiel:</i><br>
\$BBC_MAINSITE = \"http://www.meinhoster.de/irgendwo/\".",

"config_bbc_show_config" =>
"Legt fest, ob die Daten von show_config.php eingesehen werden d&uuml;rfen oder nicht",

"config_bbc_titlebar" =>
"Der Titel Ihrer Statistikseite.<br>
Dieser wird in der Navigationsleiste aller BBClone Seiten angezeigt.<br>
Folgende Makros werden ausgewertet:<br>
<ul>
<li>%SERVER: Server Name,
<li>%DATE: aktuelles Datum.
</ul>
HTML Tags sind erlaubt.",

"config_bbc_language" =>
"Die verwendete Sprache. Voreinstellung ist Englisch.<br>
Nach weiteren &Uuml;bersetzungen k&ouml;nnen Sie im 
<a href=\"http://bbclone.de\">Downloadbereich</a> der<br>
BBClone Webseite Ausschau halten.",

"config_bbc_maxtime" =>
"Zeitspanne (in Sekunden) zwischen 2 gez&auml;hlten Besuchen der gleichen IP/AGENT Kombination.<br>
Voreinstellung ist der sich abzeichnende Standard: 1800 s",

"config_bbc_maxvisible" =>
"Auf wieviele Eintr&auml;ge m&ouml;chten Sie *individuell* zugreifen k&ouml;nnen?<br>
Voreinstellung ist 100, setzen Sie diesen Wert nicht h&ouml;her als 500<br>
(was davon abgesehen sinnlos ist)",

"config_bbc_maxos" =>
"Wieviele Betriebssysteme m&ouml;chten Sie in der allgemeinen Statistik aufgef&uuml;hrt sehen?",

"config_bbc_maxbrowser" =>
"Wieviele Browser m&ouml;chten Sie in der allgemeinen Statistik aufgef&uuml;hrt sehen?",

"config_bbc_maxextension" =>
"Wieviele Kennungen m&ouml;chten Sie in der allgemeinen Statistik aufgef&uuml;hrt sehen?",

"config_bbc_maxrobot" =>
"Wieviele Robots m&ouml;chten Sie in der allgemeinen Statistik aufgef&uuml;hrt sehen?",

"config_bbc_maxpage" =>
"Wieviele Seiten m&ouml;chten Sie in der allgemeinen Statistik aufgef&uuml;hrt sehen?",

"config_bbc_maxorigin" =>
"Wieviele Besucherherk&uuml;nfte m&ouml;chten Sie in der allgemeinen Statistik aufgef&uuml;hrt sehen?",

"config_bbc_ignoreip" =>
"Welche IP Adresse (oder welches Subnetz) soll nicht gewertet werden?<br>
<i>Format:</i> &lt;IP Adresse oder Subnetz&gt;, &lt;andere IP Adresse oder anderes Subnetz&gt;<br>
Verwenden Sie ein Komma \",\" zwischen jeder IP Adresse. Standard ist die \"lokale\" IP.",

"config_bbc_ignore_refer" =>
"Falls Sie eine Reihe anderer Seiten betreiben und diese nicht in ihrer Referrer Rangliste aufgef&uuml;hrt 
werden sollen, so k�nnen sie deren Hostnamen hier eintragen. Der Referrer wird dann mit \"keine Angaben\"
gewertet wobei kein Treffer verloren geht. Verwenden Sie folgendes Format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, anderer.host2.org, nocheinanderer.host3.org\";<br />
und so weiter.",

"config_bbc_own_refer" =>
"Wenn diese Option gesetzt ist, werden alle Referrer, die vom Server stammen auf welchem BBClone l&auml;uft als 
http://www.meinserver.de/ (Platzhalter f&uuml;r Ihren Servernamen) in Ihrer Referrer Rangliste angegeben. Das 
ist dann von Nutzen wenn Sie BBClone davon abhalten wollen, Pfade zu adminstrativen Seiten, gesch&uuml;tzten 
Verzeichnissen oder andere Sachen die Sie f&uuml;r sich behalten m&ouml;chten anzuzeigen.",

"config_bbc_no_string" => "BBClone schreibt einen Kommentar in den HTML Quellcode um seinen 
gegenw&auml;rtigen Status anzuzeigen. Jedoch k�nnte der Output, obwohl praktisch, mit manchen 
Foren oder Content Management Systemen in Konflikt geraten. Falls Sie mit einer leeren Seite 
oder einer Reihe von\"header already sent by\" Meldungen konfrontiert werden, m&uuml;ssen sie 
diese Option aktivieren, um Ihre Skripte wieder lauff&auml;hig zu machen",

"config_bbc_detailed_stat_fields" =>
"Der Wert \$BBC_DETAILED_STAT_FIELDS legt fest, welche Spalten in show_detailed.php angezeigt werden.<br>
M&ouml;gliche Spalten sind:<br>
\"id\", \"time\", \"visits\", \"dns\" (Hostname), \"referer\", \"os\", \"browser\", \"ext\" (Kennung)<br>
Die Reihenfolge der Spalten ist wichtig.<br>
Nicht vorhandene Spalten werden auf der Statistikseite fehlen.<br>
<br>
<i>Beispiel:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, ext, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Sie k&ouml;nnen festlegen, wie die Hauptseite der Statistik ausgerichtet wird. 
M&ouml;gliche Werte sind \"left\" (links), \"right\" (rechts), \"center\" (zentriert)",

"config_bbc_title_size" =>
"Die Gr&ouml;&szlig;e der &Uuml;berschrift von 0 (am kleinsten) bis 6 (am gr&ouml;&szlig;ten)",

"config_bbc_subtitle_size" =>
"Die Gr&ouml;&szlig;e der Unter&uuml;berschrift von 0 (am kleinsten) bis 6 (am gr&ouml;&szlig;ten)",

"config_bbc_text_size" =>
"Die Gr&ouml;&szlig;e des normalen Textes von 0 (am kleinsten) bis 6 (am gr&ouml;&szlig;ten)"

);
?>