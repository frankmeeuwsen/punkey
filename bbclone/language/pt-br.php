<?php
/*
* This file is part of BBClone (The PHP web counter on steroids)
*
* $Header: /cvs/bbclone-0.3x/language/pt-br.php,v 1.12 2004/02/15 19:39:13 joku Exp $
*
* Copyright (C) 2001-2004, the BBClone Team (see the file authors.txt for details)
* Licensed under the terms of the GNU/GPL, see doc/copying.txt for details
*
* File: pt-br.php
* Summary: contains a portuguese translation table
*/

// The main array ($_ is for doing short in its call)
$_ = array(
// Specific charset
"global_charset" => "iso-8859-15",

// Date format (used with date() )
"global_date_format" => "d/m/Y",

// Global translation
"global_bbclone_copyright" => "O time BBClone - Distribu�do sob a Licen�a",
"global_yes" => "sim",
"global_no" => "n�o",

// The error messages
"error_cannot_see_config" =>
"N�o � permitido ver a configura��o do BBClone configuration neste servidor.",

// Address Extensions (see lib/extension.php)
"ext_other" => "Outros", "ext_com" => "Comercial",
"ext_net" => "Redes", "ext_edu" => "Educacional",
"ext_biz" => "Business", "ext_info" => "Information",
"ext_jp" => "Jap�o", "ext_us" => "Estados Unidos",
"ext_uk" => "Reino Unido", "ext_de" => "Alemanha",
"ext_mil" => "Militar dos EUA", "ext_ca" => "Canad�",
"ext_it" => "Italia", "ext_au" => "Australia",
"ext_org" => "Organiza��es", "ext_nl" => "Holanda",
"ext_fr" => "Fran�a", "ext_tw" => "Taiwan",
"ext_gov" => "Governo", "ext_fi" => "Finl�ndia",
"ext_br" => "Brasil", "ext_se" => "Su�cia",
"ext_es" => "Espanha", "ext_no" => "Noruega",
"ext_mx" => "M�xico", "ext_kr" => "Cor�ia",
"ext_ch" => "Su�ca", "ext_dk" => "Dinamarca",
"ext_be" => "Belgica", "ext_at" => "�ustria",
"ext_nz" => "Nova Zelandia", "ext_ru" => "Federa��o Russa",
"ext_pl" => "Pol�nia", "ext_za" => "�frica do Sul",
"ext_unknown" => "Desconhecido", "ext_ar" => "Argentina",
"ext_il" => "Israel", "ext_sg" => "Singapura",
"ext_arpa" => "Enganos", "ext_cz" => "Rep�blica Czech",
"ext_hu" => "Hungria", "ext_hk" => "Hong Kong",
"ext_pt" => "Portugal", "ext_tr" => "Turquia",
"ext_gr" => "Grecia", "ext_cn" => "China",
"ext_ie" => "Irlanda", "ext_my" => "Malasia",
"ext_th" => "Tail�ndia", "ext_cl" => "Chile",
"ext_co" => "Col�mbia", "ext_is" => "Iceland",
"ext_uy" => "Uruguai", "ext_ee" => "Est�nia",
"ext_in" => "�ndia", "ext_ua" => "Ucr�nia",
"ext_sk" => "Eslov�quia", "ext_ro" => "Rom�nia",
"ext_ae" => "Emirados �rabes Unidos", "ext_id" => "Indon�sia",
"ext_su" => "Uni�o Sovi�tica", "ext_si" => "Eslov�nia",
"ext_hr" => "Cro�cia", "ext_ph" => "Filipinas",
"ext_lv" => "Latvia", "ext_ve" => "Venezuela",
"ext_bg" => "Bulg�ria", "ext_lt" => "Lit�nia",
"ext_yu" => "Iugoslavia", "ext_lu" => "Luxemburgo",
"ext_nu" => "Niue", "ext_pe" => "Peru",
"ext_cr" => "Costa Rica", "ext_int" => "Organiza��es Internacionais",
"ext_do" => "Rep�blica Dominicana", "ext_cy" => "Chipre",
"ext_pk" => "Paquist�o", "ext_cc" => "Ilhas Cocos (Keeling)",
"ext_tt" => "Trinidad e Tobago", "ext_eg" => "Egito",
"ext_lb" => "L�bano", "ext_kw" => "Kuwait",
"ext_to" => "Tonga", "ext_kz" => "Casaquist�o",
"ext_na" => "Nam�bia", "ext_mu" => "Mauritania",
"ext_bm" => "Bermuda", "ext_sa" => "Ar�bia Saudita",
"ext_zw" => "Zimb�bue", "ext_kg" => "Quirguiquist�o",
"ext_cx" => "Ilhas Natal", "ext_pa" => "Panam�",
"ext_gt" => "Guatemala", "ext_bw" => "Botsuana",
"ext_mk" => "Maced�nia", "ext_gl" => "Groel�ndia",
"ext_ec" => "Equador", "ext_lk" => "Sri Lanka",
"ext_md" => "Mold�via", "ext_py" => "Paraguai",
"ext_bo" => "Bol�via", "ext_bn" => "Brunei",
"ext_mt" => "Malta", "ext_fo" => "Ilhas Faroe",
"ext_ac" => "Ilha Ascen��o", "ext_pr" => "Porto Rico",
"ext_am" => "Arm�nia", "ext_pf" => "Polin�sia Francesa",
"ext_ge" => "Georgia", "ext_bh" => "Bahrain",
"ext_ni" => "Nicar�gua", "ext_by" => "Belarus",
"ext_sv" => "El Salvador", "ext_ma" => "Morrocos",
"ext_ke" => "Qu�nia", "ext_ad" => "Andorra",
"ext_zm" => "Z�mbia", "ext_np" => "Nepal",
"ext_bt" => "But�o", "ext_sz" => "Suazilandia",
"ext_ba" => "Bosnia e Herzegovina", "ext_om" => "Om�",
"ext_jo" => "Jord�nia", "ext_ir" => "Ir�",
"ext_st" => "S�o Tom� e Pr�ncipe", "ext_vi" => "Ilhas Virgens (EUA)",
"ext_ci" => "Costa do Marfim", "ext_jm" => "Jamaica",
"ext_li" => "Liechtenstein", "ext_ky" => "Ilhas Caimam",
"ext_gp" => "Guadalupe", "ext_mg" => "Madagascar",
"ext_gi" => "Gibraltar", "ext_sm" => "San Marino",
"ext_as" => "Samoa Americana", "ext_tz" => "Tanz�nia",
"ext_ws" => "Samoa", "ext_tm" => "Turcomenist�o",
"ext_mc" => "M�naco", "ext_sn" => "Senegal",
"ext_hm" => "Ilhas Heard e Mc Donald", "ext_fm" => "Micronesia",
"ext_fj" => "Fiji", "ext_cu" => "Cuba",
"ext_rw" => "Ruanda", "ext_mq" => "Martinica",
"ext_ai" => "Anguilla", "ext_pg" => "Papua Nova Guin�",
"ext_bz" => "Belize", "ext_sh" => "St. Helena",
"ext_aw" => "Aruba", "ext_mv" => "Maldivas",
"ext_nc" => "Nova Caledonia", "ext_ag" => "Ant�gua and Barbuda",
"ext_uz" => "Usbequist�o", "ext_tj" => "Tajiquist�o",
"ext_sb" => "Ilhas Salom�o", "ext_bf" => "Burkina Faso",
"ext_kh" => "Cambodja", "ext_tc" => "Ilhas Turks e Caicos",
"ext_tf" => "Territ�rio do Sul da Fran�a", "ext_az" => "Azerbaij�o",
"ext_dm" => "Dominica", "ext_mz" => "Mo�ambique",
"ext_mo" => "Macau", "ext_vu" => "Vanuatu",
"ext_mn" => "Mong�lia", "ext_ug" => "Uganda",
"ext_tg" => "Togo", "ext_ms" => "Montserrat",
"ext_ne" => "N�ger", "ext_gf" => "Guiana Francesa",
"ext_gu" => "Guam", "ext_hn" => "Honduras",
"ext_al" => "Alb�nia", "ext_gh" => "Gana",
"ext_nf" => "Ilha Norfolk", "ext_io" => "Territ�rio Brit�nico no Oceano �ndico",
"ext_gs" => "Ilhas Sul Georgia e Ilhas Sul Sandwich", "ext_ye" => "I�men",
"ext_an" => "Antilhas Holandesas", "ext_aq" => "Antarctica",
"ext_tn" => "Tun�sia", "ext_ck" => "Ilhas Cook",
"ext_ls" => "Lesoto", "ext_et" => "Eti�pia",
"ext_ng" => "Nigeria", "ext_sl" => "Serra Leoa",
"ext_bb" => "Barbados", "ext_je" => "Jersey",
"ext_vg" => "Ilhas Virges (Inglaterra)", "ext_vn" => "Vietn�",
"ext_mr" => "Mauritania", "ext_gy" => "Guiana",
"ext_ml" => "Mali", "ext_ki" => "Kiribati",
"ext_tv" => "Tuvalu", "ext_dj" => "Djibuti",
"ext_km" => "Comoros", "ext_dz" => "Alg�ria",
"ext_im" => "Ilha do Homem", "ext_pn" => "Pitcairn",
"ext_qa" => "Qatar", "ext_gg" => "Guernsey",
"ext_bj" => "Benin", "ext_ga" => "Gab�o",
"ext_gb" => "Gr�-Bretanha", "ext_bs" => "Bahamas",
"ext_va" => "Cidade Estado do Vaticano (Vista Sagrada)", "ext_lc" => "Santa L�cia",
"ext_cd" => "Congo", "ext_gm" => "G�mbia",
"ext_mp" => "Ilhas Mariana do Norte", "ext_gw" => "Guin�-Bissau",
"ext_cm" => "Camar�es", "ext_ao" => "Angola",
"ext_er" => "Eritr�a", "ext_ly" => "L�bia",
"ext_cf" => "Republica da �frica Central", "ext_mm" => "Myanmar",
"ext_td" => "Chade", "ext_iq" => "Iraque",
"ext_kn" => "Saint Kitts and Nevis", "ext_sc" => "Seycheles",
"ext_cg" => "Congo", "ext_gd" => "Granada",
"ext_nr" => "Nauru", "ext_af" => "Afeganist�o",
"ext_cv" => "Cabo Verde", "ext_mh" => "Ilhas Marshall",
"ext_pm" => "St. Pierre e Miquelon", "ext_so" => "Som�lia",
"ext_vc" => "St. Vincent e Grenadines", "ext_bd" => "Bangladesh",
"ext_gn" => "Guin�", "ext_ht" => "Haiti",
"ext_la" => "Laos", "ext_lr" => "Lib�ria",
"ext_mw" => "Malau�", "ext_pw" => "Palau",
"ext_re" => "Reuni�o", "ext_tk" => "Tokelau",
"ext_bi" => "Burundi", "ext_bv" => "Ilha Bouvet",
"ext_fk" => "Ilhas Falkland (Malvinas)", "ext_gq" => "Guin� Equatorial",
"ext_sd" => "Sud�o", "ext_sj" => "Ilhas Svalbard e Jan Mayen",
"ext_sr" => "Suriname", "ext_sy" => "S�ria",
"ext_tp" => "Timor Leste", "ext_um" => "Ilhas menores dos EUA",
"ext_wf" => "Ilhas Wallis e Futuna", "ext_yt" => "Mayotte",
"ext_zr" => "Zaire", "ext_IP" => "Num�rico",

// Miscellaneoux translations
"misc_other" => "Outro",
"misc_unknown" => "Desconhecido",
"misc_second_unit" => "s",

// The Navigation Bar
"navbar_Main_Site" => "P�gina Principal",
"navbar_Configuration" => "Configura��o",
"navbar_Global_Stats" => "Estat�sticas Globais",
"navbar_Detailed_Stats" => "Estat�sticas Detalhadas",
"navbar_Time_Stats" => "Estat�sticas Cronol�gicas",
"navbar_Link_Stats" => "Estat�sticas de Links",

// Detailed stats words
"dstat_ID" => "ID",
"dstat_Time" => "Tempo",
"dstat_Visits" => "Visitas",
"dstat_Extension" => "Extens�o",
"dstat_DNS" => "Hostname",
"dstat_From" => "De",
"dstat_OS" => "OS",
"dstat_Browser" => "Navegador",
"dstat_New_access" => "Novo(s) acesso(s)",
"dstat_Elapsed_time" => "Tempo decorrido",
"dstat_No_new_access" => "Nenhum novo acesso",
"dstat_Visible_accesses" => "Acessos vis�veis",
"dstat_green_rows" => "linhas verdes",
"dstat_blue_rows" => "linhas azuis",
"dstat_red_rows" => "linhas vermelhas",
"dstat_last_visit" => "�ltima visita",
"dstat_robots" => "rob�s",

// Global stats words

"gstat_Accesses" => "Acessos",
"gstat_Total_visits" => "Total visitas",
"gstat_Total_unique" => "Total �nicas",
"gstat_New_visits" => "Novas visitas",
"gstat_New_unique" => "Novo �nico",
"gstat_Blacklisted" => "Lista negra",
"gstat_Operating_systems" => "Sistemas operacionais: %d Mais",
"gstat_Browsers" => "Navegadores: %d Mais",
"gstat_n_first_extensions" => "Extens�es: %d Mais",
"gstat_Robots" => "Rob�s: %d Mais",
"gstat_n_first_pages" => "P�ginas Visitadas: %d Mais",
"gstat_n_first_origins" => "Origens: %d Mais",
"gstat_Total" => "Total",
"gstat_Not_specified" => "N�o especificado",

// Time stats words
"tstat_Su" => "Dom",
"tstat_Mo" => "Seg",
"tstat_Tu" => "Ter",
"tstat_We" => "Qua",
"tstat_Th" => "Qui",
"tstat_Fr" => "Sex",
"tstat_Sa" => "S�b",

"tstat_Jan" => "Jan",
"tstat_Feb" => "Feb",
"tstat_Mar" => "Mar",
"tstat_Apr" => "Abr",
"tstat_May" => "Mai",
"tstat_Jun" => "Jun",
"tstat_Jul" => "Jul",
"tstat_Aug" => "Ago",
"tstat_Sep" => "Set",
"tstat_Oct" => "Out",
"tstat_Nov" => "Nov",
"tstat_Dec" => "Dez",

"tstat_Last_day" => "�ltimo dia",
"tstat_Last_week" => "�ltima semana",
"tstat_Last_month" => "�ltimo m�s",
"tstat_Last_year" => "�ltimo ano",

// Configuration page words and sentences

"config_Variable_name" => "Nome da Vari�vel",
"config_Variable_value" => "Valor da Vari�vel",
"config_Explanations" => "Explana��es",

"config_bbc_mainsite" =>
"A URL de nossa p�gina na Internet.<br>
Se vazia, esta URL n�o aparece na barra de navega��o de todas as p�ginas do BBClone.<br>
<br>
<i>Exemplo:</i><br>
\$BBC_MAINSITE = \"http://www.minhap�gina.com/algumlugar/\".",

"config_bbc_show_config" =>
"Determina se a configura��o deve ser vis�vel ou n�o atrav�s de show_config.php",

"config_bbc_titlebar" =>
"O t�tulo aparecendo dentro da barra de t�tulo presente em todas p�ginas do BBClone.<br>
Vari�veis reconhec�veis s�o:<br>
<ul>
<li>%SERVER: nome do servidor,
<li>%DATE: a data corrente.
</ul>
HTML tags s�o tamb�m permitidas.",

"config_bbc_language" =>
"O idioma que deseja usar. O padr�o � ingl�s.<br>
Para conhecer os idiomas dispon�veis, olhe na 
<a href=\"http://bbclone.de\">se��o de download</a> do site do BBClone.",

"config_bbc_maxtime" =>
"Intervalo de tempo (em segundos) entre duas diferentes visitas de um mesmpo par IP/AGENT.<br>
O padr�o razo�vel: 1800 s",

"config_bbc_maxvisible" =>
"Quantas entradas deseja ver nas estat�sticas detalhadas?<br>
O padr�o � 100, n�o configure isso mais que 500 (o qual � in�til, de qualquer maneira)",

"config_bbc_maxos" =>
"Quantos Sistemas Operacionais deseja ver classificados nas estat�sticas globais?",

"config_bbc_maxbrowser" =>
"Quantos navegadores deseja ver classificados nas estat�sticas globais?",

"config_bbc_maxextension" =>
"Quantas extens�es deseja ver classificadas nas estat�sticas globais?",

"config_bbc_maxrobot" =>
"Quantos rob�s deseja ver classificados nas estat�sticas globais?",

"config_bbc_maxpage" =>
"Quantas p�ginas deseja ver classificadas nas estat�sticas globais?",

"config_bbc_maxorigin" =>
"Quantas origens deseja ver classificadas nas estat�sticas globais?",

"config_bbc_ignoreip" =>
"Qual endere�o IP (ou sub-rede) deseja ignorar?<br>
<i>Formato:</i> &lt;endere�o IP ou sub-rede&gt;, &lt;outro endere�o IP ou sub-rede&gt;<br>
Inserir uma v�rgula \",\" entre cada IP. Padr�o s�o os IPs \"locais\".",

"config_bbc_ignore_refer" =>
"If you run a couple of sites and don't want them to be listed in your top 
referrer list, you can add the hostnames here. The referrer will be treated 
as \"not specified\" and no hits are lost. Use the following format:<br />
\$BBC_IGNORE_REFER = \"www.host1.org, another.host2.org, yetanother.host3.org\";<br />
and so on.",

"config_bbc_own_refer" =>
"If this flag is set, all referrers originating from the server on which 
bbclone is running are displayed as http://www.myserver.com/ (placeholder for your 
server name) in the referrer ranking. This is useful if you don't want 
bbclone to list paths to administrative pages, protected directories or other 
stuff you want to keep for yourself",

"config_bbc_no_string" => "BBClone writes a comment to the html source as indicator of its 
current state. However this output, though convenient, may interfere with some forums 
or content management systems. If you're confronted with a blank page or a couple of \"header 
already sent by\" messages you need to uncomment this flag to make your scripts work again.",

"config_bbc_detailed_stat_fields" =>
"A variavel \$BBC_DETAILED_STAT_FIELDS determina os campos para mostrar em show_detailed.php.<br>
Poss�veis nomes de campos s�o:<br>
\"id\", \"time\", \"visits\", \"dns\" (hostname), \"referer\", \"os\", \"browser\", \"ext\" (extension)<br>
A ordem dos nomes dos campos � importante.<br>
Campos n�o presentes estar�o faltando na p�gina de estat�stica.<br>
<br>
<i>Exemplos:</i><br>
\$BBC_DETAILED_STAT_FIELDS = \"id, time, visits, ext, os, browser\"<br>
\$BBC_DETAILED_STAT_FIELDS = \"date, browser, os, dns\"<br>",

"config_bbc_general_align_style" =>
"Poder� ser configurado aqui o estilo de alinhamento da p�gina principal de estat�sticas. Poss�veis 
valores s�o \"left\", \"right\", \"center\"",

"config_bbc_title_size" =>
"O tamanho dos t�tulos entre 0 (menor) e 6 (maior)",

"config_bbc_subtitle_size" =>
"O tamanho dos subt�tulos entre 0 (menor) e 6 (maior)",

"config_bbc_text_size" =>
"O tamanho do texto simples entre 0 (menor) e 6 (maior)"

);
?>