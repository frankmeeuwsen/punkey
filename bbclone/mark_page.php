<?php
/*
 * File: mark_page.php
 * Summary: insert a marker in a page to be recorded by bbclone
 * Description: mark_page.php uses 10 files to store the data, to increase
 *              speed and avoid locking problems
 *
 * This file is part of BBClone (A PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/mark_page.php,v 1.49 2004/02/16 20:18:07 joku Exp $
 *
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

if (!defined("_MARK_PAGE")) define("_MARK_PAGE", "1");
else return;
if (!defined("_BBCLONE_DIR")) return;
else $BBCLONE_DIR = _BBCLONE_DIR;
if (!is_readable($BBCLONE_DIR."constants.php")) return;
else require_once($BBCLONE_DIR."constants.php");
if (!is_readable($BBC_CONFIG_FILE)) return err_msg($BBC_CONFIG_FILE);
else require_once($BBC_CONFIG_FILE);

// Main Class Counter
class marker {
  var $sep;

  // remove unwanted stuff from user input
  function clean($input) {
    $input = (($sp = strpos($input, $this->sep)) !== false) ? substr($input, 0, $sp) : $input;
    $input = trim(htmlentities(strip_tags($input), ENT_QUOTES));

    return str_replace("$", "&#36;", str_replace("\\", "/", $input));
  }

  // randomly choose a counter file to write to
  function counter_file($cache_path, $counter_pre, $counter_suf) {
    mt_srand((double) microtime() * 1000000);
    return ($cache_path.$counter_pre.mt_rand(0, 9).$counter_suf);
  }

  // checks for a valid url format
  function valid_ref($ref) {
    $tmp = explode(":", $ref);

    for ($i = 0, $k = count($tmp); $i < $k; $i++) $tmp[$i] = trim($tmp[$i]);
    return (((($tmp[0] == "http") || ($tmp[0] == "https")) && (substr($tmp[1], 0, 2) == "//")) ? true : false);
  }

  // checks for a valid ip4 format
  function &valid_ip($addr, $prx = 0) {
    $iptest = explode(".", $addr);
    $oct = count($iptest);

    if ($oct == 4) {
      for ($i = 0; $i < $oct; $i++) {
        $iptest[$i] = trim($iptest[$i]);

        if ((!ereg("^[0-9]{1,3}$", $iptest[$i])) || ($iptest[$i] > 255)) return false;
      }
      if (($iptest[0] < 1) || ($iptest[0] > 223) || ($iptest[3] < 1) || ($iptest[3] > 254)) return false;
      // loopback and non public addresses are only relevant for proxy checking
      if (!empty($prx)) {
        if (($iptest[0] == 10) || ($iptest[0] == 127) ||
           (($iptest[0] == 169) && ($iptest[1] == 254)) ||
           (($iptest[0] == 172) && ($iptest[1] > 15) && ($iptest[1] < 32)) ||
           (($iptest[0] == 192) && ($iptest[1] === 0) && ($iptest[1] == 2)) ||
           (($iptest[0] == 192) && ($iptest[1] == 168))) return false;
      }
      return true;
    }
    else return false;
  }

  function filter_ref($host, $ref, $srv, $srvaddr) {
    $svr = !empty($srv) ? $srv : "noname";

    $test = !$this->valid_ref($ref) ? "$srv/" : substr(strstr($ref, "://"), 3);
    $test = (($slash = strpos($test, "/")) !== false) ? substr($test, 0, $slash) : $test;
    $test = (($port = strpos($test, ":")) !== false) ? substr($test, 0, $port) : $test;
    $testad = gethostbyname($test);

    // Things of our own which we like to keep
    if (!empty($host)) {
      $ref = (($host == $test) ? "http://$srv/" : $ref);
    }
    $ref = ((($test == $srvaddr) || ($test == $srv) || ((substr($srv, 0, 4) == "www.") &&
            (substr($srv, 4) == $test)) || ($test == gethostbyaddr($srvaddr))) ? "http://$srv/" : $ref);
    // Things we don't really want to have written down somewhere
    return (((substr($testad, 0, 4) == "127.") || (substr($testad, 0, 2) == "0.")) ? "" : $ref);
  }

  // determines whether we got an index file
  function is_index($name) {
    $test = explode(".", $name);
    $tmp = strtolower(trim($test[0]));

    return (((count($test) == 2) && (($tmp == "index") || ($tmp == "default"))) ? true : false);
  }

  // avoid trails of query strings which aren't relevant for page counting
  function &filter_uri($script, $pinfo, $uri) {
    $uri = ereg_replace("[.]+(/|$)", "\\1", $uri);
    $uri = ereg_replace("[/]+", "/", $uri);
    $uri = !empty($pinfo) ? substr($uri, 0, (strlen($uri) - strlen(ereg_replace("[/]+", "/", $pinfo)))) : $uri;
    $uri = (basename($uri) !== $script) ? (((($dir = dirname($uri)) == ".") || (empty($dir))) ? "/" : $dir."/")
           .$script : $uri;

    return ($this->is_index($script) ? substr($uri, 0, (strrpos($uri, "/") + 1)) : (empty($uri) ? "/" : $uri));
  }

  // automatic page name generation in case of not being specified
  function auto_page_name($uri) {
    if (is_string($uri) && ($uri != "/")) {
      $uri = (strlen($uri) - 1 === ($slash = strrpos($uri, "/"))) ? substr($uri, 1, --$slash) :
             (($dot = strrpos($uri, ".")) !== false ? substr($uri, 1, --$dot) : substr($uri, 1));
      $uri = str_replace("/", " -&gt; ", str_replace("_", " ", $uri));
      return ucwords($uri);
    }
    else return "index";
  }

  // reverse dns checking to avoid logging unresolvable hostnames
  function rev_res($ip) {
    $temphost = gethostbyaddr($ip);
    $tempip = gethostbyname($temphost);
    return (($ip != $tempip) ? $ip : $temphost);
  }

  // Sets the values for the string
  function set_values() {
    global $HTTP_SERVER_VARS;

    // loads of initialisations
    $hdr = array("HTTP_X_FORWARDED_FOR", "HTTP_FORWARDED_FOR", "HTTP_CLIENT_IP", "HTTP_USER_AGENT", "LOCAL_ADDR",
                 "REMOTE_HOST", "REMOTE_ADDR", "HTTP_HOST", "HTTP_REFERER", "PATH_INFO", "PHP_SELF", "SCRIPT_FILENAME",
                 "SERVER_NAME", "SERVER_ADDR");

    for ($i = 0, $max = count($hdr); $i < $max ; $i++) {
      ${$hdr[$i]} = !empty($HTTP_SERVER_VARS[($hdr[$i])]) ? $this->clean($HTTP_SERVER_VARS[($hdr[$i])]) : "";
    }
    // IIS's PATH_INFO is completely different from Apache's so we can't use it
    $REQUEST_URI = $this->filter_uri(basename((empty($SCRIPT_FILENAME) ? $PATH_INFO : $SCRIPT_FILENAME)),
                   (!empty($SCRIPT_FILENAME) ? $PATH_INFO : ""), $PHP_SELF);
    $SERVER_ADDR = empty($SERVER_ADDR) ? (empty($LOCAL_ADDR) ? gethostbyname($SERVER_NAME) : $LOCAL_ADDR) :
                   $SERVER_ADDR;
    $SERVER_ADDR = $this->valid_ip($SERVER_ADDR) ? $SERVER_ADDR : "127.0.0.1";
    $HTTP_REFERER = $this->filter_ref($HTTP_HOST, $HTTP_REFERER, $SERVER_NAME, $SERVER_ADDR);
    // If there's no $REMOTE_ADDR fall back to loopback
    $REMOTE_ADDR = empty($REMOTE_ADDR) ? "127.0.0.1" : ((substr($REMOTE_ADDR, 0, strpos($REMOTE_ADDR, ".")) == 127) ?
                   "127.0.0.1" : $REMOTE_ADDR);
    // catch up for the event Apache doesn't perform hostname lookups
    $REMOTE_HOST = empty($REMOTE_HOST) ? $this->clean(gethostbyaddr($REMOTE_ADDR)) : $REMOTE_HOST;

    // Proxy stuff (the first 3 of $hdr are proxy variables)
    for($i = 0; $i < 3; $i++) {
      // if a header contains "unknown" we have to make sure our server doesn't
      // resolve it to an address by appending a default extension to the string
      if ((!eregi("^unknown$", ${$hdr[$i]})) && (!empty(${$hdr[$i]}))) {
        $iptest = gethostbyname(${$hdr[$i]});
        // workaround for broken gethostbyname() on Windows which doesn't return "false"
        $iptest = ($iptest == $SERVER_ADDR) ? false : $iptest;
        // Sort out malformed, private, multi- and broadcast addresses
        $ip = ($this->valid_ip($iptest, 1)) ? $iptest : $REMOTE_ADDR;
        $REMOTE_HOST = $this->rev_res($ip);
	break;
      }
    }
    // we're getting here if we didn't find any proxy headers
    if (!isset($ip)) {
        $ip = $REMOTE_ADDR;
        $REMOTE_HOST = $this->rev_res($ip);
    }

    if (empty($HTTP_USER_AGENT)) $HTTP_USER_AGENT = "unknown";
    if (empty($HTTP_REFERER)) $HTTP_REFERER = "unknown";
    // Use a page name even if the user didn't specify it
    $scriptname = (defined("_BBC_PAGE_NAME") ? $this->clean(_BBC_PAGE_NAME) : $this->auto_page_name($REQUEST_URI));
    return time().$this->sep.$ip.$this->sep.$REMOTE_HOST.$this->sep.$HTTP_USER_AGENT.$this->sep.$HTTP_REFERER
          .$this->sep.$REQUEST_URI.$this->sep.$scriptname."\n";
  }

  // constructor
  function marker() {
    global $BBC_CACHE_PATH, $BBC_COUNTER_PREFIX, $BBC_COUNTER_SUFFIX, $BBC_NO_STRING, $BBC_SEP, $BBC_VERSION;

    $this->sep = $BBC_SEP;
    $filename = $this->counter_file($BBC_CACHE_PATH, $BBC_COUNTER_PREFIX, $BBC_COUNTER_SUFFIX);

    if (file_exists($BBC_CACHE_PATH)) {
      if (is_writable($filename)) {
        ignore_user_abort(1);
        $fp = fopen($filename, "a+");
        if (flock($fp, LOCK_EX)) {
          fputs($fp, $this->set_values());
          fflush($fp);
          flock($fp, LOCK_UN);
          echo (empty($BBC_NO_STRING) ? "\n<!-- BBClone v $BBC_VERSION OK: counter file is working -->\n" : "");
        }
        fclose($fp);
        ignore_user_abort(0);
      }
      else return err_msg($filename, "write");
    }
    else return err_msg($BBC_CACHE_PATH);
  }
}

ignore_user_abort(1);
//initialize the marker class
new marker;

// Check when it's time for a counter update
if (file_exists($BBC_CACHE_PATH)) {
  if (is_readable($BBC_ACCESS_FILE)) {
    if (is_writable($BBC_ACCESS_FILE)) {
      if (is_readable($BBC_LOG_PROCESSOR)) {
        if ((($now = time()) - filemtime($BBC_ACCESS_FILE)) >= $BBC_AUTO_UPDATE) {
          if (is_readable($BBC_CACHE_PATH.".htalock")) {
            if (is_writable($BBC_CACHE_PATH.".htalock")) {
              $al = fopen($BBC_CACHE_PATH.".htalock", "a+");

              if (flock($al, LOCK_EX)) {
                require_once($BBC_LOG_PROCESSOR);
                if (!defined("_error")) {
                  require_once($BBC_ACCESS_FILE);
                  // log processor stuff
                  if (add_new_connections_to_old()) {
                    update_last_access();
                    save_access();
                  }
                  echo (empty($BBC_NO_STRING) ? "\n<!-- BBClone v $BBC_VERSION OK: access file is working -->\n" : "");
                }
                else echo err_msg(_error);
                flock($al, LOCK_UN);
              }
              fclose($al);
            }
            else echo err_msg($BBC_CACHE_PATH.".htalock", "write");
          }
          else echo err_msg($BBC_CACHE_PATH.".htalock");
        }
      }
      else err_msg($BBC_LOG_PROCESSOR);
    }
    else err_msg($BBC_ACCESS_FILE, "write");
  }
  else err_msg($BBC_ACCESS_FILE);
}

ignore_user_abort(0);
?>