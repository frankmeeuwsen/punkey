<?php

// This file is part of BBClone (The PHP web counter on steroids)

// $Header: /cvs/bbclone-0.3x/conf/config.php,v 1.31 2004/02/15 19:39:12 joku Exp $
 
// Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
// distributed with this library)

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or   
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

///////////////////////////
// BBClone Configuration //
///////////////////////////

// The URL of the main site of which you record this access stats
// If empty, this URL doesn't appear in the navigation bar of all bbclone pages.
// Example: $BBC_MAINSITE = "http://www.mywebhost.com/somewhere/";
$BBC_MAINSITE = "";

// Determines whether the data of show_config.php should be displayed or not
$BBC_SHOW_CONFIG = 0; 

// The title being displayed in the navigation bar of all bbclone pages.
// The following macros are recognized:
//       %SERVER: server name
//       %DATE: the current date
// Html tags are also allowed.
$BBC_TITLEBAR = "Statistics for %SERVER generated the %DATE";

// The language you want to use. The default is english.
// To learn about available languages, you may take a look at the download
// section of the BBClone website (http://bbclone.de/).
$BBC_LANGUAGE = "nl";

// Period of time (in seconds) between 2 different visits from the same IP/AGENT
// combination. Default the emerging standard: 1800 s
$BBC_MAXTIME = 1800;

// How many entries do you want to see in the detailed stats?
// Default is 100
// do not set it higher than 500 (which is useless, anyway)
$BBC_MAXVISIBLE = 100;

// How many OS do you want to see specified?
// Do not set it negative! Of course...
$BBC_MAXOS = 10;

// How many browsers do you want to see specified in the global stats?
// Do not set it negative! Of course...
$BBC_MAXBROWSER = 10;

// How many extensions do you want to see specified in the global stats?
// Do not set it negative! Of course...
$BBC_MAXEXTENSION = 10;

// How many robots do you want to see specified in the global stats?
// Do not set it negative! Of course...
$BBC_MAXROBOT = 10;

// How many pages do you want to see specified in the global stats?
$BBC_MAXPAGE = 10;

// How many origins do you want to see specified in the global stats?
$BBC_MAXORIGIN = 10;

// This option can be used to prevent ip's or ranges from being noticed of
// bbclone. This is useful if you don't want to count the hits of your local LAN
// or IP (if it's static). possible notations are:
// 127.             => ignore 127.0.0.1 - 127.255.255.254
// 192.168.         => ignore all addresses from 192.168.0.1 - 192.168.0.254
// 192.168.5., 192.168.7.77 => ignore all addresses from 192.168.5.1 - 
//                             192.168.5.254 and 192.168.7.77
// If you list more than one expression use a comma as separator and the
// following format:
// $BBC_IGNOREIP = "127., 192.168.";
$BBC_IGNOREIP = "127.0.0.1";

// If you run a couple of *remote* sites and don't want them to be listed in
// your referrer ranking, you can add the hostname(s) here. The referrer will be
// treated as "not specified" and no hits are lost. Use the following format:
//
// $BBC_IGNORE_REFER = "www.host1.org, another.host2.org, yetanother.host3.org";
//
// and so on. Only hostnames are accepted to make sure we can exclude those with
// dynamically changing IP addresses as well. If you want to ignore rererrers
// from your *local* server (= on which bbclone is running) please use the next
// option.
$BBC_IGNORE_REFER = "";

// If this flag is set, all referrers from your server (on which bbclone is
// running) will be displayed as http://www.myserver.com/ (placeholder for your
// server name) in the referrer ranking, regardless from the original referrer.
// This is useful if you don't want bbclone to list paths to administrative
// pages, protected directories or other stuff you want to keep for yourself. If
// you leave this option empty it will make all your local referrers from the
// server completely suppressed. It does *not* turn off filterring one's own
// referrers for security reasons.
$BBC_OWN_REFER = 1;

// BBClone writes a comment to the html source as indicator for its current
// state. However this output, though convenient, may interfere with some forums
// or content management systems. This is most likely whenever they perform a
// redirect or set a cookie after the inclusion of BBClone. If you're confronted
// with a blank page or a couple of "header already sent by" messages you need
// to uncomment this flag to make your scripts work again.
// $BBC_NO_STRING = 1;

// The variable $BBC_DETAILED_STAT_FIELDS determines the columns to display in
// show_detailed.php. Possible column names are:
// id, time, visits, dns (hostname), ip, referer, os, browser, ext (extension)
// and script_name
// The order of the column names is important.
// Non-existing columns will be missing on the statistics page.
// Examples:
// $BBC_DETAILED_STAT_FIELDS = "id, time, visits, ip, ext, os, browser";
// $BBC_DETAILED_STAT_FIELDS = "date, ext, browser, os, script_name";
$BBC_DETAILED_STAT_FIELDS = "id, time, visits, ext, dns, referer, os, browser";

// Here you can set the main stats page's align style.
// Possible values are left, right, center
$BBC_GENERAL_ALIGN_STYLE = "center";

// These variables influence the font size of text and titles
// Size is defined by an integer between 0 (smallest) and 6 (largest)
// Previous versions used the following settings:
// $BBC_TITLE_SIZE    = 5;
// $BBC_SUBTITLE_SIZE = 3;
// $BBC_TEXT_SIZE     = 2;
$BBC_TITLE_SIZE    = 4;
$BBC_SUBTITLE_SIZE = 2;
$BBC_TEXT_SIZE     = 1;

?>