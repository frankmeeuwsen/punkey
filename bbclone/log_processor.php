<?php
/* File: log_processor.php
 * Summary: Process the new connection
 * 
 * Description: Process the new connection and sort them in various files
 * 
 * License:
 * 
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/log_processor.php,v 1.50 2004/02/15 19:39:12 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

// Checking if BBCLONE_DIR is set
if (!defined("_BBCLONE_DIR")) return;

for ($i = 0, $j = array("io", "browser", "os", "robot"), $k = count($j); $i < $k; $i++) {
  if (is_readable($BBC_LIB_PATH.$j[$i].".php")) require_once($BBC_LIB_PATH.$j[$i].".php");
  else {
    if (!defined("_error")) define("_error", $BBC_LIB_PATH.$j[$i].".php");
    return;
  }
}

// used by usort()
function sort_time_sc($row_a, $row_b) {
  if ($row_a["time"] == $row_b["time"]) return 0;
  return ($row_a["time"] > $row_b["time"] ) ? 1 : -1;
}

// Check if an IP is blacklisted in the config file
function is_ignored_ip($ip) {
  global $BBC_IGNOREIP;

  $ignoreip = (empty($BBC_IGNOREIP) ? "" : explode(",", $BBC_IGNOREIP));

  if (empty($ignoreip)) return false;
  for($k = count($ignoreip) - 1; $k >= 0; $k--) {
    $match = trim($ignoreip[$k]);
    if (substr($ip, 0, strlen($match)) === $match) return true;
  }
  return false;
}

function process_new_connection($connect) {
  global $browser, $os, $robot, $access;

  static $ext_array = array(
    "numeric", "com", "net", "edu", "biz", "info", "jp", "us", "uk", "de", "mil", "ca", "it", "au", "org", "nl", "fr",
    "tw", "gov", "fi", "br", "se", "es", "no", "mx", "kr", "ch", "dk", "be", "at", "nz", "ru", "pl", "za", "unknown",
    "ar", "il", "sg", "arpa", "cz", "hu", "hk", "pt", "tr", "gr", "cn", "ie", "my", "th", "cl", "co", "is", "uy", "ee",
    "in", "ua", "sk", "ro", "ae", "id", "su", "si", "hr", "ph", "lv", "ve", "bg", "lt", "yu", "lu", "nu", "pe", "cr",
    "int", "do", "cy", "pk", "cc", "tt", "eg", "lb", "kw", "to", "kz", "na", "mu", "bm", "sa", "zw", "kg", "cx", "pa",
    "gt", "bw", "mk", "gl", "ec", "lk", "md", "py", "bo", "bn", "mt", "fo", "ac", "pr", "am", "pf", "ge", "bh", "ni",
    "by", "sv", "ma", "ke", "ad", "zm", "np", "bt", "sz", "ba", "om", "jo", "ir", "st", "vi", "ci", "jm", "li", "ky",
    "gp", "mg", "gi", "sm", "as", "tz", "ws", "tm", "mc", "sn", "hm", "fm", "fj", "cu", "rw", "mq", "ai", "pg", "bz",
    "sh", "aw", "mv", "nc", "ag", "uz", "tj", "sb", "bf", "kh", "tc", "tf", "az", "dm", "mz", "mo", "vu", "mn", "ug",
    "tg", "ms", "ne", "gf", "gu", "hn", "al", "gh", "nf", "io", "gs", "ye", "an", "aq", "tn", "ck", "ls", "et", "ng",
    "sl", "bb", "je", "vg", "vn", "mr", "gy", "ml", "ki", "tv", "dj", "km", "dz", "im", "pn", "qa", "gg", "bj", "ga",
    "gb", "bs", "va", "lc", "cd", "gm", "mp", "gw", "cm", "ao", "er", "ly", "cf", "mm", "td", "iq", "kn", "sc", "cg",
    "gd", "nr", "af", "cv", "mh", "pm", "so", "vc", "bd", "gn", "ht", "la", "lr", "mw", "pw", "re", "tk", "bi", "bv",
    "fk", "gq", "sd", "sj", "sr", "sy", "tp", "um", "wf", "yt", "zr"
  );

  // Giving a an identification number to the new connection
  if (isset($access["stat"]["totalcount"])) $connect["id"] = $access["stat"]["totalcount"] + 1;
  else $connect["id"] = 1;

  $connect["visits"] = 1;

  // Detecting the extension
  $tmp = explode(".", $connect["dns"]);
  $connect_ext = strtolower($tmp[(count($tmp) - 1)]);

  if (ereg("^[0-9]+$", $connect_ext)) $connect["ext"] = "numeric";
  elseif (!in_array($connect_ext, $ext_array)) $connect["ext"] = "unknown";
  else $connect["ext"] = $connect_ext;

  // Recording the detected extension in the global statistics
  if (isset($access["stat"]["ext"][$connect["ext"]])) $access["stat"]["ext"][$connect["ext"]]++;
  else $access["stat"]["ext"][$connect["ext"]] = 1;

  // Detecting robots, browsers and os
  for ($i = 0, $j = array("robot", "browser", "os"), $max = count($j); $i < $max; $i++) {
    foreach (${$j[$i]} as ${$j[$i]."_name"} => ${$j[$i]."_elem"}) {
      foreach (${$j[$i]."_elem"}["rule"] as $pattern => $note) {
        if (eregi($pattern, $connect["agent"], $regs)) {
          $connect[($j[$i])] = ${$j[$i]."_name"};
          if (ereg("\\\\[0-9]{1}" ,$note)) {
            $str = ereg_replace("\\\\([0-9]{1})", "\$regs[\\1]", $note);
            eval("\$str = \"$str\";");
            $connect[($j[$i])."_note"] = $str;
          }
          // Recording the detected robot in the global statistics
          if (isset($access["stat"][($j[$i])][${$j[$i]."_name"}])) $access["stat"][($j[$i])][${$j[$i]."_name"}]++;
          else $access["stat"][($j[$i])][${$j[$i]."_name"}] = 1;
          break 2;
        }
      }
    }
    if (!empty($connect["robot"])) break;
  }
  return $connect;
}

// Update the time stats into $access["time"]
function update_time_stat($new_time) {
  global $access;

  // Independent variables
  static $nb_seconds_in_day  = 86400;
  static $nb_seconds_in_week = 604800;

  // Variables for the new time
  $new_hour   = date("G", $new_time);
  $new_wday   = date("w", $new_time);
  $new_day    = date("j", $new_time) - 1;
  $new_month  = date("n", $new_time) - 1;

  // Variables for the last time
  $last_time = !empty($access["time"]["last"]) ? $access["time"]["last"] : 0;
  $last_hour  = date("G", $last_time);
  $last_wday  = date("w", $last_time);
  $last_day   = date("j", $last_time) - 1;
  $last_month = date("n", $last_time) - 1;
  $last_lgmonth = date("t", $last_time);

  $nb_seconds_in_last_month = $last_lgmonth * $nb_seconds_in_day;
  $nb_seconds_in_last_year = (date("L", $last_time) ? 366 : 365) * $nb_seconds_in_day;

  // Updating the last connection time to the new one 
  // for the next call of update_time_stat
  $access["time"]["last"] = $new_time;


  // Updating the hourly time stats (in a day)
  // Setting to zero the time-counters present between the last connections
  // and the new one, and incrementing the new time-counter
  // Last day, there are 24 hours = 86400 seconds
  if (($new_time - $last_time) > $nb_seconds_in_day) {
    for ($k = 0; $k < 24; $k++) $access["time"]["hour"][$k] = 0;
  }
  else {
    $elapsed = $new_hour - $last_hour;
    $elapsed = ($elapsed < 0) ? $elapsed + 24 : $elapsed;
    for ($k = 1; $k <= $elapsed; $k++) $access["time"]["hour"][($last_hour + $k) % 24] = 0;
  }
  $access["time"]["hour"][$new_hour]++;

  // Updating the daily time stats (in a week)
  // Last week, there are 7 days = 604800 seconds
  if (($new_time - $last_time) > $nb_seconds_in_week) {
    for ($k = 0; $k < 7; $k++) $access["time"]["wday"][$k] = 0;
  }
  else {
    $elapsed = $new_wday - $last_wday;
    $elapsed = ($elapsed < 0) ? $elapsed + 7 : $elapsed;
    for ($k = 1; $k <= $elapsed; $k++) $access["time"]["wday"][($last_wday + $k) % 7] = 0;
  }

  $access["time"]["wday"][$new_wday]++;

  // Updating the daily time stats (in a month)
  // Last month, there are 28, 29, 30 or 31 days
  // Note: we have to reset 31 days even if we are a month with less days
  if (($new_time - $last_time) > $nb_seconds_in_last_month) {
    for ($k = 0; $k < 31; $k++) $access["time"]["day"][$k] = 0;
  }
  else {
    $elapsed = $new_day - $last_day;
    $elapsed = ($elapsed < 0) ? $elapsed + $last_lgmonth : $elapsed;
    for ($k = 1; $k <= $elapsed; $k++) $access["time"]["day"][($last_day + $k) % $last_lgmonth] = 0;
  }

  $access["time"]["day"][$new_day]++;

  // Updating the monthly time stats (in a year)
  // Last year, there are 12 months 
  if (($new_time - $last_time) > $nb_seconds_in_last_year) {
    for ($k = 0; $k < 12; $k++) $access["time"]["month"][$k] = 0;
  }
  else {
    $elapsed = $new_month - $last_month;
    $elapsed = ($elapsed < 0) ? $elapsed + 12 : $elapsed;
    for ($k = 1; $k <= $elapsed; $k++) $access["time"]["month"][($last_month + $k) % 12] = 0;
  }
  $access["time"]["month"][$new_month]++;
}

// referer stats
function update_referer_stat($referer) {
  global $access;

  $referer_light = trim(substr(strstr($referer, "://"), 3));

  if (!empty($referer_light)) {
    $referer_light = str_replace("\n", "", $referer_light);

    if (($qm = strpos($referer_light, "?")) !== false) $referer_light = substr($referer_light, 0, $qm);
    if ((($parent = dirname($referer_light)) == ".") || ($parent === false)) {
      $referer_light = ((strlen($referer_light) - 1) === strrpos($referer_light, "/")) ? $referer_light :
                       $referer_light."/";
    }
    else $referer_light = $parent."/";

    // compare whether we got a "www.*" equivalent recorded (or missing)
    $prefix = substr($referer_light, 0, ($tmp = strpos($referer_light, ".")));
    $suffix = substr($referer_light, ++$tmp);
    $result = ($prefix != "www") ? "www.".$referer_light : $suffix;
    // patterns for old records without trailing slashes
    $ref_no_slash = substr($referer_light, 0, (strlen($referer_light) - 1));
    $res_no_slash = substr($result, 0, (strlen($result) - 1));

    // neither recorded with "www." nor without, seems to be our 1st visit ;)
    if ((!isset($access["referer"][$referer_light])) && (!isset($access["referer"][$result]))) {
      $access["referer"][$referer_light] = 1;
    }
    // if there are old records without trailing slashes we will add them to our score too...
    elseif ((!isset($access["referer"][$referer_light])) && (isset($access["referer"][$result]))) {
      if (isset($access["referer"][$res_no_slash])) {
        $access["referer"][$result] += $access["referer"][$res_no_slash];
        unset($access["referer"][$res_no_slash]);
      }
      $access["referer"][$result]++;
    }
    // same here...
    elseif ((isset($access["referer"][$referer_light])) && (!isset($access["referer"][$result]))) {
      if (isset($access["referer"][$ref_no_slash])) {
        $access["referer"][$referer_light] += $access["referer"][$ref_no_slash];
        unset($access["referer"][$ref_no_slash]);
      }
      $access["referer"][$referer_light]++;
    }
    // Now we got both of them, let's continue with the one we got most of
    else {
      if ($access["referer"][$referer_light] < $access["referer"][$result]) {
        $access["referer"][$result] += $access["referer"][$referer_light];
        unset($access["referer"][$referer_light]);
        $access["referer"][$result]++;
      }
      else {
        $access["referer"][$referer_light] += $access["referer"][$result];
        unset($access["referer"][$result]);
        $access["referer"][$referer_light]++;
      }
    }
  }
  else {
    if (!isset($access["referer"]["not_specified"])) $access["referer"]["not_specified"] = 1;
    else $access["referer"]["not_specified"]++;
  }
}

function update_page_stat($script, $script_name) {
  global $access;

  $raw = $GLOBALS["HTTP_SERVER_VARS"]["SCRIPT_NAME"];

  // cleanup of old main entrance
  if ($script == "/") {
    if (isset($access["page"][""])) {
      if (isset($access["page"]["/"])) $access["page"]["/"]["count"] += $access["page"][""]["count"];
      else $access["page"]["/"]["count"] = $access["page"][""]["count"];
      unset($access["page"][""]);
    }
    if (isset($access["page"]["/"])) {
      if (isset($access["page"][""])) {
        $access["page"]["/"]["count"] += $access["page"][""]["count"];
        unset($access["page"][""]);
      }
      if ($script_name != "/") {
        if (isset($access["page"][$script_name])) {
          $access["page"][$script_name]["count"] += $access["page"]["/"]["count"];
        }
        else $access["page"][$script_name]["count"] = $access["page"]["/"]["count"];
        unset($access["page"]["/"]);
      }
    }
  }
  // removal of unfilterred URIs as page title
  if (isset($access["page"][$raw])) {
    // recycle useful marker functions without initialising objects
    $new_script_name = marker::filter_uri($raw);
    $new_script_name = marker::auto_page_name($new_script_name);

    if (isset($access["page"][$new_script_name])) {
      $access["page"][$new_script_name]["count"] += $access["page"][$raw]["count"];
    }
    else {
      $access["page"][$new_script_name]["count"] = $access["page"][$raw]["count"];
      $access["page"][$new_script_name]["uri"] = $script;
    }
    unset($access["page"][$raw]);
  }

  if (!isset($access["page"][$script_name]["count"])) $access["page"][$script_name]["count"] = 0;
  $access["page"][$script_name]["count"]++;
  $access["page"][$script_name]["uri"] = $script;
}

// Transfer the raw data from the main counters of var into $access["last"].
// Any new data (more recent than $BBC_MAXTIME) is used in the global stats
function add_new_connections_to_old() {
  global $BBC_MAXTIME, $access;

  // Resetting the counter of new connections
  $access["stat"]["newvisits"] = 0;
  $access["stat"]["newcount"] = 0;

  // Loading new connections if any
  if (!$new_access = counter_to_array()) return false;

  $nb_new_access = (!empty($new_access) ? count($new_access) : 0);
  $nb_last_access = (!empty($access["last"]) ? count($access["last"]) : 0);

  // Checking whether we have new connections
  for($k = 0; $k < $nb_new_access; $k++) {
    $connect = $new_access[$k];

    // Updating various basic counters:

    // The script viewed
    if ((isset($connect["script_name"])) &&  (!is_ignored_ip($connect["ip"]))) {
      update_page_stat($connect["script"], $connect["script_name"]);
    }

    // The blacklisting
    if (is_ignored_ip($connect["ip"])) {
      if (!isset($access["stat"]["blacklisted"])) $access["stat"]["blacklisted"] = 0;

      $access["stat"]["blacklisted"]++;
      continue;
    }
    $connect_time = $connect["time"];

    // The time stats
    update_time_stat($connect["time"]);

    // Check if a similar connection has been recorded yet
    $prev_recorded = 0;
    for($l = $nb_last_access - 1; 
       ($l >= 0) && (($connect_time - $access["last"][$l]["time"]) < $BBC_MAXTIME); $l--) {
      if ($connect["ip"] == $access["last"][$l]["ip"]) {
        $access["last"][$l]["time"] = $connect_time;
        $access["last"][$l]["visits"]++;
        $access["last"][$l]["script"] = $connect["script"];
        $access["last"][$l]["script_name"] = $connect["script_name"];
        $access["stat"]["totalvisits"]++;
        $access["stat"]["newvisits"]++;
        $prev_recorded = 1;
        break;
      }
    }

    // If not recorded, this new connection becomes a new element 
    // of $access["last"] at the end of this latter.
    // The time order is preserved because $access["last"] is sorted by increasing time.
    if (!$prev_recorded) {
      if (empty($access["stat"]["totalvisits"])) $access["stat"]["totalvisits"] = 0;
      if (empty($access["stat"]["totalcount"])) $access["stat"]["totalcount"] = 0;

      $access["last"][$nb_last_access] = process_new_connection($connect);
      $access["stat"]["newvisits"]++;
      $access["stat"]["newcount"]++;
      $access["stat"]["totalvisits"]++;
      $access["stat"]["totalcount"]++;
      $nb_last_access++;

      // The referer list is update according to this new visit
      update_referer_stat($connect["referer"]);
    }
  }
  return $nb_new_access;
}

// Removed unnecessary connections from $access["last"], namely those after the $BBC_MAXVISBLE more recent 
// and older than time() - $BBC_MAXTIME.
function update_last_access() {
  global $access, $BBC_MAXTIME, $BBC_MAXVISIBLE;

  $ctime = time();
  if ((empty($access["last"])) || (!is_array($access["last"]))) return;
  else {
    $nb_connect = count($access["last"]);
    for ($k = $nb_connect - 1 - $BBC_MAXVISIBLE; $k >= 0; $k--) {
      if (($ctime - $access["last"][$k]["time"]) > $BBC_MAXTIME) unset($access["last"][$k]);
    }
    usort($access["last"],"sort_time_sc");
  }
}
?>