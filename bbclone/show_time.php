<?php
/* File:  show_time.php
 * Summary: Display the time stats of bbclone
 * 
 * Description:
 * 
 * License:
 * 
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/show_time.php,v 1.34 2004/01/18 11:18:02 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

if (is_readable("constants.php")) require_once("constants.php");
else return;
$j = array($BBC_CONFIG_FILE, $BBC_LIB_PATH."selectlang.php", $BBC_LIB_PATH."html.php", $BBC_ACCESS_FILE);
for ($i = 0, $k = count($j); $i < $k; $i++) {
  if (is_readable($j[$i])) require_once($j[$i]);
  else return err_msg($j[$i]);
}

// Functions

function show_plot_time_type($time_type, $width, $height) {
  global $access, $_;

  $last_time = isset($access["time"]["last"]) ? $access["time"]["last"] : 0;
  $current_time = time();
  $nb_seconds_in_day  = 86400;
  $nb_seconds_in_week = 7 * $nb_seconds_in_day;
  $last_month = date("n", $last_time) - 1;
  $nb_seconds_in_last_year = (date("L", $last_time) ? 366 : 365) * $nb_seconds_in_day;

  switch ($time_type) {
    case "hour":
      $current_hour = date("G", $current_time);
      $last_hour    = date("G", $last_time);

      for ($k = $current_hour - 23; $k <= $current_hour; $x[] = ($k < 0) ? $k + 24 : $k, $k++);
      for ($k = 0; $k < 24; $y[$k] = 0, $k++);
      if (($current_time - $last_time) <= $nb_seconds_in_day) {
        $elapsed = $current_hour - $last_hour;
        $elapsed = ($elapsed < 0) ? $elapsed + 24 : $elapsed;

        for ($k = $elapsed; $k < 24; $k++) {
          $y[$k - $elapsed] = $access["time"]["hour"][($last_hour + 1 + $k) % 24];
        }
      }
      break;

    case "wday":
      $day_name = array($_["tstat_Su"], $_["tstat_Mo"], $_["tstat_Tu"],
                        $_["tstat_We"], $_["tstat_Th"], $_["tstat_Fr"],
                        $_["tstat_Sa"]);

      $current_wday = date("w", $current_time);
      $last_wday    = date("w", $last_time);

      for ($k = $current_wday - 6; $k <= $current_wday; 
        $x[] = $day_name[($k < 0) ? $k + 7 : $k], $k++);
      for ($k = 0; $k < 7; $y[$k] = 0, $k++);
      if (($current_time - $last_time) <= $nb_seconds_in_week) {
        $elapsed = $current_wday - $last_wday;
        $elapsed = ($elapsed < 0) ? $elapsed + 7 : $elapsed;

        for ($k = $elapsed; $k < 7; $k++) {
          $y[$k - $elapsed] = $access["time"]["wday"][($last_wday + 1 + $k) % 7];
        }
      }
      break;

    case "day":
      // We suppose that the first day of the month is 0 for array compatibility
      $current_day    = date("j", $current_time) - 1;
      $last_day       = date("j", $last_time) - 1;
      $time_in_prec_month = $current_time - ($current_day + 1) * $nb_seconds_in_day;
      $lg_prec_month  = date("t", $time_in_prec_month);
      $lg_prec_month  = ($current_day >= $lg_prec_month ? $current_day + 1 : $lg_prec_month);
      $current_month  = date("n", $current_time);
      $prec_month     = date("n", $time_in_prec_month);

      // Computing the $x
      for ($k = $current_day + 1; $k < $lg_prec_month; $x[] = ($k + 1), $k++);
      for ($k = 0; $k <= $current_day; $x[] = ($k + 1), $k++); 
      // Computing the $y
      for ($k = 0; $k < 31; $y[$k] = 0, $k++);
      if (($current_time - $last_time) <= ($lg_prec_month * $nb_seconds_in_day)) {
        $elapsed = $current_day - $last_day;
        $elapsed = ($elapsed < 0) ? $elapsed + $lg_prec_month : $elapsed;

        for ($k = $elapsed; $k < $lg_prec_month; $k++) {
          $y[$k - $elapsed] = $access["time"]["day"][($last_day + 1 + $k) % $lg_prec_month];
        }
      }
      break;

    case "month":
      $month_name = array($_["tstat_Jan"], $_["tstat_Feb"], $_["tstat_Mar"],
                          $_["tstat_Apr"], $_["tstat_May"], $_["tstat_Jun"],
                          $_["tstat_Jul"], $_["tstat_Aug"], $_["tstat_Sep"],
                          $_["tstat_Oct"], $_["tstat_Nov"], $_["tstat_Dec"]);

      $current_month = date("n", $current_time) - 1;
      $last_month    = date("n", $last_time) - 1;

      for($k = $current_month - 11; $k <= $current_month; 
        $x[] = $month_name[($k < 0) ? $k + 12 : $k], $k++);
      for($k = 0; $k < 12; $y[$k] = 0, $k++);
      if (($current_time - $last_time) <= $nb_seconds_in_last_year) {
        $elapsed = $current_month - $last_month;
        $elapsed = ($elapsed < 0) ? $elapsed + 12 : $elapsed;
        for ($k = $elapsed; $k < 12; $k++) {
          $y[$k - $elapsed] = $access["time"]["month"][($last_month + 1 + $k) % 12];
        }
      }
      break;
  }

  return bbc_plot($x, $y, $width, $height);
}

// MAIN

echo bbc_html_document_begin()
    .bbc_topbar()

    ."<div align=\"$BBC_GENERAL_ALIGN_STYLE\">\n"
    ."<table border=\"0\" cellpadding=\"20\" cellspacing=\"0\">\n"
    ."<tr>\n"
    ."<td valign=\"top\" align=\"center\">\n"
    ."<b>". $_["tstat_Last_day"]."</b><br />\n"
    ."<br />\n"
    .show_plot_time_type("hour", 375, 200)
    ."</td>\n"
    ."<td valign=\"top\" align=\"center\">\n"
    ."<b>". $_["tstat_Last_week"]."</b><br />\n"
    ."<br />\n"
    .show_plot_time_type("wday", 375, 200)
    ."</td>\n"
    ."</tr>\n"
    ."<tr>\n"
    ."<td valign=\"top\" align=\"center\">\n"
    ."<b>".$_["tstat_Last_month"]."</b><br />\n"
    ."<br />\n"
    .show_plot_time_type("day", 375, 200)
    ."</td>\n"
    ."<td valign=\"top\" align=\"center\">\n"
    ."<b>".$_["tstat_Last_year"]."</b><br />\n"
    ."<br />\n"
    .show_plot_time_type("month", 375, 200)
    ."</td>\n"
    ."</tr>\n"
    ."</table>\n"
    ."</div>\n"

    .bbc_copyright()
    ."<br />\n"
    .bbc_topbar(0, 1)
    .bbc_html_document_end();
?>