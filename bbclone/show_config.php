<?php
/* File:  show_config.php
 * Summary: Display the configuration of bbclone
 * 
 * Description:
 * 
 * License:
 * 
 * This file is part of BBClone (The PHP web counter on steroids)
 *
 * $Header: /cvs/bbclone-0.3x/show_config.php,v 1.37 2004/01/18 14:54:32 joku Exp $
 * 
 * Copyright (C) 2001-2004, the BBClone Team (see the file AUTHORS 
 * distributed with this library)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or   
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

if (is_readable("constants.php")) require_once("constants.php");
else return;
$j = array($BBC_CONFIG_FILE, $BBC_LIB_PATH."selectlang.php", $BBC_LIB_PATH."html.php");
for ($i = 0, $k = count($j); $i < $k; $i++) {
  if (is_readable($j[$i])) require_once($j[$i]);
  else return err_msg($j[$i]);
}

// Determine if the user want to see or no the configuration through show_conf.php
if (empty($BBC_SHOW_CONFIG)) die($_["error_cannot_see_config"]);

$server = !empty($HTTP_SERVER_VARS["SERVER_NAME"]) ? htmlentities($HTTP_SERVER_VARS["SERVER_NAME"], ENT_QUOTES) :
          "no name";
$title = str_replace("%DATE", date($_["global_date_format"]), str_replace("%SERVER", $server, $BBC_TITLEBAR));

// MAIN

echo bbc_html_document_begin()
    .bbc_topbar()

    ."<br />\n"
    ."<div align=\"$BBC_GENERAL_ALIGN_STYLE\">\n"
    ."<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" width=\"760\">\n"
// TABLE TITLES
    ."<tr bgcolor=\"#d1daf4\">\n"
    ."<td valign=\"top\" width=\"20%\">\n"
    ."<b>". $_["config_Variable_name"]."</b>\n"
    ."</td>\n"
    ."<td valign=\"top\" width=\"60%\">\n"
    ."<b>".$_["config_Explanations"]."</b>\n"
    ."</td>\n"
    ."<td valign=\"top\" width=\"20%\">\n"
    ."<b>".$_["config_Variable_value"]."</b>\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAINSITE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAINSITE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_mainsite"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .(!empty($BBC_MAINSITE) ? "\"<a href=\"$BBC_MAINSITE\">$BBC_MAINSITE</a>\"\n" : $_["global_no"])."\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_SHOW_CONFIG
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_SHOW_CONFIG\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_show_config"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .(!empty($BBC_SHOW_CONFIG) ? $_["global_yes"] : $_["global_no"])."\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_TITLE_BAR
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_TITLEBAR\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_titlebar"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."\"$BBC_TITLEBAR\"<br />-&gt;<br /> \"$title\"\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_LANGUAGE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_LANGUAGE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_language"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."\"$BBC_LANGUAGE\"\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXTIME
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXTIME\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxtime"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXTIME\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXVISIBLE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXVISIBLE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxvisible"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXVISIBLE\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXOS
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXOS\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxos"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXOS\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXBROWSER
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXBROWSER\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxbrowser"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXBROWSER\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXEXTENSION
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXEXTENSION\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxextension"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXEXTENSION\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXROBOT
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXROBOT\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxrobot"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXROBOT\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXPAGE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXPAGE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxpage"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXPAGE\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_MAXORIGIN
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_MAXORIGIN\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_maxorigin"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_MAXORIGIN\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_IGNOREIP
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_IGNOREIP\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_ignoreip"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .(!empty($BBC_IGNOREIP) ? "\"".$BBC_IGNOREIP."\"" : $_["global_no"])."\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_IGNORE_REFER
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_IGNORE_REFER\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_ignore_refer"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .(!empty($BBC_IGNORE_REFER) ? "\"".$BBC_IGNORE_REFER."\"" : $_["global_no"])."\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_OWN_REFER
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_OWN_REFER\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_own_refer"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .(!empty($BBC_OWN_REFER) ? $_["global_yes"] : $_["global_no"])."\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_NO_STRING
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_NO_STRING\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_no_string"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .(!empty($BBC_NO_STRING) ? $_["global_yes"] : $_["global_no"])."\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_DETAILED_STAT_FIELDS
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_DETAILED_STAT_FIELDS\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_detailed_stat_fields"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."\"$BBC_DETAILED_STAT_FIELDS\"\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_GENERAL_ALIGN_STYLE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_GENERAL_ALIGN_STYLE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_general_align_style"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."\"$BBC_GENERAL_ALIGN_STYLE\"\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_TITLE_SIZE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_TITLE_SIZE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_title_size"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_TITLE_SIZE\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_SUBTITLE_SIZE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_SUBTITLE_SIZE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_subtitle_size"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_SUBTITLE_SIZE\n"
    ."</td>\n"
    ."</tr>\n"
// $BBC_TEXT_SIZE
    ."<tr bgcolor=\"#e8edf9\">\n"
    ."<td valign=\"top\">\n"
    ."\$BBC_TEXT_SIZE\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    .$_["config_bbc_text_size"]."\n"
    ."</td>\n"
    ."<td valign=\"top\">\n"
    ."$BBC_TEXT_SIZE\n"
    ."</td>\n"
    ."</tr>\n"
    ."</table>\n"
    ."</div>\n"
    .bbc_copyright()
    ."<br />\n"
    .bbc_topbar(0, 1)
    .bbc_html_document_end();
?>