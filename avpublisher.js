// Flash Version Detector  v1.1.5
// http://www.dithered.com/javascript/flash_detect/index.html
// code by Chris Nott (chris@NOSPAMdithered.com - remove NOSPAM)
// with VBScript code from Alastair Hamilton

var flashVersion = 0;
function getFlashVersion() {
	var agent = navigator.userAgent.toLowerCase(); 

   // NS3 needs flashVersion to be a local variable
   if (agent.indexOf("mozilla/3") != -1 && agent.indexOf("msie") == -1) {
      flashVersion = 0;
   }

	// NS3+, Opera3+, IE5+ Mac (support plugin array):  check for Flash plugin in plugin array
	if (navigator.plugins != null && navigator.plugins.length > 0) {
		var flashPlugin = navigator.plugins['Shockwave Flash'];
		if (typeof flashPlugin == 'object') { 
			if (flashPlugin.description.indexOf('7.') != -1) flashVersion = 7;
			else if (flashPlugin.description.indexOf('6.') != -1) flashVersion = 6;
			else if (flashPlugin.description.indexOf('5.') != -1) flashVersion = 5;
			else if (flashPlugin.description.indexOf('4.') != -1) flashVersion = 4;
			else if (flashPlugin.description.indexOf('3.') != -1) flashVersion = 3;
		}
	}

	// IE4+ Win32:  attempt to create an ActiveX object using VBScript
	else if (agent.indexOf("msie") != -1 && parseInt(navigator.appVersion) >= 4 && agent.indexOf("win")!=-1 && agent.indexOf("16bit")==-1) {
	 	document.write('<scr' + 'ipt language="VBScript"\> \n');
		document.write('on error resume next \n');
		document.write('dim obFlash \n');
		document.write('set obFlash = CreateObject("ShockwaveFlash.ShockwaveFlash.7") \n');
		document.write('if IsObject(obFlash) then \n');
		document.write('flashVersion = 7 \n');
		document.write('else set obFlash = CreateObject("ShockwaveFlash.ShockwaveFlash.6") end if \n');
		document.write('if flashVersion < 7 and IsObject(obFlash) then \n');
		document.write('flashVersion = 6 \n');
		document.write('else set obFlash = CreateObject("ShockwaveFlash.ShockwaveFlash.5") end if \n');
		document.write('if flashVersion < 6 and IsObject(obFlash) then \n');
		document.write('flashVersion = 5 \n');
		document.write('else set obFlash = CreateObject("ShockwaveFlash.ShockwaveFlash.4") end if \n');
		document.write('if flashVersion < 5 and IsObject(obFlash) then \n');
		document.write('flashVersion = 4 \n');
		document.write('else set obFlash = CreateObject("ShockwaveFlash.ShockwaveFlash.3") end if \n');
		document.write('if flashVersion < 4 and IsObject(obFlash) then \n');
		document.write('flashVersion = 3 \n');
		document.write('end if');
		document.write('</scr' + 'ipt\> \n');
  }

	// WebTV 2.5 supports flash 3
	else if (agent.indexOf("webtv/2.5") != -1) flashVersion = 3;

	// older WebTV supports flash 2
	else if (agent.indexOf("webtv") != -1) flashVersion = 2;

	// Can't detect in all other cases
	else {
		flashVersion = flashVersion_DONTKNOW;
	}
	return flashVersion;
}

flashVersion_DONTKNOW = -1;

function setFlashWidth(divid, newW){
	//document.getElementById(divid).style.width = newW+"px";
}
function setFlashHeight(divid, newH){
	//alert(newH);
	//document.getElementById(divid).style.height = newH+"px";		
}
function setFlashSize(divid, newW, newH){
	//setFlashWidth(divid, newW);
	//setFlashHeight(divid, newH);
}

var temp = window.location.href;
filename = temp.substring(temp.lastIndexOf('/') + 1);
if(getFlashVersion() < 6){
	window.open('http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash');
} 

	function setSettings(AVPublisher_Settings){
		if(getFlashVersion() > 6){ 
			switch (AVPublisher_Settings["type"]) {
				case 'studio_video': if ((AVPublisher_Settings["width_url"]<220||AVPublisher_Settings["height_url"]<140)||(AVPublisher_Settings["width_url"]==undefined||AVPublisher_Settings["height_url"]==undefined)){AVPublisher_Settings["width_url"] = 220; AVPublisher_Settings["height_url"] = 140;} break;
				case 'player_video': if (AVPublisher_Settings["width_url"]<160||AVPublisher_Settings["height_url"]<120){AVPublisher_Settings["width_url"] = 160; AVPublisher_Settings["height_url"] = 120;} break;
				case 'studio_audio': if (AVPublisher_Settings["width_url"]<220||AVPublisher_Settings["height_url"]<140){AVPublisher_Settings["width_url"] = 220; AVPublisher_Settings["height_url"] = 140;} break;
				case 'player_audio': if (AVPublisher_Settings["width_url"]<160||AVPublisher_Settings["height_url"]<28){AVPublisher_Settings["width_url"] = 160; AVPublisher_Settings["height_url"] = 28;} break;
				default : setSettings('studio_video',AVPublisher_Settings["width_url"],AVPublisher_Settings["height_url"],AVPublisher_Settings["quality"],AVPublisher_Settings["maxRecordTime"],AVPublisher_Settings["domain"],AVPublisher_Settings["uniqueID"]); break;
			}

			AVPublisher_divheight = AVPublisher_Settings["height_url"] + 28;
			document.write('<div id="'+AVPublisher_Settings["uniqueID"]+'" style="width:'+AVPublisher_Settings["width_url"]+'px; height:'+AVPublisher_divheight+'px;padding:0;margin:0;border:1px solid #000;background-color:#000000;">');
			var PATH_TO_SWF = '../'+AVPublisher_Settings["type"]+'.swf?width_url='+AVPublisher_Settings["width_url"]+'&height_url='+AVPublisher_Settings["height_url"]+'&domain='+AVPublisher_Settings["domain"]+'&maxRecordTime='+AVPublisher_Settings["maxRecordTime"]+'&quality='+AVPublisher_Settings["quality"]+'&uniqueID='+AVPublisher_Settings["uniqueID"]+'&autoplay_url='+AVPublisher_Settings["autoplay"];
			document.write('<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash/cabs/swflash.cab#version=6,0,0,0" id="flash" width="'+AVPublisher_Settings["width_url"]+'" height="'+AVPublisher_divheight+'">');
			document.write('<param name="movie" value="'+PATH_TO_SWF+'" width="'+AVPublisher_Settings["width_url"]+'" height="'+AVPublisher_divheight+'"> ');
			document.write('<param name="quality" value="autohigh"> ');
			document.write('<PARAM NAME="scale" VALUE="noscale">');
			document.write('<PARAM NAME="wmode" VALUE="transparent">');
			document.write('<PARAM NAME="SALIGN" VALUE="LT">');
			document.write('<embed src="'+PATH_TO_SWF+'" name="flash" wmode="transparent" scale="noscale" swliveconnect="true" width="100%" height="100%" quality="autohigh" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" SALIGN="LT">');
			document.write('</embed>');
			document.write('</object>');
			document.write('</div>');
		}
	}

