<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------


define('__SILENT__', TRUE);
define('INPIVOT', TRUE);
define('LIVEPAGE', TRUE);


include_once("pv_core.php");
include_once("includes/getref.inc.php");
include_once("modules/module_userreg.php");

$live_output = TRUE;

if (!isset($Pivot_Vars['w'])) {
	$Pivot_Vars['w']="";
}

if (!isset($Pivot_Vars['c'])) {
	$Pivot_Vars['c']="";
}

if (!isset($Pivot_Vars['u'])) {
	$Pivot_Vars['u']="";
}

if (!isset($Pivot_Vars['t'])) {
	$Pivot_Vars['t']="";	
} else {
	$Pivot_Vars['t'] = basename($Pivot_Vars['t']);
}

$output = generate_live_page($Pivot_Vars['w'], $Pivot_Vars['c'], $Pivot_Vars['t'], $Pivot_Vars['u']);

echo $output;


// -- BBClone code --
//ob_start(); 
if ($Pivot_Vars['w']!="") {
	$title = $Pivot_Vars['w'];
} else if ($Pivot_Vars['c']!="") {
	$title = "Category ".$Pivot_Vars['c'];
} else if ($Pivot_Vars['u']!="") {
	$title = "User ". ($Pivot_Vars['c']!="");
} else {
	$title = snippet_weblogtitle();
} 
	
define("_BBC_PAGE_NAME", $title  );
define("_BBCLONE_DIR", "../bbclone/");
define("COUNTER", _BBCLONE_DIR."mark_page.php");
if (is_readable(COUNTER)) { include_once(COUNTER) ; }
//ob_end_clean();
//-- end BBClone code --



?>