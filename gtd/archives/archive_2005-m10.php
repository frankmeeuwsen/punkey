<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e6969"></span><div class="entry">
<h3>GTD Outlook add-in hotfix coming up!</h3>
	<p>Thanks to my reminder to check the productupdate-page from Netcentrics I just noticed that the <a href="http://www.punkey.com/pivot/entry.php?id=6952"  target='_blank'>GTD Outlook addin</a> will <a href="http://gtdsupport.netcentrics.com/learn/productupdates.php"  target='_blank'>get a hotfix update</a> tomorrow for a really annoying problem:<br />
<blockquote><br />
This hotfix will include the following fixes:
	<ul>
		<li>1979 � [Other] Update Task Action Error</li>
		<li>1980 &#8211; [Action] Paths in File and Save for Reference are truncated using &#8230; causing Read Restriction error</li>
		<li>2149 &#8211; [Other] CTRL+ALT+j doesn&#8217;t populate action with Projects when creating new Project</li>
		<li>2150 &#8211; [Action] Can&#8217;t Show Modal form when Non-Modal error</li>
		<li>2151 &#8211; [Installation] Outlook is still running and cannot install error even though Outlook is closed and not running in memory</li>
		<li>2152 &#8211; [Other] Default Outlook Reminder for Appointment is not used on Defer<br />
</blockquote><br />
Excellent! One of the most annoying problems right now is the truncating of folderpaths. It makes it terrible to file emails and when in a flow of getting In to Emty, it leaves me with dozens and dozens of unfiled mails in my @Action or @Waiting folder. Where they don&#8217;t belong. They need to be in shared folders on my office network. Now I hope one of the next updates will also have separate shortcut keys for Send, Send+Delegate and Send+File.</p>

  
 
<p class="info">30 10 05 - 21:59 - <a href="/pivot/entry.php?id=6969&amp;w=whats_the_next_action" title="30 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6969&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '6969'); ?></span></p>
</div><span id="e6968"></span><div class="entry">
<h3>Giving thanks to David Allen&#039;s cult time-management credo</h3>
	<p>Everyone already trying to get a &#8216;mind like water&#8217; should recognize this quote:<br />
<blockquote>Just four weeks ago, my desk was a shrine to disorder. It was cluttered with letters, takeaway menus, MiniDiscs and dead batteries, while stacks of printouts lay everywhere. A skyscraper of stuff towered in my inbox, untouched for months.</p>
	<p>Now it is pristine. My intray is empty. My desk is clear. A smile is on my lips and I am getting things done. All thanks to one book.<br />
</blockquote></p>
	<p>You can read the excellent article <a href="http://technology.guardian.co.uk/weekly/story/0,16376,1595595,00.html"  target='_blank'>Giving thanks to David Allen&#8217;s cult time-management credo</a> for more personal success with GTD</p>
	<p>(Thanks to <a href="http://www.fredscapes.nl"  target='_blank'>Fred Zelders</a> for the pointer)</p>

  
 
<p class="info">23 10 05 - 21:19 - <a href="/pivot/entry.php?id=6968&amp;w=whats_the_next_action" title="23 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6968&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6968'); ?></span></p>
</div><span id="e6967"></span><div class="entry">
<h3>Simple Outlook hack may save your day in the future</h3>
	<p>This morning, while going through some archived threads in the <a href="http://groups.google.com/group/43Folders"  target='_blank'>43Folders group</a>, I stumbled on this amazing Outlook hack in <a href="http://groups.google.com/group/43Folders/browse_thread/thread/9e3a3a251f5d58b6/1db138ac0ae9bffb#1db138ac0ae9bffb"  target='_blank'>this thread</a>, written by Simon. Don&#8217;t know if he has a blog or anything&#8230;<br />
<blockquote>&#8220;Ever get sent an e-mail with a blank or unintelligible subject line? Maybe you get CCed on a long ping pong e-mail that only reveals it&#8217;s relevance in the very last reply.<br />
Well, sometimes you need to file these away suckers for reference and this is where the trouble begins. What was top of your mind a week ago may now be completely opaque. So, when you browse through your mail folders, looking for that vital bit of information, it seems next to impossible to recall what all those e-mail exchanges were about? Where is it?<br />
Well there is an easy solution &#8211; <em>*change the subject line*</em> . Think of it as personal tagging. It is very easy to do.<br />
First, double click the e-mail (or hit enter) to open it in a separate window. Then put your cursor on the subject line and start typing. That&#8217;s it. You can add your own reference &#8211; date, project number, whatever &#8211; or change the subject line completely. Whatever is going to be meaningful to you. Use as many words you like.<br />
When you&#8217;re done, Ctrl+S to save. &#8220;</blockquote></p>
	<p>Now why didn&#8217;t I think of that myself? It is <em>so</em> easy! Especially since the searchfunction in Outlook is way too slow for me and for some reason desktop-search engines like Google or MSN slows my laptop in a considerable way. So most of the times I need to look by hand to find something fast. Using tagging or a more sensible subjectline can save the day! Thank you Simon for that excellent tip!</p>

  
 
<p class="info">22 10 05 - 09:30 - <a href="/pivot/entry.php?id=6967&amp;w=whats_the_next_action" title="22 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6967&amp;w=whats_the_next_action#comm" title="Jeroen, Frank Meeuwsen, Bob Walsh, Rob Venstra, Parker, Mo, rich, hari, cojack, Nelson Garcia, arvind">eleven comments</a> <?php echo get_editentrylink("Edit", '6967'); ?></span></p>
</div><span id="e6966"></span><div class="entry">
<h3>The difference between GTD at home and at work</h3>
	<p><em>*Preface:*</em> This article was intended as a question for the <a href="http://finance.groups.yahoo.com/group/Getting_Things_Done/"  target='_blank'>GTDgroup@Yahoo</a>. But it turned out to be some sort of therapeutic writing session for my own self. I decided to share it with you. Maybe it can be of any help, maybe you have tips, pointers or similar experiences. Please, feel free to respond!</p>
	<p>This evening, I was flipping some pages in <a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&#38;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Ready for anything</a>  when one sentence hit me. It&#8217;s on page 27:<br />
<em>*Trust that you&#8217;ll see the option again whenever you need to reassess*</em><br />
The chapter talks about putting stuff into In and review it on the Weekly Review.<br />
Now, what hit me was this&#8230;</p>

  
<a href="/pivot/entry.php?id=6966&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">19 10 05 - 23:54 - <a href="/pivot/entry.php?id=6966&amp;w=whats_the_next_action" title="19 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6966&amp;w=whats_the_next_action#comm" title="Michael Koehler, Andrew Beacock, Michael Langford, Tana, Robert, The Barrister, Solomon Folks, Frank Meeuwsen, Alan Shutko">nine comments</a> <?php echo get_editentrylink("Edit", '6966'); ?></span></p>
</div><span id="e6965"></span><div class="entry">
<h3>8 Steps to a succesfull and empty Inbox after your vacation</h3>
	<p><img src="/images/full-inbox.jpg" style="float:right;margin-left:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />As I <a href="http://www.punkey.com/pivot/entry.php?id=6962"  target='_blank'>wrote earlier</a>, I enjoyed a two-week vacation. This morning, back at work, I had a full Inbox waiting for me. <em>*912 new emails*</em> to be exact. And I left with an empty Inbox. I had a meeting outside the office today so I could only work for 3-4 hours on my Inbox today. But I have it pretty much cleaned out. Not back to zero <em>yet</em> but that will happen tomorrow! These are the steps I took. I use Outlook as my Emailclient. If you use any other client, I think you can use most of it as well.</p>
	<ol>
		<li> <strong>Don&#8217;t</strong> disable the Out Of Office assistant. This will give you some more breathingspace and an overall relaxed feeling that you&#8217;re still not <em>really</em> in the office yet;</li>
		<li>Order your emails on <em>*Sender*</em>. Start with &#8220;A&#8221; and just go down the list and delete all emails from sender&#8217;s you don&#8217;t know. This is basically to weed out the spam. Plus you&#8217;ll see some names pop up a lot more (for instance a client waiting on something) so you&#8217;ll already know you&#8217;re up for some discussion;</li>
		<li>Order them on <em>*Date*</em> and delete old newsletters and mailinglist-digests etc.;</li>
		<li>Now you can order your Inbox on <em>*Subject*</em> and decide if you can delete some discussionthreads that are going on with you in the (B)CC-list. I deleted a lot of those because most of the time, the whole discussion is repeated under the answers. So you can view the whole thread within one email;</li>
		<li>I use the &#8220;order on Subject&#8221; to answer some emails that require a <em>*short reply*</em>. Nothing that requires some real hard thinking, but just some basic replies like &#8220;I didn&#8217;t get the fax&#8221; or &#8220;yes, I will be there at 11&#8221;;</li>
		<li>Now, order your Inbox on <em>*Messagetype*</em>. This will give you all your outstanding meetingrequests in one list. You can do them all at once, but more important, you&#8217;ll see updated meetingrequests in one view. So you will (again) only have to answer to the latest version and delete older versions;</li>
		<li>After that step, you will probably have the more important messages in your Inbox. The ones that need more attention, focused reading or well&#8230;a <em>*Next Action*</em>. So decide what that NA is and act accordingly;</li>
		<li>Now turn off your Out of Office assistant and grab some coffee. You&#8217;ve earned it!</li>
	</ol>
	<p>I hope this will help you get rid of a the first clutter of emails after you&#8217;ve been away for some time. If you have any additions, a better order, or your own system, please let me know in the comments. I am always looking for ways to tweak these little lifehackers!</p>

  
 
<p class="info">17 10 05 - 22:26 - <a href="/pivot/entry.php?id=6965&amp;w=whats_the_next_action" title="17 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6965&amp;w=whats_the_next_action#comm" title="Robert Banghart, Frank Meeuwsen, Rob Venstra, Rob Evans, Andy Bryant, Frank, Itto, Damir Tomicic">eight comments</a> <?php echo get_editentrylink("Edit", '6965'); ?></span></p>
</div><span id="e6964"></span><div class="entry">
<h3>Quote on what GTD &quot;is&quot;</h3>
	<p>From the <a href="http://groups.googlegroups.com/group/43Folders"  target='_blank'>43Folders-discussiongroup</a>: </p>
	<p><em>*GTD is about habits, not software. Don&#8217;t worry about what software or outliner to use. Learn the habits first and keep it simple.*</em></p>
	<p>How true is that! More and more I come to realize that the book is not like the Ten Commandments set in stone. What it teaches you is a way of thinking about your work and your priorities. How you handle them is completely up to you. The book reaches some ideas, but I feel it is up to <em>myself</em> and my <em>personal</em> situation how I incorporate those concepts. For instance, I don&#8217;t have a deskdrawer at work where I keep my projectmaterial. Why? Because we use special envelopes with the projectplanning on them for that material. Why would I start a new habit, a new way of organizing, when all my co-workers use another way? What <em>did</em> happen is I change the way I approach and use those envelopes. They are on my desk, but in a ordened manner and I can quickly find the stuff I need. New material goes directly in the right folder instead of lingering around on my desk.<br />
This is just one example. I feel I changed a lot of little habits and are more <em>aware</em> of the projects and actions around me than I was before.</p>
	<p><em>What are your changes in habits? Do you feel more aware? Leave a comment if you do!</em></p>

  
 
<p class="info">15 10 05 - 22:24 - <a href="/pivot/entry.php?id=6964&amp;w=whats_the_next_action" title="15 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6964&amp;w=whats_the_next_action#comm" title="Coolio123, Frank Meeuwsen, Reinier">three comments</a> <?php echo get_editentrylink("Edit", '6964'); ?></span></p>
</div><span id="e6963"></span><div class="entry">
<h3>Right click, make task</h3>
	<p><a href="http://sippey.typepad.com/filtered/2005/10/right_click_mak.html"  target='_blank'>This</a> sure sounds interesting to me: <br />
<blockquote>&#8221;[...] There are two videos up on Paul Thurrott&#8217;s SuperSite for Windows from PDC 2005 that are worth watching.  The first one shows off Outlook 12&#8217;s new &#8220;to do bar,&#8221; which is viewable when you&#8217;re reading email, and the ability to <strong><em>right-click and automatically create a task</em></strong> from a message.  The second one shows off the <strong><em>integrated RSS reader</em></strong>, and the rebuilt search implementation.&#8221;</blockquote><br />
Nice nice nice&#8230;.</p>

  
 
<p class="info">12 10 05 - 22:42 - <a href="/pivot/entry.php?id=6963&amp;w=whats_the_next_action" title="12 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6963&amp;w=whats_the_next_action#comm" title="John">one comment</a> <?php echo get_editentrylink("Edit", '6963'); ?></span></p>
</div><span id="e6962"></span><div class="entry">
<h3>Getting Things Done next Monday</h3>
	<p>This is a reply I wrote on the <a href="http://finance.groups.yahoo.com/group/Getting_Things_Done/"  target='_blank'>GTD@Yahoo</a> about emptying your Inbox at work. Thought I&#8217;d share it with all of you:<br />
<blockquote>&#8221; I am currently enjoying a two week holiday. Normally I have access to my workmail from home and I know from previous holidays I will check it every now and again. So this time, I decided something else. After my last day at work I asked the system administrator to change my emailpassword to something new and not tell me. So right now I can&#8217;t access my workmail. Excellent! Best thing is, because I know I can&#8217;t access it, I don&#8217;t have the pressure to check it every now and again.<br />
This leads me to the next issue: I left a week ago with an empty Inbox. Took me one friday afternoon, but I had everything covered. NA&#8217;s after my holiday, delegating current projects, filing projectmaterial.<br />
So when I arrive, I know there is some 500+ email waiting for me. I have my &#8220;Out of office&#8221; reply running until tuesday/ This means I have monday to work through this Inbox and get it to empty. After that, it&#8217;s time to check up with my co-workers <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/><br />
So to be honest, I am excited about getting back to work to see if this plan will work. I know it will!&#8221;</blockquote></p>
	<p>I will let you know next monday how it worked out!</p>

  
 
<p class="info">10 10 05 - 19:57 - <a href="/pivot/entry.php?id=6962&amp;w=whats_the_next_action" title="10 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6962&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6962'); ?></span></p>
</div><span id="e6961"></span><div class="entry">
<h3>DIY personal planner with Pocketmod</h3>
	<p>If there is one thing I absolutely admire in all the GTD-blogs, wiki&#8217;s, mailinglists and discussiongroups, it is the absolute sense of DIY to make your own life as easy as possible. Reading Getting Things Done was a (sort of) revelation for me to start with. When I read some of the hacks on 43Folders.com and started to find more and more through the various blogs and lists. One of the highlights was the whole <a href="http://www.moleskinerie.com/"  target='_blank'>Moleskine</a> subculture. Well, no, the whole paperbased-culture. Those who don&#8217;t <em>reject</em> PDA and electronics but <em>choose</em> to use paper as a better and more flexible way to write down thoughts, capture actions, make appointments and remember their (mobile) life. I think the <a href="http://www.punkey.com/pivot/entry.php?id=6924"  target='_blank'>DIY Hipster PDA</a> is the absolute highpoint of that subculture. <br />
<p style="text-align:center;"><img src="/images/pockedmod.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
I tried the Hipster PDA but it wasn&#8217;t all to my likings. I had to bind the papers with a paperclip and keep it in my Moleskine. Made it all very bulky and not easy for me to use. Today I found the <a href="http://www.pocketmod.com/"  target='_blank'>Pocketmod</a>. In true Google-style, it is in beta and in version 0.3. But it looks promising. You can choose your own templates, shuffle them in your own order and print a little book of it. The website includes foldinginstructions on video, so you can try it for yourself. Absolutely a must-see and must-try for all paperlovers! The website sports their own forum so you can discuss ideas and thoughts with other afficionados. For instance to talk about <a href="http://www.pocketmod.com/bb/viewtopic.php?t=14"  target='_blank'>combining the Pocketmod with the GTD TiddlyWiki</a>. No really&#8230;.</p>
	<p><a href="http://www.pocketmod.com/"  target='_blank'>Pocketmod.com</a></p>

  
 
<p class="info">10 10 05 - 18:30 - <a href="/pivot/entry.php?id=6961&amp;w=whats_the_next_action" title="10 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6961&amp;w=whats_the_next_action#comm" title="pocketgtder">one comment</a> <?php echo get_editentrylink("Edit", '6961'); ?></span></p>
</div><span id="e6960"></span><div class="entry">
<h3>Online GTD Discussiongroups, a comparison</h3>
	<p>Since GTD is considered the geeks way to personal productivity, it is common sense the system and it&#8217;s details are discussed on online mailinglists. Since I am a member of two of the large mailinglists, 43Folders and GTD@Yahoo (and still lurking the lists), I feel it&#8217;s nice to discuss a bit the similarities and differences of the two so you can decide for yourself which one suits you best.</p>

  
<a href="/pivot/entry.php?id=6960&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">05 10 05 - 09:53 - <a href="/pivot/entry.php?id=6960&amp;w=whats_the_next_action" title="05 Oct '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6960&amp;w=whats_the_next_action#comm" title="Daly de Gagne">one comment</a> <?php echo get_editentrylink("Edit", '6960'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>