<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e7082"></span><div class="entry">
<h3>3 days left for a FREE Mindmanager license</h3>
	<p>Just a quick reminder that this wednesday is the final day to enter for a free (!) license of MindManager 6 Pro. Check out the contestdetails at the end of <a href="http://www.punkey.com/pivot/entry.php?id=7081&#38;w=whats_the_next_action"  target='_blank'>this review of MindManager</a> and enter your favourite mindmap. I already received a dozen or so excellent maps so you will still have a chance! Good luck!</p>

  
 
<p class="info">29 10 06 - 19:27 - <a href="/pivot/entry.php?id=7082&amp;w=whats_the_next_action" title="29 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7082&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '7082'); ?></span></p>
</div><span id="e7081"></span><div class="entry">
<h3>Mindmanager, an excellent GTD tool? Win free licenses!</h3>
	<p><img src="/images/mindjet_logo.gif" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />It is no surprise that since I received my free copy of <a href="http://www.mindjet.com/eu/"  target='_blank'>MindJet&#8217;s MindManager</a> (MM), I have been using this program like crazy. And I must say, it is a very diverse and broad application which you can use in a variety of ways. What I would like to do is <em>not</em> run you through all the features and possibilities of MM but use it as a guideline through the five steps of GTD and see how you can use it. If you need to know what Mindmapping is or need a feature-rich review of MindManager, please check out <a href="http://www.mindjet.com/eu/products/mindmanager_pro6/quicktour.php"  target='_blank'>MindJet&#8217;s tour</a> on their website.</p>
	<p>After the review, you will have <strong>the chance to win one of five copies</strong> of MindManager Version 6 Pro, so stick around!</p>

  
<a href="/pivot/entry.php?id=7081&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">25 10 06 - 17:28 - <a href="/pivot/entry.php?id=7081&amp;w=whats_the_next_action" title="25 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7081&amp;w=whats_the_next_action#comm" title="Nick Duffill, KS, Jim, speaker">four comments</a> <?php echo get_editentrylink("Edit", '7081'); ?></span></p>
</div><span id="e7080"></span><div class="entry">
<h3>Scrybe is the killer GTD app?</h3>
	<p>I received an email a couple of days ago about <a href="http://www.iscrybe.com/"  target='_blank'>Scrybe</a>. Guess I wasn&#8217;t the only one. Right now, the productivosphere (just made that one up) is <a href="http://www.technorati.com/search/scrybe"  target='_blank'>buzzing with excitement</a> about this new online office application. Check out the promovideo after the click</p>

  
<a href="/pivot/entry.php?id=7080&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">22 10 06 - 10:40 - <a href="/pivot/entry.php?id=7080&amp;w=whats_the_next_action" title="22 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7080&amp;w=whats_the_next_action#comm" title="Paul, Blixa Bargeld">two comments</a> <?php echo get_editentrylink("Edit", '7080'); ?></span></p>
</div><span id="e7079"></span><div class="entry">
<h3>How to use GTD at Disney...</h3>
<p>How can you can connect a highly effective productivity principle with Mickey Mouse? Well, <a href="http://davidco.com/forum/showthread.php?t=2179"  target='_blank'>read this story</a> on how you can use the GTD principles when visiting Disney World. Or any other amusementpark for that matter. </p> <blockquote>We go to Disney alot, so I started a project called "Plan next Disney Trip" and stored on the attached notes any ideas, tips or reminders that I want to have for the next time (i.e. approximately how much to expect to spend on food, best times/days to attend the parks, etc).</blockquote> 
 
<p class="info">21 10 06 - 22:09 - <a href="/pivot/entry.php?id=7079&amp;w=whats_the_next_action" title="21 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7079&amp;w=whats_the_next_action#comm" title="Martijn">one comment</a> <?php echo get_editentrylink("Edit", '7079'); ?></span></p>
</div><span id="e7070"></span><div class="entry">
<h3>Smart keywords are really smart!</h3>
<p>OK, I always thought of myself as a Firefox poweruser. I use the keyboard shortcuts all the time, have my extension highly configured and know how to get at parts of the browser really fast.</p> <p>But one area of Firefox I never really touched until 10 minutes ago are the Smart Keywords. Oh sure, I type my keyword in the adressbar and it goes straight to Google. But every now and again I would like to search Wikipedia or YouTube. I used the searchbar for those searches but I just found out it can go a lot faster. With Smart Keywords. </p> <p><img style="border-right: 0px; border-top: 0px; border-left: 0px; border-bottom: 0px" height="306" src="http://www.punkey.com/images/add-keyword%5B10%5D.png" width="364" border="0"> </p> <p>You can <a title="An indepth look in Smart Keywords" href="http://johnbokma.com/firefox/keymarks-explained.html" target="_blank">read here</a> how you can add those smart searches to your own bookmarks. And better yet, you can use them on any searchengine. Even your intranet or your CRM system. Really powerful stuff and worth to look into!</p>

  
 
<p class="info">21 10 06 - 20:02 - <a href="/pivot/entry.php?id=7070&amp;w=whats_the_next_action" title="21 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7070&amp;w=whats_the_next_action#comm" title="Martijn">one comment</a> <?php echo get_editentrylink("Edit", '7070'); ?></span></p>
</div><span id="e7077"></span><div class="entry">
<h3>Netvibes GTD tab</h3>
	<p><img src="/images/netvibes_module.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Since a couple of weeks, I&#8217;ve been using <a href="http://www.netvibes.com"  target='_blank'>Netvibes</a> as my main startup page. I tried several of these dashboard/aggregator kind of pages but Netvibes gives me&#8230;well&#8230;good vibes. I have several tabs with the most important information for me to see in a glance. On my personal page I see my Gmail, Google Calendar, Mint referrers for this website, important news here in Holland a few other newspages. <a href="http://www.punkey.com/images/netvibes.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9uZXR2aWJlcy5qcGc%3D&w=1259&h=580&t=','imagewindow','width=1259,height=580,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>See for yourself. </a>I erased some info on the calendar for personal purposes.<br />
Netvibes also supports other-than-RSS content like Flickr-photos, searchpanes, Writely documents and eBay integration. Very interesting which gives me the chance to write about it some more in the future, perhaps a sort of &#8220;tips and tricks on netvibes&#8221;? Anyone interested in this?<br />
One other thing Netvibes comes with are keyboard-shortcuts, yay! With the arrow buttons, enter, the keys n,p,k,j and some other combinations, I can quickly run through the messages.<br />
Anyway, as you can see I have several tabs and one of them is a GTD tab. Ofcourse, this comes with a few resources found on the &#8216;Net about GTD. I decided to open up this tab for other Netvibes users. So you can use <a href="http://www.netvibes.com/subscribe.php?url=http://eco.netvibes.com/opml/dfd62f39889ba768ea52e6b0b8506f6a/gtd-tab.opml&#38;type=opml"  target='_blank'>this link</a> to get a GTD-tab in your own Netvibes-page. <br />
Let me know if you plan on using Netvibes or if you already use it, what are your thoughts about it?</p>

  
 
<p class="info">14 10 06 - 21:06 - <a href="/pivot/entry.php?id=7077&amp;w=whats_the_next_action" title="14 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7077&amp;w=whats_the_next_action#comm" title="Len Edgerly, Douglas, browsnik, Frank Meeuwsen, Matthew Cornell, Manny Hernandez, Angel, Ken, Dave, Magali, Joao Gazolla, Frank Johnson">thirteen comments</a> <?php echo get_editentrylink("Edit", '7077'); ?></span></p>
</div><span id="e7076"></span><div class="entry">
<h3>View the David Allen Webinar</h3>
	<p>Last week, Mindjet gave us the opportunity <a href="http://www.punkey.com/pivot/entry.php?id=7072&#38;w=whats_the_next_action"  target='_blank'>to visit</a> an online webseminar with David Allen. In a (technically challenged, I must say) hour-long webseminar, The Dave gave us some insight in how he uses his own GTD-principles. He gave us a little glimpse in his own mindmaps, for instance his 20,000 feet goals, a brainstorm-map about hiring a new P.A. and the futuree of the David Allen company. And yes, that brainstorm has a little box called &#8220;book 3&#8221; Hmmmm&#8230;.<br />
<p style="text-align:center;"><img src="/images/mindmap_copy1.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
You can view the webseminar again and download the Mindmaps on <a href="http://www.mindjet.com/us/company/events/davidallen.php"  target='_blank'>this page</a>. If you don&#8217;t have Mindjet&#8217;s Mindmanager, you can download it from the same page. You like what you see? Then stay tuned, because within a couple of days, I will give away a few free copies of Mindmanager! Yes, that is correct, the first ever competition on Whatsthenextaction.com. I need some time to write the article, so keep in touch through the <a href="http://feeds.feedburner.com/WhatsTheNextAction"  target='_blank'>RSS feed</a> or the <a href="http://www.feedblitz.com/f/?Sub=4678"  target='_blank'>Feedblitz-emailservice</a>!</p>

  
 
<p class="info">13 10 06 - 22:55 - <a href="/pivot/entry.php?id=7076&amp;w=whats_the_next_action" title="13 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7076&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7076'); ?></span></p>
</div><span id="e7073"></span><div class="entry">
<h3>Must-read: Priorities don�t exist in a vacuum</h3>
	<p>Merlin Mann, author on <a href="http://www.43folders.com"  target='_blank'>43folders.com</a> has written <a href="http://www.43folders.com/2006/10/01/priorities-vacuum/"  target='_blank'>an excellent piece</a> on the use of priorities in your life and your Next Actions list. Definitely worth a read and a save in you rreference folder.<br />
<blockquote>The truth is that sometimes you have crap days, pencils need to be sharpened, or maybe you just don�t have the tools or energy to do what you want the second you want. That�s life, pal. Deal.</blockquote><br />
Make sure you also read the comments on some more insight on this subject.</p>

  
 
<p class="info">03 10 06 - 17:00 - <a href="/pivot/entry.php?id=7073&amp;w=whats_the_next_action" title="03 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7073&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7073'); ?></span></p>
</div><span id="e7072"></span><div class="entry">
<h3>Join a free webseminar with David Allen</h3>
	<p>Mindjet invites us for a <a href="http://www.webex.com/web-seminars/view_event/669437426"  target='_blank'>1-hour Webinar</a> with The Man, David Allen. On Tuesday, October 10, 2006, 10:00 AM PDT (that&#8217;s 7 PM for me on the same day&#8230;) you can join this free webseminar right from your browser. From the raving invitation: <br />
<blockquote>Join us for a scintillating hour with Allen as he reviews best practices to increase your productivity, both at home and at work. Find out how technology &#8211; in particular Mindmapping and web conferencing &#8211; are powerful tools that Allen considers part of anyone&#8217;s productivity &#8216;bag of tricks&#8217;. Leave this web seminar with new ideas and concepts on how to increase your productivity in the four key areas of Allen&#8217;s model
	<ul>
		<li>Externalizing information and capturing notes</li>
		<li>Defining current reality</li>
		<li>Generating new ideas and perspectives</li>
		<li>Organizing information</li>
	</ul></p>
	<p>We all know we should probably be more thoughtful, creative, focused, and organized &#8211; join us for this 60 minute web seminar with the person that Fast Company cites as &#8221;...one of the most influential thinkers on productivity&#8221; to find out how!<br />
</blockquote></p>
	<p>I already registered, maybe <a href="http://www.webex.com/web-seminars/view_event/669437426"  target='_blank'>you should too</a>!</p>

  
 
<p class="info">01 10 06 - 22:27 - <a href="/pivot/entry.php?id=7072&amp;w=whats_the_next_action" title="01 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7072&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7072'); ?></span></p>
</div><span id="e7071"></span><div class="entry">
<h3>New feature: Textareatools</h3>
	<p>I just added a new feature to the comments of this blog. If you think your textarea is too small, you can use the buttons on the right to increase the size of the textarea or to increase the fontsize. It should work in all common browsers. If you find some difficulty or an error, please let me know<br />
The extension is <a href="http://forum.pivotlog.net/viewtopic.php?t=7060"  target='_blank'>provided by</a> the author of Pivot weblogsoftware, Bob den Otter</p>

  
 
<p class="info">01 10 06 - 21:06 - <a href="/pivot/entry.php?id=7071&amp;w=whats_the_next_action" title="01 Oct '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7071&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7071'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ol>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7081">Mindmanager, an excellent GTD tool? Win free licenses!</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7080">Scrybe is the killer GTD app?</a>
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7069">6 ways to run an effective meeting </a>
<li><a href="http://punkey.com/pivot/entry.php?id=7077">Netvibes GTD tab</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7021">The 5 reasons why The Weekly Review is difficult</a>
<li><a href="http://punkey.com/pivot/entry.php?id=1726">Still digging Evernote?</a>
</ol>
Made possible with <a href="http://www.haveamint.com">Mint</a><br>
<p align="center"><img src="/images/mint-80x15.gif" width="80" height="15" align="middle" border="0" /><p><br />
<br />
</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#frank_johnson-0611072353' title='07 11 2006 - 23:53' ><b>Frank Johnson</b></a> (Netvibes GTD tab): Douglas: In case you check back, I u&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#steve_newson-0611041127' title='04 11 2006 - 11:27' ><b>Steve Newson</b></a> (We have our winne&hellip;): Thank you so much for picking me as &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#luciano_passuello-0611032057' title='03 11 2006 - 20:57' ><b>Luciano Passuello&hellip;</b></a> (We have our winne&hellip;): Just as Bill, I would like to thank &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#bill_reichart-0611030119' title='03 11 2006 - 01:19' ><b>Bill Reichart</b></a> (We have our winne&hellip;): Thanks for picking me as a winner.  &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#frank_meeuwsen-0611022209' title='02 11 2006 - 22:09' ><b>Frank Meeuwsen</b></a> (We have our winne&hellip;): OK, I found out when you use IE you &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#mike-0611022149' title='02 11 2006 - 21:49' ><b>Mike</b></a> (We have our winne&hellip;): I too have a problem getting the .mm&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#horacio-0611021442' title='02 11 2006 - 14:42' ><b>Horacio</b></a> (We have our winne&hellip;): Thanks for the contest!  As I&#8217;m disco&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#mario_from_italy-0611020944' title='02 11 2006 - 09:44' ><b>Mario from Italy</b></a> (We have our winne&hellip;): Well, the 1st thing I did this morni&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#martijn-0611012200' title='01 11 2006 - 22:00' ><b>Martijn</b></a> (We have our winne&hellip;): Congrats to all of you. I guess I&#8217;m j&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#speaker-0611010325' title='01 11 2006 - 03:25' ><b>speaker</b></a> (Mindmanager, an e&hellip;): I&#8217;ve used FreeMind 8.0 as well as a b&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>