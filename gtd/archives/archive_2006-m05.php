<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e7034"></span><div class="entry">
<h3>Adobe/Macromedia entering the GTD-space?</h3>
	<p>This afternoon I noticed <a href="http://blogs.adobe.com/kiwi/2006/05/an_overview_of_notetag_1.html"  target='_blank'>a very interesting post</a> on one of the developers blog from Adobe. They made a webbased tool called <a href="http://labs.adobe.com/wiki/index.php/NoteTag"  target='_blank'>NoteTag</a>, which gives you a flash-based environment to take notes. But that&#8217;s not all. The notes are not stored in some proprietary format like EverNote, but you can publish them directly to Blogger or del.icio.us for that matter. Any Atom supported service can work as a backend. Very very interesting! Check out <a href="http://labs.adobe.com/technologies/notetag/demo/"  target='_blank'>the screencast</a> for a cool demo! And <a href="http://notetag-dc1.blogspot.com/"  target='_blank'>the blog</a> that goes with it <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>
<p style="text-align:center;"><img src="/images/01_notetag_note_001.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
	<p>The big downtake is you will need a Flex server to run it, but as a proof-of-concept I can imagine this further developed by a third party and put to use as an ASP model like Backpack or RememberTheMilk. Since we have certified Flex-developers in our company, I will surely check out if I can take some R&#38;D time to discuss this concept a but further. It might be very interesting for projectgroups. But I can also see the GTD-link with this kind of applications.</p>

  
 
<p class="info">26 05 06 - 18:38 - <a href="/pivot/entry.php?id=7034&amp;w=whats_the_next_action" title="26 May '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7034&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '7034'); ?></span></p>
</div><span id="e7033"></span><div class="entry">
<h3>Lil&#039; easteregg in Google Notebook</h3>
	<p>I found a nice little easteregg which makes using <a href="http://www.google.com/notebook/"  target='_blank'>Google Notebook</a> even easier. Well&#8230;I found <a href="http://inner.geek.nz/archives/2006/05/18/google-notebook-firefox-extension-easter-egg/"  target='_blank'>an article</a> describing the easteregg. After enabling it you will see a little &#8220;note this +&#8221; sign next to your selected text. This makes it <em>really</em> easy to notate nice quotes, pieces of articles and other online stuff. <a href="http://inner.geek.nz/archives/2006/05/18/google-notebook-firefox-extension-easter-egg/"  target='_blank'>Check it out</a> for yourself.</p>

  
 
<p class="info">22 05 06 - 09:31 - <a href="/pivot/entry.php?id=7033&amp;w=whats_the_next_action" title="22 May '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7033&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7033'); ?></span></p>
</div><span id="e7032"></span><div class="entry">
<h3>Let&#039;s get David Allen to the Netherlands!</h3>
	<p style="text-align:center;"><img src="/images/gtdmasthead.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
You Americans&#8230;you are soooo lucky that The Dave lives right around the corner and he makes his <a href="http://www.davidco.com/seminars/seminar_the_roadmap.php"  target='_blank'>Roadmap tour</a> across your country. We here in Europe are stuck with the GTD Fast CD&#8217;s and <a href="http://www.google.nl/search?q=GTD+roadmap&#38;start=0&#38;ie=utf-8&#38;oe=utf-8&#38;client=firefox-a&#38;rls=org.mozilla:en-US:official"  target='_blank'>blogs</a> about how cool it is to attend a GTD seminar. <br />
Well, that got me thinking. Since David Allen is in London on july 20th, why not extend that little trip to The Netherlands? Just across the Northsea! </p>
	<p>On his Roadmap tour, David talks about implementing GTD in your professional and personal life. As he says himself on <a href="http://www.davidco.com/blogs/david/archives/2005/05/quick_answer_to.html#more"  target='_blank'>this little interview</a> : &#8220;More focus on the process of deciding where you are and what part of the models you should do what about, now&#8230; e.g. should you do 40k thinking with you spouse right now, or clean your garage&#8212;- get a planner or throw it away?&#8221;<br />
Make sure you check out <a href="http://www.davidco.com/seminars/seminar_the_roadmap.php"  target='_blank'>the contents</a> of the seminar for a more indepth overview<br />
If you are serious about getting your projects and your work straight through GTD, this might be The One seminar to attend.</p>
	<p>So I got in touch with Rachelle from the David Allen Company and she is more than happy to help me get him to come over. But we need to get a room full of people for him to talk to. Otherwise it would be just a waste of time, right?<br />
So here is the question, especially for those in the Netherlands, are you interested in attending a GTD seminar by David Allen? I t will not be a free seminar, costs are about 600 dollar per person. Please consider this when replying. I would love to get him to speak in The Netherlands, but I need about 150 people to show up! <br />
So let me know in the comments how you feel about this and I will see what I can do!</p>

  
 
<p class="info">17 05 06 - 00:20 - <a href="/pivot/entry.php?id=7032&amp;w=whats_the_next_action" title="17 May '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7032&amp;w=whats_the_next_action#comm" title="Ruben Timmerman, Niels Bom, Martijn, Guy Dickinson">four comments</a> <?php echo get_editentrylink("Edit", '7032'); ?></span></p>
</div><span id="e7031"></span><div class="entry">
<h3>I heart Weekly Review!</h3>
	<p>Yeah! Yeah! Yeah! It is sunday afternoon right now. And I feel more relaxed than any other sunday afternoon for the past couple of months. Why is that? Because I had not only one, but two <a href="http://www.punkey.com/pivot/entry.php?id=7021&#38;w=whats_the_next_action"  target='_blank'>Weekly Reviews</a> in a row! And it felt great doing them! Let me explain in this post what I did and how it affected me in my work over just this short period&#8230;Perhaps you will get some tips and inspiration from it</p>

  
<a href="/pivot/entry.php?id=7031&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">15 05 06 - 00:08 - <a href="/pivot/entry.php?id=7031&amp;w=whats_the_next_action" title="15 May '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7031&amp;w=whats_the_next_action#comm" title="speaker, Frank">two comments</a> <?php echo get_editentrylink("Edit", '7031'); ?></span></p>
</div><span id="e7029"></span><div class="entry">
<h3>The myth of keeping up with your reading</h3>
	<p>The blog &#8220;Creating Passionate Users&#8221; has <a href="http://headrush.typepad.com/creating_passionate_users/2006/04/the_myth_of_kee.html"  target='_blank'>an excellent write-up</a> on why you will never keep up with all your reading and how you can help yourself reducing the amount of information you want or need to absorb. One of the best<br />
<blockquote>Don&#8217;t file it. Don&#8217;t store it. What you don&#8217;t have piling up you can&#8217;t feel guilty about. Some people put little height limits on their &#8220;to read&#8221; stack. (OK, when it gets as high as that drawer, I must throw out the oldest 50%...)</blockquote><br />
I am also interested in the <a href="http://www.getabstract.com/index.jsp"  target='_blank'>GetAbstract</a> service. I will definitely try this one out the coming days&#8230;</p>

  
 
<p class="info">07 05 06 - 00:01 - <a href="/pivot/entry.php?id=7029&amp;w=whats_the_next_action" title="07 May '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7029&amp;w=whats_the_next_action#comm" title="Chris Walker, Matthew Cornell">two comments</a> <?php echo get_editentrylink("Edit", '7029'); ?></span></p>
</div><span id="e7028"></span><div class="entry">
<h3>OpenSubject a lifesaver for your email?</h3>
	<p><a href="http://opensubject.pbwiki.com/"  target='_blank'>OpenSubject</a> is an public project to make email correspondence more efficient through the use of simple 3 letter codes. Instead of using your project name as a subject for every message you sent you enter a code which says something about the contents or tha message and the desired action to be taken by the receiver. <br />
Example Subject: NRN: Project X Information<br />
Means: NO Reply Needed: this message contains information about Project X</p>
	<p>With their own list of three letter codes they give some good hints to start with this. Big issue: How to remember the codes?</p>

  
 
<p class="info">06 05 06 - 23:42 - <a href="/pivot/entry.php?id=7028&amp;w=whats_the_next_action" title="06 May '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7028&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7028'); ?></span></p>
</div><span id="e7027"></span><div class="entry">
<h3>I&#039;m Black Belt! Well...sort of...</h3>
	<p><img src="/images/56832765_86a232bc54_m.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Previously I mentioned a new GTD-related blog called <a href="http://www.blackbeltproductivity.net/"  target='_blank'>Blackbeltproductivity.net</a>. This didn&#8217;t stay unnoticed and the guys from the blog, Jason Echols and Michael Ramm, decided to invite me for <a href="http://www.blackbeltproductivity.net/blog/category/black-belts-series/"  target='_blank'>an upcoming series</a> on their blog called &#8220;The Black Belt Series&#8221;. I am going to write for them throughout the year about GTD and how it has affected me. Well, what an honour for a dutch cheesehead as myself! I must say, I am in very good company! Check out the other authors:
   1. Emory Lundberg (<a href="http://kvet.ch/"  target='_blank'>http://kvet.ch</a>)
   2. Matt Cornell, of <a href="http://ideamatt.blogspot.com/"  target='_blank'>Matt�s Idea Blog</a>
   3. bsag, developer of <a href="http://www.rousette.org.uk/projects/"  target='_blank'>Tracks</a>, a a web-based application to help you implement GTD� methodology
   4. Jason Womack, David Allen Co. Employee &#38; <a href="http://www.davidco.com/blogs/jason/"  target='_blank'>Evangelist</a>
   5. Punkey, of <a href="http://www.punkey.com/gtd/"  target='_blank'>What�s the next action</a> (hey that&#8217;s me!)
   6. GTD Wannabe, of <a href="http://gtdwannabe.blogspot.com/"  target='_blank'>GTD Wannabe</a>
   7. pooks, author and blogger at <a href="http://planetpooks.wordpress.com/"  target='_blank'>planet pooks</a>
   8. Allen Hall, of <a href="http://searching4arcadia.wordpress.com/"  target='_blank'>searching4arcadia</a>
   9. John Winstanley, of <a href="http://mypalmlife.com/index.php"  target='_blank'>MyPalmLife</a></p>
	<p>Not too bad &#8216;ey? I look forward to this weekend. I have planned some thinking about the given topics and some rough outlines of what I am going to write. Now if only the sun can stay away for a couple of hours so I am not tempted to go outside and catch some first rays of sun this year&#8230;<br />
And one other thing&#8230;I am starting fresh tomorrow with a <em>planned</em> Weekly Review&#8230;woohoo&#8230;.I scheduled 4 hours in the morning for myself to really do some reviewing. I hope it&#8217;s gonna work. One thing I will be focussing on is <em>when</em> I get distracted, I will try to find out <em>what&#8217;s</em> distracting me and <em>why</em>. With that information, I can perhaps help myself better focussing on the Weekly Review in the time to come. </p>
	<p>(Photo courtesy of <a href="http://www.flickr.com/photos/jackol/56832765/"  target='_blank'>Jackol</a>)</p>

  
 
<p class="info">03 05 06 - 20:54 - <a href="/pivot/entry.php?id=7027&amp;w=whats_the_next_action" title="03 May '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7027&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7027'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>