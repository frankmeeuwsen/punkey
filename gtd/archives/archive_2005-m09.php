<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e6959"></span><div class="entry">
<h3>The adventures of Action Item</h3>
	<p style="text-align:center;"><img src="/images/actionitem.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
An <a href="http://www.fatalexception.org/action_item.html"  target='_blank'>hilarious parody</a> on meetings most of us once have visited. Everybody is talking is this weird projectmanagement-stylee mumbo jumbo and no action is really taken. I can give you some true examples with my clients, but I will restrain myself <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/><br />
Take a look at <a href="http://www.fatalexception.org/action_item.html"  target='_blank'>this excellent comic</a> and <a href="http://www.cafepress.com/actionitem"  target='_blank'>order the poster</a>. Rumour has even 43Folders-guru <a href="http://www.43folders.com"  target='_blank'>Merlin Mann</a> has one&#8230;.</p>

  
 
<p class="info">26 09 05 - 19:22 - <a href="/pivot/entry.php?id=6959&amp;w=whats_the_next_action" title="26 Sep '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6959&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '6959'); ?></span></p>
</div><span id="e6958"></span><div class="entry">
<h3>Work in progress on this site</h3>
	<p>I would like to tell you all very briefly some of the plans I have on this site. Since it&#8217;s almost a year old and I haven&#8217;t paid much attention to it visually, I have some plans to redesign the whole blog. But wait! There&#8217;s more! </p>
	<p><strong>Redesign</strong><br />
One thing I would like to do is to let it look <em>less like a blog</em> and more as an information hub on GTD and personal productivity. One thing I rarely do is talk about other articles I read and blogs I visit. That is going to change. For starters, I have added a little linkbox to <a href="http://del.icio.us/punkey/gtd"  target='_blank'>my del.icio.us GTD-list</a> of articles, sites, hacks and tips. Maybe not everything there is (Well, <em>definitely</em> not all there is) but it gives you some sense on my take on GTD, what interests me.</p>
	<p><strong>Old blog</strong><br />
There is more on this server than just this weblog. I started in 2000 a personal dutch weblog. Basically a linkdump. I stopped this summer with it but the archives remain. I would like to make that archive better searchable and perhaps do something with these 5 years of blogposts. Some sort of random post of the day kind of thing&#8230;</p>
	<p><strong>Thesis in a wiki</strong><br />
Furthermore, I am working on putting my thesis online. I wrote it back in 1995 and it&#8217;s topic is scenemarketing. Much of it&#8217;s theory is still applied today and blogs, webfeeds and consumer generated media fit perfectly in this theory. The thesis is all dutch. I have the plan to put it in a (sem<img src='/extensions/emoticons/trillian/e_14.gif' alt='I-)' align='middle'/>controlled Wiki so everyone can benefit from it. </p>
	<p><strong>Resume</strong><br />
Than, last but not least, I get more questions to write articles and hold presentations on blogging and the new web. I will collect and publish these also on my server. So you see, this blog is just a part of my online world. I have the ambition to have it all done by the end of the year. </p>
	<p>And now some GTD-relation: It was on my Someday-list for a long time. Now, whenever I have some spare time, I just fiddle and tweak with some designs, try out different feels of the site, try new stylesheets. There is no grand plan, just some progress notes and next actions on a little note near my monitor. There is actually nothing holding me back than to just crawl behind my monitor and start working. So the really next physical action is: Work! Do it! <br />
But, as always, I do have a question which doesn&#8217;t stall me, but I am just curious: Does anyone know of a good reference site to alter some stylesheets in <a href="http://www.mediawiki.org/wiki/MediaWiki"  target='_blank'>MediaWiki</a>? I am looking for a more slender, centered design for my wiki if possible. I already found out the basic HTML of MediaWiki isn&#8217;t that portable, but if anyone has some pointers, please let me know! I know of the <a href="http://meta.wikimedia.org/wiki/Gallery_of_user_styles"  target='_blank'>skin-directory</a>, but I haven&#8217;t found anything usefull there</p>

  
 
<p class="info">25 09 05 - 22:42 - <a href="/pivot/entry.php?id=6958&amp;w=whats_the_next_action" title="25 Sep '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6958&amp;w=whats_the_next_action#comm" title="Daly de Gagne, Frank Meeuwsen">three comments</a> <?php echo get_editentrylink("Edit", '6958'); ?></span></p>
</div><span id="e6957"></span><div class="entry">
<h3>MyTicklerFile.com...What&#039;s the use?</h3>
	<p><img src="/images/ticklerfile.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" /><a href="http://www.myticklerfile.com/"  target='_blank'>MyTicklerfile.com</a> is a new webbased tool for creating appointments and recurring tasks. As they say for themselves it can be used to implement Getting Things Done in your own way. But I have my doubts. First off, thanks to the buzzword-friendly Web 2.0 features of <a href="http://en.wikipedia.org/wiki/Ajax_%28programming%29"  target='_blank'>Ajax</a> and tags it has some real desktop-like, rich experience to it. This really enhances the way to work with it. But what bothers me is the underlying principle. This is just for ticklers, reminders. You can use your emailclient to send reminders to your Myticklerfile-account and you can get ticklers through email. But I think to myself: Why have an online application for it? And it&#8217;s just one piece of the puzzle. What about your Next Actions? What about filing your information? It really brings back memories of my <a href="http://www.punkey.com/pivot/entry.php?id=1726"  target='_blank'>Nine Inboxes problem</a>, where all the information was scattered over mailprograms, websites, offline folders and desktop apps. When you use MTF, I get the feeling you have <em>*another*</em> website you have to keep track off, manage, update etc. I keep coming back to using some sort of dashboard-mechanism where I can get an horizontal overview of my daily to-do&#8217;s, appointments, tasks etc. MS Outlook so far can give me that overview. In combination with the <a href="http://gtdsupport.netcentrics.com/home/"  target='_blank'>GTD Outlook Addin</a> I feel I am <em>really</em> getting things done already. Still not there yet (black belt? Forget it&#8230; ) but at least I am taking little steps instead of standing still. <br />
<em>Conclusion:</em> If you just need some tool to keep track of your reminders and appointments, Myticklerfile.com can be the webtool for you. But if you are serious about your GTD, I feel it is too light.</p>

  
 
<p class="info">24 09 05 - 22:11 - <a href="/pivot/entry.php?id=6957&amp;w=whats_the_next_action" title="24 Sep '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6957&amp;w=whats_the_next_action#comm" title="Marion Newman">one comment</a> <?php echo get_editentrylink("Edit", '6957'); ?></span></p>
</div><span id="e6956"></span><div class="entry">
<h3>The &quot;new gadget&quot; itch starts again...</h3>
	<p>Oh man&#8230;.just when I was trying to get comfortable with all the software I have, a new version of <a href="http://www.jetbrains.com/omea/index.html"  target='_blank'>Omea Pro</a> drops in my Inbox. I once tried Omea Pro in beta and was very impressed by its features. It is simply described as a dashboard for your information from Outlook, browser, filesystem and webfeeds. All integrated in one environment with lots of features to aggregate, organize and find the information. <br />
<p style="text-align:center;"><img src="/images/omeaschema.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
You can have multiple &#8220;dashboard&#8221; based on your context like &#8220;Work&#8221;, &#8220;Home&#8221; with different email, webfeeds and files at your fingertips. I stopped using the beta because of the heavy load on my laptop. Hmmm&#8230;what is that anyway? OnFolio had the same problem&#8230;<br />
Now, in the new version they totally hook on the GTD-lingo. Check out this marketing-tidbit from <a href="http://www.jetbrains.com/omea/features/handle.html"  target='_blank'>the website</a></p>
	<p>&#8220;Omea Pro helps you to efficiently handle different types of incoming information, whether you decide to:
	<ul>
		<li> Respond &#38; act right away,</li>
		<li> Delegate a task to someone else, or</li>
		<li> Defer &#38; track items for attention later.&#8221;</li>
	</ul></p>
	<p>Ooooh&#8230;..makes me want to download the program and start fiddling away with it&#8230;.But I will not! Yet&#8230;.<br />
Anyone has some more information on Omea Pro and perhaps has a little review of it? I would be glad to read it.</p>

  
 
<p class="info">20 09 05 - 22:01 - <a href="/pivot/entry.php?id=6956&amp;w=whats_the_next_action" title="20 Sep '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6956&amp;w=whats_the_next_action#comm" title="Fred Zelders, Simon, David Booth, Matthew Cornell">four comments</a> <?php echo get_editentrylink("Edit", '6956'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>