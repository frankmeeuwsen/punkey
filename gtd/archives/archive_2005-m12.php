<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e6987"></span><div class="entry">
<h3>Best wishes for 2006!</h3>
<p><img src="/images/close2005.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Hey all, since I'm leaving for my birthtown in a couple of hours to celebrate the new year, I will be offline shortly. I want to take this moment to wish every single one of you the very best for 2006. For me, in The Netherlands, it's still 14,5 hours away. So I guess for most of my American visitors here, it might even take longer before you can wish your friends and loved ones all the best. I'm not gonna look back at GTD the past year. Well at least, <em>not yet</em>. I plan on doing so in a couple of days. I have some other stuff to work on this last day of 2005. Not really work, but more some loose ends I want to tie before the fireworks break loose here.</p><p>One thing that is for sure is that I will continue this weblog in 2006. Even though it's hard to update and I really have to think through my writing, I find it educational, reflecting and yes, sometimes confronting to write about my own personal productivity. I also notice it helps other people moving forward in releasing the chaos in their heads, desks and drawers. That gives me pleasure and fullfilment that it's not useless what I write here.</p><p>So, I will be back in 2006, with more articles, thoughts, rants and philosophies. I hope you will be here as well to share your thoughts and ask questions. BTW, I just found my slogan for the new year:</p><p><em><strong>Release the chaos.&nbsp;</strong></em></p><p>(photo courtesy of <a href="http://www.flickr.com/photos/almaz/79680109/"  target="_blank" target='_blank'>AlmaZ</a>)</p> 
 
<p class="info">31 12 05 - 09:48 - <a href="/pivot/entry.php?id=6987&amp;w=whats_the_next_action" title="31 Dec '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6987&amp;w=whats_the_next_action#comm" title="Matthew Cornell">one comment</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '6987'); ?></span></p>
</div><span id="e6983"></span><div class="entry">
<h3>Mindmapping eBook?</h3>
	<p><img src="/images/mindmap.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I am always a bit cautious when I see a site like this: <a href="http://www.mindmap-ebook.com/"  target='_blank'>E-Book: Power Tips &#38; Strategies for Mind Mapping Software</a> (insert bold and pompous music here and start smoke effects)<br />
I do believe in Mindmaps. I don&#8217;t use them enough in both my personal and professional life. My best way to learn new habits is to read about them, try them bit by bit and make it my own. So some sort of eBook might be a good way. Better than a blog, since a book is more structured than weblog. <br />
So does anyone of you have any thoughts on this eBook? Perhaps read it? I still have this feeling it&#8217;s all fluff and no contents because of the layout, style of writing etc. Perhaps any of my dear readers can help. Since comments are working again&#8230;<img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>

  
 
<p class="info">28 12 05 - 17:04 - <a href="/pivot/entry.php?id=6983&amp;w=whats_the_next_action" title="28 Dec '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6983&amp;w=whats_the_next_action#comm" title="Michael Langford, Frank, Katy">three comments</a> <?php echo get_editentrylink("Edit", '6983'); ?></span></p>
</div><span id="e6980"></span><div class="entry">
<h3>Comments have been broken...</h3>
	<p>I was wondering today why I didn&#8217;t get any comments for about two months already. Since I am not used to a lot of comments here, It didn&#8217;t occur to me that something might be wrong with the commentsystem itself. Well, it is&#8230;I just found out the commentsystem is broken. Don&#8217;t know why or how it happened but I will try to solve it as fast as possible!</p>
	<p><strong><em>Update 90 minutes later</em></strong>: Maybe not the best solution, but I downgraded the commentsystem to an earlier version of Pivot. So comments are open again!</p>

  
 
<p class="info">27 12 05 - 22:02 - <a href="/pivot/entry.php?id=6980&amp;w=whats_the_next_action" title="27 Dec '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6980&amp;w=whats_the_next_action#comm" title="Frank, Marianne Powers">three comments</a> <?php echo get_editentrylink("Edit", '6980'); ?></span></p>
</div><span id="e6975"></span><div class="entry">
<h3>Get into Backpack easy!</h3>
	<p>OK, so here&#8217;s the deal. You all know I use Backpack as a personal organiser. I wrote about it a <a href="http://www.punkey.com/pivot/entry.php?id=6971"  target='_blank'>couple of weeks ago</a> and according to the stats, the article is still very popular. So why don&#8217;t I recommend Backpack to all of you and get a little better of it myself? Yes, it&#8217;s affiliate-marketing-time!</p>
	<p><a href="http://backpackit.com/?referrer=BPF9BJ9"  target='_blank'><img border="0" title="Backpack: Get Organized and Collaborate" alt="Backpack: Get Organized and Collaborate" src="http://punkey.backpackit.com/images/backpack46860.gif" width="468" height="60" /></a></p>
	<p>So here it is, if you <a href="http://backpackit.com/?referrer=BPF9BJ9"  target='_blank'>sign up</a> for a paid plan at Backpackit.com (starting with $5 a month) and use the promotional code <strong>BPF9BJ9</strong> as a referencecode, I will receive credits which makes me want to use Backpackit.com even more! And better yet, I can give you guys more tips and tricks on how to use it! And the even bestest part (is that a word?) is when you sign up, <em>you</em> will also have access to the affiliate program! So even you can have Backpack for free if you use it smart. So once again, go to <a href="http://www.Backpackit.com"  target='_blank'>Backpackit.com</a>, sign up for one of their plans and use promotional code <strong>BPF9BJ9</strong> as a reference.</p>

  
 
<p class="info">20 12 05 - 09:20 - <a href="/pivot/entry.php?id=6975&amp;w=whats_the_next_action" title="20 Dec '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6975&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6975'); ?></span></p>
</div><span id="e6974"></span><div class="entry">
<h3>Share in my joy!</h3>
	<p style="text-align:center;"><a href="http://www.punkey.com/images/myinbox.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9teWluYm94LmpwZw%3D%3D&w=849&h=412&t=','imagewindow','width=849,height=412,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'><img src="http://www.punkey.com/images/myinbox.thumb.jpg" border="1" alt="" title=""  class='pivot-popupimage'/></a></p><br />
<a href="http://www.punkey.com/images/myinbox.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9teWluYm94LmpwZw%3D%3D&w=849&h=412&t=','imagewindow','width=849,height=412,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>Click here</a> for the full picture. Aaaah&#8230;..this feels good&#8230;.Everything is in my Next Actions list, delegated of deferred. And LOTS of it is just trashed!</p>

  
 
<p class="info">19 12 05 - 18:39 - <a href="/pivot/entry.php?id=6974&amp;w=whats_the_next_action" title="19 Dec '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6974&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6974'); ?></span></p>
</div><span id="e6973"></span><div class="entry">
<h3>How to use shoeboxes with GTD</h3>
	<p>Now this is a really funny post over at the <a href="http://groups.google.com/group/43Folders"  target='_blank'>43Folder group</a> at Googlegroups. One reader was thinking how he could have some sort of reversable Inbox. That way, anything that gets in First, will get processed first. Michael Langford came with the following tip&#8221;<br />
<blockquote>Take 2 identical shoe boxes. Remove lid, discard box 1</p>
	<p>Cut out bottom of box 2. Put lid 1 on bottom of box 2. Write &#8220;INBOX&#8221;<br />
on side of box 2. Flip box over write &#8220;PROCESSING&#8221; on side.</p>
	<p>Use inbox with INBOX readable. Flip over to do processing.</p>
	<p>I have no idea what to do with the extra lid when you&#8217;re not flipping<br />
the box over.</blockquote></p>
	<p>How funny and how simple!</p>

  
 
<p class="info">17 12 05 - 17:54 - <a href="/pivot/entry.php?id=6973&amp;w=whats_the_next_action" title="17 Dec '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6973&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6973'); ?></span></p>
</div><span id="e6972"></span><div class="entry">
<h3>My mind is playing tricks on me...</h3>
	<p>OK, I read the book, listened to the CD, bought the Outlook add-in (which mysteriously unregistered but that&#8217;s another story) and still it doesn&#8217;t click with me. To give an example. Every morning, I drive by a convention centre on the way to work. Yesterday I saw an announcement for an interesting conference I felt I should remember. But two blocks further I totally forgot about it due to traffic and other things on my mind. Never thought about it untill this morning when I saw the sign again. And I think to myself: &#8221; &#8216;Aight, let&#8217;s do this black belt stylee.&#8221; As David talks about on one of his GTD Fast CD&#8217;s, you should envision yourself writing the thing down on the first moment you can. Because I couldn&#8217;t write it down when riding a bike and no way am I gonna stop to write it down. It&#8217;s a cold winter man! So I envisioned myself walking in the office to my desk and writing the date and name of the conference. Great, let&#8217;s move along</p>
	<p>Yeah, that&#8217;s where the trickery starts. Each morning when I go to work, I like to think about things to do, set some sort of a roadmap for that day. Today is a day to write an offer for a client. So I wanted to think about that during the morning-commute. But whaddayaknow<img src="?" alt="" /> From the moment I envisioned myself to write down that conference, I couldn&#8217;t stop thinking about that! Not able to think of anything else but the moment I walk into the office and write down those conference details. Now how does that suck! Because that is not why I wanted to envision it! I wanted to envision it so I wouldn&#8217;t have to think about it.</p>
	<p>As I mumbled to myself when I drove along: &#8220;Damn you, David Allen, for making my life so much harder with GTD&#8230;&#8221;<br />
Once you know these things, there is no turning back. Because you know there is a better way&#8230;.</p>

  
 
<p class="info">09 12 05 - 08:34 - <a href="/pivot/entry.php?id=6972&amp;w=whats_the_next_action" title="09 Dec '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6972&amp;w=whats_the_next_action#comm" title="Marc, Rich">two comments</a> <?php echo get_editentrylink("Edit", '6972'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>