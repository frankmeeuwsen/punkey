<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e6935"></span><div class="entry">
<h3>Overorganization, some thoughts</h3>
	<p>This evening, I was once again reading some excellent conversations in the 43Folders group. If you are looking form some inspirational articles on GTD and personal productivity, make sure to check them out. One thread was of particular interest for me. The thread is called Overorganising, To Many Tools and can be found in the <a href="http://groups-beta.google.com/group/43Folders/browse_frm/thread/4c190395a80870a9/1deb088d87b0188d?tvc=1&#38;q=Overorganising,+To+Many+Tools&#38;hl=en#1deb088d87b0188d"  target='_blank'>Google Groups Archive</a><br />
It deals with the problem I also deal with every now and then. You try to do everything in a nice GTD-kind of way and after a few days,weeks, software switches, you find yourself twiddling and tweaking your system more than you are getting things done. When I read the thread, especially <a href="http://groups-beta.google.com/group/43Folders/tree/browse_frm/thread/4c190395a80870a9/1deb088d87b0188d?rnum=1&#38;hl=en&#38;_done=%2Fgroup%2F43Folders%2Fbrowse_frm%2Fthread%2F4c190395a80870a9%2F1deb088d87b0188d%3Ftvc%3D1%26hl%3Den%26#doc_0ddbe418380be572"  target='_blank'>Larry Underhill&#8217;s remarks</a> struck a chord with me. He describes how he uses GTD but not tries to implement it in a black belt kind of way. But just enough for him to get his stuff done.</p>

  
<a href="/pivot/entry.php?id=6935&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">24 07 05 - 00:04 - <a href="/pivot/entry.php?id=6935&amp;w=whats_the_next_action" title="24 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6935&amp;w=whats_the_next_action#comm" title="Robin Scanlon">one comment</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '6935'); ?></span></p>
</div><span id="e6929"></span><div class="entry">
<h3>A small victory</h3>
	<p>I am very proud that as of today, this weblog has over 1000 readers for the RSS feed. Since the niche which I am targetting uses RSS as a tool for reading online content, I am very glad to be part of such a growing community. Here is the <a href="http://www.feedburner.com"  target='_blank'>Feedburner</a> stat to prove it<br />
<p style="text-align:center;"><img src="/images/wtnafeedburner.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
My plans are to be more active on this weblog and within the GTD-community. This certainly is a boost for me! Thank you my kind readers. I appreciate the attention.</p>
	<p>(Now don&#8217;t get me started on the subjective nature of these digits. Let me enjoy the moment!)</p>

  
 
<p class="info">19 07 05 - 15:35 - <a href="/pivot/entry.php?id=6929&amp;w=whats_the_next_action" title="19 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6929&amp;w=whats_the_next_action#comm" title="Fred Zelders, ryan">two comments</a> <?php echo get_editentrylink("Edit", '6929'); ?></span></p>
</div><span id="e6924"></span><div class="entry">
<h3>How to print the DIY planner in Europe</h3>
	<p>OK, yeah, I confess. I recently started to use the <a href="http://www.douglasjohnston.net/weblog/archives/2005/06/11/diyp2_hipsterpda/"  target='_blank'>DIY planner Hipster PDA version</a> from A Million Monkeys Typing. Just trying it out in conjuncture with my Moleskine to see where it leads me. So far, it&#8217;s been pretty handy. Making some notes during a short meeting with some direct NA&#8217;s on it, using it a shopping list in my wallet, jotting down some ideas on the bus. Again, it&#8217;s a tool to use <em>besides</em> something else for me. It is not <em>the</em> tool or the holy grail. <br />
What I found troubling about the downloadable templates, is they all use the American system for printsizes. OK, listen up: In Europe we don&#8217;t have indexcard sizes like 3&#215;5 inch. We use the A-system (or whatever it&#8217;s called) and we have indexcards in A7 size. Or A6, whatever you prefer. A7 equals to 74&#215;105 mm which comes to 2,9&#215;4,1 inch. So the templates are too big. I was too lazy to fire up Fireworks and sort out how to resize the templates, so I just changed the printersettings to print the PDF&#8217;s in a proper size. So to all Europeans who want to print the Hipster templates, here&#8217;s the trick:<br />
In Acrobat, go to your printersettings and open the printer properties. I have a HP deskjet 5150. Your own printersetting will differ from mine. First, make sure your papersize is set at &#8220;Indexcard (3&#215;5 inch)&#8221;. You might want to make a customsize of it. Then look for <strong>Resizing options</strong> in your settings and change them to <strong>85%</strong>. This will resize your template to 85% and it will fit on A7-index cards.<br />
Now I look forward to anyone changing the original png-files to A7 format as well. Or perhaps Doug Johnson will do this himself <img src='/extensions/emoticons/trillian/e_121.gif' alt=';-)' align='middle'/></p>

  
 
<p class="info">19 07 05 - 00:16 - <a href="/pivot/entry.php?id=6924&amp;w=whats_the_next_action" title="19 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6924&amp;w=whats_the_next_action#comm" title="m3">one comment</a> <?php echo get_editentrylink("Edit", '6924'); ?></span></p>
</div><span id="e6923"></span><div class="entry">
<h3>Quick tip for minor tasks in Outlook</h3>
	<p>I use the GTD toolbar, but sometimes the project I&#8217;m is so tiny or has a really short timeframe. Most of the time it&#8217;s a mini-project for an existing customer for which I&#8217;m already engaged in another project. When that task is in @Waiting for-mode, what I find myself doing most of the time is the following: Let&#8217;s say I send an email with a question. I put it in my @Waiting for list and during the Weekly Review (which, to be honest, happens every two weeks or so&#8230;I&#8217;m progressing!) I find it again. Or it pops up with a reminder to act on it. For instance, call or email if there is a reply underway. Whenever I call or email and I don&#8217;t get a proper respons, I <em>don&#8217;t</em> check the original item as &#8220;completed&#8221; but I keep it. I add date and time to the large textarea in the task, together with some small info like &#8220;called but was unavailable&#8221; or &#8220;sent mail regarding follow-up&#8221;. So I&#8217;ll always have this little mini-archive of this mini-project with me in my tasks. I don&#8217;t have to make a special projectfolder or anything. <br />
Whenever a clients calls and asks on the status of this project, I can just say &#8220;Well, I emailed you on so-and-so date, called you 5 days later, sent another email, so you better tell <em>me</em> buddy!&#8221; Well not that last bit, but you get the point right?</p>
	<p><em>Maybe it&#8217;s not GTD, but to me it&#8217;s trusted, keeps my head clean and infomration in place. And that&#8217;s the key right?</em></p>

  
 
<p class="info">18 07 05 - 23:46 - <a href="/pivot/entry.php?id=6923&amp;w=whats_the_next_action" title="18 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6923&amp;w=whats_the_next_action#comm" title="felicitas">one comment</a> <?php echo get_editentrylink("Edit", '6923'); ?></span></p>
</div><span id="e6920"></span><div class="entry">
<h3>Wired News: A Guide to Getting Things Done</h3>
	<p>Probably linked all over the GTD-blogosphere, but it&#8217;s a must read for a 101 on GTD. Wired&#8217;s <a href="http://www.wired.com/news/culture/0,1284,68110,00.html"  target='_blank'>A Guide to Getting Things Done</a><br />
With the already infamous question: <strong>So project management is a cult? You mean, like Waco?</strong></p>

  
 
<p class="info">12 07 05 - 15:34 - <a href="/pivot/entry.php?id=6920&amp;w=whats_the_next_action" title="12 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6920&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6920'); ?></span></p>
</div><span id="e6919"></span><div class="entry">
<h3>Inbox flooded</h3>
	<p>Heh&#8230;had a couple of days off and came into the office this morning. 381 unread mail, 21 reminders and a pile of analog mail. It&#8217;s gonna be Processing-day I guess&#8230;</p>

  
 
<p class="info">12 07 05 - 09:12 - <a href="/pivot/entry.php?id=6919&amp;w=whats_the_next_action" title="12 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6919&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6919'); ?></span></p>
</div><span id="e6918"></span><div class="entry">
<h3>The Post-it Portable Workspace</h3>
	<p>Today I have a day off. Just some quality-time for myself, doing some work in and around the house. Really nice actually. Not only did I get to do some great things in the house (hanging art on the wall, fixing a wine-cabinet) but I also had some time to catch up on old mail that was in my &#8220;Someday/Maybe&#8221; list. I am subscribed to the excellent <a href="http://groups.googlegroups.com/group/43Folders"  target='_blank'>43folders discussionlist</a> and today I found some true gems over there to check out, try out or think about. One project really struck me. Not because it is something that will work for me (it won&#8217;t) or that it is suitable for my kind of work (it isn&#8217;t) but just the sheer ingenuity and originality of it. It is called <a href="http://blog.warmfuzzy.com/index.php/2005/07/02/introducing-the-post-it-portable-workspace/"  title="PPWS" target='_blank'>The Post-it Portable Workspace</a> and it is courtesy of Tammy Cravit at Warmfuzzy.com (love that domainname!) To qoute her story on the how and what of the system: &#8220;<em>The basic idea of the PPW is simple: I took a series of plastic sheet protectors, which provide a nice surface for Post-it notes to adhere to. Inside them, I placed sheets of card stock (file folders, cut down to 8-1/2&#215;11) for rigidity, and I put the whole mess in a 3-ring binder.</em>&#8221;<br />
It looks excellent and I&#8217;m sure Tammy really gets things done with it.<br />
For me? I am trying the DIY Hipster PDA cards right now, see if it fits me. More on that in a later post!</p>

  
 
<p class="info">11 07 05 - 17:57 - <a href="/pivot/entry.php?id=6918&amp;w=whats_the_next_action" title="11 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6918&amp;w=whats_the_next_action#comm" title="Eric Mack, play pacific poker games for free">two comments</a> <?php echo get_editentrylink("Edit", '6918'); ?></span></p>
</div><span id="e6908"></span><div class="entry">
<h3>No comments possible</h3>
	<p>Some of you may have already noticed (thanks for the emails!) but it is not possible to comment on my stories right now. There are some memory-problems on the server or with the weblogsoftware (Pivot) that gives errors when you try to comment. So please do not write down big stories and click &#8220;Publish&#8221; because you are in for a dissapointment. I am looking into the issue with the Pivot-community and the author of the software so I hope it will be resolved in a short period of time.</p>
	<p><strong>Update: Comments are working again!</strong></p>

  
 
<p class="info">04 07 05 - 08:53 - <a href="/pivot/entry.php?id=6908&amp;w=whats_the_next_action" title="04 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6908&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6908'); ?></span></p>
</div><span id="e6907"></span><div class="entry">
<h3>Next Action Cards</h3>
	<p><img src="/images/nac.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I received a nice email from Charles Rubin. He created <a href="http://www.nextactioncards.com/"  target='_blank'>Next Action Cards</a>. These are preprinted cards to either capture items on before moving to your regular system, or a full system itself for GTD management. He had these made up for himself and some friends, and then made them available for everyone. Next Action Cards are sold in lots of 250, at a cost of $10.95. On his website, Charles gives some samples on how you can use the cards. <br />
I must say, it does remind me very much of the <a href="http://www.43folders.com/2004/09/introducing_the.html"  target='_blank'>Hipster PDA project</a> over at 43Folders and <a href="http://www.douglasjohnston.net/weblog/archives/2005/06/11/diyp2_hipsterpda/"  target='_blank'>Million Monkeys Typing</a> So I am quite curious how it compares to each other. Especially since the Hipster PDA is free (as in donateware) and looks a bit nicer, which also counts in my opinion.</p>

  
 
<p class="info">03 07 05 - 22:13 - <a href="/pivot/entry.php?id=6907&amp;w=whats_the_next_action" title="03 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6907&amp;w=whats_the_next_action#comm" title="Steve, Usara Howden">two comments</a> <?php echo get_editentrylink("Edit", '6907'); ?></span></p>
</div><span id="e6904"></span><div class="entry">
<h3>What happened?</h3>
	<p>Ooooh..yeahhhh&#8230;it has been quite some time since I&#8217;ve been here. Sorry for that. How come? Didn&#8217;t have anything to say? Well no&#8230;I got a lot on my mind, but not all is good for this blog. But the last couple of weeks have been more hectic that I could imagine. Moving to new city and basically changing the way you lived for that last 7 years has been quite a shock for me. More than I imagined. My whole rhythm of the day has changed, and it had it&#8217;s effects on lots of things. I mean, when do I read my web feeds now I don&#8217;t have to commute 4 hours a day by train but 40 minutes by bike? When I get home, I just chill for an hour, doing nothing. That&#8217;s nice you know&#8230;<br />
What struck me the most was the way I totally and utterly fell of the GTD-bandwagon. Man, what a bump! Suddenly, my Inbox was flooded with 200+ messages, phone calls all around, projects not closed properly, meetings in and out, changing agenda&#8217;s, forgotten tasks, reminders piling on each other. I just didn&#8217;t have the energy to do anything about it. I thought to myself:&#8221;Aaarrgghhh, forget that whole GTD-religion thing, I just do it my own way&#8230;&#8221;</p>
	<p>Yeah.</p>
	<p>That would work now would it? I guess not! Once you <em>know</em> there is a better way to organize your life and your work, you just can&#8217;t go back to the old way. Because this little voice in the back of your head keeps whispering &#8220;Frank, please, c&#8217;mon, dedicate a slot of the day for your Inbox, you&#8217;ll feel better!&#8221;<br />
Yesterday I had that day. And did I feel great about myself! I ploughed through my Inbox. I had some time on my hands and I just started. Not to process and organize <em>everything</em> but to <strong><em>do</em></strong> what had to be done <strong><em>right now</em></strong>, even if it took me an half an hour to do or 10 seconds. I just did it. And after that, organized references, deleted old discussions and newsletters and made some tasks in my agenda. And dropped some Next Actions in my task list.<br />
What caused this? I only have one explanation and that is the Weekly review. I wrote about it in an earlier post (Which I can&#8217;t find because my archive is broken&#8230;sorry!) and it still is a major issue with me. I just can&#8217;t seem to find the time to really <em>reallyreally</em> have a weekly review for everything I do. For some reason there is always something that&#8217;s more important. May it be a co-worker who is behind on his project and needs my assistance, a crisis meeting, a phone call from a customer, anything. So scheduling a weekly review seems like a no-go for me.<br />
Sometimes, I briefly check my task list and I see some tasks that need to be done. Most of the time, these are tasks that take more than a couple of minutes. And most of the time, these are tasks that are less important than the phone call from that client who needs some follow-up on project XYZ. So it just doesn&#8217;t get done. Whole day long, I am engaged in tasks that need to be done <em>that day</em>. Not the day after, not next week, no, <em>today</em>. And everything else just gets pushed forward in time. Why do these tasks need to be done today? Because they hold up other projects that pop up, because the client needs an answer right away, because otherwise, it takes too long to answer to a client and they get pissed.</p>
	<p>I don&#8217;t know, sometimes it all gets really frustrating when things don&#8217;t work out the way you want them to. You get into a negative spiral and the only way to get out of it is to look up and really really try hard. And just do it. No matter what the consequences are.</p>
	<p>I am still trying, and I know there must be some way for me to really get organized and think only happy thoughts. And I will succeed in it. Someday/Maybe&#8230;</p>

  
 
<p class="info">01 07 05 - 00:19 - <a href="/pivot/entry.php?id=6904&amp;w=whats_the_next_action" title="01 Jul '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6904&amp;w=whats_the_next_action#comm" title="Omaha, Punkey, darrell">three comments</a> <?php echo get_editentrylink("Edit", '6904'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>