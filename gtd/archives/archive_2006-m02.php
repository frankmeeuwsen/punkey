<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e7012"></span><div class="entry">
<h3>Pocket RSS feeds</h3>
OK, I am writing this on my iPaq while reading my webfeeds on the same device. I already feel I have acquired another Inbox because I use PocketRSS. sorry, no links, that's a drag on this tiny machine. But I read my favorite feeds and I see a lot of interesting articles. but clicking through makes me go to the normal website instead of a  PDA or mobile version, which could be less heavy on graphics and other fluff. That's to understand, since I use the normal webfeed. So is there a site that offers Mobile RSS feeds which click through to a mobile version of the site? any thoughts are welcome in the comments... 
 
<p class="info">26 02 06 - 08:52 - <a href="/pivot/entry.php?id=7012&amp;w=whats_the_next_action" title="26 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7012&amp;w=whats_the_next_action#comm" title="Frank, blund">four comments</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '7012'); ?></span></p>
</div><span id="e7010"></span><div class="entry">
<h3>How to make copy and pasting just a little easier</h3>
	<p><img src="/images/ptext.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Don&#8217;t you hate that? You want to copy something from a webpage or a formatted document into another document. Biggest issue (on a Windows machine) is that all formatting comes with the copying. So you end up using a reroute like notepad where you temporarily paste the text, re-copy it and paste it in the proper document. Fear no more! The answer is here! And we shall call it <a href="http://www.stevemiller.net/puretext/"  target='_blank'>Pure Text</a>! Just download this 13Kb standalone executable and it will nest in your systemtray. Now, the fun starts. Copy a chunk of heavilly formatted text. But! Instead of using Control-V to paste it, use Windows-V. It&#8217;s magic! It&#8217;s incredible! All text is pasted but without the formatting! A little timesaver, nice and warm in your systemtray. Wouldn&#8217;t it be great if everything was as simple?</p>

  
 
<p class="info">21 02 06 - 16:54 - <a href="/pivot/entry.php?id=7010&amp;w=whats_the_next_action" title="21 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7010&amp;w=whats_the_next_action#comm" title="Lorie, Doug, Stewart">three comments</a> <?php echo get_editentrylink("Edit", '7010'); ?></span></p>
</div><span id="e7009"></span><div class="entry">
<h3>I need a new feature in Gmail!</h3>
	<p><img src="/images/label.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I use <a href="http://www.gmail.com"  target='_blank'>Gmail</a> more and more as my primary emailapp for everything I do besides my work (like <a href="http://www.aboutblank.nl"  target='_blank'>a weblogmagazine</a> , <a href="http://www.dutchbloggies.nl"  target='_blank'>weblogawards</a>, <a href="http://www.eventblogs.nl"  target='_blank'>eventblogs</a>). Since I mail a lot of requests that need some kind of answer or followup I made a label called @waiting in Gmail. This is where I put all my emails that I have send and need an answer from the receiver. Every week or so (..cough&#8230;Weekly review checkup&#8230;cough&#8230;) I check the list and see what&#8217;s still out in the open. The great thing about Gmail is that any answer that arrives drops in your mailbox with the label attached to it. So I see immediately the emails I have been waiting for and I can easily remove the label if necessary.<br />
But one thing that Gmail misses is an easy way <em>to label a message when sending it</em>. Now I need to go to &#8220;Sent Mail&#8221; and manually label the message. No big deal, but when I&#8217;m on a roll with email (sending 20-30 messages at a time) some might slip through the cracks. So it would be great if you could label a message directly when you are in &#8220;writing-mode&#8221; in Gmail. Not when you&#8217;re in &#8220;I-just-sent-the-mail-let&#8217;s-relax-mode&#8221;. So maybe someone from Google is reading this blog and might see this desparate plea for more comfort and ease-of-use in a otherwise excellent mailapp!</p>

  
 
<p class="info">18 02 06 - 19:42 - <a href="/pivot/entry.php?id=7009&amp;w=whats_the_next_action" title="18 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7009&amp;w=whats_the_next_action#comm" title="Niels, Marshall Sontag, Frank, Dan, dl">six comments</a> <?php echo get_editentrylink("Edit", '7009'); ?></span></p>
</div><span id="e7007"></span><div class="entry">
<h3>This blog is listed on Listible</h3>
	<p>I never heard of it but will definitely check it out. <a href="http://www.listible.com/"  target='_blank'>Listible</a> is a (beta) service where you can make your own Best-Of lists. I am very honoured to be in <a href="http://www.listible.com/list/the-best-gtd-resources"  target='_blank'>the top 10 of Best GTD resources</a> on the web according to this list. I will try harder to get into the top 5 <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>

  
 
<p class="info">17 02 06 - 23:05 - <a href="/pivot/entry.php?id=7007&amp;w=whats_the_next_action" title="17 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7007&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7007'); ?></span></p>
</div><span id="e7006"></span><div class="entry">
<h3>Comments are back again!</h3>
	<p>Oh yeah, it took some time for me to really find a moment to dig in Pivot, my weblogsoftware, and find the error in the comments. It turns out there is a new notation in the version I am using for including comments. This is not documented very well so I had to turn to the forum and search, try, post, try again. But now they work! I would like to thank those who have posted some comments by email. I will post them in the extended entry of this post. Ofcourse, you are free to comment here again!</p>

  
<a href="/pivot/entry.php?id=7006&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">17 02 06 - 22:16 - <a href="/pivot/entry.php?id=7006&amp;w=whats_the_next_action" title="17 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7006&amp;w=whats_the_next_action#comm" title="Manny Hernandez">one comment</a> <?php echo get_editentrylink("Edit", '7006'); ?></span></p>
</div><span id="e7004"></span><div class="entry">
<h3>Testing my new ipaq</h3>
	<p>Oh yeah&#8230;my new gadget has arrived. The HP Ipaq 6515. With windows mobile 2003, outlook synching and integrated phone functionality. Some of the features can be interesting to get things done. This entry is written on my iPaq in a cafetaria for instance. I just found the Transcriber function to enter data. Looks promising and already a lot better than my earlier PDA&#8217;s. Also my GPRS subscription gives me complete acces to my Backpack pages and my mobile Gmail. I am online 24/7 now and wouldn&#8217;t want it any other way&#8230;</p>

  
 
<p class="info">12 02 06 - 11:10 - <a href="/pivot/entry.php?id=7004&amp;w=whats_the_next_action" title="12 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7004&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7004'); ?></span></p>
</div><span id="e7003"></span><div class="entry">
<h3>Comments broken again!</h3>
<p>I'm sorry but I just found out the comments to this blog are broken again! And I don't have the time to fix this right now since I am very busy with my work and the organisation the Dutch Weblogawards (talking about the use of GTD and Backpack....sjeeeszzz)</p><p>If you have any comments on any of the articles, please email me on punkey at gmail dot com (fight spam, figure this out yourself) and I will make sure they get on this site. </p><p>I apologize for the inconvenience!</p> 
 
<p class="info">10 02 06 - 08:38 - <a href="/pivot/entry.php?id=7003&amp;w=whats_the_next_action" title="10 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7003&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7003'); ?></span></p>
</div><span id="e7002"></span><div class="entry">
<h3>Using Backpack and GTD, continued</h3>
<p>Since my last article on <a target="_blank" href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a> I did some minor tweaking of my use with it which I would like to share with you. If you don't know what Backpack is, I advice you to take a look at <a href="http://punkey.com/pivot/entry.php?id=6971"  target='_blank'>this article</a> or at <a target="_blank" href="http://backpackit.com/?referrer=BPF9BJ9">Backpack's site</a>. Ofcourse you can try it for free for 30 days!</p><p>What I changed first is my accountplan. I had the Basic plan which allowed up to 25 pages but I needed some breathing room for more pages. Because my plan was to put every project in my personal life in my Backpack. Well, at least every project that allowed multiple steps, next actions or had some different contexts. So I changed my plan to Plus, which allows 100 pages. I now have around 40 pages on average in my Backpack. But what changed the most was my use of <em><strong>the tags</strong></em> in Backpack.</p> 
<a href="/pivot/entry.php?id=7002&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">05 02 06 - 23:14 - <a href="/pivot/entry.php?id=7002&amp;w=whats_the_next_action" title="05 Feb '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7002&amp;w=whats_the_next_action#comm" title="John Ratcliffe-Lee, Forrest">two comments</a> <?php echo get_editentrylink("Edit", '7002'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>