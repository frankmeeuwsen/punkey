<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e1824"></span><div class="entry">
<h3>The loop in collecting, processing and organizing</h3>
	<p>I found a major flaw in my &#8220;system&#8221;. Since it&#8217;s not really a system yet, it&#8217;s in quotes. But what I find myself doing most of the time is Collecting, processing and organizing. Especially readingmaterial. With 100+ RSS feeds, newsletters in both print and mail and new, interesting books coming out every week I just have too much interesting stuff to read. My &#8220;To read list&#8221; expands every weekend, since I don&#8217;t travel 4 hours a day when I actually had time to read. What to do? 
	<ul>
		<li><em>*Collect less material. Unsubscribe to newsletters and RSS feeds*</em>. Pro: gives me less headaches on everything that comes in. Con: I might miss out on something (job- or personal related)</li>
		<li><em>*Take a dedicated timeslot to read*</em>. Pro: Gives me peace of mind to actually read something. Con: Work is soooo hectic and at home I have so much other stuff to do, I&#8217;m afraid I might drop the dedicated timeslot sooner than later.</li>
		<li><em>*Move further away from work to have time to read*</em>. Are you kidding me?</li>
	</ul></p>
	<p>Any thoughts from my valued readers?</p>

  
 
<p class="info">30 05 05 - 09:42 - <a href="/pivot/entry.php?id=1824&amp;w=whats_the_next_action" title="30 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1824&amp;w=whats_the_next_action#comm" title="Marleen, N, Kjjsn">four comments</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '1824'); ?></span></p>
</div><span id="e1813"></span><div class="entry">
<h3>del.icio.us bookmarks</h3>
	<p><img src="/images/delicious.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I decided to add <a href="http://blog.del.icio.us/blog/2005/05/bookmark_this.html"  target='_blank'>a little service</a> for my readers. From now on, you can bookmark every individual post directly in del.icio.us with a little link under the post that says (surprise surprise) &#8220;Bookmark this post&#8221;. Is this useful for you? Need more widgets like these? I&#8217;m here for you, my precious reader!</p>

  
 
<p class="info">23 05 05 - 22:01 - <a href="/pivot/entry.php?id=1813&amp;w=whats_the_next_action" title="23 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1813&amp;w=whats_the_next_action#comm" title="Hashim, Jacob Rask, N">three comments</a> <?php echo get_editentrylink("Edit", '1813'); ?></span></p>
</div><span id="e1808"></span><div class="entry">
<h3>Has Google changed? Searchqueries disappear...</h3>
	<p>Is it just me, or has Google changed the last couple of days for Firefox? It seems that whenever I type in a query in the Google searchbox, the resultpage doesn&#8217;t show the query in the searchbox anymore. Also, Google sometimes produces some <em>really</em> weird queries. For instance, I just searched for &#8220;Synchronizing Outlook offline laptop exchange&#8221; and Google gave me this: &#8220;Results 1-30 for [...] synchronizing Outlook offline laptop Ec %%%exchange&#8221;<br />
Pretty weird huh! Anyone else have these problems with Google on Firefox?</p>

  
 
<p class="info">19 05 05 - 13:10 - <a href="/pivot/entry.php?id=1808&amp;w=whats_the_next_action" title="19 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1808&amp;w=whats_the_next_action#comm" title="Tait, brett, N, Geff, Jeff, Punkey">eight comments</a> <?php echo get_editentrylink("Edit", '1808'); ?></span></p>
</div><span id="e1807"></span><div class="entry">
<h3>Zoom your textarea!</h3>
	<p><img src="/images/zoomtextarea.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Taken shamelessly from the <a href="http://www.lifehacker.com/software/productivity/download-of-the-day-zoom-textarea-firefox-user-script-104004.php"  target='_blank'>excellent Lifehacker-blog</a>:<br />
The Zoom Textarea user script lets you enlarge textareas on any web page for more glorious room to type &#8211; so handy for writing forum posts, weblog comments or posts, and web-based email.</p>
	<p>The Zoom Textarea user script requires the Firefox Greasemonkey extension (at least version 0.3.) If you&#8217;re a Firefox user without Greasemonkey, here&#8217;s how to install the script:</p>
	<ol>
		<li>In Firefox, click &#8220;Install Greasemonkey&#8221; from <a href="http://greasemonkey.mozdev.org/"  target='_blank'>here</a>. Be sure to allow &#8220;greasemonkey.mozdev.org&#8221; to install software in your browser.</li>
		<li>Restart Firefox.</li>
		<li>Right click on this link: <a href="http://diveintogreasemonkey.org/download/zoomtextarea.user.js"  target='_blank'>zoomtextarea.user.js</a>. </li>
		<li>Click &#8220;Install User Script.&#8221; Press OK.</li>
		<li>Refresh any page with a text area on it, and you will see a zoom in and zoom out button above it.</li>
	</ol>
	<p>If you spend a good part of your day typing into web pages, Zoom Textarea will quickly become indispensable for your writable web.<br />
<a href="http://diveintogreasemonkey.org/casestudy/zoomtextarea.html"  target='_blank'>Case study: Zoom Textarea</a> [Dive into Greasemonkey]</p>
	<p><strong>Update:</strong> It also makes the font in the textarea itself bigger, so the effect is not that great&#8230;.hmmm..</p>

  
 
<p class="info">19 05 05 - 08:29 - <a href="/pivot/entry.php?id=1807&amp;w=whats_the_next_action" title="19 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1807&amp;w=whats_the_next_action#comm" title="Marcus">one comment</a> <?php echo get_editentrylink("Edit", '1807'); ?></span></p>
</div><span id="e1806"></span><div class="entry">
<h3>Workflow strategies</h3>
	<p>Chris Murtland posted an interested article last month about <a href="http://www.murtworld.com/2005/04/revolving-workflow-strategies.php"  target='_blank'>resolving workflow strategies</a>. I didn&#8217;t get to read it, but fortunately it is in my Reading List in OnFolio (yeah yeah yeah I know&#8230;) so it popped up this afternoon. I discovered one fine benefit of living closer to work: You get to spend time reading at home which gives me a more relaxed way of reading and thinking about the stuff I read. I already noticed a different approach to my reading pattern this afternoon. But that&#8217;s for another post. <br />
Back to Chris&#8217; article about different approaches to his daily workflow. In addition to David Allen&#8217;s threefold model for evaluating daily work, he introduces some of his own models. They are all very recognizable as I use them quite often myself. Especially the &#8220;big chunks of time on certain projects&#8221; and &#8220;newest first&#8221; on &#8220;panic-days&#8221;. I would like to add another strategy and that&#8217;s the one where I most need to talk with my projectteams. I work on different projects at the same time with different teams (sometimes the same person is on more than one team) and I try to touch base with the most urgent projects in one chunk of time, let&#8217;s say a day. So instead of &#8220;big chunks of time on certain projects&#8221; I try to do &#8220;divided chunks of time on different projects&#8221;. I try to meet with all the folks in one day or part of the day. I focus on a succesfull outcome of the project during those meetings, get the most urgent matters on my agenda and create Next Actions of those matters. These get processed, organized and reviewed. And ofcourse done on the proper moment. Sometimes right away (call a client for a certain RGB-color) sometimes they get on my calendar and sometimes they end up in my Tasklist under the appropriate context and project.<br />
Do some actions fall between the cracks. Sure, it happens. But less and less. Not everything is done just in the right time, sometimes, some actions tend to get &#8220;stale&#8221; and actually <em>not done</em> by me. Why is that? Because the action is to diverse? Because the context is out of order? Because it really <em>isn&#8217;t</em> a next action? Something to think about for me on my bike to work tomorrow morning&#8230;</p>

  
 
<p class="info">18 05 05 - 22:24 - <a href="/pivot/entry.php?id=1806&amp;w=whats_the_next_action" title="18 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1806&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1806'); ?></span></p>
</div><span id="e1805"></span><div class="entry">
<h3>GTD flowchart mashup</h3>
	<p>Interesting. Just before I decided to turn off the PC for today I checked the <a href="http://www.flickr.com/groups/gettingthingsdone/pool/"  target='_blank'>GTD-Photopool at Flickr.com</a> [<a href="http://www.flickr.com/groups/gettingthingsdone/pool/feed/?format=rss_200"  target='_blank'>RSS</a>] and saw some nice diagram-mashups of the original GTD flowchart. They are made by KoolPal and are <a href="http://www.flickr.com/photos/koolpal/8872491/"  target='_blank'>worth looking over</a> and having a thought about it. Added it to my OnFolio Inbox for later reviewing!</p>
<p style="text-align:center;"><img src="/images/koolpal.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
	<p>Euhmmm&#8230;yeah&#8230;I got a discount at OnFolio regarding <a href="http://www.punkey.com/pivot/entry.php?id=1804"  target='_blank'>my last post</a> (thanks Sebastian) since I was a betatester. So I got the Pro-version for $ 29,95. We&#8217;ll see what Newsgator comes up with and if it beats OnFolio. &#8216;Cause it still is a good application! Both FeedDemon and OnFolio. But hey, they beat me with their offer. Man, I&#8217;m so easy sometimes&#8230;</p>

  
 
<p class="info">18 05 05 - 00:08 - <a href="/pivot/entry.php?id=1805&amp;w=whats_the_next_action" title="18 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1805&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1805'); ?></span></p>
</div><span id="e1804"></span><div class="entry">
<h3>Choices choices choices....</h3>
	<p style="text-align:center;"><img src="/images/onfoliobetaexpired.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
So..I&#8217;ve moved to a new city, basically eliminating my need to read news and websites offline on my laptop. I am 15 minutes away from work on my bike. And then three things happened today. Well, two,but hey:<br />
1. My Beta version of <a href="http://www.onfolio.com"  target='_blank'>Onfolio</a> on my dekstop machine expired, so it&#8217;s basically crippled and I can&#8217;t really do anything with it.<br />
2. The downloaded 2.0 version of OnFolio on my laptop is nagging to purchase a version (29 dollar version? Nah, I want the 99 bucks-pro-version! Check <a href="http://www.onfolio.com/product/compareEditions.cfm"  target='_blank'>the comparison chart</a>)<br />
3. Newsgator and Feeddemon <a href="http://nick.typepad.com/blog/2005/05/newsgator_acqui.html"  target='_blank'>declare their marriage</a>. I own a version of <a href="http://www.feeddemon.com"  target='_blank'>FeedDemon</a> and they will give me a <a href="http://www.newsgator.com/business.aspx"  target='_blank'>business account</a> of Newsgator within the next month or so as a wedding gift. Nice!</p>
	<p>Soooowwww&#8230;.hmmm&#8230;..yeeeaah&#8230;.where does that leave me with my big review of OnFolio, Evernote and so on? <a href="http://www.punkey.com/pivot/entry.php?id=1726"  target='_blank'>The Nine Inboxes problem</a>? Starting all over again? Man&#8230;why do I need to make these choices? Life is hard for an information junkie like me&#8230;</p>
	<p>I think I just sit here, staring at my screen and wallow in my sorrows&#8230;.sigh&#8230;</p>

  
 
<p class="info">17 05 05 - 21:29 - <a href="/pivot/entry.php?id=1804&amp;w=whats_the_next_action" title="17 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1804&amp;w=whats_the_next_action#comm" title="Sebastian Gard">one comment</a> <?php echo get_editentrylink("Edit", '1804'); ?></span></p>
</div><span id="e1803"></span><div class="entry">
<h3>Three little time-savers!</h3>
	<p>Hey all! We&#8217;re halfway through the moving process from the south of the Netherlands to the middle. That is, we&#8217;ve painted the new house (check <a href="http://www.flickr.com/photos/punkey/sets/286033/"  target='_blank'>some pics here</a>) and recoated the floor (is that how you call that?) This Friday, we&#8217;ll be moving our stuff from house A to house B and to put first things first: The network cable and Wifi-router are on top of the stack!<br />
Anyway, I would like to talk a little about three time-saving plug ins/apps I am using right now to make my life on the web a wee-little easier.</p>
	<p><strong>Targetalert</strong><br />
Don&#8217;t you hate it when you click a link in Firefox and darn&#8230;it&#8217;s a PDF! Or a mailto-link. Well fear no more! Because <a href="http://www.bolinfest.com/targetalert/"  target='_blank'>Targetalert</a> let&#8217;s you tweak these links <br />
<p style="text-align:center;"><img src="/images/targetalert.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
Now you can see beforehand if the link is for instance a PDF-file. If so, make sure you have installed&#8230;</p>
	<p><strong>PDF Download</strong><br />
This <a href="https://do-not-add.mozilla.org/extensions/moreinfo.php?id=636&#38;vid=2225"  target='_blank'>nifty little gem</a> gives you a couple of options: Download the PDF, view it in a new tab or view it as HTML. The last option is a bit buggy, but the rest works fine!<br />
<p style="text-align:center;"><img src="/images/pdfdownload.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>

	<p><strong>Foldercache</strong><br />
<a href="http://www.nesoft.org/foldercache.shtml"  target='_blank'>Foldercache</a> is a nice shareware-tool I actually bought because it saves me a lot of headaches browsing to the right folder on our network. Let me explain: We work on a lot of projects which all have their own client and project folder on our network. I work on 5-10 projects a day (depending the size of projects). During a day I have to go to that particular network folder a lot of times. But I also would like to check some music or find some programs I downloaded. With Foldercache, you will have an extra icon in your system tray which gives you a popupscreen of your last visited folders and your favorite folders. The application has a lot of tweaking power for the way the folders are shown, which drives should be in- or excluded when offline (read: off the network) and a deletable history of visited folders. It sure is a time saver for me to just add a new project folder to the list and delete old ones and I&#8217;m ready to go.<br />
Also, it nests itself in various Windows-dialogs, so Saving or Opening in the right folder is a breeze!<br />
<p style="text-align:center;"><img src="/images/foldercache.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>

	<p>I hope these little timesavers will help you do your work faster. Do you have some interesting extensions or apps you use? Let me know in the comments!</p>

  
 
<p class="info">10 05 05 - 11:36 - <a href="/pivot/entry.php?id=1803&amp;w=whats_the_next_action" title="10 May '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1803&amp;w=whats_the_next_action#comm" title="Robert, N">two comments</a> <?php echo get_editentrylink("Edit", '1803'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ol>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7081">Mindmanager, an excellent GTD tool? Win free licenses!</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7080">Scrybe is the killer GTD app?</a>
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7069">6 ways to run an effective meeting </a>
<li><a href="http://punkey.com/pivot/entry.php?id=7077">Netvibes GTD tab</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7021">The 5 reasons why The Weekly Review is difficult</a>
<li><a href="http://punkey.com/pivot/entry.php?id=1726">Still digging Evernote?</a>
</ol>
Made possible with <a href="http://www.haveamint.com">Mint</a><br>
<p align="center"><img src="/images/mint-80x15.gif" width="80" height="15" align="middle" border="0" /><p><br />
<br />
</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=1824&amp;w=whats_the_next_action#kjjsn-0611131512' title='13 11 2006 - 15:12' ><b>Kjjsn</b></a> (The loop in colle&hellip;): Hello this is pretty useful site.I l&hellip;<br />
<a href='/pivot/entry.php?id=7085&amp;w=whats_the_next_action#frank_meeuwsen-0611090741' title='09 11 2006 - 07:41' ><b>Frank Meeuwsen</b></a> (Hack: Netvibes wi&hellip;): Franck: Thanks for the update. The s&hellip;<br />
<a href='/pivot/entry.php?id=7085&amp;w=whats_the_next_action#franck_mahon-0611090032' title='09 11 2006 - 00:32' ><b>Franck Mahon</b></a> (Hack: Netvibes wi&hellip;): Thanks for sharing this with your vi&hellip;<br />
<a href='/pivot/entry.php?id=7084&amp;w=whats_the_next_action#martijn-0611081550' title='08 11 2006 - 15:50' ><b>Martijn</b></a> (We're moving serv&hellip;): Succes with it! Will you be down, te&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#scot_herrick-0611080007' title='08 11 2006 - 00:07' ><b>Scot herrick</b></a> (We have our winne&hellip;): This was a fun contest. It&#8217;s always i&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#frank_johnson-0611072353' title='07 11 2006 - 23:53' ><b>Frank Johnson</b></a> (Netvibes GTD tab): Douglas: In case you check back, I u&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#steve_newson-0611041127' title='04 11 2006 - 11:27' ><b>Steve Newson</b></a> (We have our winne&hellip;): Thank you so much for picking me as &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#luciano_passuello-0611032057' title='03 11 2006 - 20:57' ><b>Luciano Passuello&hellip;</b></a> (We have our winne&hellip;): Just as Bill, I would like to thank &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#bill_reichart-0611030119' title='03 11 2006 - 01:19' ><b>Bill Reichart</b></a> (We have our winne&hellip;): Thanks for picking me as a winner.  &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#frank_meeuwsen-0611022209' title='02 11 2006 - 22:09' ><b>Frank Meeuwsen</b></a> (We have our winne&hellip;): OK, I found out when you use IE you &hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>