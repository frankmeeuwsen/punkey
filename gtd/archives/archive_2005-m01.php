<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e1669"></span><div class="entry">
<h3>Lifeposter &gt; @Reference</h3>
	<p style="text-align:center;"><img src="/images/lifeposter.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
This is a great idea for someones birthday or any other special occassion: <a href="http://www.mikematas.com/blog/2005/01/how-to-make-life-poster.html"  target='_blank'>Make a life poster</a> with your own photo collection, but perhaps you can also add some stockphoto&#8217;s or other online-found pictures. I put this is my @Reference map! </p>
	<p>[Via <a href="http://www.lifehacker.com/"  target='_blank'>Lifehacker</a>, the excellent new blog from the <a href="http://www.gawker.com/"  target='_blank'>Gawker-imperium</a>]</p>

  
 
<p class="info">31 01 05 - 22:07 - <a href="/pivot/entry.php?id=1669&amp;w=whats_the_next_action" title="31 Jan '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1669&amp;w=whats_the_next_action#comm" title="Arno">one comment</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '1669'); ?></span></p>
</div><span id="e1664"></span><div class="entry">
<h3>GTD wiki</h3>
	<p>You  might have already seen it floating around, but just to be complete: Jeff Sandquist started a <a href="http://wiki.jeffsandquist.com/default.aspx/GTD/HomePage.html"  target='_blank'>GTD Wiki</a> and I must say I am very proud to be in the initial list of GTD weblogs. Just as proud as I am to be on the introduced on the <a href="http://www.officezealot.com/gtd"  target='_blank'>OfficeZealot list of GTD-weblogs</a>. Which I forgot to mention earlier this week. So here&#8217;s my good deed for this week <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>
	<p>One more question: I feel my english writing is not up to par with the way I speak it. Since I&#8217;m Dutch, my English sounds more American (hey, blame the TV!) but now that I am writing in English I&#8217;ve noticed I can&#8217;t express myself articulate enough the way I could do it in Dutch. <br />
<em>How do you, as a reader, experience this? Do you see holes in my thoughts that need further explanation? Is my writing clear? So I make sense? I hope to learn from your comments!</em></p>

  
 
<p class="info">30 01 05 - 21:15 - <a href="/pivot/entry.php?id=1664&amp;w=whats_the_next_action" title="30 Jan '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1664&amp;w=whats_the_next_action#comm" title="Bob, N">two comments</a> <?php echo get_editentrylink("Edit", '1664'); ?></span></p>
</div><span id="e1662"></span><div class="entry">
<h3>Sunday GTD thoughts</h3>
	<p>The last couple of weeks I have been extremely busy with a rather large internetproject at my company. As a projectmanager, I am responsible for a projectteam with a minimum of 4 people and a pool of people  changing each week. This asks for a lot of energy and gives me little to no time to work on my GTD skills. Especially those in my personal life.This sunday, my girlfriend was out of town and I was home alone. No appointments, no mandatory to-do&#8217;s. So I decided to pick up a lot of little things that have been lying around the house. Reading magazine articles as old as november last year, checking some weblogs, listening to podcasts. But also fixing some stuff in and around the house, cleaning up. <br />
Right now I am lying at the couch, with my laptop. Just relaxing, chilling. I still have a lot of things to do in the house, but I am in the knowledge that they will get done. I did do the most urgent jobs, the rest will come in time.<br />
Did I use any software of lists? Well, not really. I just looked around the house, decided what to do and then just did it.<br />
Is this bad GTD? I&#8217;m not sure, since I did these tasks focusing on the outcome, thinking about what I want to accomplish and looked forward to the little rewards I would give myself when a task is finished (getting a nice sandwich, checking some online games, watching an old episode of The Apprentic. Yeah, my life is simple <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/>)<br />
Maybe the whole GTD is not always following the right path the book describes, exactly doing what David tells you to do (brrrrr&#8230;.) but it gives you some handles on how to look at the work that lies ahead. It sets the stage and lets you make the play. Something like that. How do you think about those subjects? Please let me know in the comments</p>

  
 
<p class="info">30 01 05 - 20:24 - <a href="/pivot/entry.php?id=1662&amp;w=whats_the_next_action" title="30 Jan '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1662&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1662'); ?></span></p>
</div><span id="e1647"></span><div class="entry">
<h3>The best of the best in Freeware</h3>
	<p>The Pricelessware list is a compilation of software collected through a yearly vote by the participants of the <a href="http://groups.google.nl/groups?hl=nl&#38;lr=&#38;client=firefox-a&#38;rls=org.mozilla:en-US:official&#38;group=alt.comp.freeware"  target='_blank'>alt.comp.freeware</a> newsgroup.  It is a list of what people have voted as &#8220;the best of the best in Freeware&#8221;. It has a nice list of free <a href="http://www.pricelessware.org/thelist/org.htm"  target='_blank'>organizer software</a> as well as <a href="http://www.pricelessware.org/thelist/txt.htm"  target='_blank'>texteditors</a> and <a href="http://www.pricelessware.org/thelist/biz.htm"  target='_blank'>business software</a>. Worth a look if you ask me!</p>

  
 
<p class="info">23 01 05 - 18:55 - <a href="/pivot/entry.php?id=1647&amp;w=whats_the_next_action" title="23 Jan '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1647&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1647'); ?></span></p>
</div><span id="e1644"></span><div class="entry">
<h3>Using GTD, what&#039;s the lesson?</h3>
	<p>I started using the GTD methodology in late october of last year. I was blown away by David&#8217;s book and felt that this was something I could really use in my personal and professional life. After a while I figured I could help other people by writing about my experiences with this method. So this weblog started on dec. 2nd. I realised that I would be on holiday 10 days later but hey! I can pick it up after I get home right? Wrong! I had three amazing weeks, not thinking about lists, next actions, ticklerfiles and brainstorming. Just chilling on the beach and in the mountains, doing only what we want to do. So when I came home and had to go to work, I already had a slight feeling that picking up GTD the way I planned to would be that easy. I would like to talk a bit how I started using GTD, what I think went wrong (before I went on a holiday!) and what my plans are for the coming months. Yes, I have been inspired by the amazing posts of Merlin over at <a href="http://www.43folders.com"  target='_blank'>43folders.com</a> and yes, some observations are very similar. It shows me that I am not alone in this and help is at hand when you go online. </p>
	<h2>How did I start?</h2>
	<p>If I remember correctly, somewhere in september/october I read some weblogposts about GTD. Especially the ones from <a href="http://www.43folders.com/2004/09/getting_started.html"  target='_blank'>Merlin</a> and <a href="http://www.dashes.com/anil/2004/09/20/meme_exploding"  target='_blank'>Anil Dash</a>. All within the same week. I thought to myself: &#8220;this must really be something, let&#8217;s check it out&#8221;. After buying the book, I immediately started implementing the system. That&#8217;s where the first flaw (in my opinion) started. I didn&#8217;t take the time, <em>really</em> take the time, to fully implement the system. For instance, I didn&#8217;t collect all the ongoing actions, projects and open ends in to one big box. No, what I did was cleaning my Inbox into some tasks and to do&#8217;s and hoped everything started to fall in place. <a href="http://www.punkey.com/pivot/entry.php?id=1578"  target='_blank'>Collecting, Processing and Organizing</a> at once&#8230;.I run a lot of projects, both big and small, and I couldn&#8217;t help it. I didn&#8217;t give myself enough free time at work to really start the system. The result: Still a lot of papers, open ends, different tasks and reminders all over the place. <br />
Also I didn&#8217;t invest in hardware (yet). I don&#8217;t have a good ticklerfile, archive cabinet or other hardware that is recommended in the book. The reasons are multiple: I don&#8217;t have GTD implemented in my homesituation yet (more on that later), I don&#8217;t have the room for (analog) archiving at home. Did&#8217;t find a good ticklerfile yet. At work we have archives for our client material. But since that is on the other side of the office, I&#8217;d have to walk every time to get something. Ofcourse that is good for my health, but it stops me from working that way. An archive at my desk? No option, due to space constraints.</p>
	<h2>The first month</h2>
	<p>That&#8217;s when my other vices came into play. Well, they are not really <em>bad</em> vices, but they tend to get the upperhand. First: Software cures all. I have the nasty habit to keep fiddling and tweaking with software untill it suits me just fine. Not realizing (then) it was just procrastinating me for what I really needed to do, Getting Things Done! I clicked on every link in every forumpost on <a href="http://www.davidco.com/forum/viewforum.php?f=3"  target='_blank'>GTD : Gear,Gadgets,software and toys</a> and installed it to see how it worked. This worked perfectly with my second bad habit: Informationjunkieness. Yes that is a made up word. Forgive my poor English writing skills. But the thing is: I love to read and <em>know</em> things. I use RSS intensively (see <a href="http://www.punkey.com/pivot/entry.php?id=1583"  target='_blank'>my post on the GTD setup</a>). Both for work and private. With RSS, I read too much. Picture this: I commute every day. 1 hour in the train. So that is 2 hours a day. In those 2 hours, I read my RSS feeds, see interesting articles and flag them as interesting and &#8220;must read more on it&#8221;. Result: One big pile of articles I don&#8217;t get around to read. In comes vice no.1 :&#8221;Hmmm&#8230;.I need some software to make this reading easier&#8221;...<br />
Same with GTD, instead of just <em>doing things</em> and getting it to work for myself, I surfed the net for tips and tricks. Ofcourse that is not a bad thing, but it stopped me from really really implementing the system in my life and work. I kept seeing tips and thinking:&#8221;yeah I can do that, good idea!&#8221; and instead of implementing that idea, I made a note of it, dropped it and never looked back at it. Bad bad bad&#8230;</p>
	<p>Not everything was <em>that</em> bad. One of the best things I learned the first months is the concept of &#8220;The Next Action&#8221;. Just by some clear thinking and untangling a big list of projects and to do&#8217;s I got some control of projects again. I am currently working on a huge project at work. The process of GTD, the basic rules, gave me an understanding of the work that had to be done. Not only by me, but also by my team of webdesigners, programmers and interface engineers. This helped me identify tasks for others and enforced some clear communication to my client. One tip from the book has helped me tremendously: in a meeeting, just ask what the next action is. Don&#8217;t let decisions ringing in the air, don&#8217;t let people stare at each other about the about the work that lies ahead. But shout it out loud: What is your next action? How do you get forward in this project?</p>
	<h2>After the holiday</h2>
	<p>When I came back from New Zealand I found out life goes on without me. A big no-brainer you learn early in life but it&#8217;s good to be reminded sometimes. I didn&#8217;t read my workmail for three weeks, hadn&#8217;t read any RSS feeds and didn&#8217;t know what the latest and greatest thing in online marketing was when I came home. And the best thing? I didn&#8217;t feel bad at all. One thing I learned while on vacation is to really relax. I don&#8217;t go on vacation really often, I just keep on working and reading and writing. I decided: &#8220;That&#8217;s gotta stop.&#8221; <br />
I truly think that GTD is going to help me with this. I don&#8217;t know how just yet, but I am going to find out. <br />
When I got back, the bad implementation of GTD backfired on me. I couldn&#8217;t find anything, had a long list of overdue items, projects sort of stumbled on but weren&#8217;t running real smoothly and my notes were all over the place. So what I need to do is start again. Not totally from scratch, but just pick up the book and review my notes from the implementation phase. This might take some time, while the projects I am working on don&#8217;t stop. Worse, they need a kickstart as well and I can&#8217;t allow myself to put that of&#8230; So I have the feeling I am going to juggle with the implementation <em>again</em> and I don&#8217;t want that! Maybe I should take one day off but still be at work to re-start the implementation again. Any thoughts on that?</p>
	<h2>Home implementation</h2>
	<p>This has been discussed in forums as well and I have to agree, when you don&#8217;t work at home, you need a separate system for work and private. I want to implement GTD at home as well, but I want to do this with my girlfriend. She hasn&#8217;t read the book yet (<a href="http://www.danbrown.com"  target='_blank'>The DaVinci Code</a> was more interesting, couldn&#8217;t blame her) and I think she has to understand from the book why some things are implemented the way they are. Furthermore we are in the process of buying a house and moving to another city. Eventhough that is a great GTD-project, I feel I have to wait with the full implementation after the move. I don&#8217;t want a half implementation at home and a half at work, that will freak me out even more. </p>
	<h2>Software and hardware</h2>
	<p>Ah! The big procrastinators! On the hardware side: I need a ticklerfile and a good archive. Period. This will be the first purchase the coming week, because I found out that the lack of these items gives me stress when at work. I need to talk to my supervisors about this investment. <br />
On the software side: Re-evaluate my <a href="http://gtdsupport.netcentrics.com/tour/home.asp"  target='_blank'>Outlook add-in</a> and re-evaluate the software I use now. I am currently evaluating <a href="http://www.mindjet.com/eu/"  target='_blank'>Mindmanager</a> for use at work (remember the big project?) and it feels good to me. Not using <a href="http://www.evernote.com/en/"  target='_blank'>Evernote</a> for a month makes me not going back to it instinctively, so I have to figure out why that is. My setup at home is mostly non-Microsoft (apart from the OS) and pro-Mozilla. Since there is some movement in the open source community for GTD-related software, I might keep an eye on that. For instance, <a href="http://www.essentialpim.com/"  target='_blank'>EssentialPIM</a> is on my evaluate-list. But foremost on this subject: <strong>Stop fiddling, get to work!</strong><br />
On this subject one more resolution: update this weblog for everything to work properly. I found out some links and archives are not functioning well. This will be fixed soon!</p>
	<h2>Suggestions</h2>
	<p>Since GTD got a lot of coverage the last year, the buzz and discussions around it have gone bigger. It has become clear that the system as described might need an update. I can only find myself <a href="http://www.43folders.com/2004/12/a_year_of_getti_2.html"  target='_blank'>next to Merlin</a> on this one: Branch out and drill down, update for the always-on generation and tools for implementation. The Outlook add-in needs to much tweaking if you are a manager for an interactive agency like I am. 90% of my work is done online and the default settings for the add-in don&#8217;t cover that enough in my opinion. <br />
Furthermore, I would like to see David or anyone from <a href="http://davidco.com/"  target='_blank'>DavidCo</a> come to Europe (well, the Netherlands actually) for some seminars. Or give us some information on consultants in this area. Maybe I overlooked something, but I couldn&#8217;t find it. Just to be able to talk to someone about GTD really helps me get forward. Mail and web may not always be the best solution. Sometimes you just have to sit down with someone and talk face to face.</p>
	<h2>Conclusion</h2>
	<p>I think Q4 of 2004 was a sort of dress rehearsal for my GTD implementation. I am glad I started this weblog, because I can read back and find some thoughts and reasonings from months ago. And it gives me a platform to speak about progress, problems and victories. That helps. <br />
I do hope the popularity GTD gained last year will continue this year and I can read (in lower volume ;<del>)) some great new tips and tricks. Ofcourse, David&#8217;s company can profit from that and they should. GTD has the great feature that it makes you a evangelist right away. That is something they should benefit from and I really hope they will.<br />
GTD in 2005? I am gonna work this thing. Really really hard. Just so it gives me time and space to do the nicer things in life and perhaps catch up on some new things :</del>)</p>
	<p>I hope this (lengthy) article helps you focus on your own implementation of and dealing with GTD. If you have any questions or some of my rambling is unclear to you, please leave a comment. I appreciate all reader input and try to answer as best as I can.</p>

  
 
<p class="info">23 01 05 - 13:32 - <a href="/pivot/entry.php?id=1644&amp;w=whats_the_next_action" title="23 Jan '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1644&amp;w=whats_the_next_action#comm" title="Stew, Francois Lavaste">two comments</a> <?php echo get_editentrylink("Edit", '1644'); ?></span></p>
</div><span id="e1639"></span><div class="entry">
<h3>Tadalist.com: Make listst and get stuff done!</h3>
	<p>Yes, the article on my progress is coming! I am still swamped with work and need my rest at night. But please be patient. In the meantime: Has anyone seen <a href="http://www.tadalist.com/"  target='_blank'>this website</a>  for making and maintaining online to-do lists? Brought to you by the excellent folks over at <a href="http://www.basecamphq.com/"  target='_blank'>Basecamp</a>! Sounds promising to me! Anyone had experience with this online tool?</p>

  
 
<p class="info">20 01 05 - 14:46 - <a href="/pivot/entry.php?id=1639&amp;w=whats_the_next_action" title="20 Jan '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1639&amp;w=whats_the_next_action#comm" title="meridia">one comment</a> <?php echo get_editentrylink("Edit", '1639'); ?></span></p>
</div><span id="e1624"></span><div class="entry">
<h3>GTD for 2005, just a quick note again!</h3>
	<p>Yes, after a long and fantastic holiday in New Zealand I am back in The Netherlands again and ready for 2005. I am also back at work so I do need some time to get updates on everything. Meanwhile, my GTD-principles need some dusting. Haven&#8217;t really thought about it for a month. Why would you if you walk in <a href="http://www.punkey.com/coppermine/displayimage.php?album=9&#38;pos=32"  target='_blank'>gorgeous National Parks</a> like Abel Tasman? <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/><br />
I am reading up on my GTD-weblogs. Just finished the excellent three-piece article on 43folders.com. If you haven&#8217;t read it yet, I recommend you do so. Check <a href="http://www.43folders.com/2004/12/a_year_of_getti.html"  target='_blank'>Part 1: The Good Stuff</a>, <a href="http://www.43folders.com/2004/12/a_year_of_getti_1.html"  target='_blank'>Part 2: The Stuff I Wish I Were Better At</a> and <a href="http://www.43folders.com/2004/12/a_year_of_getti_2.html"  target='_blank'>Part 3: The Future of GTD?</a><br />
Just a short note this time again, Expect a longer article the coming week on my resolutions for 2005 and thoughts on GTD!</p>

  
 
<p class="info">12 01 05 - 09:37 - <a href="/pivot/entry.php?id=1624&amp;w=whats_the_next_action" title="12 Jan '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1624&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1624'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>