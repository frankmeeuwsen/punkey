<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e1706"></span><div class="entry">
<h3>GTD Audiobooks</h3>
	<p>The last couple of days I had the luxury of using a car to drive to work (I normally commute by train) and I decided that was some nice time alne to rethink the GTD-strategy I was following. Especially al the hints I got from my previous posts made me think I really needed to refresh my GTD-memory. But&#8230;in a car you can&#8217;t read, so I bought <a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&#38;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>the audiobooks over at Audible.com</a> and burned them on CD. I have to say, all the material was not new, but what an eye-opener it was (again)! All the little nuances, details in mastering your workflow and getting that black-belt feel is very well captured in the audiobook. If you travel a lot and have an MP3-player or portable CD-player, I highly recommend you get the GTD-audiobook at Audible.com. It runs just over 2 hours, it doesn&#8217;t have all the details the book has, but just enough to get you going again. <br />
And if you are a first time customer, you get the audio for just $9,95. That&#8217;s a bargain!<br />
I have a MP3-player, but I couldn&#8217;t connect it to the Audible-manager to convert and transfer the file. Make sure you have a CD burner, since the proprietairy .aa-files of audible.com don&#8217;t convert very well to MP3 using freeware or simple tools&#8230;You&#8217;d better burn it on CD and rip that CD back to MP3 again.</p>

  
 
<p class="info">15 02 05 - 22:54 - <a href="/pivot/entry.php?id=1706&amp;w=whats_the_next_action" title="15 Feb '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1706&amp;w=whats_the_next_action#comm" title="Bren, Paul, N">three comments</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '1706'); ?></span></p>
</div><span id="e1701"></span><div class="entry">
<h3>Extending the conversation about many Next Actions</h3>
	<p>In a previous post, I referred to The Big Project which makes me really uncomfortable about doing all the other little things that keep coming in. <a href="http://www.punkey.com/pivot/entry.php?id=1687#mike_sale-0502102324"  target='_blank'>Mike Sale</a> has some excellent remarks on that topic and I would like to extend the conversation a bit about this subject. Because it is true. Even the Biggest of Biggest Projects start with a small, controllable Next Action. Isolating can be a good option, but the Project is suffering from &#8220;progressive insight&#8221; meaning we face new challenges as we go along. New user scenario&#8217;s, changed features, uncertainties about new features. These should have been faced before we started but right now, we are facing this and nothing can change that. Ofcourse it is a good lesson for a next project. So what does this have to do with isolating? I can isolate like hell, but when I plug in again, there are changes in the (voice) mail and people on my desk asking for assistance because of changing point-of-views. <br />
You might say this is a projectmanagement-problem which cannot be solved by GTD and I agree. This has more to do with the way Projectmanagement was handled and I am the first to admit that. But with that as a given, I am looking how I can make the best of it with the techniques at hand.<br />
&#8220;Breakdown&#8221; might be a good alternative and I will try that out coming days. <br />
Corie also has an excellent remark about making a little list. But if you read the above, you will find that this list can be of no use just one hour after the start of the day <img src='/extensions/emoticons/trillian/e_121.gif' alt=';-)' align='middle'/><br />
I do feel Katherine also touched a nerve. I am not familiar with Covey&#8217;s techniques but heard a lot about it. I will look into this some more in the future<br />
I sure hope to find the time to read some pages in David&#8217;s book again. I think it will get me kickstarted again. </p>
	<p><em>Did you have similar experiences? Please let me know and tell me how you faced them. Maybe they will help me!</em></p>

  
 
<p class="info">13 02 05 - 20:24 - <a href="/pivot/entry.php?id=1701&amp;w=whats_the_next_action" title="13 Feb '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1701&amp;w=whats_the_next_action#comm" title="Corie, N">two comments</a> <?php echo get_editentrylink("Edit", '1701'); ?></span></p>
</div><span id="e1696"></span><div class="entry">
<h3>Where did davidco.com go?</h3>
	<p>Hey what&#8217;s that? I finally have this saturdaynight where I can check out some of the forumposts over at the David Allen website. But when I click throught <a href="http://www.davidco.com/forum/rss.php"  target='_blank'>the RSS-feed</a> all I get is this stripped down Technorati site? Check <a href="http://www.punkey.com/images/forumstuk.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9mb3J1bXN0dWsuanBn&w=934&h=396&t=','imagewindow','width=934,height=396,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>this screenshot</a>. Does anybody else have this as well?</p>
	<p><strong>Update:</strong> Everything works fine again. Must have been some hickup in cyberspace?</p>

  
 
<p class="info">12 02 05 - 23:44 - <a href="/pivot/entry.php?id=1696&amp;w=whats_the_next_action" title="12 Feb '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1696&amp;w=whats_the_next_action#comm" title="Dwayne Melancon">one comment</a> <?php echo get_editentrylink("Edit", '1696'); ?></span></p>
</div><span id="e1687"></span><div class="entry">
<h3>Many little Next Actions make a Big One</h3>
	<p>The last couple of weeks have been extremely hectic and have given me ample time to really update my GTD principles. But step by step I am getting more organized again. But my Next Action list just keeps growing. The biggest issue I am facing right now is how to divide my time between little jobs that have to be done (a phonecall, email, letter) and balancing my time in actually <em>not</em> doing all these little things because my big project I am working on gets less attention. It&#8217;s the <a href="http://longtail.typepad.com/the_long_tail/"  target='_blank'>Long Tail thought</a> in some way is it not? If I do everything right now that takes less than two minutes, it will take me 2 hours to do it. But! In these two hours, urgent email rushes in, phones ring because a server went down, my co-workers need to be updated on the project, have questions and oh yeah, I have an appointment waiting. And I don;t get the things done for the Big Project<br />
I know, this is the classic case where GTD can be of help. But really, what is the signal, the red flag that tells you to stop doing these little things and get on with it? Even though these little are equally important? How do you handle with it? I find the book very vague about this. </p>
	<p>PS: I can&#8217;t tell you or show you anything on The Big Project, since it is for a client. But I will let you know as soon as I can in public</p>

  
 
<p class="info">10 02 05 - 21:37 - <a href="/pivot/entry.php?id=1687&amp;w=whats_the_next_action" title="10 Feb '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1687&amp;w=whats_the_next_action#comm" title="Mike Sale, N">two comments</a> <?php echo get_editentrylink("Edit", '1687'); ?></span></p>
</div><span id="e1685"></span><div class="entry">
<h3>The Outlook tricks work!</h3>
	<p>As I mentioned in a previous article, I had some trouble in Outlook with the toolbars. Thanks to <a href="http://www.punkey.com/pivot/entry.php?id=1674#comm"  target='_blank'>the solution provided by Robert and John</a>, everything is working fine now. Thanks guys!</p>

  
 
<p class="info">08 02 05 - 09:11 - <a href="/pivot/entry.php?id=1685&amp;w=whats_the_next_action" title="08 Feb '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1685&amp;w=whats_the_next_action#comm" title="Matt, Punkey">two comments</a> <?php echo get_editentrylink("Edit", '1685'); ?></span></p>
</div><span id="e1678"></span><div class="entry">
<h3>Gotta get my stuff done</h3>
	<p><a href="http://www.i-am-bored.com/bored_link.cfm?link_id=7240"  target='_blank'>This is a really funny animation</a> about all the procratination you can think of when you try to get your stuff done. Nice distraction when you&#8217;re working <img src='/extensions/emoticons/trillian/e_121.gif' alt=';-)' align='middle'/><br />
(via <a href="http://www.Frankwatching.com"  target='_blank'>Frankwatching.com</a>)</p>

  
 
<p class="info">06 02 05 - 09:10 - <a href="/pivot/entry.php?id=1678&amp;w=whats_the_next_action" title="06 Feb '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1678&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1678'); ?></span></p>
</div><span id="e1674"></span><div class="entry">
<h3>Buggy GTD add-in in Outlook?</h3>
	<p>OK, this is buggin&#8217; me (hehehe) for quite some time now. I have the <a href="http://www.davidco.com/productDetail.php?id=63"  target='_blank'>GTD Outlook Add in</a> installed on Outlook 2003. I also have the normal Outlook bars (standard, formatting) and the Lookout Searchbar (worth an evaluation! <a href="http://www.microsoft.com/downloads/details.aspx?FamilyID=09b835ee-16e5-4961-91b8-2200ba31ea37&#38;DisplayLang=en"  target='_blank'>It&#8217;s free!</a>)<br />
When I open a new Task, i would like my toolbars to stay in a specific position. But no matter what I try or what I do, the positions of the toolbars <em>al-ways</em> change. This is <em>so</em> annoying! Toolbars switch positions, slide al the way to the right, position themselves in three (3!!) rows. Does anyone have a solution for this? <br />
And yes, I have to use Outlook at work, so switching to Thunderbird is not a valid option, sorry&#8230;</p>

  
 
<p class="info">02 02 05 - 15:21 - <a href="/pivot/entry.php?id=1674&amp;w=whats_the_next_action" title="02 Feb '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1674&amp;w=whats_the_next_action#comm" title="Phil, N">two comments</a> <?php echo get_editentrylink("Edit", '1674'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>