<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e1777"></span><div class="entry">
<h3>Firefox tabs as startpage</h3>
	<p>Just another little &#8220;hack&#8221; I completely forgot to use. <br />
<strong>Problem:</strong> I am a statistic junkie. I have stats for all sorts of things (MyBloglog, Feedburner, Nedstat). But since these sites don&#8217;t offer me a webfeed to see what&#8217;s happening (insert salespitch here) I am forced to go to their website. I hate that. Because I forget to do it. And at the end of the day, when I am offline, reading my mail and webfeeds, I think to myself: &#8220;Why did I forget it this time?&#8221;<br />
<strong>Solution:</strong> Firefox tabbed browsing. In Firefox, I made a folder with bookmarks to the different statisticspages. Go to Tools > Options > General and in the box Homepage select &#8220;Use bookmark&#8221;. I select the folder I just made and bada-bing, done!<br />
<strong>Result:</strong> Every time I start Firefox (read: in the morning, with a fresh cup of tea) I see the stats coming up before me as a dashboard. I am a happy man.</p>
	<p>Sigh&#8230;if all could be that simple&#8230;</p>

  
 
<p class="info">30 03 05 - 00:52 - <a href="/pivot/entry.php?id=1777&amp;w=whats_the_next_action" title="30 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1777&amp;w=whats_the_next_action#comm" title="Laura, N">two comments</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '1777'); ?></span></p>
</div><span id="e1773"></span><div class="entry">
<h3>Great news from the Evernote camp</h3>
	<p>I got a message from the Evernote VP of productmanagement through their forum about the next version of <a href="http://www.evernote.com:" Thanks" target='_blank'>Evernote</a> for your very nice review of EverNote. The only things I would add are a mention of the SmartSearch (for finding notes by keyword literally as fast as you can type); the fact that you can create your own Auto Categories including Keyword Search categories; and assurance that EverNote will be integrating with Google Desktop Search in the coming months.&#8221;</p>
	<p>So here it goes. Three nice features. Two of them I indeed forgot to mention (SmartSearch and Autocategories) but the GDS-integration is quite interesting! Since I use GDS a lot these days, this can be a nice add-on!</p>
	<p>Next week I will have the time to <em>really</em> evaluate OnFolio for the showdown.</p>

  
 
<p class="info">29 03 05 - 00:12 - <a href="/pivot/entry.php?id=1773&amp;w=whats_the_next_action" title="29 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1773&amp;w=whats_the_next_action#comm" title="Joe Cheng, Greg">two comments</a> <?php echo get_editentrylink("Edit", '1773'); ?></span></p>
</div><span id="e1768"></span><div class="entry">
<h3>First thing tomorrow in the office: uncheck Outlook options</h3>
	<p>1. Open Outlook<br />
2. Tools->Options<br />
3. Click E-mail Options&#8230;<br />
4. Click Advanced E-mail Options&#8230;<br />
5. Uncheck:
	<ol>
		<li>Play a Sound</li>
		<li>Briefly change the mouse cursor</li>
		<li>Show an envelope in the notification area</li>
		<li>Display a New Mail Desktop Alert (default Inbox only)</li>
	</ol></p>
	<p>It has been on my mind for a while now and I already unchecked all of them except number 3. Still needed something to hang on to. But enough is enough. I&#8217;m gonna do email on my time.</p>
	<p>Thanks to <a href="http://www.shahine.com/omar/TakeControlOfEmail.aspx"  target='_blank'>Omar</a> for reminding me</p>

  
 
<p class="info">28 03 05 - 23:03 - <a href="/pivot/entry.php?id=1768&amp;w=whats_the_next_action" title="28 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1768&amp;w=whats_the_next_action#comm" title="Kevin Rutherford, Stew, N">three comments</a> <?php echo get_editentrylink("Edit", '1768'); ?></span></p>
</div><span id="e1758"></span><div class="entry">
<h3>Showdown 1: Using Evernote</h3>
<p style="text-align:center;"><img src="/images/banner_evernote_onfolio.gif" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
	<p>Well, it&#8217;s been quite a week here&#8230;The big fight between Evernote and OnFolio has been a quit one. I actually did use Evernote the last week so it&#8217;s not that wasn&#8217;t anything happening! So how&#8217;s my experience with Evernote? Overall: pretty good! It has been fun using the program and it has quite some potential to use as a &#8220;bin&#8221; for you material. I will review Evernote using the 5 steps from GTD and tell you how you can use it and how I use it for each step. Where possible I have included some nice demovideo&#8217;s of the program</p>

  
<a href="/pivot/entry.php?id=1758&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">23 03 05 - 12:30 - <a href="/pivot/entry.php?id=1758&amp;w=whats_the_next_action" title="23 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1758&amp;w=whats_the_next_action#comm" title="Nitin, N, PeterCashmore">three comments</a> <?php echo get_editentrylink("Edit", '1758'); ?></span></p>
</div><span id="e1745"></span><div class="entry">
<h3>Evernote vs. OnFolio</h3>
	<p>I wrote about my use of Evernote a couple of times. But recently, I found <a href="http://www.onfolio.com"  target='_blank'>OnFolio</a>. A browser-integrated RSS reader and clippingscollector. So now I have both products. Great move huh! For a guy who wants to <a href="http://www.punkey.com/pivot/entry.php?id=1726"  target='_blank'>eliminate his inboxes</a>. Yeah well, it&#8217;s just that both products have a lot of neat features that I can and would use. And now I read that Evernote has <a href="http://www.evernote.com/en/products/evernote/web-clipper.php"  target='_blank'>added Firefo<img src='/extensions/emoticons/trillian/e_26.gif' alt='X-S' align='middle'/>upport</a> in its webclipping functionality, which makes it even harder for me to make a solid decision! So here&#8217;s what I&#8217;m gonna do: I am gonna test both products for one week each. The coming week, I will test Evernote and the next week I will test OnFolio. I will use both products in a normal, day-to-day business manner and will report my findings here on this weblog. <br />
So by April 1st I should make a decision on which product to use. If there is no other product that enters the market and makes my life even more difficult.<br />
The coming weeks, it will be Evernote vs. OnFolio!!!</p>
<p style="text-align:center;"><img src="/images/banner_evernote_onfolio.gif" style="border:0px solid" title="" alt="" class="pivot-image" /></p>

  
 
<p class="info">13 03 05 - 18:37 - <a href="/pivot/entry.php?id=1745&amp;w=whats_the_next_action" title="13 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1745&amp;w=whats_the_next_action#comm" title="johnnu">one comment</a> <?php echo get_editentrylink("Edit", '1745'); ?></span></p>
</div><span id="e1741"></span><div class="entry">
<h3>Braindump on tasks</h3>
	<p>So here&#8217;s another thing I noticed. First off, I must say, this whole GTD-thing really gives you a sense of self-exploration and self-evaluation. This piece is written while commuting in the train. I do have to say that this is a time during the day where I can think about GTD instead of Do-ing it. <br />
Here&#8217;s an issue I found out about myself and I am wondering if my readers also experience this. Whenever something pops up in my head, depending on context, I put it somewhere. When I&#8217;m behind a PC (80% of the time I&#8217;m awake) I put it in my Tasklist immediately with the proper context and project. When not in digital mode I put it in my notebook and process it later. Oh by the way, I bought a Moleskine. More on that in a later post! See my <a href="http://www.flickr.com/photos/punkey/sets/157298/show/"  target='_blank'>Flickr slideshow</a> for pictures. Can&#8217;t believe I made a picture of a notebook&#8230;<br />
Now, when the notes are processed it&#8217;s in my (almost) trusted system. Here&#8217;s where the nagging problem enters. <br />
Say, I remind myself I have to make an appointment with the dentist within a month. So I put it in my agenda about 3 weeks from now to &#8220;Call dentist on number xxx-xxxxxxx regarding new appt.&#8221; It&#8217;s out of my head, into my system. Excellent!<br />
A week later, I am sitting on the couch and thinking to myself &#8220;Hmmm&#8230;.I have to make an appointment with the dentist.&#8221; I put another task in my agenda on the day I want to make an appointment.<br />
Catch my drift? Why is this? Is it because it is out of my head, it can come in (again) easy? Is it because I have the brain of a goldfish? Is it because my system is not trusted yet? Expanding on the last one, let&#8217;s say my system is trusted. If you have the same thought again, would you think to yourself &#8220;Wait a minute, I already processed that. No need to do that again.&#8221; Because if you do that, isn&#8217;t the system also in your head? You know it&#8217;s in your system, so it&#8217;s in your head. Why need an external system then? That&#8217;s a bit harsh, but it is an issue I am facing. I had the same task pop up three times, just because I entered it three times in a week&#8217;s time.<br />
Maybe one of the solutions is my Weekly Review. I know I am not the only one who has trouble with this part of GTD, and yes I have been procrastinating this review way too much. I should try to have this Weekly Review really once a week in an allocated time slot instead of sometimes just quick browsing through my tasks and projects. </p>
	<p>Hmmmm&#8230;.Nice outcome of another braindump!</p>
	<p><em>Any thoughts on this issue? I would like to hear them from you!</em></p>

  
 
<p class="info">11 03 05 - 09:05 - <a href="/pivot/entry.php?id=1741&amp;w=whats_the_next_action" title="11 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1741&amp;w=whats_the_next_action#comm" title="flamme, N">two comments</a> <?php echo get_editentrylink("Edit", '1741'); ?></span></p>
</div><span id="e1738"></span><div class="entry">
<h3>And again someone converts to GTD!</h3>
	<p>Remember I <a href="http://www.punkey.com/pivot/entry.php?id=1706"  target='_blank'>bought</a> the GTD audiobook a couple of weeks ago? Well, I made a CD copy for a collegeau and guess what? He left the dark side and is a GTD-practicioner as well. This can really help since we both use the same tools and are on the same job. So during lunch, we can discuss (among other <a href="http://www.santabanta.com/newsmaker.asp?select=1421&#38;catname=Newsmaker"  target='_blank'>current events</a> <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/>) all things GTD. Welcome aboard Martijn!<br />
One more hack I implemented and starting to pay off is the use of reminder-time in my Tasks. So right now, my current activities flow as follows:<br />
1. What has to be done on a certain date goes to my calendar and that is the first thing I do in the morning.<br />
2. For Next Action&#8217;s without a real due date but has to be done a.s.a.p I put a reminder so I get a list of &#8220;urgent&#8221; Next Actions. Currently, this list is about 20-30 items long. (which most of the time are &#8220;Waiting For&#8221;-actions)<br />
3. When those are done, I go to my lists in the Tasklist and work on those NA&#8217;s.</p>
	<p>Even though it&#8217;s not perfect, it get&#8217;s a big part of the job done for me. </p>
	<p>Update on Evernote: Hmmm&#8230;.haven&#8217;t really used it&#8230;again&#8230;I got drawn to a new product called <a href="http://www.onfolio.com"  target='_blank'>OnFolio</a> which has a lot of potential since it&#8217;s an RSS reader and clipboard in one. With Firefox support!</p>

  
 
<p class="info">10 03 05 - 15:16 - <a href="/pivot/entry.php?id=1738&amp;w=whats_the_next_action" title="10 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1738&amp;w=whats_the_next_action#comm" title="just another GTD user">one comment</a> <?php echo get_editentrylink("Edit", '1738'); ?></span></p>
</div><span id="e1726"></span><div class="entry">
<h3>Still digging Evernote?</h3>
	<p>Slacker Manager <a href="http://www.slackermanager.com/slacker_manager/2005/03/i_still_dig_eve.html"  target='_blank'>writes</a> about Evernote and how he still digs it. I still have a beta version on my laptop but I rarely use it anymore. I just opened it to see what was still in there.<br />
Most of the time I needed <a href="http://www.evernote.com/"  target='_blank'>Evernote</a> to drag and drop some article thoughts for this weblog (Well, that helped, considering the amount of updates over here&#8230;), our company weblog and some other sites I maintain. But, for some reason, I didn&#8217;t get the &#8220;click&#8221; to use Evernote again once I collected the snippets. Somehow, Evernote was just one more big Inbox for me I needed to process as well. Come to think of it, I do believe it is because I have <em>so many</em> inboxes. I made a little article about this. Please read on if you want to know more</p>

  
<a href="/pivot/entry.php?id=1726&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">03 03 05 - 09:27 - <a href="/pivot/entry.php?id=1726&amp;w=whats_the_next_action" title="03 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1726&amp;w=whats_the_next_action#comm" title="Bren, N">two comments</a> <?php echo get_editentrylink("Edit", '1726'); ?></span></p>
</div><span id="e1725"></span><div class="entry">
<h3>It&#039;s not trusted</h3>
	<p>Just a thought which I would like to think more on for a while. I think what the problem is with my system is it&#8217;s not <strong>trusted</strong>. I just don;t trust my opwn system enough yet to get my mid empty. Example: When I put something in my action-list, a couple of hours later I think about the item again and put it in the list again! No really! Do I have a short term memory problem? I don&#8217;t think so. I just don;t trust my own system yet. Ha! That&#8217;s a revelation!<br />
Thank you for all your kind words and comments on my last post. And David, thanks for the <a href="http://www.punkey.com/pivot/entry.php?id=1724#david_tebbutt-0503020857"  target='_blank'>productplug</a> but the last thing I need right now is another product! But I&#8217;ll keep it in mind for later evaluation (so it&#8217;s on the @someday list!). And Dwayne, you can get some Stroopwafels <a href="http://www.hollandsbest.com/stroopwafels/stroopwafels_verweij.php"  target='_blank'>here</a> <img src='/extensions/emoticons/trillian/e_121.gif' alt=';-)' align='middle'/></p>

  
 
<p class="info">02 03 05 - 10:04 - <a href="/pivot/entry.php?id=1725&amp;w=whats_the_next_action" title="02 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1725&amp;w=whats_the_next_action#comm" title="Bert">one comment</a> <?php echo get_editentrylink("Edit", '1725'); ?></span></p>
</div><span id="e1724"></span><div class="entry">
<h3>Deafening silence</h3>
	<p>What happened? Why is it so quiet here? I don&#8217;t know. I haven&#8217;t put anything on for the last 14 days. That&#8217;s not nice to my readers. But the truth is: I am looking for a format for this weblog. And at the same time I am struggling with GTD and the implementation. Yes, still&#8230;I don&#8217;t know what it is, but every time when I think to myself &#8220;This weekend is going to be THE weekend to REALLY get going&#8221;, something happens. Most of the time I find something else to do. Like rearranging books or changing the desktop on my PC. You know what I&#8217;m talking about right? Yeah, it&#8217;s procrastination-a-gogo over here. I&#8217;ve listened to the tapes, read the books but somehow, something is stopping me. I think it&#8217;s two issues:<br />
1. Extreme situation at work. I am currently involved in some major projects (The Big One) and quite some other at work. This makes me feel really stressed and still not really able to get going. You know, the Inbox is empty, but the tasklist is full. And still I have the idea I am missing something. Result: stress! Extra result: procrastination.<br />
I can <em>so</em> relate to <a href="http://rickyspears.com/blog/index.php?cat=2"  target='_blank'>Ricky&#8217;s stories</a> about the GTD Fast CD&#8217;s. It feels like I&#8217;m reading my own stories&#8230;<br />
2. A new house. Another pretty big change in my life. We are moving to another city in a new house. Lots of stuff to do, lists to make, things to arrange. Makes me a bit stressfull too. Especially since my girlfriend can&#8217;t really help that much because she is looking for a job most of the times. </p>
	<p>So what to do? I don&#8217;t know right now. I really really appreciate all your support and comments. But I feel like I first need some calmness and ease in my life. It&#8217;s a whirlwind right now and trying to make big changes in how I deal with it at the same time just doesn&#8217;t cut it for me.<br />
Ah hell, that&#8217;s how I feel right now. Maybe when I think about it, read some chapters, just get some &#8220;Zen&#8221; in here, things might look better for me.</p>
	<p>sigh&#8230;</p>

  
 
<p class="info">01 03 05 - 19:43 - <a href="/pivot/entry.php?id=1724&amp;w=whats_the_next_action" title="01 Mar '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1724&amp;w=whats_the_next_action#comm" title="George Mixon, Paul, N">three comments</a> <?php echo get_editentrylink("Edit", '1724'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>