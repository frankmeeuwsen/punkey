<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e6955"></span><div class="entry">
<h3>Anything that&#039;s a &quot;problem&quot; is a &quot;project&quot;</h3>
	<p>The GTD Road Map is full on. Already there are some reviews popping up like on <a href="http://buzzmodo.typepad.com/buzznovation/2005/08/gtdthe_road_map.html"  target='_blank'>Buzz Bruggeman&#8217;s site</a> and <a href="http://blogs.oreillynet.com/beasts/archives/2005/08/getting_things_1.html"  target='_blank'>this one</a>. There is also <a href="https://www.livemeeting.com/cc/marketing4/view?id=allenknowledge&#38;pw=650650"  target='_blank'>a presentation in slides and audio</a>. Looks like some nice readingmaterial for a couple of moonlight-hours&#8230;.Anyone else has some interesting reviews from the Roadmaps?</p>

  
 
<p class="info">24 08 05 - 15:51 - <a href="/pivot/entry.php?id=6955&amp;w=whats_the_next_action" title="24 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6955&amp;w=whats_the_next_action#comm" title="Matthew Cornell">one comment</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '6955'); ?></span></p>
</div><span id="e6954"></span><div class="entry">
<h3>TiddlyPedia?</h3>
	<p>From the install-instructions: &#8220;To use the <a href="http://15black.bluedepot.com/styles/tiddlypedia.htm"  target='_blank'>TiddlyPedia</a> skin on your own TiddlyWiki, copy the contents of TiddlyPediaPackage tiddler and tag it as &#8216;systemConfig.&#8217; &#8221; C&#8217;mon&#8230;.can&#8217;t we think of a less laughable name than TiddlyWiki and TiddlyPedia? I think it&#8217;s stupid.<br />
On the other hand, I also think it&#8217;s time for a personal tool-stop and start working on my own system with the tools I have. And not try new stuff whenever I am stuck with the current ones. So no Tiddly-Whatevers for me for a while. Just my Outlook, Moleskine, FeedDemon (yup, dropped OnFolio, way to heavy on the resources) and some printouts of the <a href="http://www.douglasjohnston.net/weblog/archives/2005/06/11/diyp2_hipsterpda/"  target='_blank'>DIY Hipster PDA</a>. Like I said earlier, it&#8217;s time to <a href="http://www.punkey.com/pivot/entry.php?id=6950"  target='_blank'>consolidate</a>.</p>

  
 
<p class="info">24 08 05 - 09:07 - <a href="/pivot/entry.php?id=6954&amp;w=whats_the_next_action" title="24 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6954&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6954'); ?></span></p>
</div><span id="e6953"></span><div class="entry">
<h3>Problem with webfeed?</h3>
	<p>Hmmm..it seems there is a slight problem with my Feedburner-based webfeed (forget that <a href="http://www.infoworld.com/article/05/08/17/HNmsrss_1.html?source=rss&#38;url=http://www.infoworld.com/article/05/08/17/HNmsrss_1.html"  target='_blank'>RSS-webfeed debate</a>...) and it doesn&#8217;t show up properly. I don&#8217;t have a solution right now but will look into it on a later time this weekend. <br />
<p style="text-align:center;"><img src="/images/feedburnerwtna.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>


  
 
<p class="info">19 08 05 - 13:57 - <a href="/pivot/entry.php?id=6953&amp;w=whats_the_next_action" title="19 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6953&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6953'); ?></span></p>
</div><span id="e6952"></span><div class="entry">
<h3>Rule of thumb to put something in a list?</h3>
	<p><img src="/images/sen.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I was working through my Inbox this morning and had a few mails I had to delegate to someone else. Short questions, little assignments. But I need an answer from the receiving party to move the project forward. Not <em>now</em>, but whenever possible. I was thinking and in doubt. I use the <a href="http://www.davidco.com/productDetail.php?id=63&#38;IDoption=20"  target='_blank'>GTD Outlook plugin</a> so I have the possibility to put a request in my Waiting-For list with one click. But then I thought to myself, &#8220;I think she is going to answer pretty quick, since it&#8217;s such a simple question. Why should I put in my WF-list? When I put it in, perhaps an hour later I can throw it out! Maybe I should just email and not put a reminder on my list.&#8221; <br />
But I was in doubt because what if she doesn&#8217;t respond in an hour? What if she gets flooded with mail and doesn&#8217;t find the time? <br />
Anyway, the whole process of doubt and thinking took me more than 2 minutes so I decided to put it in my WF-list anyway and see how it works out the coming days/weeks. If, during my Weekly Review, I find a lot of already answered mail, perhaps I should find another way. Maybe another context? @ShortAnswer?</p>
	<p><em>Any thoughts on little rules of thumb on these issues? How do you deal with it?</em></p>

  
 
<p class="info">12 08 05 - 09:19 - <a href="/pivot/entry.php?id=6952&amp;w=whats_the_next_action" title="12 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6952&amp;w=whats_the_next_action#comm" title="Greg Rohrbach, Jeroen, Frank Meeuwsen, Matthew Cornell">four comments</a> <?php echo get_editentrylink("Edit", '6952'); ?></span></p>
</div><span id="e6951"></span><div class="entry">
<h3>The Empty Cup Principle</h3>
	<p>Through some other searchword, I came on <a href="http://willpate.org/"  target='_blank'>this weblog</a> and I found a nice article about <a href="http://willpate.org/the-empty-cup-principle"  target='_blank'>The Empty Cup Principle</a>. It defines in an excellent way what I wrote about earlier. It is not good to know everything at any given time. Your mind will overflow like a full cup of tea.</p>
	<p>&#8220;We can react to new opportunities (call them challenges if you must) with much greater success if we use the empty cup principle.&#8221; </p>

  
 
<p class="info">10 08 05 - 11:27 - <a href="/pivot/entry.php?id=6951&amp;w=whats_the_next_action" title="10 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6951&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6951'); ?></span></p>
</div><span id="e6950"></span><div class="entry">
<h3>A time to consolidate</h3>
	<p>The past couple of weeks I&#8217;ve been thinking about my implementation of GTD in both personal and professional life. And I have decided that it&#8217;s time to really stop tinkering and tweaking the system and just leave it for a while. As a lot of GTD-devotees tend to do, I also kept tweaking my work system, implementing new ideas I read here and there, trying new software (Yes, I will get back on <a href="http://www.punkey.com/pivot/entry.php?id=1644#francois_lavaste-0508090251"  target='_blank'>those evaluations</a>, thanks for the reminder!) and testing ideas from <a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&#38;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>The Book</a>. But for the last couple of days I have found myself in a state of mind where I think my system is pretty OK for me at this point and I should really focus on succesfull outcomes for my different projects. Ofcourse this is a goal you should always have, but I feel I should leave my system <em>as is</em> right now and go further with some projects. What are those projects? Well, apart from my professional projects I always have stuff to do in the house. This varies from hanging some lights to painting the outdoors. But I also have some projects regarding this weblog. I have ideas for a new layout, perhaps a more commercial approach of this blog. And I am working to put my 10 year old thesis on scenemarketing online as a open source project for everyone to benefit from. </p>
	<p>How do I manage those projects? Simple. I have found out that in my personal life, the need for a fully implemented system with different contexts, projectlists and actions is not really necessary. I don&#8217;t have a lot of pressure to change the layout of this blog, so whenever I feel like working on it, I work on it. I read some weblogs on making money, but I don&#8217;t have a grand scheme on how to implement it. I just go along with the flow. And when I have a weekend planned to paint the frontdoor, but something else comes along? Ah well, deal with it. Next weekend is just 6 days away.<br />
I know, it&#8217;s a strange conclusion to draw, but really, this is a nice way to look at it. Ofcourse, there are some things that need to <em>Get Done</em> in  a specific timeframe, like paying the bills. But you know what? I have these activities planned on a solid day of the week. Same with cleaning parts of the house. So because I have a trusted system for some of the activities, I can have more flexibility in time and resources for other activities.<br />
Personal life: System is consolidated right now. Professional life: Same issue. I think I implemented GTD in a nice way around the workflow we already have at our office. More on that in a later post!</p>

  
 
<p class="info">10 08 05 - 06:17 - <a href="/pivot/entry.php?id=6950&amp;w=whats_the_next_action" title="10 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6950&amp;w=whats_the_next_action#comm" title="Ren&eacute;, Oliver Nielsen">two comments</a> <?php echo get_editentrylink("Edit", '6950'); ?></span></p>
</div><span id="e6949"></span><div class="entry">
<h3>Temptation Blocker</h3>
	<p>So, have a major deadline looming or ripe opportunity closing and just don&#8217;t have time to waste playing Half Life 2 or checking Bloglines one last time? Well then, add Half Life 2 and Firefox to the list of programs you want to block in Temptation Blocker, set the timer for how long you want to block them and then hit the &#8220;Get Work Done!&#8221;-button.</p>
	<p><a href="http://www.webjillion.com/archives/2005/08/01/free-software-temptation-blocker"  target='_blank'>Free Software: Temptation Blocker &#8211; WebJillion</a></p>
	<p><em>*Excellent!*</em></p>

  
 
<p class="info">03 08 05 - 09:09 - <a href="/pivot/entry.php?id=6949&amp;w=whats_the_next_action" title="03 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6949&amp;w=whats_the_next_action#comm" title="paul, Punkey, avery, foo">four comments</a> <?php echo get_editentrylink("Edit", '6949'); ?></span></p>
</div><span id="e6948"></span><div class="entry">
<h3>meereffect.nl</h3>
	<p>For all the dutch readers willing to read more about GTD in our native language, check out Taco&#8217;s weblog <a href="http://www.meereffect.nl/"  target='_blank'>meereffect.nl</a> (translated &#8220;More effect&#8221;) where he gives the lowdown about GTD, explaining some of the core principles and how he thinks about it. <em>Subscribed!</em></p>

  
 
<p class="info">03 08 05 - 08:44 - <a href="/pivot/entry.php?id=6948&amp;w=whats_the_next_action" title="03 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6948&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6948'); ?></span></p>
</div><span id="e6947"></span><div class="entry">
<h3>10 Steps To Better Meetings</h3>
	<p>To-Done has some great <a href="http://www.to-done.com/2005/08/10-steps-to-better-meetings/"  target='_blank'>tips and steps</a> on holding better meetings. Not just by content, but also by <em>context</em>. For instance: <strong>Start on time</strong>. Sounds easy? You should know how many time I&#8217;ve wasted waiting on people &#8220;just having one more smoke&#8221;, &#8220;just finishing this phonecall&#8221; or any other lame excuse to show up late. And what if you adress it to them? &#8220;owww c&#8217;mon don&#8217;t be so nitpicky&#8230;.&#8221; exhausting man&#8230;.exhausting&#8230;<br />
Another tip? <strong>Have a leader</strong>. I have been in meetings with absolutely no coordination, no agenda, no notes. Nothing. They last a couple of hours, take forever to make decisions. <br />
Oh the frustration when I see that list. Because part of me knows I have been a bad boy too when it comes to meetings (and sometimes still am) but part of it also knows that meetings can be held in a better way. But for some reason, people don&#8217;t listen&#8230;</p>

  
 
<p class="info">02 08 05 - 23:39 - <a href="/pivot/entry.php?id=6947&amp;w=whats_the_next_action" title="02 Aug '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6947&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6947'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>