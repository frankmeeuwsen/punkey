<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e7000"></span><div class="entry">
<h3>Feeddemon 2.0 in beta</h3>
A little message for all of you to let you know one of THE best newsreaders, Feeddemon, is <a href="http://nick.typepad.com/blog/2006/01/ann_feeddemon_2.html"  target="_blank" target='_blank'>in beta for version 2.0</a>. Nick Bradbury has done a tremendous job again defining how a newsreader should look and work. Expect a review in the near future on this site 
 
<p class="info">24 01 06 - 08:59 - <a href="/pivot/entry.php?id=7000&amp;w=whats_the_next_action" title="24 Jan '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7000&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '7000'); ?></span></p>
</div><span id="e6997"></span><div class="entry">
<h3>The effects of Moore&#039;s Law and slacking on large computations</h3>
<p>Ha! Check <a href="http://www.gil-barad.net/~chrisg/mooreslaw/Paper.html"  target='_blank'>this study</a>! They prove that if you wait long enough to buy a new computer, you will actually work faster on that new PC because of Moore's Law. Or something like that. I'm a bit scientifically challenged...As they say themselves: &quot;You could go to the beach for 2 years, then come back and buy a new computer and  compute for a year, and get the same amount of work done.&quot;</p><p>They also calculate the optimal and maximal slackage.&nbsp; Now that's getting things done!</p> 
 
<p class="info">18 01 06 - 22:23 - <a href="/pivot/entry.php?id=6997&amp;w=whats_the_next_action" title="18 Jan '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6997&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6997'); ?></span></p>
</div><span id="e6996"></span><div class="entry">
<h3>Mindmap on GTD</h3>
<p>Eric Sinclair attended a GTD Connect Circle in Chicago. These meetings are ongoing discussions furthering the work being done on the GTD seminars. Very interesting (when do they come to the Netherlands?) and Eric <a href="http://www.kittyjoyce.com/eric/log/archive/001512.html#001512"  target="_blank" target='_blank'>created a mindmap</a> around these meetings. Very interesting topics and questions in this map. It is made in Freemind, the free mindmap software you can <a href="http://freemind.sourceforge.net/"  target="_blank" target='_blank'>download from Sourceforge</a>. </p><p>So does anybody else visit these kind of circles? I never heard of them but it sounds interesting to meet up with others to talk about personal productivity, talking and learning from each other.</p> 
 
<p class="info">14 01 06 - 23:53 - <a href="/pivot/entry.php?id=6996&amp;w=whats_the_next_action" title="14 Jan '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6996&amp;w=whats_the_next_action#comm" title="Eric Sinclair, Frank">two comments</a> <?php echo get_editentrylink("Edit", '6996'); ?></span></p>
</div><span id="e6995"></span><div class="entry">
<h3>New feature: Search engine highlighting</h3>
I just implemented a new feature on the individual articlepages of this blog. When you visit this page via a searchengine, you will see the searchwords highlighted on the page. I hope you will find this helpfull. You can test this by going to <a href="http://www.google.nl/search?q=gtd%20backpack&amp;start=0&amp;ie=utf-8&amp;oe=utf-8&amp;client=firefox-a&amp;rls=org.mozilla:en-US:official"  target='_blank'>this searchresult</a> on &quot;gtd backpack&quot; and click the link to my page. You will see the searchwords highlighted. You can find the script and helpfiles on <a href="http://fucoder.com/code/se-hilite/"  target='_blank'>this site</a> 
 
<p class="info">14 01 06 - 22:56 - <a href="/pivot/entry.php?id=6995&amp;w=whats_the_next_action" title="14 Jan '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6995&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6995'); ?></span></p>
</div><span id="e6994"></span><div class="entry">
<h3>6 types of email and how to deal with it</h3>
<p><img src="/images/29670213_d245dbffd4_m.thumb.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />When dealing with your personal productivity in an electronic workspace, email is practically everywhere. For me, almost all communication goes through email. Actually, my Intray on my desk has been empty for a week now. I get very few mail on paper, magazines go straight into a &quot;to read&quot; basket and every other little scrap of paper gets dealt with right away. Because it takes less than two minutes to drop some form over at a co-worker's desk, read a leaflet on something and decide what to do with it or get those receipts from businesslunches and gas and make sure they get to the Office Manager. </p><p>But email, that's a whole other story. Actually, over at David Allen's website, Julie Daniel wrote <a href="http://www.davidco.com/coaches_corner/Julie_Daniel/article9.html"  target="_blank" target='_blank'>a fine piece</a> on the different types of email that can and <em>will</em> linger in your Inbox. She describes the following sorts of email...</p> 
<a href="/pivot/entry.php?id=6994&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">12 01 06 - 23:34 - <a href="/pivot/entry.php?id=6994&amp;w=whats_the_next_action" title="12 Jan '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6994&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6994'); ?></span></p>
</div><span id="e6993"></span><div class="entry">
<h3>Top 10 articles on Whatsthenextaction.com</h3>
<p style="text-align:center;"><img src="/images/56021383_e1d12dcdce_m.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
<p>After writing for more than a year, I feel it's time to let you know which articles are most popular here. Also for new readers (welcome!) to get a feel on what I write about and what goes on here. For this top 10 I use the statisticsapp <a href="http://www.haveamint.com/"  target='_blank'>Mint</a>. If you have a weblog, I can highly recommend this stats-program. It gives you a variety of information without becoming too cluttered like other (free) counters. Best feature: You will get a RSS feed for referrers to your site. Excellent...</p><p>Alas, let us continue to The Top 10 WTNA Blogposts!</p> 
<a href="/pivot/entry.php?id=6993&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">11 01 06 - 23:48 - <a href="/pivot/entry.php?id=6993&amp;w=whats_the_next_action" title="11 Jan '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6993&amp;w=whats_the_next_action#comm" title="chrispian, Frank Meeuwsen, Terry Porter, Frank">four comments</a> <?php echo get_editentrylink("Edit", '6993'); ?></span></p>
</div><span id="e6990"></span><div class="entry">
<h3>Presentation Zen</h3>
<p>Maybe not directly related to your personal productivity but I do feel <a target="_blank" href="http://presentationzen.blogs.com/presentationzen/2005/11/the_zen_estheti.html">this article</a> touches some points you can incorporate in your own thinking about productivity. If you present in public every now and then, I can recommend the article &quot;Presentation Zen&quot; by former Apple employee  							Gerry Reynolds. He discusses the presentation style, slides and gesture of both Bill Gates and Steve Jobs and compares this with the some of the Zen aesthetic values</p><ul><li>Simplicity </li><li>Subtlety </li><li>Elegance </li><li>Suggestive rather than the descriptive or obvious </li><li>Naturalness (i.e., nothing artificial or forced), </li><li>Empty space (or negative space) </li><li>Stillness, Tranquility </li><li>Eliminating the non-essential</li></ul><p><img src="/images/jobs_question2_2.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />This really resonates with me, not only because of <a href="/pivot/entry.php?id=6970"  target='_blank'>my earlier post</a> on GTD and Tai Chi, but also on other levels. If you think about the GTD-principles, they really do fit in the Zen values don't they? Just let the five phases roll in you head (Collection, Process, Organize, Review, Do) while looking at this list and see the connections and feel the logic. Every single thing I do, even the simplest action as cleaning my Inbox (well it <em>used to be</em> not that simple...) really comes down to these values. Choosing the right software, hardware or online service let's you review those with at least values like simplicity and elegance.</p> 
 
<p class="info">04 01 06 - 18:25 - <a href="/pivot/entry.php?id=6990&amp;w=whats_the_next_action" title="04 Jan '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6990&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '6990'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>