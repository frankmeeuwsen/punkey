<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e7069"></span><div class="entry">
<h3>6 ways to run an effective meeting</h3>
	<p>Ever had the feeling that the meetings you attend are a complete waste of time? No agenda, no organizer, just some incoherent emotional rambling about stuff that bothers you and the others. Well, check out these six ways to hold a tight meeting. Just as they do at Google HQ.</p>

  
<a href="/pivot/entry.php?id=7069&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">28 09 06 - 22:21 - <a href="/pivot/entry.php?id=7069&amp;w=whats_the_next_action" title="28 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7069&amp;w=whats_the_next_action#comm" title="Hobie Swan">one comment</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '7069'); ?></span></p>
</div><span id="e7068"></span><div class="entry">
<h3>Mindjet&#039;s MindManager for free</h3>
	<p><img src="/images/172651123_798af692b8_m.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" /><br />
Today I received a very interesting link in my feedreader. The dutch PR Firm for <a href="http://www.mindjet.com/eu/"  target='_blank'>Mindjet</a> is giving away free copies of the excellent program Mindmanager for bloggers. If you like to brainstorm, think through processes and generaly get things of your mind, Mindmanager can be a wonderful tool for you. <br />
As they say for themself (translated from dutch):<br />
<blockquote>The number of blogs, both personal and professional, are growing. The give a new way of knowledge sharing and information flows. Mindjet plays a big role in that area. For that reason we would like to support bloggers with their creativity and offer MindManager for free. With this software they can order their data in a better way and updating your blog becomes easier<br />
</blockquote><br />
I have already received my copy and will definitely write a showdown the coming weeks here on my blog. I think mindmapping is a very very powerful tool for your brain. <br />
Anyway, you could try to email them at blog@mindjet.nl and see if you can get a copy. Once again, I am not sure if this action is eligible outside The Netherlands.<br />
(picture courtesy of <a href="http://www.flickr.com/photos/vaxzine/172651123/"  target='_blank'>VaXzine @ Flickr.com</a>)</p>

  
 
<p class="info">28 09 06 - 14:38 - <a href="/pivot/entry.php?id=7068&amp;w=whats_the_next_action" title="28 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7068&amp;w=whats_the_next_action#comm" title="Graham English, Hobie Swan">two comments</a> <?php echo get_editentrylink("Edit", '7068'); ?></span></p>
</div><span id="e7067"></span><div class="entry">
<h3>Great (old) article on GTD</h3>
	<p>From the Ready For Anything reading group I found <a href="http://www.fastcompany.com/magazine/34/allen.html"  target='_blank'>this link</a> to an old Fast Company interview with The Dave. Great stuff. With one very true and very beautiful quote</p>
<blockquote>You can do anything&#8212;but not everything.</blockquote>
	<p>Print it on a tile, hang it on your monitor. Very true!</p>

  
 
<p class="info">25 09 06 - 22:21 - <a href="/pivot/entry.php?id=7067&amp;w=whats_the_next_action" title="25 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7067&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7067'); ?></span></p>
</div><span id="e7066"></span><div class="entry">
<h3>Shortcut Sunday #4: Gmail Macros</h3>
	<p>In the fourth installment of Sunday Shortcuts (yes I know it&#8217;s monday&#8230;) I would like to put the spotlight on one of the best Firefox extensions I have seen. The <a href="http://www.mscape.com/cgi-bin/mt/mt-search.cgi?IncludeBlogs=1&#38;search=macros"  target='_blank'>Gmail Macros</a> make it very very easy for you to manage your email in Gmail. The script is Firefox only and you will need the <a href="http://greasemonkey.mozdev.org/"  target='_blank'>Greasemonkey Extension</a>. If you don&#8217;t know what Greasemonkey is, check out <a href="http://diveintogreasemonkey.org"  target='_blank'>this website</a> which explains it in good detail. Ofcourse you will need a GMail account to work with the Gmail Macros (GM)</p>

  
<a href="/pivot/entry.php?id=7066&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">25 09 06 - 21:28 - <a href="/pivot/entry.php?id=7066&amp;w=whats_the_next_action" title="25 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7066&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7066'); ?></span></p>
</div><span id="e7065"></span><div class="entry">
<h3>SmarterTasks brings GTD to your mobile</h3>
	<p>Do you use your mobile smartphone a lot for capturing ideas or checking tasks? Do you have Outlook as your primary GTD system? Than perhaps <a href="http://www.smartertasks.com/default.aspx"  target='_blank'>SmarterTasks</a> might be an interesting option for you. I haven&#8217;t had the chance to try it out myself but the screenshots and key features look promising<br />
<p style="text-align:center;"><img src="/images/screenshots-anim.gif" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
	<ol>
		<li>Designed for GTD methodology</li>
		<li>Fast switching between contexts and projects</li>
		<li>Edit contexts and projects directly on your Smartphone</li>
		<li>Supports landscape and portrait</li>
		<li>Quick entry bar for fast entry of new tasks</li>
		<li>SmartText insertion for frequently used phrases</li>
		<li>SmartActions to automate dialing</li>
	</ol></p>
	<p>You can try it for 30 days and afterwards purchase it for $14,95. If anyone has some experience with this mobile app, please let me know!</p>

  
 
<p class="info">24 09 06 - 13:32 - <a href="/pivot/entry.php?id=7065&amp;w=whats_the_next_action" title="24 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7065&amp;w=whats_the_next_action#comm" title="Lars Axelsen, Frank Meeuwsen">two comments</a> <?php echo get_editentrylink("Edit", '7065'); ?></span></p>
</div><span id="e7062"></span><div class="entry">
<h3>I&#039;m a blackbelt father now!</h3>
	<p><img src="/images/tessthumb.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />&#8220;Where are the sunday shortcuts?&#8221;, &#8220;Where are the new articles?&#8221; All valid questions&#8230;but I had something far far far more important to do the last couple of weeks. And the coming years. Me and my girlfriend Helie are very happy with our new daughter Tess. Her full name is Tess Moana Meeuwsen. Her middle name Moana is the Maori word for &#8220;ocean&#8221; or as they say &#8220;big blue surface&#8221;. We have a weakness for New Zealand and the colour of the ocean back there so that&#8217;s why we named here Moana. <br />
We also made <a href="http://www.tessmoana.nl"  target='_blank'>a website</a> for her. Ofcourse it&#8217;s a weblog. It&#8217;s all in dutch, but you can check out some of the photos we made of Tess and our new happy family. <br />
Right now, Tess is sleeping so I have some time for my own work and check some stuff online. I have 10 days off so plenty of time to get used to my new life as a loving father. I am very pleased to already see some of the GTD principles coming back in the way I take care of Tess. We write stuff down very quick so we don&#8217;t forget new diapers, or mailing some government-issue about the birth. Also, I keep a notebook (yes, a Moleskine) to write little stuff down we learn while raising and caring Tess. I hope I find the will and the time to keep this up and bring on some of the thoughts and principles of GTD to little Tess. Makes a well-cleaned house I hope <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>

  
 
<p class="info">21 09 06 - 13:41 - <a href="/pivot/entry.php?id=7062&amp;w=whats_the_next_action" title="21 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7062&amp;w=whats_the_next_action#comm" title="Mirko, Michael, Taco Oosterkamp, Matthew Cornell, Ruben Timmerman, N&eacute;stor Rojas (Caracas, Venezuela)">six comments</a> <?php echo get_editentrylink("Edit", '7062'); ?></span></p>
</div><span id="e7061"></span><div class="entry">
<h3>Save time with opening folders</h3>
	<p>Sometimes the solution to a problem can be so simple. If you have Windows XP and MS Office running. James gave me <a href="http://www.friedbeef.com/2006/09/19/how-to-attach-and-save-files-faster/"  target='_blank'>a quick tip on how to get to regularly opened folders</a> in a flash while working in MS Office. Just add it to &#8220;My Places&#8221; and it appears in the shortcut bar. Check out James&#8217; post with a very clear screenshot. Thanks for the tip!</p>

  
 
<p class="info">21 09 06 - 13:32 - <a href="/pivot/entry.php?id=7061&amp;w=whats_the_next_action" title="21 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7061&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7061'); ?></span></p>
</div><span id="e7060"></span><div class="entry">
<h3>Some fun on timemanagement</h3>
<p><a href="http://blaugh.com/2006/08/29/managing-time-management/"  rel="bookmark" target='_blank'><img class="comic" src="http://blaugh.com/cartoons/060829_time_management.gif" border="0" alt="Managing Time Management" title="Managing Time Management" width="447" height="250" /></a></p><p>Hehehe...from the <a href="http://www.blaugh.com"  target="_blank" target='_blank'>bLaugh website</a>. Comics from the blogosphere.</p> 
 
<p class="info">10 09 06 - 17:07 - <a href="/pivot/entry.php?id=7060&amp;w=whats_the_next_action" title="10 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7060&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7060'); ?></span></p>
</div><span id="e7059"></span><div class="entry">
<h3>Do a braindump and feel relief</h3>
<p><img src="/images/65766060_fbb4d0d747_m_copy1.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I recently joined a readingclub. Yeah, a good ol&#39;fashioned readingclub. Well, online ofcourse. C&#39;mon, I&#39;m not gonna sit in some library or bookstore. Just kidding folks, just kidding. On the <a href="http://finance.groups.yahoo.com/group/Ready4Anything/"  target="_blank" target='_blank'>Ready4Anything mailinglist</a> you can jump in a new readingsession of David Allen&#39;s &quot;other&quot; book: <a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target="_blank" target='_blank'>Ready for Anything</a>. This book doesn&#39;t describe the GTD process but it tells little stories and anecdotes which relate to GTD. They are short columns (2-3 pages) which The Dave wrote for his online newsletters. The book consists of 52 chapters, so you can read a chapter a week. That is exactly what we do at the mailinglist. We read a chapter and discuss it during the week. We start with chapter two tomorrow so feel free to subscribe and hop aboard!</p><p>Anyway,&nbsp; last week I got pointed to an interesting PDF. It is called <a href="http://www.orgcoach.net/pdf/ram_dump.pdf"  target="_blank" target='_blank'>RAMdump.pdf</a> and it gives you startingpoints for you mental dump. Everything you have on your mind. Big, small, long, short, multi-step, anything, just dump it on paper or on your screen as a part of your collection process. The list is really big I must say but definitely worth to take a look at. I think it is a list you don&#39;t need to take a look at every week and I think it has some pointers that might give you new ideas instead of reminding you of hidden, old ideas. But <a href="http://www.orgcoach.net/pdf/ram_dump.pdf"  target="_blank" target='_blank'>take a look at it</a> and take the points you need to get started with the RAM dump of your brain. From my own experience I do have to say: Do a braindump but make sure you follow it up correctly with the processing and organizing of your open loops. And even better: Weekly Review! Otherwise you keep dumping the same open loops over and over again. Not good for your health and your own time and energy. </p><p>Do you have some tips for a good braindump (I love this word...) or collection-techniques? Let us know in the comments!&nbsp;</p><p>Have fun!</p> 
 
<p class="info">07 09 06 - 21:48 - <a href="/pivot/entry.php?id=7059&amp;w=whats_the_next_action" title="07 Sep '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7059&amp;w=whats_the_next_action#comm" title="Alex Fayle">one comment</a> <?php echo get_editentrylink("Edit", '7059'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ol>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7081">Mindmanager, an excellent GTD tool? Win free licenses!</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7080">Scrybe is the killer GTD app?</a>
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7069">6 ways to run an effective meeting </a>
<li><a href="http://punkey.com/pivot/entry.php?id=7077">Netvibes GTD tab</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7021">The 5 reasons why The Weekly Review is difficult</a>
<li><a href="http://punkey.com/pivot/entry.php?id=1726">Still digging Evernote?</a>
</ol>
Made possible with <a href="http://www.haveamint.com">Mint</a><br>
<p align="center"><img src="/images/mint-80x15.gif" width="80" height="15" align="middle" border="0" /><p><br />
<br />
</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#bill_reichart-0611030119' title='03 11 2006 - 01:19' ><b>Bill Reichart</b></a> (We have our winne&hellip;): Thanks for picking me as a winner.  &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#frank_meeuwsen-0611022209' title='02 11 2006 - 22:09' ><b>Frank Meeuwsen</b></a> (We have our winne&hellip;): OK, I found out when you use IE you &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#mike-0611022149' title='02 11 2006 - 21:49' ><b>Mike</b></a> (We have our winne&hellip;): I too have a problem getting the .mm&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#horacio-0611021442' title='02 11 2006 - 14:42' ><b>Horacio</b></a> (We have our winne&hellip;): Thanks for the contest!  As I&#8217;m disco&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#mario_from_italy-0611020944' title='02 11 2006 - 09:44' ><b>Mario from Italy</b></a> (We have our winne&hellip;): Well, the 1st thing I did this morni&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#martijn-0611012200' title='01 11 2006 - 22:00' ><b>Martijn</b></a> (We have our winne&hellip;): Congrats to all of you. I guess I&#8217;m j&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#speaker-0611010325' title='01 11 2006 - 03:25' ><b>speaker</b></a> (Mindmanager, an e&hellip;): I&#8217;ve used FreeMind 8.0 as well as a b&hellip;<br />
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>