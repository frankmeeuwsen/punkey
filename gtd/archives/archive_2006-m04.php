<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e7026"></span><div class="entry">
<h3>How is your comfortzone?</h3>
	<p>The last couple of weeks have been a rollercoaster ride. Again. It just doesn&#8217;t seem to stop and yeah, the first I seem to do is fall back in old habits. I forget I have a Next action list. I haven&#8217;t looked in it for a couple of weeks but I just look at the work at hand when I arrive at work and start right away. Weekly review? <a href="http://www.punkey.com/pivot/entry.php?id=7021&#38;w=whats_the_next_action"  target='_blank'>Nah, forget it</a>. Didn&#8217;t do that one in a month of two I think. Is that bad? I don&#8217;t know. I still get some projects done and I do feel I have enough balls up in the air to keep some projects going. Okay, i need to look at my mails a few times more than usual (now, where was that answer from Jeff I need for this task&#8230;) but since I know I can only have it in a couple of places, it doesn&#8217;t take me long to find it.<br />
One other thing is my comfortzone and my changing internal standards. I feel it dropping for the stuff around me and how I feel I need to do something to change. Let me explain this in the way The Dave does this in his GTD Fast series (Disc 5 track 4). This CD serie is not for sale any longer.</p>

  
<a href="/pivot/entry.php?id=7026&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">30 04 06 - 21:51 - <a href="/pivot/entry.php?id=7026&amp;w=whats_the_next_action" title="30 Apr '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7026&amp;w=whats_the_next_action#comm" title="Ruben Timmerman, Matthew Cornell">two comments</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '7026'); ?></span></p>
</div><span id="e7025"></span><div class="entry">
<h3>New GTD log started</h3>
	<p>I would like to welcome the people of <a href="http://www.blackbeltproductivity.net/blog/"  target='_blank'>Blackbeltproductivity.net</a> in the GTD-blogspace! Started yesterday and I look forward in what they have to offer on insights, new tips and tricks on implementing GTD.</p>

  
 
<p class="info">19 04 06 - 12:08 - <a href="/pivot/entry.php?id=7025&amp;w=whats_the_next_action" title="19 Apr '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7025&amp;w=whats_the_next_action#comm" title="Michael, Matthew Cornell">two comments</a> <?php echo get_editentrylink("Edit", '7025'); ?></span></p>
</div><span id="e7024"></span><div class="entry">
<h3>Onfolio aqcuired by Microsoft</h3>
	<p>This is quite a surprise to me. During my day off because of Easter, I cleaned my offline magazine-stack by basically throwing them all away. I was planning on doing the same with my collection of PDF&#8217;s on my laptop. But I thought &#8220;Well&#8230;There are some interesting ones but I need a system to find them on the right moment.&#8221; I immediately thought of <a href="http://www.onfolio.com/"  target='_blank'>OnFolio</a>. I used this program for a while, I even wrote <a href="http://www.technorati.com/search/onfolio?from=http://www.whatsthenextaction.com"  target='_blank'>some articles</a> about it, for instance the showdown with Evernote (<a href="http://punkey.com/pivot/entry.php?id=1758"  target='_blank'>part 1</a> and <a href="http://punkey.com/pivot/entry.php?id=1790"  target='_blank'>part 2</a>)<br />
What I need is some sort of system where I can tag documents with proper keywords and find them again when needed. Or be reminded that I have some sort of document on a topic when I am working on a project. That last one is the hardest, but that&#8217;s another topic.<br />
When I visited the site for a new download (I don&#8217;t keep earlier downloaded exe&#8217;s) I found they have been aqcuired by Microsoft. Kudo&#8217;s for them, but bummer for <em>moi</em>. Because OnFolio as I used to know, does not exist anymore. It is integrated in the Windows Live Beta Toolbar. Whatever that is, since I only use Firefox. But nowhere a sign of an earlier version of OnFolio? The forum already talks about <a href="http://www.onfolio.com/support/forums/messageview.cfm?catid=10&#38;threadid=1184"  target='_blank'>abandoned Firefox-users</a> and people are looking for alternatives. Any other thoughts here are welcome!</p>
	<p>Update: While browsing through the forum I found a link to <a href="http://www.onfolio.com/support/releasehistory/"  target='_blank'>the releasehistory</a>. Old versions of OnFolio are still available over there.</p>

  
 
<p class="info">16 04 06 - 19:33 - <a href="/pivot/entry.php?id=7024&amp;w=whats_the_next_action" title="16 Apr '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7024&amp;w=whats_the_next_action#comm" title="Diane">one comment</a> <?php echo get_editentrylink("Edit", '7024'); ?></span></p>
</div><span id="e7022"></span><div class="entry">
<h3>Chatting about GTD</h3>
	<p>I got a comment from John Ratcliffe-Lee on <a href="http://www.punkey.com/pivot/entry.php?id=7002&#38;w=whats_the_next_action"  target='_blank'>my article</a> on GTD and Backpack. He wrote an <a href="http://jratlee.newsvine.com/_news/2006/04/13/165058-campfire-gtd"  target='_blank'>excellent follow-up</a> where he describes how he uses the new 37Signal product <a href="http://www.campfirenow.com"  target='_blank'>Campfire</a> to complement his GTD-ness in Backpack/Basecamp. I don&#8217;t quite understand yet what the advantage is, but perhaps that will come in the following days.<br />
What I <em>have</em> done is using Campfire to set up some chatrooms for you all to use. You can chat about GTD or well, whatever you like. Just keep it nice and clean!<br />
I have made two chatrooms called <a href="http://gtd.campfirenow.com/98212"  target='_blank'>Basic Talk</a> and <a href="http://gtd.campfirenow.com/7683e"  target='_blank'>GTD Tips</a>. Feel free to try out Campfire with these chatrooms and let me know what you think. Maybe I will be around every now and again but I can&#8217;t promise it&#8230;<br />
Since I am on the free version of Campfire, only 4 simultaneous chatters are allowed. Which pretty much sucks &#8216;cause that is not enough for some real conversation. So if any of the <a href="http://www.37signals.com"  target='_blank'>37Signals</a> peeps are reading along with us, hook me up with a better plan so I can promote it better <img src='/extensions/emoticons/trillian/e_121.gif' alt=';-)' align='middle'/></p>

  
 
<p class="info">16 04 06 - 00:56 - <a href="/pivot/entry.php?id=7022&amp;w=whats_the_next_action" title="16 Apr '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7022&amp;w=whats_the_next_action#comm" title="John Ratcliffe-Lee">one comment</a> <?php echo get_editentrylink("Edit", '7022'); ?></span></p>
</div><span id="e7021"></span><div class="entry">
<h3>The 5 reasons why The Weekly Review is difficult</h3>
<p>As David Allen always says: The one thing that makes or breaks your implementation of GTD is the Weekly Review. Be sure to check out my article &quot;<a href="/pivot/entry.php?id=1578&w=whats_the_next_action"  target='_blank'>What IS Getting Things Done</a>&quot; to get an overview of the WR in the whole system. Nothing is more difficult to start or maintain than that Weekly Review. I find it very hard to do and keep my WR. Earlier this week I was driving on the highway, pondering about some upcoming projects and what had to be done to get them going. One thing led to another and I came up with these reasons why the WR is so hard. I think anyone has their own reasons, but they will all end up in one of these five I think. Here we go<br /><br /><strong>The 5 reasons why The Weekly Review is difficult</strong></p> 
<a href="/pivot/entry.php?id=7021&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">10 04 06 - 08:20 - <a href="/pivot/entry.php?id=7021&amp;w=whats_the_next_action" title="10 Apr '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7021&amp;w=whats_the_next_action#comm" title="Mark Polino, Ricky Spears, Grey, TomL">four comments</a> <?php echo get_editentrylink("Edit", '7021'); ?></span></p>
</div><span id="e7019"></span><div class="entry">
<h3>New features in Backpack coming up</h3>
<p>When opening up Backpack this morning I found a pleasant surprise. Click on the image</p><p><a href="http://www.punkey.com/images/request.jpg"  rel="lightbox" title=""  target='_blank'><img src="http://www.punkey.com/images/request.thumb.jpg" border="0" alt="" title="" align="" class='pivot-popupimage' /></a>&nbsp;</p><p>Yeey! I wonder what timeframe they mean by &quot;soon&quot; but it's worth waiting I guess...Want to try out Backpack in the meantime? Please use <a href="http://backpackit.com/?referrer=BPF9BJ9"  target='_blank'>this link</a>. When you sign up for a paid plan, some referralcash is flowing my way so I can pay for the <a target="_blank" href="http://www.davidco.com/seminars/seminar_the_roadmap.php">GTD Roadmap</a> when it hits Europe (Someday...Maybe...)</p> 
 
<p class="info">08 04 06 - 09:03 - <a href="/pivot/entry.php?id=7019&amp;w=whats_the_next_action" title="08 Apr '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7019&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7019'); ?></span></p>
</div><span id="e7017"></span><div class="entry">
<h3>The GTD prayer</h3>
<p>This is just brilliant. I am not christian or religious of any kind, but this is great...<br /></p><p>Our lifehacks, which art in contexts,<br />Inbox zero be thy aim.<br />Thy Kinkless done.<br />Thy Mind Sweep fun, in @work as it is in @honeydo.<br />Give us this day our next action.<br />And forgive us our open loops, as we forgive those who delete our email.<br />And lead us not into web surfing.<br />Deliver us from IM.<br />For thine is the Moleskine, the Project and the Due Date<br />For someday/maybe,<br />Allen.</p><p><strong>Update: </strong>As mentioned in the comments, I should credit the author. Absolutely right. I was a bit too fast this morning to post this. All credits for the above go to Giles Turnbull over at <a href="http://gilest.org/2006/04/gtd-prayer.html"  target='_blank'>Gilest.org</a></p> 
 
<p class="info">06 04 06 - 08:55 - <a href="/pivot/entry.php?id=7017&amp;w=whats_the_next_action" title="06 Apr '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7017&amp;w=whats_the_next_action#comm" title="Neil, rob, Frank">three comments</a> <?php echo get_editentrylink("Edit", '7017'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>