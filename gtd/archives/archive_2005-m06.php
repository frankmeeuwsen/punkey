<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e1836"></span><div class="entry">
<h3>How to download a podcast later</h3>
	<p>An excellent tip I just picked up while freestyle-surfing the net (you know, pointless click-and-reading, feels great every once in a while). Have you ever come across an mp3 file on the Internet and wanted to download it and put it on your iPod, but didn&#8217;t have time to deal with it? With the combination of del.icio.us, Feedburner and an podcatch-client you will have a nice combination. Read the fine art of tagging and linking <a href="http://avc.blogs.com/a_vc/2005/06/this_is_cool.html"  target='_blank'>over here</a>!</p>

  
 
<p class="info">09 06 05 - 22:00 - <a href="/pivot/entry.php?id=1836&amp;w=whats_the_next_action" title="09 Jun '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1836&amp;w=whats_the_next_action#comm" title="Fred Zelders">one comment</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '1836'); ?></span></p>
</div><span id="e1833"></span><div class="entry">
<h3>GTD add-in update and new magazine</h3>
	<p>In this month&#8217;s newsletter from the David Allen company some interesting news from the homebase of GTD: NetCentrics is working on the next release of the <a href="http://gtdsupport.netcentrics.com/tour/"  target='_blank'>GTD Outlook Add-In</a>, tentatively available in August. And other interesting news: GTD | Connect! will be launching this fall. It will be a monthly subscription program with teleseminars, printed newsletters, expanded e-zines, PDF&#8217;s, recorded interviews with some of the brilliant minds (not my words! &#8211; FM) in the David Allen network, special discounts on new products and new events and much more. </p>
	<p>You can subscribe to this free newsletter at the <a href="http://www.davidco.com/productivity_principles.php"  target='_blank'>David Allen-website</a></p>

  
 
<p class="info">08 06 05 - 08:43 - <a href="/pivot/entry.php?id=1833&amp;w=whats_the_next_action" title="08 Jun '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1833&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1833'); ?></span></p>
</div><span id="e1829"></span><div class="entry">
<h3>Enlarge your textarea!</h3>
	<p><img src="/images/resizer.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Sounds like a spam-message, but make sure you check this Firefox extension. I recently <a href="http://www.punkey.com/pivot/entry.php?id=1807"  target='_blank'>wrote</a> about a Greasemonkey-script to make the textarea in forumposts, weblogsoftware etc bigger, but the downside was, it also enlarges your font. Thanks to <a href="http://marcusvorwaller.com/"  target='_blank'>Marcus</a>, who responded to this, I found a better extension. <a href="http://www.extensionsmirror.nl/index.php?showtopic=2796"  target='_blank'>This extension</a> works as if you drag and resize a application-window. Excellent! Make sure you check it out!</p>

  
 
<p class="info">05 06 05 - 15:34 - <a href="/pivot/entry.php?id=1829&amp;w=whats_the_next_action" title="05 Jun '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1829&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1829'); ?></span></p>
</div><span id="e1828"></span><div class="entry">
<h3>EverNote upgrade</h3>
	<p>While processing my Inbox, I came across a note from Courtney Brigham, the Senior PR Manager from the Evernote corp. Thank you Courtney!<br />
You might remember I did a <a href="http://www.punkey.com/pivot/entry.php?id=1790"  target='_blank'>Evernote vs. OnFolio showdown</a> where Evernote was defeated at the last moment bij OnFolio. But, Evernote is new and improved! To quote Courtney&#8217;s message:</p>
	<p>&#8221;[...]I wanted to let you know that an EverNote beta update is now available and share some of its latest features and improvements with you. </p>
	<p>A few highlights include: Extensive category improvements; Backup support; Full screen support; New keyboard shortcuts (a new total of 80); File Import / Export; New date finder; Improved Outlook email transfers; Improved graphics support; Scanner support; Other improvements and bug fixes. My favorite enhancement is the new category icons (over 50), where you can easily assign icons to identifiable individual categories, such as Web Clips, Business, Personal, Ink Notes, and more.</p>
	<p>For a complete overview of the release notes, please see: <a href="http://www.evernote.com/en/products/evernote/releasenotes.php"  target='_blank'>http://www.evernote.com/en/products/evernote/releasenotes.php</a>&#8221;</p>
<p style="text-align:center;"><img src="/images/evernote_copy.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
	<p>Well, I have to say, it looks impressive. But sorry Courtney, OnFolio works like a charm for me. It has it&#8217;s crazy behaviours like eating all my RAM, but the possibilities to collect are just right for me. I will keep an eye on Evernote to see where it goes. But for now, I&#8217;m sticking to OnFolio.</p>

  
 
<p class="info">05 06 05 - 14:23 - <a href="/pivot/entry.php?id=1828&amp;w=whats_the_next_action" title="05 Jun '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1828&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1828'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>