<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e6971"></span><div class="entry">
<h3>Backpack and GTD</h3>
	<p><img src="/images/backpack_logo.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I am turning into a fan of 37signal&#8217;s product <a href="http://backpackit.com/"  target='_blank'>Backpack</a> as a tool for my personal management. It takes some time and some tweaking of the system, but we are getting there! What is Backpack? It is a web based project planning tool. No gant charts or any difficult projectmanagement-lingo, but plain ol&#8217; lists! In Backpack, you can make several pages on which you can make lists of &#8220;things&#8221;. You can make a page for a conference you are visiting (like I am next week) and on that page you can make lost of things you need to bring, emails with confirmation from the hotel and train and a map of the area. <br />
<img src="/images/backpack_emails.jpg" style="float:right;margin-left:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />If I can relate it to GTD-speak, it is a <em>Next Action List</em> and <em>Organizing tool</em> in one application. Because you can make multiple lists on one page, you can make a list of &#8220;things to remember&#8221; and before that make a list of Next Actions to make sure you have everything. <br />
Like I said, it is not a GTD-app in the pure sense of the word, but hey, what is? To me, Backpack fills my need to use it whenever and where ever I want in a way I can think of myself. Therefore, I would like to go through some of my practical uses of Backpackit.com and in the process, explain some of the features. I will end with a good/bad comparison and the conclusion. I have a Basic Account which means I have all the features of the site with a monthly payment of 5 dollars. Some of the features I mention are not available in the free version</p>

  
<a href="/pivot/entry.php?id=6971&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">28 11 05 - 00:10 - <a href="/pivot/entry.php?id=6971&amp;w=whats_the_next_action" title="28 Nov '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6971&amp;w=whats_the_next_action#comm" title="Rich, Frank Meeuwsen, Andy, Sanji, Ali Daniali, Forrest, vbnbvn">nine comments</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '6971'); ?></span></p>
</div><span id="e6970"></span><div class="entry">
<h3>The connection between GTD and Tai Chi</h3>
	<p>I have been practicing Tai Chi (or T&#8217;ai chi or Taijiquan) for a couple of years now. For me, this martial art gives me relief, a moment of meditation and it is a pool of tranquility in a hectic lifestyle. As some of you may know, Tai Chi consists of a series of movements and postures. For those interested, I practice the <a href="http://en.wikipedia.org/wiki/Yang_style_T%27ai_Chi_Ch%27uan"  target='_blank'>Yang style</a> as thought by Cheng Man-Ch&#8217;ing. This consists of 37 postures. To quote <a href="http://en.wikipedia.org/wiki/Taichi"  target='_blank'>Wikipedia</a> on this martial art:<br />
<blockquote>T&#8217;ai Chi Ch&#8217;uan is considered a soft style martial art, an art applied with as complete a relaxation or &#8220;softness&#8221; in the musculature as possible, to distinguish its theory and application from that of the hard martial art styles which use a degree of tension in the muscles.</blockquote></p>
	<p><strong>The relation between form and principle</strong><br />
<img src="/images/180pxyang_chengfu_circa_1918.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Today, during Tai Chi class, we talked about a couple of principles my teacher has tought from his teacher and so forth. And some of these principles really resonated to me when thinking about personal productivity, GTD and implementing &#8220;systems&#8221;. I am listening to the GTD Fast seminar on my way over so some of <em>those</em> principles came to mind when listening to my teacher. It all may seem really obvious and straightforward when you think of it, but I really felt something clicking in my brain about these subjects. How they connect to each other and perhaps influence one another. </p>
	<p>I will not quote the whole discussion we had but here are some of the best ones</p>
	<ul>
		<li>&#8220;When truly mastering Tai Chi, the form is no longer important. It is the principle that counts&#8221;</li>
		<li>&#8220;Even when you master the form to perfection, when you do not adhere to the principle, you will not have true Tai Chi&#8221;</li>
		<li>&#8220;The form is just the doorway to understanding true Tai Chi&#8221;</li>
	</ul>
	<p>One of the best tips I had (without my teacher knowing) was when he said: &#8220;sometimes it&#8217;s better to have as little form as possible to fully understand the principle&#8221;</p>
	<p><strong>How does this relate to GTD?</strong><br />
Well, think of the principle as the theory, the rules, the 5 stages of productivity, the mindset. Think of the form of Tai Chi as the tools, the software, the Hipster PDA etc. You can have all the software, you can have your PDA, your Moleskine, your Backpack-account and your Outlook Add-in. But when you do not fully understand the principles and how they apply to your situation, you&#8217;re toast! Simply put, you can&#8217;t get a management summary of GTD. You can check the 5 steps, but when you do not see the internal relationships, when you don&#8217;t understand the underlying principles of the how and why of these steps, you will never fully master it. That&#8217;s why I feel the last quote is so strong. You can be fully functional, totally black belt GTD with a pen and paper. Just as long as you understand the underlying principles. </p>
	<p>We talked about the different principles of Tai Chi (body alignment, calmness, moving from the centre, open-closed) and when I referred to the term &#8220;to have a mind like water&#8221;, it struck a chord at my teacher and the other students. Without explaining GTD and personal productivity, I explained what &#8220;a mind like water&#8221; means and how it fits into the philosophy of Tai Chi to me. We didn&#8217;t learn much today at class when thinking about form. But I sure learned more about the principle. And how this affects me in more ways I sometimes think about. It makes me think more about practicing Tai Chi, and practicing GTD. You can even go as far as saying that a principle as GTD can be healthy and have some meditational aspects. Think of the Zen-like feeling when your Inbox is empty. Think about the flow you can be in when checking of Next Actions.<br />
Perhaps anyone can relate to this or have some thoughts on this relationship?</p>
	<p>If you want to know more about Tai Chi, I can recommend the mentioned <a href="http://en.wikipedia.org/wiki/Taichi"  target='_blank'>Wikipedia-entry</a> or you can always check the almighty <a href="http://www.google.com/search?q=Tai+Chi+Chuan"  target='_blank'>Google</a></p>

  
 
<p class="info">15 11 05 - 22:43 - <a href="/pivot/entry.php?id=6970&amp;w=whats_the_next_action" title="15 Nov '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=6970&amp;w=whats_the_next_action#comm" title="Vlasta Bednar, Frank Meeuwsen">two comments</a> <?php echo get_editentrylink("Edit", '6970'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ol>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7081">Mindmanager, an excellent GTD tool? Win free licenses!</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7080">Scrybe is the killer GTD app?</a>
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7069">6 ways to run an effective meeting </a>
<li><a href="http://punkey.com/pivot/entry.php?id=7077">Netvibes GTD tab</a>
<li><a href="http://punkey.com/pivot/entry.php?id=7021">The 5 reasons why The Weekly Review is difficult</a>
<li><a href="http://punkey.com/pivot/entry.php?id=1726">Still digging Evernote?</a>
</ol>
Made possible with <a href="http://www.haveamint.com">Mint</a><br>
<p align="center"><img src="/images/mint-80x15.gif" width="80" height="15" align="middle" border="0" /><p><br />
<br />
</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><a href="/gtd/archives/archive_2006-m11.php">01 Nov - 30 Nov 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=6971&amp;w=whats_the_next_action#vbnbvn-0611030823' title='03 11 2006 - 08:23' ><b>vbnbvn</b></a> (Backpack and GTD): dfg dfgdfg ddfgdfgdfg dfgdfg<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#bill_reichart-0611030119' title='03 11 2006 - 01:19' ><b>Bill Reichart</b></a> (We have our winne&hellip;): Thanks for picking me as a winner.  &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#frank_meeuwsen-0611022209' title='02 11 2006 - 22:09' ><b>Frank Meeuwsen</b></a> (We have our winne&hellip;): OK, I found out when you use IE you &hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#mike-0611022149' title='02 11 2006 - 21:49' ><b>Mike</b></a> (We have our winne&hellip;): I too have a problem getting the .mm&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#horacio-0611021442' title='02 11 2006 - 14:42' ><b>Horacio</b></a> (We have our winne&hellip;): Thanks for the contest!  As I&#8217;m disco&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#mario_from_italy-0611020944' title='02 11 2006 - 09:44' ><b>Mario from Italy</b></a> (We have our winne&hellip;): Well, the 1st thing I did this morni&hellip;<br />
<a href='/pivot/entry.php?id=7083&amp;w=whats_the_next_action#martijn-0611012200' title='01 11 2006 - 22:00' ><b>Martijn</b></a> (We have our winne&hellip;): Congrats to all of you. I guess I&#8217;m j&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#speaker-0611010325' title='01 11 2006 - 03:25' ><b>speaker</b></a> (Mindmanager, an e&hellip;): I&#8217;ve used FreeMind 8.0 as well as a b&hellip;<br />
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>