<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e1802"></span><div class="entry">
<h3>It&#039;s time to move!</h3>
	<p>Noooo&#8230;.not with this weblog! But in real life! Yep, that&#8217;s the reason things have been really quiet (again) on these pages. I have been swamped with things to do for our move to a new city. If you are from The Netherlands or familiar with it, I now live in the town called Breda. It&#8217;s near the south. Actually there is a <a href="http://en.wikipedia.org/wiki/Breda_%28Netherlands%29"  target='_blank'>Wikipedia-entry about it</a>. As of this monday, me and my girlfriend will move into our new house in Utrecht, which is in <a href="http://en.wikipedia.org/wiki/Utrecht_%28city%29"  target='_blank'>the center of The Netherlands</a>. It&#8217;s actually about 60 kilometres (about 37 miles) apart from each other, but nonetheless it&#8217;s a big step for both of us. We have been living together for 6 years, but now we bought a house instead of renting one. So we need to take care of a lot of things. That&#8217;s were some of the GTD principles come in handy&#8230;</p>

  
<a href="/pivot/entry.php?id=1802&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">29 04 05 - 23:33 - <a href="/pivot/entry.php?id=1802&amp;w=whats_the_next_action" title="29 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1802&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '1802'); ?></span></p>
</div><span id="e1800"></span><div class="entry">
<h3>Monday morning observation...</h3>
	<p>Last week, while walking from the bus stop to work, I noticed a funny sticker with an URL. I wanted to remember the URL. Since my work was just around the corner, I thought to myself: &#8220;I can remember that, when I come in, I plug in my laptop and check the site. Takes me less than 2 minutes.&#8221;<br />
When I walked in, the phone rang and while plugging in the laptop I was already engaged with a client about a support issue. I totally forgot the URL.<br />
This morning, I walked past the sticker again. This time, I took out my mini-Moleskine and wrote down the URL. When I walked in, same thing happened. This time, it was a co-worker, asking for some assistance. Right now, the URL is in my notebook, my little Inbox which I check every day. I don&#8217;t have that feeling I&#8217;m missing something I need to see or check.<br />
Lesson learned: Don&#8217;t try to remember everything you see, just write it down in your trusted system and you&#8217;ll feel a lot better</p>

  
 
<p class="info">18 04 05 - 23:33 - <a href="/pivot/entry.php?id=1800&amp;w=whats_the_next_action" title="18 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1800&amp;w=whats_the_next_action#comm" title="David Keltie">one comment</a> <?php echo get_editentrylink("Edit", '1800'); ?></span></p>
</div><span id="e1799"></span><div class="entry">
<h3>One OnFolio tip on multiple PC&#039;s</h3>
	<p><img src="/images/onfolio.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I have a desktop PC and a laptop. Most of the time I use the laptop, but this weekend I was working on my desktop PC for some coding and arranging stuff for our move to Utrecht. After just one week, I realized I missed my <a href="http://www.onfolio.com/support/mozilla.cfm"  target='_blank'>OnFolio-plugin in Firefox</a> (are you reading this, <a href="http://blog.joecheng.com/index.html"  target='_blank'>Joe</a>?) so I quickly installed this. While surfing and reading some feeds, I realized I found a lot of stuff I would like to read in more detail, put in a safe place (Reference) or put under the attention of a client or co-worker. Ofcourse, I could email it all to myself at work, but why not use some of the other tools at hand? So here&#8217;s what I did: I capture links, keywords, snippets in my Main Collection in OnFolio. This collection gets published every 30 minutes or so to <a href="http://www.xs4all.nl/~punkey/"  target='_blank'>my homepage at my ISP</a>. With the inclusion of <a href="http://www.xs4all.nl/~punkey/rss.xml"  target='_blank'>a RSS feed</a>. Now, all I need to do is add this feed to my OnFolio on the laptop and I have all the information in one place. There I can use it at work, read offline, put in my reference-file or email directly. Kind of a detour-solution for a simple problem? Maybe for you, but for me, using it this way keeps it out of my sight (read: Inbox) when I&#8217;m at work and it gives me chance to process the information in a single timeframe because it&#8217;s all in the same feed.</p>

  
 
<p class="info">17 04 05 - 22:09 - <a href="/pivot/entry.php?id=1799&amp;w=whats_the_next_action" title="17 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1799&amp;w=whats_the_next_action#comm" title="wondering">one comment</a> <?php echo get_editentrylink("Edit", '1799'); ?></span></p>
</div><span id="e1798"></span><div class="entry">
<h3>A new template coming up?</h3>
	<p><img src="/images/slickdemo.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />This evening, I was just browsing my Reference file because I was looking for the tagging-extension in Pivot. If you&#8217;re not familiar with the concept of tagging, be sure to check out <a href="http://www.technorati.com/help/tags.html"  target='_blank'>this explanation</a> on Technorati. I would like my posts to be in the Tagging-cosmos too. Even more then <a href="http://www.technorati.com/tag/GTD"  target='_blank'>they already are</a> ... Now, a Dutch guy made an extension for Pivot once. Just one Google search and shazam! <a href="http://www.i-marco.nl/weblog/pivot/entry.php?id=96"  target='_blank'>there it was</a>. But unfortunately the server where the installation-files can be found, is down right now. So no tagging for me right now. As I was looking through i-Marco&#8217;s website (I believe I met the guy somewhere) I noticed the excellent design of his blog. Turns out he made it himself and <a href="http://www.i-marco.nl/slickdemo/"  target='_blank'>he turned it</a> into a Pivot-template. Yay! I love the template and the slick look of it. When the server with the template and the tagging-extension is up again, be sure to find some new juice over here <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>

  
 
<p class="info">17 04 05 - 21:46 - <a href="/pivot/entry.php?id=1798&amp;w=whats_the_next_action" title="17 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1798&amp;w=whats_the_next_action#comm" title="Marco">one comment</a> <?php echo get_editentrylink("Edit", '1798'); ?></span></p>
</div><span id="e1795"></span><div class="entry">
<h3>Blink and GTD</h3>
	<p><img src="/images/blink.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />I was browsing my feeds this evening, basically just chilling on the couch. I read <a href="http://www.jambo.net/blog/archives/2005/04/blink.html"  target='_blank'>a nice article</a> on the weblog Face-to-Face about the new book of Malcolm Gladwell <a href="http://www.amazon.com/exec/obidos/tg/detail/-/0316172324/104-9454513-0227928?v=glance"  target='_blank'>&#8216;Blink&#8217;</a>. I was already curious about this book, because of his earlier book <a href="http://www.amazon.com/exec/obidos/tg/detail/-/0316346624/ref=pd_sim_b_1/104-9454513-0227928?%5Fencoding=UTF8&#38;v=glance"  target='_blank'>&#8216;The tipping point&#8217;</a> and I just got more curious. Because the book deals with decisions we make without thinking. And this got me thinking (no pun intended) about how this relates to Getting Things Done. Since GTD involves a lot of thinking sometimes (processing, reviewing, five phases of planning) I can imagine how you can change your mind while thinking about Next Actions. But is this the right decision then? I haven&#8217;t read the book yet, but as I read the editorial review, it seems you make the right decision in the first couple of seconds. So how does longer and more intense thinking about projects (What is the next physical action? How can I move this forward?) change this? Interesting thought. I should read the book first before I talk more about this I guess. Perhaps someone already read the book and has an opinion? I look forward to your comments.</p>

  
 
<p class="info">16 04 05 - 21:50 - <a href="/pivot/entry.php?id=1795&amp;w=whats_the_next_action" title="16 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1795&amp;w=whats_the_next_action#comm" title="Dwayne Melancon">one comment</a> <?php echo get_editentrylink("Edit", '1795'); ?></span></p>
</div><span id="e1793"></span><div class="entry">
<h3>Easy reference thanks to OnFolio</h3>
	<p>Will you read raving reviews for OnFolio from now on? No, of course not. But this is one thing I had to share with you. While I am writing this in a OF-note on the train, I am reading a saved website in a OF-collection. The site is <a href="http://www.gyford.com/phil/notes/2005/01/03/getting_things_don.php"  target='_blank'>the notes from Phil Gyford</a> on the GTD-book. Since one of my co-workers borrowed the book, I don&#8217;t have any reference to fall back into every now and then. But reading Phil&#8217;s summary, it really gives me the right pointers and little lists to remind myself of various aspects of GTD. For instance the 5 phases of planning. Give it a read and put the page in a reference-bucket with the right keywords/tags for future reference!</p>

  
 
<p class="info">14 04 05 - 22:26 - <a href="/pivot/entry.php?id=1793&amp;w=whats_the_next_action" title="14 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1793&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1793'); ?></span></p>
</div><span id="e1792"></span><div class="entry">
<h3>Developments in GTD (short)</h3>
	<p>Just a short note to let my readers know what&#8217;s going on in GTD-country over here. Some pretty interesting issues I might focus on in a later post. Here they are
	<ul>
		<li>As of today, I have converted 2 co-workers into reading the book, 1 co-worker (the CEO actually) to implement GTD in his professional work and 2 projectmanagers to be at least curious about this whole &#8220;Getting Things Done thingy&#8221;... Woohoo!</li>
		<li>Which gives me more power and strength to implement and hold on to the principles here at work. Actually, conversations between me and my co-workers now focus more on Next Actions and physical appointments instead of vague to do lists. What a progress!</li>
		<li>I now have two Moleskines. It gets worse! It&#8217;s an addiction! I have a large and a little one. The little one is for on the road. For small thoughts, Inbox-material etc. The large one has the destination to become more of a lifejournal. Now I need a life&#8230;<img src='/extensions/emoticons/trillian/e_121.gif' alt=';-)' align='middle'/></li>
		<li>We plan on using <a href="http://www.salesforce.com"  target='_blank'>salesforce.com</a> for our projects and sales-opportunities (and more). I foresee an issue with my @waiting for list in Outlook. Anyone have any experience on this topic?</li>
		<li>My desk is empty! I planned two hours to clean up, made a dedicated In-basket, filled my filecabinet with manila-folders and projectfiles and started fresh.</li>
		<li>Now I need to find the balance between have-to-do work that day and emergency-jobs that come in between. I find myself re-negotiating a lot of appointments with myself. </li>
	</ul></p>
	<p>One appointment I made with myself is to post more often. I see that visits and interest in this weblog has somewhat declined and I don&#8217;t like that! So I need to regain my audience again. Nice project!</p>

  
 
<p class="info">14 04 05 - 16:30 - <a href="/pivot/entry.php?id=1792&amp;w=whats_the_next_action" title="14 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1792&amp;w=whats_the_next_action#comm" title="Adam">one comment</a> <?php echo get_editentrylink("Edit", '1792'); ?></span></p>
</div><span id="e1790"></span><div class="entry">
<h3>Showdown 2: Using OnFolio</h3>
<p style="text-align:center;"><img src="/images/banner_evernote_onfolio.gif" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
	<p>Finally! Here it is! Man, it took some time already! Almost a full month between the two showdowns. I am in deep deep shame. And yes, I have fallen off the GTD-bandwagon again. It has been so hectic and such a frustating time for me the last month that working on a mind-like-water technique was well, the last thing on my mind. It kinda worked like water, you know, looking for the easiest way, with the least loss of energy. That&#8217;s how I worked the last couple of weeks. Just like in the old days. Because I didn&#8217;t have to think about my actions, I just let everything in my Inbox and solved it from there. No defer, just crisis management. But hey. I have seen the light again! Wanna know why? I&#8217;ll tell you in another post. But right now, let&#8217;s focus on our showdown between OnFolio and Evernote.<br />
Do you remember what the initial plan was? If not, please read the following articles for a history on this Showdown:</p>
	<ul>
		<li><a href="http://www.punkey.com/pivot/entry.php?id=1726"  target='_blank'>Still digging Evernote?</a> on my discovery of the <em>Nine Inboxes</em></li>
		<li><a href="http://www.punkey.com/pivot/entry.php?id=1745"  target='_blank'>Evernote vs. OnFolio</a> on my decision for the showdown</li>
		<li><a href="http://www.punkey.com/pivot/entry.php?id=1758"  target='_blank'>Showdown 1: Using Evernote</a> on my review of Evernote</li>
	</ul>
	<p>Due to servermishaps at work and the following synchronization errors, I didn&#8217;t have quite the time and possibilities to really dig into OnFolio and all of it&#8217;s features. So I found out some of the features while writing this review. Hey, I&#8217;m not a professional reviewer OK? </p>
	<p>And right now we are gonna jump into OnFolio to see what it&#8217;s all about!</p>

  
<a href="/pivot/entry.php?id=1790&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">11 04 05 - 23:02 - <a href="/pivot/entry.php?id=1790&amp;w=whats_the_next_action" title="11 Apr '05">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1790&amp;w=whats_the_next_action#comm" title="Joe Cheng, N">two comments</a> <?php echo get_editentrylink("Edit", '1790'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>