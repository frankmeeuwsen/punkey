<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e7057"></span><div class="entry">
<h3>Shortcut Sunday #3: MS Outlook</h3>
<p>In this edition of Shortcut Sunday I want to touch on one of the most used programs in the GTD-sphere (I think): Microsoft Outlook. Because of the tight integration of email, calendar, contacts and tasks, Outlook provides a good solid ground to at least start a digital GTD-system. I use Outlook purely in a business environment. for personal email I have no desktop emailclient, I only use Gmail. With Outlook, I also had the <a href="http://gtdsupport.netcentrics.com/home/"  target="_blank" target='_blank'>GTD addin</a> installed. But we are moving to a new webbased projectmanagement/CRM system, which makes using my GTD addin pretty obsolete. But I can still recommend it. Outlook also has some nice keyboard shortcuts so you can change swiftly from email to calendar etc. Here they are...</p> 
<a href="/pivot/entry.php?id=7057&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">27 08 06 - 20:52 - <a href="/pivot/entry.php?id=7057&amp;w=whats_the_next_action" title="27 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7057&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '7057'); ?></span></p>
</div><span id="e7056"></span><div class="entry">
<h3>GTDGmail is coming?</h3>
<p>From <a href="http://www.gtdgmail.com/"  target='_blank'>their website</a>: &quot;GTDGmail is a Firefox extension that integrates the highly effective methodology of &quot;Getting Things Done&quot; into the popular email service Gmail.&quot;&nbsp; Well, that sure looks interesting to me. Too bad you can&#39;t download the extension yet but you an sign up for the notification if they go live. I wonder how this extension will live next to my <a href="http://persistent.info/archives/2005/12/23/greasemonkey"  target="_blank" target='_blank'>Gmail Macros</a> for easy archiving and keyboard-usability (you can see a Shortcut Sunday coming for that one don&#39;t you?)</p> 
 
<p class="info">22 08 06 - 08:48 - <a href="/pivot/entry.php?id=7056&amp;w=whats_the_next_action" title="22 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7056&amp;w=whats_the_next_action#comm" title="Dave">one comment</a> <?php echo get_editentrylink("Edit", '7056'); ?></span></p>
</div><span id="e7055"></span><div class="entry">
<h3>Shortcut Sunday #2: Winamp</h3>
<a href="http://www.Winamp.com"  target="_blank" target='_blank'>Winamp</a> is one of those programs you&#39;d wish Microsoft put in Windows itself instead of that pesky Windows Media Player. Winamp is of the first apps I reinstall after a fresh start with Windows or otherwise. One of the finest features in Winamp is the way to configure the shortcut keys for quick acces to the most common tasks. So for this second shortcut post, let&#39;s look at the way I have configured Winamp for my best use. 
<a href="/pivot/entry.php?id=7055&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">20 08 06 - 16:49 - <a href="/pivot/entry.php?id=7055&amp;w=whats_the_next_action" title="20 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7055&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7055'); ?></span></p>
</div><span id="e7054"></span><div class="entry">
<h3>GTD Connect to expensive?</h3>
<p>Michael <a href="http://www.blackbeltproductivity.net/blog/08-15-2006/gtd-connect-is-here/"  target="_blank" target='_blank'>informs us</a> over at Blackbelt Productivity about the new membership program from David Co. called <a href="http://www.davidco.com/connect/"  target="_blank" target='_blank'>GTD Connect</a>. It talks about the steep price of 48 dollars a month to get all the benefits of this program like a library with audio, video and print, Weekly Review emails, store promotions and The Dave as your personal coach. The comments at the article are twofold but most of them think the price is too high. Mostly because, well, you can get virtually anything on the web for free. So why pay for it? </p><p style="text-align:center;"><img src="/images/gtdconnect.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
<p>I can relate to that, but on the other hand, I can understand the price range and the people David Co wants to reach. I don&#39;t think they want &quot;us&quot; (the GTD afficionado&#39;s) as their member as primary targetgroup. Not that &quot;they&quot; hate &quot;us&quot;. I hope. But we can find our way online, subscribe to newsgroups, read and write blogs, share konwledge. I think David Co. wants to reach a broader audience who simply don&#39;t have the time, energy, focus or knowledge to engage in this online conversation and want everything in one place. Most CEO&#39;s and high-end managers could really benefit from this program. The price of 48 dollars a month (tax deductable in a corporate environment I think) is not that much for that targetgroup. But on the other hand, it would be nice to see some sort of review of this program by GTD-bloggers to see what it really is and if it really is better/more interesting than what&#39;s to find on the Web for free. I see <a href="http://blogs.officezealot.com/marc/"  target="_blank" target='_blank'>Marc Orchant</a> is a writer for the program, so perhaps he can talk about it?</p> 
 
<p class="info">20 08 06 - 11:18 - <a href="/pivot/entry.php?id=7054&amp;w=whats_the_next_action" title="20 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7054&amp;w=whats_the_next_action#comm" title="Marc Orchant, Dave, Frank Meeuwsen, Lisa, Alex Fayle">eleven comments</a> <?php echo get_editentrylink("Edit", '7054'); ?></span></p>
</div><span id="e7053"></span><div class="entry">
<h3>A new profession: The Professional Simplifier</h3>
One of the fun things about blogposts is you can talk about a subject that has been posted long ago and still be current about it. Not all blogs talk about the here and now. Most of them address issues that are of all times and the ideas need to be spread to a wider audience. <br />Today I want to give you a translation of a dutch blogpost by my good cyberfriend <a href="http://www.annedienhoen.nl/"  target="_blank" target='_blank'>Annedien Hoen</a>. Eventhough we never met in person we have some sort of email/social network contact about GTD. I call her a uber-networker, but she&#39;s also a devoted productivity geek. <br />In january of this year she wrote <a href="http://justienmarseille.blogspot.com/2006/01/nieuw-beroep-professioneel.html"  target="_blank" target='_blank'>a blogpost about a new profession</a> she would like to see. The Professional Simplifier. I have the permission and the honor to translate this article in English and present her idea to my readers. So...what <em><strong>is</strong></em> a Professional Simplifier? 
<a href="/pivot/entry.php?id=7053&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">17 08 06 - 23:26 - <a href="/pivot/entry.php?id=7053&amp;w=whats_the_next_action" title="17 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7053&amp;w=whats_the_next_action#comm" title="Alex Fayle">one comment</a> <?php echo get_editentrylink("Edit", '7053'); ?></span></p>
</div><span id="e7052"></span><div class="entry">
<h3>Shortcut Sunday #1: Windows XP</h3>
<p>I felt it was time for a new serie here on What&#39;s The Next Action. Since one of the great timesavers in my life is the use of keyboard-shortcuts I thought to myself: Why not share my joy with the rest of the GTD community?</p><p>So here we are with the first installment of <strong><em>Shortcut Sunday</em></strong>. Every sunday for the coming weeks I will take one of my programs and let you in on some of the shortcuts I use, how you can find them and sometimes, how you can tweak them. Because that is what makes a great program fabulous: Tweakable shortcuts. More on that in coming episodes. Well, let&#39;s kick off with the first list of shortcuts. And why start with a program when we can start with the OS that runs all of them: Windows XP. Sorry Mac and Linux fans, since I don&#39;t use a Mac or *nix-based machine for a longer period of time I am not known with the shortcuts in those OS&#39;s.</p> 
<a href="/pivot/entry.php?id=7052&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">13 08 06 - 15:38 - <a href="/pivot/entry.php?id=7052&amp;w=whats_the_next_action" title="13 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7052&amp;w=whats_the_next_action#comm" title="Patrick, Frank Meeuwsen, Hyperreality">three comments</a> <?php echo get_editentrylink("Edit", '7052'); ?></span></p>
</div><span id="e7050"></span><div class="entry">
<h3>Show me your Inbox and I tell you who you are</h3>
<p>The Northwest Florida Daily News runs <a target="_blank" href="http://www.nwfdailynews.com/articleArchive/aug2006/inboxsaysaboutyou.php">a little article</a> on how your Inbox reflects your state of mind. How organized are you in life is reflected on the number of messages in you Inbox. Are you a hoarder or a deleter? There is some advice from consultants and writers of books for the chronically disorganized</p>
<blockquote>Because &quot;inboxes are metaphors for our lives,&quot; Dr. Greenfield says, there's no  cure-all solution to inbox management. We're all too different. But he believes  an awareness of our inbox behavior can help us better understand other areas of  our lives. </blockquote><br /></p><p>Perhaps some hoarders could use my <a target="_blank" href="http://punkey.com/pivot/entry.php?id=6965">8 steps to get an empty Inbox</a> or how you can <a target="_blank" href="http://punkey.com/pivot/entry.php?id=1768">uncheck the desktop alerts</a> every time a new message arrives...Or check <a href="http://punkey.com/pivot/entry.php?id=6967"  target='_blank'>this simple Outlook hack </a>on subjectlines!</p> 
 
<p class="info">11 08 06 - 15:06 - <a href="/pivot/entry.php?id=7050&amp;w=whats_the_next_action" title="11 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7050&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '7050'); ?></span></p>
</div><span id="e7049"></span><div class="entry">
<h3>Three questions to ponder about</h3>
<p>I always wanted to use the word &quot;ponder&quot; in a blogpost, for obvious <a href="http://en.wikiquote.org/wiki/Pinky_and_the_Brain"  target="_blank" target='_blank'>Pinky and the Brain</a> reasons... <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p><p>But during my time off, I had some encounters with situations in which I thought to myself: &quot;Hmmm...now how would a true blackbelt Lifehacker wrestle himself through these issues?&quot; So instead of trying to find all the answers myself, I turn to the wisdom of the crowd. My valuable readers, can you help me answering these questions?</p> 
<a href="/pivot/entry.php?id=7049&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">11 08 06 - 09:36 - <a href="/pivot/entry.php?id=7049&amp;w=whats_the_next_action" title="11 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7049&amp;w=whats_the_next_action#comm" title="GeeSpot, Bryan Villarin, Alexia, Frank Meeuwsen, Leonard Bick">five comments</a> <?php echo get_editentrylink("Edit", '7049'); ?></span></p>
</div><span id="e7048"></span><div class="entry">
<h3>Quicklogger versus pen and paper.</h3>
<p>OK, no means to diss anyone at the excellent <a href="http://lifehacker.com/"  target="_blank" target='_blank'>Lifehacker</a> crew (especially Gina!) but I tried the Quicklogger script which was featured <a href="http://lifehacker.com/software/top/geek-to-live--quicklog-your-work-day-189772.php"  target="_blank" target='_blank'>last week</a> and there is a very very obvious reason why it just doesn't work for me. First a little introduction in what we're talking about. Quicklogger is a VB script which gives you the opportunity to log your workday to the minute with a very simple popup script (Windows only). The logging takes place in a textfile. So it's all very lowtech and very lifehackery. Which made me want to try it. I have a major problem with logging my workhours. My job involves a lot of switching between projects, meeting, calling and other important grown-up stuff you're supposed to do as a Brand Director. Most of it are billable hours and I need to update my timesheet every now and again so I can eat at the end of the month...So I need some sort of system which makes me remind to fill in my hours.</p><p style="text-align:center;"><img src="/images/quicklogger.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
<p>My way of working is also what makes the Quicklogger not very handy for me. It works very linear which means you cannot make any blocks of time in the past. Every time you log a project&nbsp;or an activity, it accumulates on the previous one. This is not an ideal situation for me, since I can have 5 or 6 different tasks in a period of time and simply not in the opportunity to log them on my PC. </p><p>Which brings me to the second reason: Last week, the script is updated with, <a href="http://lifehacker.com/software/exclusive-lifehacker-download/geek-to-live-quicklogger-redux-192144.php"  target="_blank" target='_blank'>amongst other</a>, the possibility to popup the loggerscreen every 15 minutes or so. Since I am not behind my PC all day, this is just not a big help. I would still need to go back in time and make previous logs after a period of time.</p><p>So after trying the script for two days I stepped back to even moe low key. Pen and paper. For some reason, this works perfect. I have a reminder in my calendar to print an Excelsheet every monday. I print it 5 times and jam a staple through it. On the excelsheet my workday is divided in parts of 15 minutes with the possibility to add a description for every quarter of an hour. At the end of the day, or after a couple of days, I update my intranet-based timesheets with the written ones. The biggest advantage of paper is I am not bounded by a script or time. I can jot down a quick message in one block of time. Use abbreviations, point to other projects on the same sheet and most of all, go back in time during a day. So at lunch I fill in the gaps from 9 to lunch from memory and that's that. I do the same at the end of the day or in some discretionairy time (for instance, while on hold on the phone). Where is this paper? It's just on my desk. Since the rest of my desk is very tidy (thank you The Dave for projectfolders...) it's not easy to miss the stapled stack of paper. Very easy, very lowtech and very very flexible.</p> 
 
<p class="info">09 08 06 - 23:30 - <a href="/pivot/entry.php?id=7048&amp;w=whats_the_next_action" title="09 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7048&amp;w=whats_the_next_action#comm" title="Joe, Frank Meeuwsen">two comments</a> <?php echo get_editentrylink("Edit", '7048'); ?></span></p>
</div><span id="e7047"></span><div class="entry">
<h3>It&#039;s time to return!</h3>
<p>Well, I'm back! Not that I have been away, but I feel it's time again to share my GTD thoughts and practices with you. The renovation of our house is nearly done, sopme parts are still not finished but we can live in it. The baby is not born yet but we are getting close. It should happen sometime the coming weeks.</p><p>I have collected some subjects on which I will write the coming weeks. It includes ways to form a habit, why you always need what you don't have, a new CRM system at work, when to clean your inbox and some thoughts on the biggest procrastinator of them all. And ofcourse my own struggle with GTD and staying on the wagon. I'm still no blackbelt, but boy am I trying hard. Hope to see you all back here real soon!</p> 
 
<p class="info">07 08 06 - 11:03 - <a href="/pivot/entry.php?id=7047&amp;w=whats_the_next_action" title="07 Aug '06">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=7047&amp;w=whats_the_next_action#comm" title="Arnout">one comment</a> <?php echo get_editentrylink("Edit", '7047'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>