<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>What's the next action - A weblog about Getting Things Done</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Archive of What's the next action" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/mojito_structure.css" media="screen" />
 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/WhatsTheNextAction"/>
<script src="/mint/?js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 <h1><a href="/gtd/index.php">What's the next action</a></h1>
 <h5>A weblog about Getting Things Done</h5>
</div>
<div id="main">
 <span id="e1610"></span><div class="entry">
<h3>A note when on vacation...</h3>
	<p>Just a reminder if you&#8217;re going on a holiday and you think you don&#8217;t need a thing like a little notebook. Please buy one! We are roadtripping in New Zealand for 5 days now and I&#8217;m getting sick of myself of not having a little notebook for URL&#8217;s, mailadresses and phone numbers. So I&#8217;m going to buy one tomorrow <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/><br />
Just wanted to let you know!</p>

  
 
<p class="info">17 12 04 - 07:12 - <a href="/pivot/entry.php?id=1610&amp;w=whats_the_next_action" title="17 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1610&amp;w=whats_the_next_action#comm" title="Stu Schaff">one comment</a> <?php 
DEFINE('INWEBLOG', TRUE);
$weblog='whats_the_next_action';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><?php echo get_editentrylink("Edit", '1610'); ?></span></p>
</div><span id="e1605"></span><div class="entry">
<h3>I&#039;m going on a holiday</h3>
	<p>It was only a short introduction from me to my new visitors o this weblog. But I am leaving on a 3 week holiday to New Zealand tomorrow. So I will not be updating this weblog for that period. Ofcourse GTD helped me quite a lot during the preparation of the travel and now it is time to leave!<br />
I will be back on this weblog after January 10th. Hope to see you all then!</p>

  
 
<p class="info">11 12 04 - 17:32 - <a href="/pivot/entry.php?id=1605&amp;w=whats_the_next_action" title="11 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1605&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1605'); ?></span></p>
</div><span id="e1601"></span><div class="entry">
<h3>Using Slogger as a webfeed-builder</h3>
	<p>I got a question how I configured <a href="http://www.kenschutte.com/firefoxext/"  target='_blank'>Slogger</a> to make RSS-feeds (we call them webfeeds in the Netherlands, but oh well). When I wrote the mail, I thought to myself: Why not share it with everyone? So if you use Slogger and would like to make RSS feeds of your saved pages wo you can read them in your feedreader. Check this little tutorial with pictures!</p>

  
<a href="/pivot/entry.php?id=1601&amp;w=whats_the_next_action#body"  >More >></a> 
<p class="info">08 12 04 - 18:56 - <a href="/pivot/entry.php?id=1601&amp;w=whats_the_next_action" title="08 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1601&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1601'); ?></span></p>
</div><span id="e1588"></span><div class="entry">
<h3>This is cool!</h3>
	<p>OK, one non-GTD link for today :<del>) I just saw this weblog is mentioned on the <a href="http://del.icio.us/popular/"  target='_blank'>del.icio.us/popular</a> list. Woehaa! that&#8217;s cool! I made a screenshot in case it&#8217;s gone tomorrow ;</del>)<br />
<p style="text-align:center;"><a href="http://www.punkey.com/images/booiiinnnggg.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9ib29paWlubm5nZ2cuanBn&w=699&h=445&t=','imagewindow','width=699,height=445,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'><img src="http://www.punkey.com/images/booiiinnnggg.thumb.jpg" border="1" alt="" title=""  class='pivot-popupimage'/></a></p>

	<p>As Adam Curry would say in <a href="http://www.dailysourcecode.com/"  target='_blank'>his podcasts</a>: booooiiiiinnnnnggggg!!!</p>

  
 
<p class="info">06 12 04 - 12:09 - <a href="/pivot/entry.php?id=1588&amp;w=whats_the_next_action" title="06 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1588&amp;w=whats_the_next_action#comm" title="cyberhill, N">two comments</a> <?php echo get_editentrylink("Edit", '1588'); ?></span></p>
</div><span id="e1586"></span><div class="entry">
<h3>Advanced Workflow Diagram</h3>
	<p>Tip of the day: You might want to check out this <a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Advanced Workflow Diagram</a> (PDF) for the GTD system if you are a visual learner! you&#8217;ll still need the book for all the nuts and bolts. You can <a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&#38;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>order</a> it at Amazon! Yes, that is a Amazon Allianced partner link&#8230;</p>

  
 
<p class="info">05 12 04 - 22:30 - <a href="/pivot/entry.php?id=1586&amp;w=whats_the_next_action" title="05 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1586&amp;w=whats_the_next_action#comm" title="">No comments yet</a> <?php echo get_editentrylink("Edit", '1586'); ?></span></p>
</div><span id="e1583"></span><div class="entry">
<h3>My setup for GTD</h3>
	<p>I want to give you a glimpse of the setup I have on my laptop to use the GTD system. </p>
	<p><strong>Next Actions</strong><br />
The center of the GTD universe is my Outlook XP with the <a href="http://gtdsupport.netcentrics.com/updates/"  target='_blank'>GTD add-in toolbar</a> (<a href="http://www.punkey.com/images/gtdaddin.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9ndGRhZGRpbi5qcGc%3D&w=617&h=57&t=','imagewindow','width=617,height=57,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>popup</a>). You can try the GTD toolbar for 30 days with the full features before you buy it. I think it&#8217;s worth it&#8217;s money, but if you want, you can build the features in the toolbar yourself. But I don&#8217;t have the knowledge to do that, so I use the toolbar. The toolbar gives you a quick way to empty your Inbox and delegate, defer, file your mail or give them a Next Action. With the add in you receive a PDF with all features explained. The toolbar is not strict in its usage. Meaning, you can make your own actions, projects and tweak it to your own situation. I am still working on that. Especially the relationship between filing mail and reroute it to the right project. The other problem I am facing is working both on- and offline with my laptop. The toolbar makes two XML files with settings (online and offline, see this <a href="http://www.punkey.com/images/gtdsettings.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9ndGRzZXR0aW5ncy5qcGc%3D&w=351&h=152&t=','imagewindow','width=351,height=152,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>popup</a>) and for some reason I can&#8217;t get it synchronized properly when I change projectnames or add/modify tasknames. I get a settings error and have to manually update the task actions. It would be nice if the add-in could automatically detect the changed names, but I feel it has something to do with the Exchange server we are on at work.<br />
In Outlook, I also use the excellent <a href="http://www.lookoutsoft.com/Lookout/"  target='_blank'>Lookout search addin</a>. It gives me unprecedented power to search not only in my own Outlook folders, but also our serverside Exchange folders and filefolders. It is a great tool and if you are a email poweruser like I am, I can recommend this tool!</p>
	<p><strong>RSS</strong><br />
For my RSS feed I use <a href="http://www.newzcrawler.com"  target='_blank'>Newzcrawler</a>. For me it is a great newsreader. I have tried numerous others and all have their own advantages and strange behaviours. Newzcrawler for me makes a great program because of the use of the keyboard. Since I read a lot of newsfeeds offline while commuting, I don&#8217;t have a mouse with me. Using the keypad on a laptop is not my favorite device, so I like to use the keyboard. I can navigate quick and esasy through the feeds and individual posts. <br />
I have tried <a href="http://www.newsgator.com"  target='_blank'>Newsgator</a>&#8221; in Outlook. It&#8217;s features are about the same as Newzcrawler, but the use of the keyboard in Outlook is dramatic. Every time I switch from one map to the other, Outlook completely loses focus and I have to use the keypad to select the right map or post. Very inefficient and time consuming. To bad, since I use my RSS feed a lot to find interesting articles to post, get new thoughts and ideas I can incorporate in my work. To have that directly in Outlook (with the GTD addin) would be a great help.</p>
	<p><strong>Notes</strong><br />
I am evaluating <a href="http://www.evernote.com/en/"  target='_blank'>Evernote</a> as my notetaker on the laptop. I use it to copy and paste newitems from Newzcrawler that can be of use. I am not sure if I keep using it. It would be nice to have some sort of direct link to the Tasks in Outlook, but I hate the switching and <em>direct</em> deciding what to do with individual Tasks. As you can see in <a href="http://www.punkey.com/images/evernote.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9ldmVybm90ZS5qcGc%3D&w=703&h=503&t=','imagewindow','width=703,height=503,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>this screenshot</a>, I keep Newzcrawler and Evernote side by side so I can drag and drop selections of text into Evernote. When I am done, I can categorize articles and decide what to do with them. Evernote also gives me additional features as template notes (to do lists, shopping lists) but I find myself not using that very often. </p>
	<p><strong>Extensions</strong><br />
I use <a href="http://www.spreadfirefox.com/"  target='_blank'>Firefox</a>. Period. I only use Internet Explorer to read my webmail (Outlook Exchange) but for all the rest, I use Firefox. So I can also have a wide choice of extensions. I have two favourite extensions: <a href="http://easygestures.mozdev.org/"  target='_blank'>EasyGestures</a> and <a href="http://www.kenschutte.com/firefoxext/"  target='_blank'>Slogger</a>. The first gives you ease of navigation with mouse gestures. It requires some tweaking of the default settings and some training but it&#8217;s worth it! Slogger creates a complete backup of your browsing history and is very, very customizable. How do I use it? When I see a webpage I would like to read in detail (a long post, productinformation, features, a forum) I click on a button and Slogger makes a backup on my laptop. In the Slogger options, I made a RSS feed of the saved pages. I read the RSS feed in my newsreader. Since it is a habit to read my newsfeeds, I don&#8217;t have to remind myself to read the saved pages in any other way. It may sound strange, but hey! It works for me!</p>
	<p><strong>Other tools</strong><br />
I have two other tools. One is a small notebook and a pen I keep in my coat. When I am walking outside or not in the situation to grab my laptop, I just write the thoughts and (try!) to process them once a day. And I do mean <em>try</em> because I forget it a lot of times. I need to remind myself more often to process these notes.<br />
Another tool I have is a read/reviw folder to keep in my bag. It has multiple tabs so I can keep eveything that is &#8220;to read&#8221; in the front and process it accordingly. I have a tab for &#8220;delete&#8221;, &#8220;unsubscribe&#8221;, &#8220;file&#8221; and some special tabs like &#8220;material to write about&#8221;, &#8220;presentation material&#8221; and &#8220;to discuss&#8221; with my colleagues.</p>
	<p><strong>At home</strong><br />
This is still unfound territory <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/> At my home PC I don&#8217;t have Outlook but Thunderbird so I can&#8217;t use the GTD addin. I still have to find good software I can use at home and have some sort of transparant sync-mode with my laptop. I will look into this after my vacation, so that is a project for 2005. And I would like to implement the GTD system here together with my girlfriend. She is reading the book now, so within a couple of weeks we have a perfectly running household! <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>

  
 
<p class="info">05 12 04 - 21:19 - <a href="/pivot/entry.php?id=1583&amp;w=whats_the_next_action" title="05 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1583&amp;w=whats_the_next_action#comm" title="Bob, Rick Cecil">two comments</a> <?php echo get_editentrylink("Edit", '1583'); ?></span></p>
</div><span id="e1580"></span><div class="entry">
<h3>A small victory</h3>
	<p>Just to let you know how GTD can help you also in little pieces: The last couple of days, I had this task to complete for a project. Really simple, processing notes into a new project description. But because it was so small and I have a lot of other projects waiting, I kept procrastinating the task. The task kept lingering on my desk, waiting to be completed. This evening I was cleaning out my desk and bumped into it again. Since I was waiting for my ride home I thought &#8220;You know what, if i just check <em>what</em> has to be done exactly, I can do it tomorrow&#8221;. So I looked at the notes and thought to myself &#8220;This is not hard, it&#8217;s really easy!&#8221; So within ten minutes I had the notes processed in a new project description, emailed the client for approval and made a &#8220;Waiting for&#8221; task for the answer. All in 15 minutes! Just by really picking up the notes and thinking about it.<br />
The feeling was great!</p>

  
 
<p class="info">01 12 04 - 23:56 - <a href="/pivot/entry.php?id=1580&amp;w=whats_the_next_action" title="01 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1580&amp;w=whats_the_next_action#comm" title="Nash K, N">two comments</a> <?php echo get_editentrylink("Edit", '1580'); ?></span></p>
</div><span id="e1578"></span><div class="entry">
<h3>What IS Getting Things Done?</h3>
<p>Since this weblog is dedicated to the noble art of Getting Things Done, I think it&rsquo;s a good idea to first talk about what GTD actually is. <br /> GTD can be best described as a disciplined process to manage your work, information and time more effectively and easy. The main thought around the process to have a &ldquo;mind like water&rdquo;. Because you have a trusted system with GTD where all you to do&rsquo;s and actions are stored, you can have clear thoughts on your projects, your work and your life. <br /> The system is based around a 5 word mantra:</p> 	<ul> 	<li>Collect</li> 		<li>Process</li> 		<li>Organize</li> 		<li>Review</li> 		<li>Do</li> 	</ul>  	<p>First, you <strong>collect</strong> all open ends in a consistent way and in a trusted place, your &ldquo;Inbox&rdquo; This may be a virtual and/or a real Inbox. The collecting process makes you go through literally <em>everything</em> in your personal and professional life that has to be done. To give an example. After just 10 minutes of sitting down and writing my to do&rsquo;s down, I made a list of about 75 items which were just hanging in my head. Keep in mind that this is just collecting. There is no need to actually do something with it.<br /> </p><p><strong>Processing </strong>makes you ask one of the most important questions for every item in your Inbox: &ldquo;What is it?&rdquo;. Take the time to understand the essence of each item in your Inbox and decide the outcome that this information should lead to and the next step towards that outcome. <br /> If you answers determine into which bucket the item goes. In the chart below you will find 8 buckets.</p>  	<p style="text-align:center;"><img src="/images/workflow_diagram.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>
  	<p>First up: If the next action takes less than two minutes to finish, do it immediately. Otherwise, the item goes into one of the following buckets:</p> 	<ul> 	<li>Project List: If there&rsquo;s more than one action needed</li> 		<li>Projectplan: A list of the actions needed to finish a project</li> 		<li>Delegate: If the next action has to be done by someone else, you let him/her know and put a &ldquo;waiting for&rdquo; tracker in you system</li> 		<li>Defer: If the next action has to be done on a particular day or time, it goes on you calendar</li> 		<li>Next action: Otherwise it goes into you Next Actions List which is organized by context (at home, office, online, offline etc)<br /> For stuff that doesn&rsquo;t directly need a Next Action we have three extra buckets: 		</li><li>File: for information that might be usefull in the future</li> 		<li>Tickler File: for items that you like to do someday or items you might need on a particular day</li> 		<li>Trash: items you don;t need at all</li> 	</ul>  	<p>So now you&rsquo;ve got all your collected stuff <strong>organized</strong> in the appropriate bucket. Next up is <strong>Reviewing</strong> on a timely beasis, best is the <em>Weekly Review</em> to actually go through all the stuff on your lists and check to see what needs to be done. Allen suggests reviewing your calendar each morning, and the other buckets (plus any &ldquo;stuff&rdquo; you haven&rsquo;t got around to processing, to clear your mind again) early each Friday afternoon, moving next actions between buckets as appropriate. Then whenever you have openings in your calendar, you select from the next actions based on (a) context (are you in the right place with the right tools to do this), (b) time available (be able to finish what you start), (c) energy available (right frame of mind) and (d) priority (relative importance). Then just do it.<br /> Last but not least is actually <strong><em>doing</em></strong> your work. Because that&rsquo;s why we started this right? To be able to just <em>do</em> your work and be in the knowledge that all the other stuff that needs to be done is in a trusted system.</p>  	<p>There&rsquo;s more to the system and the book than I just described here. He also talks about the phases of projectplanning, how to setup your system and how to use it effectively. If you are interested, please check out the book at <a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Amazon</a>. Next up I will talk about my setup and what software I use to implement GTD.</p> 
 
<p class="info">01 12 04 - 18:49 - <a href="/pivot/entry.php?id=1578&amp;w=whats_the_next_action" title="01 Dec '04">Permanent link</a> - <span class="comments"><a href="/pivot/entry.php?id=1578&amp;w=whats_the_next_action#comm" title="Alper">two comments</a> <?php echo get_editentrylink("Edit", '1578'); ?></span></p>
</div>
 <p id="footer">
 template created by el73
 </p>
</div>
<div id="secondary">
 <div class="about">
  <h3>About</h3>
  <p>This weblog deals with everything GTD and the five phases of projectplanning as written by Dave Allen in his book "Getting Things Done"<br />
I will try to record and publish my thoughts and experiences with this system to really "Get Things Done" in my personal and professional life.
  </p>
 </div>
 <div class="search">
  <h3>Search</h3>
<script type="text/javascript" src="http://technorati.com/embed/hhcmz65qf.js"></script><br>
 </div>
 <div class="archives">
  <h3>Archives</h3>
<p><a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br /><p>
 </div>
<div class="stuff">
<h3>Popular articles</h3>
Here are today's most popular articles:<br />
<ul>
<li><a href="http://punkey.com/pivot/entry.php?id=6971">Backpack and GTD</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7002">Using Backpack and GTD, continued</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7057">Shortcut Sunday #3: MS Outlook</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=7068">Mindjet's MindManager for free</a><br />
<li><a href="http://punkey.com/pivot/entry.php?id=6967">Simple Outlook hack may save your day in the future</a>
</ul>

</div>
 <div class="stuff">
  <h3>Need some help getting started?</h3>
Inspired by this blog and the principles of GTD but don't know what to do next?<br>
Read my article on <a href="http://punkey.com/pivot/entry.php?id=6971">using Backpack and GTD</a> and <a href="http://backpackit.com/?referrer=BPF9BJ9">try it riskfree</a> for yourself for the next 30 days! Yes, gather your ideas, to-do's, notes, files and photos online. Plus set reminders to be sent trough email or to your cellphone!<br><a href="http://backpackit.com/?referrer=BPF9BJ9">Start your account now</a>
 </div>


 <div class="archives">
  <h3>Archives</h3>
  <p>
   <a href="/gtd/archives/archive_2004-m11.php">01 Nov - 30 Nov 2004 </a><br /><a href="/gtd/archives/archive_2004-m12.php">01 Dec - 31 Dec 2004 </a><br /><a href="/gtd/archives/archive_2005-m01.php">01 Jan - 31 Jan 2005 </a><br /><a href="/gtd/archives/archive_2005-m02.php">01 Feb - 28 Feb 2005 </a><br /><a href="/gtd/archives/archive_2005-m03.php">01 Mar - 31 Mar 2005 </a><br /><a href="/gtd/archives/archive_2005-m04.php">01 Apr - 30 Apr 2005 </a><br /><a href="/gtd/archives/archive_2005-m05.php">01 May - 31 May 2005 </a><br /><a href="/gtd/archives/archive_2005-m06.php">01 Jun - 30 Jun 2005 </a><br /><a href="/gtd/archives/archive_2005-m07.php">01 Jul - 31 Jul 2005 </a><br /><a href="/gtd/archives/archive_2005-m08.php">01 Aug - 31 Aug 2005 </a><br /><a href="/gtd/archives/archive_2005-m09.php">01 Sep - 30 Sep 2005 </a><br /><a href="/gtd/archives/archive_2005-m10.php">01 Oct - 31 Oct 2005 </a><br /><a href="/gtd/archives/archive_2005-m11.php">01 Nov - 30 Nov 2005 </a><br /><a href="/gtd/archives/archive_2005-m12.php">01 Dec - 31 Dec 2005 </a><br /><a href="/gtd/archives/archive_2006-m01.php">01 Jan - 31 Jan 2006 </a><br /><a href="/gtd/archives/archive_2006-m02.php">01 Feb - 28 Feb 2006 </a><br /><a href="/gtd/archives/archive_2006-m03.php">01 Mar - 31 Mar 2006 </a><br /><a href="/gtd/archives/archive_2006-m04.php">01 Apr - 30 Apr 2006 </a><br /><a href="/gtd/archives/archive_2006-m05.php">01 May - 31 May 2006 </a><br /><a href="/gtd/archives/archive_2006-m06.php">01 Jun - 30 Jun 2006 </a><br /><a href="/gtd/archives/archive_2006-m07.php">01 Jul - 31 Jul 2006 </a><br /><a href="/gtd/archives/archive_2006-m08.php">01 Aug - 31 Aug 2006 </a><br /><a href="/gtd/archives/archive_2006-m09.php">01 Sep - 30 Sep 2006 </a><br /><a href="/gtd/archives/archive_2006-m10.php">01 Oct - 31 Oct 2006 </a><br />
  </p>
 </div>
 <div class="comments">
  <h3>Last Comments</h3>
  
<a href='/pivot/entry.php?id=7062&amp;w=whats_the_next_action#neacutestor_rojas_caracas_venezuela-0610281414' title='28 10 2006 - 14:14' ><b>N&eacute;stor Rojas (Cara&hellip;</b></a> (I'm a blackbelt f&hellip;): With this cute girl u have many next&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#jim-0610260802' title='26 10 2006 - 08:02' ><b>Jim</b></a> (Mindmanager, an e&hellip;): Dude, excellent article!
	I too just s&hellip;<br />
<a href='/pivot/entry.php?id=6949&amp;w=whats_the_next_action#foo-0610260209' title='26 10 2006 - 02:09' ><b>foo</b></a> (Temptation Blocke&hellip;): Then just block Outlook as well! ^^<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#ks-0610252209' title='25 10 2006 - 22:09' ><b>KS</b></a> (Mindmanager, an e&hellip;): There is also an open source tool, F&hellip;<br />
<a href='/pivot/entry.php?id=7081&amp;w=whats_the_next_action#nick_duffill-0610251816' title='25 10 2006 - 18:16' ><b>Nick Duffill</b></a> (Mindmanager, an e&hellip;): Frank &#8211; the solid dots on the Main To&hellip;<br />
<a href='/pivot/entry.php?id=7077&amp;w=whats_the_next_action#joao_gazolla-0610230103' title='23 10 2006 - 01:03' ><b>Joao Gazolla</b></a> (Netvibes GTD tab): Hello Guys,
     I&#8217;d like to invite al&hellip;<br />
<a href='/pivot/entry.php?id=7079&amp;w=whats_the_next_action#martijn-0610222020' title='22 10 2006 - 20:20' ><b>Martijn</b></a> (How to use GTD at&hellip;): Usefull but aren&#8217;t you managing to mc&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#blixa_bargeld-0610221746' title='22 10 2006 - 17:46' ><b>Blixa Bargeld</b></a> (Scrybe is the kil&hellip;): The video preview is very impressive&hellip;<br />
<a href='/pivot/entry.php?id=7080&amp;w=whats_the_next_action#paul-0610221738' title='22 10 2006 - 17:38' ><b>Paul</b></a> (Scrybe is the kil&hellip;): just watched the clip,and it looks s&hellip;<br />
<a href='/pivot/entry.php?id=7070&amp;w=whats_the_next_action#martijn-0610212127' title='21 10 2006 - 21:27' ><b>Martijn</b></a> (Smart keywords ar&hellip;): Good suggestion, makes life a lot ea&hellip;<br />
 </div>

</div>


<div id="stuff">
[error: could not include _GTD_Wedstrijd_sidebar.html. File does not exist]
 <div class="stuff">
  <h3>Help</h3>
   <script type="text/javascript">
     var irr_lang = 'en';
   </script>
   <script src="http://fragments.irrepressible.info/js/fragment-125.js" type="text/javascript"></script>
 </div>

 <div class="stuff">
<script language="javascript" src="http://blogads.ebanner.nl/adserve.asp?tag=138"></script>
 </div>

 <div class="links">
  <h3>Links</h3>
  <p>
<a href="http://www.gettingthingsdone.com/"  target='_blank'>The David Allen Company, home of the GTD implementation</a><br />
<a href="http://www.davidco.com/pdfs/gtd_workflow_advanced.pdf"  target='_blank'>Referencecard for the GTD principles [PDF]</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670899240%2Fref%3Dlpr_g_1%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=http%3A%2F%2Fwww.audible.com%2Fadbl%2Fstore%2FamazonProduct.jsp%3FamazonCategory%3Dproduct%26productID%3DBK_SANS_000347%26source_code%3DWSAZS01001102000%26scic%3D4"  target='_blank'>Listen to the book</a><br />
<a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0670032506%2Fqid%3D1101681547%2Fsr%3D1-5%2Fref%3Dsr_1_5%3Fv%3Dglance%26s%3Dbooks"  target='_blank'>Buy "Ready for anything", David's new book!</a><br />
<a href="http://del.icio.us/punkey/gtd"  target='_blank'>My GTD-archive</a> [<a href="http://del.icio.us/rss/punkey/gtd"  target='_blank'>RSS</a>]
</p>

</div>


 <div class="linkdump">
  <h3>Del.icio.us links</h3>
<script type="text/javascript" src="http://del.icio.us/feeds/js/punkey/gtd/"></script>
 </div>
 <div class="stuff">
  <h3>Miscellany</h3>
  <a href="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27"  title="Powered byPivot - 1.40.0 beta: 'Dreadwind'" target='_blank'><img src="/pivot/pics/pivotbutton.png" width="94" height="15" alt="Powered byPivot - 1.40.0 beta: 'Dreadwind'" class="badge" longdesc="http://www.pivotlog.net/?ver=Pivot+-+1.40.0+beta%3A+%27Dreadwind%27" /></a>&nbsp;<br />
<a href="http://feeds.feedburner.com/WhatsTheNextAction"><img src="http://feeds.feedburner.com/~fc/WhatsTheNextAction?bg=99CCFF&amp;fg=444444&amp;anim=0" height="26" width="88" style="border:0" alt="" /></a><br />
 <a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="XML: RSS feed" style="border: 0px none ;"></a>&nbsp;<a href="http://feeds.feedburner.com/WhatsTheNextAction" title="XML: RSS feed" target="_blank">Webfeed</a><br>
 <a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank"><img src="/images/feed-icon-16x16.gif" alt="Del.icio.us webfeed" style="border: 0px none ;" ></a>&nbsp;<a href="http://del.icio.us/rss/punkey/gtd" title="Del.icio.us webfeed" target="_blank">My Del.icio.us GTD feed</a><br>
<a href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http://feeds.feedburner.com/WhatsTheNextAction" title="What's the next action"><img src="http://www.newsgator.com/images/ngsub1.gif" alt="Subscribe in NewsGator Online" style="border:0"/></a><br>

<form method="post" action="http://www.feedblitz.com/feedblitz.exe?BurnUser"><p><label for="email">Enter your email to subscribe:</label><br /><input name="email" maxlength="255" type="text" size="26" id="email" /><br /><input name="uri" type="hidden" value="WhatsTheNextAction" /> <input type="submit" value="Subscribe me!" /></p><p id="poweredByFeedBlitz">Powered by <a href="http://www.feedblitz.com">FeedBlitz</a></p></form>
<p><script type="text/javascript">
<!--
rojo_ad_client = '958'; rojo_ad_width = '120'; rojo_ad_height = '240';
// -->
</script>
<script type="text/javascript"
src="http://www.rojo.com/javascript/ShowFeedExchangeAds.js">
</script>
</p> 
 </div>

</body>
</html>