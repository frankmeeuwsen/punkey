<?php

/* This code is a modified version of Elliott Back's WP-Hashcash version 2.3
   All credit for this incredible wizardry go entirely to Elliott
   
   http://www.elliottback.com/

   This port for Pivot by Marco van Hylckama Vlieg */

/**
 * Takes: An integer l and an array of strings exclude
 * Returns: A random unique string of length l
 */
function hashcash_random_string($l, $exclude = array()) {
	// Sanity check
	if($l < 1){
		return '';
	}

	srand((double) microtime() * 1000000);
	
	$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$chars = preg_split('//', $alphabet, -1, PREG_SPLIT_NO_EMPTY);
	$len = count($chars) - 1;
	
	$str = '';
	while(in_array($str, $exclude) || strlen($str) < 1){
		$str = '';
		while(strlen($str) < $l){
			$str .= $chars[rand(0, $len)];
		}
	}
	return $str;
}

/**
 * Takes: A string md5_function_name to call the md5 function
 * Returns: md5 javascript bits to be randomly spliced into the header
 */
function hashcash_get_md5_javascript($md5_function_name){
	$p = '';
	$s = '';

	$names = array();
	$excl = array('a', 's', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
			'v', 'w', 'x', 'y', 'z', 'num', 'cnt', 'str', 'bin', 
			'length', 'len', 'var', 'Array', 'mask', 'return', 'msw',
			'lsw', 'olda', 'oldb', 'oldc', 'oldd', 'function', 'new');
	for($i = 0; $i < 20; $i++){
		$t = hashcash_random_string(rand(1,6), $excl);
		$names [] = $t;
		$excl [] = $t;
	}

	$bits = array();
	$bits [] = $p . 'function ' . $md5_function_name . '(s){return ' . $names[5] . '(' . $names[6] . '(' . $names[7] . '(s),s.length*8));}' . $s;
	$bits [] = $p . 'function ' . $names[6] . '(x,len){x[len>>5]|=0x80<<((len)%32);x[(((len+64)>>>9)<<4)+14]=len;var a=1732584193;var b=-271733879;var c=-1732584194;var d=271733878;for(var i=0;i<x.length;i+=16){var olda=a;var oldb=b;var oldc=c;var oldd=d;a=' . $names[8] . '(a,b,c,d,x[i+0],7,-680876936);d=' . $names[8] . '(d,a,b,c,x[i+1],12,-389564586);c=' . $names[8] . '(c,d,a,b,x[i+2],17,606105819);b=' . $names[8] . '(b,c,d,a,x[i+3],22,-1044525330);a=' . $names[8] . '(a,b,c,d,x[i+4],7,-176418897);d=' . $names[8] . '(d,a,b,c,x[i+5],12,1200080426);c=' . $names[8] . '(c,d,a,b,x[i+6],17,-1473231341);b=' . $names[8] . '(b,c,d,a,x[i+7],22,-45705983);a=' . $names[8] . '(a,b,c,d,x[i+8],7,1770035416);d=' . $names[8] . '(d,a,b,c,x[i+9],12,-1958414417);c=' . $names[8] . '(c,d,a,b,x[i+10],17,-42063);b=' . $names[8] . '(b,c,d,a,x[i+11],22,-1990404162);a=' . $names[8] . '(a,b,c,d,x[i+12],7,1804603682);d=' . $names[8] . '(d,a,b,c,x[i+13],12,-40341101);c=' . $names[8] . '(c,d,a,b,x[i+14],17,-1502002290);b=' . $names[8] . '(b,c,d,a,x[i+15],22,1236535329);a=' . $names[9] . '(a,b,c,d,x[i+1],5,-165796510);d=' . $names[9] . '(d,a,b,c,x[i+6],9,-1069501632);c=' . $names[9] . '(c,d,a,b,x[i+11],14,643717713);b=' . $names[9] . '(b,c,d,a,x[i+0],20,-373897302);a=' . $names[9] . '(a,b,c,d,x[i+5],5,-701558691);d=' . $names[9] . '(d,a,b,c,x[i+10],9,38016083);c=' . $names[9] . '(c,d,a,b,x[i+15],14,-660478335);b=' . $names[9] . '(b,c,d,a,x[i+4],20,-405537848);a=' . $names[9] . '(a,b,c,d,x[i+9],5,568446438);d=' . $names[9] . '(d,a,b,c,x[i+14],9,-1019803690);c=' . $names[9] . '(c,d,a,b,x[i+3],14,-187363961);b=' . $names[9] . '(b,c,d,a,x[i+8],20,1163531501);a=' . $names[9] . '(a,b,c,d,x[i+13],5,-1444681467);d=' . $names[9] . '(d,a,b,c,x[i+2],9,-51403784);c=' . $names[9] . '(c,d,a,b,x[i+7],14,1735328473);b=' . $names[9] . '(b,c,d,a,x[i+12],20,-1926607734);a=' . $names[10] . '(a,b,c,d,x[i+5],4,-378558);d=' . $names[10] . '(d,a,b,c,x[i+8],11,-2022574463);c=' . $names[10] . '(c,d,a,b,x[i+11],16,1839030562);b=' . $names[10] . '(b,c,d,a,x[i+14],23,-35309556);a=' . $names[10] . '(a,b,c,d,x[i+1],4,-1530992060);d=' . $names[10] . '(d,a,b,c,x[i+4],11,1272893353);c=' . $names[10] . '(c,d,a,b,x[i+7],16,-155497632);b=' . $names[10] . '(b,c,d,a,x[i+10],23,-1094730640);a=' . $names[10] . '(a,b,c,d,x[i+13],4,681279174);d=' . $names[10] . '(d,a,b,c,x[i+0],11,-358537222);c=' . $names[10] . '(c,d,a,b,x[i+3],16,-722521979);b=' . $names[10] . '(b,c,d,a,x[i+6],23,76029189);a=' . $names[10] . '(a,b,c,d,x[i+9],4,-640364487);d=' . $names[10] . '(d,a,b,c,x[i+12],11,-421815835);c=' . $names[10] . '(c,d,a,b,x[i+15],16,530742520);b=' . $names[10] . '(b,c,d,a,x[i+2],23,-995338651);a=' . $names[11] . '(a,b,c,d,x[i+0],6,-198630844);d=' . $names[11] . '(d,a,b,c,x[i+7],10,1126891415);c=' . $names[11] . '(c,d,a,b,x[i+14],15,-1416354905);b=' . $names[11] . '(b,c,d,a,x[i+5],21,-57434055);a=' . $names[11] . '(a,b,c,d,x[i+12],6,1700485571);d=' . $names[11] . '(d,a,b,c,x[i+3],10,-1894986606);c=' . $names[11] . '(c,d,a,b,x[i+10],15,-1051523);b=' . $names[11] . '(b,c,d,a,x[i+1],21,-2054922799);a=' . $names[11] . '(a,b,c,d,x[i+8],6,1873313359);d=' . $names[11] . '(d,a,b,c,x[i+15],10,-30611744);c=' . $names[11] . '(c,d,a,b,x[i+6],15,-1560198380);b=' . $names[11] . '(b,c,d,a,x[i+13],21,1309151649);a=' . $names[11] . '(a,b,c,d,x[i+4],6,-145523070);d=' . $names[11] . '(d,a,b,c,x[i+11],10,-1120210379);c=' . $names[11] . '(c,d,a,b,x[i+2],15,718787259);b=' . $names[11] . '(b,c,d,a,x[i+9],21,-343485551);a=' . $names[13] . '(a,olda);b=' . $names[13] . '(b,oldb);c=' . $names[13] . '(c,oldc);d=' . $names[13] . '(d,oldd);}return Array(a,b,c,d);}' . $s;
	$bits [] = $p . 'function ' . $names[12] . '(q,a,b,x,s,t){return ' . $names[13] . '(' . $names[16] . '(' . $names[13] . '(' . $names[13] . '(a,q),' . $names[13] . '(x,t)),s),b);}function ' . $names[8] . '(a,b,c,d,x,s,t){return ' . $names[12] . '((b&c)|((~b)&d),a,b,x,s,t);}' . $s;
	$bits [] = $p . 'function ' . $names[9] . '(a,b,c,d,x,s,t){return ' . $names[12] . '((b&d)|(c&(~d)),a,b,x,s,t);}' . $s;
	$bits [] = $p . 'function ' . $names[10] . '(a,b,c,d,x,s,t){return ' . $names[12] . '(b ^ c ^ d,a,b,x,s,t);}' . $s;
	$bits [] = $p . 'function ' . $names[11] . '(a,b,c,d,x,s,t){return ' . $names[12] . '(c ^(b|(~d)),a,b,x,s,t);}' . $s;
	$bits [] = $p . 'function ' . $names[13] . '(x,y){var lsw=(x&0xFFFF)+(y&0xFFFF);var msw=(x>>16)+(y>>16)+(lsw>>16);return(msw<<16)|(lsw&0xFFFF);}' . $s;
	$bits [] = $p . 'function ' . $names[16] . '(num,cnt){return(num<<cnt)|(num>>>(32-cnt));}' . $s;
	$bits [] = $p . 'function ' . $names[7] . '(' . $names[18] . '){var ' . $names[17] . '=Array();var ' . $names[19] . '=(1<<8)-1;for(var i=0;i<' . $names[18] . '.length*8;i+=8)' . $names[17] . '[i>>5]|=(' . $names[18] . '.charCodeAt(i/8)&' . $names[19] . ')<<(i%32);return ' . $names[17] . ';}' . $s;
	$bits [] = $p . 'function ' . $names[5] . '(' . $names[15] . '){var ' . $names[14] . '="0123456789abcdef";var str="";for(var i=0;i<' . $names[15] . '.length*4;i++){str+=' . $names[14] . '.charAt((' . $names[15] . '[i>>2]>>((i%4)*8+4))&0xF)+' . $names[14] . '.charAt((' . $names[15] . '[i>>2]>>((i%4)*8))&0xF);}return str;}' . $s;

	return $bits;
}
/**
 * Takes: <<void>>
 * Returns: the hashcash special code, based on the session or ip
 */


function hashcash_special_code(){
	global $pivot_path;
		$key = strip_tags(session_id());
	
		if(!$key){
			$key = $_SERVER['REMOTE_ADDR'];
		}
	
		return md5($key . $pivot_path . $_SERVER['HTTP_USER_AGENT'] . date("F j, Y, g a"));
	}
/**
 * Takes: <<void>>
 * Returns: the hashcash special field value
 * Modified by Marco to use Pivot db value
 */

function hashcash_field_value() {
        global $db;
        global $pivot_path;
        return $db->entry['code'] * strlen($pivot_path);
	}

/**
 * Takes: String name of function
 * Returns:  Javascript to compute field value
 */


function hashcash_field_value_js($val_name){
	$js = 'function ' . $val_name . '(){';
	
	$type = rand(0, 5);
	switch($type){
		/* Addition of n times of field value / n, + modulus */
		case 0:
			$eax = hashcash_random_string(rand(8,10));
			$val = hashcash_field_value();
			$inc = rand(1, $val - 1);
			$n = floor($val / $inc);
			$r = $val % $inc;
			
			$js .= "var $eax = $inc; ";
			for($i = 0; $i < $n - 1; $i++){
				$js .= "$eax += $inc; ";
			}
			
			$js .= "$eax += $r; ";
			$js .= "return $eax; ";
		
			break;
		
		/* Conversion from binary */
		case 1:
			$eax = hashcash_random_string(rand(8,10));
			$ebx = hashcash_random_string(rand(8,10));
			$ecx = hashcash_random_string(rand(8,10));
			$val = hashcash_field_value();
			$binval = strrev(base_convert($val, 10, 2));

			$js .= "var $eax = \"$binval\"; ";
			$js .= "var $ebx = 0; ";
			$js .= "var $ecx = 0; ";
			$js .= "while($ecx < $eax.length){ ";
			$js .= "if($eax.charAt($ecx) == \"1\") { ";
			$js .= "$ebx += Math.pow(2, $ecx); ";
			$js .= "} ";
			$js .= "$ecx++; ";
			$js .= "} ";
			$js .= "return $ebx; ";
			
			break;

		/* Multiplication of square roots */
		case 2:
			$val = hashcash_field_value();
			$sqrt = floor(sqrt($val));
			$r = $val - ($sqrt * $sqrt);
			$js .= "return $sqrt * $sqrt + $r; ";
			break;

		/* Closest sum up to n */
		case 3:
			$val = hashcash_field_value();
			$n = floor((sqrt(8*$val+1)-1)/2);
			$sum = $n * ($n + 1) / 2;
			$r = $val - $sum;
			$eax = hashcash_random_string(rand(8,10));

			$js .= "var $eax = $r; ";
			for($i = 0; $i <= $n; $i++){
				$js .= "$eax += $i; ";
			}
			$js .= "return $eax; ";
			break;

		/* Closest sum up to n #2 */
		case 4:
			$val = hashcash_field_value();
			$n = floor((sqrt(8*$val+1)-1)/2);
			$sum = $n * ($n + 1) / 2;
			$r = $val - $sum;

			$js .= "return $r ";
			for($i = 0; $i <= $n; $i++){
				$js .= "+ $i ";
			}
			$js .= ";";
			break;

		/* Closest sum up to n #3 */
		case 5:
			$val = hashcash_field_value();
			$n = floor((sqrt(8*$val+1)-1)/2);
			$sum = $n * ($n + 1) / 2;
			$r = $val - $sum;
			$eax = hashcash_random_string(rand(8,10));

			$js .= "var $eax = $r; var i; ";
			$js .= "for(i = 0; i <= $n; i++){ ";
			$js .= "$eax += i; ";
			$js .= "} ";
			$js .= "return $eax; ";
			break;
	}
	
	$js .= "} ";
	return $js;
}

function hashcash_calculate_values()	{

		global $Paths;

		$pivot_path = $Paths['pivot_path'];
		$field_id = hashcash_random_string(rand(6,18));
		$field_name = hashcash_random_string(rand(6,18));
		$hashcash_form_action = hashcash_random_string(rand(6,18));
		$md5_name = hashcash_random_string(rand(6,18));
		$val_name = hashcash_random_string(rand(6,18));
		$eElement = hashcash_random_string(rand(6,18));
		$in_str = hashcash_random_string(rand(6,18));
		$fn_enable_name = hashcash_random_string(rand(6,18));

		// Make stuff global (Marco)

		$GLOBALS["field_id"] = $field_id;
		$GLOBALS["field_name"] = $field_name;
		$GLOBALS["hashcash_form_action"] = $hashcash_form_action;
		$GLOBALS["md5_name"] = $md5_name;
		$GLOBALS["val_name"] = $val_name;
		$GLOBALS["eElement"] = $eElement;
		$GLOBALS["in_str"] = $in_str;
		$GLOBALS["fn_enable_name"] = $fn_enable_name;
	}

function hashcash_add_hidden_tag() {

		$field_id = $GLOBALS["field_id"];
		$field_name = $GLOBALS["field_name"];
		$hashcash_form_action = $GLOBALS["hashcash_form_action"];
		$md5_name = $GLOBALS["md5_name"];
		$val_name = $GLOBALS["val_name"];
		$eElement = $GLOBALS["eElement"];
		$in_str = $GLOBALS["in_str"];
		$fn_enable_name = $GLOBALS["fn_enable_name"];

	
		// Write in hidden field
		// For Pivot don't write but generate and return it.

        	global $db;
        	global $pivot_path;
        	$nID = $db->entry['code'];

		$sOut = "<input type=\"hidden\" id=\"".$field_id."\" name=\"field_name\" value=\"".rand(100, 99999999)."\"/><input type=\"hidden\" name=\"comment_post_ID\" value=\"$nID\" />";

		return $sOut;
		}

function hashcash_add_form_action()	{
	
		// The form action

		$sOut = "onsubmit=\"".$GLOBALS["hashcash_form_action"]."('".hashcash_special_code()."');\" ";
		return $sOut;
		}

function hashcash_add_javascript()	{

		// calculate stuff

		hashcash_calculate_values();

		$field_id = $GLOBALS["field_id"];
		$field_name = $GLOBALS["field_name"];
		$hashcash_form_action = $GLOBALS["hashcash_form_action"];
		$md5_name = $GLOBALS["md5_name"];
		$val_name = $GLOBALS["val_name"];
		$eElement = $GLOBALS["eElement"];
		$in_str = $GLOBALS["in_str"];
		$fn_enable_name = $GLOBALS["fn_enable_name"];

		// The javascript
		$hashcash_bits = hashcash_get_md5_javascript($md5_name);
		$hashcash_bits [] = "function $hashcash_form_action($in_str){ "
			. "$eElement = document.getElementById(\"$field_id\"); "
			. "if(!$eElement){ return false; } else { $eElement" . ".name = $md5_name($in_str); $eElement" . ".value = $val_name(); return true; }}";
		$hashcash_bits [] = hashcash_field_value_js($val_name);

		shuffle($hashcash_bits);
		$js = '<script type="text/javascript">' . "\n"
			. '<!--' . "\n"
			. '// HASHCASH JAVASCRIPT'."\n"
			. implode(" ", $hashcash_bits) . "\n"
			. '-->' . "\n"
			. '</script>' . "\n";
	return $js;
}

 
/**
 * Takes: The text of a comment
 * Returns: The comment iff it matches the hidden md5'ed tag
 */
function hashcash_check_hidden_tag() {
	global $pivot_path;
	// Our special codes, fixed to check the previous hour
	$special = array();

		$special[] = md5($_SERVER['REMOTE_ADDR'] . $pivot_path . $_SERVER['HTTP_USER_AGENT'] . date("F j, Y, g a"));
		$special[] = md5($_SERVER['REMOTE_ADDR'] . $pivot_path . $_SERVER['HTTP_USER_AGENT'] . date("F j, Y, g a", time()-(60*60)));
		$special[] = md5(strip_tags(session_id()) . $pivot_path . $_SERVER['HTTP_USER_AGENT'] . date("F j, Y, g a"));
		$special[] = md5(strip_tags(session_id()) . $pivot_path . $_SERVER['HTTP_USER_AGENT'] . date("F j, Y, g a", time()-(60*60)));

	foreach($special as $val){
		if($_POST[md5($val)] == ($_POST['comment_post_ID'] * strlen($pivot_path))){
		return true;
		}
	}
	pbl_logspammer($_SERVER["REMOTE_ADDR"], "hashcash");
	return false;
}
?>
