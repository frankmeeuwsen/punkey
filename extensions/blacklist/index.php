<?
@ini_set("include_path", ".");
require_once("blacklist_lib.php");

if($_GET["page"] != "login")  {
	pbl_checklogin();
}

switch($_GET["page"])  {
case "logout";
	setcookie("user", "");
	setcookie("pass", "");
	header("location:index.php?page=login");
	break;
case "login":
		include("pbl_head.php");
	echo "<div id=\"left\">&nbsp;</div>";
	echo "<div id=\"main\">";
	echo "<form action=\"index.php\" method=\"post\">";
	echo "<table style=\"margin-left:auto;margin-right:auto;margin-top:100px;\">\n";
	echo "<tr><td colspan=\"2\" style=\"text-align:left; border:none\"><b>Login for the Pivot-Blacklist ".__VERSION."</b></td></tr>\n";
	echo "<tr><td>username</td><td><input type=\"text\" name=\"user\"/></td></tr><tr>";
	echo "<td>password</td><td><input type=\"password\" name=\"pass\"/></td></tr><tr><td style=\"border:none;\"><small>Pivot users: use your <br/>Pivot administration<br/>credentials to log in.</small></td><td style=\"border:none;\">";
	echo "<input type=\"submit\" value=\"login\" /></td></tr>\n";
	echo "<tr><td colspan=\"2\" style=\"text-align:left; border:none\"><a href=\"http://www.pivotblacklist.net/\"><img style=\"border:none; margin:0px; margin-top:10px;margin-bottom:5px;\" src=\"images/pblbutton.png\" alt=\"Pivot Blacklist!\"/></a><br />";
	echo "<a href=\"http://spammerbegone.com/\" target=\"_blank\"><img src=\"http://spammerbegone.com/images/sbg_94x15.png\" title=\"SPAMMERS SUCK! We redirect ours to SpammerBeGone.com!\" width=\"94\" height=\"15\" border=\"0\" /></a>";
	echo "</td></tr>\n";
	echo "</table>\n";
	echo "</form>";
	echo "</div>";
	include("pbl_foot.php");
	break;
case "blacklist":
	include("pbl_head.php");
	pbl_menu();
	echo "<div id=\"main\">";
	pbl_blacklisteditor();
	include("pbl_foot.php");
	break;
case "referers":
	include("pbl_head.php");
	pbl_menu();
	pbl_referereditor();
	include("pbl_foot.php");
	break;
case "getblacklist":
	include("pbl_head.php");
	pbl_menu();
	echo "<div id=\"main\">";
	$sUpdated = pbl_updateblacklist(true);
	if($sUpdated != "fail")  {
		if($sUpdated == "yes")  {
			$pblmessage = "Blacklist succesfully updated!";
		} else  {
			$pblmessage = "Blacklist is up to date. No new updates available.";
		}
	} else  {
		$pblmessage = "Failed to get the blacklist. Existing list was left untouched.";
	}
	echo "<div class=\"pblmessage\">$pblmessage</div>\n";
	pbl_blacklisteditor();
	include("pbl_foot.php");
	break;
case "addpersonal":
	include("pbl_head.php");
	pbl_menu();
	echo "<div id=\"main\">";
	pbl_addpersonal();
	pbl_blacklisteditor();
	include("pbl_foot.php");
	break;
case "addwhitelist":
	include("pbl_head.php");
	pbl_menu();
	$pblmessage = pbl_addwhitelist();
	pbl_referereditor();
	include("pbl_foot.php");
	break;
case "deletewhitelist":
	include("pbl_head.php");
	pbl_menu();
	pbl_deletewhitelist();
	echo "<div class=\"pblmessage\">Entry deleted from your whitelist.</div>\n";
	pbl_referereditor();
	include("pbl_foot.php");
	break;
case "deleteexpression":
	include("pbl_head.php");
	pbl_menu();
	echo "<div id=\"main\">";
	pbl_deleteexpression();
	echo "<div class=\"pblmessage\">Expression deleted from personal blacklist.</div>\n";
	pbl_blacklisteditor();
	include("pbl_foot.php");
	break;
case "settings":
	include("pbl_head.php");
	pbl_menu();
	pbl_editsettings();
	include("pbl_foot.php");
	break;
case "savesettings":
	include("pbl_head.php");
	pbl_menu();
	pbl_savesettings();
	pbl_editsettings();
	include("pbl_foot.php");
	break;
case "ipblocks":
	include("pbl_head.php");
	pbl_menu();
	pbl_showipblock();
	include("pbl_foot.php");
	break;
case "deleteipblock":
	include("pbl_head.php");
	pbl_menu();
	pbl_deleteipblock();
	pbl_showipblock();
	include("pbl_foot.php");
	break;

default:
	include("pbl_head.php");
	pbl_menu();
	echo "<div id=\"main\">";
	if(isset($_GET["message"]))  {
		echo "<div class=\"pblmessage\">".$_GET["message"]."</div>";
	}
	echo "<div class=\"pbldescription\">";
	echo "<h5>Pivot-Blacklist version ".__VERSION."</h5>";
	echo "Welcome to the Pivot Blacklist administration interface. Use the blacklist editor to manage your blacklists or the settings editor to change settings for the spam-blocking engine.<br/><br/>Pivot-Blacklist was created by <a href=\"mailto:marco@i-marco.nl\">Marco van Hylckama Vlieg</a> and is licensed under the GNU Public License.";
	echo "<br /><br /><b>Documentation</b><br/><br /><a href=\"README.txt\">README.txt</a><br /><a href=\"WHAT_TO_ENABLE.txt\">Some hints on which settings to use</a><br /><a href=\"README.TRACKBACK.txt\">Trackback protection</a><br /><a href=\"README.HASHCASH.txt\">Hashcash</a><br /><br /><b>Shameless plugs</b><br /><br /><a href=\"http://www.pivotblacklist.net/\">Pivot Blacklist homepage</a><br/><a href=\"http://www.i-marco.nl/weblog/\">Marco's Blog</a><br/><a href=\"http://spammerbegone.com/\">Spammerbegone.com</a><br /><br/>";

	echo "<b>Some credits</b>\n";
	echo "<br/><ul id=\"credits\">";
	echo "<li>Original MT-Blacklist by Jay Allen (<a href=\"http://www.jayallen.org/\">www.jayallen.org</a>)</li>";
	echo "<li>Smart IP Blocking and .htaccess generator by <a href=\"www.xiffy.nl\">Xiffy</a>, creator of <a href=\"http://wakka.xiffy.nl/blacklist\">Nucleus Blacklist</a></li>";
	echo "<li>WP Hashcash routines by <a href=\"http://www.elliottback.com/\">Elliott Back</a></li>";
	echo "<li>Realtime spamcheck code by John Sinteur (<a href=\"http://weblog.sinteur.com/\">weblog.sinteur.com</a>)</li>";
	echo "<li>httpclient class by Simon Willison ( <a href=\"http://simon.incutio.com/\">http://simon.incutio.com/</a> )</li>";
	echo "<li>Icons from <a href=\"http://www.gnome.org/\">The Gnome Project</a></li>\n";
	echo "<li>A big thank you to everyone from the <a href=\"http://forum.pivotlog.net/\">Pivot Community</a></li>\n";
	echo "<li>And last but not least hugs and kisses to my girlfriend Renie for being able to live with all my tinkering and tweaking.</li>\n";
	echo "</ul>";
	echo "</div>";
	echo "</div>";
	include("pbl_foot.php");
	break;
case "resetlog":
	pbl_resetlog();
	header("location:index.php?page=log&message=Your logfile has been reset");
	break;
case "htaccess":
	include("pbl_head.php");
	pbl_menu();
	pbl_htaccessrules();
	include("pbl_foot.php");
	break;
case "resethtaccess":
	include("pbl_head.php");
	pbl_menu();
	pbl_resetfile("rules");
	$pblmessage = "Your matched rules file has been reset!";
	pbl_htaccessrules();
	include("pbl_foot.php");
	break;
case "resetipblocks":
	include("pbl_head.php");
	pbl_menu();
	pbl_resetfile("ip");
	$pblmessage = "Your ip blocks file has been reset!";
	pbl_htaccessrules();
	include("pbl_foot.php");
	break;
case "log":
	include("pbl_head.php");
	pbl_menu();
	echo "<div id=\"main\">";
	if(isset($_GET["message"]))  {
		echo "<div class=\"pblmessage\">".$_GET["message"]."</div>";
	}
	echo "<div class=\"pbldescription\">";
	echo "<h5>Blacklist log viewer</h5>";
	echo "This is your Pivot-Blacklist logviewer. Each blocked spam attempt will end up in this overview.If you wish you can reset the log.<br/><br/><b><b>Note: These are the last 100 entries. If you want to see the full log, click <a href=\"index.php?page=log\">here</a></div>\n";
	pbl_logtable();
	include("pbl_foot.php");
	break;
}
?>
