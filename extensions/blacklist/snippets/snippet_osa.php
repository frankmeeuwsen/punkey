<?
/* This snippet comes with Pivot-Blacklist and is rather useless without
   it. Snippet usage:

   In your _sub_comment_form.html add the following tag:

   [[osa]]

   Snippet version 1.1 by Marco van Hylckama Vlieg

   http://www.i-marco.nl/pivot-blacklist/
*/

include_once("../extensions/blacklist/blacklist_lib.php");
function snippet_osa()	{
	$aConfig = pbl_getconfig();
	if($aConfig["osaenabled"] != 1)	{
		return "<!-- OSA DISABLED IN BLACKLIST CONTROL PANEL -->";
		}
	$sSecretWord = $aConfig["osaword"];
	$ip = $_SERVER["REMOTE_ADDR"];
	$sMashString = $ip.$sSecretWord;
	$sCheckSum = MD5(trim($sMashString));
	$nowStamp = time();
	$sHTML .= "<input type=\"hidden\" name=\"osa\" value=\"$sCheckSum\"/>\n";
	$sHTML .="<input type=\"hidden\" name=\"osatime\" value=\"$nowStamp\"/>\n";
	return $sHTML;
	}
?>
