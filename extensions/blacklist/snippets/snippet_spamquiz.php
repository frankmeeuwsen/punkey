<?
/* This snippet comes with Pivot-Blacklist and is rather useless without
it. Snippet usage:

In your _sub_comment_form.html add the following tag:

[[spamquiz]]

You can provide optional formatting for the explanation text and the
question the visitor has to answer:

This example has the defaults filled in:

[[spamquiz:<small>%intro%</small>:<br /><br /><b>%question%</b><br /><br /><input type="text" class="commentinput" %value% name="pblanswer"/><br />]]

As you can see:
%intro% is the introduction/explanatory text and
%question% is the question text, both entered in the Blacklist
control panel.
%value% is a field you HAVE to enter inside the pblanswer textfield, unaltered, exactly as in the example.
This is necessary for the cookie functionality to work. Failing to do so won't do any harm except
for the cookie not functioning.

Snippet version 1.1 by Marco van Hylckama Vlieg

http://www.pivotblacklist.net/
*/

include_once("../extensions/blacklist/blacklist_lib.php");
function snippet_spamquiz($p_sIformat="", $p_sQformat="")	{
	$aConfig = pbl_getconfig();
	if($aConfig["sqenabled"] != 1)	{
		return "<!-- SPAMQUIZ DISABLED IN BLACKLIST CONTROL PANEL -->";
	}
		$aConfig = pbl_getconfig();
		$sTheAnswer = $_COOKIE["pblanswer"];
		if(trim($aConfig["sqanswer"]) != $_COOKIE["pblanswer"]) {
			$sTheAnswer = "";
		}	
	if($p_sIformat == "")	{
		$p_sIformat = "<small>%intro%</small>";
	}
	if($p_sQformat == "")	{
		$p_sQformat = "<br /><b>%question%</b><br /><input type=\"text\" class=\"commentinput\" name=\"pblanswer\" value=\"".$sTheAnswer."\"/><br />";
	}
	
	$aConfig = pbl_getconfig();
	$sQuestion = $aConfig["sqquestion"];
	$sIntro = $aConfig["sqexplain"];
	$sIntroFormat = stripslashes(str_replace("%intro%", $sIntro, $p_sIformat));
	$sQuestionFormat = stripslashes(str_replace("%question%", $sQuestion, $p_sQformat));
	$sQuestionFormat = str_replace("%value%", "value=\"$sTheAnswer\"", $sQuestionFormat);
	$sHTML = $sIntroFormat.$sQuestionFormat;
	return $sHTML;
}
?>
