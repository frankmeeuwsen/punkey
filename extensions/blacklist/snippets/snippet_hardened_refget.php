<?php

// Hardened referrers version 0.3
// By Marco van Hylckama Vlieg
// marco@i-marco.nl

// Snippet usage: add [[hardened_refget]] somewhere within the body tags
// of your Pivot templates and get rid of the [[get_referrer]] tag that
// most Pivot templates currently have.

function snippet_hardened_refget() {
	global $Paths;
	$sOut .= "<script type=\"text/javascript\" src=\"".$Paths["pivot_url"]."../extensions/snippets/getkey.php\"></script>";
	$sOut .= "<script type=\"text/javascript\">\n";
	$sOut .= "function getReferer()  {\n";
	$sOut .= "  document.write('<img src=\"".$Paths["pivot_url"]."../extensions/snippets/refgetter.php?key=' + refkey + '&referer=' + document.referrer + '\" width=\"1\" height=\"1\" alt=\"\" />');\n";
	$sOut .= "  }\n";
	$sOut .= "getReferer();\n";
	$sOut .= "</script>\n";
	return $sOut;
}
?>
