<?php

// Hardened referrers version 0.3
// By Marco van Hylckama Vlieg
// marco@i-marco.nl
//
// This file based on getref.inc.php by Bob den Otter

// bail out if someone calls this directly, with a spam script for
// example, uh huh...

if(strstr($_SERVER["REQUEST_URI"], "hgetref") != false)  {
	echo "Nice try, sucker...";
	exit;
}

if(strstr($_GET["referer"], __MYDOMAIN__) == false) {
	include_once($pivot_path."/pvlib.php");
	include_once($pivot_path."/pv_core.php");

	function path() {
		global $HTTP_SERVER_VARS;

		// Xitami does not support ['path_info'], so we set it for those users.
		if (!(isset($HTTP_SERVER_VARS['PATH_INFO'])) || (strlen($HTTP_SERVER_VARS['PATH_INFO'])<2) ) {
			if ( (isset($HTTP_SERVER_VARS['SCRIPT_NAME'])) && (strlen($HTTP_SERVER_VARS['SCRIPT_NAME'])>2) ) {
				$HTTP_SERVER_VARS['PATH_INFO']=$HTTP_SERVER_VARS['SCRIPT_NAME'];
			}
		}
		$current_path = (isset($HTTP_SERVER_VARS['PATH_INFO'])) ? $HTTP_SERVER_VARS['PATH_INFO'] : $HTTP_SERVER_VARS['PHP_SELF'];

		// for now. log_url assumes '../'. Mark disapproves ;)
		$log_url= dirname(dirname($current_path))."/";
		$log_url=str_replace("\\", "", $log_url);
		if (strpos($log_url, "php.exe") > 0) {
			$log_url= substr($log_url, strpos($log_url, "php.exe")+7);
		}

		if ($log_url=="//")  {
			$log_url="/";
		}

		return "tp://".$HTTP_SERVER_VARS['HTTP_HOST'].$log_url;


	}

	function too_old($date,$numdays) {

		$limit = date("Y-m-d-H-i-s", mktime (0,0,0,date("m")  ,date("d")-$numdays ,date("Y")));

		if ($limit < $date) {
			return FALSE;
		} else {
			return TRUE;
		}

	}


	// set the current path:
	$path = path();

	//
	// In safe mode, this won't work.. We need to change the owner of this file.
	//

	if ((isset($_GET['referer'])) &&
	        (strlen($_GET['referer']) > 5))  {

		$ref_file = $pivot_path."/db/ser_last_referrers.php";
		$rows_file = 250;
		$rows_site = 20;
		$rows_days = 5;

		$write = TRUE;

		$lastreferrers =	load_serialize($ref_file, TRUE);

		$REMOTE_HOST = @gethostbyaddr($_SERVER['REMOTE_ADDR']);

		$lastreferrers[] = array(
		                       'date' => format_date("", "%year%-%month%-%day%-%hour24%-%minute%"),
		                       'referer' => $_GET['referer'] ,
		                       'agent' => $_SERVER['HTTP_USER_AGENT'] ,
		                       'remote_host' => $REMOTE_HOST ,
		                       'remote_addr' => $_SERVER['REMOTE_ADDR'],
		                       'request_uri' => $_SERVER['REQUEST_URI']
		                   );

		if ( (count($lastreferrers)>$rows_file) ) {
			$lastreferrers = array_slice($lastreferrers, -$rows_file, $rows_file);
		}

		if ( (too_old($lastreferrers[0]['date'], $rows_days))) {
			array_shift ($lastreferrers);
			array_shift ($lastreferrers);
		}

		save_serialize($ref_file, $lastreferrers);

	}

}

?>
