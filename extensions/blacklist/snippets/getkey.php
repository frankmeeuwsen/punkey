<?php

// Hardened referrers version 0.3
// By Marco van Hylckama Vlieg
// marco@i-marco.nl

include_once("hr_conf.php");
$sKeyName = MD5(__SECRET__.time());
if(strstr($_SERVER["HTTP_REFERER"], __MYDOMAIN__) != false)  {
	touch("refkeys/$sKeyName");
}
echo "var refkey='$sKeyName';";

// delete keys older than 10 seconds

$nNow = time();
$handle=opendir('refkeys/');
while (false!==($file = readdir($handle))) {
	if ($file != "." && $file != "..") {
		$Diff = ($nNow - filectime("refkeys/$file"));
		if ($Diff > 10)
			unlink("refkeys/$file");

	}
}
closedir($handle);
?>
