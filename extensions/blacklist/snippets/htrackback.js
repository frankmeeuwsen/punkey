function loadFragmentInToElement(fragment_url, element_id) {
var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
try {
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch (e) {
try {
xmlhttp = new ActiveXObject("Msxml2.DOMDocument.4.0");
} catch (E) {
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.4.0");
}
}
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
xmlhttp = new XMLHttpRequest();
}
var element = document.getElementById(element_id);
element.innerHTML = '<em>...Creating a trackback key for you</em>';
xmlhttp.open("GET", fragment_url);
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
element.innerHTML = xmlhttp.responseText;
}
}
xmlhttp.send(null);
}
