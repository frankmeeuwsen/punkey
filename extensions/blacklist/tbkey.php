<?php
header("Pragma: no-cache");
header("Expires: 0");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
include_once("../../pivot/pv_core.php");

$__TBURL = "http://".$_SERVER["SERVER_NAME"].$Paths["pivot_url"]."tb.php";

if(!strstr($_SERVER["HTTP_REFERRER"], $_SERVER["SERVER_NAME"]))  {
  // Send a bogus key
  echo $__TBURL."?tb_id=".$_GET["id"]."&amp;key=".md5(microtime());
  exit;
  }

$sTBKEY = md5($_SERVER["REMOTE_ADDR"].time());

touch ("settings/tbkey_".$sTBKEY);
echo $__TBURL."?tb_id=".$_GET["id"]."&amp;key=".$sTBKEY;

$nNow = time();
$handle=opendir('settings/');
while (false!==($file = readdir($handle))) {
	if ($file != "." && $file != "..") {
		if(substr($file, 0, 5) == "tbkey")  {
			$Diff = ($nNow - filectime("settings/$file"));
			if ($Diff > (60 * 15))  {
				unlink("settings/$file");
			}
		}
	}
}
closedir($handle);
?>
