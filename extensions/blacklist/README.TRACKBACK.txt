There are some steps involved if you want to enable Pivot-Blacklist's
ultra secure trackback spam protection scheme. Read carefully and
you'll be fine.

1) Place the following files in your own snippets dir:

    snippets/snippet_htrackback.php snippets/htrackback.js

2) Add [[htrackback]] somewhere in your template.
   This will add a rectangular area to your site with a link that
   can generate a trackback key. Therefore you should take care
   WHERE you put the tag because it may affect your layout.  For a
   working example see my weblog: http://www.i-marco.nl/weblog/ In
   each posting you will see the trackback area.

3) Add the following line to pivot/tb.php:

   require_once('../extensions/blacklist/trackback.php');

   Or if you're lazy, use the included extras/tb.php and overwrite
   Pivot's one with it.

4) Remove all old-style trackback URL tags from your templates!!!
   The classic trackback link will NOT work anymore so it's important
   to NOT display it on your site in order to avoid confusion.  In
   step 3 of the configuration panel you can specify the format of
   the trackback link. Enter something trivial here, for example
   <br /> to get rid of the old style trackback link.

5) copy / paste the contents of snippets/htrackback.css into your
   own css stylesheet and change whatever you want to control the
   look and feel.

It should now function. Just test it by trying the following:

http://www.yoursite.com/path_to_blog/pivot/tb.php?tb_id=<someid>&url=http://www.buy-viagra-online.com&excerpt=some%20interesting%info%about%viagra,phentermine%20and%20other%20crap

It should present you with a blank screen and log the trackback
spam attempt.  Check your blacklist log! A log entry about trackback
spam should be present.
