I've ported WP HashCash to Pivot! Whee! Current HashCash version: 2.3

This is a port of the GREAT work of Elliott Back. All praise and credit
goes to him!

http://elliottback.com/wp/archives/2005/05/11/wordpress-hashcash-20/

This is harder to setup than other parts of the blacklist and therefore NOT
supported. If you can't get this to work just don't use it!

I REPEAT: IF YOU HAVE NO CLUE WHAT THE HELL ALL THIS IS ABOUT: STOP READING
AND USE THE BLACKLIST AS DESCRIBED IN THE README FILE. WITHOUT HASHCASH IT'S
WORKING FINE AS WELL (AT LEAST FOR THE TIME BEING)

I've made it harder to enable than other settings for this very reason.
If you know what you're doing and you want to use the most sophisticated anti
commentspam weapon currently available, read on.

If you do EXACTLY what is described in the steps below you'll have a hashcash
armored weblog. If you screw up your site will be spam free as well! The only
drawback is that NOBODY will be able to comment anymore! :P

1) Copy all hashcash snippets to extensions/snippets
2) In entrypage_template.html add this tag within the <head> tag:

     [[hashcash_head]]

3) Open your _sub_commentform.html template and add this tag somewhere after
   the <form> tag:

     [[hashcash_hiddenfield]]

4) Also in _sub_commentform.html, find your <form> tag. Right now it says:

   <form method="post" action=......

   Change it into:

   <form [[hashcash_onsubmit]] method="post" action=......

5) Open a page from your blog with a commentform on it. Check the sourcecode.
   Verify that you see the following stuff added that wasn't there before:

   in the <head> you should find the text HASHCASH JAVASCRIPT. If you see
   it you're in business!

   Second, in the <form> tag you should see:

   onsubmit="tiyCKvVUn('d03a33a443c0997bcd8583e9d2fca84b');"

   The garbage strings are different. As long as something similar is
   there.

   Finally There should be this hidden form tag:

   <input type="hidden" id="KzJkEJCXEkBXk" name="tGtaCcbTjWknclR" value="62" />

   Again: the content will be different. As long as something similar is there.

   Great! Almost done!!! If all this looks good then the hard part is done.

8) If all the above stuff was done succesfully, you can enable the hashcash
   check on the blacklist settings screen.

If you can now still post on your blog it means it's working. Whenever hashcash blocks a spammer you'll see it in the log with a line like:

blocked hashcash violation (...)

If you cannot post anymore you did something wrong. *doh* and you're on your own. I've explained why.

Congrats! You made it!

- Marco
