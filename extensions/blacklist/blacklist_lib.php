<?
// Pivot-Blacklist version 0.9.3
//
// A simple (but effective) spam blocker based on the MT-Blacklist
// available at: http://www.jayallen.org/comment_spam/
//
// Includes realtime blacklist check functions by
// John Sinteur (http://weblog.sinteur.com/)
//
// This code (c) 2004-2005 by Marco van Hylckama Vlieg
// License is GPL, just like Pivot
//
// http://www.i-marco.nl/
// marco@i-marco.nl

@ini_set("include_path", ".");
require_once("hashcash_lib.php");


define("__VERSION", "0.9.3");
$sLogType = "";



// uncomment this line if you're using Nucleus:

//$sLogType = "nucleus";

// don't edit anything after this line

if($sLogType == "nucleus")  {
	include_once("nucleus_specific.php");
} else  {
	include_once("pivot_specific.php");
}

include_once("httpclient.php");

function is_domain($stheDomain) {
	return ( (strpos($stheDomain,"\\")==0) && (strpos($stheDomain,"[")==0) && (strpos($stheDomain, "(")==0) );
}
function pbl_getconfig()  {

	// parse the configuration file

	$pbl_config = array();
	if (file_exists(__WEBLOG_ROOT.__EXT."/settings/blacklist.conf"))  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/blacklist.conf", "r");
		while (!feof($fpHandle)) {
			$sBuffer = fgets($fpHandle, 4096);
			$sConfigParam = explode("|", $sBuffer);
			$sKey = $sConfigParam[0];
			$sValue = $sConfigParam[1];
			if(strlen($sKey) > 1)  {
				$pbl_config[$sKey] = $sValue;
			}
		}
	}
	return $pbl_config;
}

function pbl_checkforspam($sText)  {

	$aConfig = pbl_getconfig();


	// check whether a string contains spam
	// if it does, we return the rule that was matched first

	$sText = strtolower($sText);

	// first line of defense: Check whether our poster is using
	// an open proxy

	if($aConfig["proxy"] == 1)  {
		if(check_for_open_proxy())  {
			pbl_suspectIP($aConfig["blockstrikes"]);
			return "open spam proxy";
		}
	}

	// second line of defense: Check whether our poster promotes
	// known spamsite url's listed at www.surbl.org

	if($aConfig["surbl"] == 1)  {
		$sSurblURL = check_for_surbl($sText);
		if(strlen($sSurblURL) > 0)	{
			pbl_suspectIP($aConfig["blockstrikes"]);
			return("url listed on www.surbl.org found: $sSurblURL");
		}
	}

	// third line of defense: run the personal blacklist entries

	if (file_exists(__WEBLOG_ROOT.__EXT.'/settings/personal_blacklist.pbl'))  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/personal_blacklist.pbl", "r");
		while (!feof($fpHandle)) {
			$sBuffer = fgets($fpHandle, 4096);
			$aSplitbuffer = explode("####", $sBuffer);
			$sExpression = $aSplitbuffer[0];
			$sOrgExpression = $sExpression;
			if(is_domain($sExpression))  {
				$sExpression = str_replace(".","\.",$sExpression);
			}
			if((strlen(trim($sExpression)) > 1) && (preg_match("/".trim($sExpression)."/", $sText)))  {
				pbl_logRule($sOrgExpression);
				pbl_suspectIP($aConfig["blockstrikes"]);
				return $sExpression;
			}
		}
	}

	// w00t! it's probably not spam!

	return "";
}

function pbl_updateblacklist($bForce=false)  {

        // It's dead, Jim
           return "yes";

	}


function pbl_processblacklist()  {

	// reformat the list to match our own format

	$sListString = "";
	$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/blacklist.txt", "r")  or die ("could not open: ".__WEBLOG_ROOT.__EXT."/settings/blacklist.txt");
	while (!feof($fpHandle)) {
		$sBuffer = fgets($fpHandle, 4096);
		$aSplitbuffer = explode("#", $sBuffer);
		$sExpression = $aSplitbuffer[0];
		$explodedSplitBuffer = explode("/", $sExpression);
		$sExpression = $explodedSplitBuffer[0];
		if (strlen($sExpression) > 0)  {
			$sListString .= trim($sExpression);
			if(strlen($aSplitbuffer[1]) > 5)  {
				$sListString .= " #### ".trim($aSplitbuffer[1]);
			}
			$sListString .= "\n";
		}
	}
	fclose($fpHandle);
	if(file_exists(__WEBLOG_ROOT.__EXT.'/settings/blacklist.pbl'))  {}
	$fpNewHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/blacklist.pbl", "w");
	fwrite($fpNewHandle, $sListString);
	fclose($fpNewHandle);
}


function pbl_whitelisted($sReferer)	{
	if(!file_exists(__WEBLOG_ROOT.__EXT."/settings/whitelist.pbl"))	{
		return true;
	}
	$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/whitelist.pbl", "r");
	while (!feof($fpHandle)) {
		$sBuffer = fgets($fpHandle, 4096);
		$aSplitbuffer = explode("####", $sBuffer);
		$sExpression = $aSplitbuffer[0];
		$explodedSplitBuffer = explode("/", $sExpression);
		$sExpression = $explodedSplitBuffer[0];
		if (strlen($sExpression) > 0)  {
			if(preg_match("/".trim($sExpression)."/", $sReferer))  {
				return true;
			}
		}
	}
	return false;
}

function pbl_whitelist()  {
	$aConfig = pbl_getconfig();
	if(pbl_siteban())	{
		exit;
	}
	if(($aConfig["refenabled"] == 1) && (!strstr($_SERVER["HTTP_REFERER"], $_SERVER["SERVER_NAME"])) && (strlen($_SERVER["HTTP_REFERER"]) > 0))  {
		$sTheReferer = $_SERVER["HTTP_REFERER"];
		if(pbl_whitelisted($sTheReferer))	{
			return true;
		}

		if (strlen(pbl_checkforspam($sTheReferer)) > 0)  {
			pbl_logspammer($sTheReferer, "breferer");
			if(headers_sent()) {
				echo "<meta http-equiv=\"refresh\" content=\"0;url=$sTheReferer\">";
			} else	{
				header("HTTP/1.0 404 Not Found");
			}
			exit;
		} else	{
			return true;
		}
	}
}
function pbl_siteban()  {
	$aConfig = pbl_getconfig();
	if(($aConfig["agressive"] == 1) && (file_exists(__WEBLOG_ROOT.__EXT.'/settings/blockip.pbl')))	{
		$fpBanFile = fopen(__WEBLOG_ROOT.__EXT.'/settings/blockip.pbl', 'r');
		while(!feof($fpBanFile))	{
			$sBanLine = fread($fpBanFile, 4096);
			if(strstr($sBanLine, $_SERVER["REMOTE_ADDR"]))	{
				echo "IP banned due to spamming.";
				return true;
			}
		}
	}
	return false;
}

function pbl_blacklist()  {

	pbl_updateblacklist();
	$aConfig = pbl_getconfig();
	if($aConfig["enabled"] == 1)  {


		if(strlen($aConfig["redirect"]) > 10)  {
			$sRedirURL = $aConfig["redirect"];
		} else  {
			$sRedirURL = "http://www.suespammers.org/";
		}
		// pre-scan the IP banlist. If it's a known spammer we
		// can block him immediately
		if(pbl_siteban())	{
			header("location:".$sRedirURL);
			exit;
		}

		global $Pivot_Vars;
		if((isset($Pivot_Vars["preview"])) || (isset($Pivot_Vars["vote"])))  {
			$noCheck = true;
		} else	{
			$noCheck = false;
		}
		// Do the Spamquiz check (if enabled). If Spamquiz is violated we
		// can block him immediately as well
		if(($aConfig["sqenabled"] == 1) && !$noCheck)  {
			if(strlen(trim($aConfig["sqquestion"])) > 0)  {
				if(strtolower(trim($_POST["pblanswer"])) != strtolower(trim($aConfig["sqanswer"])))	{
					pbl_logspammer($_SERVER["REMOTE_ADDR"], "spamquiz");
					pbl_suspectIP($aConfig["blockstrikes"]);
					header("location:".$sRedirURL);
					exit;
				}
			}
			setcookie("pblanswer", $_POST["pblanswer"], time()+3600*24*30, "/", preg_replace("/^www./", "",$_SERVER["SERVER_NAME"]));
		}

		if(($aConfig["hashcashenabled"] == 1) && !$noCheck)  {
			if(!hashcash_check_hidden_tag() && !$noCheck)	{
				pbl_suspectIP($aConfig["blockstrikes"]);
				header("location:".$sRedirURL);
				exit;
			}
		}

		// Do the OSA check (if enabled). If OSA is violated we
		// can block him immediately as well
		if(($aConfig["osaenabled"] == 1) && !$noCheck)  {
			if(!pbl_osa())	{
				pbl_suspectIP($aConfig["blockstrikes"]);
				header("location:".$sRedirURL);
				exit;
			}
		}


		// the main spam detection routine to call from within
		// whatever script processes comments in our blogsystem
		// We check for Pivot's preview / vote in order to let that
		// through. Won't harm the other weblogs anyway since these
		// vars don't exist there.

		if((!isset($Pivot_Vars["preview"])) && (!isset($Pivot_Vars["vote"])))  {


			foreach($_POST as $sMessagePart)	{
				$sScanText .= $sMessagePart."  ";
			}
			$sSpamDetected = pbl_checkforspam($sScanText);
			if($sSpamDetected != "")  {
				pbl_logspammer($sSpamDetected, "comment");
				//header("HTTP/1.0 404 Not Found");
				header("location:".$sRedirURL);
				exit;
			}
		}
	}
}

function pbl_menu()  {
	echo "<div id=\"left\">\n";
	echo "<div id=\"MainMenu\">\n";
	echo "<ul>\n";
	echo "<li><a href=\"index.php\"><img src=\"images/icons/i_home.gif\"/>Main</a></li>\n";
	echo "<li><a href=\"index.php?page=settings\"><img src=\"images/icons/i_prefs.gif\"/>Blacklist Settings</a></li>\n";
	echo "<li><a href=\"index.php?page=blacklist\"><img src=\"images/icons/i_edit.gif\"/>Blacklist Editor</a></li>\n";
	echo "<li><a href=\"index.php?page=referers\"><img src=\"images/icons/i_referer.gif\"/>Referer Whitelist</a></li>\n";
	echo "<li><a href=\"index.php?page=htaccess&limit=100\"><img src=\"images/icons/i_accessrules.gif\"/>.htaccess generator</a></li>\n";
	echo "<li><a href=\"index.php?page=ipblocks\"><img src=\"images/icons/i_ipblocks.gif\"/>Blocked IP's</a></li>\n";
	echo "<li><a href=\"index.php?page=log&limit=100\"><img src=\"images/icons/i_log.gif\"/>Blacklist Log</a></li>\n";
	echo "<li><a href=\"index.php?page=logout\"><img src=\"images/icons/i_logout.gif\"/>Logout</a></li>\n";
	echo "</ul>\n";
	echo "</div>\n";
	echo "<br/><br/>";
	echo "<a style=\"border:0px; padding:0px; margin:10px;margin-bottom:5px;\" href=\"http://www.i-marco.nl/pivot-blacklist/\"><img style=\"border:0px\" src=\"images/pblbutton.png\" alt=\"Pivot Blacklist\"/></a><br/>";
	echo "<a style=\"border:0px; padding:0px; margin:10px;margin-bottom:5px;\" href=\"http://spammerbegone.com/\" target=\"_blank\"><img src=\"http://spammerbegone.com/images/sbg_94x15.png\" title=\"SPAMMERS SUCK! We redirect ours to SpammerBeGone.com!\" width=\"94\" height=\"15\" border=\"0\" /></a>";
	echo "</div>\n";
}
function pbl_referereditor()  {
	echo "<div id=\"main\">";
	global $g_sPBLMessage;
	//$sRefererfile = fopen(__WEBLOG_ROOT."/pivot/includes/getref.inc.php", "r");

	if(strlen($g_sPBLMessage) > 0)  {
		echo "<div class=\"pblmessage\">$g_sPBLMessage</div>\n";
	}

	global $pblmessage;
	if(strlen($pblmessage) > 0)  {
		echo "<div class=\"pblmessage\">$pblmessage</div>\n";
	}
	echo "<div class=\"pbldescription\">\n";
	echo "<h5>Referer whitelist editor</h5>";
	echo "If you have a list of recent referers on your blog you can use this to block referer spammers. Incoming hits that have a blacklisted referer will be kicked back to their own (pathetic) website\n";
	echo "</div>\n";

	echo "<div id=\"personal\">\n";
	echo "<div class=\"pbldescription\">";
	echo "Use this form to add domains (or parts of it) that link to your site and aren't spammers in order to improve performance. By default some search engines are listed. Incoming requests with referers that match an entry in this list will not be scanned by Pivot-Blacklist.";
	echo "</div>\n";
	echo "</div>\n";
	echo "<div class=\"pbform\">\n";
	echo "<form action=\"index.php\" method=\"get\">\n";
	echo "<input type=\"hidden\" name=\"page\" value=\"addwhitelist\">\n";
	echo "<table class=\"pblform\">\n";
	echo "<tr>\n";
	echo "<td><b>Domain</b></td>\n";
	echo "<td><input class=\"pbltextinput\" type=\"text\" name=\"expression\" /></td>\n";
	echo "</tr>\n";
	echo "<tr>";
	echo "<td><b>Comment</b></td>\n";
	echo "<td><input class=\"pbltextinput\" type=\"text\" name=\"comment\" /></td>\n";
	echo "</tr>\n";
	echo "<td colspan=\"2\" style=\"border:none;\"><input type=\"submit\" value=\"Add\"></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</div>\n";
	echo "<div class=\"pbldescription\">Below is your personal referer whitelist</div>\n";
	if (file_exists(__WEBLOG_ROOT.__EXT.'/settings/whitelist.pbl'))  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/whitelist.pbl", "r");
		echo "<table>\n";
		echo "<tr>\n";
		echo "<th>(part of) domain</th>\n";
		echo "<th>comment</th>\n";
		echo "<th>deletion</th>\n";
		echo "</tr>\n";
		$nLine = 0;
		while (!feof($fpHandle)) {
			$sBuffer = fgets($fpHandle, 4096);
			$nLine++;
			$sConfigParam = explode("####", $sBuffer);
			$sKey = $sConfigParam[0];
			$sValue = $sConfigParam[1];
			if(strlen($sKey) > 0)  {
				echo "<tr>\n";
				echo "<td>".$sKey."</td>\n";
				echo "<td>".$sValue."</td>\n";
				echo "<td>";
				echo "<a href=\"index.php?page=deletewhitelist&line=".$nLine."\">delete</a>";
				echo "</td>";
				echo "</tr>\n";
			}
		}
		echo "</table>\n";
		echo "<br /><br />\n";
		echo "</div>\n";
	}
}

function pbl_blacklisteditor()  {

	global $g_sPBLMessage;
	$list_enabled = false;
	if(file_exists(__WEBLOG_ROOT."/pivot/submit.php"))	{
		$fpSubmitFile = fopen(__WEBLOG_ROOT."/pivot/submit.php", "r");
		while(!feof($fpSubmitFile))  {
			$sBuffer = fgets($fpSubmitFile, 4096);
			if((strstr($sBuffer, "blacklist.php")) && (!strstr($sBuffer, "//")))  {
				$list_enabled = true;
				fclose($fpSubmitFile);
				break;
			}
		}
	}
	if(!$list_enabled) {

		$g_sPBLMessage = "It seems you didn't read the manual or forgot to add the line as specified in the README to:<br/><br/> <b>".__WEBLOG_ROOT."/pivot/submit.php</b><br/><br/> Therefore Pivot Blacklist will not work until you fix this.<br/><br/>You have to add the following line right below the license information in the file:<br/><br/><b>require_once('../extensions/blacklist/blacklist.php');</b>";
	}




	if(strlen($g_sPBLMessage) > 0)  {
		echo "<div class=\"pblmessage\">$g_sPBLMessage</div>\n";
	}

	echo "<div class=\"pbldescription\">";
	echo "<h5>Blacklist Editor</h5>";
		echo "</div>\n";

	echo "<div id=\"personal\">\n";
	echo "<div class=\"pbldescription\">";
	echo "You can add url's, regular expressions or words to your personal blacklist below.";
	echo "</div>\n";
	echo "<div class=\"pbform\">\n";
	echo "<form action=\"index.php\" method=\"get\">\n";
	echo "<input type=\"hidden\" name=\"page\" value=\"addpersonal\">\n";
	echo "<table class=\"pblform\">\n";
	echo "<tr>\n";
	echo "<td><b>Expression</b></td>\n";
	echo "<td><input class=\"pbltextinput\" type=\"text\" name=\"expression\" /></td>\n";
	echo "</tr>\n";
	echo "<tr>";
	echo "<td><b>Comment</b></td>\n";
	echo "<td><input class=\"pbltextinput\" type=\"text\" name=\"comment\" /></td>\n";
	echo "</tr>\n";
	echo "<td colspan=\"2\" style=\"border:none;\"><input type=\"submit\" value=\"Add\"></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</div>\n";
	echo "</div>\n";
	echo "<div class=\"pbldescription\">Below is your personal blacklist</div>\n";
	if (file_exists(__WEBLOG_ROOT.__EXT.'/settings/personal_blacklist.pbl'))  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/personal_blacklist.pbl", "r");
		echo "<table>\n";
		echo "<tr>\n";
		echo "<th>expression</th>\n";
		echo "<th>comment</th>\n";
		echo "<th>deletion</th>\n";
		echo "</tr>\n";
		$nLine = 0;
		while (!feof($fpHandle)) {
			$sBuffer = fgets($fpHandle, 4096);
			$nLine++;
			$sConfigParam = explode("####", $sBuffer);
			$sKey = $sConfigParam[0];
			$sValue = $sConfigParam[1];
			if(strlen($sKey) > 0)  {
				echo "<tr>\n";
				echo "<td>".$sKey."</td>\n";
				echo "<td>".$sValue."</td>\n";
				echo "<td>";
				echo "<a href=\"index.php?page=deleteexpression&line=".$nLine."\">delete</a>";
				echo "</td>";
				echo "</tr>\n";
			}
		}
		echo "</table>\n";
		echo "</div>";
	}
}
function pbl_deletewhitelist()  {
	if(isset($_GET["line"]))  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/whitelist.pbl", "r");
		$nLine = 0;
		$sNewFile = "";
		while (!feof($fpHandle)) {
			$sBuffer = fgets($fpHandle, 4096);
			$nLine++;
			if($nLine != $_GET["line"])  {
				$sNewFile .= $sBuffer;
			}
		}
		fclose($fpHandle);
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/whitelist.pbl", "w");
		fwrite($fpHandle, $sNewFile);
		fclose($fpHandle);
	}
}
function pbl_deleteexpression()  {
	if(isset($_GET["line"]))  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/personal_blacklist.pbl", "r");
		$nLine = 0;
		$sNewFile = "";
		while (!feof($fpHandle)) {
			$sBuffer = fgets($fpHandle, 4096);
			$nLine++;
			if($nLine != $_GET["line"])  {
				$sNewFile .= $sBuffer;
			}
		}
		fclose($fpHandle);
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/personal_blacklist.pbl", "w");
		fwrite($fpHandle, $sNewFile);
		fclose($fpHandle);
	}
}
function pbl_addwhitelistdomain()  {
	if(strlen($_GET["expression"]) > 0)  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/whitelist.pbl", "a");
		if(strlen($_GET["comment"]) > 0)  {
			$sExpression = stripslashes($_GET["expression"])." #### ".$_GET["comment"];
		} else  {
			$sExpression = stripslashes($_GET["expression"]);
		}
		fwrite($fpHandle, $sExpression."\n");
		fclose($fpHandle);
	}
}
function pbl_addexpression()  {
	if(strlen($_GET["expression"]) > 0)  {
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/personal_blacklist.pbl", "a");
		if(strlen($_GET["comment"]) > 0)  {
			$sExpression = stripslashes($_GET["expression"])." #### ".$_GET["comment"];
		} else  {
			$sExpression = stripslashes($_GET["expression"]);
		}
		fwrite($fpHandle, $sExpression."\n");
		fclose($fpHandle);
	}
}

function pbl_editsettings()  {
	echo "<div id=\"main\">";
	if(isset($_GET["message"]))  {
		echo "<div class=\"pblmessage\">".$_GET["message"]."</div>\n";
	}
	echo "<div class=\"pbldescription\">";
	echo "<h5>Edit blacklist settings</h5>";
	echo "You can edit your blacklist settings below. The redirect URL is a user defined page/site to which a spammer will be redirected after a failed spamming-attempt.</div>\n";
	echo "<div class=\"pbform\">\n";
	echo "<form action=\"index.php\" method=\"get\">\n";
	echo "<input type=\"hidden\" name=\"page\" value=\"savesettings\">\n";
	echo "<input type=\"hidden\" name=\"message\" value=\"Your settings have been updated.\">\n";
	echo "<table>\n";
	$aConfigArray = pbl_getconfig();
	echo "<tr>\n";
	echo "<td><b>Engine</b></td>\n<td style=\"text-align:left\"><input name=\"enabled\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["enabled"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>Spam blocking engine enabled";
	echo "</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td style=\"vertical-align:top;\"><b>OSA</b><br/><br/><div style=\"width:120px;\"><small>Owen's Spam Action is an extra protection against spam bots. A key is added to the commentform which must be matched on the server. It's a nice extra protection and safe to enable as long as you ALSO use the snippet according to the directions.</small></div></td>\n<td style=\"text-align:left\"><input name=\"osaenabled\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["osaenabled"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>Owen's Spam Action enabled";
	echo "<br /><br />Key phrase for OSA (type anything): <input type=\"text\" name=\"osaword\" value=\"".$aConfigArray["osaword"]."\"/><br/>";
	echo "Time limit for comment submission once form is loaded: ";
	echo "<select name=\"osalimit\">";
	echo "<option value=\"0\">no time limit</option>";
	for($i=0;$i<61;($i+=5))	{
		if($i > 0)	{
			echo "<option value=\"$i\"";
			if($aConfigArray["osalimit"] == $i)	{
				echo " selected";
			}
			echo ">$i minutes\n";
		}
	}
	echo "</select>";
	echo "<br /><br /><b>DON'T set this too low! People need some time <br />to write their comment!</b> If a comment is submitted with a timestamp older than<br/> the amount of minutes you set above or if the timestamp isn't there at all the comment is blocked. <br/>The limit is disabled by default.";

	echo "<br /><br />To use OSA you MUST add the OSA snippet that comes with Pivot-Blacklist to your snippets<br />directory (extensions/snippets) and add the pivot tag <b>[[osa]]</b><br />To your <b>_sub_commentform.html</b> template, somewhere <b>after</b> the &lt;form&gt; tag.\n";
	echo "</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td style=\"vertical-align:top;\"><b>HashCash</b><br/><br/><div style=\"width:120px;\"><small>HashCash is the most powerful, completely invisible spam protection available. It requires javascript to be enabled on the client. If this is unacceptable for you then don't enable it.</small></div></td>\n<td style=\"vertical-align:top;text-align:left\"><input name=\"hashcashenabled\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["hashcashenabled"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>HashCash enabled";
	echo "<br /><br />To use HashCash you MUST first read the instructions in <a href=\"README.HASHCASH.txt\">README.HASHCASH.txt</a> and follow them. Enabling HashCash here without doing this first will result in all comments being blocked.<br /><br />HashCash library for Pivot-Blacklist based on <a href=\"http://elliottback.com/wp/archives/2005/05/11/wordpress-hashcash-20/\">WP Hashcash 2.3</a> by <a href=\"http://www.elliottback.com/\">Elliott Back</a>\n";
	echo "</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td style=\"vertical-align:top;\"><div style=\"width:120px;\"><b>Bot question</b><br/><br/><small>If you enter a question, an answer and an explanation text you can ask the user a simple question everyone knows the answer to. They have to enter the right answer in the comment form. This completely baffles automated spam bots because every blogger will choose something different.</small></div></td>\n";
	echo "<td style=\"vertical-align:top;text-align:left\">";
	echo "<input name=\"sqenabled\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["sqenabled"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>Bot question enabled<br />";

	echo "<table border=\"0\"><tr><td>Question:</td><td><input type=\"text\" style=\"width:400px;\" value=\"".stripslashes($aConfigArray["sqquestion"])."\" name=\"sqquestion\"><br /><small>Example: What are the first two letters of the word 'spam'?</small></td></tr><tr><td>Answer:</td><td><input type=\"text\" style=\"width:400px;\" name=\"sqanswer\" value=\"".stripslashes($aConfigArray["sqanswer"])."\"><br /><small>Example: <b>sp</b></small></td></tr><tr><td>Explanation</td><td><input type=\"text\" name=\"sqexplain\" value=\"".stripslashes($aConfigArray["sqexplain"])."\" style=\"width:400px\"><br /><small>Example: To prevent automated commentspam we require you to answer this silly question</small></td></tr></table><br/><br />To use the bot question, place the snipped snippet_spamquiz.php in extensions/snippets and add a tag <br /><b>[[spamquiz]]</b> to your comment form. Output formatting for this tag can be controlled with three<br />snippet parameters. Read documentation for more information.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td style=\"vertical-align:top;\"><div style=\"width:120px;\"><b>IP Blocking</b><br/><br/><small>This option effectively ban's IP's that are recognized as commentspammers by the core blacklist routine. You can configure the amount of 'strikes' before an IP is 'out'.</small></div></td>\n<td style=\"text-align:left\"><input name=\"agressive\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["agressive"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>Enable IP Blocking.<br/><br/>Block IP's after <input type=\"text\" size=\"2\" maxsize=\"2\" name=\"blockstrikes\" value=\"".$aConfigArray["blockstrikes"]."\">detected spam attempts.<br/><br/>IP blocking only blocks comment-spamming. For complete blocking use the <a href=\"index.php?page=htaccess\">.htaccess rules generator.</a>";
	echo "</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td style=\"vertical-align:top;\"><b>Referer spam</b></td>\n<td style=\"text-align:left\"><input name=\"refenabled\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["refenabled"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>Block incoming referers from spammers to prevent unwanted entries<br/>in your last referers list.<br/><br/>Note that this option doesn't do anyhing if you haven't installed<br/>the hardened referrers extension from the extras/ directory in the blacklist directory.";
	echo "<br/><br/>";
	echo "<input name=\"refwhiteonly\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["refwhiteonly"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>Ignore <b>all</b> referers except for those on your referer whitelist.";

	echo "</td>\n";
	echo "</tr>\n";

	echo "<tr>\n";
	echo "<td><b>Open proxies</b></td>\n<td style=\"text-align:left\"><input name=\"proxy\" value=\"1\" type=\"checkbox\" ";
	if($aConfigArray["proxy"] == 1)  {
		echo "checked=\"yes\"";
	}
	echo "/>Block comment posters on known spam proxy gateways";
	echo "</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	$bDisabledBox = false;
	$sWarningText = "";
	if(gethostbyname("linux.com.multi.surbl.org") != "linux.com.multi.surbl.org")	{
		echo "<input type=\"hidden\" name=\"lookupwarning\" value=\"1\">\n";
		$sWarningText = "<br/><br/><span style=\"color:#f00;\">WARNING: hostname lookups from within PHP don't seem to work properly<br/> on your server. Therefore this setting would block ANY url posted!<br/>The feature has been disabled until this is fixed.</span>";
		$bDisabledBox = true;
	}
	echo "<td><b>SURBL check</b></td>\n<td style=\"text-align:left\"><input name=\"surbl\" value=\"1\" type=\"checkbox\" ";
	if(($aConfigArray["surbl"] == 1) && (!$bDisabledBox))  {
		echo "checked=\"yes\"";
	}
	echo "/>Block comments containing links to sites listed at <a href=\"http://www.surbl.org/\">www.surbl.org</a>";
	echo $sWarningText;
	echo "</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td><b>Redirect URL</b></td>\n";
	echo "<td align=\"left\"><input class=\"pbltextinput\" type=\"text\" name=\"url\" value=\"".$aConfigArray["redirect"]."\"/></td>\n";
	echo "</tr>\n";
	echo "<tr><td align=\"left\" colspan=\"2\" style=\"border:none;\"><input type=\"submit\" value=\"save\"/></td></tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</div>\n";
}

function pbl_savesettings()  {
	if($_GET["enabled"] == 1)  {
		$sTheData = "enabled|".$_GET["enabled"]."\n";
	} else  {
		$sTheData = "enabled|0\n";
	}
	if($_GET["osaenabled"] == 1)  {
		$sTheData .= "osaenabled|".$_GET["osaenabled"]."\n";
	} else  {
		$sTheData .= "osaenabled|0\n";
	}
	if($_GET["hashcashenabled"] == 1)  {
		$sTheData .= "hashcashenabled|".$_GET["hashcashenabled"]."\n";
	} else  {
		$sTheData .= "hashcashenabled|0\n";
	}
	if($_GET["sqenabled"] == 1)  {
		$sTheData .= "sqenabled|".$_GET["sqenabled"]."\n";
	} else  {
		$sTheData .= "sqenabled|0\n";
	}
	if($_GET["agressive"] == 1)  {
		$sTheData .= "agressive|".$_GET["agressive"]."\n";
	} else  {
		$sTheData .= "agressive|0\n";
	}
	if($_GET["refwhiteonly"] == 1)  {
		$sTheData .= "refwhiteonly|".$_GET["refwhiteonly"]."\n";
	} else  {
		$sTheData .= "refwhiteonly|0\n";
	}
	if($_GET["proxy"] == 1)  {
		$sTheData .= "proxy|".$_GET["proxy"]."\n";
	} else  {
		$sTheData .= "proxy|0\n";
	}
	if($_GET["refenabled"] == 1)  {
		$sTheData .= "refenabled|".$_GET["proxy"]."\n";
	} else  {
		$sTheData .= "refenabled|0\n";
	}
	if(($_GET["surbl"] == 1) && (!isset($_GET["lookupwarning"])))  {
		$sTheData .= "surbl|".$_GET["surbl"]."\n";
	} else  {
		$sTheData .= "surbl|0\n";
	}
	$sTheData .= "redirect|".$_GET["url"]."\n";
	$sTheData .= "osaword|".$_GET["osaword"]."\n";
	$sTheData .= "osalimit|".$_GET["osalimit"]."\n";
	$sTheData .= "blockstrikes|".$_GET["blockstrikes"]."\n";
	$sTheData .= "sqquestion|".$_GET["sqquestion"]."\n";
	$sTheData .= "sqanswer|".$_GET["sqanswer"]."\n";
	$sTheData .= "sqexplain|".$_GET["sqexplain"]."\n";
	$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/blacklist.conf", "w");
	fwrite($fpHandle, $sTheData);
}

$g_reOk = false;

function _hdl($p_nErrno, $p_sErrstr) {
	global $g_reOk;
	$g_reOk = false;
}

function pbl_checkregexp($p_sRegexp) {

	// Thanks to 'OneOfBorg' on Gathering Of Tweakers
	// http://gathering.tweakers.net/forum/user_profile/109376

	global $g_reOk;
	$g_reOk = true;
	set_error_handler("_hdl");
	preg_match("/".$p_sRegexp."/", "");
	restore_error_handler();
	return $g_reOk;
}

function pbl_addwhitelist()  {
	if(isset($_GET["expression"]))  {
		if($_GET["expression"] != "")  {
			if(!pbl_checkregexp($_GET["expression"]))  {
				$pblmessage= "Your expression contained errors and couldn't be added: <b>".$_GET["expression"]."</b>";
			} else  {
				pbl_addwhitelistdomain();
				$pblmessage = "New domain added to your whitelist: <b>".stripslashes($_GET["expression"])."</b>";
			}
		} else  {
			$pblmessage = "There's no use in adding empty expressions.<b>".$_GET["expression"]."</b>";
		}
	}
	return $pblmessage;
}
function pbl_addpersonal()  {
	if(isset($_GET["expression"]))  {
		if($_GET["expression"] != "")  {
			if(!pbl_checkregexp($_GET["expression"]))  {
				echo "<div class=\"pblmessage\">Your expression contained errors and couldn't be added: <b>".$_GET["expression"]."</b></div>\n";
			} else  {
				$sExistTest = pbl_checkforspam($_GET["expression"]);

				if (strlen($sExistTest) > 0)  {
					echo "<div class=\"pblmessage\">Expression <b>".$_GET["expression"]."</b> already matched by the following rule in your system:<br/> <b>$sExistTest</b></div>\n";
				} else  {
					pbl_addexpression();
					echo "<div class=\"pblmessage\">New entry added to your list: <b>".stripslashes($_GET["expression"])."</b></div>";
				}
			}
		} else  {
			echo "<div class=\"pblmessage\">There's no use in adding empty expressions.<b>".$_GET["expression"]."</b></div>";
		}
	}
}

function pbl_logspammer($p_sSpam, $p_sType)  {

	$p_sSpam = rtrim($p_sSpam);
	$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/blacklist.log", "a");

	switch($p_sType)  {
		case "ireferer":
				$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."ignored referer: ".strip_tags(substr($p_sSpam,0,70))."...\n";
			break;
		case "breferer":
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked referer: ".strip_tags(substr($p_sSpam, 0, 70))."...\n";
			break;
		case "trackback":
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked trackback: ".stripslashes(strip_tags(substr($p_sSpam,0,70)))."...\n";
			break;
		case "htrackback":
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked trackback (hardened): ".stripslashes(strip_tags(substr($p_sSpam,0,70)))."...\n";
			break;
		case "hashcash":
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked hashcash violation: (".rtrim(substr(stripslashes(strip_tags($_POST["piv_comment"])),0,70))."...".")\n";
			break;
		case "spamquiz":
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked wrong quiz answer: (".stripslashes(rtrim(strip_tags(substr($_POST["piv_comment"],0,70))))."...)\n";
			break;
		case "osa":
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked osa violation (key mismatch) (".stripslashes(rtrim(strip_tags(substr($_POST["piv_comment"],0,70))))."...)\n";
			break;
		case "osalimit":
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked osa violation (form response time limit exceeded)\n";
			break;
		default:
			$sLogLine = date("F j, Y, g:i a")." #### ".$_SERVER["REMOTE_ADDR"]." #### "."blocked commentspam: ".$p_sSpam." (".stripslashes(rtrim(strip_tags(substr($_POST["piv_comment"],0,70))))."...)\n";
			break;
	}
	fwrite($fpHandle, $sLogLine);
	fclose($fpHandle);
}

function pbl_logtable()  {
	if (file_exists(__WEBLOG_ROOT.__EXT."/settings/blacklist.log"))  {
		echo "<div class=\"pbform\" style=\"margin-left:10px;\">\n";
		echo "<form action=\"index.php\" method=\"get\">\n";
		echo "<input type=\"hidden\" name=\"page\" value=\"resetlog\">\n";
		echo "<input type=\"submit\" value=\"Reset logs\" />\n";
		echo "</form>\n";
		echo "</div>\n";
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/blacklist.log", "r");
		$nTotalLines = count(file(__WEBLOG_ROOT.__EXT."/settings/blacklist.log"));
		$sLogRows = "";
		$nNumb=0;
		$bLimit = false;
		$nLineCount = 0;
		if(isset($_GET["limit"]))	{
			$bLimit = true;
		}
		while (!feof($fpHandle)) {
			$sBuffer = fgets($fpHandle, 4096);
			$nLineCount++;
			$sThisLine = explode("####", $sBuffer);
			if($sThisLine[0] != "")  {
				if(($bLimit) && ($nLineCount <= ($nTotalLines-$_GET["limit"])))	{
					$sLogRows = "";
				} else	{
					$sLogRows .= "<tr>";
					$sLogRows .= "<td class=\"log$nNumb\">$sThisLine[0]</td>";
					$sLogRows .= "<td class=\"log$nNumb\"><a target=\"_blank\" href=\"http://centralops.net/co/DomainDossier.aspx?dom_whois=1&net_whois=1&dom_dns=1&addr=".$sThisLine[1]."\">".$sThisLine[1]."</a></td>";
					$sLogRows .= "<td class=\"log$nNumb\">".htmlentities(strip_tags(substr($sThisLine[2],0,70)))."</td>";
					$sLogRows .= "</tr>\n";
				}
			}
			if($nNumb == 0)
				$nNumb=1;
			else
				$nNumb=0;
		}
		fclose($fpHandle);
		echo "<table class=\"pbllog\">\n";
		echo "<tr><th>Date/Time</th><th>IP</th><th>Rule Matched</th></tr>\n";
		echo $sLogRows;
		echo "</table>\n";
	}
	if(strlen($sLogRows) < 10)  {
		echo "<div class=\"pbldescription\">Your log is empty.</div>\n";
	}
}
function pbl_resetlog()	{
	if(file_exists(__WEBLOG_ROOT.__EXT."/settings/blacklist.log"))	{
		$fpHandle = fopen(__WEBLOG_ROOT.__EXT."/settings/blacklist.log", "w");
		fwrite($fpHandle, "");
		fclose($fpHandle);
	}
}


function pbl_htrackback()	{
	if(pbl_siteban())  {
		exit;
	}
	$check_vars = array_merge($_POST, $_GET, $_SERVER);
	if(strlen($_GET["key"]) < 32)	{
		pbl_logspammer('tampered key: invalid length', "htrackback");
		exit;
	} else	{
		if(!preg_match('/^[a-f0-9]{32}$/',$_GET["key"]))  {
			pbl_logspammer('tampered key: invalid characters found', "htrackback");
			exit;

			}
		if(file_exists("settings/tbkey_".$_GET["key"]))	{
			if(filectime(__WEBLOG_ROOT.__EXT."/settings/tbkey_".$_GET["key"]) < (time() - 900))	{
				$aConfig = pbl_getconfig();
				@unlink(__WEBLOG_ROOT.__EXT."/settings/tbkey_".$_GET["key"]);
				pbl_suspectIP($aConfig["blockstrikes"]);
				pbl_logspammer(stripslashes(urldecode($check_vars['excerpt'])), "htrackback");
				exit;
			}
		}
		unlink(__WEBLOG_ROOT.__EXT."/settings/tbkey_".$_GET["key"]);
		return true;
	}
}
function pbl_trackback()  {
	if(pbl_siteban())  {
		exit;
	}
	$check_vars = array_merge($_POST, $_GET, $_SERVER);
	$checkString =urldecode(stripslashes($check_vars['blog_name'])).urldecode(stripslashes($check_Vars['title'])).urldecode($check_vars['url']).urldecode(stripslashes($check_vars['excerpt']));
	$sSpam = pbl_checkforspam($checkString);
	if(strlen($sSpam) > 0)	{
		pbl_logspammer(urldecode(stripslashes($check_vars['excerpt'])), "trackback");
		exit;
	}
}

function pbl_osa()	{
	if(($_GET["vote"] != "") && (strlen($_POST["piv_comment"]) == 0))  {
		return true;
	}
	$nOsaTime = $_POST["osatime"];
	$nNow = time();
	$aConfig = pbl_getconfig();
	$sSecretWord = $aConfig["osaword"];
	$sMashString = $_SERVER["REMOTE_ADDR"];
	$sMashString .=$sSecretWord;
	$sCheckSum = MD5(trim($sMashString));
	$bTime = (($_POST["osatime"] + ($aConfig["osalimit"] * 60)) > $nNow);
	if($sCheckSum != $_POST["osa"])	{
		pbl_logspammer($_SERVER["REMOTE_ADDR"], "osa");
		return false;
	}
	if((!$bTime) && ($aConfig["osalimit"] > 0))	{
		pbl_logspammer($_SERVER["REMOTE_ADDR"], "osalimit");
		return false;
	}
	return true;
}
/*
 
check_for_open_proxy() and check_for_surbl() written by John Sinteur as a MT Plugin
and very slightly modified by Marco van Hylckama Vlieg (just the return values)
 
John's original notice:
 
Plugin Name: Block-lists anti-spam measures
Version: 1.1
Plugin URI: http://weblog.sinteur.com/index.php?p=8106
Description: check if a comment poster is on an open proxy list, and check if the content contains known spammer domains
Author: John Sinteur
Author URI: http://weblog.sinteur.com/
*/


function check_for_open_proxy()	{

	$sSpammer_ip = $_SERVER['REMOTE_ADDR'];
	list($a, $b, $c, $d) = split('.', $sSpammer_ip);
	if( gethostbyname("$d.$c.$b.$a.list.dsbl.org") != "$d.$c.$b.$a.list.dsbl.org") {
		return true;
	}
	return false;
}


function check_for_surbl ( $comment_text ) {
	/*  for a full explanation, see http://www.surbl.org
	summary: blocks comment if it contains an url that's on a known spammers list.
	*/

	//get site names found in body of comment.
	$sRegexpgex_url   = "/(www\.)([^\/\"<\s]*)/im";
	$mk_regex_array = array();
	preg_match_all($sRegexpgex_url, $comment_text, $mk_regex_array);

	for( $cnt=0; $cnt < count($mk_regex_array[2]); $cnt++ ) {
		$domain_to_test = rtrim($mk_regex_array[2][$cnt],"\\");

		if (strlen($domain_to_test) > 3) {
			$domain_to_test = $domain_to_test . ".multi.surbl.org";
			if( gethostbyname($domain_to_test) != $domain_to_test ) {
				return $domain_to_test;
			}
		}
	}
	return "";
}

// Xiffy's htaccess and ip blocking functionality
// This code comes from Nucleus Blacklist
// http://wakka.xiffy.net/blacklist

function pbl_blockIP () {
	$remote_ip = $_SERVER['REMOTE_ADDR'];
	$filename  = __WEBLOG_ROOT.__EXT."/settings/blockip.pbl";
	$block     = false;
	// already in ipblock?
	if (file_exists($filename)) {
		$fp = fopen(__WEBLOG_ROOT.__EXT."/settings/blockip.pbl", "r");
		while ($line = fgets($fp,255)) {

			if (! (strpos ($line, $remote_ip) === false)) {
				$block = true;
			}

		}
		fclose ($fp);
	} else {
		$fp = fopen(__WEBLOG_ROOT.__EXT."/settings/blockip.pbl", "w");
		fwrite($fp, "");
		fclose ($fp);
	}
	return $block;
}

function pbl_logRule($expression) {
	$filename  = __WEBLOG_ROOT.__EXT."/settings/matched.pbl";
	$count = 0;
	$fp = fopen($filename,"r+");
	if ($fp) {
		while ($line = fgets($fp, 4096)) {
			if (! (strpos($line, $expression) === false )) {
				$count++;
				break;
			}
		}
		fclose($fp);
	}
	if ($count == 0 && !trim($expression) == "" ) {
		$fp = fopen($filename,"a+");
		fwrite($fp,$expression."\n");
	}
}

// this function logs all ip-adresses in a 'suspected ip-list'
// if the ip of the currently catched spammer is above the ip-treshold (plugin option) then
// the spamming ipaddress is transfered to the blocked-ip list.
// this list is the first line of defense, so notorious spamming machine will be kicked of real fast
// improves blacklist performance
// possible danger: blacklisting real humans who post on-the-edge comments
function pbl_suspectIP ($threshold) {
	$remote_ip = $_SERVER['REMOTE_ADDR'];
	$filename  = __WEBLOG_ROOT.__EXT."/settings/suspects.pbl";
	$blockfile = __WEBLOG_ROOT.__EXT."/settings/blockip.pbl";
	$count     = 0;
	// suspectfile ?
	if (! file_exists($filename)) {
		$fp = fopen($filename, "w");
		fwrite($fp, "");
		fclose ($fp);
	}

	$fp = fopen($filename, "r");
	while ($line = fgets($fp,255)) {
		if (! (strpos ($line, $remote_ip) === false)) {
			$count++;
		}
	}
	fclose ($fp);

	// not above threshold ? add ip to suspect ...
	if ($count < $threshold) {
		$fp = fopen($filename,'a+');
		fwrite($fp,$remote_ip."\n");
		fclose($fp);
	} else {
		// remove from suspect to ip-block
		$fp = fopen($filename, "r");
		$rewrite = "";
		while ($line = fgets($fp,255)) {
			// keep all lines except the catched ip-address

			if ((strpos ($line, $remote_ip) === false)) {
				$rewrite .= $line;
			}

		}
		fclose($fp);
		$fp = fopen($filename, "w");
		fwrite($fp, $rewrite);
		fclose ($fp);
		// transfer to blocked-ip file
		$fp = fopen($blockfile,'a+');
		fwrite($fp,$remote_ip."\n");
		fclose($fp);
	}
}

function pbl_showipblock() {
	$filename  = __WEBLOG_ROOT.__EXT."/settings/blockip.pbl";
	$line = 0;
	$fp = fopen($filename,'r');
	echo "<div id=\"main\">";
	echo "<div class=\"pbldescription\">";
	echo "Below are the IP's blocked by Pivot-Blacklist. Users from these IP's can't post comments anymore. Note that it's better to use the .htaccess rules generator results to block IP's at webserver level. This will save you valuable bandwidth.";
	echo "</div>";
	echo "<table>";
	echo "<tr>\n";
	echo "<th>IP Address</th>\n";
	echo "<th>reversed lookup</th>\n";
	echo "<th>deletion</th>\n";
	echo "</tr>\n";
	while ($ip = fgets($fp,255)) {
		$line++;
		echo "<tr><td><a href=\"http://centralops.net/co/DomainDossier.aspx?dom_whois=1&net_whois=1&dom_dns=1&addr=".$ip."\">".$ip."</a></td><td>[".gethostbyaddr(rtrim($ip))."]</td><td>";
		echo "<a href=\"".$_SERVER['PHP_SELF']."?page=deleteipblock&amp;line=".$line."\">delete</a>";
		echo "</td></tr>";
	}
	echo "</table>";
	echo "</div>";
}

function pbl_deleteipblock() {
	echo "<div class=\"pblmessage\">IP number deleted from your blocked IP's list.</div>";
	$filename  = __WEBLOG_ROOT.__EXT."/settings/blockip.pbl";
	if(isset($_GET["line"]))  {
		$handle = fopen($filename, "r");
		$line = 0;
		$newFile = "";
		while (!feof($handle)) {
			$buffer = fgets($handle, 4096);
			$line++;
			if($line != $_GET["line"])  {
				$newFile .= $buffer;
			}
		}
		fclose($handle);
		$handle = fopen($filename, "w");
		fwrite($handle, $newFile);
		fclose($handle);
	}
}

function pbl_htaccess($type) {
	$htaccess = "";
	switch($type) {
		case "ip":
			$filename  = __WEBLOG_ROOT.__EXT."/settings/blockip.pbl";
			$htaccess  = "# This htaccess snippet blocks machine based on IP Address. \n"
			             . "# these lines are generated by Pivot-Blackist\n"
			             . "# This functionality was brought to you by Xiffy, author of\n"
			             . "# Nucleus-Blacklist. (http://wakka.xiffy.nl/blacklist)\n";
			break;
		case "rules":
			$filename  = __WEBLOG_ROOT.__EXT."/settings/matched.pbl";
			$htaccess  = "# This htaccess snippet blocks machine based on referrers. \n"
			             . "# these lines are generated by Pivot-Blackist\n"
			             . "# This functionality was brought to you by Xiffy, author of\n"
			             . "# Nucleus-Blacklist. (http://wakka.xiffy.nl/blacklist)\n"
			             . "#\n"
			             . "# BEGIN COPY BELOW THIS LINE\n"
			             . "# RULES GENERATED AT:".date("M-d-Y: H:m")."\n"
			             . "RewriteEngine On\n";
			break;
		default:
			$htaccess = "Here you can generate two types of .htaccess snippets. The first part is based on blocked ip's. This is only relevant if you have IP blocking enabled in the options. \nThe other part is referrer based rewrite rules. Blacklist stores all rules matched in a different file. With this tool you convert these matched rules into .htaccess rewrite rules which you can incorporate into your existings .htaccess file (Apache only)\n After you've added the snippet to your .htaccess file it's safe and wise to reset the blocked ip list and/or matched rules file. That way you won't end up with double rules inside your .htaccess file\n";
			return $htaccess;
	}

	$fp = fopen($filename, 'r');
	$count = 0;
	while ($line = fgets($fp,4096)) {
		if ($type == "ip") {
			$htaccess .= "deny from ".$line;
		} else {
			if (rtrim($line) != "" ) {
				if ($count > 0) {
					$htaccess .= "[NC,OR]\n";
				}
				// preg_replace does the magic of converting . into \. while keeping \. and _. intact
				$htaccess .= "RewriteCond %{HTTP_REFERER} ". preg_replace("/([^\\\|^_]|^)\./",'$1\\.',rtrim($line)).".*$ ";
				$count++;
			}
		}
	}
	if ($type != "ip") {
		$htaccess .= "\nRewriteRule .* n [F,L]\n";
		$htaccess .= "# END COPY ABOVE THIS LINE\n";
	}
	return $htaccess;
}

function pbl_htaccesspage() {
	global $pblmessage;
	if(strlen($pblmessage) > 0)  {
		echo "<div class=\"pblmessage\">$pblmessage</div>\n";
	}

	if (isset($_POST["type"])) {
		if (strstr($_POST["type"],"blocked")) {
			$type = 'ip';
		} else {
			$type = 'rules';
		}
	}
	echo "<div class=\"pbform\" style=\"margin-left:10px;\">\n";
	echo "<form action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
	echo "<input type=\"submit\" label=\"ip\" value=\"Generate blocked IP's\" name=\"type\" />\n";
	echo "<input type=\"submit\" label=\"ip\" value=\"Generate rewrite rules\" name=\"type\" />\n";
	echo "<br />";
	echo "<br />";
	echo "<input type=\"hidden\" name=\"page\" value=\"htaccess\" />\n";
	echo "<textarea class=\"pbltextinput\" cols=\"60\" rows=\"15\" name=\"snippet\" >". pbl_htaccess($type)."</textarea><br />";
	echo "<br />";
	echo "<input title=\"this will clean your block IP addresses file\" type=\"submit\" label=\"ip\" value=\"Reset blocked IP's\" name=\"type\" />\n";
	echo "<input title=\"This will clean your matched file\" type=\"submit\" label=\"ip\" value=\"Reset rewrite rules\" name=\"type\" />\n";
	echo "</form>\n";
	// if user asked for a reset, do it now
	if (stristr($_POST["type"],"reset")) {
		echo "restting file ...";
		pbl_resetfile($type);
	}
	echo "</div>\n";
} // pbl_htaccesspage()

function pbl_resetfile($type) {
	global $pblmessage;
	switch ($type) {
		case 'log':
			$filename = __WEBLOG_ROOT.__EXT."/settings/blacklist.log";
			break;
		case 'ip':
			$filename  = __WEBLOG_ROOT.__EXT."/settings/blockip.pbl";
			break;
		case 'rules':
			$filename  = __WEBLOG_ROOT.__EXT."/settings/matched.pbl";
			break;
	}
	if(file_exists($filename))	{
		$fp = fopen($filename, "w");
		fwrite($fp, "");
		fclose($fp);
	}
}

function pbl_test () {
	// test's user input, no loggin.
	global $pblmessage;
	if(isset($_GET["expression"]))  {
		if($_GET["expression"] != "")  {
			$pblmessage = "Your expression: <br />".htmlspecialchars($_GET["expression"]);
			$return = pbl_checkforspam($_GET["expression"],false,0,false);

			if (! $return == "" ) {
				$pblmessage .= "<br />matched rule: <strong>".$return."</strong>";
			} else {
				$pblmessage .= "<br /> did not match any rule.";
			}
		}
	}
}

function pbl_testpage () {
	// shows user testpage ...
	global $pblmessage;
	if(strlen($pblmessage) > 0)  {
		echo "<div class=\"pblmessage\">$pblmessage</div>\n";
	}
	echo "<div class=\"pbform\" style=\"margin-left:10px;\">\n";
	echo "<form action=\"".$_SERVER['PHP_SELF']."\" method=\"get\">\n";
	echo "<input type=\"hidden\" name=\"page\" value=\"test\" />\n";
	echo "<textarea class=\"pbltextinput\" cols=\"60\" rows=\"6\" name=\"expression\" ></textarea><br />";
	echo "<input type=\"submit\" value=\"Test this\" />\n";
	echo "</form>\n";
	echo "</div>\n";
}

function pbl_htaccessrules()  {
	global $pblmessage;
	echo "<div id=\"main\">";
	if(strlen($pblmessage) > 0)  {
		echo "<div class=\"pblmessage\">".$pblmessage."</div>";
	}
	echo "<div class=\"pbldescription\">";
	echo "<h5>.htaccess rule generator</h5>";
	echo "<b>WARNING: this functionality should not be used if you have no idea what it means. Errors in your .htaccess file can cause your site not to function anymore. Be warned!</b><br/>";
	echo "Below are the .htaccess rules generated by Pivot-Blacklist. You can add these lines to your .htaccess file in order to completely block referers from spammers that match these rules. After adding the rules to your own .htaccess file, reset the appropriate file in order to prevent duplicate rules ending up in your .htaccess file.<br/><br/><h5>Set 1: rule based referer blocking</h5>";
	echo "<div class=\"pblform\">";
	echo "<div class=\"pbldescription\">Copy/Paste the proper lines as directed into your own .htaccess file. After that, reset the matched rules file.</div>";
	echo "<form action=\"index.php\" method=\"get\">";
	echo "<input type=\"hidden\" name=\"page\" value=\"resethtaccess\"/>";
	echo "<textarea style=\"border:1px #ccc solid;width:600px;height:150px;font-size:10px;\">";
	echo pbl_htaccess("rules");
	echo "</textarea>";
	echo "<br/><br/>";
	echo "<input type=\"submit\" value=\"reset matched rules file\">";
	echo "</form>";
	echo "</div>";
	echo "<h5>Set 2: IP blocks through .htaccess based on <a href=\"index.php?page=ipblocks\">IP Blocks</a></h5>";
	echo "<div class=\"pblform\">";
	echo "<form action=\"index.php\" method=\"get\">";
	echo "<div class=\"pbldescription\">It's best to add these rules at the beginning of your .htaccess file. Only add the rules that start with 'deny from'. After that, reset the ip blocks file.</div>";
	echo "<input type=\"hidden\" name=\"page\" value=\"resetipblocks\"/>";
	echo "<textarea style=\"border:1px #ccc solid;width:600px;height:150px;font-size:10px;\">";
	echo pbl_htaccess("ip");
	echo "</textarea>";
	echo "<br/><br/>";
	echo "<input type=\"submit\" value=\"reset ip blocks file\">";
	echo "</form>";
	echo "</div>";
	echo "</div>";
}

?>
