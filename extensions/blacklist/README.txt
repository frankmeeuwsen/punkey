PIVOT-BLACKLIST VERSION 0.9.3

READ THIS FIRST. I am a very helpful guy but I won't respond to
emails asking things that can be found right in this file. So there
:P


NUCLEUS USERS: Use Xiffy's version especially tailored for use with
Nucleus:

http://wakka.xiffy.nl/Blacklist?v=v5w

Can't figure out what to enable in the configuration panel? Read
WHAT_TO_ENABLE.txt to get some help.

This is the 0.9.3 release of Pivot-Blacklist, a comprehensive
anti-spam suite for Pivot weblogs. It's based on the centralized
MT-Blacklist maintained by Jay Allen who also wrote the original
MT-Blacklist code. Pivot-Blacklist only uses the list itself, no
code from MT-Blacklist was used.

*** IMPORTANT: Pivot-Blacklist was developed with Pivot 1.2b3 and
might not
    work with older versions! It DOES with newer versions of course.

FEATURES

- checking posted URL's against the realtime blacklist at www.surbl.org
- checking poster's IP's against the realtime blacklist at www.dsbl.org
- scanning of comments for spam based on the MT-Blacklist -
key/timestamp challenge to annoy spam bots (OSA) - MD5 challenge
with javascript (Hashcash) - User configurable textual question/answer
challenge - trackback spam protection - agressive full IP block on
people caught comment spamming - blocking of incoming referers that
are spammers - whitelist for sites that are trusted referers -
automatic daily updates of the MT-Blacklist file - scanning of
comments for spam based on personal unwanted expressions - redirection
of spammers to a user-defined URL if spam has been detected -
auto-generation of blocking rules for use in your .htaccess file -
logging of spam activity on your blog - online viewing of the spam
log - full web based administration



Please follow the installation instructions below. If you encounter
problems with anything but the (unsupported) hashcash feature please
open the following URL in your browser:

http://www.yoursite/extensions/blacklist/testconfig.php

This is a special diagnostics tool which catches a slew of frequently
made mistakes

Run this tool BEFORE asking for support please!



BASIC INSTALL

Use the following steps to enable basic (but powerful) spam blocking
powered by the MT blacklist datafile:

Since Version 1.2b3 Pivot features an extensions directory which
is located at the webroot of your site.

1) In Pivot, go to Administration->Configuration and enter a name
for the
   extensions dir. Enter 'extensions/' and save your settings.
2) Copy the blacklist/ directory to this extensions directory and
chmod it
   to 755.
3) chmod 777 blacklist/settings  ( drwxrwxrwx in your ftp program
)
   chmod 666 blacklist/settings/* ( all files, -rw-rw-rw- )
4) edit submit.php in your pivot dir and add the following line:
   require_once('../extensions/blacklist/blacklist.php'); right
   below the comment section on top. In several Pivot distributions
   this line is already present. If so you simply remove the // in
   front of it to enable it.

Done! You can now go to the actual control panel.

1) Surf to http://your.site.com/weblogdir/extensions/blacklist/ to
log in
   to the blacklist admin panel. You can use any login that has
   ADMIN on your blog.  Go to the blacklist editor and hit the
   Update Now! button to fetch your first MT blacklist datafile.
   Make sure no error is displayed. After this your list will be
   updated automatically every day.  You can now use in addition
   to the basic blacklist engine:

   - Agressive blocking - SURBL check - Open Proxies check

   Don't touch the other options before you've taken the appropriate
   steps mentioned under 'EXTRA'S' right here below.

EXTRA'S

These steps describe extra options apart from basic blacklist
scanning.

All snippets described in this text are in blacklist/snippets. You
have to copy them into your OWN snippets dir.

REFERER SPAM PROTECTION

  In the extras/ directory you will find the hardened_getref extension
  in ZIP format. Unzip it and install according to the instructions.
  It will automatically work with Pivot Blacklist.

OSA (OWEN'S SPAM ACTION)

Owen's Spam Action adds a hidden form field to your commentform
that needs to be submitted in order to be accepted as a valid
comment. Most spam bots don't parse the commentform which means
they'll fail when they encounter a blog with this option enabled.

1) For the OSA extension place the file called snippet_osa.php in
the
   snippets/ dir which resides in your extensions dir. If there's
   no directory named snippets create it there and copy snippet_osa.php
   into it.
2) Add the following tag to your _sub_commentform.html template:

   [[osa]]

   Add it somewhere within the <form> tag.

SPAM QUIZ (A.K.A. THE SILLY QUESTION HACK)

This feature adds a textfield to your commentform in which the user
has to answer a trivial question. You can change the question and
the corresponding answer in the Blacklist Settings panel. This is
a very powerful anti-spam measure, especially when you change your
question from time to time.

1) Add the following tag to your _sub_commentform.html template:

   [[spamquiz]]

   Add it somewhere within the <form> tag. Note: it adds a form
   textfield.

   Open the snippet itself in a text editor to learn how you can
   customize it's output.

HASHCASH:

   this extension comes with it's own README with instructions.
   You can find it, together with the necessary files, in the
   hashcash directory in the archive.

TRACKBACK USERS:

   read README.TRACKBACK for info about how to enable Pivot-Blacklist's
   trackback-spam protection

extra warning: The extensions dir should be located AT THE SAME
LEVEL as your pivot directory. That means _NOT_ IN the pivot directory
but a level higher.

It should work now. In case you have any problems or if you have
suggestions feel free to send me an email.

NOTES:

The SURBL feature does not work on some servers. This is due to
gethostbyname() not functioning properly on these environments. If
the blacklist configuration panel tells you it won't work you're
out of luck. Pivot Blacklist will refuse to enable it on these
environments because it would result in EVERYTHING being blocked
which is something you probably don't want. Unfortunately I don't
know how to fix this on the server. If someone does, drop me a line!

CREDITS:

- realtime open-proxy check and surbl domain check functions by
John Sinteur (http://weblog.sinteur.com) - original MT-Blacklist
datafile maintained by Jay Allen (http://www.jayallen.org) - includes
httpclient class by Simon Willison ( http://simon.incutio.com/ ) -
includes WP-HashCash code by Elliott Back (http://www.elliottback.com/)
- Thanks to Arnoud de Jong (www.verbaljam.nl) for the Nucleus idea
and for testing - Thanks to Xiffy (www.xiffy.nl) for making a Nucleus
version of this software - Thanks to everyone on the Pivot forum
for suggestions and testing - Special thanks to Bob den Otter
(http://www.pivotlog.net), author of Pivot for all his help

--
    Marco van Hylckama Vlieg marco@i-marco.nl http://www.i-marco.nl/weblog/

    Pivot-Blacklist homepage: http://www.i-marco.nl/pivot-blacklist/
