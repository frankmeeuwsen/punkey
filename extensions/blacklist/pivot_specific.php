<?
// All pivot-specific functions are here
//
// thanks for this Bob ;)
@ini_set("include_path", ".");

define('__WEBLOG_ROOT', dirname(dirname(dirname(realpath(__FILE__)))));
define('__EXT', '/extensions/blacklist');

$pivot_path = __WEBLOG_ROOT."/pivot/";

include_once($pivot_path."pv_core.php");
include_once($pivot_path."pvlib.php");

function pbl_login()  {
	global $Users;
	if(isset($_POST["user"]))  {
		foreach($Users as $currentUser)  {
			if (($currentUser["userlevel"] > 2) &&
			        ($currentUser["pass"] == md5($_POST["pass"]))  &&
			        ($currentUser["username"] == $_POST["user"]))  {
				setcookie("user", $currentUser["username"], (time() + (3600*24)));
				setcookie("pass", $currentUser["pass"], (time() + (3600*24)));
				return true;
			}
		}
	}
	reset($Users);
	for($i=0;$i<sizeof($Users);$i++)  {
		$currentUser = current($Users);
		if((key($Users) == $_POST["user"]) &&
		        ($currentUser["userlevel"] > 2) &&
		        ($currentUser["pass"] == md5($_POST["pass"])))  {
			setcookie("user", key($Users), (time() + (3600*24)));
			setcookie("pass", $currentUser["pass"], (time() + (3600*24)));
			return true;
		}
		next($Users);
	}
	return false;
}

function pbl_checklogin()  {

	global $Users;

	if(isset($_COOKIE))  {
		$thecookies = $_COOKIE;
	} else  {
		global $HTTP_COOKIE_VARS;
		$thecookies = $HTTP_COOKIE_VARS;
	}
	if((isset($thecookies["user"])) && (isset($thecookies["pass"])))  {
		$theUser = $Users[$thecookies["user"]];
		if($theUser["pass"] == $thecookies["pass"])  {
			return true;
		}
	}
	if(pbl_login())  {
		return true;
	} else  {
		header("location:index.php?page=login");
		exit;
	}
}
?>
