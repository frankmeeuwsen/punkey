<?
if(ini_get("include_path") == "")  {
	@ini_set("include_path", ".");
}
echo "Pivot Blacklist config test script.<br/><br/>";
echo "This scripts checks your installation and notifies you in case of problems with it, including instructions on how to fix it. It should detect all commonly made installation errors.<br/><br/>";
if(@ini_get("include_path") == "")  {
	echo "<b>ERROR:</b> Your PHP include_path is empty and I cannot set it myself. Therefore Pivot Blacklist can NOT run. Please ask your hosting provider to add . (current working dir) to the php include path.";
	exit;
} else  {
	echo "* PHP include path: ".ini_get("include_path")." is OK!<br/>";
}

if(!file_exists("../../pivot/entry.php"))	{
	echo "<b>ERROR:</b> Pivot Blacklist is at the wrong location. The directory blacklist/ with all it's contents must be in /root_of_your_weblog/extensions/<br />";
	exit;
} else	{
	echo "* Blacklist directory location OK!<br/>";
}
/* This is for future use
 
if(!$fpTest = @fopen("./settings/ctest.pbl", "a"))	{
	  echo "<b>ERROR:</b> cannot write to files in blacklist/settings<br/>";
	  echo "The blacklist/settings/ directory must be made world writable. (mode 755)";
	exit;
	}
else	{
	echo "* Directory blacklist/settings is OK!<br/>";
	@unlink("./settings/ctest.pbl");
	}
*/
if(!$fpTest = @fopen("./settings/blacklist.txt", "a"))	{
	echo "<b>ERROR:</b> cannot open existing files in blacklist/settings for writing<br/>";
	echo "Please make the files in blacklist/settings/ world writable! (mode 666)";
	exit;
} else	{
	fclose($fpTest);
	echo "* Files in blacklist/settings are OK!<br/>";
}
include_once("blacklist_lib.php");
/*
if(pbl_updateblacklist(true))	{
	echo "* Downloaded fresh blacklist: OK!<br/>";
	}
else	{
	echo "<b>ERROR:</b> cannot fetch fresh blacklist file. reason unknown.<br/>";
	}
*/
$bSubmitChanged = false;
if(!file_exists(__WEBLOG_ROOT."/pivot/submit.php"))  {
	echo "<b>ERROR:</b> ".__WEBLOG_ROOT."/pivot/submit.php doesn't seem to exist! If you changed it's name, please change it back. If it's not present please check your Pivot setup before proceeding.<br/>";
	exit;
} else  {
	$fpSubmitFile = fopen(__WEBLOG_ROOT."/pivot/submit.php", "r");
	while(!feof($fpSubmitFile))  {
		$sBuffer = fgets($fpSubmitFile, 4096);
		if((strstr($sBuffer, "blacklist.php")) && (!strstr($sBuffer, "//")))  {
			$bSubmitChanged = true;
			fclose($fpSubmitFile);
			break;
		}
	}
}
if($bSubmitChanged)	{
	echo "* ".__WEBLOG_ROOT."/pivot/submit.php is present and has been edited: OK!<br/>";
} else  {
	echo "<b>ERROR:</b> You didn't edit ".__WEBLOG_ROOT."/pivot/submit.php and added the line as described in README.txt. If the line is already there, you didn't remove the // in front of it.<br/>";
}

echo "<br/><br/><b>Congratulations!</b>. You've correctly installed Pivot Blacklist for <b>basic operation</b>. You may now proceed by logging in to the control panel by clicking <a href=\"./\">HERE</a> or test your kit for one or more of the advanced options by clicking on one below.";
echo "<br/><br/>";
echo "<a href=\"testconfig.php?mode=osa\">check for OSA</a><br/>";
echo "<a href=\"testconfig.php?mode=spamquiz\">check for Spamquiz</a><br/>";
echo "<a href=\"testconfig.php?mode=hashcash\">check for HashCash</a><br/>";
echo "<br/>";


switch($_GET["mode"])  {
case "osa":
		echo "<b>Checking configuration for OSA</b><br/><br/>";
	if(!is_dir("../snippets"))	{
		echo "You don't have a snippets directory in extensions/ !!!";
		exit;
	} else  {
		echo "Snippets dir found. OK!";
		echo "<br/>";
	}
	if(!file_exists("../snippets/snippet_osa.php"))  {
		echo "<b>ERROR:</b> snippet_osa.php NOT found in extensions/snippets/";
		exit;
	} else  {
		echo "snippet_osa.php found in your snippets dir: OK!<br />";
	}
	$fpOSA = fopen(__WEBLOG_ROOT."/pivot/templates/_sub_commentform.html", "r");
	while(!feof($fpOSA))	{
		$sBuffer = fgets($fpOSA, 4096);
		if(strstr($sBuffer, "<form"))  {
			$bFormStart = true;
		}
		if(strstr($sBuffer, "</form>"))  {
			$bFormEnd = true;
		}
		if(strstr($sBuffer, "[[osa"))	{
			if($bFormStart && (!$bFormEnd))  {
				echo "[[osa]] tag found in _sub_commentform.html at the right place: OK!<br/>";
				echo "<br/>It's safe to enable OSA.<br/>";
				exit;
			} else  {
				echo "<b>ERROR:</b> [[osa]] tag found but it's not at the right place in _sub_commentform.html. It must be AFTER &lt;form&gt; and BEFORE &lt;/form&gt;";
				exit;
			}
		}
	}
	echo "<b>ERROR:</b> no [[osa]] tag found in _sub_commentform.html. Enabling OSA while this hasn't been fixed will completely BLOCK commenting!";
	break;
case "hashcash":
	echo "<b>Checking configuration for HashCash</b><br/><br/>";
	$fpSQ = fopen(__WEBLOG_ROOT."/pivot/templates/entrypage_template.html", "r") or die ("entrypage_template.html doesn't seem to exist. This script is kinda stupid and can only check that specific file. This doesn't mean you installed hashcash the wrong way, it just means this script can't verify your installation.");
	while(!feof($fpSQ))	{
		$sBuffer = fgets($fpSQ, 4096);
		if(strstr($sBuffer, "<head"))  {
			$bHeadStart = true;
		}
		if(strstr($sBuffer, "</head>"))  {
			$bHeadEnd = true;
		}
		if(strstr($sBuffer, "[[hashcash_head"))	{
			if($bHeadStart && (!$bHeadEnd))  {
				$bHcHead = true;
				echo "[[hashcash_head]] tag found in entrypage_template.html at the right place: OK!<br/>";
			} else  {
				echo "<b>ERROR:</b> [[hashcash_head]] tag found but it's not at the right place in entrypage_template.html. It must be AFTER &lt;head&gt; and BEFORE &lt;/head&gt;";
				exit;
			}
		}
	}
	if(!$bHcHead)  {
		echo "<b>ERROR:</b> no [[hashcash_head]] tag found in entrypage_template.html. Enabling HashCash while this hasn't been fixed will completely BLOCK commenting!";
		exit;
	}
	$bHcHead = false;
	$bHeadEnd = false;
	$bHeadStart = false;
	$fpSQ = fopen(__WEBLOG_ROOT."/pivot/templates/_sub_commentform.html", "r");
	while(!feof($fpSQ))	{
		$sBuffer = fgets($fpSQ, 4096);
		if(strstr($sBuffer, "<form"))  {
			$bHeadStart = true;
		}
		if(strstr($sBuffer, "</form>"))  {
			$bHeadEnd = true;
		}
		if(strstr($sBuffer, "[[hashcash_onsubmit"))	{
			if($bHeadStart && (!$bHeadEnd))  {
				$bHcHead = true;
				echo "[[hashcash_onsubmit]] tag found in _sub_commentform.html, <b>probably </b> at the right place. Make sure it's WITHIN the <form> opening tag. Example: &lt;form [[hashcash_onsubmit]] method=\"post\" ... OK!<br/>";
			} else  {
				echo "<b>ERROR:</b> [[hashcash_onsubmit]] tag found but it's not at the right place in _sub_commentform.html. It must be WITHIN the <form> opening tag. Example: &lt;form [[hashcash_onsubmit]] method=\"post\" ...";
				exit;
			}
		}
	}
	if(!$bHcHead)  {
		echo "<b>ERROR:</b> no [[hashcash_onsubmit]] tag found in _sub_commentform.html. Enabling HashCash while this hasn't been fixed will completely BLOCK commenting!";
		exit;
	}

	$bHcHead = false;
	$bHeadStart = false;
	$bHeadEnd = false;
	$fpSQ = fopen(__WEBLOG_ROOT."/pivot/templates/_sub_commentform.html", "r");
	while(!feof($fpSQ))	{
		$sBuffer = fgets($fpSQ, 4096);
		if(strstr($sBuffer, "<form"))  {
			$bHeadStart = true;
		}
		if(strstr($sBuffer, "</form>"))  {
			$bHeadEnd = true;
		}
		if(strstr($sBuffer, "[[hashcash_hiddenfield"))	{
			if($bHeadStart && (!$bHeadEnd))  {
				$bHcHead = true;
				echo "[[hashcash_hiddenfield]] tag found in _sub_commentform.html at the right place: OK!<br/>";
			} else  {
				echo "<b>ERROR:</b> [[hashcash_hiddenfield]] tag found but it's not at the right place in _sub_commentform.html. It must be AFTER &lt;form&gt; and BEFORE &lt;/form&gt;.";
				exit;
			}
		}
	}
	if(!$bHcHead)  {
		echo "<b>ERROR:</b> no [[hashcash_hiddenfield]] tag found in _sub_commentform.html. Enabling HashCash while this hasn't been fixed will completely BLOCK commenting!";
		exit;
	}

	echo "<br/>Success! As far as this script can tell you've corrrectly installed hashcash.You can now safely uncomment the hashcash code in blacklist_lib.php.";
	break;


case "spamquiz":
	echo "<b>Checking configuration for Spamquiz</b><br/><br/>";
	if(!is_dir("../snippets"))	{
		echo "You don't have a snippets directory in extensions/ !!!";
		exit;
	} else  {
		echo "Snippets dir found. OK!";
		echo "<br/>";
	}
	if(!file_exists("../snippets/snippet_spamquiz.php"))  {
		echo "<b>ERROR:</b> snippet_spamquiz.php NOT found in extensions/snippets/";
		exit;
	} else  {
		echo "snippet_spamquiz.php found in extensions/snippets/: OK!<br/>";
	}
	$fpSQ = fopen(__WEBLOG_ROOT."/pivot/templates/_sub_commentform.html", "r");
	while(!feof($fpSQ))	{
		$sBuffer = fgets($fpSQ, 4096);
		if(strstr($sBuffer, "<form"))  {
			$bFormStart = true;
		}
		if(strstr($sBuffer, "</form>"))  {
			$bFormEnd = true;
		}
		if(strstr($sBuffer, "[[spamquiz"))	{
			if($bFormStart && (!$bFormEnd))  {
				echo "[[spamquiz]] tag found in _sub_commentform.html at the right place: OK!<br/>";
				echo "<br/>You can safely enable Spamquiz!";
				exit;
			} else  {
				echo "<b>ERROR:</b> [[spamquiz]] tag found but it's not at the right place in _sub_commentform.html. It must be AFTER &lt;form&gt; and BEFORE &lt;/form&gt;";
				exit;
			}
		}
	}
	echo "<b>ERROR:</b> no [[spamquiz]] tag found in _sub_commentform.html. Enabling Spamquiz while this hasn't been fixed will completely BLOCK commenting!";
	break;
}
?>

