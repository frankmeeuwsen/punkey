<?php

/**
 * ==================================================================
 * Snippet-title:          Hardened BBClone v0.5 (15th November 2005)
 * Creator:                Hans Fredrik Nordhaug, Marco van Hylckema
 *                         Vlieg, Bob den Otter
 * E-mail:                 hans@nordhaug.priv.no, marco@i-marco.nl,
 *                         bob@twokings.nl
 * Creation date:          10th October 2005
 * License:                GPL
 * ==================================================================
 *
 * This snippet protects your BBClone stats from referer spam.
 * Read readme.txt for more information.
 *
 *
 */

$bbclone_debug = false;

$pivot_path = dirname(dirname(dirname(__FILE__)))."/pivot/";
$refkeydir = $pivot_path."db/refkeys";


hr_makedir($refkeydir);


define ('__SECRET__', check_salt());
define ('__MYDOMAIN__', $_SERVER['HTTP_HOST']);


// Compensate for &amp; in url:
foreach ($_GET as $key => $val) {
	$_GET[ str_replace("amp;", "", $key) ] = $val;
}

// an easy function to recursively create chmodded directory's
function hr_makedir($name) {

	// if it exists, just return.
	if (file_exists($name)) {
		return;
	}

	// if more than one level, try parent first..
	if (dirname($name) != ".") {
		hr_makedir(dirname($name));
	}

	$oldumask = umask(0);
	@mkdir ($name, 0777);
	@chmod ($name, 0777);
	umask($oldumask);

}


/**
 * Check if the salt is defined
 */
function check_salt() {
	global $refkeydir;

	if(file_exists($refkeydir."/salt.php")) {
		include_once($refkeydir."/salt.php");
	} else {
		$salt = make_getrefkey();
		$fp = @fopen($refkeydir."/salt.php", "w");
		@fwrite($fp, sprintf('<?php $salt="%s"; ?>', $salt));
	}

}


/**
 * Make a getrefkey
 */
function make_getrefkey() {

	$sid="";
	for ($i = 1 ; $i <= 20; $i++) {
		$rchar = mt_rand(1,30);
		if($rchar <= 10) {
			$sid .= chr(mt_rand(65,90));
		}elseif($rchar <= 20) {
			$sid .= mt_rand(0,9);
		}else{
			$sid .= chr(mt_rand(97,122));
		}
	}
	return $sid;
}


?>
