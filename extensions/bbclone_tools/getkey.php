<?php

/**
 * ==================================================================
 * Snippet-title:          Hardened BBClone v0.5 (15th November 2005)
 * Creator:                Hans Fredrik Nordhaug, Marco van Hylckema
 *                         Vlieg, Bob den Otter
 * E-mail:                 hans@nordhaug.priv.no, marco@i-marco.nl,
 *                         bob@twokings.nl
 * Creation date:          10th October 2005
 * License:                GPL
 * ==================================================================
 *
 * This snippet protects your BBClone stats from referer spam.
 * Read readme.txt for more information.
 *
 *
 */

include_once("hr_conf.php");
include_once($pivot_path."pv_core.php");

$sKeyName = MD5(__SECRET__.time());
if(strstr($_SERVER["HTTP_REFERER"], __MYDOMAIN__) != false)  {
	if (!@touch("$refkeydir/$sKeyName")) {
		debug("bbclone tools: directory $refkeydir isn't writable - can't count");
		echo "function bbcloneCount(title){}\n";
		return;
	}
}
$nNow = time();

echo "\nvar refkey='$sKeyName';\n";

// Getting the time offset between the web and file server (if there is any)
$offset = timediffwebfile($bbclone_debug);

// delete keys older than 10 seconds
$handle=opendir("$refkeydir");
while (false!==($file = readdir($handle))) {
	$filepath = $refkeydir."/".$file;
	if (!is_dir($filepath) && ($file != "index.html") && ($file != "salt.php")) {
		$Diff = ($nNow - filectime($filepath));
		if (($Diff > (10+$offset)) && ($bbclone_debug!=true)) {
			unlink($filepath);
		}
	}
}
closedir($handle);


echo <<< EOM

function bbcloneCount(title)  {
	var ref=escape("NO");
	var uri=escape("NO");
	var ua=escape(navigator.userAgent);
	var rem=escape("{$_SERVER["REMOTE_ADDR"]}");

	var bbcloneimg = '{$Paths["extensions_url"]}bbclone_tools/count.php?title='+escape(title)+'&amp;refkey='+refkey+'&amp;rem='+rem+'&amp;ref='+escape(document.referrer)+'&amp;uri='+escape(document.URL)+'&amp;ua='+ua;

	document.write('<img src="' + bbcloneimg +'" />');

EOM;

if ($bbclone_debug) {
	echo "\nprompt('Your bbclone link:', bbcloneimg);\n";
}

echo "}";






?>
