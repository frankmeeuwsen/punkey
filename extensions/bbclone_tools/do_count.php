<?php
/*

==================================================================
Snippet-title:          Hardened BBClone v0.1 (10th October 2005)
Creator:                Hans Fredrik Nordhaug
E-mail:                 hans@nordhaug.priv.no
Creation date:          10th October 2005
License:                GPL
==================================================================

This snippet protects your BBClone stats from referer spam.
Read readme.txt for more information.

*/

// don't access directly..
if(!defined('INPIVOT')){ exit('NO!'); }

// Remvoing anchors from page URI
list($uri) = explode("#", $_GET['uri']);
// Setting variables so bbclone can count correctly
$title = stripslashes(urldecode($_GET["title"]));
// Removing "pivot/" from the (end of) paths to make it work for frontpages and archives...
$url = substr($Paths['pivot_url'],0,-6);
$path = substr($Paths['pivot_path'],0,-6);
$host = $Paths['host'];
$script = str_replace($host,"",$uri);
if (($host == $uri) || ($host."/" == $uri)) {
    // For this special case BBclone needs the filename to create the correct
    // URL in the stats. (Using "index.php" as index page since BBclone strips
    // that off anyway.)
    $file = $path."index.php";
    $script .= "index.php";
} else {
    $file = str_replace($host.$url,$path,$uri);
}
$_SERVER["HTTP_REFERER"] = $_GET["ref"];
$_SERVER["REMOTE_ADDR"] = $_GET["rem"];
$_SERVER["SCRIPT_FILENAME"] = $file;
$_SERVER["PATH_TRANSLATED"] = $file;
$_SERVER["PATH_INFO"] = "";
$_SERVER["SCRIPT_NAME"] = $script;
$_SERVER["PHP_SELF"] = $script;
$_SERVER["HTTP_USER_AGENT"] = $_GET["ua"];
define( "_BBC_PAGE_NAME", $title );
define( "_BBCLONE_DIR",  $Paths['bbclone_path']);
define( "COUNTER",       _BBCLONE_DIR."mark_page.php");
if( is_readable( COUNTER )) { include_once( COUNTER ); }

?>
