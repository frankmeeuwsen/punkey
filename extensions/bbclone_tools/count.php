<?php
/**
 * ==================================================================
 * Snippet-title:          Hardened BBClone v0.5 (15th November 2005)
 * Creator:                Hans Fredrik Nordhaug, Marco van Hylckema
 *                         Vlieg, Bob den Otter
 * E-mail:                 hans@nordhaug.priv.no, marco@i-marco.nl,
 *                         bob@twokings.nl
 * Creation date:          10th October 2005
 * License:                GPL
 * ==================================================================
 *
 * This snippet protects your BBClone stats from referer spam.
 * Read readme.txt for more information.
 *
 *
 */



include_once("./hr_conf.php");

include_once($pivot_path."pvlib.php");
include_once($pivot_path."pv_core.php");


if ($bbclone_debug==true) {
	echo "Debug on..<br />\n";
	error_reporting(E_ALL);
}

if ( ($_GET["refkey"]!="") && file_exists("$refkeydir/".$_GET["refkey"])) {

	if ($bbclone_debug==true) { echo "Refkey found..<br />"; }

	// Getting the time offset between the web and file server (if there is any)
	$offset = timediffwebfile($bbclone_debug);

	if ((time() - filectime("$refkeydir/".$_GET["refkey"])) < (1000+$offset)) {

		include("do_count.php");
		if ($bbclone_debug!=true) {
			header("content-type:image/gif");
			readfile("pixel.gif");
		} else {
			echo "Counted normally";
		}
		die();

	} else if ($bbclone_debug==true) {
		echo "too old!";
	}

	if ($bbclone_debug!=true) {
		unlink("$refkeydir/".$_GET["refkey"]);
	}
}

// Skipped count..
if ($bbclone_debug!=true) {
	header("content-type:image/gif");
	readfile("pixel.gif");
} else {
	echo "Skipped count";
}
?>
