<?php

/**
 * Language strings for use with the bbclone extensions.
 */

$lang['bbclone'] = array(

	'total_visits'   => 'Total visits',
	'unique_visits'  => 'Unique visitors',
	'this_month'  => 'This month',
	'this_week'  => 'This week',
	'today'  => 'Today',

);

?>