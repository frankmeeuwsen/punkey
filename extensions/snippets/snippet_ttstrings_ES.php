<?

/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 March 2005
License:                GPL
==================================================================

Traducci�n al Espa�ol por Erick Tovar (etovar@gmail.com)

*/

define(__TT_TAGS_IN_POSTING, "Tags utilizados en esta entrada");
define(__TT_CLICK_FOR_UNIVERSE, "Click para ir al p�gina del universo de tags. Click en la im�gen <img src=\"/weblog/pivot/pics/ticon.gif\"> para ir a la p�gina de tags de Technorati.");
define(__TT_LOCALCOSMOS_DESCRIPTION, "Esta es la constelaci�n local  de tags para este weblog. Entre m�s grande el tag, existen m�s entradas relacionadas. Dar click en cualquier tag para ver sus detalles. El orden de los tags es aleatorio y cambiar� cada que refresque la p�gina.<br/><br/>");
define(__TT_TAGOVERVIEW_HEADER, "Vista general del Tag: ");
define(__TT_ENTRIES_WITH_TAG, "Entradas en este sitio relacionadas a ");
define(__TT_RELATED_TAGS, "Tags relacionados");
define(__TT_LATEST_ON, "Ultimos enlaces desde");
define(__TT_LINKS_TO_TAG_UNIVERSES, "Enlaces a otros universos relacionados");
define(__TT_NOTHING_ON, "No hay nada en");
define(__TT_ON, "en");
define(__TT_FOUND_ON, "encontrado en");
define(__TT_OTHER_POSTS_WITH_TAG, "Acerca de otras entradas");

?>
