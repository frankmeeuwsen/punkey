<?php


/**
 * An example snippet to show how you can make dynamic extension snippets.
 * meaning: It gets executed every time the page is accessed by a visitor,
 * instead of only when the page was created.
 *
 * If you make snippets like these, I propose you name the 'main' part like
 * the snippet, only with 'main_' prepended. Doing that will prevent name
 * collisions with other extension snippets.
 *
 * @param void
 * @return string output
 * @see main_dumdyn
 *
 */
function snippet_dumdyn() {
	global $Cfg, $pivot_path;
	
	if (defined('LIVEPAGE')){
		
		// for pages like 'live entries' and 'dynamic archives'..
		return main_dumdyn(); 
		
	} else{
		
		// For genreated pages like the frontpage and archives.
		$file = __FILE__;
		
		$output = "<"."?php\n";
		$output .= "	include_once('$file');\n";
		$output .= "	echo main_dumdyn();\n";
		$output .= "?".">\n";
		
	}

	// return $output to the parser..
	return $output;

}



/**
 * This is where the actual work takes place. In this example it merely
 * generates a random number to show exactly how dynamic it is.
 *
 * @param void
 * @return string output
 * @see snippet_dumdyn
 *
 */
function main_dumdyn() {

	$randval = rand(1,10);
	$output = 'Your lucky number is '. $randval . '<br>';
	return $output;

}

?> 