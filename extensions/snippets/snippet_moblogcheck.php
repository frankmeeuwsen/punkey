<?php

function snippet_moblogcheck() {
	global $Paths;
	
	if (defined('LIVEPAGE')) {
		
		$output = "<!-- moblogcheck -->";
		include_once( dirname(dirname(__FILE__))."/moblog/fetchmail.php");
		
	} else {
		
		$output = "<!-- moblogcheck -->";
		$output .= sprintf("<"."?php include_once('%s/moblog/fetchmail.php'); ?".">",
					dirname(dirname(__FILE__))
					);
	}
	
	
	return $output;
	
}


?>