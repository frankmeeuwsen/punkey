<?
/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 March 2005
License:                GPL
==================================================================

USAGE: Add this tag below your [[body]] tag in entrypage_template.html:

[[ttaglist]]

*/

@ini_set("include_path", ".");
include_once("snippet_tt.php");
include_once("snippet_ttstrings.php");
include_once("snippet_tagpage.php");

function make_related_tags($p_sTag, $p_aAllTags)	{
	if(!file_exists(snippet_pivot_path()."db/tagdata/$p_sTag".".rel"))  {
		$aRelTags = array();
		foreach($p_aAllTags as $sTheTag)	{
			if($sTheTag != $p_sTag)	{
				array_push($aRelTags, $sTheTag);
			}
		}
		if(sizeof($p_aAllTags) > 1)	{
			$fpRelFile = fopen(snippet_pivot_path()."db/tagdata/".str_replace(" ","+",$p_sTag).".rel", "w");
			flock($fpRelFile, LOCK_EX);
			fwrite($fpRelFile, implode(",",$aRelTags));
			flock($fpRelFile, LOCK_UN);
		}
	}
	else	{
		$fpRelFile = fopen(snippet_pivot_path()."db/tagdata/$p_sTag".".rel", "r");
		$sRelString = "";
		while(!feof($fpRelFile))	{
			$sRelString .= fread($fpRelFile, 4096);
		}
		fclose($fpRelFile);
		$aRelArray = explode(",", $sRelString);
		$bMustWrite = false;
		foreach($p_aAllTags as $sThisOne)	{
			if((!in_array($sThisOne, $aRelArray)) && (!in_array($p_sTag, $p_aAllTags)))	{
				array_push($aRelArray, $sThisOne);
				$bMustWrite = true;
			}
		}
		if($bMustWrite)		{
			$fpRelFile = fopen(snippet_pivot_path()."db/tagdata/$p_sTag".".rel", "w");
			flock($fpRelFile, LOCK_EX);
			fwrite($fpRelFile, implode(",",$aRelArray));
			flock($fpRelFile, LOCK_UN);
			fclose($fpRelFile);
		}
	}
}

function snippet_ttaglist()	{
	global $db;
	global $Paths;
	$sFullEntry = $db->entry["introduction"].$db->entry["body"];
	$aRawTags = explode("[[",$sFullEntry);
	$aTagsList = array();
	foreach($aRawTags as $sTag)	{
		if(substr($sTag, 0, 3) == "tt:")	{
			$aRealTag = explode("]]", $sTag);
			if(!in_Array(strtolower(substr($aRealTag[0], 3)), $aTagsList))  {
				$sTheRTag = substr($aRealTag[0], 3);
				
				if(!strstr($sTheRTag, ":"))	{
	
					array_push($aTagsList, strtolower($sTheRTag));
				}
				else	{
					$aTheRTag = explode(":",$sTheRTag);
					array_push($aTagsList, strtolower($aTheRTag[0]));
				}
			}
		}
	}
	if(sizeof($aTagsList) > 0)	{
		$sHTML = __TT_TAGS_IN_POSTING."<small>".__TT_CLICK_FOR_UNIVERSE."</small><br/><br/>";
		$bDummyCount = 0;
		foreach($aTagsList as $sTag)	{
			make_related_tags($sTag, $aTagsList);
			$sHTML .= "<a rel=\"tag\" href=\"".__TAGPAGE_URL.str_replace(" ","+",$sTag)."\" title=\"tag: $sTag\">".str_replace("+", " ", $sTag)."</a>&nbsp;<a href=\"http://www.technorati.com/tag/".str_replace(" ","+",$sTag)."\" rel=\"tag\" title=\"Technorati: $sTag\"><img src=\"".$Paths["pivot_url"]."pics/ticon.gif\" alt=\"$sTag on Technorati\" style=\"border:none;\"/></a>";
			$bDummyCount++;
			if(sizeof($aTagsList) > $bDummyCount)	{
				$sHTML .= ", ";
			}
			
		}
		$sHTML .= "<br/>";
		reset($aTagsList);
		foreach($aTagsList as $sRelated)	{
			$sTheRelatedLinks = get_entries_with_tag($sRelated, $db->entry["code"]);
			if(!strlen($sTheRelatedLinks) == 0)	{
				$sHTML .= "<br/><h2>";
				$sHTML .= __TT_OTHER_POSTS_WITH_TAG." '".$sRelated."'</h2><br/>";
				$sHTML .=  $sTheRelatedLinks;
			}
		}
		$sHTML .= "<br/><br/><br/><br/>";
		return $sHTML;
	}
	else	{
		return "";
	}
}
?>
