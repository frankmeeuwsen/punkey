<?


/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 March 2005
License:                GPL
==================================================================

Dutch translation by Bakkel (http:/weblog.bakkel.com/)

*/

define(__TT_TAGS_IN_POSTING, "<h3><img src=http://weblog.bakkel.com/images/icons/doc.png> Tags gebruikt in dit artikel:</h3>");
define(__TT_CLICK_FOR_UNIVERSE, "klik hier voor de tag universe pagina");
define(__TT_LOCALCOSMOS_DESCRIPTION, "Dit is de lokale tag cosmos voor dit weblog. Hoe groter de tag, des te meer artikelen verwant zijn. Klik op de tag voor meer info..<br/><br/>");
define(__TT_TAGOVERVIEW_HEADER, "Tag overzicht van: ");
define(__TT_ENTRIES_WITH_TAG, "Artikelen op dit log voor");
define(__TT_RELATED_TAGS, "gerelateerde tags");
define(__TT_LATEST_ON, "Laatste links van");
define(__TT_LINKS_TO_TAG_UNIVERSES, "Links naar tag universums");
define(__TT_NOTHING_ON, "Niets voor");
define(__TT_ON, "voor");
define(__TT_FOUND_ON, "gevonden op");
define(__TT_OTHER_POSTS_WITH_TAG, "Andere posts over");
?>
