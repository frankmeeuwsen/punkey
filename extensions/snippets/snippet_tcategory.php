<?

/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 Marco 2005
License:                GPL
==================================================================

USAGE: See README.txt

This snippet replaces Pivot's [[category]] tag. If you want tagged
categories in you entries replace [[category]] with [[tcategory]] in the
appropriate template.

*/


@ini_set("include_path", ".");
include_once("snippet_tt.php");

function snippet_tcategory($filter="") {
        global $db, $Weblogs, $Current_weblog, $Current_subweblog;
        $output=$db->entry["category"];

        if ( ($filter != "") && (isset($Weblogs[$Current_weblog]['sub_weblog'][$Current_subweblog])) ) {
                $output = array_intersect ( $Weblogs[$Current_weblog]['sub_weblog'][$Current_subweblog]['categories'], $output);
        }

        if (is_array($output)) {
		$count = 0;
		$sOut = "";
		foreach($output as $category)	{
			$sOut .= snippet_tt($category);
			$count++;
			if($count < sizeof($output))	{
				$sOut .= ", ";
				}
			}
		return $sOut;
        } else {
                return "";
        }
}
?>
