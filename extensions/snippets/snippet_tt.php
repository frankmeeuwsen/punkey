<?
/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 March 2005
License:                GPL
==================================================================

USAGE: See README.txt

*/

define(__TAGPAGE_URL, "http://www.i-marco.nl/weblog/tags/");

function getFileContents($p_sFile)	{
	$sFileString = "";
	$fpFile = fopen($p_sFile, "r");
	while(!feof($fpFile))	{
		$sBuffer = fread($fpFile, 4096);
		$sFileString .= $sBuffer;
	}
	fclose($fpFile);
	$aFileArr = explode(",", $sFileString);
	return $aFileArr;
}

function snippet_tt($p_sTagName, $p_sExternalLink)	{
	$p_sTagName = str_replace(" ", "+", $p_sTagName);
	global $pivot_path;
	global $db;
	$sFileName = strtolower($p_sTagName);
	if(!is_dir(snippet_pivot_path()."db/tagdata"))	{
		return "<b>ERROR: You must create ".snippet_pivot_path()."db/tagdata and set the permissions to world writable!!! Bailing out.";
	}
	$sFileName = strtolower($p_sTagName);
	if(file_exists(snippet_pivot_path()."db/tagdata/".$sFileName.".tag"))	{
		$aFileArr = getFileContents(snippet_pivot_path()."db/tagdata/".$sFileName.".tag");
		if(!in_array($db->entry["code"], $aFileArr))	{
			array_push($aFileArr, $db->entry["code"]);
			if(sizeof($aFileArr) == 1)  {
				$sNewFileString = $aFileArr[0];
			}
			else	{
				$sNewFileString = implode(",",$aFileArr);
			}
			$fpNewFile = fopen(snippet_pivot_path()."db/tagdata/".$sFileName.".tag", "w");
			flock($fpNewFile, LOCK_EX);
			fwrite($fpNewFile, $sNewFileString);
			flock($fpNewFile, LOCK_UN);
			fclose($fpNewFile);
		}
	}
	else  {
		$fpNewFile = fopen(snippet_pivot_path()."db/tagdata/".$sFileName.".tag", "w");
		flock($fpNewFile, LOCK_EX);
		fwrite($fpNewFile, $db->entry["code"]);
		flock($fpNewFile, LOCK_UN);
		fclose($fpNewFile);
	}
	if(strlen($p_sExternalLink) > 0)	{
		$sTagString = "<a rel=\"tag\" class=\"taglink\" href=\"http://".$p_sExternalLink."\" title=\"tagged external link: $p_sTagName\">".str_replace("+", " ",$p_sTagName)."</a>";
	}
	else	{
		$sTagString = "<a rel=\"tag\" class=\"taglinkext\" href=\"".__TAGPAGE_URL.$p_sTagName."\" title=\"tag: $p_sTagName\">".str_replace("+", " ",$p_sTagName)."</a>";
	}
	return $sTagString;
	
}
?>
