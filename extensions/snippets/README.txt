Hardened Referrers Version 0.4

By Marco van Hylckama Vlieg

marco@i-marco.nl

---------------------------------------------------------------------

This is a pretty damn hard to spam replacement for Pivot's [[get_referrer]]
template tag and underlying routines.

This code is still somewhat experimental but first signs are good. It all
seems to work nicely.

What it does:

It keeps nasty spam referrers from appearing in your 'last referrers list' 
on your homepage. If Pivot Blacklist is installed as well it will use
Pivot-Blacklist to perform additional scanning on the incoming referrer.

What it does NOT do:

It doesn't protect any other stats package such as bbclone from referrer spam
and it does not prevent spammers draining your bandwidth by doing tons of
spam attempts on your site. For this use Pivot Blacklist's agressive 
.htaccess rule based blocking.

Instructions:

1: Place everything in /your_root/extensions/snippets/

2: Edit hr_conf.php and change these defines:

   define("__MYDOMAIN__", "i-marco.nl");                                           define("__SECRET__", "marcokicksass");
 
   Change the i-marco.nl into your-domain.com whatever your domain is.
   Change the 'marcokicksass' into anything you want.

3: Create a directory named 'refkeys' inside your extensions/snippets dir
   and make it world writable (chmod 777)

4: Put    [[hardened_refget]] 

   somewhere in your <body> in your templates. It MUST be within the <body>
   tag. A nice place would be right before the </body> line.

5: Remove ALL occurrences of Pivot's original [[get_referer]] tag from
   your templates or you'll still get spammed!

6: Edit pivot/archive.php and pivot/entry.php and remove or comment out
   the lines that include getref.inc.php. THIS IS VERY IMPORTANT or 
   the extension will have zero effect!

Feedback welcome as long as you're SURE you followed these instructions!

Marco van Hylckama Vlieg (marco@i-marco.nl)
