<?php


/**
 * An example snippet to show how to write a simple Snippet Extension.
 * It gets executed when the page is generated.
 *
 * @param void
 * @return string output
 *
 */
function snippet_helloworld() {
	global $Cfg, $pivot_path;
	
	$output = "<strong>Hello World</strong><br />\n";
	$output .= "This page was generated on ". date("Y-m-d H:i:s") ."\n";

	// return $output to the parser..
	return $output;

}




?> 