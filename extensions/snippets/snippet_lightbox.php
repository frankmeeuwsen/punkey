<?php

/**
 * Lightbox is javascript image viewer, made by Lokesh Dhakar - 
 * lokesh [@] huddletogether.com
 * This pivot extension allows easy insertion of 'lightboxed' popups in your 
 * Pivot weblog. 
 * 
 * For instructions, see this page on the wiki: 
 * http://pivotlog.net/wiki/doku.php?id=lightbox_extensions
 * 
 * @author Lokesh Dakar (original script) and Bob den Otter (Pivot Extension)
 * @version 0.92 
 * @see http://www.huddletogether.com/projects/lightbox/
 * @see http://pivotlog.net/wiki/doku.php?id=lightbox_extensions
 *
 */



/**
 * Insert a 'lightboxed' image popup.
 * 
 * We're using a trick to insert the needed scripts. If you call it using
 * [[lightbox:head]] it will insert the needed files for you.
 *
 * @param string $filename
 * @param string $thumbname
 * @param string $alt
 * @param string $align
 * @param string $border
 * @return string HTML
 * 
 */
function snippet_lightbox($filename, $thumbname,  $alt="", $align="center", $border="") {
	global	$Cfg, $Paths;

	/**
	 * Special case: if $filename is 'head', we return the code that goes into
	 * the <head> section of the page.
	 */
	if ($filename=="head") {
		return snippet_lightbox_head();
	}
	
	if (is_numeric($align)) {
		// the border and align properties were swapped, so we need
		// to compensate for the wrong ones.
		$i = $border;
		$border = $align;
		$align = $i;
	}

	if ( ($border=="") || (!is_numeric($border)) ) { $border=1; }

	$border= set_target("style=\"border: ".$border."px solid;\"", "border=\"$border\"");

	// Fix filename, if necessary
	$org_filename = $filename;

	if (!file_exists("../".$filename)) {
		$filename = "../".$Cfg['upload_path'].$filename;
	} else {
		$filename = "../".$filename;
	}

	// Fix Thumbname, perhaps use a thumbname, instead of textual link
	$org_thumbname = $thumbname;
	if ( ($thumbname=="") || ($thumbname=="(thumbnail)") ) {
		$ext = getextension($org_filename);
		$thumbname = preg_replace("/(\.)(.*)$/i", ".thumb.".$ext, $org_filename);
	}
	if (!file_exists("../".$thumbname)) {
		$thumbname = "../".$Cfg['upload_path'].$thumbname;
	} else {
		$thumbname = "../".$thumbname;
	}

	// If the thumbnail exists, make the HTML for it, else just use the text for a link.
	if (file_exists($thumbname)) {
		$thumbname = $Paths['host'] . fixpath( $Paths['pivot_url'].$thumbname);

		$ext=getextension($thumbname);
		if ( ($ext=="jpg")||($ext=="jpeg")||($ext=="gif")||($ext=="png") ) {
			if ($align=="center") {
				$thumbname="<img src=\"$thumbname\" $border alt=\"$alt\" title=\"$alt\"  class='pivot-popupimage'/>";
			} else {
				$thumbname="<img src=\"$thumbname\" $border alt=\"$alt\" title=\"$alt\" align=\"$align\" class='pivot-popupimage' />";
			}
		} else {
			$thumbname = $org_thumbname;
		}
	} else {
		if (strlen($org_thumbname)>2) {
			$thumbname = $org_thumbname;
		} else {
			$thumbname = "popup";
		}
	}

	// Finally, make the HTML for the popup..
	// changed 2004/10/08 =*=*= JM

	if( file_exists( $filename )) {
		list( $width,$height ) = @getimagesize( $filename ) ;
		$filename = $Paths['host'].fixpath( $Paths['pivot_url'].$filename ) ;

		// $url=$Paths['host'] . sprintf($Paths['pivot_url']."includes/photo.php?img=%s&amp;w=%s&amp;h=%s&amp;t=%s",$filename, $width, $height, $alt);
		$url  = $Paths['host'].$Paths['pivot_url'] ;
		$url .= 'includes/photo.php?img='. base64_encode($filename).'&amp;w='.$width.'&amp;h='.$height.'&amp;t='.base64_encode($alt);

		$target = set_target('','target="_self"');
		// removed the onmouseover=\"window.status='(popup)';return true;\"
		// and onmouseout=\"window.status='';return true;\"
		// that is _so_ 2002..
		$code = sprintf( "<a href='%s' rel=\"lightbox\" title=\"%s\" >%s</a>",
				$filename,
				$alt,
				$thumbname 
			);

		if( 'center'==$align ) {
			$code = '<p style="text-align:center;">'.$code.'</p>' ;
		}
	} else {
		$code = '[!-- error: could not popup '.$filename.'. File does not exist --]' ;
	}

	return $code;
}


/**
 * Insert the HTML that goes into the <head> section that includes the js and
 * css files.
 *
 * @return unknown
 */

function snippet_lightbox_head() {
	global $Paths;

	$html = "<!-- Includes for Lightbox script -->\n";
	
	// Lightbox loading gif:
	$html .= sprintf("<script type=\"text/javascript\">\nvar loadingImage = '%slightbox/loading.gif';\n</script>\n",
			$Paths['extensions_url']
		);
		
	// Lightbox script
	$html .= sprintf("<script type=\"text/javascript\" src=\"%slightbox/lightbox.js\"></script>\n",
			$Paths['extensions_url']
		);
	
	// Lightbox CSS	
	$html .= sprintf("<link rel=\"stylesheet\" href=\"%slightbox/lightbox.css\" type=\"text/css\" media=\"screen\" />\n",
			$Paths['extensions_url']
		);
			
	return $html;
	
}


?>