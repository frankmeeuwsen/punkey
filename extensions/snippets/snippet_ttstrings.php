<?

/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 Marco 2005
License:                GPL
==================================================================

Change this file if you want Taggerati in another language

*/

define(__TT_TAGS_IN_POSTING, "<h2>Tags used in this posting</h2><br/>");
define(__TT_CLICK_FOR_UNIVERSE, "Click for local tag universe page. Click the <img src=\"/weblog/pivot/pics/ticon.gif\" alt=\"Description\" /> for Technorati tag page.");
define(__TT_LOCALCOSMOS_DESCRIPTION, "This is the local tag cosmos for this weblog. The large the tag, the more entries on this blog are related to it. Click on any tag to find out more. The ordering of the tags is randomized and will change with every refresh.<br/><br/>");
define(__TT_TAGOVERVIEW_HEADER, "Tag overview for: ");
define(__TT_ENTRIES_WITH_TAG, "Entries on this site with ");
define(__TT_RELATED_TAGS, "Related tags");
define(__TT_LATEST_ON, "Latest links from");
define(__TT_LINKS_TO_TAG_UNIVERSES, "Links to tag universes");
define(__TT_NOTHING_ON, "Nothing on");
define(__TT_ON, "on");
define(__TT_FOUND_ON, "found on");
define(__TT_OTHER_POSTS_WITH_TAG, "Other entries about");

?>
