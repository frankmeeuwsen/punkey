<?
/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 March 2005
License:                GPL
==================================================================

/*
USAGE: See README.txt


Change these for a different max / min font size in your tag cosmos
and the amount of shrinkage that's applied every time the system
makes a tag smaller
The smaller the shrinkage, the more variation in font sizes

*/
define("__MIN_FONT", 12);
define("__MAX_FONT", 52);
define("__SHRINK_FACTOR", 1);


@ini_set("include_path",".");
include_once("snippet_tt.php");
include_once("snippet_ttstrings.php");

function local_tag_cosmos()	{
	if(!$dh = opendir(snippet_pivot_path()."db/tagdata"))	{
		echo "error opening dir";
		exit;
	}
	
	$aTagCosmos = array();
	while ($file = readdir($dh)) {
		$aCheckFile = explode(".",$file);
		if($aCheckFile[1] == "tag")	{
			$aTagCosmos[$aCheckFile[0]] = filesize(snippet_pivot_path()."db/tagdata/$file");
		}
	}
	arsort($aTagCosmos);
	closedir($dh);
	$nSize = __MAX_FONT;
	$aHTMLLinks = array();
	foreach($aTagCosmos as $key => $value)	{
		if($value <= $currentSize)  {
			$nSize = $nSize - __SHRINK_FACTOR;
		}
		if(($nSize < __MIN_FONT) || ($value < 3))	{ $nSize = __MIN_FONT; }
		$sHTML = "";
		$sHTML .= "<a style=\"font-size:".$nSize;
		$sHTML .="px;\"";
		$sHTML .= " href=\"";
		$sHTML .= __TAGPAGE_URL;
		$sHTML .= $key;
		$sHTML .= "\">".str_replace("+"," ",$key)."</a><span style=\"font-size:8px;\"> </span>";
		array_push($aHTMLLinks, $sHTML);
		$currentSize = $value;
	}
	$sHTML = __TT_LOCALCOSMOS_DESCRIPTION;
	$sHTML .= "<div style=\"text-align:left;\">";
	shuffle($aHTMLLinks);
	foreach($aHTMLLinks as $sLink)	{
		$sHTML .= $sLink;
	}
	$sHTML .= "<br/><br/><small>Taggerati extension for <a href=\"http://www.pivotlog.net/\">Pivot</a> by <a href=\"http://www.i-marco.nl/weblog/\">Marco van Hylckama Vlieg</a></small>";
	$sHTML .= "</div>";
	return $sHTML;
}


function get_entries_with_tag($p_sTag, $p_nSkip=0)	{
	global $db;
	$p_sTag = strtolower($p_sTag);
	$p_sTag = str_replace(" ","+", $p_sTag);
	if(file_exists(snippet_pivot_path()."db/tagdata/$p_sTag.tag"))  {
		$fpTagFile = fopen(snippet_pivot_path()."db/tagdata/$p_sTag.tag", "r");
	}
	else  {
		return "";
	}
	$sEntriesString = "";
	while(!feof($fpTagFile))	{
		$sEntriesString .= fread($fpTagFile, 4096);
	}
	fclose($fpTagFile);
	$aEntries = explode(",",$sEntriesString);
	rsort($aEntries);
	$sLinkList = "";
	if(sizeof((($aEntries) > 0) && (sizeof($aEntries) == 1) && ($db->entry["code"] != $p_nSkip)) || (isset($_GET["tag"])))	{
		$sLinkList = "<ul style=\"padding-left:10px;\">";
	}
	foreach($aEntries as $nThisEntry)	{
		$myDB = new db();
		global $Paths;
		$myDB->read_entry($nThisEntry);
		$sTitle = $myDB->entry["title"];
		
		if($myDB->entry["code"] != $p_nSkip)	{
			$sLinkList .= "<li><a href=\"".$Paths["pivot_url"]."entry.php?id=".$myDB->entry["code"]."\">".$sTitle."</a></li>";
		}
	}
	if(strlen($sLinkList) > 0)	{
		$sLinkList .= "</ul>";
	}
	// this sucks but I'm too lazy to figure it out
	if($sLinkList == "<ul style=\"padding-left:10px;\"></ul>")	{
		$sLinkList = "";
	}
	return $sLinkList;
}

function snippet_tagpage()	{
	global $Paths;
	if(!is_dir(snippet_pivot_path()."db/tagdata"))	{
		return "<b>ERROR: You must create ".snippet_pivot_path()."db/tagdata and set permissions to world writable on it.</b>";
	}
	
	if((!isset($_GET["tag"])) || (@$_GET["tag"] == ""))	{
		return local_tag_cosmos();
	}
	$sTheTag = strtolower(str_replace(" ", "+", $_GET["tag"]));
	$sHTML = "<h3>".__TT_TAGOVERVIEW_HEADER." '".str_replace("+"," ",$sTheTag)."'</h3>";
	$sHTML .= "<b>".__TT_ENTRIES_WITH_TAG." '".str_replace("+"," ",$sTheTag)."'</b>";
	$sHTML .= get_entries_with_tag($sTheTag);
	$sHTML .= "<b>".__TT_RELATED_TAGS."</b><br/><br/>";
	if(file_exists(snippet_pivot_path()."db/tagdata/".$sTheTag.".rel"))	{
		$fpTagFile = fopen(snippet_pivot_path()."db/tagdata/".$sTheTag.".rel", "r");
		$sTagString = "";
		while(!feof($fpTagFile))	{
			$sTagString .= fread($fpTagFile, 4096);
		}
		fclose($fpTagFile);
		$aTagList = explode(",", $sTagString);
		$nCount = 0;
		foreach($aTagList as $sThisTag)	{
			$sHTML .= "<a href=\"".__TAGPAGE_URL.str_replace(" ", "+",$sThisTag)."\">$sThisTag</a>";
			$nCount++;
			if($nCount < sizeof($aTagList))	{
				$sHTML .= ", ";
			}
			
		}
	}
	else	{
		$sHTML .= "no related tags.";
	}
	$sHTML .= "<br/><br/>";
	$sHTML .= "<b>".__TT_LINKS_TO_TAG_UNIVERSES."</b>";
	$sHTML .= "<ul style=\"padding-left:10px;\">";
	$sHTML .= "<li><a href=\"http://www.technorati.com/tag/".$sTheTag."\" rel=\"tag\">'".str_replace("+"," ",$sTheTag)."' ".__TT_ON." Technorati</a></li>";
	$sHTML .= "<li><a href=\"http://del.icio.us/tag/".$sTheTag."\" rel=\"tag\">'".str_replace("+"," ",$sTheTag)."' ".__TT_ON." del.icio.us</a></li>";
	$sHTML .= "<li><a href=\"http://www.furl.net/furled.jsp?topic=".$sTheTag."\" rel=\"tag\">'".str_replace("+"," ",$sTheTag)."' ".__TT_ON." furled.net</a></li>";
	$sHTML .= "<li><a href=\"http://www.flickr.com/photos/tags/".$sTheTag."\" rel=\"tag\">'".str_replace("+"," ",$sTheTag)."' ".__TT_ON." flickr.com</a></li>";
	$sHTML .= "<li><a href=\"http://www.43things.com/tag/".$sTheTag."\" rel=\"tag\">'".str_replace("+"," ",$sTheTag)."' ".__TT_ON." 43things.com</a></li>";
	$sHTML .= "</ul>";
	$sHTML .="<br/><b>".__TT_LATEST_ON." del.icio.us ".__TT_ON." '".str_replace("+"," ",$sTheTag)."' <a href=\"http://del.icio.us/rss/tag/".$sTheTag."\"><img src=\"".$Paths["pivot_url"]."/pics/rss.gif\" border=\"0\" alt=\"rss\"/></a></b><br/>";
	$sHTML .= "<ul style=\"padding-left:10px;\">";
	$sRSS = snippet_rss("http://del.icio.us/rss/tag/".$sTheTag."",10,"<li><a href=\"%link%\">%title%</a></li>");
	$sRSS = str_replace("<br/><small>%description%</small>", "", $sRSS);
	if($sRSS == "")	{
		$sRSS = "<li>".__TT_NOTHING_ON." '".str_replace("+"," ",$sTheTag)."' ".__TT_FOUND_ON." del.icio.us.</li>";
	}
	$sHTML .= $sRSS;
	$sHTML .= "</ul>";
	$sHTML .="<br/><b>".__TT_LATEST_ON." flickr.com ".__TT_ON." '".str_replace("+"," ",$sTheTag)."' <a href=\"http://www.flickr.com/services/feeds/photos_public.gne?tags=".$sTheTag."&amp;format=rss_200\"><img src=\"".$Paths["pivot_url"]."/pics/rss.gif\" border=\"0\" alt=\"rss\"/></a></b><br/><br/>";
	if(strstr($sTheTag, "+"))	{
		$sHTML .= "<br /><i>Tags with spaces not supported by Flickr.com.</i><br />";
	}
	else	{
		$fpRSS = fopen("http://www.flickr.com/services/feeds/photos_public.gne?tags=".str_replace("+"," ", $sTheTag)."&format=rss_200", "r");
		$sFile = "";
		while(!feof($fpRSS))    {
			$linecount++;
			$line = fread($fpRSS, 4096);
			$sFile .= $line;
		}
		$sFileArr = explode("http://", $sFile);
		$sLinkArr = explode("<link>", $sFile);
		foreach($sLinkArr as $sRawLink)	{
			$aRawLinkArr = explode("</link>", $sRawLink);
			if(!strstr($aRawLinkArr[0], $sTheTag))	{
				$aLinks[] = $aRawLinkArr[0];
			}
		}
		if(sizeof($aLinks) == 0)	{
			$sHTML .= "<br/><i>Nothing found on flickr.com for $sTheTag</i><br/>";
		}
		foreach($sFileArr as $rawURL)   {
			$aRawURL = explode("_m.jpg", $rawURL);
			foreach($aRawURL as $rawimg)    {
				if((substr($rawimg, 0, 6) == "photos") && (sizeof($aRawURL) > 1))       {
					$aImages[] = $rawimg;
				}
			}
		}
		$nPicCount = 0;
		foreach($aImages as $sThisImage)	{
			$nPicCount++;
			$sHTML .= "<a href=\"".current($aLinks)."\"><img src=\"http://".$sThisImage."_s.jpg\" alt=\"flickr image on $sTheTag\" class=\"flimage\"></a>&nbsp;";
			if($nPicCount == 5)	{
				$sHTML .= "<br />";
			}
			next($aLinks);
		}
		$sHTML .= "<br />";
	}
	$sHTML .="<br/><b>".__TT_LATEST_ON." 43things.com ".__TT_ON." '".str_replace("+"," ",$sTheTag)."' <a href=\"http://www.43things.com/rss/goals/tag?name=".$sTheTag."&amp;format=rss_200\"><img src=\"".$Paths["pivot_url"]."/pics/rss.gif\" border=\"0\" alt=\"rss\"/></a></b><br/>";
	$sHTML .= "<ul style=\"padding-left:10px;\">";
	$sRSS = snippet_rss("http://www.43things.com/rss/goals/tag?name=".$sTheTag."&amp;format=rss_200",10,"<li><a href=\"%link%\">%title%</a><br/><small>%description%</small></li>");
	$sRSS = str_replace("<br/><small>%description%</small>", "", $sRSS);
	if($sRSS == "")	{
		$sRSS = "<li>".__TT_NOTHING_ON." '".str_replace("+"," ",$sTheTag)."' ".__TT_FOUND_ON." 43things.com.</li>";
	}
	$sHTML .= $sRSS;
	$sHTML .= "</ul>";
	$sHTML .="<br/><b>".__TT_LATEST_ON." furled.net ".__TT_ON." '".str_replace("+"," ",$sTheTag)."' <a href=\"http://www.furl.net/members/rss.xml?topic=".$sTheTag."&amp;format=rss_200\"><img src=\"".$Paths["pivot_url"]."/pics/rss.gif\" border=\"0\" alt=\"rss\"/></a></b><br/>";
	$sHTML .= "<ul style=\"padding-left:10px;\">";
	$sRSS = snippet_rss("http://www.furl.net/members/rss.xml?topic=".$sTheTag."&amp;format=rss_200",10,"<li><a href=\"%link%\">%title%</a><br/><small>%description%</small></li>");
	$sRSS = str_replace("<br/><small>%description%</small>", "", $sRSS);
	if($sRSS == "")	{
		$sRSS = "<li>".__TT_NOTHING_ON." '".str_replace("+"," ",$sTheTag)."' ".__TT_FOUND_ON." furl.net.</li>";
	}
	$sHTML .= $sRSS;
	$sHTML .= "</ul>";
	return $sHTML;
}
?>
