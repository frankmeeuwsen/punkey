<?php

// Hardened referrers version 0.3
// By Marco van Hylckama Vlieg
// marco@i-marco.nl

define("__SILENT__", true);

include_once("hr_conf.php");

include_once("../../pivot/pvlib.php");
include_once("../../pivot/pv_core.php");

// only include if the referrer is eh.. you!

if(strstr($_SERVER["HTTP_REFERER"], __MYDOMAIN__) != false)  {
	if(  (file_exists("refkeys/".$_GET["key"])) &&
	        ((time() - filectime("refkeys/".$_GET["key"]) < 10))
	  )  {
		unlink("refkeys/".$_GET["refkey"]);

		// If we have Pivot-Blacklist, apply blacklist check on the
		// referrer for even more protection

		if(file_exists("../blacklist/blacklist_lib.php"))  {
			include_once("../blacklist/blacklist_lib.php");
			$aConfig = pbl_getconfig();
			if($aConfig["refwhiteonly"] == 1)  {
				if(pbl_whitelisted($_GET["referer"]))  {
					include_once("hgetref.php");
					header("content-type:image/gif");
					readfile("pixel.gif");
					exit;
					}
				}
			if (strlen(pbl_checkforspam($_GET["referer"])) > 0)  {
				pbl_logspammer($_GET["referer"], "breferer");
				// act normal
				header("content-type:image/gif");
				readfile("pixel.gif");
				exit;
			}
		}
		include_once("hgetref.php");
	} else  {
		unlink("refkeys/".$_GET["refkey"]);
	}
}
header("content-type:image/gif");
readfile("pixel.gif");
?>
