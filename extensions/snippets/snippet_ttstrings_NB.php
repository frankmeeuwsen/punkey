<?

/*

==================================================================
Snippet-title:          Taggerati v.2.3
Creator:                Marco van Hylckama Vlieg
E-mail:                 marco@i-marco.nl
Creation date:          25 Marco 2005
License:                GPL

==================================================================

Norwegian translation by Hans Fredrik Nordhaug <hans@nordhaug.priv.no>

*/

define(__TT_TAGS_IN_POSTING, "Tagger brukt i denne oppf�ringen");
define(__TT_CLICK_FOR_UNIVERSE, "Klikk for lokalt taggunivers-side. Klikk <img src=\"/weblog/pivot/pics/ticon.gif\"> for Technorati taggsider.");
define(__TT_LOCALCOSMOS_DESCRIPTION, "Dette er det lokale tagguniverset for denne bloggen. Jo st�rre tagg, jo flere oppf�ringer er relatert til den. Klikk p� hvilken som helst tagg for � finne ut mer. Taggenes rekkef�lge er tilfeldig og vil endres seg ved hver oppdatering av siden.<br/><br/>");
define(__TT_TAGOVERVIEW_HEADER, "Taggoversikt for: ");
define(__TT_ENTRIES_WITH_TAG, "Oppf�ringer p� dette nettstedet med ");
define(__TT_RELATED_TAGS, "Relaterte tagger");
define(__TT_LATEST_ON, "Siste lenke fra");
define(__TT_LINKS_TO_TAG_UNIVERSES, "Lenker til taggunivers");
define(__TT_NOTHING_ON, "Ingenting om");
define(__TT_ON, "om");
define(__TT_FOUND_ON, "funnet p�");
define(__TT_OTHER_POSTS_WITH_TAG, "Andre oppf�ringer om");
?>