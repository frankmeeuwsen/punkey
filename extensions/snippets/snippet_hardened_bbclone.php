<?php
/**
 * ==================================================================
 * Snippet-title:          Hardened BBClone v0.5 (15th November 2005)
 * Creator:                Hans Fredrik Nordhaug, Marco van Hylckema
 *                         Vlieg, Bob den Otter
 * E-mail:                 hans@nordhaug.priv.no, marco@i-marco.nl,
 *                         bob@twokings.nl
 * Creation date:          10th October 2005
 * License:                GPL
 * ==================================================================
 *
 * This snippet protects your BBClone stats from referer spam.
 * Read readme.txt for more information.
 *
 * Note: When you have a large number of entries(1000 or more), 
 * do *not* use a page-specific bbclone counter, since bbclone is 
 * not very efficient in dealing with a great number of tracked 
 * pages. This will result in a very large access.php file, and 
 * an increased chance of corrupting the file.
 * 
 * This is also the reason that %title% is not the default any more.
 *
 */


if(!defined('INPIVOT')){ exit('not in pivot'); }

function snippet_hardened_bbclone($title = '') {
    global $Paths, $Pivot_Vars, $db;
    
    if(empty($Paths['bbclone_path'])) {
        return "<!-- error in snippet hardened_bbclone: bbclone directory not found -->";
    }
    
    if ($title != '') {
        $weblogtitle = snippet_weblogtitle();
        if ($Pivot_Vars['c']!="") {
                $archivetitle = category_from_para($Pivot_Vars['c']);
        } else {
                $archivetitle = $weblogtitle;
        }
        if ($Pivot_Vars['tag']!="") {
                $tagtitle = $Pivot_Vars['tag'];
        } else {
                $tagtitle = $weblogtitle;
        }
        $title = str_replace("%tagtitle%", $tagtitle, $title);
        $title = str_replace("%date%", date("Y-m"), $title);
        $title = str_replace("%archivetitle%", $archivetitle, $title);
        $title = str_replace("%title%", snippet_title(), $title);
        $title = str_replace("%weblogtitle%", $weblogtitle, $title);
        $title = addslashes($title);
    } else {
    	
    	// The default value..
    	$title = sprintf("Pivot Page %s", date("Y-m"));
    	
    }
    
    return '
        <!-- hardened bbclone counter -->
        <script type="text/javascript" src="'.
        $Paths["extensions_url"].'bbclone_tools/getkey.php"></script>

        <script type="text/javascript">
        bbcloneCount("'.$title.'");
        </script>';
}

?>

