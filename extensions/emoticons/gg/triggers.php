<?php

// ---------------------------------------------------------------------------
//
// PIVOT - LICENSE:
//
// This file is part of Pivot. Pivot and all its parts are licensed under 
// the GPL version 2. see: http://www.pivotlog.net/help/help_about_gpl.php
// for more information.
//
// ---------------------------------------------------------------------------


// standard ones:
//$emot['(lach)']="e_01.gif";
$emot['(lach)']="e_01.gif";
//$emot['(hmz)']="e_18.gif";
$emot['(hmz)']="e_18.gif";
//$emot['(zuchtje)']="e_86.gif";
$emot['(zuchtje)']="e_86.gif";
//$emot['(hihi)']="e_52.gif";
$emot['(hihi)']="e_52.gif";
$emot['(blbl)']="e_69.gif";
$emot['(blbl)']="e_69.gif";
//$emot['(kusje)']="e_13.gif";
$emot['(kusje)']="e_13.gif";
//$emot['(oef)']="e_103.gif";
$emot['(oef)']="e_103.gif";
$emot['(knipoog)']="e_121.gif";
$emot['(knipoog)']="e_121.gif";
//$emot['(tsja)']="e_112.gif";
$emot['(tsja)']="e_112.gif";
$emot['(tsja)']="e_112.gif";
$emot['(tsja)']="e_112.gif";
$emot["(zucht)"]="e_28.gif";
$emot["(zucht)"]="e_28.gif";
$emot["(zucht)"]="e_28.gif";
$emot["(zucht)"]="e_28.gif";
$emot["(zucht)"]="e_28.gif";
$emot["(nee)"]="e_10.gif";
$emot["(nee)"]="e_10.gif";
$emot["(whaa)"]="e_11.gif";
$emot["(whaa)"]="e_11.gif";
//$emot['(euh)']="e_129.gif";
$emot['(euh)']="e_129.gif";

// special ones:
$emot['(oh)']="e_43.gif";
$emot['(yeah)']="e_26.gif";
$emot['(snik)']="e_168.gif";
$emot['(olee)']="e_75.gif";
$emot['(olee)']="e_75.gif";
$emot['(nanana)']="e_92.gif";
$emot['(nanana)']="e_92.gif";
$emot['(ha-nana)']="e_41.gif";
$emot['(bloempje)']="e_07.gif";
$emot['(koe)']="e_73.gif";
$emot['(koe)']="e_73.gif";
$emot['(grr)']="e_48.gif";
$emot['(devinger)']="e_31.gif";

// and some more :)
$emot['(ja)']="e_02.gif";
$emot['(ach)']="e_03.gif";
$emot['(bloos)']="e_04.gif";
$emot['(tadaa)']="e_05.gif";
$emot['(tss)']="e_06.gif";
$emot['(knipper)']="e_08.gif";
$emot['(argh)']="e_09.gif";
$emot['(ieks)']="e_12.gif";
$emot['(hm-hm)']="e_14.gif";
$emot['(yuck)']="e_15.gif";
$emot['(grmpf)']="e_16.gif";
$emot['(ha)']="e_17.gif";
$emot['(grijns)']="zmgrijns.gif";
$emot['(buiging)']="rev.gif";
$emot['(fluit)']="zm54.gif";
$emot['(dans)']="hurra.gif";
$emot['(hoi)']="e_53.gif";


?>