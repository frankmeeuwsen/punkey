<?php
// Name: Pre Display Hook
// Version: 
// Author: 
// License: GPL 2.0





/** 
 * Is called after the entry is parsed and just before the ouptut is written
 * to the browser. The parameters are passed by reference, so if you change them in 
 * your function, they will also be changed in the part of the code from
 * which it was called.
 * 
 * @param array $entry 
 * @param string $output
 *
 */
function post_display_entry(&$entry, &$output) {

	
}

?>