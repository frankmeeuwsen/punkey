<?php
// Name: Pre Display Hook
// Version: 
// Author: 
// License: GPL 2.0



/**
 * Is called at the beginning of entry.php 
 *
 */
function pre_display_entry_init() {
		
}


/** 
 * Is called just before the entry is fetched from the DB and  
 * parsed. The parameters are passed by reference, so if you change them in 
 * your function, they will also be changed in the part of the code from
 * which it was called.
 * 
 * @param integer $id 
 * @param string $override_weblog
 *
 */
function pre_display_entry(&$id, &$override_weblog) {
	
}

?>