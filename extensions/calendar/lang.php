<?php

/**
 * These are the language strings used in PCalendar.
 */

$lang['pcalendar'] = array(

	'calendar_summary'   => 'This table represents a calendar of entries in the weblog with hyperlinks on dates with entries.',
	'calendar_noscript'  => 'The calendar provides a means to access entries in this weblog',

);


?>