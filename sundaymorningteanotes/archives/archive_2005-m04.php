<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1796"></span><div class="entry">
<h3><span class='date'>17 April 05 - 11&#58;22</span>Sunday morning teanotes</h3>

	<p>Goedemorgen! Nou ja, bijna middag. Even flink uitgeslapen deze zondag. Gisteren al weer druk bezig geweest met inpakken en verf kopen. En de kogel is door de spreekwoordelijke kerk. We hebben de kleur gevonden voor onze voordeur in het Utrechtse huisje: Brabants Rood! Hahaha! Toch nog een stukje Brabant meenemen naar Utrecht.<br />
De komende tijd wordt akelig hectisch. Niet alleen met de verhuizing, maar ook met het werk. Een aantal congressen, nieuwe opdrachten, lopende opdrachten die moeten worden afgerond. En tussendoor nog in een verfkloffie de boel mooi maken. Dus het zal hier stil worden de komende weken. Niet alleen door de week, maar ook op zondag. Want dit is zo&#8217;n beetje de laatste zondag voor een flinke poos dat ik achter een PC kan zitten of dat ik tijd heb. Ga maar na:
	<ul>
		<li>24 april: Ben ik onderweg in de Thalys naar <a href="http://www.socialtext.net/loicwiki/index.cgi?internet_2_0"  target='_blank'>Internet 2.0</a></li>
		<li>1 mei: Zijn we al in het huis bezig als het goed is. Daar is nog geen Internet&#8230;</li>
		<li>8 mei: Zijn we zeker in het huis bezig. Nog steeds zonder Internet</li>
		<li>15 mei: Als het goed is de eerste zondag in het nieuwe huis. Met Internet wat ik dan moet aansluiten&#8230;</li>
		<li>22 mei: Dan zou de eerstvolgende echte SMTN wel weer eens online kunnen staan.</li>
	</ul></p>
	<p>Want laat even duidelijk zijn: Deze stukjes worden ook echt op zondagochtend geschreven! Niets vooruit gewerkt, niets genept. Dus dat houden we ook zo!<br />
Verder ben ik ook druk bezig met een nieuw op te starten bedrijfje met een goede vriend van me. Ik hou nog steeds even geheim wat het is, maar de komende maanden zal ook dat steeds duidelijker worden. Dat betekent nog minder tijd om te loggen. Zoals ik al eerder heb gehint, vraag ik me ook sterk af of ik nog door moet gaan met punkey.com. Er zijn ondertussen 100.000-en andere logs bij gekomen die hetzelfde doen als ik, links dumpen en af en toe eens een mening geven. Ik doe dat ondertussen ook op andere plaatsen en, eerlijk is eerlijk, met meer voldoening dan ik hier doe. Dit is meer en meer een soort speeltuintje, een zandbakje, waar ik wel eens wat in gooi. <br />
Maak je niet druk, de boel gaat (nog) niet op zwart, maar reken er op dat het aantal posts hier sterk minder zal worden en dat je meer zult tegen komen op andere plekken zoals <a href="http://www.frank-ly.nl"  target='_blank'>Frank-ly</a>, <a href="../gtd"  target='_blank'>What&#8217;s The Next Action</a>, <a href="http://www.Marketingfacts.nl"  target='_blank'>Marketingfacts</a> en <a href="http://www.AboutBlank.nl"  target='_blank'>About:Blank</a>. <br />
Ik ga snel aan de slag. Ik heb vandaag twee projecten op stapel: een nieuwe frshdjs-site in orde maken zodat de redactie netjes verder kan op iets nieuws. En ik moet een presentatie voorbereiden voor het nieuwe businessplan. Spannend spannend spannend!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1796&amp;w=sunday_morning_teanotes#comm" title="M@gicToon, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1796&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m04.php#e1796" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1796&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1787"></span><div class="entry">
<h3><span class='date'>10 April 05 - 10&#58;19</span>Sunday morning teanotes</h3>

	<p>Wederom een korte SMTN want we hebben het druk druk druk met de aankomende verhuizing. Afgelopen week zijn er verhuisbedrijven langsgeweest voor offertes om de boel in Breda op te pikken en in Utrecht uit te laden. Die gasten zijn behoorlijk vergevorderd! We wonen nu op de eerste verdieping en ik zag een paar van die verhuizers al de trap op en af denderen met dozen. Maar dat hoeft dus niet, want ze hebben tegenwoordig laadruimtes die ze op kunnen krikken tot de eerste verdieping. Nee maar <em>hoe</em> vet is dat?! We hebben een keuze gemaakt en deze week krijgen we 70 verhuisdozen voor onze spullen. Met name voor onze verzameling boeken en CD&#8217;s denk ik&#8230;</p>
	<p><strong>Verf</strong><br />
Zaterdagmiddag op de bouwboulevard is niet mijn meest favoriete uitje, maar rondlopen in een Praxis met allerlei DHZ-gear is toch wel kicken. Eerst op zoek naar verf. We willen in de woonkamer per se de kleur van Tasmaanse Zee die we in Nieuw Zeeland <a href="http://www.flickr.com/photos/punkey/8659700"  target='_blank'>hebben gezien</a>. En we hebben het gevonden! Heel simpel en eigenlijk heel logisch, de kleur heet (drumroll&#8230;.)...Azuur! Logisch he? Vonden wij wel, zeer beschaamd na 10 kleurstalen naast een foto houden en er dan achter komen dat je de kleur Azuur nodig hebt&#8230;<br />
Daarna nog briljante mexicaanse tegels voor de keuken gevonden, een boormachine gekocht en checken hoe we het beste een houten vloer kunnen schuren en lakken. Ik ben weer een stuk slimmer!</p>
	<p>OK, dat was het even voor nu, ik moet aan de slag om kasten op te gaan ruimen met alles wat NIET mee gaat naar Utrecht&#8230;</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1787&amp;w=sunday_morning_teanotes#comm" title="Mark Green, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1787&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m04.php#e1787" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1787&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1782"></span><div class="entry">
<h3><span class='date'>03 April 05 - 10&#58;46</span>Sunday morning teanotes</h3>

	<p>Heerlijke zondagochtend met het zonnetje op de bol. Nu zit ik nog in de serre, maar over een paar maanden in de achtertuin van ons nieuwe huis <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/> hehehehe. </p>
	<p><strong>Spin Awards</strong><br />
Jullie hebben <a href="http://www.rhinofly.nl/frank-ly/index.cfm?Fuseaction=frankly.search&#38;trefwoord=&#38;auteur=&#38;maand=04"  target='_blank'>al kunnen lezen</a> dat ik vrijdag bij de jureringsdag van de Spin Awards ben geweest. Leuk om die verschillende cases te horen en te weten te komen hoe succesvol ze zijn geweest. Niet iedereen is even scheutig met die cijfers. Logisch, want de opdrachtgever wil dat niet altijd laten weten. Ben je werkzaam in online marketing of heb je iets te maken met reclame campagnes, dan kun je hier niet wegblijven. Veel goede cases, mooi materiaal en interessante mensen te ontmoeten. Komende donderdag ben ik ook op de uitreiking van de Spin Awards namens Frank-ly. Daar zullen (als de UMTS kaarten gaan werken) live updates zijn van de uitslag</p>
	<p><strong>Frshdjs.com</strong><br />
Niet iedereen weet dit, maar ik ben ook verantwoordelijk voor de bouw en continuering van de site <a href="http://www.frshdjs.com"  target='_blank'>frshdjs.com</a>. Deze regionale DJ-site heb ik met twee vrienden, <a href="http://www.dahuge.com"  target='_blank'>Da Huge</a> en <a href="http://markgreen.web-log.nl/"  target='_blank'>DJ Mark Green</a>, opgezet in het voorjaar van 2000. Sindsdien zijn er zo&#8217;n 700 DJ&#8217;s ingeschreven en honderden artikelen geschreven. Echter, ik kan het niet meer opbrengen om me nog actief met de site te bemoeien. Het wordt te veel, ik ben te druk met andere zaken en het bouwen en onderhouden van een website interesseert me niet meer zo veel als vroeger. Dus heb ik de knoop doorgehakt en besloten te stoppen met frshdjs.com. Dat wil niet zeggen dat de site weg gaat. Als er in het publiek ontwikkelaars zijn die de site willen onderhouden en wellicht vernieuwen (hard nodig), laat het weten. De site is geheel in Cold Fusion gebouwd met een SQL database er achter.</p>
	<p><strong>Moleskine</strong><br />
Sinds enkele weken heb ik op aanraden van een collega een Moleskine notebook gekocht. Een papieren notebook. En ik moet zeggen, het is een heerlijk ding. Het ziet er relatief simpel uit, maar het briljante zit in de details. Zo kun je de Moleskine eenmaal open op een pagina, helemaal plat leggen. Dus de pagina&#8217;s bollen niet op naar het midden. Verder is het papier niet behandeld met allerlei zuren. Dat betekent een andere schrijfervaring. Het schrijft veel &#8220;zachter&#8221; en prettiger dan op normaal papier<br />
Andere extra&#8217;s: Een elastiek die om het boekje kan om alles bij elkaar te houden en een extra opbergvakje achterin om losse papiertjes en ideen in te verzamelen.<br />
Meer extra&#8217;s: een complete cultbeweging op Internet met geweldige sites over het gebruik van de Moleskine, hacks en foto&#8217;s
	<ul>
		<li><a href="http://www.moleskinerie.com/"  target='_blank'>Moleskinerie</a></li>
		<li><a href="http://www.43folders.com/2004/11/post.html"  target='_blank'>Moleskine hacks</a></li>
		<li><a href="http://wiki.43folders.com/index.php/Moleskine_Hacks"  target='_blank'>Meer Moleskine hacks</a> in een Wiki-project</li>
		<li><a href="http://octolan.com/journey/"  target='_blank'>The wandering Moleskine Project</a>: Een Moleskine gaat de wereld over en scans worden op deze pagina verzameld</li>
		<li>Flickr <a href="http://flickr.com/groups/36521985904@N01/"  target='_blank'>Moleskinerie fotopool</a> en de <a href="http://flickr.com/photos/tags/moleskine/"  target='_blank'>Moleskine public tag</a></li>
		<li><a href="http://groups-beta.google.com/group/Moleskinerie"  target='_blank'>Google groups</a></li>
	</ul></p>
	<p>Volgens mij heeft een merk als <a href="http://www.ahrenddirect.nl/indexahd.html"  target='_blank'>Ahrend</a> toch niet zo&#8217;n fanclub voor iets normaals als een schrijfblok&#8230;</p>
	<p><strong>Punkey, TPG en VT Wonen</strong><br />
De verhuizing brengt ook een hoop administratieve rompslomp met zich mee. Zoals adreswijzigingen doorgeven. Nu kan dat tegenwoordig bij de TPG online waarvoor <a href="http://www.punkey.com/pivot/entry.php?id=1767"  target='_blank'>wederom hulde</a>. Bij <a href="http://www.tpgpost.nl/voorthuis/verhuisservice/index.php"  target='_blank'>het formulier</a> staat een invoerveld :VT Wonen actiecode. Aangezien wij abonnee zijn op dat blad, denk ik: Ha! Korting! Mooi! Maar ik heb geen idee waar ik die code vandaan moet halen. Bij de TPG site staat niets, op de VT Wonen site staat niets en in het blad is ook niets te vinden. Dus maar een mailtje gestuurd naar VT Wonen. Natuurlijk hoor ik daar noooooit meer iets van. Het is nu al bijna een week geleden en ik hoor niets van die koekenbakkers.<br />
Wat schetst dan ook mijn verbazing als ik ga zoeken bij Google op <a href="http://www.google.nl/search?hl=nl&#38;q=vt+wonen+actie+tpg+post&#38;btnG=Google+zoeken&#38;meta"  target='_blank'>vt wonen actie tpg post</a>=. Inderdaad&#8230;mijn eigen post over TPG post. Zie <a href="http://www.punkey.com/images/vtwonengoogle.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy92dHdvbmVuZ29vZ2xlLmpwZw%3D%3D&w=657&h=221&t=','imagewindow','width=657,height=221,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>het screenshot</a> voor het nageslacht. Ja, zo kom ik er natuurlijk nooit achter&#8230;</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1782&amp;w=sunday_morning_teanotes#comm" title="Mark Green, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1782&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m04.php#e1782" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1782&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1796"></span><span class="archief"><a href="/pivot/entry.php?id=1796&amp;w=sunday_morning_teanotes">17 April 2005:Ready for Utreg</a><br></span>
<span id="e1787"></span><span class="archief"><a href="/pivot/entry.php?id=1787&amp;w=sunday_morning_teanotes">10 April 2005:Verf</a><br></span>
<span id="e1782"></span><span class="archief"><a href="/pivot/entry.php?id=1782&amp;w=sunday_morning_teanotes">03 April 2005:Moleskine en de Spin Awards</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>