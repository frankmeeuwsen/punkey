<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1764"></span><div class="entry">
<h3><span class='date'>27 Maart 05 - 11&#58;31</span>Sunday morning teanotes</h3>

	<p>Paaszondag&#8230;lekker! Ik heb geen idee hoe laat het nu eigenlijk is, omdat ik de klok van mijn PC niet vertrouw, zomer/wintertijdtechnisch gezien en omdat ik geen zin om een andere klok te zoeken. Dus euhmm&#8230;.het zal wel.<br />
Afgelopen week is een nogal hectisch weekje geweest. We hebben bij Rhinofly <a href="http://www.rhinofly.nl/frank-ly/index.cfm?fuseaction=frankly.view&#38;FranklyId=CAE56649-E309-1B52-98C2709BB4526A84"  target='_blank'>een van onze grootste projecten opgeleverd</a> in het begin van de week en daar zit natuurlijk altijd nog wat beheer aan vast. Met als gevolg dat je er toch nog wel een week langer mee bezig bent. Nu hoef ik niet het harde codeklopwerk te doen, maar na een dagje mailen en bellen met de opdrachtgever ben je toch ook best gaar.<br />
De nieuwe site <a href="http://www.WerkenbijhetRijk.nl"  target='_blank'>WerkenbijhetRijk.nl</a> zit vol met allerlei nieuwe snufjes en technieken. Ik heb in elk geval al begrepen dat door het gebruik van XHTML en CSS het dataverbruik is gehalveerd in de eerste week, terwijl het bezoek is toegenomen! Dat gaat knaken schelen!<br />
We hebben ook een van mijn stokpaardjes toegevoegd: RSS. Op verschillende plaatsen in de site kun je RSS feeds vinden. Zoals de <a href="http://www.werkenbijhetrijk.nl/rss/top25.xml"  target='_blank'>top 25 nieuwe vacatures</a>, of <a href="http://www.werkenbijhetrijk.nl/verrijking/rss/"  target='_blank'>nieuwe artikelen in het digitale magazine De Verrijking</a>. Echt leuk wordt het pas als je een Persoonlijk dossier aanmaakt met een vacature zoekprofiel. Je krijgt dan automagisch een RSS feed die je kunt gebruiken. En dat gaat een stuk sneller dan de email-optie. Er wordt namelijk maar 2 per week een mail uitgestuurd met nieuwe vacatures in je profiel. De RSS feed update constant (nou ja, met een cache van 1 uur). Dus nieuwe vacatures staan sneller in je RSS-inbox dan in je mail-inbox! </p>
	<p>De komende weken ook meer dan gemiddeld bezoek aan congressen enzo. Allereerst ga ik vrijdag naar de <a href="http://www.spinawards.nl/?pg=13"  target='_blank'>SpinAwards jureringsdag</a> in het Mediaplaza. Want Buutvrij for Life is genomineerd en daar moet ik natuurlijk bij zijn. Je kunt trouwens nog <a href="http://www.spinawards.nl/stemmen/"  target='_blank'>steeds stemmen voor de publieksprijs</a>. Buutvrij dus he.<br />
Dan de week erop ga ik naar de SpinAwards uitreiking in Amsterdam. Omdat we met Frank-ly mediapartner zijn is de kans zeeeeer aanwezig dat we gaan liveloggen met video. Hou het in de gaten! 21 April is <a href="http://www.Blognomics.nl"  target='_blank'>Blognomics.nl</a> waar ik namens <a href="http://www.aboutblank.nl"  target='_blank'>About:Blank</a> aanwezig zal zijn om verslag te doen van dit middagseminar. En de kers op de appelmoes is het weekend erna, als ik voor Frank-ly en Rhinofly naar <a href="http://www.socialtext.net/loicwiki/index.cgi?internet_2_0"  target='_blank'>Internet 2.0</a> mag om daar te hubnubben met de groten uit de blogosfeer, zoals <a href="http://calacanis.weblogsinc.com/"  target='_blank'>Jason Calcanis</a> van Weblogs inc. en <a href="http://www.sifry.com/alerts/"  target='_blank'>David Siffry</a> van Technorati. Erg cool en ik zie er erg naar uit om daar heen te gaan!</p>
	<p>In de tussentijd moeten we ook nog verhuizen, wat betekent dat we nog wel een ander moeten regelen. Dus die vrije paasdagen komen wel mooi uit om eens wat orde op zaken te stellen met belastingpapieren, bankzaken, verzekeringen, verhuisservice en nieuwe spullen die we moeten kopen. Voor het laatste hebben we gisteren al een mooie aankoop gedaan: 2 prachtige mozaiek lampen. Ze schijnen uit de Fillipijnen te komen, dus zit natuurlijk weer een levertijd op. Maar we hebben ze op tijd als het mee zit! Helaas heb ik er geen foto van gemaakt in de winkel, dus je moet me maar geloven dat ze mooi zijn <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/><br />
Oh, en ik heb <a href="http://www.96hours.com/"  target='_blank'>nieuwe sneakertjes</a> gekocht. Blits to the max nietwaar! Heeft geen ruk met het nieuwe huis te maken, maar was wel even nodig</p>
	<p>Tijd voor de honing-aardbei omeletjes op deze paasdag!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1764&amp;w=sunday_morning_teanotes#comm" title="QT">een reactie</a> /  <a href="/pivot/entry.php?id=1764&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m03.php#e1764" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1764&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1757"></span><div class="entry">
<h3><span class='date'>20 Maart 05 - 09&#58;12</span>Sundaymorning teanotes</h3>

	<p>Nou ja, zeg maar Sunday morning colanotes. Ik zit in de trein en ben onderweg naar Utrecht. Tegen de tijd dat jullie dit lezen ben ik daar al. Vandaag leggen we de laatste hand aan de nieuwe site voor <a href="http://www.werkenbijhetrijk.nl"  target='_blank'>werkenbijhetrijk.nl</a>.  Dit is een enorm project waar ik met een collega als projectmanager verantwoordelijk voor ben. Dus ik zat om 7 uur al op de fiets om de trein te halen. Geen tijd om te ontbijten en op het station aangekomen zijn de diverse kiosken nog dicht. Breda CS is schijnbaar nog niet gereed om een koffie- en thee automaat te plaatsen dus dan maar aan de cola. Het is even wennen en het is best wel vies. Maar om nou een uur in de trein te zitten met een droge keel is ook maar vervelend.<br />
Ik kan jullie melden dat er weinig spectaculairs is gebeurd de afgelopen week. Voornamelijk gewerkt eigenlijk. Nog wel nieuwe klanten binnengehaald, maar daar kan ik nog even niets over vertellen. Eerst de contracten tekenen! <br />
Natuurlijk zijn we al wel bezig met het nieuwe huis. Zo langzaam maar zeker zijn we aan het verzamelen wat we nu eigenlijk moeten doen, wat we verhuizen en vooral wat niet. Wat moeten we vernieuwen? Welke abonnementen moet ik overzetten naar een nieuw adres? Welk verhuisbedrijf kiezen we?<br />
We hebben ook een nieuw kunstwerk aangeschaft voor het nieuwe huis. Het is dit schilderij wat we in Auckland in een gallery zagen hangen. We zijn er meteen verliefd op geworden en nu is de koop dan ook rond. Echt te gek! <a href="http://www.punkey.com/images/now_thats_g_copy.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9ub3dfdGhhdHNfZ19jb3B5LmpwZw%3D%3D&w=640&h=480&t=','imagewindow','width=640,height=480,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>Hier zie je een foto</a> die we van de gallery eigenaar hebben gekregen<br />
Het nieuws van de afgelopen week heb ik ook niet echt meegekregen dus daar kan ik ook niet op reageren&#8230;tja&#8230;<br />
O ja, nog bedankt voor de enorme hoeveelheden ontwerpen die ik heb gekregen voor een icoon bij de SMTN. Welgeteld 1 en die is niet zo mooi. Maar die passen we nog even aan en dan komt dat wel in orde. Natuurlijk is hij door mijn grote vriend <a href="http://www.BabyGrandpa.com"  target='_blank'>Baby Grandpa</a> gemaakt.<br />
Ik denk dat ik nog maar even een tukje ga doen in de trein.</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1757&amp;w=sunday_morning_teanotes#comm" title="Maarten Van E., N">twee reacties</a> /  <a href="/pivot/entry.php?id=1757&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m03.php#e1757" title="Permanent link to 'Sundaymorning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1757&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sundaymorning teanotes'">&para;</a>--> </p>
</div><span id="e1743"></span><div class="entry">
<h3><span class='date'>13 Maart 05 - 10&#58;14</span>Sunday morning teanotes</h3>

	<p>Een volle week is het geweest en een volle week gaat het worden. Omdat ik op het corporate HQ met <a href="http://www.rhinofly.nl/frank-ly/index.cfm?fuseaction=frankly.view&#38;FranklyId=897C0E57-9868-5B6C-05E79AF79E7693B6"  target='_blank'>een nogal flink project</a> bezig ben is het nog steeds zo dat ik (te) weinig tijd overhou om punkey.com een andere richting op te sturen. Want natuurlijk heb ik nog 1001 andere bezigheden die evenveel, zo niet meer, aandacht vragen. Wat dat betreft ben ik blij dat we gaan verhuizen naar Utrecht (nog 2 maanden!!). Ik kan dan ook in huis met een schone lei beginnen en ook daar de <a href="http://www.whatsthenextaction.com"  target='_blank'>Getting Things Done-methodiek</a> meer gaan toepassen. Op werk gaat dat steeds beter, nu prive nog! </p>
	<p><strong>Moleskine</strong><br />
Al een aantal maanden verbaas ik me over de diverse posts die ik lees over een specifiek notebook. Nee, geen digitaal apparaat, maar gewoon,  een schrijfblok met pagina&#8217;s, die je vast kan houden. Een Moleskine om precies te zijn. Ik heb vanochtend eens <a href="http://www.furl.net/members/punkey/Moleskine"  target='_blank'>een aantal links</a> verzameld en zal er snel een post over schrijven. Waarom? Ten eerste omdat er zelfs <a href="http://www.punkey.com/images/moleskinechick.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9tb2xlc2tpbmVjaGljay5qcGc%3D&w=279&h=224&t=','imagewindow','width=279,height=224,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>pinup-plaatjes</a> van zijn en ten tweede omdat ik er nu zelf ook <a href="http://www.flickr.com/photos/punkey/sets/157298/"  target='_blank'>een heb</a> en inderdaad enthousiast ben over de manier waarop het product een &#8220;experience&#8221; wegzet voor een simpele actie als &#8220;schrijven&#8221;. </p>
	<p><strong>Nieuwe server Pivot</strong><br />
Afglopen week is deez&#8217; website naar een nieuwe server gegaan. Het kan dan ook zijn dat je wat posts mist, de RSS feed dubbele items geeft of ander ongerieven. Namens de Directie, Dagelijks Management, Raad van Bestuur en de Technische Dienst, onze excuses. Hopelijk komt dit niet meer voor.</p>
	<p><strong>Nieuwe muis</strong><br />
Oh de ironie. Gelijk met de Moleskine heb ik ook een nieuwe muis gekocht. Een superduperturbo ding man! Wow! Een <a href="http://www.logitech.com/index.cfm/products/details/NL/NL,CRID=2135,CONTENTID=9043"  target='_blank'>Logitech X1000 Cordless</a> dus. Ligt lekker in de hand, doet zijn ding goed, muizen dus, en is met een oplader. Dus geen gedoe met batterijen enzo. Is er een minpuntje? Ja. De software. Ik heb er een hekel aan om bij elk stuk hardware ook weer software te moeten installeren wat altijd meer doet, overneemt en pakt dan ik wil. Zie ook mijn eerdere <em>rant</em> over CD&#8217;s en DVD&#8217;s. Bij de muis is dat een stuk minder, maar toch heb ik nu weer zo&#8217;n irritant extra icoontje in mijn taskbar waarvoor ik weer moeite moet doen om het uit te zetten. Allemaal kostbare opstarttijd. Maar wel <a href="http://www.flickr.com/photos/punkey/6422395/"  target='_blank'>een lekker ding</a> hoor! Ik ben gelukkig niet de enige malloot die <a href="http://www.flickr.com/photos/tags/logitech/"  target='_blank'>een foto</a> van zijn muis online zet&#8230;</p>
	<p><strong>Onverschillige reclames</strong><br />
Als we nu toch aan het <em>ranten</em> zijn, wat vinden jullie van die onverschillige reclames? Wat bedoel ik daarmee? Ik zal het je uitleggen! Let op!<br />
Pak de reclame van Nationale Nederlanden (jochie schiet bal door ruit), Tiscali (Man vindt vrouw met buurman in bed) en <del>ik meen</del> een of ander vruchtendrankje (jochie komt thuis met tattoo&#8217;s op arm en nek). Valt het jullie ook op dat iedereen in de reclames zo verschrikkelijk onverschillig en nietszeggend reageert? Wat is dat? Nieuwe trend onder de reclamemakers? Sign of the times?</p>
	<p><strong>Gebaren</strong><br />
We kunnen allemaal met heel veel woorden schrijven wat we bedoelen, of proberen met foto&#8217;s een verhaal te vertellen. Maar <em>in the end</em> kun je toch het beste <a href="http://home.wanadoo.nl/tjvermeys/deafstandup%5B1%5D.wmv"  target='_blank'>met gebaren</a> (rechst-klik link!) je punt duidelijk maken. met dank aan <a href="http://www.frankwatching.com"  target='_blank'>Frank Janssen</a>, de rijzende ster in het businessblog firmament! Hij staat nu zelfs in de Telegraaf! (Hier is <a href="http://www.mediafact.nl/comments.php?id=7753_0_1_0_C"  target='_blank'>een scan</a> te vinden)</p>
	<p><strong>Coverville</strong><br />
Deze SMTN is geschreven terwijl ik naar de <a href="http://www.coverville.com/archives/2005/03/coverville_60_t.html"  target='_blank'>laatste Coverville</a> luister. Met een ge-wel-dige cover van Chrissie Hynde die Jimi Hendrix&#8217; &#8220;Bold as Love&#8221; doet. Mindblowing en de moeite van het luisteren waard!</p>
	<p><strong>Logo voor SMTN?</strong><br />
Zomaar iets wat me nu te binnen schiet. Wie heeft er zin/tijd/inspiratie om een klein logo te maken voor deze categorie posts? Liefst een vierkant logo/beeldmerk wat de SMTN uitbeeldt. Dit logo komt dan bij elke post te staan. </p>
	<p>Ik ga ontbijten!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1743&amp;w=sunday_morning_teanotes#comm" title="Eric, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1743&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m03.php#e1743" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1743&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1732"></span><div class="entry">
<h3><span class='date'>06 Maart 05 - 10&#58;15</span>Sunday morning teanotes</h3>

	<p>Wat een weer he? Man man man&#8230;.guttguttegut&#8230;het mag wel weer wat gaan dooien hoor&#8230;sjongejongejonge&#8230;</p>
	<p>Zondagochtend, tijd voor de teanotes. Ik heb nu net even geen thee, maar het gaat om de traditie nietwaar? Drie onderwerpjes deze ochtend&#8230;</p>
	<p><strong>Feeddemon en Bloglines</strong><br />
Op aanraden van een collega ben ik eens aan de slag gegaan met <a href="http://www.feeddemon.com/feeddemon/"  target='_blank'>Feeddemon</a> als mijn RSS-reader. Met name om de geintegreerde podcast-client en omdat het een koppeling heeft met Bloglines. &#8220;Eindelijk!&#8221; dacht ik, &#8220;Kan ik mijn feeds centraal wegzetten en op verschillende werkplekken met 1 RSS-client lezen!&#8221; Euhm&#8230;nou nee dus&#8230;De integratie met Bloglines is leuk, maar niet effectief. Ik heb FeedDemon op mijn laptop en thuis PC staan. En beiden maken gebruik van dezelfde feeds uit Bloglines. Wat nu bedoeling <em>zou moeten zijn</em> is dat hetgeen ik op de laptop heb gelezen, niet meer als &#8220;ongelezen&#8221; op mijn PC komt te staan. Dat is dus niet zo. Niet alleen haalt FeedDemon vaak dezelfde berichten op die ik al eerder heb gelezen, maar op mijn PC komt helemaal niets nieuws en kan ik alleen nieuws van een week geleden ophalen. What&#8217;s up with that?<br />
Dus FeedDemon heeft weer wat minpuntjes bij me. Zucht&#8230;toch weer op zoek naar iets nieuws? Tja&#8230;Mijn oog is ook op <a href="http://www.onfolio.com/"  target='_blank'>OnFolio</a> gevallen, omdat dit een RSS-reader/organizer/publishtool is. Handig, omdat ik veel interessante berichten op een handige manier wil bewaren, ook als ik offline ben. </p>
	<p><strong>Verhuizen</strong><br />
Gisteren zijn we weer in ons nieuwe huis gaan kijken in Utrecht. Ook weer wat nieuwe foto&#8217;s gemaakt en wat muren opgemeten enzo. Het begint nu toch echt allemaal. Al het regelwerk, nieuwe spullen uitzoeken, weg uit Breda, inschrijven in Utrecht. Zoals het er nu naar uitziet gaan we half mei over. En dan hebben we een achtertuin! Sunday Morning Teanotes uit de achtertuin! Woehoe!!!!<br />
Maar dan moet er niet zoveel sneeuw liggen hoor&#8230;sjongejongejonge&#8230;.poepoepoe&#8230;</p>
	<p><strong>Video op je weblog</strong><br />
Ik heb het vorige week tijdens de <a href="http://www.dutchbloggies.nl"  target='_blank'>Dutch Bloggies</a> al aangekondigd, maar door de zenuwen kwam het er niet zo goed uit geloof ik. Maar we (<a href="http://www.rhinofly.nl"  target='_blank'>Rhinofly</a>) zijn bezig om een nieuwe dienst op te zetten. We willen een service opzetten voor weblogs om makkelijk en snel video&#8217;s op te nemen via een webcam en te publiceren op hun weblog. Is niet nieuw, dat weet ik ook wel, maar wij zijn een weblog-loving company (kijk maar <a href="http://www.frank-ly.nl"  target='_blank'>hier</a>, <a href="http://www.coolbook.net"  target='_blank'>hier</a>, <a href="http://www.margin-right.nl"  target='_blank'>hier</a>, al is die wat stil, en euhm&#8230;hier dus) en we willen het voor scherpe prijzen weg gaan zetten.<br />
We willen echter wel een en ander wat goed testen en daarom ben ik op zoek naar wat weblogs die een en ander voor ons willen testen en hun bevindingen met ons willen delen. En de rest van de wereld als je wilt. Maar voornamelijk met ons. Op basis van deze bevindingen kunnen we de dienst verbeteren en in het voorjaar gaan lanceren. Wil je meer weten? Laat een berichtje achter in de comments of mail me op punkey-Apestaartje-gmail-punt-com (antispam!) en ik zet je op de lijst. We zullen je niet volgende week al de spullen sturen, maar zo kunnen we al wel wat meer informatie krijgen en eerste ideeen over de dienst!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1732&amp;w=sunday_morning_teanotes#comm" title="dennis, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1732&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m03.php#e1732" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1732&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1764"></span><span class="archief"><a href="/pivot/entry.php?id=1764&amp;w=sunday_morning_teanotes">27 Maart 2005:Pasen</a><br></span>
<span id="e1757"></span><span class="archief"><a href="/pivot/entry.php?id=1757&amp;w=sunday_morning_teanotes">20 Maart 2005:Uit de trein</a><br></span>
<span id="e1743"></span><span class="archief"><a href="/pivot/entry.php?id=1743&amp;w=sunday_morning_teanotes">13 Maart 2005:Muizen, gebaren en covers</a><br></span>
<span id="e1732"></span><span class="archief"><a href="/pivot/entry.php?id=1732&amp;w=sunday_morning_teanotes">06 Maart 2005:Bloglines en video</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>