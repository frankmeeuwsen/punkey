<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e6986"></span><div class="entry">
<h3><span class='date'>31 December 05 - 00&#58;32</span>Sunday morning teanotes</h3>

<span style="font-weight: bold">Wat krijgen we nu!</span><br />Op 1 januari zomaar ineens een update! En nog wel een Sunday Morning Teanote! Ja, het bloed kruipt toch waar het niet gaan kan. Nadat ik op 31 juli mijn laatste post op <a href="..//"  target='_blank'>Punkey.com</a> schreef was het voor mij ook echt wel &quot;klaar ermee:. En dat ben ik nog steeds met punkey.com als weblog zoals het was. Wat me echter wel opviel is dat ik heel veel positieve reacties op de Teanotes kreeg, zowel in de reacties van de laatste als via email in de maanden er na. Of in de kroeg of tijdens conferenties. En ja, dan gaat het toch een beetje kriebelen. Ik wilde niet meteen weer een heel nieuw iets weg gaan zetten dus waarom niet gewoon doorgaan waar ik ben gestopt 5 maanden geleden? Met de SMTN! Op een eigen domein en met een eigen vormgeving. Nu zijn dat twee zaken waar het bij mij altijd een beetje schort. Het domein verwijst namelijk gewoon naar de server van punkey.com en de vormgeving is natuurlijk ook nog niet af. En wat denk je nog meer? Ik schrijf dit niet eens op 1 januari! Nee, dit heb ik op oudjaarsdag geschreven omdat ik op 1 januari hoogstwaarschijnlijk alleen even uit bed zal komen om op Publish te klikken voor dit postje. </p><p><span style="font-weight: bold">What happened?</span><br />Nou ja, best wel een hoop en tegelijkertijd ook weer niet. Ik woon nog steeds in Utrecht met mijn meisje QT, werk nog steeds bij Rhinofly. Waar ik probeer de <a href="http://www.frank-ly.nl/"  target="_blank" target='_blank'>Maandagochtend Koffie Notities</a> van de grond te krijgen maar dat wil nog niet echt lukken. Ben nog steeds veel bij de nederlandse blogosfeer betrokken via <a href="http://www.aboutblank.nl/"  target="_blank" target='_blank'>About:Blank</a> en ben samen met wat andere businessbloggers allerlei projecten aan het opzetten. Een van de leukste projecten op dit moment is toch wel <a href="http://www.eventblogs.nl/"  target="_blank" target='_blank'>Eventblogs.nl</a> wat ik samen met Marco Derksen, Frank Janssen en mijn eigen QT het opgezet. Doel van Eventblogs.nl is om voor, tijdens en na conferenties, seminars en workshops via een weblog verslag te doen in verschillende vormen. Denk aan gewone posts, maar ook foto's en video. Op <a href="http://www.marketing3.nl/"  target="_blank" target='_blank'>Marketing3</a> in Utrecht afgelopen december hebben we de eerste echte job gedaan en het was een enorm succes. Het evenement heeft door de weblog zo'n 400% meer &quot;kijkers&quot; gehad. Je kunt discussieren over het businessmodel voor zo'n evenement en hoe deze extra &quot;kijkers&quot; nu eigenlijk gratis meeliften via het weblog. Maar de organisatie was tevreden, wij zijn tevreden en we strijken de kinderziektes nog glad. Om maar even wat gezegden door elkaar te halen.<br />Euhm...ja wat doe ik nog meer tegenwoordig...O ja, sinds gisteren heb ik ook een audiocolumn op de <a href="http://www.ecast.nl/"  target="_blank" target='_blank'>E-cast</a> van Elger van der Wel. In deze podcast geef ik elke twee weken mijn commentaar op gebeurtenissen in Internetland. Van gadgets tot nieuwe diensten en van software tot rechtszaken, ik zal overal wel een mening over hebben.</p><p><span style="font-weight: bold">Dutch Bloggies</span><br />Ik zal deze eerste SMTN niet te lang maken, we moeten er allemaal weer even aan wennen nietwaar? Maar ik wel even de aandacht vestigen op de <a href="http://www.dutchbloggies.nl/"  target="_blank" target='_blank'>Dutch Bloggies 2006</a> die vandaag zijn begonnen! Vanaf vandaag is het mogelijk om je favoriete blogs te nomineren voor deze prijs der prijzen in neerlands weblogland. Lees <a href="http://www.dutchbloggies.nl/faq.php"  target="_blank" target='_blank'>de regeltjes</a> even goed door, we hebben het natuurlijk allemaal weer net even anders gedaan dan vorige jaren. Belangrijkste regel: Je hebt na inschrijving je eigen nominatiepagina zodat je niet in 1 keer alle nominaties hoeft in te vullen. En het is niet verplicht om bij alle categorien 3 URL's in te vullen. Weet je bij een categorie maar 1 blog? Vul je die in en laat je de rest leeg. Geen probleem!</p><p><span style="font-weight: bold">Meer weten?</span><br />Ja, laten we het eens anders doen deze eerste SMTN. Roept u eens in de reacties waar ik het volgende keer over ga hebben. Waar ben je benieuwd naar wat ik de afgelopen maanden heb uitgespookt? Roep maar wat onderwerpen en ik kijk of ik er wat leuks uit kan halen voor de volgende editie! Euh trouwens, het emailen als er een reactie is werkt nog niet helemaal jofel, evenals het verbergen van het emailadres. Weet nog niet exact wat dat kan zijn, maar dat fixen we wel na 2 januari!</p> 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=_SMTN&amp;w=sunday_morning_teanotes">_SMTN</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=6986&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=6986&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m12.php#e6986" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=6986&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e6986"></span><span class="archief"><a href="/pivot/entry.php?id=6986&amp;w=sunday_morning_teanotes">31 December 2005:The Phoenix rises from the ashes</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>