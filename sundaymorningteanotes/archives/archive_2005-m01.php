<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1661"></span><div class="entry">
<h3><span class='date'>30 Januari 05 - 09&#58;55</span>Sunday morning teanotes</h3>

	<p>Gisteren naar het <a href="http://www.iffr.nl"  target='_blank'>IFFR</a> geweest, dus deze ochtend wat korte recensies van de films die ik heb gezien en het festival zelf! Live ingeklopt op de laptop, maar geen zin om via de hotspots van KPN de boel te posten a 5 euri per uur. Waarom hebben ze <a href="https://portal.hotspotsvankpn.com/portal/asp/laptop/connect/paymentSelection.aspx"  target='_blank'>geen WiFi minuten bundels</a> die je ter plekke met creditcard kan betalen?</p>
	<p><strong>Proloog</strong><br />
Ik heb zojuist de kaartjes opgehaald voor het filmfestival. Ik moet zeggen dat kopen via internet nu niet echt voordelen biedt. Ten eerste is het systeem veranderd t.o.v. vorig jaar. Kon je toen met een geprinte barcode zelf je kaartjes afhalen bij een mobiele, onbemande stand in de Doelen, nu moet je in de rij staan om je kaartjes te krijgen. En dat gaat niet altijd goed. De meeste internetbestellingen zijn schijnbaar niet goed verlopen (die van mij wel natuurlijk) en ik was dan ook van de weinigen, volgens de cassiere, bij wie de bestelling in 1 x klopt. <br />
Ik begrijp ook niet goed hoe een festival met 350.000 bezoekers (waarvan een groot deel overzees) dit nog niet op orde kan hebben? Waarom geen mobile payments via SMS? Of de zojuist beschreven barcode scanner weer terug halen? Het is ook een teken aan de wand dat een organisatie als Belbios het niet voor elkaar krijgt om de internetbestellingen en betalingen op orde te krijgen. Hoe kan dit toch? We lopen toch zo voor op de rest van wereld met internet technologie? We hebben toch slimme mensen? Ik kan wel wat bedenken: Budgetten en ego&#8217;s bij de organisatie. Alles moet maar gedaan worden in een te korte tijd voor te weinig geld. Er wordt niet voldoende geinvesteerd in servers, een Service Level Agreement? Ach dat is toch niet nodig. Als het maar gelikt uitziet. Dan is de afdeling Marketing tevreden. Stresstests? Nee hoor, hoeft niet. <br />
Tenminste, ik denk dat het zo is gegaan. Dat hoeft natuurlijk niet. Ik hoop het ook niet.</p>
	<p>Wat ik ook niet begrijp is waarom de hele stad bedolven moet zijn onder Festivalkrantjes? Waarom dat ook niet mobiel of online aanbieden (RSS?) ? Nog wel een leuke guerilla marketingactie gezien van twee studenten die de stad bestickerde met de URL <a href="http://www.filmfestivalruil.nl/gesponsord"  target='_blank'>Filmfestivalruil.nl</a> door Bavaria. Hier kun je je kaartjes ruilen en in het forum over film kletsen. De festivaldirectie was minder <em>amused</em> en alle stickers moesten worden verwijderd.</p>
	<h2>De films</h2>
	<p><strong>Le conseguenze dell&#8217;Amore</strong><br />
<a href="http://www.filmfestivalrotterdam.com/nl/film/30702.html"  target='_blank'>Weggezet</a> als de Ferrari onder de maffiafilms. Maar dan wel een met startproblemen. Want mijn god, wat een trage film. Er gebeurt bar weinig (zeg maar gerust <em>niets</em>). Het draait om een corrupte belastingconsulent die voor de maffia werkt. Hij brengt elke maand een koffer met een paar miljoen dollar naar de bank en hij zegt niets tegen de serveerster in het hotel waar hij al 24 jaar is opgesloten door diezelfde maffia. Ogenschijnlijk lijkt hij een rijke niksnut, maar langzaam blijkt dat dat dus niet het geval is. Suffe bedoening als je het mij vraagt. 2 sterren</p>
	<p><strong>Z Channel</strong><br />
Een &#8220;Wie kent hem niet&#8221; <a href="http://www.filmfestivalrotterdam.com/nl/film/30791.html"  target='_blank'>film</a> over Jerry Harvey, de legendarische programmeur van de Californische filmzender Z-Channel, dat voor veel filmliefhebbers de introductie in de wereldcinema betekende. Hij was beroemd om zijn keuze van films. de meest obscure films werden naast blockbusters geprogrammeerd. In 1988 schoot hij zijn vrouw dood en daarna zichzelf. In deze docu blikken vrienden en collegas terug op zijn leven. Inclusief &#8220;onze&#8221; Paul Verhoeven die in onvervalst steenkolen Engels (&#8220;How could you not follow de boek?&#8221;) over zijn meesterwerk Turks Fruit verhaalt, die door Z Channel is uitgezonden. Aardig, maar ik vraag me af wanneer een dergelijke film komt over de &#8220;dolly grip&#8221; of de &#8220;catering guy&#8221; met een terugblik op zijn leven. Wel veel tips gekregen voor films die ik (weer) eens moet zien. 3 sterren</p>
	<p><strong>The Edukators</strong><br />
Leuke onderhoudende <a href="http://www.filmfestivalrotterdam.com/nl/film/30733.html"  target='_blank'>film</a> over twee links-radicale vrienden en een vriendin die er tussen in staat. &#8217;s Nachts breken ze in bij villa&#8217;s, niet om iets te stelen, maar om te laten weten dat de eigenaar te veel geld heeft en het meubilair wat op zijn kop te zetten. Tijdens een van deze inbraken gaat het mis. Ze zijn genoodszaakt deze oude hippie-turned-businessman mee te nemen naar de Tiroler bergen. Daar speelt de man een slim spelletje en speelt ze tegen elkaar uit. Het einde is fenomenaal verrassend.<br />
Zeker de moeite waard, al was het alleen maar om de Jeff Buckley versie van Hallelujah die de laatste 10 minuten van de film als achtergrond dient. 4 sterren</p>
	<p><strong>Baghdad Blogger</strong><br />
Salam Pax <a href="http://www.filmfestivalrotterdam.com/nl/film/32898.html"  target='_blank'>beschrijft</a> uit een belegerd Baghdad zijn leven. Niet alleen op zijn <a href="http://dear_raed.blogspot.com/"  target='_blank'>weblog</a>, maar ook met een videocamera. Geef een weblogger geen videocamera, zo blijkt maar weer. Je ziet wat interessante beelden uit Baghdad en andere steden, je hoort wat van de andere kant van het nieuws, maar al met al is het erg vermoeiend om naar te kijken. 2 sterren.</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1661&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=1661&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m01.php#e1661" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1661&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1643"></span><div class="entry">
<h3><span class='date'>23 Januari 05 - 12&#58;44</span>Sunday morning teanotes</h3>

	<p>Het is al middag maar dat mag de pret niet drukken! Deze keer een korte SMTN omdat ik gewoon niet veel te vertellen heb. Wel genoeg te doen. Zo ga ik direct weer eens een stukje schrijven voor <a href="http://www.whatsthenextaction.com"  target='_blank'>mijn andere weblog</a>, ik moet een weblogster nog helpen met een presentatie over businessblogging, mijn meisje heeft RSS ontdekt, dus die moet ik ook wat tips geven ondertussen en ik heb een boek liggen waarvoor ik ben gevraagd om te recenseren. In non-PC gerelateerde zaken: Een stapel van plm 60 tijdschriften, twee cavia&#8217;s die moeten worden verschoond, de lijst met vakantiekosten afronden en wegstoppen en ik bedenk me nu dat er nog was in de wasmachine moet&#8230;.<br />
Dus u begrijpt dat ik nu even wat anders ga doen!</p>
	<p>O ja, vergeet niet 26 februari naar het Museum van Communicatie te komen. Op die dag is de uitreiking van de volgens velen meest overgewaardeerde prijzen, <a href="http://www.dutchbloggies.nl"  target='_blank'>de Dutch Bloggies</a>. Punkey.com is nergens voor genomineerd, <a href="http://www.frank-ly.nl"  target='_blank'>Frank-ly</a> wel. Dus hopelijk komt die nog in de lijst waar iedereen vanaf 1 februari op kan stemmen.</p>
	<p>Voor de geinteresseerde: Ik heb nog geen kaartjes voor het Fimmelfestival in Rotterdam. Balen&#8230;kom er gewoon niet door, zowel telefoon als Internet. Dus kom maar op met die verhalen in de comments dat je binnen 10 minuten 12 kaartjes had en amper hebt hoeven wachten en dat je via Internet perfect hebt kunnen bestellen. Pfffff&#8230;.</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1643&amp;w=sunday_morning_teanotes#comm" title="Mark">een reactie</a> /  <a href="/pivot/entry.php?id=1643&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m01.php#e1643" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1643&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1630"></span><div class="entry">
<h3><span class='date'>16 Januari 05 - 09&#58;10</span>Sunday morning teanotes</h3>

	<p>Mijn jetlag is nog niet helemaal over merk ik. Ik ben namelijk al sinds 7 uur weer op terwijl ik om half 1 vannacht wakker schrok op de bank omdat de aftiteling begon van &#8220;The talented Mr. Ripley&#8221;. Nu weet ik nog niet hoe het is afgelopen! Deze ochtend met name nog wat recente artikelen gelezen. Interessante ontwikkelingen uit de US wat betreft het makkelijker abonneren op RSS-feeds. Dave Winer heeft <a href="http://www.reallysimplesyndication.com/2005/01/12#a254"  target='_blank'>een voorstel gedaan</a> hoe dat geregeld kan worden. Probleem is namelijk dat op dit moment elke RSS-aanbieder aan leverancier-lockin doet. Kijk naar Yahoo. Je kunt via myYahoo je webfeeds bekijken. Op sites vind je nu dan ook de knop &#8220;Add to myYahoo&#8221; (zoals <a href="http://www.cnn.com/services/rss/"  target='_blank'>CNN</a>) maar daarmee zit je dus vast aan Yahoo. Als je je abonnementen uit Yahoo wil halen en in een ander programma, dan wordt het knap lastig. Tel daarbij op dat veel diensten dit aanbieden (Bloglines, Userland schieten me zo te binnen) en je begrijpt dat het een wirwar van buttons gaat worden op sites om iedereen te kunnen voorzien in een webfeed-abonnee button. Dus een oplossing lijkt me nodig. En in de oplossing van Winer kan ik me als techneut vinden. Als marketeer zeg ik: Nog steeds teveel gedoe. De meeste mensen weten niet eens wat RSS is. Het is een te technische term. Webfeeds is beter. Tijdens besprekingen met klanten zoek ik nog steeds naar de juiste definitie om het in 1 x uit te leggen. Pas als je het laat zien valt het kwartje maar ook dan is de logische opmerking: &#8220;Heb ik weer een programma nodig?&#8221; Het nut zien de meeste marketeers die ik spreek wel van webfeeds.<br />
In het voorstel gaat het van RSS ineens naar OPML. WTF? Je krijgt RSS al niet uitgelegd!<br />
Ik dwaal af. Nu is het zo dat alles wat Dave Winer zegt gelijktijdig met gejuich en afkeur wordt ontvangen. Dat maakt hem ook zo&#8217;n markant figuur vind ik. Blogdigger heeft nog <a href="http://www.blogdigger.com/blog/2005/01/14/1105716726000.html"  target='_blank'>een verdere uitleg</a> over het voorstel. </p>
	<p>Waarom zo weinig posts de laatste week? Nou, ik ben deze week weer aan het werk gegaan. Na een maand moet je er dan toch weer even &#8220;in komen&#8221;. Alle projecten weer oppikken, bijpraten met collega&#8217;s en mail wegwerken. &#8217;s Avonds had ik dan gewoon weinig zin om nog heel actief achter de PC te gaan zitten. Dat komt wel weer merk ik, dus verwacht de komende tijd weer wat meer updates</p>
	<p>Een van de onderwerpen waar ik over wil schrijven is <a href="http://www.danbrown.com"  target='_blank'>Dan Brown</a>. Tijdens de vakantie heb ik drie boeken van hem gelezen waaronder uiteraard &#8220;Da Vinci Code&#8221;. Een geweldig boek! Maar als je zijn andere boeken ook leest gaat er toch wat knagen. Daarover later deze week meer.<br />
Verwacht ook snel weer een nieuwe doorstart van <a href="http://www.whatsthenextaction.com"  target='_blank'>mijn Getting Things Done-weblog</a> en -pogingen. Ik merk dat door de vakantie veel is weggevallen en dat je snel terugkeert in oude gewoonten. Slecht slecht slecht!</p>
	<p>Podcasten gaat ook gewoon door heb ik gemerkt. Ik heb in mijn nieuwe Creative Zen Touch, waarover ik ook nog wat ga schrijven, dagelijks mijn uitzendingen zitten van onder andere <a href="http://www.covercille.com"  target='_blank'>Coverville</a>, <a href="http://hardcoreinsomniaradio.blogspot.com"  target='_blank'>Hardcore Insomnia Radio</a> en <a href="http://www.Indiefeed.com"  target='_blank'>Indiefeed.com</a>. Even geen techietalk meer van The Dailysourcecode of andere pioniers van het podcasten. Het grootste probleem bij de meeste podcasts blijft echter: Normaliseer je geluid! Tenminste, als dat zo heet. Want vaak is de muziek snoeihard en het commentaar tussendoor te zacht of andersom. <br />
Zou podcasten iets zijn voor <a href="http://www.frshdjs.com"  target='_blank'>frshdjs.com</a>? Is er een behoefte aan wekelijkse of in elk geval periodieke uitzendingen met bijv gast-DJ&#8217;s? Check <a href="http://www.libsyn.com/index.php?&#38;mode=logout&#38;message"  target='_blank'>www.libsyn.com</a>= voor informatie over makkelijk podcasts maken, opslaan en distribueren. Ik heb nog geen Nederlands initiatief gezien op dat vlak dus frshdjs.com zou wel eens kunnen pionieren hiermee!</p>
	<p>Dat waren de teanotes voor de ochtend. Deze ochtend trouwens <a href="http://www.celestialseasonings.com/products/herb/lz.php"  target='_blank'>Celestial Seasonings Lemon Zinger</a>. Zou ik niet eens gesponsord moeten worden hiervoor? Vanmiddag even rocken bij <a href="http://www.mezz.nl/jointhemusic/nieuws/news_article.asp?articleid=151"  target='_blank'>de Gecko Bros. in de Mezz</a>. En vanaf morgen gaan we weer gezellig posten hier!<br />
<p style="text-align:center;"><img src="/images/thee.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p>


  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1630&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=1630&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m01.php#e1630" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1630&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1621"></span><div class="entry">
<h3><span class='date'>09 Januari 05 - 09&#58;25</span>Sunday morning teanotes</h3>

	<p>Daar is hij weer. De vertrouwde SMTN op de zondagochtend. De afgelopen twee dagen hebben we wat uitgerust van de vakantie (da&#8217;s gek&#8230; ) en de boel opgeruimd. Dat betekent praktisch dat het huis nu vol ligt met gadgets, gekochte spullen, vuile en schone was, achterstallige post (<a href="http://www.pivothosting.net"  target='_blank'>Bob</a>, de rekening wordt zsm betaald!) en tijdschriften. Maar onze hoofden zitten vol met herinneringen en het plezier van de afgelopen maand. Het lijkt al weer zo&#8217;n tijd geleden dat we de reuzenbomen in het Kauri-woud hebben gezien, terwijl dat een week geleden is. Nog geen 4 dagen geleden stonden we in Auckland vertwijfeld om ons te heen kijken: &#8220;Wat doen we hier?&#8221;<br />
Dat komt met name door de terugreis denk ik. Door dat we een wat ongelukkige aansluiting hadden in Seoul, hebben we 8 uur moeten wachten. En 8 uur op een vliegveld vol Aziaten? Je wilt het niet. Je wilt het <em>zo</em> enorm niet. Eenmaal in Frankfurt kregen we meteen weer in de gaten dat we in Europa waren. Een broodje en wat frisdrank was even duur als in Nieuw Zeeland, echter, de valuta verschilde (Euro:NZ Dollar = 2:1) In NZ is het leven echt goedkoper. Niet alleen het eten en verblijf, maar ook bijvoorbeeld benzine. Een liter loodvrij kost daar $1,09. Dat is met de huidige koers zo&#8217;n 50 eurocent per liter. <br />
Hier staat wel tegenover dat de salarissen in NZ vele malen lager liggen dan in Nederland. We spraken een chefkok die 17 dollar per uur verdient!<br />
Een doel van de vakantie was ook om eens te kijken of het land is waar we willen wonen. Helie is 7 jaar geleden al een halfjaar in NZ geweest voor haar studie en sindsdien heeft ze altijd de wens gehad om nog eens terug te gaan.<br />
We hebben gekeken naar huizenprijzen en andere omstandigheden. Ook de huizen zijn nog goedkoop. Voor hetzelfde bedrag als we nu een klein huis in Utrecht hebben gekocht, kunnen we ook in Kaikoura in een 4-kamer huis zitten, in de bergen, met zee-uitzicht. Dit blijft echter niet zo. De Amerikanen zijn als gekken alles aan het opkopen en bieden absurde bedragen voor huizen en grond. Met als gevolg dat voor de &#8220;gewone man&#8221; een eigen huis steeds meer onbereikbaar wordt. <br />
Tijdens Kerstmis hebben we de oude huisbazin van Helie bezocht en zijn we uitgenodigd om kerst mee te vieren. De groep mensen die ik toen heb ontmoet is een van mijn hoogtepunten van de vakantie. Nu eens niet een gesprek met backpackers over het vorige en volgende hostel, de dolfijnen en het bungeejumpen (niets mis mee, maar toch&#8230; ) maar met een groep mensen die daadwerkelijk iets betekenen voor het land. Ann, de huisbazin, is persoonlijk verantwoordelijk geweest voor een verregaande integratie in de schoolsystemen van Maori en <a href="http://maorinews.com/writings/papers/other/pakeha.htm"  target='_blank'>Pakeha</a> (blanke NZ-ers). Philip, haar vriend, heeft een goedlopend PR bureau in Wellington, Kat is een lerares Duits in Wellington en Mary is op universitair bezig geweest met het Whaitangi-tribunaal en heeft diverse publicaties op haar naam staan over o.a. de aardbeving in Napier van 1931.<br />
Met deze groep mensen hebben we uitgebreid gesproken. Natuurlijk ook over vakantie en de luchtige zaken, maar ook over de situatie in Nederland op dit moment, de mislukte integratie, de moord op Theo van Gogh en Pim Fortuyn, het EU beleid etc. Daarnaast hebben zij veel verteld over de stand van zaken in NZ. Hoe zij met de vreemdelingenstroom uit Azie omgaan, hoe door de US alle prijzen omhoog gaan, maar de lonen bevriezen, wat de rol van de Maori is in het huidige NZ. Een erg boeiende en interessante dag die mijn kijk op het land toch wel heeft bijgesteld.<br />
Een paar dagen later kwamen we in een hostel in Whangarei een groep Kiwi&#8217;s (NZ-ers) tegen die actief zijn in de jongerenpolitiek en de situatie voor jonge Maori&#8217;s en kiwi&#8217;s. Zij bevestigde de verhalen die we eerder hadden gehoord en gaven er ook hun kijk op. Het heeft mij in elk geval aan het denken gezet over de verhoudingen tussen Nieuw-Zeeland en Nederland. </p>
	<p>Toen we terugkwamen in Nederland was het al avond, maar de ochtend er op zag ik meteen weer waarom ik zo graag op vakantie wilde. Een grauwe grijze deken in de lucht, guur, kou. Toen we die middag boodschappen gingen doen voelden we ook direct de verschillen tussen de mensen van NZ en Nederland. Is men in NZ bij voorkeur open en vriendelijk, hier merkten we meteen de geslotenheid van de mensen. Iedereen doet zijn ding, kijkt niet naar elkaar, let niet op elkaar, maar is vooral met zichzelf bezig. Een vrouw met kinderwagen die haar boodschappen inpakt en ziet dat Helie er langs wilt met een winkelwagen, verschuift haar kinderwagen zo dat Helie er nog niet langs kan. Een vriendelijke blik van Helie met de vraag of ze er alsnog bij mag wordt afgedaan met een snauw en een verwarde blik van de moeder. Het is de uitstraling die Nederlanders hebben. We herkenden Nederlanders al van 25 meter afstand in NZ. Zonder ze te horen, alleen maar de aanblik. Ik denk dat we 5% van de pogingen fout hadden. Dan waren het Duitsers. </p>
	<p>Wat is er nog meer over NZ te vertellen? Veel. Veel te veel. Het is een mooi land, prachtige natuur, aardige mensen, geweldige cultuur en zeker het bezoeken waard. <br />
We willen ook iedereen bedanken voor de reacties op onze stukjes en de SMS-jes tijdens Oud en Nieuw! We vragen ons alleen af waarom twee foto&#8217;s in het album zo vaak zijn bekeken, namelijk <a href="http://www.punkey.com/coppermine/displayimage.php?album=9&#38;pos=2"  target='_blank'>deze</a> en <a href="http://www.punkey.com/coppermine/displayimage.php?album=9&#38;pos=10"  target='_blank'>deze</a>. Kan iemand daar een antwoord op geven?<br />
De komende tijd zal ik nog eens wat tips en links bij elkaar zetten over onze vakantie. Evenals natuurlijk de foto&#8217;s en de video&#8217;s die we hebben gemaakt. Maar die moet ik eerst op een afspeelbare DVD zien te krijgen. Want daar ben ik dus ook al een avond en een ochtend mee bezig en het wil maar niet lukken. </p>
	<p>Vanaf vandaag, nou ja morgen denk ik, dus weer een gewone weblog!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1621&amp;w=sunday_morning_teanotes#comm" title="cyclone, N, tom">drie reacties</a> /  <a href="/pivot/entry.php?id=1621&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m01.php#e1621" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1621&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1661"></span><span class="archief"><a href="/pivot/entry.php?id=1661&amp;w=sunday_morning_teanotes">30 Januari 2005:</a><br></span>
<span id="e1643"></span><span class="archief"><a href="/pivot/entry.php?id=1643&amp;w=sunday_morning_teanotes">23 Januari 2005:</a><br></span>
<span id="e1630"></span><span class="archief"><a href="/pivot/entry.php?id=1630&amp;w=sunday_morning_teanotes">16 Januari 2005:</a><br></span>
<span id="e1621"></span><span class="archief"><a href="/pivot/entry.php?id=1621&amp;w=sunday_morning_teanotes">09 Januari 2005:</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>