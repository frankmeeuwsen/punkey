<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1718"></span><div class="entry">
<h3><span class='date'>27 Februari 05 - 09&#58;50</span>Sunday morning teanotes</h3>

	<p>Ahhh&#8230;..het is weer zondag, tijd voor een onvervalste SMTN. Je kunt waarschijnlijk wel raden waar we het deze ochtend over hebben&#8230;</p>
	<p><strong>Dutch Bloggies 2005</strong><br />
Gisteren was de uitreiking van de <a href="http://www.aboutblank.nl/dutchbloggies/2005/"  target='_blank'>Dutch Bloggies 2005</a> in het <a href="http://www.muscom.nl"  target='_blank'>Museum voor Communicatie</a> te Den Haag. Dit jaar een grotere opkomst dan vorig jaar. Er was zo&#8217;n 80-100 man aanwezig. En ja, voornamelijk man, weinig vrouw. Erg jammer, waar blijven die vrouwelijke loggers nu in grote getale?<br />
De uitreiking werd voorgezeten door ondergetekende, die op van de zenuwen zijn entree maakte. Mijn introductie speech viel dan ook behoorlijk in het water door die zenuwen. Hopelijk was het niet te irritant om naar te luisteren. <br />
De grote winnaar en tevens grote afwezige was Geenstijl. Ze hebben 10 beeldjes gewonnen, inclusief de vakjury prijs voor beste weblog. Geheel in stijl zijn ze de prijzen niet komen ophalen, gelukkig zijn andere bloggers zo aardig geweest de beeldjes te jatten en <a href="http://www.marktplaats.nl/index.php3?g=antiek&#38;u=antiekdiversen&#38;ID=425261"  target='_blank'>te koop aan te bieden</a>. Strakke actie!<br />
De hele dag was erg prettig, veel oude bekenden, een flink aantal nieuwe gezichten en er werd aardig gepraat en gelachen over de Dutch Bloggies en weblogs in het algemeen. Een (letterlijk) erg grote verrassing was de taart die vlak voor aanvang werd binnengedragen namens <a href="http://www.punt.nl/"  target='_blank'>punt.nl</a>. Bedankt gasten, lekker bij het bier!<br />
Er werden niet alleen prijzen uitgereikt, <a href="http://www.mennonicolai.nl/"  target='_blank'>Menno Nicolai</a> vertelde over zijn leven als cabaretier en <a href="http://www.frackers.com"  target='_blank'>Michiel Frackers</a> legt uit hoe hij als nietskunnende drop out doet wat hij leuk vindt en dus geld verdient als water. En passant vertelde hij ook dat zijn weblog is verkocht aan VNU. Goede verhalen!<br />
De gasten van <a href="http://www.quotenet.nl/2005/02/26/220007.html"  target='_blank'>Quote</a>, met Jort vooraan, zorgde nog voor wat consternatie door bij de uitreiking van Best Company Weblog de beamer van tafel te kieperen. Hij heeft het overleefd. De beamer ook.<br />
Wat me opviel was dat na de uitreiking veel gesprekken gingen over &#8220;geld verdienen met weblogs&#8221;. Misschien was het door het verhaal van Michiel en Jort, misschien door de businessblogges die druk in gesprek waren met mensen als Retecool en MKT. Maar dat viel me op. er gebeuren dingen. Ook naderhand, toen we met een select gezelschap nog wat zijn gaan drinken, vlogen de businessplannen over tafel. En allemaal niet eens zo slecht. Ik denk dat 2005 een mooi blogjaar gaat worden!<br />
Ik wil nog een opmerking maken over een reactie die ik bij <a href="http://www.mediafact.nl/comments.php?id=7570_0_1_0_C"  target='_blank'>Marketingfacts</a> mocht lezen. Evert Jan van <a href="http://www.vue-royale.nl/"  target='_blank'>Vue-Royale</a> vind de uitreiking een farce en wil een concurrent voor de prijzen en &#8220;het lachertje van&#8221; de organisatie. Bij deze wil ik Evert Jan uitnodigen om niet met een concurrent te komen, maar samen met de organisatie te kijken hoe we de Dutch Bloggies 2006 naar een hoger niveau te tillen. Ik spreek namens de voltallige a:b-redactie als ik zeg dat ook wij niet tevreden zijn met de manier waarop de nominatie- en stemronde inhoudelijk is verlopen. Hier moeten we een verandering in brengen en dat gaan we ook doen. Echter, het laatste waar we op zitten te wachten, is een alternatieve jurering en prijzenfestijn voor weblogs. Laten we dan de krachten bundelen en kijken hoe we een gedegen platform kunnen wegzetten. Dan kunnen we namelijk echt gaan spreken van toonaangevende prijzen.</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1718&amp;w=sunday_morning_teanotes#comm" title="peter, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1718&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m02.php#e1718" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1718&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1709"></span><div class="entry">
<h3><span class='date'>20 Februari 05 - 11&#58;05</span>Sunday morning teanotes</h3>

	<p>Aangezien ik maar weinig meemaak en weinig te melden heb, deze week even geen SMTN!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1709&amp;w=sunday_morning_teanotes#comm" title="Wilma Slinger, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1709&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m02.php#e1709" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1709&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1699"></span><div class="entry">
<h3><span class='date'>13 Februari 05 - 10&#58;09</span>Sunday morning teanotes</h3>

	<p>Een barre en boze zondag. Mooi weer om lekker de hele dag thuis achter de PC te gaan zitten <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/> En dat gaat dus ook gebeuren. Ik heb nog een hoop te doen voor mijn werk. Het is nogal een heksenketel de laatste tijd met grote opdrachten waardoor een en ander wel eens ondersneeuwt. Tja, dan moet je soms even een dagje opgeven om de rest van de week nog enigszins kalm verder te werken. <br />
Verder verwacht ik een lekkerre relaxte zondag. Toch, eigenlijk wel, ja&#8230;</p>
	<p><strong>Banner</strong><br />
Jullie hebben de Buutvrij-banner hopelijk al gezien. Ik ben blij met alle reacties die er op komen. Not! Kom op! Waar zijn die praatgrage, altijd-een-mening-hebbende lezers nou? Ik ben serieus geinteresseerd in jullie mening, dus laat maar horen!</p>
	<p><strong>Podcasting</strong><br />
Afgelopen week heb ik een auto tot mijn beschikking gehad en ik heb het podcasten weer herontdekt. Ik was het wat beu omdat ik in de trein ook vaak nog wat zit te lezen. En gelijktijdig naar een talkshow luisteren zit er dan niet echt in. Uitzondering is natuurlijk <a href="http://www.coverville.com"  target='_blank'>Coverville</a> en <a href="http://hardcoreinsomniaradio.blogspot.com/"  target='_blank'>Hardcore Insomnia</a>. Maar in de auto kan ik op mijn gemak naar de uitmuntende gesprekken luisten van Cam en Mick op <a href="http://www.gdayworld.com/podcast/"  target='_blank'>G&#8217;Day World</a>. Zo heb ik vrijdag naar een gesprek met <a href="http://blogs.officezealot.com/marc/"  target='_blank'>Marc Orchant</a>. Hij is een <a href="http://office.weblogsinc.com/"  target='_blank'>Office-_geek_</a>, <a href="http://tabletpcs.weblogsinc.com/"  target='_blank'>Tablet PC-_evangelist_</a> en hardcore <a href="http://www.officezealot.com/GTD/GTDwelcome.aspx"  target='_blank'>GTD-_practicioner_</a>. Gevolg was een erg leuk gesprek over Office applicaties, Tablet PC&#8217;s en andere hardware. Ja erg geeky, maar dat is nu eenmaal mijn voorkeur <img src='/extensions/emoticons/trillian/e_121.gif' alt=';-)' align='middle'/> </p>
	<p><strong>Spin Awards</strong><br />
Nog zo&#8217;n mooie. <a href="http://www.spinawards.nl"  target='_blank'>De Spin Awards</a>. Niet alleen zijn we als Rhinofly mediapartner van de Awards (Via <a href="http://www.frank-ly.nl"  target='_blank'>Frank-ly</a>), maar natuurlijk doen we ook mee. En het aanmelden en invoeren van de cases ligt nu al weken op mijn bureau te wachten om te doen. Maar het lukt maar niet. Dus nu moet ik nog voor dinsdag middernacht de boel ingevoerd krijgen zodat we kunnen meedingen voor deze prestigieuze (nou ja, in interactief marketing-land) award.</p>
	<p><strong>Dutch Bloggies</strong><br />
Minstens zo prestigieus maar wat kleiner van opzet zijn de <a href="http://www.dutchbloggies.nl"  target='_blank'>Dutch Bloggies</a>. Het mag geen geheim zijn dat ik in de organisatie zit en me dus al bezig hou met de uitreiking op 26 februari in Den Haag. Heb je je stem nog niet uitgebracht? Doe dat dan alsnog. Het kan nog tot dinsdag!</p>
	<p><strong>Eternal Sunshine of the Spotless Mind</strong><br />
<img src="/images/eternal.jpg" style="float:left;margin-right:10px;margin-bottom:5px;border:0px solid" title="" alt="" class="pivot-image" />Gisterenavond hebben we deze magistrale film gekeken [<a href="http://www.lacunainc.com/"  target='_blank'>Geweldige URL!</a>]. Wat een briljant verhaal. Dit is de eerste keer dat ik Jim Carrey in een rol zie waarbij ik niet denk: &#8220;Hey, da&#8217;s Jim Carrey&#8221;. Prachtig gefilmd, subtiele effecten, <em>spot-on</em> muziek en een cast om te smullen. Kate Winslet is in Titanic een tutje, maar hier zet ze een geweldige Clementine weg, &#8220;<em>just a fucked up girl who is looking for her own peace of mind</em>&#8221;<br />
De schrijver is Charlie Kaufmann, bekend van Being John Malkovich, Adaptation en Confessions of a dangerous mind. De regisseur is Michael Gondry, bekend van videoclips van Chemical Brothers en Bjork. Een wereldcombinatie. Een wereldfilm. Zoals Flo al <a href="http://www.punkey.com/pivot/entry.php?id=1673#flo-0502041000"  target='_blank'>eerder schreef</a> in de comments: Hij is prachtig verrassend. En wat lijkt het me geweldig om de mok te hebben met de foto van Clementine <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/></p>
	<p>Korte SMTN deze keer. Ik heb zoals gezegd nog genoeg te doen en wil nog wel wat aan mijn zondag overhouden!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1699&amp;w=sunday_morning_teanotes#comm" title="Mark Green, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1699&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m02.php#e1699" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1699&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1679"></span><div class="entry">
<h3><span class='date'>06 Februari 05 - 10&#58;39</span>Sunday morning teanotes</h3>

	<p>Carnavalszondag! Jawel, in het zuiden is het volksfeest weer losgebarsten en ik kan toch niet thuis blijven zitten. Ondanks eerdere beloften aan mezelf dat ik dit jaar even oversla, heb ik al weer twee dagen met een biertje in de hand op feestjes gestaan. Vanmiddag gaan we <em>toch</em> traditiegetrouw naar de <a href="http://www.boemeldonck.nl/Activiteiten/Met_Karnaval/GroteOptocht/Wie.html"  target='_blank'>Beekse Optocht</a>. Bij de ouders van <a href="http://www.djdazzle.com/"  target='_blank'>DJ Dazzle</a> hebben we altijd een perfect uitzicht vanaf het balkon. Maar vanavond op tijd thuis, want ik heb geen vrije dagen opgenomen. Dus gewoon werken morgen&#8230;</p>
	<p><strong>Dutch Bloggies</strong><br />
Ik wil jullie er aan herinneren dat je nog een week kunt stemmen bij de <a href="http://www.dutchbloggies.nl"  target='_blank'>Dutch Bloggies</a>. Helaas niet voor deze weblog, maar wel voor <a href="http://www.frank-ly.nl"  target='_blank'>Frank-ly</a>, de weblog die we bij het corporate HQ onderhouden. En trouwens, volgend jaar vind ik dat <a href="http://www.digidiary.co.uk/"  target='_blank'>Digidiary</a> ook moet worden genomineerd. Reina schrijft onwijs leuk over haar verblijf in Londen, waar ze haar PhD gaat halen.<br />
We zijn op de achtergrond druk bezig met de voorbereidingen. Ik kan al wel zeggen dat we wat verrassingen in petto hebben. Komt allen! Toegang is gratis.</p>
	<p><strong>punkey.com</strong><br />
Mja, deez&#8217; weblog. Ik zit de laatste weken te denken wat ik er nu eigenlijk mee wil. Ik heb het gevoel dat het een stuurloos schip is geworden. Veelal drop ik hier wat links van flauwe sites etc, maar ik heb niet de ambitie om een tweede Flabber of Jaggle te worden. Waarom zou ik? Zo nu en dan post ik wat over RSS of over marketing, maar daar heb ik ook Frank-ly al voor en sinds kort post ik eens wat op <a href="http://www.Marketingfacts.nl"  target='_blank'>Marketingfacts</a>.<br />
Lijfloggen is so wie so niet aan mij besteed, dus ja? Wat blijft er dan over? Dit jaar is mijn jubileumblogjaar, ik moet er nog eens over nadenken of ik er mee doorga. Misschien vind ik nog wel een richting die ik op wil, misschien heeft iemand van jullie een suggestie?</p>
	<p>Ik ga me weer in mijn <a href="http://www.punkey.com/coppermine/displayimage.php?album=10&#38;pos=0"  target='_blank'>Elvispak</a> hijsen. Tot later!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1679&amp;w=sunday_morning_teanotes#comm" title="Roh!, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1679&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m02.php#e1679" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1679&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1718"></span><span class="archief"><a href="/pivot/entry.php?id=1718&amp;w=sunday_morning_teanotes">27 Februari 2005:Dutch Bloggies</a><br></span>
<span id="e1709"></span><span class="archief"><a href="/pivot/entry.php?id=1709&amp;w=sunday_morning_teanotes">20 Februari 2005:</a><br></span>
<span id="e1699"></span><span class="archief"><a href="/pivot/entry.php?id=1699&amp;w=sunday_morning_teanotes">13 Februari 2005:</a><br></span>
<span id="e1679"></span><span class="archief"><a href="/pivot/entry.php?id=1679&amp;w=sunday_morning_teanotes">06 Februari 2005:</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>