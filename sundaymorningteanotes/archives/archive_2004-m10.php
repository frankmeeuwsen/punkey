<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1523"></span><div class="entry">
<h3><span class='date'>31 Oktober 04 - 11&#58;36</span>Sunday morning teanotes</h3>

Nou ja, het is bijna zondagmiddag, maar toch...na een nachtje chatten met de <a href="http://www.weblogqueens.nl/"  title="" target='_blank'>Weblogqueens</a> (ze zijn bijna klaar met de recordpoging!) even uitgeslapen en rustig ontbeten. Er is de afgelopen week een hoop gebeurd in logland als ik zo eens terugdenk. Natuurlijk <a href="http://omroep.vara.nl/tvradiointernet_detail.jsp?maintopic=424&detail=94355&subtopic=823"  title="" target='_blank'>de uitzending van B&W</a> met <a href="http://www.Geenstijl.nl"  title="" target='_blank'>Geenstijl.nl</a> en <a href="http://www.2525.com"  title="" target='_blank'>Fransisco van Jole</a>. Ik sprak vannacht Markus van <a href="http://www.moslam.nl"  title="" target='_blank'>moslam.nl</a> die bij de uitzending was. Het schijnt dat na de uitzending nog aardig is doorgeborreld en dat er ook wat verdere (commerciele) afspraken zijn gemaakt. Geenstijl heeft stijlloze boodschappen van Speurders.nl staan en je kunt er donder op zeggen dat deze snel ook elders zijn te vinden. Ik vraag me alleen af of ze werken. Heeft Speurders.nl werkelijk meer omzet door de inzet van banners en boodschappen op deze shocklogs? Of gaat het ze nu alleen om maar boven Marktplaats.nl te komen? Ik vermoed het laatste maar zie graag het tegendeel. <br />
De komende week kun je ook onze corporate weblog <a href="http://www.frank-ly.nl"  title="" target='_blank'>Frank-ly</a> bijhouden. Een aantal collega's zijn op <a href="http://www.macromedia.com/macromedia/events/max/"  title="" target='_blank'>Max2004</a>, de Macromedia Developers Conference in New Orleans. Gewapend met laptops en videocamera's zullen ze niet alleen in woord maar hopelijk ook in beeld verslag doen van deze conferentie. Natuurlijk is er een <a href="http://feeds.feedburner.com/Max2004"  title="" target='_blank'>aparte RSS feed</a> voor deze conferentie!<br />
Zoals ik al eerder heb geschreven is 10 november de eerste <a href="http://www.rhinofly.nl/frank-ly/index.cfm?fuseaction=frankly.view&FranklyId=0591DF1E-012F-46B2-B67E666CE186A648"  title="" target='_blank'>BusinessBlogMeeting</a>. Deze is invitation only en op dit moment zitten we ook vol. Wie worden er voor uitgenodigd? Met deze eerste meeting alleen weblogs die zich bezighouden met (online) marketing, zoekmachinemarketing en reclame. De reden? Om elkaar te ontmoeten maar ook om van gedachten te wisselen over genoemde onderwerpen. Denk aan de toekomst (en mogelijkheden) van zakelijke weblogs, RSS advertising, maar ook de advertentiemodellen die Speurders.nl nu heeft bij Geenstijl. <br />
Op 25 november kun je me vinden op het podium van De Rode Hoed in Amsterdam. Ik zal dan samen met Michiel Frackers (ex-Planet Internet, nu Frackers.com) en Marco Derksen (Marketingfacts.nl) spreken over Business Blogging op het <a href="http://www.emerce.nl/artikel.jsp?id=379027"  title="" target='_blank'>Emerce Outlook 2005</a> evenement. <br />
Op 1 en 2 december kun je me ook vinden op <a href="http://www.marketing3.nl/"  title="" target='_blank'>Marketing3</a>, het jaarlijkse seminar over interactieve marketing. Ik moet me nog naar binnen lullen voor 30 november (invitation only) dus als iemand nog connecties heeft...<br />
Verder heb ik veel, heel veel leesvoer voor de komende tijd. Mijn <a href="http://www.furl.net/search?enc=UTF-8&search=browse&id=punkey&sort=&dir=&pos=&keyword=&search=Search&category=133339&date=0"  title="" target='_blank'>Furl-archief</a> puilt uit, mijn <a href="http://www.bloglines.com/public/punkey"  title="" target='_blank'>RSS feeds</a> blijven binnenstromen en ik heb nog een stapel boeken liggen die ik wil lezen. Verder ben ik nog steeds bezig met een nieuw project (non-internet!) en ik moet nog wat voor <a href="http://www.aboutblank.nl"  title="" target='_blank'>About:Blank</a> schrijven! Ik ben blij dat ik over een maand op vakantie ga! 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1523&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=1523&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m10.php#e1523" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1523&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1500"></span><div class="entry">
<h3><span class='date'>24 Oktober 04 - 11&#58;11</span>Sunday morning teanotes</h3>

Goedemorgen happy campers. Het is weer zondag, ik zit te chillen achter mijn PC. Wat mail en nieuws door te nemen. Gisteren naar de i-Trendz beurs geweest wat niet zo'n denderende ervaring was zoals je al hebt kunnen lezen. Ik had daarna nog een afspraak met iemand. Die had pas een gadget! Check het uit<br />
<p style="text-align:center;"><a href="http://www.punkey.com/images/afbeelding516.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9hZmJlZWxkaW5nNTE2LmpwZw%3D%3D&amp;w=640&amp;h=480&amp;t=','imagewindow','width=640,height=480,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'><img src="http://www.punkey.com/images/afbeelding516.thumb.jpg" border="1" alt="" title=""  class='pivot-popupimage'/></a></p><br />
<br />
Een Pimp-watch met coole blingbling lampjes die de tijd aangeven! Nog een close-up foto<br />
<p style="text-align:center;"><a href="http://www.punkey.com/images/afbeelding514.jpg"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9hZmJlZWxkaW5nNTE0LmpwZw%3D%3D&amp;w=640&amp;h=480&amp;t=','imagewindow','width=640,height=480,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'><img src="http://www.punkey.com/images/afbeelding514.thumb.jpg" border="1" alt="" title=""  class='pivot-popupimage'/></a></p><br />
<br />
Deze keer wat langere stukken in de teanotes. Ben benieuwd wat jullie er van vinden. Reacties zijn altijd welkom!<br />
<br />
<b>Slogger</b><br />
Anyway, wat ik jullie nog even wil meegeven is de volgende tip: Als je gebruik maakt <a href="http://desktop.google.com/"  title="" target='_blank'>Google Desktop Search</a> zijn er een paar "probleempjes"<br />
1. Je kunt niet in PDF's zoeken<br />
2. Je kunt niet op netwerk-schijven zoeken<br />
3. Je kunt niet in de Firefox cache zoeken. <a href="http://desktop.google.com/support/bin/answer.py?answer=10135&amp;amp;topic=168"  title="" target='_blank'>Hun FAQ</a> geeft dat al aan<br />
<br />
De eerste twee heb ik geen antwoord op, maar de derde wel. Als je, net als ik , een fervent Firefox gebruiker bent, zal je cache niet gelezen worden via GDS. En dat is naar. Maar de oplossing is er! In de vorm van de extensie <a href="http://www.kenschutte.com/firefoxext/"  title="" target='_blank'>Slogger</a>. Deze installeert zich netjes in je menubalk en in de default settings, maakt een cache op je harddisk die weer door GDS gelezen kan worden. Ik gebruik het nu een dag of 4 en ik moet zeggen, het werkt erg prettig!<br />
Ik heb de settings van Sloger wel veranderd, omdat ik niet per se de HELE cache hoef te doorzoeken. Voor mijn werk kom ik vaak artikelen en websites tegen die ik later nog eens wil lezen. Via Slogger maak een copie van de pagina op mijn harddisk en ik kan het later (offline) op mijn gemak doorlezen. Slogger maakt automagisch een index-pagina met tijd, titel en link naar de lokale copie. Het mooie is dat in een volgende versie de mogelijkheid komt om RSS-feeds van deze lijst te maken. Om dit alles te doen moet je de settings van Slogger wel wat tweaken. Wil je er echt mee aan de slag, dan raad ik je aan in elk geval de help-file goed door te nemen. <br />
<br />
<b>Omea</b><br />
Waarom vind ik het prettig als er RSS-feeds van gemaakt worden? Omdat ik ook een ander programma aan het evalueren ben. Omea Pro. Dit is een newsreader "on steroids". Want je kunt niet alleen je RSS-feeds bijhouden, maar in het programma kun je ook je Outlook mail. taken en contactpersonen indexeren, nieuwsgroepen lezen en bestanden beheren.  Het is zoals zij zelf noemen een <i>Integrated Information Environment</i>. Het boeiende is, dat je verschillende workspaces kunt maken waarin je uit de grote berg infomratie op je PC de juiste mappen en feeds kiest om in de gaten te houden. Bijvoorbeeld een workspace "Marketing" waarin ik alle marketingfeeds volg, maar waar ook marketinginformatie vanaf een harddisk is te vinden (plannen, huisstijlhandboek, illustraties etc). Door ook bronnen weer juist te categoriseren IN die workspace, creeer je voor alle informatie een context. <br />
Zoals gezegd, ik ben het nog aan het evalueren, dus ik kan het nog wat lastig uitleggen. Als je nieuwsgierig bent geworden kan ik je aanraden <a href="http://www.jetbrains.com/omea/features/index.html"  title="" target='_blank'>het programma</a> eens te proberen. Tot 1/1/2005 krijg je een gratis licentie.<br />
<br />
<b>Getting Things Done</b><br />
OK, laatste tip op de zondagochtend. Heb je ook problemen om al het werk wat op je afkomt op een juiste manier af te handelen? Heb je het idee dat er "open loops" in je hoofd zitten met klusjes die je maar niet afgesloten krijgt? Dan kan ik je aanraden het boek "Getting Things Done" van David Allen te lezen (<a href="http://www.davidco.com/"  title="" target='_blank'>website</a>). Hij beschrijft in 250 pagina's welke simpele stappen je kunt nemen om je hoofd vrij en leeg te krijgen van al die openstaande klusjes. En ze tegelijkertijd wel te doen! Als je volgens het mantra "Collect, Process, Organize, Review, Do" werkt en je zelf steeds de vraag stelt "Wat is de volgende stap?" ben je al een heel eind. Het boek heeft op internet een soort van <a href="http://merlin.blogs.com/43folders/2004/09/getting_started.html"  title="" target='_blank'>cult-status</a> <a href="http://www.minezone.org/wiki/MVance/GettingThingsDone"  title="" target='_blank'>gekregen</a> en ik begrijp wel waarom. Het is een wat geeky, binaire benadering van je projecten en je to-do's. Maar wel een die ook echt werkt. Ik ben zelf ook bezig om langzaam maar zeker alles in de GTD-stijl te krijgen. Dat gaat niet vanzelf, dat kost tijd, maar ik merkte vrijwel direct al resultaat. En dat geeft een heel vrij gevoel! Dus check het uit: <a href="http://www.amazon.com/exec/obidos/redirect?tag=punkeycom-20&amp;path=tg%2Fdetail%2F-%2F0142000280%2Fqid%3D1098610067%2Fsr%3D8-1%2Fref%3Dpd_csp_1%3Fv%3Dglance%26s%3Dbooks%26n%3D507846"  title="" target='_blank'>Getting things Done, David Allen</a> 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1500&amp;w=sunday_morning_teanotes#comm" title="Cor Fijneman">een reactie</a> /  <a href="/pivot/entry.php?id=1500&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m10.php#e1500" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1500&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1464"></span><div class="entry">
<h3><span class='date'>17 Oktober 04 - 15&#58;25</span>Een wat verlate sunday morning teanotes...</h3>

<p style="text-align:center;"><img src="/images/sims.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
Tja, het is een typische zondag hier in Casa Punkey & QT. Eerst heerlijk uitgeslapen, dan relaxed brunchen en vervolgens achter de PC <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/> Aangezien ik grote problemen had met een tweede harddisk moest een <a href="http://thesims2.ea.com/"  title="" target='_blank'>niet geheel onbelangrijk spel</a> van QT opnieuw worden geinstalleerd. En toen besloot ik het zelf ook eens uit te proberen. Eerlijk is eerlijk. Sims 2 is een verslavend en verschrikkelijk boeiend spel. Vooral omdat er geen linair doel in zit. Je kunt doen wat je wilt, met wie je wilt en op je eigen wijze. Ik heb anderhalf uur gebiologeerd de tutorials zitten doen waarna ik werd verjaagd naar de laptop. Dan maar eens de RSS reader een zwengel geven en kijken wat er is gebeurd. Er is weer veel gaande in de wereld van RSS, weblogs en podcasting. Ik ga het deze namiddag eens op mijn gemak doornemen... 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1464&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=1464&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m10.php#e1464" title="Permanent link to 'Een wat verlate sunday morning teanotes...' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1464&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Een wat verlate sunday morning teanotes...'">&para;</a>--> </p>
</div><span id="e1424"></span><div class="entry">
<h3><span class='date'>10 Oktober 04 - 08&#58;50</span>Sunday morning teanotes</h3>

Hehehe....vrij naar Dave Winer's <a href="http://archive.scripting.com/2004/06/11"  title="" target='_blank'>Morning Coffeenotes</a>. Het is weer eens een normale zondagochtend, vroeg wakker, lekker uitgeslapen. een bak thee gezet en de <a href="http://www.dailysourcecode.com/"  title="" target='_blank'>Daily Source Code</a> op mijn iTunes. Deze ochtend gebruik ik nu vooral om mijn MP3-speler eens wat uit te mesten en alle ID3-tags weer eens goed te zetten. Natuurlijk niet helemaal met de hand. Gewoon <a href="http://www.id3-tagit.de/english/index.htm"  title="" target='_blank'>ID3-TagIt</a> gebruiken. iPodder staat weer wat nieuwe MP3-shows te downloaden voor de op de player. Straks ook weer eens de RSS feeds doornemen, kijken wat er nog meer in de wereld is gebeurd. U hoort nog van me! 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1424&amp;w=sunday_morning_teanotes#comm" title="Roh!">een reactie</a> /  <a href="/pivot/entry.php?id=1424&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m10.php#e1424" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1424&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1523"></span><span class="archief"><a href="/pivot/entry.php?id=1523&amp;w=sunday_morning_teanotes">31 Oktober 2004:</a><br></span>
<span id="e1500"></span><span class="archief"><a href="/pivot/entry.php?id=1500&amp;w=sunday_morning_teanotes">24 Oktober 2004:</a><br></span>
<span id="e1464"></span><span class="archief"><a href="/pivot/entry.php?id=1464&amp;w=sunday_morning_teanotes">17 Oktober 2004:</a><br></span>
<span id="e1424"></span><span class="archief"><a href="/pivot/entry.php?id=1424&amp;w=sunday_morning_teanotes">10 Oktober 2004:</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>