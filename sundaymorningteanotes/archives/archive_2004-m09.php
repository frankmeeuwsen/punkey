<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1336"></span><div class="entry">
<h3><span class='date'>19 September 04 - 14&#58;19</span>Lazy sunday afternoon</h3>

Het is een rustige zondagmiddag, dus een mooie tijd om eens de stapel links te posten die ik de laatste tijd heb verzameld. Doordat ik veel onderweg lees op de laptop, heb ik genoeg materiaal voor de komende tijd <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/> Niet alles is even nieuw, maar wel leuk, boeiend of interessant. <br />
Check bijvoorbeeld <a href="http://www.theglobeandmail.com/servlet/ArticleNews/TPStory/LAC/20040916/NHLNIKE16/TPSports/Hockey"  title="" target='_blank'>het verhaal</a> van Nike die een campagne in Canada zijn gestart om de ijshockeyspelers, die in staking zijn, weer terug op het ijs te krijgen. Nogal logisch, een bron van hun inkomsten dreigt zo flink weg te vallen. ..<br />
<br />
"Nike plans to begin running 30-second television commercials today playing on the emotions Canadians will feel from the withdrawal of the NHL game that is their national passion.<br />
<br />
The stark image is that of an idle, soundless hockey rink, its 19,000 seats vacant. The "action" is that of the ice, melting away, right down to the concrete floor. The simple message is "bring it back." 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1336&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=1336&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m09.php#e1336" title="Permanent link to 'Lazy sunday afternoon' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1336&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Lazy sunday afternoon'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1336"></span><span class="archief"><a href="/pivot/entry.php?id=1336&amp;w=sunday_morning_teanotes">19 September 2004:</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>