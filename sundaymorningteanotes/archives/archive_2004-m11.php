<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1573"></span><div class="entry">
<h3><span class='date'>28 November 04 - 12&#58;12</span>Sunday morning teanotes</h3>

Goedemorgen! Het is weer zondag, tijd voor de traditionele Sunday Morning Teanotes. Ik krijg zelfs "in het wild" al complimenten van mensen die dit onderdeel van de website erg leuk vinden om te lezen. Dank u, dank u! Afgelopen week was een hectische week. Druk op het werk (vakantie in zicht!), op bezoek bij potentiele klanten en natuurlijk de Emerce Outlook 2005 presentatie. Die is erg goed gegaan. Voor een publiek van plm. 200 man heb ik met 4 andere marketeers een verhaal gehouden over BusinessBlogging. Dit waren Marco Derksen, Interactive Marketing consultant van Share of Mind (en het gezicht achter <a href="http://www.mediafact.nl/comments.php?id=6120_0_1_0_C"  title="" target='_blank'>Marketingfacts</a>), Marco van Veen, Algemeen Directeur van <a href="http://www.checkit.nl/"  title="" target='_blank'>Checkit</a>, Jeroen de Bakker, Managing Director van <a href="http://www.qi-ideas.com/"  title="" target='_blank'>Qi</a> en Frank Janssen, manager Internet & Intranet bij TPG Post en schrijver van <a href="http://www.Frankwatching.com"  title="" target='_blank'>Frankwatching.com</a>. Mijn presentatie staat intussen ook <a href="http://www.rhinofly.nl/outlook2005"  title="" target='_blank'>online</a> evenals van de <a href="http://www.emerce.nl/download_index.jsp?rubriek=408459"  title="" target='_blank'>overige sprekers</a>. Je kunt commentaren onder andere lezen bij <a href="http://vnunl.typepad.com/outlook/"  title="" target='_blank'>Emerce</a>, <a href="http://www.mediafact.nl/comments.php?id=6120_0_1_0_C"  title="" target='_blank'>Marketingfacts</a>, <a href="http://www.frankwatching.com/2004/11/emerce-outlook-2005-enkele-nachtelijke.html"  title="" target='_blank'>Frankwatching</a> en natuurlijk <a href="http://www.rhinofly.nl/frank-ly/index.cfm?fuseaction=frankly.view&amp;FranklyId=3670ED91-E0A1-4BB7-8BC6DCA893463D75"  title="" target='_blank'>Frank-ly</a>.  Lees ook <a href="http://www.dutchcowboys.nl/reacties.php?nieuws=1188"  title="" target='_blank'>het commentaar</a> van Igor Beuker, voorzitter van IAB Nederland. Erg leuk!<br />
Komende week wederom een congres, deze keer <a href="http://www.marketing3.nl"  title="" target='_blank'>Marketing3</a> in Mediaplaza. Dit tweedaagse congres zal ik waarschijnlijk alleen donderdag bezoeken omdat het gewoonweg druk is op mijn werk en ik echt wat zaken af moet krijgen voor ik op vakantie ga! Aangezien het event in Mediaplaza is, verwacht dat de WiFi verbindingen goed zijn en ik wat live kan loggen.<br />
<br />
<b>Camera</b><br />
Zojuist ben ik eens wat dieper gaan zoeken naar een nieuwe digitale camera. Nog mijn dank voor alle goede adviezen en tips bij <a href="http://www.punkey.com/pivot/entry.php?id=1545"  title="" target='_blank'>mijn vraag</a> over camera's. Ik ben aan de hand van kieskeurig.nl eens op zoek gegaan. Belangrijkste criteria zijn budget, optische zoom en aantal megapixel. De keuze is nu gevallen op de <a href="http://www.kieskeurig.nl/nl/product.nsf/search1/7F6CE6A934DDB799C1256EFF00327E29"  title="" target='_blank'>HP Photosmart M307</a>. Onder andere vanwege de scherpe prijs, maar met name <a href="http://www.kieskeurig.nl/nl/product.nsf/compare?readform&amp;code=digitalecamera&amp;id=7F6CE6A934DDB799C1256EFF00327E29~DD08CEC5D0E1A480C1256EFD0033D456~0D834D082F659416C1256E41004AB277"  title="" target='_blank'>in vergelijking met andere kanshebbers</a> zijn de specificaties erg goed. De mogelijkheid om handmatig scherp te stellen, de grootte van het LCD scherm en gewicht zien er aantrekkelijk uit. Dus ik ga vandaag eens heerlijk de koopzondag in om te kijken waar ik hem kan kopen!<br />
<br />
<b>Getting Things Done</b><br />
Mensen die me kennen weten dat ik de laatste weken erg druk bezig ben met het GTD-principe. Ook in eerdere teanotes heb ik er al eens over <a href="http://www.punkey.com/pivot/entry.php?id=1500"  title="" target='_blank'>geschreven</a>. Ondertussen gaat het steeds beter, ik krijg langzaam maar zeker echt grip op mijn werk wat binnenkomt, wat er ligt en ik krijg meer gevoel voor prioriteiten. En natuurlijk is het leuk als je van <a href="http://www.coolbook.net"  title="" target='_blank'>een collega</a> te horen krijgt dat je ineens werk sneller afhebt en meer doet in minder tijd. Thankx Nic!<br />
<br />
<b>Weblogcards</b><br />
Laatste onderwerp voor deze SMTN is een idee waar ik al heel lang mee rondloop maar gewoon niet van de grond krijg. Door drukte, andere projecten, geen zin, whatever. Het gaat om Weblogcards.<br />
Wat is het? Heel simpel: een digitale variant op de Amerikaanse baseballcards maar dan voor webloggers. Op een digitaal kaartje (in Flash ofzo) kun je als weblogger wat statistieken van jezelf opgeven (logt sinds, soort weblog, standplaats etc) en die kaart kun je op je weblog zetten. Anderen kunnen deze weblogcard lezen maar, en nu komt het leuke, je kunt ze ook verzamelen! Door middel van een offline of online verzamelboek, kun je de weblogcards van je favoriete webloggers verzamelen en delen met anderen. Je kunt zelf denken aan een koppeling met Bloglines of Weblogs.com om te zien wanneer ze weer iets hebben gepost.<br />
Leuk idee toch? Vind ik wel. Maar ik kom er gewoon niet aan toe om het te maken. Ben ook niet meer zo'n programmeurwizard dus dan gaat het allemaal wat trager. <br />
Daarom gooi ik het idee nu in de publieke ruimte. Ik heb twee domeinnamen in mijn bezit (weblogcard.com en weblogcards.com) die ik zal laten aflopen bij de eerstvolgende update. Lijkt het je leuk om iets te doen met dit idee en wil je in bezit komen van de domeinnamen, neem dan <a href="mailto:punkey@gmail.com"  title="" target='_blank'>contact</a> met me op, dan zal ik de boel overdragen. Ik zou het wel leuk vinden om zijdelings nog wat betrokken te blijven bij het project, omdat het toch mijn "kindje" is. Ik ben ook benieuwd wat jullie van het idee vinden, ik lees het graag in de comments!<br />
<br />
Dat was hem weer voor deze ochtend! Ik ga ontbijten, douchen en op pad voor een camera! U hoort nog van me! 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1573&amp;w=sunday_morning_teanotes#comm" title="sytze, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1573&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m11.php#e1573" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1573&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1557"></span><div class="entry">
<h3><span class='date'>21 November 04 - 10&#58;40</span>Sunday morning teanotes</h3>

Goedemorgen op deze klimatologisch enigszins rustige zondagochtend, vergeleken met eerdere dagen. Man, de herfst is hier in Breda in elk geval losgebarsten in al zijn hevigheid. Echt weer om binnen te blijven en te chillen. Dus dat doen we dan ook. Met name het chillen. Want binnen blijven is ook zo wat niet waar? <br />
Als ik de vorige SMTN er op nalees is er eigenlijk weinig veranderd afgelopen week (hoe saai!). Nog steeds retedruk op het werk, dus te weinig tijd en zin om in mijn vrije tijd nog iets achter de PC te doen. Over drie weken zitten we in het vliegtuig naar Auckland. Afgelopen dagen hebben we de treinreis naar Frankfurt flughafen en de binnenlandse vlucht van Auckland naar Christchurch gereserveerd. Nu alleen nog een auto boeken en slaapplaatsen voor de eerste nacht, Kerstmis en Nieuwjaar. En dan zijn we boekingstechnisch klaar.<br />
<br />
Ik zal later deze ochtend ook nog proberen wat filmpjes online te zetten die we gisteren met de telefoon van QT hebben geschoten. We zijn op de regiofinales geweest van een VJ wedstrijd [<a href="http://www.visualsensations.nl/"  title="" target='_blank'>klotesite</a>] hier in de Mezz. En de getoonde films (of hoe noem je dat?) waren echt geweldig. Nu gaan we dus proberen om de filmpjes die we zelf hebben gemaakt van de wedstrijd via de Lifeblog software van Nokia ook werkelijk online te krijgen. Aangezien ik nog steeds geen account voor mezelf heb laten aanmaken voor de <a href="http://www.avpublisher.nl"  title="" target='_blank'>TICC AV Publisher</a> moet ik dus deze omslachtige methode gebruiken. Het zij zo!<br />
Einde van de dag mag ik trouwens weer naar de Mezz, deze keer voor <a href="http://www.mezz.nl/jointhemusic/agenda/details1.asp?actid=530&amp;refer=startpage"  title="" target='_blank'>The Rawk and Roll Monsterball</a> met als klapperrrrrrrr (tchop tchop tchop) een optreden van de legendarische Batmobile! Ik heb me voorgenomen om er pas heen te gaan als ik mijn presentatie voor het <a href="http://vnunl.typepad.com/outlook/"  title="" target='_blank'>Emerce Outlook 2005</a> event af heb, dus daar ga ik nu mee aan de slag! 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1557&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=1557&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m11.php#e1557" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1557&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1544"></span><div class="entry">
<h3><span class='date'>14 November 04 - 12&#58;18</span>Sunday morning teanotes</h3>

Goh, er zit slechts 1 postje tussen de twee SMTN's. Dat komt niet vaak voor. Maar het is nu wel begrijpelijk, aangezien het de afgelopen week razend druk is geweest en ik weinig tijd en zin heb gehad om 's avonds nog eens achter de PC te kruipen. Het is ondertussen ook zondag<i>middag</i>, maar dat mag deze traditie niet de kop in drukken <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/><br />
Deze keer een wat korte SMTN, ik heb genoeg te vertellen, maar ook genoeg te doen! We zijn bijvoorbeeld druk bezig met onze vakantieroute. Zoals jullie weten gaan we naar Nieuw Zeeland over 4 weken. We zijn nu aan het uitdokteren waar we Kerstmis en Nieuwjaar vieren. Dat begint vorm te krijgen. Het plan is nu om bij aankomst in Auckland (noordereiland) direct een binnenlandse vlucht te nemen naar Queenstown (zuidereiland) te nemen en dan naar boven te reizen. Zo kunnen we met Kerstmis in Wellington zijn, de oude woonplaats van QT, en zijn we met Nieuwjaar in Bay of Islands. En dat willen we! Oud en Nieuw op het strand! Tips, ideen en suggesties over NZ zijn meer dan welkom in de reacties trouwens! <br />
Verder ben ik ooklangzaam bezig om het hele GTD-principe (<a href="http://www.gettingthingsdone.com"  title="" target='_blank'>Getting Things Done</a>) vorm te geven op mijn werk. Gaat niet heel soepel omdat het een nogal hectische periode is nu. Maar het is een goed voornemen voor 2005 <img src='/extensions/emoticons/trillian/e_01.gif' alt=':-)' align='middle'/> Ik vind het wel leuk om te zien dat ik ook andere mensen aansteek om er mee aan de slag te gaan.<br />
Laatste puntje voor nu: BusinessBlogMeeting. Afgelopen woensdag was de eerste nederlandse BusinessBlogMeeting in Utrecht met plm. 15 marketeers die een weblog hebben. Doel was om eens kennis te maken met elkaar en ideeen en ervarigen uit te wisselen. Dat was erg succesvol en ik geloof dat er al weer plannen zijn om een nieuwe meeting te organiseren. Wil je meer weten over business weblogs, kom dan 25 november naar de <a href="http://www.emerce.nl/artikel.jsp?id=379027"  title="" target='_blank'>Emerce Outlook 2005</a> in Amsterdam waar ik met onder ander Marco Derksen een presentatie zal houden over dit onderwerp. Check trouwens ook <a href="http://vnunl.typepad.com/outlook/"  title="" target='_blank'>de weblog</a> rondom dit event! Uiteraard zal ik op die dag ook webloggen via <a href="http://www.frank-ly.nl"  title="" target='_blank'>de corporate weblog.</a><br />
<br />
That's it voor nu, ik ga een relaxed zondagje hebben! Later meer! 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1544&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=1544&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m11.php#e1544" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1544&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1542"></span><div class="entry">
<h3><span class='date'>07 November 04 - 11&#58;48</span>Sunday morning teanotes</h3>

Ik geloof dat we het deze ochtend over weinig anders kunnen hebben dan wat er afgelopen dinsdag is gebeurd. Ik heb de laatste dagen veel met vrienden gepraat over de gevolgen van de moord op Theo van Gogh. Wat is er aan de hand met dit land? Hoe kan zoiets gebeuren? Hoe zorgen we er voor dat dit niet nog een keer gebeurt? Iedereen heeft er zo zijn mening over. Veelal hoor ik de opmerking "Flikker ze allemaal het land uit" of "Pak de nederlandse nationaliteit af" en het doet me pijn om te constateren dat ik het daar mee eens ben. Gisterenavond sprak ik iemand die ik beschouw als een van de meest linkse personen die ik ken. Zelfs hij is omgeslagen. Zijn eerste opmerking: "Godverdomme nou ben ik het zat. Flikker ze allemaal maar het land uit". Ik merk het ook aan mezelf. Maar zo eenvoudig is het toch allemaal niet lijkt me. Moet je nu zomaar iedereen met een ander kleurtje over de grens zetten? Of moet je iedereen die "lastig" is het land uit kieperen? Hoe gaan we dan om met de spreekkoren en lastige kaalkoppen met een nederlands paspoort? Moet je die dan vast gaan zetten? Kost mij weer belastingcenten om extra cellen te bouwen. <br />
<br />
Wat ik vooral aan mezelf merk is dat ik kwaad ben op dit land. Dit is niet meer het land waar ik wil zijn, waar ik wil wonen. Ik was gisteren op de verjaardag van een van mijn beste vrienden. We kennen elkaar sinds we 5 zijn. We hebben veel, heel veel samen meegemaakt. En we raakten automatisch aan de praat over "vroeger". Ja, we worden oude lullen, want vroeger was inderdaad alles beter. We haalden herinneringen op over hoe het was in de straat waar we woonden, waar we gewoon konden buiten spelen, waar onze ouders de deur open lieten zonder dat er ongenode gasten binnen kwamen. Waar we het ook over hadden is over de eerste Marokkaanse familie die in de straat kwam wonen. Dat was begin jaren '80. Een gezin met 3 zonen en 1 dochter. Wat deden we? Wat alle jonge jochies doen tegen andere jonge jochies die ze niet kennen: ruzie maken. Maar een paar dagen later merkte we dat die jonge jochies in die familie net zo zijn als wij ook zijn en dan is het klaar. Toen merkte we al dat het geen ruk uitmaakt waar je vandaan komt. Als je je maar normaal gedraagt.<br />
Maar ergens in de afgelopen 25 jaar is het misgelopen in Nederland. En ik weet niet wat het is. Ik ben blij dat ik een tolerant land mag wonen. Ik ben blij dat je hier mag zeggen wat je wilt, zolang het binnen de grenzen van de wet valt. Maar helaas zijn er mensen die niet goed met die verworven vrijheden kunnen omgaan. Het groepje wat het verpest voor de anderen zullen we maar zeggen. Dus moeten we nu een paar van die vrijheden inleveren? Steeds als het over dat onderwerp gaat krijg ik Orwelliaanse "1984"-beelden op mijn netvlies. Ik weet niet meer wat ik er van moet denken, ik weet ook niet meer wat ik moet vinden van de vrijheden die we hebben.<br />
Heel egoistisch: Het enige wat ik nu nog vind is dat blij ben dat ik over een maand een tijdje naar de andere kant van de wereld ga. Even drie weken niet aan Nederland denken. Ik ben er aan toe. Nu meer dan ooit.<br />
<br />
<p style="text-align:center;"><img src="/images/doooden.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p> 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1542&amp;w=sunday_morning_teanotes#comm" title="Baby Grandpa zelf, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1542&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m11.php#e1542" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1542&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1573"></span><span class="archief"><a href="/pivot/entry.php?id=1573&amp;w=sunday_morning_teanotes">28 November 2004:</a><br></span>
<span id="e1557"></span><span class="archief"><a href="/pivot/entry.php?id=1557&amp;w=sunday_morning_teanotes">21 November 2004:</a><br></span>
<span id="e1544"></span><span class="archief"><a href="/pivot/entry.php?id=1544&amp;w=sunday_morning_teanotes">14 November 2004:</a><br></span>
<span id="e1542"></span><span class="archief"><a href="/pivot/entry.php?id=1542&amp;w=sunday_morning_teanotes">07 November 2004:</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>