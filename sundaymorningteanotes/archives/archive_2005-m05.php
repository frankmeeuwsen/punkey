<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1823"></span><div class="entry">
<h3><span class='date'>29 Mei 05 - 20&#58;21</span>Sunday Morning Teanotes?</h3>

	<p style="text-align:center;"><img src="/images/charlielost.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
Je zult je wel afvragen, waar zijn ze? Ik was vanochtend niet in de gelegenheid om ze te schrijven. Ik ben vanavond pas weer achter een PC gekropen. En deze keer voor een nieuwe TV verslaving: Lost. Man man man wat een geweldige serie! Aangezien de cliffhanger afgelopen vrijdag te erg was en er nu een zomerstop aankomt, hebben QT en ik besloten om niet op de grillen van een zender-programmeur te wachten, maar gewoon, downloaden via Internet. We hebben net de finale van The Apprentice achter de rug (lees <a href="http://www.rhinofly.nl/frank-ly/index.cfm?fuseaction=frankly.view&#38;FranklyId=061146A9-95A8-960D-6DE3617D3B8423D8"  target='_blank'>hier</a> hoe dat was) dus we kunnen ons vol op Lost storten.<br />
Het downloaden van The Apprentice gebeurde via BitTorrent. Dat ging redelijk goed. Na wat geklungel met NAPT-instellingen op de router (huh? wat? vergeet maar&#8230;) kwamen de afleveringen langzaam maar zeker binnengedruppeld. <br />
Met Lost ga ik het toch weer op de oude vertrouwde manier van nieuwsgroepen doen. BitTorrent is nog te onbetrouwbaar qua snelheid, aanbod en veiligheid. Via een dienst als <a href="http://www.Newzbin.com"  target='_blank'>Newzbin.com</a> kan ik direct zien of er nieuwe afleveringen zijn gepost in een nieuwsgroep. Binnen een half uur heb ik deze binnen via een payserver van Astraweb. Het is wat meer werk, maar dat is het wel waard. <br />
Het is trouwens verdomde moeilijk om wat over Lost te lezen zonder spoilers tegen te komen die ik nog moet zien! Er zijn wel weer enkele geweldige fora over de show. En als je een fan bent, kijk dan ook zeker naar de site van Oceanic Airlines! Zoek op vluchtnummer 815, check <a href="http://www.oceanic-air.com/seatingchart.htm"  target='_blank'>de seatingchart</a> aan de hand van de nummers <a href="http://athensohio.net/entertainment/4-8-15-16-23-42/"  target='_blank'>die overal terugkomen</a> of bekijk <a href="http://www.oceanic-air.com/images/oa_front-letter1a.jpg"  target='_blank'>deze scan</a> van het script voor het tweede seizoen. Of <a href="http://lost-forum.com/showthread.php?t=8265"  target='_blank'>lees de theorie</a> over het logo van Oceanic Air. Of wat te denken van <a href="http://kidneynotes.blogspot.com/2005/04/in-praise-of-lost-tv-show.html"  target='_blank'>een bloggende dokter die de show roemt</a> om het aanpakken van complexe medische problemen als alcholisme en drugsgebruik. Ja, de producers hebben zelfs gedacht aan <a href="http://www.driveshaftband.com/"  target='_blank'>een site voor Driveshaft</a>, de band van een van de hoofdrolspelers. Meer Lost?<br />
<a href="http://lost-forum.com/"  target='_blank'>Lost Forum</a><br />
<a href="http://abc.go.com/primetime/lost/index.html"  target='_blank'>Officiele site van de show</a><br />
<a href="http://lost-media.com/"  target='_blank'>Lost Media</a><br />
<a href="http://www.gotlost.nl/"  target='_blank'>Nederlandse fansite</a> met de geweldige <a href="http://www.gotlost.nl/sawyersong.htm"  target='_blank'>Sawyer-song</a><br />
<a href="http://www.lost-tv.nl/"  target='_blank'>Grootste nederlandse Lost-site</a><br />
<a href="http://www.startkabel.nl/k/lost/"  target='_blank'>Startpagina met meer links&#8230;</a></p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1823&amp;w=sunday_morning_teanotes#comm" title="Carlo, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1823&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m05.php#e1823" title="Permanent link to 'Sunday Morning Teanotes?' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1823&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday Morning Teanotes?'">&para;</a>--> </p>
</div><span id="e1809"></span><div class="entry">
<h3><span class='date'>22 Mei 05 - 10&#58;35</span>Sunday morning teanotes</h3>

	<p>We leven nog! Een maand niets op deze log gepost is volgens mij een unicum in de geschiedenis van Punkey.com. Maar het was niet geheel zonder reden. Eigenlijk is dit de eerste zondag waar ik echt weer eens de tijd kan nemen om wat te posten. En ook <em>wil</em> posten. Want dat speelt ook mee. Want wat heb ik de afgelopen tijd allemaal uitgevoerd?</p>
	<p><strong>Conferenties</strong><br />
De afgelopen maand heb ik twee erg interessante conferenties bezocht. Allereerst <a href="http://blognomics.typepad.com/"  target='_blank'>Blognomics</a>, in de RAI te Amsterdam. Gedurende de dag heb ik dankbaar gebruik mogen maken van de WiFi verbindingen op het congres, waardoor ik voor Frank-ly <a href="http://www.rhinofly.nl/frank-ly/index.cfm?Fuseaction=frankly.search&#38;trefwoord=blognomics&#38;auteur=&#38;maand=&#38;categorie"  target='_blank'>aardig wat posts</a>= heb kunnen schrijven gedurende de interessante middag. Via <a href="http://www.technorati.com/tag/blognomics"  target='_blank'>Technorati</a> kun je ook nog diverse posts en foto&#8217;s vinden van andere weblogs,<br />
Een paar dagen later zat ik de Thalys, op weg naar Parijs voor <a href="http://www.socialtext.net/loicwiki/index.cgi?internet_2_0"  target='_blank'>Les Blogs</a>. Deze zeer succesvolle conferentie werd door een grote groep nederlanders bezocht, wat resulteerde in een zeer aangename dag met veel nieuwe ontmoetingen. Uiteraard heb ik ook van Les Blogs weer <a href="http://www.rhinofly.nl/frank-ly/index.cfm?Fuseaction=frankly.search&#38;trefwoord=les+blogs&#38;auteur=&#38;maand=&#38;categorie"  target='_blank'>verschillende posts</a>= geschreven op Frank-ly. Door het intensieve gebruik van tagging is ook via Technorati <a href="http://www.technorati.com/tag/les+blogs"  target='_blank'>een natuurgetrouwe weergave</a> van het evenement te vinden. Een van de hoogtepunten van dit jaar tot nu toe vind ik.</p>
	<p><strong>Werk</strong><br />
Bovenstaande conferenties zijn ook bezocht wegens mijn werk bij Rhinofly. Ook dat staat niet stil. Sterker nog, het is een <em>rollercoasterride</em> die niet lijkt op te houden. Een flink aantal leuke nieuwe opdrachten, nieuwe ontwikkelingen en trends. We zijn ook op zoek naar nieuwe mensen, zie <a href="http://www.rhinofly.nl"  target='_blank'>onze site</a> voor details. Maar dat resulteert onder andere in minder tijd en zin om te loggen. Want als je al 10 uur achter een beeldscherm zit heb je weinig zin om &#8217;s avonds nog eens een weblog vol te kalken. </p>
	<p><strong>Andere weblogs</strong><br />
Want als ik al voor weblogs schrijf, is het meer voor andere weblogs dan voor deze. Natuurlijk is daar <a href="http://www.frank-ly.nl"  target='_blank'>Frank-ly</a>, de corporate weblog. Hier schrijven we over marketing, Rich Internet, online video etc. Ik vind het prettig om daar te schrijven, omdat het over mijn vakgebied en mijn passie gaat. Datzelfde geldt voor <a href="http://www.marketingfacts.nl"  target='_blank'>Marketingfacts</a>. Samen met een illuster gezelschap van online marketeers schrijf ik periodiek over nieuwtjes op het gebied van marketing. Wederom, passie en interesse die de boventoon voeren. <br />
Naast deze marketingblogs schrijf ik ook weer geregeld in het engels op <a href="http://www.whatsthenextaction.com"  target='_blank'>What&#8217;s The Next Action</a>. Deze weblog gaat over personal productivity (of <a href="http://wiki.43folders.com/index.php/Productivity_pr0n"  target='_blank'>Productivity pr0n</a> volgens sommigen) en heeft als uitgangspunt de productiviteitsprincipes van <a href="http://wiki.43folders.com/index.php/Getting_Things_Done"  target='_blank'>Getting Things Done</a>. Een onderwerp wat ook mijn interesse heeft en waar ik als persoon ook werkelijk wat aan heb. Niet dat ik nu een efficient productiemonster ben, maar in elk geval wel op de goede weg. <br />
Als laatste schrijf ik ook nog voor <a href="http://www.aboutblank.nl"  target='_blank'>About:Blank</a>, neerlands weblogzine. Al heb ik daar de laatste tijd ook weinig geschreven door eerder genoemde redenen. Niet geheel als andere weblogs aan te kaarten, maar wel iets wat de laatste tijd mijn aandacht heeft is Flickr.com. Sinds ik hier <a href="huwww.flickr.com/photos/punkey/"  target='_blank'>een Pro account</a> heb, is het voor mij een bron van tijdverspilling om doorheen te bladeren, eigen foto&#8217;s te organiseren en tags te bekijken. En alle <a href="http://www.frankwatching.com/archive/2005/05/16/tooltip_update_flickr_features#body"  target='_blank'>diensten die er omheen worden ontwikkeld</a> vanuit de <a href="http://www.flickr.com/services/api/"  target='_blank'>Flickr API</a>. Zeer interessante ontwikkelingen die voortkomen uit de principes van <a href="http://www.rhinofly.nl/frank-ly/index.cfm?fuseaction=frankly.view&#38;FranklyId=82FFDA1F-BA32-ACB2-C8A42A6B64F0ADDF"  target='_blank'>Folksonomy en tagging</a>. Zoals bijvoorbeeld <a href="http://www.flickr.com/groups/nederlands/"  target='_blank'>de discussiegroep ter bevordering van Nederlandse trefwoorden op Flickr.com</a></p>
	<p><strong>Verhuizing</strong><br />
En <em>last</em> maar zeker niet <em>least</em> natuurlijk onze verhuizing van Breda naar Utrecht. Sinds 2 mei wonen we in Utrecht en sinds 13 mei zijn we ook echt compleet over (<a href="http://www.flickr.com/photos/punkey/sets/286033/"  target='_blank'>foto&#8217;s!</a>). Veel geverfd, geschuurd, gelakt, geschroefd en getimmerd. We zijn nog lang niet klaar, maar ons huis is bewoonbaar. Maar in een nieuw huis moeten ook nieuwe meubelen, dus zijn we in de weekenden veel op pad naar meubelboulevards en kleine meubelwinkels. Sinds vorige week hebben we ook een internetverbinding, al is die nog niet helemaal in orde lijkt het. Want al mijn BitTorrent verkeer lijkt niets meer te doen of in elk geval tergend traag te zijn. Als iemand ideeen heeft hoe en waar ik moet kijken om een oplossing te vinden, ik hou me aanbevolen! </p>
	<p><strong>Dusssss&#8230;..</strong><br />
Ga ik weer meer posten hier? Weet ik nog niet. Het is niet zozeer dat ik er nog steeds weinig tijd voor heb, maar de bovengenoemde passie en interesse is wat verdwenen. Punkey.com is toch altijd een soort linkdump geweest en ondertussen hou ik me met andere onderwerpen bezig dan leuke links op internet. <br />
&#8220;<em>Maar die kun je toch ook hier posten?</em>&#8221;<br />
Natuurlijk, maar dan zou ik naast Frank-ly, Marketingfacts en What&#8217;s The Next Action weer een extra plaats hebben waar dezelfde links terugkomen. Daar heb ik niet zo&#8217;n zin meer in. Ik zou Punkey.com om kunnen katten naar een marketinggerelateerde blog, maar daar heb ik ook al andere uitlaatkleppen voor. <br />
&#8220;<em>Linkdumps blijven leuk</em>&#8221;<br />
Inderdaad. Linkdumps zijn er ook genoeg te vinden online en ik zie niet in hoe ik daar een aanvulling op kan zijn. Om weer een weblog te zijn die voor 100e keer een filmpje of een foto post, daar pas ik voor. Dus wil ik op zoek gaan naar een andere manier om punkey.com invulling te geven. Wat en hoe weet ik nog niet, maar reken er maar op dat er geexperimenteerd gaat worden hier. Een van de eerste experimenten die ik op korte termijn zal inzetten is een automatische linkdump vanuit mijn offline programma <a href="http://www.onfolio.com"  target='_blank'>OnFolio</a> naar deze site. Hierdoor kun je bijhouden wat ik lees en waar mijn interesses liggen. Hoe ik dat precies ga doen weet ik nog niet, maar het gaat er komen. Als experiment. <br />
Een tweede experiment zal mijn afstudeerscriptie over scenemarketing zijn. Met deze scriptie ben ik in 1995 afgestudeerd. Nu, 10 jaar later, merk ik dat veel van die onderwerpen gemeengoed zijn geworden. Met name door de opkomst van Internet en specifiek de opkomst van het nieuwe WWW (Weblogs, Wiki&#8217;s en Webfeeds/RSS) zijn de ideeen rondom scenemarketing actueler dan ooit. Daarom zal ik de komende tijd ook mijn scriptie online gaan zetten als open source experiment. Onder een Creative Commons licentie kan en mag je er mee gaan doen wat je wilt. Allereerst moet ik mijn scriptie weer in digitale vorm terugvinden in de verhuisdozen, dus hou het in de gaten. <br />
Een derde experiment is dat ik een online CV ga maken. Nee, niet omdat ik op zoek ben naar een andere baan, maar omdat ik de laatste tijd erg veel heb geschreven en heb gedaan en dat graag op 1 plaats terugvind. Denk aan workshops en presentaties, maar ook artikelen die ik heb geschreven. Ik heb nooit veel aandacht besteedt aan het verzamelen en samenballen van deze informatie. Maar denk eens aan de mogelijkheden voor <a href="http://ola.rynge.net/blog/"  target='_blank'>personal branding</a>, en ik zie daar wel mogelijheden in om mezelf beter te promoten en er uiteindelijk zelf ook beter van te worden op persoonlijk vlak.</p>
	<p>Dat zijn zo&#8217;n beetje de plannen voor de komende tijd. <em>light posting ahead</em> dus, maar ook wat nieuwe experimenten. Als je vragen of opmerkingen hebt, aarzel niet om ze te stellen in de comments!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1809&amp;w=sunday_morning_teanotes#comm" title="cyclone, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1809&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m05.php#e1809" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1809&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1823"></span><span class="archief"><a href="/pivot/entry.php?id=1823&amp;w=sunday_morning_teanotes">29 Mei 2005:Lost</a><br></span>
<span id="e1809"></span><span class="archief"><a href="/pivot/entry.php?id=1809&amp;w=sunday_morning_teanotes">22 Mei 2005:Converenties, werk en de verhuizing</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>