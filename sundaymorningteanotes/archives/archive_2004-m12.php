<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1582"></span><div class="entry">
<h3><span class='date'>05 December 04 - 11&#58;08</span>Sunday morning teanotes</h3>

	<p>Goedemorgen op deze (waarschijnlijk) laatste SMTN voor 2004! Volgende week zijn we op pad naar Frankfurt, om van daaruit te vliegen op Seoul en door te sjezen naar Auckland, Nieuw Zeeland! Nog een weekje hard werken en dan mag ik eindelijk gaan genieten van een welverdiende vakantie. Velen hebben al gevraagd of ze de reis kunnen bijhouden op deze weblog. Uiteraard! Ik verwacht niet dat we dagelijks zullen posten, maar we zullen proberen zo nu en dan een teken van leven te geven op deze site. Hou het dus in de gaten. Je ziet <a href="http://www.punkey.com/images/nzmap_copy.gif"  onclick="window.open('http://www.punkey.com/pivot/includes/photo.php?img=aHR0cDovL3d3dy5wdW5rZXkuY29tL2ltYWdlcy9uem1hcF9jb3B5LmdpZg%3D%3D&w=600&h=849&t=','imagewindow','width=600,height=849,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no,left=0,top=0');return false" style='border: 0;' target="_self"  class='pivot-popuptext'  target='_blank'>hier</a> een kaart van de route die we doen. We beginnen in Christchurch (zuiden) en eindigen in Auckland (noorden)</p>
	<p>Deze SMTN is een wat kortere, ik wil eigenijk een onderwerp aansnijden<br />
Afgelopen vrijdag ben ik naar een concert van de Jungle Brothers geweest, hier in Breda. Dit was het enige concert van deze hiphop grootheden in Nederland. De <a href="http://en.wikipedia.org/wiki/Jungle_Brothers"  target='_blank'>Jungle Brothers</a> komen in het midden van de jaren &#8216;80 met het fenomenale album &#8220;Straight out the jungle&#8221;. Samen met De La Soul en A Tribe Called Quest zijn ze de spil van <a href="http://en.wikipedia.org/wiki/Native_Tongues_Posse"  target='_blank'>The Native Tongue Posse</a>. Bekijk <a href="http://www.mp3.com/musicvine/index.php?artist_id=42"  target='_blank'>hier</a> hun plaats in de muziekgeschiedenis via MusicVine. Hun tweede album &#8220;Done by the forces of nature&#8221; is ook een klassieker maar helaas gaat het daarna bergafwaarts met ze en verdwijnen ze in de vergetelheid (is dat een goede zegswijze?)<br />
Ik heb ze in 1986 in de Effenaar gezien met A Tribe Called Quest en nu dus weer in Breda. Het concert was te gek! Niet fenomenaal of het beste wat ik ooit heb gezien, maar gewoon een goed concert. Het is natuurlijk te gek als je al die oude nummers voorbij hoort komen. <br />
Maar wat is er nu zo jammer? Dat er slechts 30 man in de zaal stond. Een van de grotere hiphopbands uit de begindagen van deze muziekstroming en er komt geen hond op af. Nu is de Mezz al vaker gebleken een ster te zijn in het goed promoten van concerten, maar dit slaat echt alles. Ook andere bezoekers waren erg verbaasd over het wegblijven van publiek. Het is het enige concert van de Jbeez in Nederland, dus waar zijn de fans uit andere steden? Maar ook de eigen hiphopscene uit Breda blonk uit in afwezigheid. Echt heel erg jammer, want nogmaals, het was een te gek concert. Wil je toch wel eens wat horen van de JBeez? Kijk dan of je via BitTorrent wat kun vinden (mij is het niet gelukt) of check een van onderstaande sites</p>
	<p><a href="http://www.mp3.com/albums/22345/downloads.html"  target='_blank'>MP3.com</a><br />
<a href="http://www.epitonic.com/artists/thejunglebrothers.html"  target='_blank'>Epitonic.com</a></p>
	<p>Goed, dat was het weer voor deze ochtend! Zoals gezegd, heeeeel misschien volgende week nog een SMTN. En anders vind je hier na deze week onze avonturen uit Kiwiland, Aotearoa of Land of the Long White Cloud!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1582&amp;w=sunday_morning_teanotes#comm" title="bareuh">een reactie</a> /  <a href="/pivot/entry.php?id=1582&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2004-m12.php#e1582" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1582&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1582"></span><span class="archief"><a href="/pivot/entry.php?id=1582&amp;w=sunday_morning_teanotes">05 December 2004:</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>