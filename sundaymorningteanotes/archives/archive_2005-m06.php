<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e1850"></span><div class="entry">
<h3><span class='date'>26 Juni 05 - 22&#58;00</span>Sunday morning teanotes</h3>

	<p>Euhm..ja&#8230;zondagavond dus&#8230;En ik heb nog geen thee op vandaag. Maar goed, ik kreeg gisteren al het verwijt dat ik sinds de verhuizing naar Utrecht niet echt meer consistent mee ben. Nou, hier heb je het tegendeel! Gisteren hadden we een klein feestje voor wat vrienden in ons nieuwe huis. Erg gezellig! Vrienden uit zowel Breda als Utrecht. Erg goed, leuke gesprekken, mooie afspraken gemaakt. Ik geloof dat ik binnenkort moet gaan wakeboarden? <a href="http://www.ditadres.com"  target='_blank'>Tjak</a>? Hoe zit het nu? Ik heb gisterenochtend ook besteed aan het bekijken van de <a href="http://channel9.msdn.com/ShowPost.aspx?PostID=80533"  target='_blank'>RSS-video van Microsoft</a>. Voor degenen die het niet echt bijhouden, <a href="http://msdn.microsoft.com/longhorn/understanding/rss/rsslonghorn/#rss"  target='_blank'>Microsoft gaat RSS ondersteunen</a> in de nieuwe versie van Windows (Longhorn) en Internet Explorer 7. Interessante ontwikkelingen, op <a href="http://www.rhinofly.nl/frank-ly/index.cfm?fuseaction=frankly.view&#38;FranklyId=BA299632-FFCC-370F-006ED622E0AE1FC4"  target='_blank'>het corporate log</a> heb ik er over geschreven.<br />
Vandaag was de enorme uitbrak-dag. <a href="http://www.babygrandpa.com"  target='_blank'>Opa Bibs en Coochie</a> zijn blijven slapen, dus eerst maar eens uitgebreid ontbijten om 1 uur &#8217;s middags en daarna heel relaxed opgeruimd en schoongemaakt. Om een uur of 5 zijn we naar de Maarseveense plassen gefiets, even aan het water gelegen en nu zitten we rustig op de bank zondagavond te hebben. Ik heb zojuist naar de <a href="http://mp3.dailysourcecode.podshow.com/DSC-2005-06-25.mp3"  target='_blank'>200e Daily Source Code</a> (MP3, 33 min.) van Adam Curry geluisterd. Deze nam hij live op tijdens zijn keynote speech op <a href="http://www.gnomedex.com"  target='_blank'>Gnomedex</a>. Een geweldig verhaal, bottomline: <em>We are the media</em>. Wij bepalen, als gebruikers, zelf wat we willen zien, lezen of horen. Een ongelooflijk krachtig kanaal wat we effectiever moeten en kunnen inzetten. Developers and users party together.<br />
Dit sluit ook aan bij een presentatie waar ik afgelopen donderdag was. Op de tweede Nederlandse Businessblogmeeting sprak Neville Hobson over blogs als PR-instrument. <a href="http://www.frankwatching.com/archive/2005/06/25/tweede_nederlandse_businessblo#body"  target='_blank'>Erg goed verhaal</a> en zijn bottomline was: &#8220;These tools (RSS, Wiki, Weblogs) represent change for companies.&#8221; Ik ben daar meer dan ook van overtuigd. Weblogs gaan en kunnen verder dan de linkdump of het persoonlijke dagboek. Zolang ze met passie en authenticiteit zijn geschreven, kunnen ze meer betekenen dan welk persbericht ook.<br />
Nou, daar denk ik dan zo&#8217;n beetje over na op een chilly zondagavond&#8230;</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1850&amp;w=sunday_morning_teanotes#comm" title="Tjarko, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1850&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m06.php#e1850" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1850&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1845"></span><div class="entry">
<h3><span class='date'>19 Juni 05 - 10&#58;33</span>Sunday morning teanotes</h3>

	<p style="text-align:center;"><img src="/images/dsc01619_copy.jpg" style="border:0px solid" title="" alt="" class="pivot-image" /></p><br />
Tja, waren er vorige week inene weer geen SMTN! Om je eerlijk te zeggen, helemaal vergeten! Ik had het zo druk met voorbereidingen voor een <em>housewarmingfeestje</em> dat ik er pas later in de week achter kwam dat ik ze niet had geschreven. Maar nu zijn we er weer. Met een paar onderwerpen deze ochtend</p>
	<p><strong>Apple powerbook</strong><br />
Meerderen vroegen zich al af voor wie de Powerbook is die ik heb besteld. Ga ik <em>switchen</em> naar Apple? Nee hoor, de laptop is voor QT. Omdat we de PC weer boven hebben staan willen we die langzaam maar zeker ombouwen naar een soort mediacenter waar alle muziek en film staat. QT kan dan, net als ik, simpel vanaf de bank beneden (of de tuin!) haar ding doen. En waarom dan een Apple? Omdat het mooie apparaten zijn en omdat ik wel benieuwd ben naar alle hoopla rondom Apple en het operating system. </p>
	<p><strong>XS4All mail</strong><br />
Ik meen dat er meer lezers zijn met een mailadres bij xs4all. Elke abonnee krijgt daar extragratis een spamfilter bij de mailbox en een aparte mailbox waar alle ellende in wordt gepleurd. Maar is het alleen bij mij of bij ook bij anderen dat die filter niet meer jofel werkt? Ik word echt gek van de spam die dagelijks op mijn xs4all-mail binnenkomt. Nog geen honderden mails, maar toch wel tientallen. Iemand anders daar ook last van?</p>
	<p><strong>Verbouwen</strong><br />
Na een maandje in het nieuwe huis begint de verbouwingsmoeheid al toe te slaan. Aangezien QT nu ook 40 uur in de week werkt, zijn we afhankelijk van het weekend om nog dozen op te ruimen, dingen op te hangen, tuintje te schoffelen en ga zo maar door. Maar in het weekend ben ik blij als we uit kunnen rusten en even niks hoeven te doen. Terwijl er nog wel snoeren door de woonkamer liggen, gaten in muren moeten worden geboord om een kapstok op te hangen en er een kledingstang moet komen. Lastig hoor, zo&#8217;n eigen huis hebben&#8230;</p>
	<p><strong>Fields of Rock</strong><br />
Ik was gisteren jarig, dat was al bekend. PS, de Kreatief met Kurk-DVD is al binnen! Maar mijn eigen cadeau was een tripje naar Fields of Rock in Nijmegen. Een dagje lekker de beuk er in en bandjes kijken. En met een zonovergoten dag als gisteren is dat uit-ste-kend gelukt. Niet alles was even geweldig (Velvet Revolver) maar de sfeer en de lokatie waren perfect. Naast de wat nieuwere bands waren er ook de oude rotten. Black Sabbath. In de originele bezetting. En dat was te gek! Ozzy op het podium tekeer zien gaan met klassiekers als &#8220;War pigs&#8221;, &#8220;Paranoid&#8221; en &#8220;Sabbath Bloody Sabbath&#8221;. Helemaal goed! Vlak voor Black Sabbath speelde Audioslave. Het leek alsof ze daar bij elk nummer een andere geluidsman hadden. Het ene nummer klonk geweldig (Show me how to live) terwijl het volgende nummer echt beroerd was. Ook werden er oude Soundgarden en Rage against the machine nummers gespeeld. En met een voorman als Chris Cornell kun je dan beter bij Soundgarden-nummers blijven, want RATM-klassiekers als &#8220;Killing in the name of&#8221; en &#8220;Sleep now in the fire&#8221; klinken behoorlijk shitty uit zijn strot. Maar verder een top-optreden.<br />
En toen de klapper van de dag. Flogging Molly. In een volle tent, openend met Black Sabbath&#8217;s Paranoid en meteen iedereen meekrijgen. Wat een briljante show was het, wat een geweldige opzwepende muziek en wat een feest. Voor het eerst sinds jaren heb ik echt uit volle borst staan meezingen, dansen, pogo-en en springen. Flogging Molly was de absolute winnaar van de dag voor ons. We zijn in Utrecht nog even naar Ierse pub gegaan om uit te hijgen na het festival. Topdag!<br />
Natuurlijk is een verhaal over een festival leuk, maar het wordt veel leuker als je een beetje kan meegenieten. Dus heb ik wat foto&#8217;s en video&#8217;s online gezet. De foto&#8217;s zijn traditiegetrouw bij Flickr.com te vinden in <a href="http://www.flickr.com/photos/punkey/sets/472136/show/"  target='_blank'>deze slideshow</a> <br />
Ik heb een paar video&#8217;s bij Dailymotion geupload. Je vindt ze ook al in de linkerbalk van deze site, maar voor de duidelijkheid even melden wat wat is. Het zijn allemaal korte clipjes van plm 20-40 seconden.<br />
<a href="http://www.dailymotion.com/video/632"  target='_blank'>Audioslave &#8211; Black Hole Sun akoestisch</a><br />
<a href="http://www.dailymotion.com/video/633"  target='_blank'>Audioslave &#8211; Killing in the name of&#8230;</a><br />
<a href="http://www.dailymotion.com/video/631"  target='_blank'>Audioslave &#8211; Sleep now in the fire</a><br />
<a href="http://www.dailymotion.com/video/630"  target='_blank'>Flogging Molly &#8211; geen idee welk nummer, maar het rock!</a><br />
<a href="http://www.dailymotion.com/video/634"  target='_blank'>Flogging Molly &#8211; wederom geen flets benul hoe het nummer heet&#8230;</a><br />
<a href="http://www.dailymotion.com/video/635"  target='_blank'>Motorhead &#8211; Ace of spades</a></p>
	<p><strong>San Fransisco</strong><br />
Heb ik voor mezelf kaartjes gekocht voor een festival in Nijmegen, het cadeau wat ik van mijn allerliefste QT kreeg is veel heftiger! We gaan een week naar San Fransisco! In 2001 zijn we er ook al eens geweest en sinds die tijd ben ik echt dol op die stad. Ik had het er vreselijk naar mijn zin, prachtige gebouwen, mooie straten en parken, leuke mensen. En in augustus gaan we weer! Echt helemaal te gek! Ik ga mijn National Geographic-gids weer van de plank halen en natuurlijk online eens bijhouden wat er in die periode allemaal is te doen!</p>
	<p><strong>About:Blank</strong><br />
Sommigen weten het al, de meesten denk ik nog niet. De afgelopen twee jaar heb ik maandelijks artikelen geschreven voor <a href="http://www.aboutblank.nl"  target='_blank'>About:Blank</a>. Maar met ingang van juni heb ik besloten daar mee te stoppen. Naast al het andere schrijfgeweld op de verschillende weblogs moest er iets sneuvelen. En dat is About:blank geworden. Ik zal nog wel zijdelings wellicht zo nu en dan mijn mening (mogen) geven bij de redactie, maar ik zal niet meer actief betrokken zijn bij de maandelijkse uitgaven. Dat neemt niet weg dat de redactie nog wel naarstig op zoek is naar nieuwe redactieleden! Dus heb je interesse? Laat het dan even weten op de site!</p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1845&amp;w=sunday_morning_teanotes#comm" title="Tjarko, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1845&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m06.php#e1845" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1845&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e1827"></span><div class="entry">
<h3><span class='date'>05 Juni 05 - 11&#58;14</span>Sunday morning teanotes</h3>

	<p>Zondagochtend is en blijft toch een heerlijk moment van de week. Ik weet niet waarom, maar op een zondagochtend voel ik me altijd net even wat meer relaxed en ontspannen dan op een woensdagochtend ofzo. Misschien omdat de omgeving ook rustiger is, stiller. Ik heb al gemerkt dat onze straat een doorvoerkanaal is voor fietsende scholieren. En op zondag heb je die niet. Heerlijk! Zojuist weer de traditionele kop thee gezet (<a href="http://www.celestialseasonings.com/products/herb/lz.php"  target='_blank'>Lemon Tea Zinger</a>) en rechtop in de bank gaan zitten met de laptop op schoot. Vandaag, en hopelijk alleen nog vandaag, zal weer een dag zijn van plat liggen. Moeilijk voor iemand met tonnen aan ideeen, to-do&#8217;s en geen geduld. <br />
Het gaat nog niet echt veel beter met mijn rug. Met name door bovenstaande. Ik kan gewoon niet stil liggen op mijn rug. Ik wil en moet wat gaan doen. Dus loop ik wat in het huis op en neer, zit achter de PC om wat DVD&#8217;s te branden met afleveringen van de Apprentice en Lost en ga wat boodschappen doen. Mja, niet echt bevorderlijk voor de rug. Gisterenavond ook nog even op een feestje van QT&#8217;s nieuwe werk geweest, maar ook dat werkt niet mee aan een gezond herstel. Dus vandaag moet ik de hele dag rusten. Nou ja, ik ga het dus maar proberen met wat tijdschriften, boeken en DVD&#8217;s die ik nog heb liggen. Een kleine bloemlezing aan nieuw spul wat hier is binnengekomen: 
	<ul>
		<li>Tijdschriften: <a href="http://www.makezine.com"  target='_blank'>Make</a>, <a href="http://www.blend.nl"  target='_blank'>Blend</a>, <a href="http://www.bright.nl/"  target='_blank'>Bright</a>, <a href="http://www.nationalgeographic.nl/"  target='_blank'>National Geographic</a>, <a href="http://www.blagmagazine.com/spring.html"  target='_blank'>Blag</a>, <a href="http://www.intermediair.nl"  target='_blank'>Intermediair</a>, <a href="http://www.esquire.com/"  target='_blank'>Esquire</a></li>
		<li>Boeken: <a href="http://www.amazon.com/exec/obidos/redirect?link_code=ur2&camp=1789&tag=punkeycom-20&creative=9325&path=tg/detail/-/0316172324/qid=1117962143/sr=8-1/ref=pd_csp_1?v=glance%26s=books%26n=507846"  target='_blank'>Blink</a>, <a href="http://www.amazon.com/exec/obidos/redirect?link_code=ur2&camp=1789&tag=punkeycom-20&creative=9325&path=tg/detail/-/0316316962/ref=lpr_g_1?v=glance%26s=books"  target='_blank'>Tipping Point</a>, <a href="http://www.amazon.com/exec/obidos/redirect?link_code=ur2&camp=1789&tag=punkeycom-20&creative=9325&path=tg/detail/-/0874775043?%5Fencoding=UTF8%26v=glance"  target='_blank'>The Now Habit</a>, <a href="http://www.amazon.com/exec/obidos/redirect?link_code=ur2&camp=1789&tag=punkeycom-20&creative=9325&path=tg/detail/-/0670032506/qid=1117962439/sr=8-2/ref=pd_csp_2?v=glance%26s=books%26n=507846"  target='_blank'>Ready for anything</a>, <a href="http://www.amazon.com/exec/obidos/redirect?link_code=ur2&camp=1789&tag=punkeycom-20&creative=9325&path=tg/detail/-/1591840783?%5Fencoding=UTF8%26v=glance"  target='_blank'>Brand HiJack</a> (u merkt, ik ben een sucker voor businessbooks), </li>
		<li>DVD&#8217;s: <a href="http://www.amazon.com/exec/obidos/redirect?link_code=ur2&camp=1789&tag=punkeycom-20&creative=9325&path=tg/detail/-/0792846427/qid=1117962490/sr=8-3/ref=pd_csp_3?v=glance%26s=dvd%26n=507846"  target='_blank'>Fargo</a>, <a href="http://www.amazon.com/exec/obidos/redirect?link_code=ur2&camp=1789&tag=punkeycom-20&creative=9325&path=tg/detail/-/B0001CCXHA/qid=1117962533/sr=1-1/ref=sr_1_1?v=glance%26s=dvd"  target='_blank'>on the road with the Dropkick Murphy&#8217;s</a>,  (Luister alvast naar de <a href="http://www.amazon.com/exec/obidos/tg/detail/-/B0009IOR0C/ref=m_art_li_4/102-5023800-9164908?v=glance&#38;s=music!)" Lost" target='_blank'>nieuwe DKM-CD</a> afleveringen&#8221;:http://www.lost-tv.com<br />
Dus het is niet dat ik niks te doen heb. Tel daarbij nog de stapel RSS feeds die ik geregeld lees en ik vermaak me wel vandaag vermoed ik!</p>
	<p>Ik wil jullie zondag ook nog even opvrolijken met wat geinige filmpjes die ik op iFilm heb gevonden. Die site wordt echt met de dag beter. Check deze virale filmpjes:
		<li><a href="http://www.ifilm.com/ifilmdetail/2671841"  target='_blank'>Buschauffeur draait door</a></li>
		<li><a href="http://www.ifilm.com/ifilmdetail/2664187"  target='_blank'>Weerman draait door</a></li>
		<li><a href="http://www.ifilm.com/ifilmdetail/2669689"  target='_blank'>Japanse verkoper van een keyboard draait door maar blijkt dan toch heel goed te kunnen drummen</a></li>
		<li><a href="http://www.ifilm.com/ifilmdetail/2669010"  target='_blank'>Baby Metallica drummer</a></li>
		<li><a href="http://www.ifilm.com/ifilmdetail/2667867"  target='_blank'>Leer dansen als een blanke</a></li>
		<li><a href="http://www.ifilm.com/ifilmdetail/2671020"  target='_blank'>Is this the way to Armadillo, army-stylee</a></li>
	</ul></p>

  
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=Sunday_morning_teanotes&amp;w=sunday_morning_teanotes">Sunday morning teanotes</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=1827&amp;w=sunday_morning_teanotes#comm" title="QT, N">twee reacties</a> /  <a href="/pivot/entry.php?id=1827&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2005-m06.php#e1827" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=1827&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e1850"></span><span class="archief"><a href="/pivot/entry.php?id=1850&amp;w=sunday_morning_teanotes">26 Juni 2005:RSS, podcast en de Maarseveense Plassen</a><br></span>
<span id="e1845"></span><span class="archief"><a href="/pivot/entry.php?id=1845&amp;w=sunday_morning_teanotes">19 Juni 2005:Fields of Rock</a><br></span>
<span id="e1827"></span><span class="archief"><a href="/pivot/entry.php?id=1827&amp;w=sunday_morning_teanotes">05 Juni 2005:Virale filmpjes</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>