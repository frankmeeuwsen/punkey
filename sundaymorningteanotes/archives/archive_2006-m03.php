<?php
	// First line defense.
	if (file_exists("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php")) {
		include_once("/home/virtual/site102/fst/var/www/html/pivot/first_defense.php");
		block_refererspam();
	}
	?><?php 
 DEFINE('INWEBLOG', TRUE);
 $Current_weblog='sunday_morning_teanotes';
 include_once '/home/virtual/site102/fst/var/www/html/pivot/pv_core.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
 <title>Sunday Morning Teanotes - </title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link rel="stylesheet" type="text/css" href="/pivot/templates/sundaymorningteanotes.css" media="screen" />
 <link rel="stylesheet" type="text/css" href="/extensions/calendar/calendar.css" />

 <link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/SundayMorningTeanotes"/>
<script src="/mint/mint.js.php" type="text/javascript" language="javascript"></script>
<!-- Includes for Lightbox script -->
<script type="text/javascript">
var loadingImage = '/extensions/lightbox/loading.gif';
</script>
<script type="text/javascript" src="/extensions/lightbox/lightbox.js"></script>
<link rel="stylesheet" href="/extensions/lightbox/lightbox.css" type="text/css" media="screen" />

</head>
<body>

<div id="container">

<div id="header">
 <h1 class='shadow'>Sunday Morning Teanotes</h1>
 <h1 class='top'><a href="/sundaymorningteanotes/index.php">Sunday Morning Teanotes</a></h1>

 <h5 class='shadow'></h5>
 <h5 class='top'></h5>
</div>
<div id="rssbutton"><a href="http://feeds.feedburner.com/SundayMorningTeanotes"  title="XML Feed (RSS 2.0)" target='_blank'><img src="/pivot/pics/rssbutton.png" width="94" height="15" alt="XML Feed (RSS 1.0)" class="badge" longdesc="http://feeds.feedburner.com/SundayMorningTeanotes" /></a></div>
<div id="main-2columns">
<span id="e7015"></span><div class="entry">
<h3><span class='date'>27 Maart 06 - 22&#58;13</span>Sunday morning teanotes</h3>

<p>En jullie denken dat wij de enige bloggers zijn met <a href="entry.php?id=7005&amp;w=sunday_morning_teanotes"  target='_blank'>uitbreidingsplannen</a>? <a href="http://www.10e.nl/pivot/entry.php?id=1590#comm"  target='_blank'>Think again...</a></p><p>Het zal wel in het water zitten ofzo...</p> 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=_SMTN&amp;w=sunday_morning_teanotes">_SMTN</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=7015&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=7015&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2006-m03.php#e7015" title="Permanent link to 'Sunday morning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=7015&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sunday morning teanotes'">&para;</a>--> </p>
</div><span id="e7014"></span><div class="entry">
<h3><span class='date'>05 Maart 06 - 09&#58;11</span>Sundaymorning teanotes</h3>

<p>Het is weer zondag en aangezien ik toch vroeg uit bed ben kan er wel weer eens een SMTN vanaf vind je niet? Het is nog zo vroeg dat er alleen homeshopping op TV is, dus de MP3 speler introduceert rustig op de achtergrond wat nieuwe muziek voor me. Ik heb volgens mij enorm veel te vertellen maar ik geef mezelf te weinig tijd om het allemaal vertellen. Want er speelt &eacute;&eacute;n onderwerp de komende tijd volcontinu door mijn hersens</p><p><strong>Dutch Bloggies 2006</strong></p><p>Ja, zaterdag 11 maart is het zover. Dan gaan we de prijzen uitreiken in <a href="http://www.muscom.nl"  target='_blank'>het Museum voor Communicatie</a> te Den Haag. In de mooie Graham Bell zaal kun je vanaf 12 uur gratis binnen. Om 13.00 uur beginnen we. We hebben geleerd van voorgaande jaren en geprobeerd de hele uitreiking wat aantrekkelijker te maken. En minder eentonige winnaars te krijgen. Dat laatste is zeker gelukt. Of de uitreiking ook leuk wordt is natuurlijk nog afwachten maar de omstandigheden zijn gunstig. We hebben wat DJ's voor de pauze, een presentator die de boel aan elkaar lult en een videoblogger die de nominatiefilmpjes heeft gemaakt. En als alles goed gaat hebben we ook nog een afterparty in Den Haag. Ook zin om te komen? Laat het ons even via <a href="mailto:aanmelden@dutchbloggies.nl?subject=Ja,ik%20ben%20er%20bij%20de%2011e%20met%20xx%20personen"  target='_blank'>de mail</a> weten zodat we een idee hebben van de hoeveelheid bezoekers die we kunnen verwachten. </p><p><strong>Texel</strong></p><p>Yeah! Vorige week even heerlijk uitgewaaid op Texel. Wat een leuk eiland! Er is deze tijd van het jaar geen drol te doen maar dat was nu juist ook onze bedoeling. We wilden even alle hectiek ontvluchten en dat is wel aardig gelukt. We hebben twee dagen echt geen flikker uitgevoerd. Nou ja, dat is ook weer niet helemaal waar, we hebben wel een en ander bezocht op het eiland zoals het EcoMare, waar we de zeehond Robbie hebben geadopteerd. Omdat het schijnbaar de grootste eigenwijs is van de hele roedel. Check hem <a href="http://www.punkey.com/images/dsc02478.jpg"  rel="lightbox" title=""  target='_blank'>hier op de foto</a>. Verder hebben we het Juttersmuseum in Oudeschild bezocht. <a href="http://www.punkey.com/images/dsc02494.jpg"  rel="lightbox" title=""  target='_blank'>Een mooi stadje</a> met een leuk museum. Een van de mooiste onderdelen vond ik de maquette die is gemaakt van Texel tijdens de 17 eeuw. Zie <a href="http://www.punkey.com/images/dsc02498.jpg"  rel="lightbox" title=""  target='_blank'>hier een detailfoto</a> van een van de schepen. En we zijn even over het eiland gereden van zuid naar noord (20 minuten op een zondag) om de <a href="http://www.punkey.com/images/dsc02511.jpg"  rel="lightbox" title=""  target='_blank'>Texelse vuurtoren</a> te bekijken. Ik moet zeggen dat ik de Nieuwzeelandse vuurtoren op het meest noordelijke puntje van het land toch wat leuker vond.</p><p><strong>Technolust!</strong></p><p>Omdat ik zo druk bezig ben met de Dutch Bloggies zit ik ook meer achter mijn laptop dan normaal (nog meer?) Dat zorgt er voor dat ik de laatste tijd meer en meer nieuwe diensten en apps probeer. Gewoon, even installeren of accountje aanmaken en kijken of het wat is. Een paar geinige diensten:</p><ol><li><a href="http://www.calendarhub.com/"  target='_blank'>Calendarhub: </a>Handig om online mijn afspraken en reminders te hebben. </li><li><a href="http://www.furl.net"  target='_blank'>Furl</a>: Deze bookmarkmanager heb ik herontdekt. Uitgebreider dan del.icio.us maar ook goed samen te gebruiken met laatstgenoemde</li><li><a href="http://www.johnsadventures.com/backend/BackgroundSwitcher/"  target='_blank'>John's Background Switcher</a>: Desktop backgrounds vind ik al snel saai worden. Met dit kleine programmaatje kun je elk uur of elke 10 minuten een nieuwe background krijgen. Vul een favoriete Flickr-group in of tags en laat jezelf verrassen.</li><li><a href="http://www.stevemiller.net/puretext/"  target='_blank'>PureText</a>: Ja dit is echt een handige. Moet je vaak content van een website naar een Word document copieren? Dan maak je vast ook mee dat de opmaak wordt meegenomen en je weer loopt te klunzen met een tussenstap als Notepad. Is niet meer nodig! Installeer PureText en gebruik voortaan de sneltoets Windows-V in plaats van Control-V. Einde probleem! Natuurlijk configureerbaar voor andere sneltoetskeuzen.</li></ol><p>Dat was het weer voor de SMTN van vandaag. Ik ga zo naar de <a href="http://www.davidlloyd.nl"  target='_blank'>Healthclub</a> (hahahaha) om even een paar uur te trainen en te relaxen. Vanmiddag weer druk in de weer met de Dutch Bloggies!</p> 
 

<p class="info">  <span class="poster">punkey</span> - <!-- <span class="category"><a href="/pivot/archive.php?c=_SMTN&amp;w=sunday_morning_teanotes">_SMTN</a></span> --> 
	<span class="comments"><a href="/pivot/entry.php?id=7014&amp;w=sunday_morning_teanotes#comm" title="">Geen reacties</a> /  <a href="/pivot/entry.php?id=7014&amp;w=sunday_morning_teanotes#track" title="">Geen trackback</a></span><!-- - 
	<a href="/sundaymorningteanotes/archives/archive_2006-m03.php#e7014" title="Permanent link to 'Sundaymorning teanotes' in the archives">&sect;</a> <a href="/pivot/entry.php?id=7014&amp;w=sunday_morning_teanotes" title="Permanent link to entry 'Sundaymorning teanotes'">&para;</a>--> </p>
</div>
</div>
 <div style='clear: both;'>&nbsp;</div>
<div id="archief_container">
	<div id="archief_titel">Ouwe theeblaadjes</div>
<span id="e7015"></span><span class="archief"><a href="/pivot/entry.php?id=7015&amp;w=sunday_morning_teanotes">27 Maart 2006:Het zal wel in de lucht zitten</a><br></span>
<span id="e7014"></span><span class="archief"><a href="/pivot/entry.php?id=7014&amp;w=sunday_morning_teanotes">05 Maart 2006:Technofreak!</a><br></span>

	</div>
</div>
 </div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-166073-2";
urchinTracker();
</script>
<script type="text/javascript" src="http://www.assoc-amazon.com/s/link-enhancer?tag=punkeycom-20"></script>
<noscript><img src="http://www.assoc-amazon.com/s/noscript?tag=punkeycom-20" alt="" /></noscript>
</body>
</html>